//
//  main.m
//  TestServer
//
//  Created by John Scott on 28/05/2014.
//  Copyright (c) 2014 Clarity. All rights reserved.
//

/*
 
 This program is mainly used to test the object -> XML parts of RESTController/RESTObject
 which wouldn't get tested otherwise. Enabling this functuonality has an effect on the
 desigen of the XML -> object leg so it's important to give it a kick every so often.
 
 */

#import <Foundation/Foundation.h>

#import "GCDWebServer.h"
#import "GCDWebServerDataResponse.h"
#import "GCDWebServerDataRequest.h"
#import "RESTController.h"

#import "RESTServerBase.h"

int main(int argc, const char* argv[]) {
    @autoreleasepool {
        
        RESTServerBase *restServer = [[RESTServerBase alloc] init];
        
        // Create server
        GCDWebServer* webServer = [[GCDWebServer alloc] init];
        
        // Add a handler to respond to GET requests on any URL
        [webServer addDefaultHandlerForMethod:@"GET"
                                 requestClass:[GCDWebServerRequest class]
                                 processBlock:^GCDWebServerResponse *(GCDWebServerRequest* request) {
                                     
                                     return [GCDWebServerDataResponse responseWithHTML:@"<html><body><p>Hello World</p></body></html>"];
                                     
                                 }];
        
        
        [restServer addHandlers:webServer];
                
        // Use convenience method that runs server on port 8080
        // until SIGINT (Ctrl-C in Terminal) or SIGTERM is received
        [webServer runWithPort:8080 bonjourName:nil];
        NSLog(@"Visit %@ in your web browser", webServer.serverURL);
        
    }
    return 0;
}