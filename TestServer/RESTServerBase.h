//
//  RESTServer.h
//  mPOS
//
//  Created by John Scott on 16/06/2014.
//  Copyright (c) 2014 Clarity. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RESTController.h"

@interface RESTServerBase : NSObject <RESTControllerMethods>

-(void)addHandlers:(GCDWebServer*)webServer;

@end
