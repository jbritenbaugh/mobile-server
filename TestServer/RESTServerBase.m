//
//  RESTServer.m
//  mPOS
//
//  Created by John Scott on 16/06/2014.
//  Copyright (c) 2014 Clarity. All rights reserved.
//

#import "GCDWebServer.h"

#import "RESTServerBase.h"
#import "GCDWebServerDataRequest.h"
#import "GCDWebServerDataResponse.h"

@implementation RESTServerBase

-(Stores *)listStoresWithBrandId:(NSString *)brandId
                     isHomeStore:(BOOL)isHomeStore
                        latitude:(double)latitude
                       longitude:(double)longitude
                       maxResult:(NSInteger)maxResult
                           error:(NSError *__autoreleasing *)error
                     serverError:(ServerError *__autoreleasing *)serverError
{
    Till *till = [Till tillWithString:@"1"];
    
    Store *store = [[Store alloc] init];
    store.storeId = @"1";
    store.name = @"Bob's store";
    store.tills = @[till];
    
    Stores *stores = [Stores storesWithArray:@[store]];
    return stores;
}

-(void)addHandlers:(GCDWebServer*)webServer
{
    __weak __typeof(self) welf = self;
    if ([self respondsToSelector:@selector(listStoresWithBrandId:isHomeStore:latitude:longitude:maxResult:error:serverError:)])
    {
        [webServer addHandlerForMethod:@"GET" path:@"/MShopper/private/store/list"
                          requestClass:[GCDWebServerDataRequest class]
                          processBlock:^GCDWebServerResponse *(GCDWebServerRequest *request) {
                              GCDWebServerDataRequest *dataRequest = (GCDWebServerDataRequest*)request;
                              
                              [RESTController.sharedInstance objectForData:dataRequest type:@""
                              
                              NSError *error = nil;
                              ServerError *serverError = nil;
                              
                              Stores *result = [welf listStoresWithBrandId:nil isHomeStore:YES latitude:0 longitude:0 maxResult:1 error:&error serverError:&serverError];
                              
                              NSString *content = [RESTController.sharedInstance stringForObject:result type:@"Stores"];
                              
                              return [GCDWebServerDataResponse responseWithHTML:content];
                          }];

    }
         
    if ([self respondsToSelector:@selector(loginAssociateWithCredentials:signOffExistingOperator:error:serverError:)])
    {
        [webServer addHandlerForMethod:@"GET" path:@"/MShopper/private/auth"
                          requestClass:[GCDWebServerDataRequest class]
                          processBlock:^GCDWebServerResponse *(GCDWebServerRequest *request) {
                              GCDWebServerDataRequest *dataRequest = (GCDWebServerDataRequest*)request;
                              
                              Credentials* credentials = [RESTController.sharedInstance objectForData:dataRequest type:@"Credentials"];
                              BOOL signOffExistingOperator = [dataRequest.query[signOffExistingOperator] boolValue];
                              NSError *error = nil;
                              ServerError *serverError = nil;
                              
                              Credentials* result = [welf loginAssociateWithCredentials:credentials signOffExistingOperator:signOffExistingOperator error:&error serverError:&serverError];
                              
                              NSString *content = [RESTController.sharedInstance stringForObject:result type:@"Credentials"];
                              
                              return [GCDWebServerDataResponse responseWithHTML:content];
                          }];
        
    }
}

@end
