//
//  OCGDebugger.h
//  mPOS
//
//  Created by John Scott on 19/05/2015.
//  Copyright (c) 2015 Clarity. All rights reserved.
//

#import <Foundation/Foundation.h>

/*
 OCGLog / OCGLogv should be used as replacements for NSLog / NSLogv within Objective-C
 methods ONLY.
 */

void OCGLog(NSString *format, ...) NS_FORMAT_FUNCTION(1,2);
void OCGLogv(NSString *format, va_list args) NS_FORMAT_FUNCTION(1,0);

/*
 OCGCLog / OCGCLogv should be used as replacements for NSLog / NSLogv within normal C
 functions.
 */

void OCGCLog(NSString *format, ...) NS_FORMAT_FUNCTION(1,2);
void OCGCLogv(NSString *format, va_list args) NS_FORMAT_FUNCTION(1,0);

/**
 @brief This function should be called from within application:didFinishLaunchingWithOptions:
 to start the debugger.
 */

void OCGDebuggerDidFinishLaunchingWithOptions(NSDictionary* launchOptions);

/**
 @brief Returns the address to access the debugger
 */

NSURL *OCGDebuggerServerAddress();

/**
 @brief Returns TRUE if the current process is being debugged (either running under the
 debugger or has a debugger attached post facto).
 */
BOOL OCGDebuggerIsApplicationBeingDebugged(void);

/**
 @brief A utility function that returns a HTML representation of the value.
 */

NSString* OCGDebuggerHtmlObjectDescription(id value, NSDictionary *formatOptions);

@protocol OCGDebuggerHtmlDescription <NSObject>
@optional

-(NSString*)OCGDebugger_htmlDescriptionWithFormatOptions:(NSDictionary *)formatOptions;

@end

/**********************************************************************************************************************************
 
 NOTHING BELOW THIS LINE SHOULD BE USED IN NEW CODE
 
 **********************************************************************************************************************************/


void _OCGLog(id self, SEL _cmd, const char* function, int line, const char* file, NSString *format, ...) NS_FORMAT_FUNCTION(6,7);
void _OCGLogv(id self, SEL _cmd, const char* function, int line, const char* file, NSString *format, va_list args) NS_FORMAT_FUNCTION(6,0);

#define OCGLog(format, ...) _OCGLog(self, _cmd, __FUNCTION__, __LINE__, __FILE__, format, ## __VA_ARGS__)
#define OCGLogv(format, args) _OCGLogv(self, _cmd, __FUNCTION__, __LINE__, __FILE__, format, args)

#define OCGCLog(format, ...) _OCGLog(nil, NULL, __FUNCTION__, __LINE__, __FILE__, format, ## __VA_ARGS__)
#define OCGCLogv(format, args) _OCGLogv(nil, NULL, __FUNCTION__, __LINE__, __FILE__, format, args)

/*
 Handle the built in function. It's redirected to OCGCLog as we can't guarentee it's
 always used in a method. (hint, it isn't)
 */

#define NSLog(format, ...) OCGCLog(format, ## __VA_ARGS__)
#define NSLogv(format, args) OCGCLogv(format, args)

/*
 Historic macros from OCGLogger. Use OCGLog, OCGLogv, OCGCLog and OCGCLogv instead.
 */

#define DebugLog(format, ...) OCGLog(format, ## __VA_ARGS__)
#define ErrorLog(format, ...) OCGLog(format, ## __VA_ARGS__)
#define WarningLog(format, ...) OCGLog(format, ## __VA_ARGS__)
#define InfoLog(format, ...) OCGLog(format, ## __VA_ARGS__)

#define DebugLog2(format, ...) OCGLog([format stringByReplacingOccurrencesOfString:@"{}" withString:@"%@"], ## __VA_ARGS__)
#define ErrorLog2(format, ...) OCGLog([format stringByReplacingOccurrencesOfString:@"{}" withString:@"%@"], ## __VA_ARGS__)
