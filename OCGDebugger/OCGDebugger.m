//
//  OCGDebugger.m
//  mPOS
//
//  Created by John Scott on 19/05/2015.
//  Copyright (c) 2015 Clarity. All rights reserved.
//

#ifdef NSLog
#undef NSLog
#endif

#ifdef NSLogv
#undef NSLogv
#endif

#import <objc/runtime.h>

#import "GCDWebServer.h"
#import "GCDWebServerDataResponse.h"
#import "GCDWebServerStreamedResponse.h"

#include <sys/sysctl.h>

@interface UIDevice ()

- (id)_deviceInfoForKey:(NSString*)key;
- (id)deviceInfoForKey:(NSString*)key;

@end

static GCDWebServer* _webServer = nil;
static NSMutableArray *_waitingCompletionBlocks;
static dispatch_queue_t _responseQueue;

static NSUInteger _messageCount = 0;
static NSMutableArray *_messageBuffer;
static CFAbsoluteTime _messageBufferTimeLimit;

static NSDateFormatter *_timestampFormatter;
static NSDateFormatter *_dateFormatter;

void _OCGDebuggerASync(dispatch_block_t block);
void _OCGDebuggerDidFinishLaunchingWithOptions(NSDictionary* launchOptions);

NSUInteger _OCGDebuggerHashInRange(NSString *string, NSUInteger min, NSUInteger max);
NSString *_OCGDebuggerHtmlColorForString(NSString *string, NSUInteger brightness);
NSString* _OCGDebuggerHtmlObjectDescription(id value, NSDictionary *formatOptions);
NSString* _OCGDebuggerStringObjectDescription(id value, NSDictionary *formatOptions);
void _OCGMessage(NSString *format, ...) NS_FORMAT_FUNCTION(1,2);
void _OCGDebuggerRainbowBackgroundColor(UIView *view);

void OCGDebuggerDidFinishLaunchingWithOptions(NSDictionary* launchOptions)
{
#if DEBUG
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _OCGDebuggerDidFinishLaunchingWithOptions(launchOptions);
    });
#endif
}

NSURL *OCGDebuggerServerAddress()
{
    NSURL *serverAddress = nil;
#if DEBUG
    serverAddress = _webServer.serverURL;
#endif
    return serverAddress;
}

void _OCGDebuggeraddHandlerForAction(NSMutableString *classDescriptions, NSString *name, dispatch_block_t block)
{
    [classDescriptions appendFormat:@"<button onclick='(new Image()).src=\"%@\"' style='background-color: %@'>%@</button>", name, _OCGDebuggerHtmlColorForString([NSString stringWithFormat:@"X%@O", name], 85), name];
    [_webServer addHandlerForMethod:@"GET"
                               path:[@"/" stringByAppendingString:name]
                       requestClass:[GCDWebServerRequest class]
                       processBlock:^GCDWebServerResponse *(GCDWebServerRequest* request)
     {
         block();
         return [GCDWebServerDataResponse responseWithData:NSData.data contentType:@"image/png"];
     }];
}

void _OCGDebuggerDidFinishLaunchingWithOptions(NSDictionary* launchOptions)
{
    NSMutableString *classDescriptions = [NSMutableString string];
//    
//    unsigned int classCount = 0;
//    Class *classList = objc_copyClassList(&classCount);
//    NSMutableDictionary *classColors = [NSMutableDictionary dictionary];
//    for (unsigned int classIndex=0; classIndex<classCount; classIndex++)
//    {
//        NSString *imageName = [NSString stringWithUTF8String:class_getImageName(classList[classIndex])];
//        if ([imageName isEqualToString:NSBundle.mainBundle.executablePath])
//        {
//            NSString *className = NSStringFromClass(classList[classIndex]);
//            
//            className = [className stringByReplacingOccurrencesOfString:@"^_+"
//                                                             withString:@""
//                                                                options:NSRegularExpressionSearch
//                                                                  range:NSMakeRange(0, [className length])];
//            
//            classColors[className] = [NSString stringWithFormat:@"<span style='background: %@'>%@</span>", _OCGDebuggerHtmlColorForString(className, 89), NSStringFromClass(classList[classIndex])];
//        }
//    }
//    free(classList);
//    
    [classDescriptions appendString:@"<div id='classColors'>"];
//    for (NSString* header in [classColors.allKeys sortedArrayUsingSelector:@selector(compare:)])
//    {
//        [classDescriptions appendFormat:@"\n%@", classColors[header]];
//    }
    
    // Create server
    [GCDWebServer setLogLevel:4];
    _webServer = [[GCDWebServer alloc] init];
    
    _OCGDebuggeraddHandlerForAction(classDescriptions, @"abort", ^{
        _OCGMessage(@"Received abort request");
        abort();
    });
    
    _OCGDebuggeraddHandlerForAction(classDescriptions, @"device info", ^{
        UIDevice *device = [UIDevice currentDevice];
        NSArray *deviceInfoKeys = @[
                                    @"ActiveWirelessTechnology",  @"AirplaneMode",  @"assistant",  @"BasebandCertId",
                                    @"BasebandChipId",  @"BasebandPostponementStatus",  @"BasebandStatus",
                                    @"BatteryCurrentCapacity",  @"BatteryIsCharging",  @"BluetoothAddress",
                                    @"BoardId",  @"BootNonce",  @"BuildVersion",  @"CertificateProductionStatus",
                                    @"CertificateSecurityMode",  @"ChipID",  @"CompassCalibrationDictionary",
                                    @"CPUArchitecture",  @"DeviceClass",  @"DeviceColor",  @"DeviceEnclosureColor",
                                    @"DeviceEnclosureRGBColor",  @"DeviceName",  @"DeviceRGBColor",
                                    @"DeviceSupportsFaceTime",  @"DeviceVariant",  @"DeviceVariantGuess",  @"DiagData",
                                    @"dictation",  @"DiskUsage",  @"EffectiveProductionStatus",
                                    @"EffectiveProductionStatusAp",  @"EffectiveProductionStatusSEP",
                                    @"EffectiveSecurityMode",  @"EffectiveSecurityModeAp",  @"EffectiveSecurityModeSEP",
                                    @"FirmwarePreflightInfo",  @"FirmwareVersion",  @"FrontFacingCameraHFRCapability",
                                    @"HardwarePlatform",  @"HasSEP",  @"HWModelStr",  @"Image4Supported",  @"InternalBuild",
                                    @"InverseDeviceID",  @"ipad",  @"MixAndMatchPrevention",  @"MLBSerialNumber",
                                    @"MobileSubscriberCountryCode",  @"MobileSubscriberNetworkCode",  @"ModelNumber",
                                    @"PartitionType",  @"PasswordProtected",  @"ProductName",  @"ProductType",
                                    @"ProductVersion",  @"ProximitySensorCalibrationDictionary",
                                    @"RearFacingCameraHFRCapability",  @"RegionCode",  @"RegionInfo",
                                    @"SDIOManufacturerTuple",  @"SDIOProductInfo",  @"SerialNumber",  @"SIMTrayStatus",
                                    @"SoftwareBehavior",  @"SoftwareBundleVersion",  @"SupportedDeviceFamilies",
                                    @"SupportedKeyboards",  @"telephony",  @"UniqueChipID",  @"UniqueDeviceID",
                                    @"UserAssignedDeviceName",  @"wifi",  @"WifiVendor",
                                    ];
        
        NSMutableDictionary *deviceInfo = [NSMutableDictionary dictionary];
        if ([device respondsToSelector:@selector(deviceInfoForKey:)])
        {
            for (NSString *deviceInfoKey in deviceInfoKeys)
            {
                [deviceInfo setValue:[device deviceInfoForKey:deviceInfoKey] forKey:deviceInfoKey];
            }
        }
        else if ([device respondsToSelector:@selector(_deviceInfoForKey:)])
        {
            for (NSString *deviceInfoKey in deviceInfoKeys)
            {
                [deviceInfo setValue:[device _deviceInfoForKey:deviceInfoKey] forKey:deviceInfoKey];
            }
        }
        
        _OCGMessage(@"Device info: %@", deviceInfo);
    });
    
    _OCGDebuggeraddHandlerForAction(classDescriptions, @"exit", ^{
        _OCGMessage(@"Received exit request");
        exit(1);
    });
    
    _OCGDebuggeraddHandlerForAction(classDescriptions, @"layout", ^{
        _OCGMessage(@"Layouts: %@", UIApplication.sharedApplication.windows);
    });
    
    _OCGDebuggeraddHandlerForAction(classDescriptions, @"rainbow", ^{
        ocg_sync_main(^{
            _OCGDebuggerRainbowBackgroundColor(UIApplication.sharedApplication.delegate.window);
        });
        _OCGMessage(@"Rainbowed main window");
    });
    
    
    _OCGDebuggeraddHandlerForAction(classDescriptions, @"screenshot", ^{
        UIWindow *window = UIApplication.sharedApplication.delegate.window;
        
        UIGraphicsBeginImageContextWithOptions(window.bounds.size, window.opaque, 0.0);
        [window.layer renderInContext:UIGraphicsGetCurrentContext()];
        UIImage *screenshot = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        
        _OCGMessage(@"%@", screenshot);
    });
    
    [classDescriptions appendString:@"</div>"];
    
    [_webServer addHandlerForMethod:@"GET"
                               path:@"/"
                       requestClass:[GCDWebServerRequest class]
                       processBlock:^GCDWebServerResponse *(GCDWebServerRequest* request)
     {
         __block BOOL hasRespondedWithHeader = NO;
         GCDWebServerAsyncStreamBlock streamBlock = ^(GCDWebServerBodyReaderCompletionBlock completionBlock)
         {
             _OCGDebuggerASync(^{
                 if (hasRespondedWithHeader)
                 {
                     [_waitingCompletionBlocks addObject:completionBlock];
                 }
                 else
                 {
                     hasRespondedWithHeader = YES;
                     
                     NSMutableString *htmlHeader = [NSMutableString string];
                     [htmlHeader appendString:@"<!DOCTYPE html>"];
                     [htmlHeader appendString:@"<html><head>"];
                     [htmlHeader appendString:@"<link href='debugger.css' media='all' rel='stylesheet'/>"];
                     [htmlHeader appendString:@"</head><body>"];
                     [htmlHeader appendString:[@"" stringByPaddingToLength:1<<14 withString:@"\r\n" startingAtIndex:0]];
                     [htmlHeader appendString:classDescriptions];
                     [htmlHeader appendString:@"<div id='messages'>"];
                     for (NSString *message in _messageBuffer)
                     {
                         [htmlHeader appendString:message];
                     }
                     completionBlock([htmlHeader dataUsingEncoding:NSUTF8StringEncoding], nil);
                 }
             });
         };
         
         return [GCDWebServerStreamedResponse responseWithContentType:@"text/html"
                                                     asyncStreamBlock:streamBlock];
     }];
    
    NSString *debuggerCSSPath = [NSBundle.mainBundle.bundlePath stringByAppendingPathComponent:@"debugger.css"];
    
    [_webServer addGETHandlerForPath:@"/debugger.css"
                            filePath:debuggerCSSPath
                        isAttachment:NO
                            cacheAge:0
                  allowRangeRequests:YES];
    
    [_webServer addHandlerForMethod:@"GET" pathRegex:@"^/save/[^/]+/"
                       requestClass:[GCDWebServerRequest class]
                       processBlock:^GCDWebServerResponse *(GCDWebServerRequest* request)
     {
         NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:@"^/save/([^/]+)/(.*)$"
                                                               options:NSRegularExpressionCaseInsensitive
                                                                 error:NULL];
         
         NSTextCheckingResult* match = [regex firstMatchInString:request.URL.path
                                                         options:0
                                                           range:NSMakeRange(0, [request.URL.path length])];

         NSString *path = [request.URL.path substringWithRange:[match rangeAtIndex:1]];
         NSData *data = [[NSData alloc] initWithBase64EncodedString:[request.URL.path substringWithRange:[match rangeAtIndex:2]]
                                                            options:kNilOptions];
         GCDWebServerResponse *response = [GCDWebServerDataResponse responseWithData:data contentType:@"application/octet-stream"];
         [response setValue:[NSString stringWithFormat:@"attachment; filename=%@", path] forAdditionalHeader:@"Content-Disposition"];
         return response;
     }];
    
    // Start server on port 8080
    [_webServer startWithPort:8080 bonjourName:nil];
    NSLog(@"Debugging at %@", _webServer.serverURL);
}

/*
 _CFStringCreateWithFormatAndArgumentsAux is the private function from CFString.c which is used by
 +[NSString stringWithFormat:] (and everything else) to parse and build the final string.
 
 The reason it's needed here is for its parameter copyDescFunc, a function used to stringify
 objects - just what we need for producing a string in HTML (or anything else).
 */

CFStringRef  _CFStringCreateWithFormatAndArgumentsAux(CFAllocatorRef alloc, CFStringRef (*copyDescFunc)(void *, const void *), CFDictionaryRef formatOptions, CFStringRef format, va_list arguments);

CFStringRef _OCGDebuggerStringCopyDesctiption(void * anObject, const void * formatOptions);
CFStringRef _OCGDebuggerHtmlCopyDesctiption(void * anObject, const void * formatOptions);

void _OCGLog(id self, SEL _cmd, const char* function, int line, const char* file, NSString *format, ...)
{
    va_list argList;
    
    va_start(argList, format);
    _OCGLogv(self, _cmd, function, line, file, format, argList);
    va_end(argList);
}

void _OCGLogv(id self, SEL _cmd, const char* function, int line, const char* file, NSString *format, va_list args)
{
#if DEBUG
    NSDictionary *formatOptions = @{};
    __block NSString *htmlMessage = (__bridge NSString*) _CFStringCreateWithFormatAndArgumentsAux(NULL, _OCGDebuggerHtmlCopyDesctiption, (__bridge CFDictionaryRef)formatOptions, (__bridge CFStringRef)format, args);
    
    __block NSString *stringMessage = (__bridge NSString*) _CFStringCreateWithFormatAndArgumentsAux(NULL, _OCGDebuggerStringCopyDesctiption, (__bridge CFDictionaryRef)formatOptions, (__bridge CFStringRef)format, args);

    _OCGDebuggerASync(^{
        
        NSMutableString *title = [NSMutableString string];
        [title appendString:[_timestampFormatter stringFromDate:NSDate.date]];
        if (function)
        {
            [title appendFormat:@" %s", function];
        }
        
        if (line)
        {
            [title appendFormat:@" %d", line];
        }
        
        htmlMessage = [htmlMessage stringByReplacingOccurrencesOfString:@"(null)" withString:OCGDebuggerHtmlObjectDescription(nil, formatOptions)];
        
        htmlMessage = [NSString stringWithFormat:@"<div class='message'><div class='title'>%@ <a class='download' href='/save/%@-%@.txt/%@'>&#x2913;</a></div><div class='content' style='background: %@'>%@</div></div>",
                       title,
                       _webServer.serverURL.host,
                       [_dateFormatter stringFromDate:NSDate.date],
                       [[stringMessage dataUsingEncoding:NSUTF8StringEncoding] base64EncodedStringWithOptions:kNilOptions],
                       _OCGDebuggerHtmlColorForString(NSStringFromClass([self class]), 89),
                       htmlMessage];
        
        _messageCount++;
        
        if (_messageBuffer && _messageBufferTimeLimit < CFAbsoluteTimeGetCurrent())
        {
            _messageBuffer = nil;
        }
        
        [_messageBuffer addObject:htmlMessage];

        NSData *messageData = [htmlMessage dataUsingEncoding:NSUTF8StringEncoding];
        
        for (GCDWebServerBodyReaderCompletionBlock completionBlock in _waitingCompletionBlocks)
        {
            completionBlock(messageData, nil);
        }
        
        [_waitingCompletionBlocks removeAllObjects];
    });
    
    NSLog(@"%@", stringMessage);
#endif
}

void _OCGMessage(NSString *format, ...)
{
    va_list argList;
    
    va_start(argList, format);
    _OCGLogv(nil, NULL, NULL, 0, NULL, format, argList);
    va_end(argList);
}

CFStringRef _OCGDebuggerHtmlCopyDesctiption(void * anObject, const void * formatOptions)
{
    return CFBridgingRetain(OCGDebuggerHtmlObjectDescription((__bridge id) anObject, (__bridge NSDictionary *) formatOptions));
}

CFStringRef _OCGDebuggerStringCopyDesctiption(void * anObject, const void * formatOptions)
{
    return CFBridgingRetain(_OCGDebuggerStringObjectDescription((__bridge id) anObject, (__bridge NSDictionary *) formatOptions));
}

void _OCGDebuggerASync(dispatch_block_t block)
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _waitingCompletionBlocks =  [NSMutableArray array];
        _messageBuffer =  [NSMutableArray array];
        _messageBufferTimeLimit = CFAbsoluteTimeGetCurrent() + 60;
        _responseQueue = dispatch_queue_create("com.omnicogroup.debugger", DISPATCH_QUEUE_SERIAL);
        
        _timestampFormatter = [[NSDateFormatter alloc] init];
        _timestampFormatter.dateFormat = @"HH:mm:ss.SSS";
        
        _dateFormatter = [[NSDateFormatter alloc] init];
        /* http://www.w3.org/TR/NOTE-datetime YYYY-MM-DDThh:mm:ss.sTZD */
        _dateFormatter.dateFormat = @"yyyy-MM-dd'T'HH-mm-ss";
    });
    dispatch_async(_responseQueue, block);
}

NSString* OCGDebuggerHtmlObjectDescription(id value, NSDictionary *formatOptions)
{
    if ([value respondsToSelector:@selector(OCGDebugger_htmlDescriptionWithFormatOptions:)])
    {
        return [value OCGDebugger_htmlDescriptionWithFormatOptions:formatOptions];
    }
    else
    {
        return _OCGDebuggerHtmlObjectDescription(value, formatOptions);
    }
}

NSString* _OCGDebuggerHtmlObjectDescription(id value, NSDictionary *formatOptions)
{
    NSMutableString *result = [NSMutableString string];
    if ([value isKindOfClass:[NSString class]])
    {
        if ([value length] > 0)
        {
            NSData *data = [value dataUsingEncoding:NSUTF32LittleEndianStringEncoding];
            
            const uint32_t *characters = data.bytes;
            const NSUInteger characterCount = data.length / sizeof(uint32_t);
            
            for (NSUInteger characterIndex = 0; characterIndex < characterCount; characterIndex++)
            {
                uint32_t character = characters[characterIndex];
                BOOL showHex = NO;
                
                if (character == 0x20)
                {
                    character = 0x2423;
                }
                else if (character <= 0x1f && character != 0x0a)
                {
                    showHex = YES;
                }
                else if (character == 0x7f)
                {
                    showHex = YES;
                }
                
                if (showHex)
                {
                    [result appendFormat:@"&thinsp;<span class='unprintable'>%02x</span>&thinsp;", character];
                }
                else if (character == characters[characterIndex])
                {
                    [result appendFormat:@"&#x%x;", character];
                }
                else
                {
                    [result appendFormat:@"<span class='unprintable'>&#x%x;</span>", character];
                }
            }
        }
        else
        {
            [result appendString:@"<span class='nil'>&#x03B5</span>"];
        }
    }
    else if ([value isKindOfClass:[NSArray class]])
    {
        [result appendString:@"<ol start='0' class='NSArray'>"];
        for (NSString *childValue in value)
        {
            [result appendFormat:@"<li>%@</li>", OCGDebuggerHtmlObjectDescription(childValue, formatOptions)];
        }
        [result appendString:@"</ol>"];
    }
    else if ([value isKindOfClass:[NSDictionary class]])
    {
        NSArray *dictionaryKeys = [[value allKeys] sortedArrayUsingSelector:@selector(caseInsensitiveCompare:)];
        [result appendString:@"<table class='dictionary'>"];
        for (NSString *dictionaryKey in dictionaryKeys)
        {
            id dictionaryValue = [value objectForKey:dictionaryKey];
            [result appendFormat:@"<tr><th>%@</th><td>%@</td></tr>", OCGDebuggerHtmlObjectDescription(dictionaryKey, formatOptions), OCGDebuggerHtmlObjectDescription(dictionaryValue, formatOptions)];
        }
        [result appendString:@"</table>"];
    }
    else if ([value isKindOfClass:[NSURL class]])
    {
        [result appendString:@"<span class='url'>"];
        [result appendString:OCGDebuggerHtmlObjectDescription([value absoluteString], formatOptions)];
        [result appendString:@"</span>"];
        [result appendString:@"<table class='url'>"];
        // split into key/value pairs
        NSArray *queryKeyValues = [[value query] componentsSeparatedByString:@"&"];
        
        for (NSString *keyValue in queryKeyValues) {
            // find the '=' seperator
            NSRange equalRange = [keyValue rangeOfString:@"="];
            if (equalRange.location != NSNotFound) {
                // split the pair
                NSString *key = [keyValue substringToIndex:equalRange.location];
                NSString *value = [keyValue substringFromIndex:equalRange.location + 1];
                // undo percent escapes
                value = [value stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
                NSInteger maxLength = 60;
                if ([value length] > maxLength)
                {
                    value = [value stringByReplacingCharactersInRange:NSMakeRange(maxLength-3, [value length]-(maxLength-3)) withString:@"..."];
                }
                
                [result appendFormat:@"<tr><th>%@</th><td>%@</td></tr>", OCGDebuggerHtmlObjectDescription(key, formatOptions), OCGDebuggerHtmlObjectDescription(value, formatOptions)];
            }
        }
        [result appendString:@"</table>"];
    }
    else if ([value isKindOfClass:[NSData class]])
    {
        NSString *stringValue = [[NSString alloc] initWithData:value encoding:NSUTF8StringEncoding];
        if (stringValue != nil)
        {
            if ([stringValue hasPrefix:@"<?xml "] || [stringValue hasPrefix:@"<!DOCTYPE"])
            {
                NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:@"(?:<[^>]+>)|([^<]+)" options:0 error:NULL];
                NSArray *matches = [regex matchesInString:stringValue options:0 range:NSMakeRange(0, [stringValue length])];
                
                for (NSTextCheckingResult* match in matches)
                {
                    NSString *component = [stringValue substringWithRange:[match range]];
                    if ([component hasPrefix:@"</"])
                    {
                        [result appendString:OCGDebuggerHtmlObjectDescription(component, formatOptions)];
                        [result appendString:@"</x>"];
                    }
                    else if ([component hasPrefix:@"<"])
                    {
                        [result appendString:@"<x>"];
                        [result appendString:OCGDebuggerHtmlObjectDescription(component, formatOptions)];
                        if ([component hasSuffix:@"/>"] || [component hasPrefix:@"<?"] || [component hasPrefix:@"<!"])
                        {
                            [result appendString:@"</x>"];
                        }
                    }
                    else
                    {
                        if ([component length] > 100)
                        {
                            [result appendString:@"<y>"];
                        }
                        [result appendString:OCGDebuggerHtmlObjectDescription(component, formatOptions)];
                        if ([component length] > 100)
                        {
                            [result appendString:@"</y>"];
                        }
                    }
                }
            }
            else
            {
                [result appendString:OCGDebuggerHtmlObjectDescription(stringValue, formatOptions)];
            }
        }
        else
        {
            [result appendString:OCGDebuggerHtmlObjectDescription([value description], formatOptions)];
        }
    }
    else if ([value isKindOfClass:[UIColor class]])
    {
        CGFloat red = 0.0, green = 0.0, blue = 0.0, alpha =0.0;
        [value getRed:&red green:&green blue:&blue alpha:&alpha];
        [result appendFormat:@"rgba(%.0f,%.0f,%.0f,%.3f) <span style='background-color:rgba(%.0f,%.0f,%.0f,%.2f);padding:3px;border: 1px solid #333;'>&nbsp;&nbsp;</span>", red*255, green*255, blue*255, alpha, red*255, green*255, blue*255, alpha];
    }
    else if ([value isKindOfClass:[NSError class]])
    {
        NSError *error = value;
        [result appendFormat:@"<span style='color:rgb(255,128,128);'>%d : %@</span>", [error code], OCGDebuggerHtmlObjectDescription([value localizedDescription], formatOptions)];
    }
    else if ([value isKindOfClass:[UIView class]])
    {
        [result appendString:@"<x>"];
        UIView *view = value;
        //        [result appendFormat:@"<img src='%@'>", [self imageForView:value]];
        
        if ([view respondsToSelector:@selector(nextResponder)] && [view.nextResponder isKindOfClass:[UIViewController class]])
        {
            [result appendFormat:@"<span style='font-style:italic;'>%@</span>", NSStringFromClass(view.nextResponder.class)];
            [result appendString:@"<x>"];
        }
        
        
        if ([view respondsToSelector:@selector(isFirstResponder)] && [view isFirstResponder])
        {
            [result appendFormat:@"<span style='color:rgb(255,128,128);'>%@</span>", NSStringFromClass(view.class)];
        }
        else
        {
            [result appendString:NSStringFromClass(view.class)];
        }
        
        if ([view respondsToSelector:@selector(accessibilityLabel)] && view.accessibilityLabel != nil)
        {
            [result appendFormat:@": %@", view.accessibilityLabel];
        }
        
        for (id subview in [value subviews])
        {
            [result appendString:OCGDebuggerHtmlObjectDescription(subview, formatOptions)];
        }
        
        if ([view respondsToSelector:@selector(nextResponder)] && [view.nextResponder isKindOfClass:[UIViewController class]])
        {
            [result appendString:@"</x>"];
        }
        
        [result appendString:@"</x>"];
    }
    else if ([value isKindOfClass:[UIImage class]])
    {
        NSData *imageData = UIImagePNGRepresentation(value);
        
        NSString *url = [NSString stringWithFormat:@"data:image/png;base64,%@", [imageData base64EncodedStringWithOptions:0]];

        [result appendFormat:@"<img src='%@'>", url];
    }
    else if ([value isKindOfClass:[NSIndexPath class]])
    {
        NSIndexPath *indexPath = value;
        if ([indexPath length] > 2)
        {
            [result appendString:OCGDebuggerHtmlObjectDescription([indexPath description], formatOptions)];
        }
        else
        {
            [result appendFormat:@"{section:%d row: %d}", [indexPath indexAtPosition:0], [indexPath indexAtPosition:1]];
        }
    }
    else if (value)
    {
        [result appendString:OCGDebuggerHtmlObjectDescription([value description], formatOptions)];
    }
    else
    {
        [result appendString:@"<span class='nil'>nil</span>"];
    }
    return result;
}

NSString* _OCGDebuggerStringObjectDescription(id value, NSDictionary *formatOptions)
{
    NSMutableString *result = [NSMutableString string];
    if ([value isKindOfClass:[NSDictionary class]])
    {
        NSMutableDictionary *textDictionary = [NSMutableDictionary dictionary];
        
        for (id key in value)
        {
            textDictionary[_OCGDebuggerStringObjectDescription(key, formatOptions)] = _OCGDebuggerStringObjectDescription(value[key], formatOptions);
        }
        [result appendString:[textDictionary description]];
    }
    else if ([value isKindOfClass:[NSData class]])
    {
        NSString *stringValue = [[NSString alloc] initWithData:value encoding:NSUTF8StringEncoding];
        if (stringValue != nil)
        {
            if ([stringValue hasPrefix:@"<?xml "])
            {
                NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:@"(?:<[^>]+>)|([^<]+)" options:0 error:NULL];
                NSArray *matches = [regex matchesInString:stringValue options:0 range:NSMakeRange(0, [stringValue length])];
                
                NSInteger depth = 0;
                BOOL afterTag = YES;
                for (NSTextCheckingResult* match in matches)
                {
                    NSString *component = [stringValue substringWithRange:[match range]];
                    if (afterTag && [component hasPrefix:@"<"])
                    {
                        [result appendString:@"\n"];
                        for (NSInteger i=0; i<depth; i++)
                        {
                            [result appendString:@"  "];
                        }
                        afterTag = NO;
                    }
                    if ([component hasPrefix:@"</"])
                    {
                        [result appendString:_OCGDebuggerStringObjectDescription(component, formatOptions)];
                        depth--;
                        afterTag = YES;
                    }
                    else if ([component hasPrefix:@"<"])
                    {
                        depth++;
                        [result appendString:@"  "];
                        [result appendString:_OCGDebuggerStringObjectDescription(component, formatOptions)];
                        if ([component hasSuffix:@"/>"] || [component hasSuffix:@"?>"])
                        {
                            depth--;
                        }
                        afterTag = YES;
                    }
                    else
                    {
                        [result appendString:_OCGDebuggerStringObjectDescription(component, formatOptions)];
                        afterTag = NO;
                    }
                }
            }
            else
            {
                [result appendString:_OCGDebuggerStringObjectDescription(stringValue, formatOptions)];
            }
        }
        else
        {
            stringValue = [value description];
            [result appendString:_OCGDebuggerStringObjectDescription(stringValue, formatOptions)];
        }
    }
    else if ([value isKindOfClass:[UIColor class]])
    {
        CGFloat red = 0.0, green = 0.0, blue = 0.0, alpha =0.0;
        [value getRed:&red green:&green blue:&blue alpha:&alpha];
        [result appendFormat:@"rgba(%.0f,%.0f,%.0f,%.3f)", red*255, green*255, blue*255, alpha];
    }
    else if (value == nil)
    {
        [result appendString:@"(null)"];
    }
    else
    {
        [result appendString:[value description]];
    }
    return result;
}

NSUInteger _OCGDebuggerHashInRange(NSString *string, NSUInteger min, NSUInteger max)
{
    NSUInteger hash = 0;
    
    NSData *data = [string dataUsingEncoding:NSUTF32LittleEndianStringEncoding];
    NSInteger charCount = data.length/sizeof(UTF32Char);
    const UTF32Char *characters = data.bytes;
    
    
    for (NSInteger charIndex=0; charIndex<charCount; charIndex++)
    {
        hash = ((hash<<5)-hash)+characters[charIndex];
    }
    
    hash = hash % (max - min);
    hash += min;
    return hash;
}

NSString *_OCGDebuggerHtmlColorForString(NSString *string, NSUInteger brightness)
{
    NSArray *parts = [[string stringByReplacingOccurrencesOfString:@"^([A-Z]+[a-z0-9]*)([A-Z].*)"
                                                        withString:@"$1\t$2"
                                                           options:NSRegularExpressionSearch
                                                             range:NSMakeRange(0, [string length])] componentsSeparatedByString:@"\t"];
    
    NSInteger hue = 0;
    if	(parts.count > 0)
    {
        hue = _OCGDebuggerHashInRange(parts[0], 0, 360);
    }
    
    NSInteger saturation = 0;
    if	(parts.count > 1)
    {
        saturation = _OCGDebuggerHashInRange(parts[1], 35, 75);
    }
    return [NSString stringWithFormat:@"hsl(%d, %d%%, %d%%)", hue, saturation, brightness];
}

void _OCGDebuggerRainbowBackgroundColor(UIView *view)
{
    CGFloat redLevel    = rand() / (float) RAND_MAX;
    CGFloat greenLevel  = rand() / (float) RAND_MAX;
    CGFloat blueLevel   = rand() / (float) RAND_MAX;
    
    view.backgroundColor = [UIColor colorWithRed: redLevel
                                           green: greenLevel
                                            blue: blueLevel
                                           alpha: 0.4];
    
    for (UIView *subview in view.subviews)
    {
        _OCGDebuggerRainbowBackgroundColor(subview);
    }
}

// code from http://stackoverflow.com/a/4746378

BOOL OCGDebuggerIsApplicationBeingDebugged(void)
{
#if DEBUG
    int                 junk;
    int                 mib[4];
    struct kinfo_proc   info;
    size_t              size;
    
    // Initialize the flags so that, if sysctl fails for some bizarre
    // reason, we get a predictable result.
    
    info.kp_proc.p_flag = 0;
    
    // Initialize mib, which tells sysctl the info we want, in this case
    // we're looking for information about a specific process ID.
    
    mib[0] = CTL_KERN;
    mib[1] = KERN_PROC;
    mib[2] = KERN_PROC_PID;
    mib[3] = getpid();
    
    // Call sysctl.
    
    size = sizeof(info);
    junk = sysctl(mib, sizeof(mib) / sizeof(*mib), &info, &size, NULL, 0);
    assert(junk == 0);
    
    // We're being debugged if the P_TRACED flag is set.
    
    return ( (info.kp_proc.p_flag & P_TRACED) != 0 );
#else
    return NO;
#endif
}


