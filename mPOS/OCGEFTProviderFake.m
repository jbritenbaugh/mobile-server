//
//  OCGEFTProviderFake.m
//  mPOS
//
//  Created by Antonio Strijdom on 18/04/2013.
//  Copyright (c) 2013 Clarity. All rights reserved.
//

#import "OCGEFTProviderFake.h"

@implementation OCGEFTProviderFake

+ (NSString *) providerURL
{
    return @"com-omnicogroup-paywareVX-fake";
}

@end
