//
//  SalesMenuItemCell.h
//  MenuTest
//
//  Created by Antonio Strijdom on 14/03/2013.
//  Copyright (c) 2013 Antonio Strijdom. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OCGMenuCell.h"
#import "CRSGlossyButton.h"

@interface SalesMenuItemCell : OCGMenuCell

@end
