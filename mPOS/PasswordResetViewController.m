//
//  PasswordResetViewController.m
//  mPOS
//
//  Created by John Scott on 02/09/2013.
//  Copyright (c) 2013 Clarity. All rights reserved.
//

#import "PasswordResetViewController.h"
#import "NSArray+OCGIndexPath.h"
#import "OCGTableViewCell.h"
#import "OCGTextField.h"
#import "CRSSkinning.h"
#import "BasketController.h"
#import "UIImage+OCGExtensions.h"
#import "UITableView+OCGExtensions.h"

#define kOperatorID @"kOperatorID"
#define kNewOperatorPasswordA @"kNewOperatorPasswordA"
#define kNewOperatorPasswordB @"kNewOperatorPasswordB"

@interface PasswordResetViewController () <UITextFieldDelegate>

@property (strong, nonatomic) NSArray *rowIdentifiers;
@property (strong, nonatomic) NSMutableDictionary *values;

@end

@implementation PasswordResetViewController

- (id)initWithOperatorID:(NSString*)operatorID
{
    self = [super initWithStyle:UITableViewStyleGrouped];
    if (self) {
        self.values = [NSMutableDictionary dictionary];
        self.values[kOperatorID] = operatorID;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    self.navigationItem.rightBarButtonItem.tag = kPrimaryButtonSkinningTag;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated
{
    self.title = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁫󠁏󠀱󠀯󠁲󠁕󠁇󠁁󠁥󠁘󠀵󠀲󠀲󠁙󠁡󠁇󠁌󠁇󠁖󠀵󠁭󠁅󠀶󠁢󠁮󠁬󠁣󠁿*/ @"Password", nil);
    
    if (YES || self == self.navigationController.viewControllers[0])
    {
        UIBarButtonItem *dismissButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel
                                                                                       target:self
                                                                                       action:@selector(dismissButtonTapped)];
        self.navigationItem.leftBarButtonItem = dismissButton;
    }
    else
    {
        self.navigationItem.leftBarButtonItem = nil;
    }
    
    UIBarButtonItem *setButton = [[UIBarButtonItem alloc] initWithTitle: NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁆󠀸󠁸󠁆󠁈󠁨󠁨󠁲󠁖󠀸󠁧󠁮󠁘󠁌󠁫󠀫󠁉󠁸󠀹󠀳󠁍󠀶󠁺󠁋󠁩󠁍󠁉󠁿*/ @"Update", nil)
                                                                  style: UIBarButtonItemStylePlain
                                                                 target: self
                                                                 action: @selector(updateButtonTapped)];
    self.navigationItem.rightBarButtonItem = setButton;
    
    NSMutableArray *identifiers = [NSMutableArray array];
    [identifiers addObject:kOperatorID];
    //[identifiers addObject:kOperatorPassword];
    [identifiers addObject:kNewOperatorPasswordA];
    [identifiers addObject:kNewOperatorPasswordB];
    [identifiers addObjectsFromArray:BasketController.sharedInstance.passwordRules];

    /*
     A special rule to check the two password feilds against each other. The ruleExpression is deliberately nil.
     */
    PasswordRule *passwordRule = [[PasswordRule alloc] init];
    passwordRule.ruleDescription = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁤󠁸󠁊󠀲󠁹󠁱󠁍󠀷󠁹󠁂󠁹󠁒󠁦󠁸󠁈󠁱󠀫󠁺󠁷󠁄󠁑󠀰󠀳󠁳󠁔󠁋󠁑󠁿*/ @"New passwords match each other", nil);
    [identifiers addObject:passwordRule];
    
    self.rowIdentifiers = @[identifiers];
    
    
    [self updatePasswordRules];

    [super viewWillAppear:animated];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    NSArray *indexPaths = [self.rowIdentifiers indexPathsForObject:kNewOperatorPasswordA];
    OCGTableViewCell *cell = (OCGTableViewCell *) [self.tableView cellForRowAtIndexPath:indexPaths[0]];
    [cell.detailTextField becomeFirstResponder];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [self.rowIdentifiers count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.rowIdentifiers[section] count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    OCGTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        id rowIdentifier = [self.rowIdentifiers objectForIndexPath:indexPath];
        UITableViewCellStyle style;
        if ([rowIdentifier isKindOfClass:[PasswordRule class]])
        {
            style = UITableViewCellStyleDefault;
        }
        else
        {
            style = UITableViewCellStyleValue2;
        }

        cell = [[OCGTableViewCell alloc] initWithStyle:style reuseIdentifier:CellIdentifier];
        cell.detailTextField.delegate = self;
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(OCGTableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    id rowIdentifier = [self.rowIdentifiers objectForIndexPath:indexPath];
    if ([rowIdentifier isKindOfClass:[PasswordRule class]])
    {
        cell.textLabelWidth = 200;
        
    }
    else
    {
        cell.textLabelWidth = 140;
    }

    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    cell.detailTextField.tag = kEditableCellValueSkinningTag;
    cell.tag = kEditableTableCellSkinningTag;
    cell.textLabel.tag = kEditableCellKeySkinningTag;

    if ([rowIdentifier isEqual:kOperatorID] || [rowIdentifier isKindOfClass:[PasswordRule class]])
    {
        cell.detailTextField.enabled = NO;
        cell.detailTextField.secureTextEntry = NO;
        cell.detailTextField.clearButtonMode = UITextFieldViewModeNever;
    }
    else
    {
        cell.detailTextField.enabled = YES;
        cell.detailTextField.secureTextEntry = YES;
        /*
         All the things we need to set if we're not using secure entry but was to enter wierd stuff (eg a password)
        cell.detailTextField.autocapitalizationType = UITextAutocapitalizationTypeNone;
        cell.detailTextField.autocorrectionType = UITextAutocorrectionTypeNo;
        cell.detailTextField.spellCheckingType = UITextSpellCheckingTypeNo;
        */
        cell.detailTextField.clearButtonMode = UITextFieldViewModeWhileEditing;
    }
    
//    cell.backgroundColor = [UIColor whiteColor];
    if ([rowIdentifier isKindOfClass:[PasswordRule class]])
    {
        PasswordRule *passwordRule = rowIdentifier;
        cell.textLabel.text = passwordRule.ruleDescription;
        cell.detailTextField.text = passwordRule.ruleExpression;
        
        UIImage *image = nil;
        if ([self string:self.values[kNewOperatorPasswordA] isValidForExpression:passwordRule.ruleExpression])
        {
            image = [UIImage OCGExtensions_imageNamed:@"Tick"
                                                 tint:[UIColor colorWithRed:0 green:0.5 blue:0 alpha:1]];
        }
        else
        {
            image = [UIImage OCGExtensions_imageNamed:@"Cross"
                                                 tint:[UIColor colorWithRed:0.7 green:0 blue:0 alpha:1]];
        }
        cell.accessoryView = [[UIImageView alloc] initWithImage:image];
        cell.textLabel.tag = kEditableCellValueSkinningTag;
    }
    else
    {
        if ([rowIdentifier isEqual:kOperatorID])
        {
            cell.textLabel.text = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁴󠁱󠁂󠁖󠀰󠁩󠀯󠁘󠁨󠀫󠁤󠁌󠁋󠁐󠁁󠁷󠁫󠁲󠁷󠁊󠀯󠁰󠁯󠁚󠁂󠀴󠁧󠁿*/ @"User", nil);
        }
        else if ([rowIdentifier isEqual:kNewOperatorPasswordA])
        {
            cell.textLabel.text = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠀸󠁏󠀵󠁯󠁳󠁲󠁔󠁢󠁥󠁥󠁋󠁺󠁏󠁸󠁲󠀶󠁫󠁍󠁨󠁑󠁘󠁊󠀹󠁇󠁬󠁡󠁧󠁿*/ @"New password", nil);
        }
        else if ([rowIdentifier isEqual:kNewOperatorPasswordB])
        {
            cell.textLabel.text = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠀫󠀲󠁇󠁮󠀰󠁱󠁎󠁇󠁘󠁭󠁰󠁆󠁬󠁁󠁆󠁌󠀰󠁶󠀯󠀱󠁡󠀴󠁣󠁙󠁆󠀷󠁳󠁿*/ @"New password (again)", nil);
        }
        cell.detailTextField.text = self.values[rowIdentifier];
    }
    [[CRSSkinning currentSkin] applyViewSkin: self];
}

-(BOOL)string:(NSString*)string isValidForExpression:(NSString*)expression
{
    /*
     If the expression is nil then we assume it's the special rule we added in viewDidLoad to
     check the password (kNewOperatorPasswordA) against the second one entered by the operator
     (kNewOperatorPasswordB)
     */
    if (expression != nil)
    {
        NSRange range = [string rangeOfString:expression options:NSRegularExpressionSearch];
        
        if (range.length > 0 && range.location != NSNotFound)
        {
            return YES;
        }
        else
        {
            return NO;
        }
    }
    else
    {
        return [string isEqual:self.values[kNewOperatorPasswordB]];
    }
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    OCGTableViewCell *cell = (OCGTableViewCell *) [self.tableView cellForRowAtIndexPath:indexPath];
    [cell.detailTextField becomeFirstResponder];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    id rowIdentifier = [self.rowIdentifiers objectForIndexPath:indexPath];
    if ([rowIdentifier isKindOfClass:[PasswordRule class]])
    {
        return 28.;
    }
    else
    {
        return 44.;
    }
}

#pragma mark - Actions

-(IBAction)dismissButtonTapped
{
    [self.delegate passwordResetViewController:self password:nil];
}

-(IBAction)updateButtonTapped
{
    [self.delegate passwordResetViewController:self password:self.values[kNewOperatorPasswordA]];
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(textFieldDidChange:) name:UITextFieldTextDidChangeNotification object:textField];
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UITextFieldTextDidChangeNotification object:textField];
}

- (BOOL)textFieldShouldClear:(UITextField *)textField
{
    NSIndexPath *indexPath = [self.tableView indexPathForCell:textField.OCGTableViewCell_tableViewCell];

    textField.text = nil;
    NSString *rowIdentifier = [self.rowIdentifiers objectForIndexPath:indexPath];
    [self.values removeObjectForKey:rowIdentifier];
    [self updatePasswordRules];
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    NSIndexPath *indexPath = [self.tableView indexPathForCell:textField.OCGTableViewCell_tableViewCell];
    NSIndexPath *nextIndexPath = indexPath;
    do
    {
        nextIndexPath = [self.tableView indexPathFromIndexPath:nextIndexPath offset:1 wrap:YES];
        
        [self.tableView scrollToRowAtIndexPath:nextIndexPath atScrollPosition:UITableViewScrollPositionMiddle animated:NO];
        OCGTableViewCell *nextCell = (OCGTableViewCell*) [self.tableView cellForRowAtIndexPath:nextIndexPath];
        
        if ([nextCell respondsToSelector:@selector(detailTextField)] && [nextCell.detailTextField becomeFirstResponder])
        {
            return NO;
        }
    } while (![indexPath isEqual:nextIndexPath]);
    
    return NO;
}

/*
 Why use notifications when we could just use textField:shouldChangeCharactersInRange:replacementString:?
 
 Here's what happened:
 1. Start editing the secture text field.
 2. Type "bbbb".
 3. Stop editing the text field.
 4. Start editing the secture text field again.
 5. Type "u".
 6. textField:shouldChangeCharactersInRange:replacementString: is called with @"u" and NSMakeRange(4,0).
 7. The text field is then wiped and @"u" placed in it (it's a secure text feild thing)
 8. We think the new string is @"bbbbu", but the real content is @"u".
 9. !!!
 
 I bet the notifications are sounding a lot better now aren't they.
 */

- (void)textFieldDidChange:(NSNotification*)notification
{
    UITextField *textField = notification.object;
    NSIndexPath *indexPath = [self.tableView indexPathForCell:textField.OCGTableViewCell_tableViewCell];
    NSString *rowIdentifier = [self.rowIdentifiers objectForIndexPath:indexPath];
    self.values[rowIdentifier] = textField.text;
    [self updatePasswordRules];
}

-(void)updatePasswordRules
{
    for (OCGTableViewCell *cell in [self.tableView visibleCells])
    {
        NSIndexPath *visibleIndexPath = [self.tableView indexPathForCell:cell];
        //NSLog(@"visibleIndexPath: %@", visibleIndexPath);
        if ([[self.rowIdentifiers objectForIndexPath:visibleIndexPath] isKindOfClass:[PasswordRule class]])
        {
            [self.tableView.delegate tableView:self.tableView willDisplayCell:cell forRowAtIndexPath:visibleIndexPath];
        }
    }
    BOOL isValideForAllRules = YES;
    
    for (PasswordRule *passwordRule in BasketController.sharedInstance.passwordRules)
    {
        if (![self string:self.values[kNewOperatorPasswordA] isValidForExpression:passwordRule.ruleExpression])
        {
            isValideForAllRules = NO;
        }
    }
    
    self.navigationItem.rightBarButtonItem.enabled = isValideForAllRules;
}

@end
