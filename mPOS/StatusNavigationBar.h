//
//  StatusNavigationBar.h
//  mPOS
//
//  Created by John Scott on 15/05/2014.
//  Copyright (c) 2014 Clarity. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface StatusNavigationBar : UINavigationBar

@end
