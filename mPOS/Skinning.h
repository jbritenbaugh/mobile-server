//
//  Skinning.h
//  mPOS
//
//  Created by Antonio Strijdom on 22/08/2013.
//  Copyright (c) 2013 Clarity. All rights reserved.
//

#ifndef mPOS_Skinning_h
#define mPOS_Skinning_h

typedef NS_ENUM(NSInteger, Skinning)
{
    kGlobalTag = -999,
    kMenuSkinningTag = 1,
    kBasketMenuButtonSkinningTag,
    kPrimaryButtonSkinningTag,
    kPrimaryButtonInvertedSkinningTag,
    kSecondaryButtonSkinningTag,
    kTableCellKeySkinningTag,
    kTableCellValueSkinningTag,
    kEditableCellKeySkinningTag,
    kEditableCellValueSkinningTag,
    kEditableTableSkinningTag,
    kEditableTableCellSkinningTag,
    kTableCellSkinningTag,
    kTableCellTextSkinningTag,
    kBasketItemCellSkinningTag,
    kBasketItemCellDetailSkinningTag,
    kBasketItemAltCellSkinningTag,
    kBasketItemAltCellDetailSkinningTag,
    kTableSkinningTag,
    kScanButtonSkinningTag,
    kSalesMenuSkinningTag,
    kKeyboardAccessoryTag,
    kKeyboardAccessoryButtonTag,
    kKeyboardAccessoryTextFieldTag,
    kSearchSkinningTag,
    kSearchBarSkinningTag,
    kNoSaleMenuItemSkinningTag,
    kStatusViewTag,
    kStatusViewTrainingTag,
    kStatusViewiPadTag,
};

#endif

NSDictionary* SkinningDictionary();
