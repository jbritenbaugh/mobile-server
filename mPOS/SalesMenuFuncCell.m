//
//  SalesMenuFuncCell.m
//  mPOS
//
//  Created by Antonio Strijdom on 09/01/2014.
//  Copyright (c) 2014 Clarity. All rights reserved.
//

#import "SalesMenuFuncCell.h"
#import "UIImage+CRSUniversal.h"
#import "POSFuncController.h"

@interface SalesMenuFuncCell () {
    UIColor *originalColor;
}
@property (nonatomic, strong) UIButton *button;
@property (nonatomic, strong) UILabel *buttonCaption;
@end

@implementation SalesMenuFuncCell

#pragma mark - Properties

- (void) setMenuItem:(SoftKey *)menuItem
{
    [super setMenuItem:menuItem];
    self.button.hidden = YES;
    self.buttonCaption.hidden = YES;
    if (self.menuItem) {
        if (([self.menuItem.keyType isEqualToString:@"POSFUNC"]) ||
            ([self.menuItem.keyType isEqualToString:@"MENU"])) {
            // setup a menu item cell
            self.buttonCaption.hidden = NO;
            self.button.hidden = NO;
            // get the cell info
            UIImage *scaledImage = nil;
            NSString *title = nil;
            [POSFuncController getKeyInfoWithKey:self.menuItem
                                           title:&title
                                           image:&scaledImage];
            self.buttonCaption.text = title;
            if (scaledImage) {
                [self.button setImage:scaledImage
                             forState:UIControlStateNormal];
                UIImage *tinted = [scaledImage imageTintedWithColor:[UIColor whiteColor]];
                [self.button setImage:tinted
                             forState:UIControlStateHighlighted];
            }
            // set the accessibility label
            if ([self.menuItem.keyType isEqualToString:@"MENU"]) {
                self.button.accessibilityLabel = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠀯󠁆󠁆󠁹󠀳󠁓󠁯󠁐󠁙󠀸󠁕󠁰󠀯󠀱󠁉󠁏󠁶󠁣󠁺󠁕󠁏󠁁󠁦󠁰󠁨󠀯󠁙󠁿*/ @"More options", nil);
            } else {
                self.accessibilityLabel = self.buttonCaption.text;
            }
        } else if ([self.menuItem.keyType isEqualToString:@"EMPTY"]) {
            // setup an empty cell
            
        }
    }
}

- (void) setHighlighted:(BOOL)highlighted
{
    [super setHighlighted:highlighted];
    self.button.highlighted = highlighted;
    if (highlighted) {
        if (originalColor == nil) {
            originalColor = self.buttonCaption.textColor;
        }
        self.buttonCaption.textColor = [UIColor whiteColor];
    } else {
        if (originalColor != nil) {
            self.buttonCaption.textColor = originalColor;
        }
    }
}

#pragma mark - Methods

- (id) initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // setup the button background
        self.button = [UIButton buttonWithType:UIButtonTypeCustom];
        self.button.translatesAutoresizingMaskIntoConstraints = NO;
        self.button.userInteractionEnabled = NO;
        self.button.tag = kBasketMenuButtonSkinningTag;
        [self.contentView addSubview:self.button];
        [self.button constrain:@"top = top"
                            to:self.contentView];
        [self.button constrain:@"left = left"
                            to:self.contentView];
        [self.button constrain:@"bottom = bottom"
                            to:self.contentView];
        [self.button constrain:@"right = right"
                            to:self.contentView];
        self.button.backgroundColor = [UIColor yellowColor];
        // setup the label
        self.buttonCaption = [[UILabel alloc] init];
        self.buttonCaption.translatesAutoresizingMaskIntoConstraints = NO;
        self.buttonCaption.userInteractionEnabled = NO;
        self.buttonCaption.tag = kBasketMenuButtonSkinningTag;
        self.buttonCaption.backgroundColor = [UIColor redColor];
        self.buttonCaption.textAlignment = NSTextAlignmentCenter;
        self.buttonCaption.shadowColor = [UIColor blackColor];
        self.buttonCaption.shadowOffset = CGSizeMake(1, 1);
        [self.contentView addSubview:self.buttonCaption];
        [self.buttonCaption constrain:@"left = left"
                                   to:self.contentView];
        [self.buttonCaption constrain:@"top = bottom - 9.0"
                                   to:self.contentView];
        [self.buttonCaption constrain:@"width = width"
                                   to:self.contentView];
        [self.buttonCaption constrain:@"height = 12.0"
                                   to:nil];
    }
    return self;
}

- (NSString *) reuseIdentifier
{
    return @"SalesMenuFuncCell";
}

- (void) prepareForReuse
{
    [super prepareForReuse];
    // default to hidden
    self.button.hidden = YES;
    self.buttonCaption.hidden = YES;
}

@end
