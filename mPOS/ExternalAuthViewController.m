//
//  ExternalAuthViewController.m
//  mPOS
//
//  Created by Antonio Strijdom on 19/09/2013.
//  Copyright (c) 2013 Clarity. All rights reserved.
//

#import "ExternalAuthViewController.h"
#import "TestModeController.h"
#import "BasketController.h"
#import "CRSSkinning.h"
#import "AppDelegate.h"
#import "RESTController.h"
#import "UIDevice+OCGIdentifier.h"
#import "ServerErrorHandler.h"
#import "TillSetupController.h"
#import "CRSLocationController.h"

@interface ExternalAuthViewController () <UIWebViewDelegate>
@property (nonatomic, strong) UIWebView *webView;
@property (nonatomic, assign) BOOL startedLoad;
@property (nonatomic, assign) BOOL finishedLoad;
@property (nonatomic, assign) BOOL reloading;
- (void) handleConfigurationChange:(NSNotification *)note;
- (void) loadAuthURLClearCookies:(BOOL)clear;
- (IBAction) presentSettingsView:(id)sender;
- (IBAction) refreshWebView:(id)sender;
- (NSDictionary *) dictionaryForQuery:(NSString *)query;
- (NSString *) getErrorDescriptionForExternalError:(NSString *)error;
- (void) loginOperatorWithURL:(NSURL *)url;
@end

@implementation ExternalAuthViewController

#pragma mark - Private

- (void) handleConfigurationChange:(NSNotification *)note
{
    if ([TestModeController serverMode] == ServerModeOnline) {
        [self loadAuthURLClearCookies:YES];
    }
}

- (void) loadAuthURLClearCookies:(BOOL)clear
{
    if (clear) {
        // reload
        [[NSURLCache sharedURLCache] removeAllCachedResponses];
        for (NSHTTPCookie *cookie in [[NSHTTPCookieStorage sharedHTTPCookieStorage] cookies]) {
            [[NSHTTPCookieStorage sharedHTTPCookieStorage] deleteCookie:cookie];
        }
    }
    // load the external auth URL
    NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:[BasketController sharedInstance].externalAuthURL];
    urlRequest.HTTPMethod = @"GET";
    [urlRequest setCachePolicy:NSURLRequestReloadIgnoringCacheData];
    DebugLog(@"Requesting external auth URL : %@", urlRequest.URL);
    [self.webView loadRequest:urlRequest];
}

- (NSDictionary *) dictionaryForQuery:(NSString *)query
{
    NSMutableDictionary *result = [NSMutableDictionary dictionary];
    
    // split into key/value pairs
    NSArray *queryKeyValues = [query componentsSeparatedByString:@"&"];
    
    for (NSString *keyValue in queryKeyValues) {
        // find the '=' seperator
        NSRange equalRange = [keyValue rangeOfString:@"="];
        if (equalRange.location != NSNotFound) {
            // split the pair
            NSString *key = [keyValue substringToIndex:equalRange.location];
            NSString *value = [keyValue substringFromIndex:equalRange.location + 1];
            // undo percent escapes
            value = [value stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
            // update the dictionary
            [result setValue:value
                      forKey:key];
        }
    }
    
    return result;
}

- (NSString *) getErrorDescriptionForExternalError:(NSString *)error
{
    NSString *result = error;
    
    if ([error isEqualToString:@"access_denied"]) {
        result = @"Access Denied";
    }
    
    return result;
}

- (void) loginOperatorWithURL:(NSURL *)url
{    
    Credentials *credentials = [[Credentials alloc] init];
    
    NSDictionary *queryDict = [self dictionaryForQuery:url.query];
    // check for errors
    NSString *error = queryDict[@"error"];
    if (error != nil) {
        error = [self getErrorDescriptionForExternalError:error];
        UIAlertView *alert =
        [[UIAlertView alloc] initWithTitle:nil
                                   message:NSLocalizedString(error, nil)
                                  delegate:nil
                         cancelButtonTitle:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁡󠁩󠁡󠁍󠁰󠀸󠁷󠁣󠁹󠁩󠁕󠀷󠁪󠁶󠀱󠀸󠁐󠁖󠀵󠁥󠀳󠁫󠁕󠁚󠁄󠁶󠁙󠁿*/ @"Dismiss", nil)
                         otherButtonTitles:nil];
        [alert show];
        [self loadAuthURLClearCookies:YES];
    } else {
        credentials.code = queryDict[@"code"];
        credentials.deviceId = [[UIDevice currentDevice] uniqueDeviceIdentifier];
        credentials.storeId = [CRSLocationController getLocationKey];
        credentials.tillNumber = [TillSetupController getTillNumber];
        // log on
        BasketController *bc = [BasketController sharedInstance];
        [bc authenticateWithCredentials:credentials
                signOffExistingOperator:NO
                              succeeded:^(BasketController *controller) {
                                  if (self.delegate != nil) {
                                      if ([self.delegate respondsToSelector:@selector(dismissModalLoginViewController:)]) {
                                          [self.delegate dismissModalLoginViewController:self];
                                      }
                                  } else {
                                      [self dismissViewControllerAnimated:YES
                                                               completion:nil];
                                  }
                              }
                                 failed:^(BasketController *controller, NSError *error, ServerError *serverError) {
                                          [[ServerErrorHandler sharedInstance] handleServerError:serverError
                                                                                 transportError:error
                                                                                    withContext:nil
                                                                                    dismissBlock:^{
                                                                                        self.reloading = YES;
                                                                                        [self loadAuthURLClearCookies:YES];}
                                                                                     repeatBlock:nil];
                                 }];
    }
}

#pragma mark - UIViewController

- (void) viewDidLoad
{
    [super viewDidLoad];
    // make sure that we can't navigate back to the main view
    [self.navigationController setNavigationBarHidden:NO
                                             animated:NO];
    // show the reload option
    self.navigationItem.rightBarButtonItem =
    [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemRefresh
                                                  target:self
                                                  action:@selector(refreshWebView:)];
	// show a web view
    self.webView = [[UIWebView alloc] initWithFrame:self.view.bounds];
    self.webView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    self.webView.scalesPageToFit = NO;
    self.webView.delegate = self;
    [self.view addSubview:self.webView];
    [self.view bringSubviewToFront:self.webView];
    // listen for config updates
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(handleConfigurationChange:)
                                                 name:kBasketControllerConfigurationDidChangeNotification
                                               object:nil];
}

- (void) viewWillAppear:(BOOL)animated
{
    // set the view's title
    self.navigationItem.title = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁦󠀯󠁢󠁘󠁈󠁡󠁰󠁥󠁧󠁯󠁥󠁓󠁨󠁁󠁯󠁧󠁧󠀯󠁣󠁧󠁍󠁵󠁑󠁈󠁓󠁥󠁫󠁿*/ @"Sign in", nil);

    // show the settings option
    self.navigationItem.leftBarButtonItem =
    [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁏󠁐󠀵󠁖󠁨󠁭󠁭󠁑󠁁󠁉󠁚󠁁󠁃󠁭󠁚󠁓󠁅󠀵󠁘󠀴󠀱󠁂󠀫󠁤󠀱󠁃󠁯󠁿*/ @"Settings", nil)
                                     style:UIBarButtonItemStylePlain
                                    target:self
                                    action:@selector(presentSettingsView:)];

    // skin
    [[CRSSkinning currentSkin] applyViewSkin:self];
    [super viewWillAppear:animated];
}

- (void) viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    // load the external auth URL
    NSURL *url = [BasketController sharedInstance].externalAuthURL;
    if (url) {
        [self loadAuthURLClearCookies:YES];
    }
}

- (void) viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - UIWebViewDelegate

- (BOOL) webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    BOOL result = YES;
    
    // get the original auth URL
    NSURL *authURL = [[BasketController sharedInstance] externalAuthURL];
    NSDictionary *authURLDict = [self dictionaryForQuery:authURL.query];
    // grab the redirect_URI
    NSString *redirectURI = authURLDict[@"redirect_uri"];
    // unescape it
    redirectURI = [redirectURI stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSURL *serverURL = [NSURL URLWithString:redirectURI];
    if ([request.URL.host isEqualToString:serverURL.host]) {
        DebugLog(@"Processing response: %@", request);
        self.finishedLoad = YES;
        self.startedLoad = NO;
        [self loginOperatorWithURL:request.URL];
        result = NO;
    }
    
    return result;
}

- (void) webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    if ((!self.finishedLoad) && (!self.reloading) && [error code] != NSURLErrorCancelled) {
        [[ServerErrorHandler sharedInstance] handleServerError:nil
                                                transportError:error
                                                   withContext:nil
                                                  dismissBlock:nil
                                                   repeatBlock:nil];
    }
    self.startedLoad = NO;
    self.reloading = NO;
}

- (void) webViewDidStartLoad:(UIWebView *)webView
{
    if (!self.startedLoad) {
        self.startedLoad = YES;
        self.finishedLoad = NO;
        double delayInSeconds = 60.0;
        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
        dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
            if (!self.finishedLoad) {
                self.reloading = NO;
                [self.webView stopLoading];
            }
            self.startedLoad = NO;
        });
    }
}

- (void) webViewDidFinishLoad:(UIWebView *)webView
{
    self.finishedLoad = YES;
    self.startedLoad = NO;
    self.reloading = NO;
}

#pragma mark - Actions

- (IBAction) presentSettingsView:(id)sender
{
    AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    [appDelegate presentSettingsViewAnimated:YES viewController:self];
}

- (IBAction) refreshWebView:(id)sender
{
    self.reloading = YES;
    [self loadAuthURLClearCookies:NO];
}

-(void)dealloc
{
    self.webView.delegate = nil;
}
@end
