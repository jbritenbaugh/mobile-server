//
//  BasketItemDetailViewController.m
//  mPOS
//
//  Created by John Scott on 27/01/2014.
//  Copyright (c) 2014 Clarity. All rights reserved.
//

#import "BasketItemDetailViewController.h"

#import "UICollectionView+OCGExtensions.h"
#import "FNKCollectionViewLayout.h"
#import "OCGContextCollectionViewDataSource.h"

#import "OCGCollectionViewCell.h"

#import "OCGTextField.h"
#import "NSArray+OCGIndexPath.h"
#import "CRSSkinning.h"
#import "BasketController.h"
#import "OCGSwitchTableViewCell.h"
#import "ContextViewController.h"
#import "POSFuncController.h"

@interface BasketItemDetailViewController () <OCGSwitchTableViewCellDelegate, OCGPrintOptionSwitchDelegate> {
    dispatch_group_t _updateGroup;
    OCGContextCollectionViewDataSource *_dataSource;
}
- (void) handleBasketChangeNotification:(NSNotification *)note;
- (BOOL) isRowSelectableAtIndexPath:(NSIndexPath *)indexPath;
- (SoftKey *) getPOSFuncSoftKeyForPOSFunc:(POSFunc)posFunc;
- (void) performPOSFuncIfAvailable:(POSFunc)posFunc;
- (void) reloadData;
@end

@implementation BasketItemDetailViewController

#pragma mark - Private

- (void) handleBasketChangeNotification:(NSNotification *)note
{
    // find the updated basket item
    for (MPOSBasketItem *item in BasketController.sharedInstance.basket.basketItems) {
        if ([item.basketItemId isEqualToString:self.basketItem.basketItemId]) {
            _basketItem = item;
            break;
        }
    }
    [self reloadData];
}

- (BOOL) isRowSelectableAtIndexPath:(NSIndexPath *)indexPath
{
    BOOL result = NO;
    
    OCGContextCollectionViewDataSource *item = [_dataSource dataSourceForIndexPath:indexPath];
    if ([item.softKey.keyType isEqualToString:@"DISPLAY_INFO"]) {
        if ([item.softKey.data isEqualToString:@"pricing.display"] ||
            [item.softKey.data isEqualToString:@"retailPrice.display"]) {
        result = ([self getPOSFuncSoftKeyForPOSFunc:POSFuncModifyPrice] != nil);
        }
    } else if ([item.softKey.keyType isEqualToString:@"POSFUNC"]) {
        result = [[POSFuncController sharedInstance] isMenuOptionActive:item.softKey];
    }
    
    return result;
}

- (SoftKey *) getPOSFuncSoftKeyForPOSFunc:(POSFunc)posFunc
{
    __block SoftKey *result = nil;
    
    // get the string identifier for the POSFunc
    NSString *posFuncId = NSStringForPOSFunc(posFunc);
    // first find the menu item for this pos func
    SoftKeyContext *itemContextLayout = [[ContextViewController sharedInstance] contextLayoutMenuForBasketItem:self.basketItem];
    [BasketController.sharedInstance enumerateSoftKeysForKeyMenu:itemContextLayout.rootMenu
                                                  softKeyContext:itemContextLayout
                                                      usingBlock:^(SoftKey *softKey, BOOL *stop)
    {
        if ([softKey.posFunc isEqualToString:posFuncId]) {
            result = softKey;
            *stop = YES;
        }
    }];
    
    return result;
}

- (void) performPOSFuncIfAvailable:(POSFunc)posFunc
{
    // if found, select the pos func softkey
    SoftKey *funcKey = [self getPOSFuncSoftKeyForPOSFunc:posFunc];
    if (funcKey != nil) {
        [[ContextViewController sharedInstance] didSelectContextMenuItem:funcKey];
    }
}

- (void) reloadData
{
    if (_updateGroup == nil) {
        _updateGroup = dispatch_group_create();
    }
    if (dispatch_group_wait(_updateGroup, DISPATCH_TIME_NOW) == 0) {
        dispatch_group_enter(_updateGroup);
        if (self.collectionView) {
            // clear the collection view
            _dataSource = [[OCGContextCollectionViewDataSource alloc] init];
            _dataSource.printOptionDelegate = self;
            [_dataSource registerCellReuseIdsWithCollectionView:self.collectionView];
            self.collectionView.dataSource = _dataSource;
            [self.collectionView reloadData];
            // reload
            if (self.basketItem) {
                SoftKeyContext *itemContextLayout = [[ContextViewController sharedInstance] contextLayoutMenuForBasketItem:self.basketItem];
                [_dataSource updataDataSourceWithContext:itemContextLayout
                                                withData:self.basketItem];
                // update collection view
                [self.collectionView reloadData];
                dispatch_group_leave(_updateGroup);
            } else {
                dispatch_group_leave(_updateGroup);
            }
        } else {
            dispatch_group_leave(_updateGroup);
        }
    }
}

#pragma mark - Properties

@synthesize basketItem = _basketItem;
- (void) setBasketItem:(MPOSBasketItem *)basketItem
{
    if (_basketItem != basketItem) {
        _basketItem = basketItem;
        [self reloadData];
    }
}

#pragma mark - init

- (id)init
{
    FNKCollectionViewLayout *layout = [[FNKCollectionViewLayout alloc] init];
    layout.pinLastSectionToBottom = NO;
    self = [super initWithCollectionViewLayout:layout];
    if (self) {
       
    }
    
    return self;
}

- (void) dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    _updateGroup = nil;
}

#pragma mark - UIViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // register cells
    [_dataSource registerCellReuseIdsWithCollectionView:self.collectionView];
    self.collectionView.delaysContentTouches = NO;
    
    // subscribe to discount change notifications
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(handleBasketChangeNotification:)
                                                 name:kBasketControllerDiscountChangeNotification
                                               object:nil];
    
    // subscribe to basket change notifications
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(handleBasketChangeNotification:)
                                                 name:kBasketControllerBasketItemChangeNotification
                                               object:nil];
    
    // subscribe to basket change notifications
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(handleBasketChangeNotification:)
                                                 name:kBasketControllerBasketChangeNotification
                                               object:nil];
    
    self.collectionView.backgroundColor = [UIColor colorWithRed:0.937 green:0.937 blue:0.957 alpha:1.000];
    
    // update collection view
    [self reloadData];
}

- (void)viewWillAppear:(BOOL)animated
{
    self.navigationItem.title = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁈󠁣󠁤󠁗󠁪󠁵󠀲󠁢󠁵󠀰󠁃󠁘󠁩󠁲󠁎󠁁󠁘󠁦󠁹󠁫󠁑󠁹󠁂󠁓󠁴󠁏󠁙󠁿*/ @"Item Details", nil);
    [super viewWillAppear:animated];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UICollectionViewDelegate

- (BOOL) collectionView:(UICollectionView *)collectionView shouldSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    BOOL result = [self isRowSelectableAtIndexPath:indexPath];
    
    return result;
}

- (BOOL) collectionView:(UICollectionView *)collectionView shouldHighlightItemAtIndexPath:(NSIndexPath *)indexPath
{
    BOOL result = [self collectionView:collectionView shouldSelectItemAtIndexPath:indexPath];
    
    return result;
}

- (void) collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if ([self isRowSelectableAtIndexPath:indexPath]) {
        OCGContextCollectionViewDataSource *item = [_dataSource dataSourceForIndexPath:indexPath];
        if ([item.softKey.keyType isEqualToString:@"DISPLAY_INFO"]) {
            NSString *basketItemKey = item.softKey.data;
            if ([basketItemKey isEqualToString:@"quantity"]) {
                [self performPOSFuncIfAvailable:POSFuncModifyQuantity];
            } else if ([basketItemKey isEqualToString:@"pricing.display"] ||
                       [basketItemKey isEqualToString:@"retailPrice.display"]) {
                [self performPOSFuncIfAvailable:POSFuncModifyPrice];
            }
        } else if ([item.softKey.keyType isEqualToString:@"POSFUNC"]) {
            if ([[POSFuncController sharedInstance] isMenuOptionActive:item.softKey]) {
                [[ContextViewController sharedInstance] didSelectContextMenuItem:item.softKey];
            }
        }
    }
}

#pragma mark - OCGPrintOptionSwitchDelegate

- (void) printReceiptSwitchToggled:(UISwitch *)switchDetail
{
    
}

- (void) printSummarySwitchToggled:(UISwitch *)switchDetail
{

}

- (void) emailReceiptSwitchToggled:(UISwitch *)switchDetail
{

}

- (void) giftReceiptSwitchToggled:(UISwitch *)switchDetail
{
    NSIndexPath *indexPath =
    [self.collectionView indexPathForCell:(UICollectionViewCell *)switchDetail.superview.superview];
    if (indexPath) {
        [self collectionView:self.collectionView
    didSelectItemAtIndexPath:indexPath];
    }
}

@end
