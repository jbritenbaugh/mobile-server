//
//  ContextMenuViewController.m
//  mPOS
//
//  Created by Antonio Strijdom on 14/01/2014.
//  Copyright (c) 2014 Clarity. All rights reserved.
//

#import "ContextMenuViewController.h"
#import "OCGMenuPageViewController.h"
#import "CRSSkinning.h"
#import "OCGSplitViewController.h"
#import "ContextMenuCell.h"
#import "AlternateContextMenuCell.h"
#import "POSFunc.h"
#import "POSFuncController.h"
#import "BasketController.h"

@interface ContextMenuViewController () <OCGMenuPageViewControllerDataSource, OCGMenuPageViewControllerDelegate> {
CGFloat _contextMenuHeight;
}
@property (nonatomic, strong) SoftKeyContext *menu;
@property (nonatomic, strong) OCGMenuPageViewController *menuVC;
@property (nonatomic, strong) UIView *disabledView;
@property (nonatomic, strong) SoftKey *firstKey;
- (NSArray *) sortedKeysForContext:(SoftKeyContext *)context;
- (BOOL) isSelectableMenuItem:(SoftKey *)menuItem;
@end

@implementation ContextMenuViewController

#pragma mark - Private

- (NSArray *) sortedKeysForContext:(SoftKeyContext *)context
{
    NSMutableArray *sortedKeys = [NSMutableArray array];
    
    // get the layouts key ids from the key string (space seperated)
    NSArray *keyIds = [context.rootMenu componentsSeparatedByString:@" "];
    // build the keys arrays
    for (NSString *keyId in keyIds) {
        if (![keyId isEqualToString:@"K0"]) {
            // find the key
            SoftKey *key = [BasketController.sharedInstance softKeyForKeyId:keyId softKeyContext:context];
            if (key)
            {
                [sortedKeys addObject:key];
            }
        }
    }
    
    return sortedKeys;
}

- (BOOL) isSelectableMenuItem:(SoftKey *)menuItem
{
    BOOL result = NO;
    
    if ([menuItem.context.contextType rangeOfString:@"ITEM"].location == NSNotFound) {
        if ([menuItem.keyType isEqualToString:@"POSFUNC"]) {
            if ((POSFuncForString(menuItem.posFunc) == POSFuncContextMenuItemBasket) ||
                (POSFuncForString(menuItem.posFunc) == POSFuncContextMenuItemMenu) ||
                (POSFuncForString(menuItem.posFunc) == POSFuncContextMenuItemSearch)) {
                result = YES;
            }
        } else if ([menuItem.keyType isEqualToString:@"MENU"]) {
            result = YES;
        }
    }
    
    return result;
}

#pragma mark - Properties

@synthesize menu = _menu;

@synthesize selectedItem = _selectedItem;
- (void) setSelectedItem:(SoftKey *)selectedItem
{
    // if _selectedItem is nil and we are already displaying a menu
    // then we have just switched menus
    BOOL switchedMenus = ((_selectedItem == nil) && (self.menuVC.menu != nil));
    // only remember selectable menu selections
    if ([self isSelectableMenuItem:selectedItem]) {
        _selectedItem = selectedItem;
    }
    // always call the select block
    if (self.menuItemSelectedBlock != nil) {
        self.menuItemSelectedBlock(selectedItem, switchedMenus);
    }
}

@synthesize selectedMenuColor = _selectedMenuColor;
- (void) setSelectedMenuColor:(UIColor *)selectedMenuColor
{
    if (selectedMenuColor != _selectedMenuColor) {
        _selectedMenuColor = selectedMenuColor;
        [self.menuVC.collectionView reloadData];
    }
}

@synthesize enabled = _enabled;
- (void) setEnabled:(BOOL)enabled
{
    if (enabled != _enabled) {
        _enabled = enabled;
        if (_enabled) {
            [self.view sendSubviewToBack:self.disabledView];
            self.disabledView.hidden = YES;
            self.menuVC.collectionView.userInteractionEnabled = YES;
        } else {
            self.menuVC.collectionView.userInteractionEnabled = NO;
            self.disabledView.hidden = NO;
            [self.view bringSubviewToFront:self.disabledView];
        }
    }
}

#pragma mark - UIViewController

- (void) viewDidLoad
{
    [super viewDidLoad];
    self.view.tag = kMenuSkinningTag;
    _contextMenuHeight = 50.0f;
    if (self.alternateCells) {
        _contextMenuHeight = 70.0f;
    }
    self.preferredContentSize = CGSizeMake(0, _contextMenuHeight);
    self.selectedMenuColor = [UIColor whiteColor];
    // build the cell dict
    NSDictionary *cellReuseIds = nil;
    if (self.alternateCells) {
        cellReuseIds = @{@"ContextMenuCell":[AlternateContextMenuCell class]};
    } else {
        cellReuseIds = @{@"ContextMenuCell":[ContextMenuCell class]};
    }
    // create the menu
    self.menuVC =
    [[OCGMenuPageViewController alloc] initWithMenu:self.menu
                                   withCellReuseIds:cellReuseIds];
    self.menuVC.menuDataSource = self;
    self.menuVC.menuDelegate = self;
    [self.menuVC willMoveToParentViewController:self];
    [self addChildViewController:self.menuVC];
    [self.view addSubview:self.menuVC.collectionView];
    [self.menuVC.collectionView constrain:@"top = top" to:self.view];
    [self.menuVC.collectionView constrain:@"left = left" to:self.view];
    [self.menuVC.collectionView constrain:@"right = right" to:self.view];
    [self.menuVC.collectionView constrain:@"bottom = bottom" to:self.view];
    [self.menuVC didMoveToParentViewController:self];
    // create the disabled view
    self.disabledView = [[UIView alloc] init];
    self.disabledView.translatesAutoresizingMaskIntoConstraints = NO;
    self.disabledView.userInteractionEnabled = NO;
    [self.view addSubview:self.disabledView];
    [self.disabledView constrain:@"top = top" to:self.view];
    [self.disabledView constrain:@"left = left" to:self.view];
    [self.disabledView constrain:@"right = right" to:self.view];
    [self.disabledView constrain:@"bottom = bottom" to:self.view];
    self.disabledView.opaque = NO;
    self.disabledView.alpha = 0.5;
    self.disabledView.backgroundColor = [UIColor blackColor];
    
    // default to enabled
    self.enabled = YES;
    
    [[CRSSkinning currentSkin] applyViewSkin:self];
}

#pragma mark - OCGMenuPageViewControllerDataSource

- (OCGMenuCell *) menuPageViewController:(OCGMenuPageViewController *)page
                         cellForMenuItem:(SoftKey *)menuItem
                             atIndexPath:(NSIndexPath *)indexPath
{
    OCGMenuCell *result = [page dequeueMenuItemCellWithReuseId:@"ContextMenuCell"
                                                  forIndexPath:indexPath];
    if (result) {
        // assign the menu item
        SoftKey *resultMenuItem = [menuItem copy];
        
        
        if ([resultMenuItem.keyType isEqual:@"DISPLAY_INFO"])
        {
            NSString *basketValue = [BasketController.sharedInstance valueForKeyPath:resultMenuItem.data];
            
            if (basketValue)
            {
                if (resultMenuItem.primitiveKeyDescription.length > 0) {
                    resultMenuItem.keyDescription = [resultMenuItem.primitiveKeyDescription stringByAppendingFormat:@" %@", basketValue];
                } else {
                    resultMenuItem.keyDescription = basketValue;
                }
                
                for (Language *language in resultMenuItem.keyDescriptionLanguages)
                {
                    if (language.value.length > 0) {
                        language.value = [language.value stringByAppendingFormat:@" %@", basketValue];
                    } else {
                        language.value = basketValue;
                    }
                }
            }
            
            
            NSString *keyDescription = resultMenuItem.keyDescription;
            

            
            resultMenuItem.keyDescriptionLanguages = nil;
            resultMenuItem.keyDescription = keyDescription;
        }
        result.foregroundColor = self.selectedMenuColor;
        result.menuItem = resultMenuItem;
        result.selected = [self.selectedItem isEqual:resultMenuItem];
        
        // skin it
        [[CRSSkinning currentSkin] applyViewSkin:result.contentView
                                 withTagOverride:result.tag];
    }
    
    return result;
}

#pragma mark - OCGMenuPageViewControllerDelegate

- (void) menuPageViewController:(OCGMenuPageViewController *)page
                  didSelectItem:(SoftKey *)menuItem
{
    if ([[POSFuncController sharedInstance] isMenuOptionActive:menuItem]) {
        self.selectedItem = menuItem;
        [page.collectionView reloadData];
    }
}

- (CGPoint) cellPaddingForMenuPageViewController:(OCGMenuPageViewController *)page
{
    if (self.alternateCells) {
        return CGPointMake(0.0, 5.0f);
    } else {
        return CGPointMake(0.0f, 0.0f);
    }
}

- (CGFloat) cellHeightForMenuPageViewController:(OCGMenuPageViewController *)page
{
    return _contextMenuHeight;
}


- (CGFloat) cellWidthForMenuItem:(SoftKey *)menuItem
                          onPage:(OCGMenuPageViewController *)page
{
    CGFloat width = page.collectionView.frame.size.width / (CGFloat)page.numberOfColumns;
    
    if ([menuItem isKindOfClass:[SoftKey class]]) {
        if ([menuItem.keyType isEqual:@"DISPLAY_INFO"])
        {
            width = page.collectionView.frame.size.width;
        }
    }

    // get the padding value from the delegate
    CGPoint padding = CGPointZero;
    if ((page.menuDelegate != nil) &&
        ([page.menuDelegate respondsToSelector:@selector(cellPaddingForMenuPageViewController:)])) {
        padding = [page.menuDelegate cellPaddingForMenuPageViewController:page];
    }
    
    CGFloat cellWidth = width - (padding.x * 2.0f);
    return floor(cellWidth);
}

#pragma mark - Methods

- (void) reloadMenu:(BOOL)recalculate
{
    [self.menuVC reloadMenu:recalculate];
}

- (CGSize) keySizeForCurrentPage
{
    CGSize result = CGSizeZero;
    
    if (self.menuVC != nil) {
        result = self.menuVC.calculatedKeySize;
    }
    
    return result;
}

- (void) selectFirstTab
{
    [self menuPageViewController:self.menuVC
                   didSelectItem:self.firstKey];
}

- (void)  setMenu:(SoftKeyContext *)menu
andSelectFirstTab:(BOOL)selectFirstTab
{
    _menu = menu;
    if (_menu != nil) {
        // sort the new list by key position
        NSArray *keys = [self sortedKeysForContext:self.menu];
        // select the first selectable item
        self.firstKey = nil;
        for (NSUInteger index = 0; index < keys.count; index++) {
            SoftKey *key = keys[index];
            if ([self isSelectableMenuItem:key]) {
                if (self.firstKey == nil) {
                    // make a note of the first selectable key
                    self.firstKey = key;
                    break;
                }
            }
        }
        if (self.firstKey && selectFirstTab) {
            DebugLog(@"Selecting key: %@", self.firstKey);
            // clear out current selection (this indicates to the setter that we are
            // switching menus that that's what triggered the selected item change
            _selectedItem = nil;
            self.selectedItem = self.firstKey;
        } else {
            // try and select the existing selected tab
            if (self.selectedItem) {
                BOOL found = NO;
                for (NSUInteger index = 0; index < keys.count; index++) {
                    SoftKey *key = keys[index];
                    if ([self isSelectableMenuItem:key]) {
                        if ([self.selectedItem.posFunc isEqualToString:key.posFunc]) {
                            // make a note of the first selectable key
                            _selectedItem = nil;
                            self.selectedItem = key;
                            found = YES;
                            break;
                        }
                    }
                }
                // if the selected item isn't part of the new menu
                if (!found) {
                    // just select the first tab
                    _selectedItem = nil;
                    self.selectedItem = self.firstKey;
                }
            } else {
                // if there is no current selection
                // just select the first tab
                _selectedItem = nil;
                self.selectedItem = self.firstKey;
            }
        }
    }
    if (self.menuVC != nil) {
        self.menuVC.menu = self.menu;
        if (self.menu) {
            // resize the menu
            self.preferredContentSize = CGSizeMake(0, _contextMenuHeight * (CGFloat)self.menuVC.numberOfRows);
        } else {
            // hide the menu
            self.preferredContentSize = CGSizeMake(0, 0);
        }
    }
}

@end
