//
//  OperatorOverrideViewController.m
//  mPOS
//
//  Created by Antonio Strijdom on 18/03/2015.
//  Copyright (c) 2015 Clarity. All rights reserved.
//

#import "OperatorOverrideViewController.h"
#import "FNKCollectionViewLayout.h"
#import "OCGCollectionViewCell.h"
#import "CRSSkinning.h"
#import "NSArray+OCGIndexPath.h"
#import "OverrideErrorMessage+MPOSExtensions.h"
#import "FNKInputView.h"
#import "BasketController.h"

#define kTableEntryMessage      @"kTableEntryMessage"
#define kTableEntryUserId       @"kTableEntryUserId"
#define kTableEntryUserPassword @"kTableEntryUserPassword"

@interface OperatorOverrideViewController () <UITextFieldDelegate>
@property (nonatomic, readonly) OverrideErrorMessage *override;
@property (nonatomic, strong) NSArray *layout;
@property (nonatomic, assign) CGFloat messageHeight;
- (void)resendInvocationWithConfirm;
- (IBAction)cancelTapped:(id)sender;
- (IBAction)doneTapped:(id)sender;
@end

@implementation OperatorOverrideViewController

#pragma mark - Private

- (void)resendInvocationWithConfirm
{
    // get the original invocation
    RESTInvocation *originalInvocation = self.serverError.invocation;
    if (originalInvocation) {
        // add override information
        NSString *userId = nil;
        NSString *userPassword = nil;
        switch (self.override.parsedErrorLevel) {
            case OverrideErrorMessageLevelOperatorConfirm:
                userId = BasketController.sharedInstance.credentials.username;
                break;
            case OverrideErrorMessageLevelManagerIdRequired:
            case OverrideErrorMessageLevelManagerPasswordRequired: {
                OCGCollectionViewCell *userIdFieldCell = nil;
                NSArray *indexPaths = [self.layout indexPathsForObject:kTableEntryUserId];
                if (indexPaths.count > 0) {
                    userIdFieldCell = (OCGCollectionViewCell *)[self.collectionView cellForItemAtIndexPath:indexPaths[0]];
                }
                OCGCollectionViewCell *passwordFieldCell = nil;
                indexPaths = [self.layout indexPathsForObject:kTableEntryUserPassword];
                if (indexPaths.count > 0) {
                    passwordFieldCell = (OCGCollectionViewCell *)[self.collectionView cellForItemAtIndexPath:indexPaths[0]];
                }
                userId = userIdFieldCell.detailTextField.text;
                userPassword = passwordFieldCell.detailTextField.text;
                break;
            }
            default:
                break;
        }
        [originalInvocation addOverrideForErrorNumber:self.override.errorCode
                                      forBasketItemId:(self.override.basketItemId == nil) ? @"0" : self.override.basketItemId
                                       withApproverId:userId
                                  andApproverPassword:userPassword];
        // invoke it again
        ocg_async_background_overlay(^{
            [[RESTController sharedInstance] invokeSynchronous:originalInvocation
                                                        server:nil
                                                          etag:nil
                                                      complete:originalInvocation.complete];
        });
    }
}

- (IBAction)cancelTapped:(id)sender
{
    // just dismiss the view controller, we are done
    [self dismissViewControllerAnimated:YES
                             completion:nil];
}

- (IBAction)doneTapped:(id)sender
{
    // dismiss view controller
    [self dismissViewControllerAnimated:YES
                             completion:^{
                                 [self resendInvocationWithConfirm];
                             }];
}

#pragma mark - Properties

@synthesize serverError = _serverError;
- (void) setServerError:(ServerError *)serverError
{
    _serverError = serverError;
    _override = nil;
}

@synthesize override = _override;
- (OverrideErrorMessage *)override
{
    OverrideErrorMessage *result = _override;
    if (_override == nil) {
        for (ServerErrorMessage *message in self.serverError.messages) {
            if ([message isKindOfClass:[OverrideErrorMessage class]]) {
                OverrideErrorMessage *override = (OverrideErrorMessage *)message;
                if ((override.parsedErrorLevel != OverrideErrorMessageLevelUnknown) &&
                    (override.parsedErrorLevel != OverrideErrorMessageLevelAbort)) {
                    result = override;
                }
                break;
            }
        }
    }
    _override = result;
    
    return result;
}

#pragma mark - Init

- (instancetype)init
{
    FNKCollectionViewLayout *layout = [[FNKCollectionViewLayout alloc] init];
    self = [super initWithCollectionViewLayout:layout];
    if (self) {
        
    }
    return self;
}

#pragma mark - UIViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.title = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁴󠁮󠀫󠀲󠁬󠁇󠁓󠁒󠁁󠁤󠀵󠁑󠁇󠀸󠁔󠁇󠁐󠁖󠀱󠁔󠁊󠁁󠁨󠁲󠁨󠁭󠁍󠁿*/ @"Override", nil);

    self.collectionView.tag = kEditableTableSkinningTag;
    self.collectionView.backgroundColor = [UIColor colorWithRed:0.937
                                                          green:0.937
                                                           blue:0.957
                                                          alpha:1.000];
//    self.collectionView.backgroundColor = [UIColor whiteColor];
    self.collectionView.delaysContentTouches = NO;
    [OCGCollectionViewCell registerForDetailClass:[UITextField class]
                                          options:OCGCollectionViewCellDetailOptionUseTextLabelBaseline
                                   collectionView:self.collectionView];
}

- (void)viewWillAppear:(BOOL)animated
{
    [[CRSSkinning currentSkin] applyViewSkin:self];
    // calcuate the message height
    
    CGRect messageBoundingBox =
    [self.override.errorMessage boundingRectWithSize:CGSizeMake(self.collectionView.frame.size.width - 13, MAXFLOAT)
                                             options:NSStringDrawingUsesLineFragmentOrigin
                                          attributes:@{ NSFontAttributeName : [UIFont preferredFontForTextStyle:UIFontTextStyleBody] }
                                             context:nil];
    
    self.messageHeight = MAX(44.0f, CGRectGetHeight(messageBoundingBox) + 23);

    // add the done button
    self.navigationItem.rightBarButtonItem =
    [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁃󠀲󠁕󠁲󠁤󠀲󠁯󠁮󠁊󠀸󠁘󠁓󠁌󠁕󠁆󠁨󠁲󠁤󠁅󠁣󠁃󠁯󠁙󠁵󠁂󠀹󠁁󠁿*/ @"Done", nil)
                                     style:UIBarButtonItemStyleDone
                                    target:self
                                    action:@selector(doneTapped:)];
    // setup the table
    switch (self.override.parsedErrorLevel) {
        case OverrideErrorMessageLevelManagerIdRequired: {
            self.layout = @[ @[kTableEntryMessage], @[kTableEntryUserId] ];
            self.navigationItem.leftBarButtonItem =
            [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁩󠁢󠁺󠁬󠁏󠁶󠁣󠁧󠁫󠁍󠁋󠁧󠁫󠀱󠁶󠁐󠁐󠀹󠁘󠀴󠁘󠁡󠁕󠁭󠁹󠁷󠀰󠁿*/ @"Cancel", nil)
                                             style:UIBarButtonItemStylePlain
                                            target:self
                                            action:@selector(cancelTapped:)];
            break;
        }
        case OverrideErrorMessageLevelManagerPasswordRequired: {
            self.layout = @[ @[kTableEntryMessage], @[kTableEntryUserId, kTableEntryUserPassword] ];
            self.navigationItem.leftBarButtonItem =
            [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁩󠁢󠁺󠁬󠁏󠁶󠁣󠁧󠁫󠁍󠁋󠁧󠁫󠀱󠁶󠁐󠁐󠀹󠁘󠀴󠁘󠁡󠁕󠁭󠁹󠁷󠀰󠁿*/ @"Cancel", nil)
                                             style:UIBarButtonItemStylePlain
                                            target:self
                                            action:@selector(cancelTapped:)];
            break;
        }
        case OverrideErrorMessageLevelOperatorConfirm: {
            self.layout = @[@[kTableEntryMessage]];
            self.navigationItem.leftBarButtonItem =
            [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁩󠁢󠁺󠁬󠁏󠁶󠁣󠁧󠁫󠁍󠁋󠁧󠁫󠀱󠁶󠁐󠁐󠀹󠁘󠀴󠁘󠁡󠁕󠁭󠁹󠁷󠀰󠁿*/ @"Cancel", nil)
                                             style:UIBarButtonItemStylePlain
                                            target:self
                                            action:@selector(cancelTapped:)];
            break;
        }
        default:
            self.layout = @[@[kTableEntryMessage]];
            break;
    }
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated
{
    // make sure that the user id becomes first responder
    dispatch_async(dispatch_get_main_queue(), ^(void) {

        if (self.layout) {
            NSArray *indexPaths = [self.layout indexPathsForObject:kTableEntryUserId];
            if (indexPaths.count > 0) {
                OCGCollectionViewCell *userIDFieldCell =
                (OCGCollectionViewCell *)[self.collectionView cellForItemAtIndexPath:indexPaths[0]];
                [userIDFieldCell becomeFirstResponder];
            }
        }
    });
    
    [super viewDidAppear:animated];
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return self.layout.count;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView
     numberOfItemsInSection:(NSInteger)section
{
    return [self.layout[section] count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView
                  cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell *result = nil;
    
    NSString *entryIdentifier = [self.layout objectForIndexPath:indexPath];
    if ([entryIdentifier isEqualToString:kTableEntryMessage]) {
        NSString *reuseId = [OCGCollectionViewCell registerForDetailClass:[UILabel class]
                                                                  options:OCGCollectionViewCellDetailOptionUseTextLabelBaseline
                                                           collectionView:collectionView];
        
        OCGCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:reuseId
                                                                                forIndexPath:indexPath];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.textLabel.tag = kEditableCellValueSkinningTag;
        cell.tag = kEditableTableCellSkinningTag;
        cell.backgroundColor = [UIColor whiteColor];
        cell.textLabel.numberOfLines = 0;
        cell.textLabel.text = self.override.errorMessage;
        [[CRSSkinning currentSkin] applyViewSkin:cell
                                 withTagOverride:cell.tag];
        cell.textLabel.font = [UIFont preferredFontForTextStyle:UIFontTextStyleBody];
        result = cell;
    } else {
        NSString *reuseId = [OCGCollectionViewCell registerForDetailClass:[UITextField class]
                                                                  options:OCGCollectionViewCellDetailOptionUseTextLabelBaseline
                                                           collectionView:collectionView];
        
        OCGCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:reuseId
                                                                                forIndexPath:indexPath];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.textLabel.tag = kEditableCellKeySkinningTag;
        cell.detailView.tag = kEditableCellValueSkinningTag;
        cell.tag = kEditableTableCellSkinningTag;
        cell.detailRequirement = OCGCollectionViewCellDetailRequirementRequired;
        cell.backgroundColor = [UIColor whiteColor];
        UITextField *textField = cell.detailTextField;
        textField.text = nil;
        textField.clearButtonMode = UITextFieldViewModeAlways;
        textField.autocapitalizationType = UITextAutocapitalizationTypeNone;
        textField.autocorrectionType = UITextAutocorrectionTypeNo;
        textField.delegate = self;
        textField.FNKInputView_keyboardTypes = @[@(UIKeyboardTypeASCIICapable), @(UIKeyboardTypeNumberPad)];
        // add the toolbar
        textField.inputAccessoryView = [[FNKInputView alloc] initWithFrame:CGRectMake(0, 0, 200, 44)
                                                            inputViewStyle:UIInputViewStyleKeyboard];
        
        if ([entryIdentifier isEqualToString:kTableEntryUserId]) {
            // initialize the user id field
            cell.textLabel.text = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁈󠁲󠁸󠁏󠁙󠁄󠀲󠁳󠁩󠁵󠁉󠁅󠁵󠁍󠁈󠀫󠁕󠀹󠁫󠁢󠁸󠁐󠁵󠁋󠀸󠁇󠁅󠁿*/ @"Manager ID", nil);
            textField.secureTextEntry = NO;
        } else if ([entryIdentifier isEqualToString:kTableEntryUserPassword]) {
            // initialize the password field
            UITextField *textField = cell.detailTextField;
            cell.textLabel.text = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁫󠁏󠀱󠀯󠁲󠁕󠁇󠁁󠁥󠁘󠀵󠀲󠀲󠁙󠁡󠁇󠁌󠁇󠁖󠀵󠁭󠁅󠀶󠁢󠁮󠁬󠁣󠁿*/ @"Password", nil);
            textField.secureTextEntry = YES;
        } else {
            NSAssert(NO, @"Can't find a table entry for %@", indexPath);
        }
        
        [[CRSSkinning currentSkin] applyViewSkin:cell
                                 withTagOverride:cell.tag];
        result = cell;
    }
    
    return result;
}

#pragma mark - UICollectionViewDelegate

- (void)  collectionView:(UICollectionView *)collectionView
didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *entryIdentifier = [self.layout objectForIndexPath:indexPath];
    
    if ([entryIdentifier isEqualToString:kTableEntryUserId]) {
        OCGCollectionViewCell *userNameFieldCell = (OCGCollectionViewCell *)[collectionView cellForItemAtIndexPath:indexPath];
        [userNameFieldCell.detailTextField becomeFirstResponder];
    } else if ([entryIdentifier isEqualToString:kTableEntryUserPassword]) {
        OCGCollectionViewCell *passwordFieldCell = (OCGCollectionViewCell *)[collectionView cellForItemAtIndexPath:indexPath];
        [passwordFieldCell.detailTextField becomeFirstResponder];
    }
}

#pragma mark - FNKCollectionViewDataSourceDelegate

- (CGFloat)collectionView:(UICollectionView *)collectionView
 heightForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat result = 44.0f;
    
    NSString *entryIdentifier = [self.layout objectForIndexPath:indexPath];
    if ([entryIdentifier isEqualToString:kTableEntryMessage]) {
        result = self.messageHeight;
    }
    
    return result;
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    // TFS70058 - default switching keyboard to NO, otherwise CAPS doesn't stay on in iOS 7
    textField.FNKInputView_switchingKeyboardType = NO;
    return YES;
}

- (BOOL)textFieldShouldClear:(UITextField *)textField
{
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    OCGCollectionViewCell *userIdFieldCell = nil;
    NSArray *indexPaths = [self.layout indexPathsForObject:kTableEntryUserId];
    if (indexPaths.count > 0) {
        userIdFieldCell = (OCGCollectionViewCell *)[self.collectionView cellForItemAtIndexPath:indexPaths[0]];
    }
    OCGCollectionViewCell *passwordFieldCell = nil;
    indexPaths = [self.layout indexPathsForObject:kTableEntryUserPassword];
    if (indexPaths.count > 0) {
        passwordFieldCell = (OCGCollectionViewCell *)[self.collectionView cellForItemAtIndexPath:indexPaths[0]];
    }
    
    if (textField == userIdFieldCell.detailTextField) {
        [passwordFieldCell.detailTextField becomeFirstResponder];
    } else if (textField == passwordFieldCell.detailView) {
        [self doneTapped:nil];
    }
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    textField.FNKInputView_switchingKeyboardType = NO;
}

@end
