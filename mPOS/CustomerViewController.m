//
//  CustomerViewController.m
//  mPOS
//
//  Created by John Scott on 14/05/2013.
//  Copyright (c) 2013 Clarity. All rights reserved.
//

#import "CustomerViewController.h"

#import "NSArray+OCGIndexPath.h"
#import "OCGTableViewCell.h"
#import "OCGTextField.h"
#import "OCGSelectViewController.h"
#import "CRSSkinning.h"
#import "CustomerController.h"
#import "ServerErrorHandler.h"
#import "OCGProgressOverlay.h"
#import "CRSGlossyButton.h"
#import "BasketController.h"
#import "UITableView+OCGExtensions.h"

#define kCustomerFirstName                          @"firstName"
#define kCustomerLastName                           @"lastName"
#define kCustomerSalutation                         @"salutation"
#define kCustomerGender                             @"gender"
#define kCustomerAddressLine1                       @"address.line1"
#define kCustomerAddressLine2                       @"address.line2"
#define kCustomerAddressCity                        @"address.city"
#define kCustomerAddressTerritory                   @"address.territory"
#define kCustomerAddressPostcode                    @"address.postcode"
#define kCustomerAddressCountry                     @"address.country"
#define kCustomerPhone1                             @"phone1"
#define kCustomerEmail                              @"email"
#define kCustomerCustomerId                         @"customerId"
#define kCustomerNote                               @"CustomerNote"
#define kCustomerNoteAdd                            @"CustomerNoteAdd"
#define kCustomerCard                               @"CustomerCard"
#define kCustomerDefault                            @"CustomerDefault"
#define kCustomerAssign                             @"CustomerAssign"

@interface CustomerViewController () <UITextFieldDelegate>

@property (strong, nonatomic) NSMutableArray* layout;

@property (strong, nonatomic) NSMutableArray *notes;

@property (strong, nonatomic) NSMutableDictionary *details;

@property (assign) CGFloat textLabelWidth;

- (void) customerAssignedNotification:(NSNotification *)note;

@end

@implementation CustomerViewController

#pragma mark - Private

- (void) customerAssignedNotification:(NSNotification *)note
{
    if (self.delegate != nil) {
        if ([self.delegate respondsToSelector:@selector(customerAssignedController:adding:)]) {
            [self.delegate customerAssignedController:self
                                               adding:!self.presentedFromSearch];
        }
    }
//    } else {
//        // dismiss self
//        [self dismissViewControllerAnimated:YES
//                                 completion:nil];
//    }
}

#pragma mark - UIViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:UITableViewStyleGrouped];
    if (self) {
        self.presentedFromSearch = NO;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.tableView.tag = kEditableTableSkinningTag;
    self.tableView.allowsSelectionDuringEditing = YES;
    [[CRSSkinning currentSkin] applyViewSkin: self];
}

-(void)setCustomer:(MPOSCustomer *)customer
{
    // if we are switching to a new customer to display
    if (customer != nil) {
        if ((_customer == nil) || (![_customer.customerId isEqualToString:customer.customerId])) {
//            if (!(customer.notesRefreshRequiredValue || customer.cardsRefreshRequiredValue)) {
//                Disabled for Burberry M2
//                // reload notes and cards
//                [[CustomerController sharedInstance] getCustomerDetails:customer
//                                                                  cards:YES
//                                                                  notes:YES];
//            }
        }
    }
    _customer = customer;
    [self resetLocalCustomerDetails];
}

-(void)resetLocalCustomerDetails
{
    self.notes = [NSMutableArray arrayWithArray:[self.customer.notes componentsSeparatedByString:@"\n"]];
    self.details = [NSMutableDictionary dictionary];
}

-(void)updateViewStateAnimated:(BOOL)animated
{
    
    self.navigationItem.rightBarButtonItem = nil;
    if (self.editing)
    {
        UIBarButtonItem *doneButton =
        [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone
                                        target:self
                                        action:@selector(saveEditing)];
        self.navigationItem.rightBarButtonItem = doneButton;
    }
    else if (self.presentedFromSearch) {
        self.navigationItem.rightBarButtonItem = nil;
    } else {
        UIBarButtonItem *editButton =
        [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemEdit
                                                      target:self
                                                      action:@selector(startEditing)];
        self.navigationItem.rightBarButtonItem = editButton;
        
        BOOL customerHasChanged = NO;
        for (NSString *entryIdentifier in self.details)
        {
            if (![self.details[entryIdentifier] isEqual:[self.customer valueForKeyPath:entryIdentifier]])
            {
                [self.customer setValue:self.details[entryIdentifier] forKeyPath:entryIdentifier];
                customerHasChanged = YES;
            }
        }
    }
    
    NSMutableArray *notesLayout = [NSMutableArray array];
    for (NSString *note in self.notes)
    {
        [notesLayout addObject:kCustomerNote];
    }
    if (self.editing)
    {
        [notesLayout addObject:kCustomerNoteAdd];
    }
    
    NSMutableArray *cardsLayout = [NSMutableArray array];
    for (MPOSCustomerCard *customerCard in self.customer.customerCards)
    {
        [cardsLayout addObject:kCustomerCard];
    }
    
    self.layout = [NSMutableArray array];

    if (!self.editing && (self.customer.customerId != nil) && (![self.customer.customerId isEqualToString:@""]))
    {
        [self.layout addObject:@[kCustomerAssign]];
    }
    
    [self.layout addObject:@[kCustomerEmail,
                             kCustomerLastName,
                             kCustomerFirstName,
                             kCustomerGender,
                             kCustomerSalutation,
                             kCustomerPhone1,
                             kCustomerAddressLine1,
                             kCustomerAddressLine2,
                             kCustomerAddressCity,
                             kCustomerAddressTerritory,
                             kCustomerAddressPostcode,
                             kCustomerAddressCountry,
                             ]];

    OCGTableViewCell *cell = [[OCGTableViewCell alloc] init];
    for (NSInteger section=0; section < [self.layout count]; section++)
    {
        for (NSInteger row=0; row < [self.layout[section] count]; row++)
        {
            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:row inSection:section];
            [self tableView:nil willDisplayCell:cell forRowAtIndexPath:indexPath];
            self.textLabelWidth = MAX(self.textLabelWidth, cell.textLabelWidth);
        }
    }
    
    [self.tableView reloadData];
}

-(void)viewDidDisappear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
    [super viewDidDisappear:animated];
}

-(void)viewWillAppear:(BOOL)animated
{
    self.title = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁗󠀫󠀹󠁗󠁊󠁈󠁅󠀰󠁮󠁸󠀰󠁆󠁺󠀷󠁨󠀲󠁈󠁢󠁍󠁚󠁏󠁰󠁈󠁸󠁴󠁇󠁉󠁿*/ @"Customer", nil);

    [[CRSSkinning currentSkin] applyViewSkin: self];
    
    [[NSNotificationCenter defaultCenter] addObserver: self
                                             selector: @selector(managedObjectContextDidSave:)
                                                 name: kBasketControllerKickedNotification
                                               object: BasketController.sharedInstance];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(customerAssignedNotification:)
                                                 name:kBasketControllerBasketItemChangeNotification
                                               object:nil];
    
    if (!self.presentedFromSearch)
    {
        UIBarButtonItem *dismissButton = [[UIBarButtonItem alloc] initWithTitle: NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁡󠁩󠁡󠁍󠁰󠀸󠁷󠁣󠁹󠁩󠁕󠀷󠁪󠁶󠀱󠀸󠁐󠁖󠀵󠁥󠀳󠁫󠁕󠁚󠁄󠁶󠁙󠁿*/ @"Dismiss", nil)
                                                                          style: UIBarButtonItemStylePlain
                                                                         target: self
                                                                         action: @selector(dismissViewController:)];
        self.navigationItem.leftBarButtonItem = dismissButton;

    }
    else
    {
        self.navigationItem.leftBarButtonItem = nil;
    }
    
    [super viewWillAppear:animated];
    
    [self updateViewStateAnimated:NO];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)setEditing:(BOOL)editing animated:(BOOL)animated
{
    [super setEditing:editing animated:animated];
    
    if (editing)
    {
        UIBarButtonItem *doneButton =
        [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone
                                                      target:self
                                                      action:@selector(saveEditing)];
        self.navigationItem.rightBarButtonItem = doneButton;
    }
    else if (self.presentedFromSearch) {
        self.navigationItem.rightBarButtonItem = nil;
    } else {
        UIBarButtonItem *editButton =
        [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemEdit
                                                      target:self
                                                      action:@selector(startEditing)];
        self.navigationItem.rightBarButtonItem = editButton;
    }
    
    for (OCGTableViewCell *visibleCell in self.tableView.visibleCells)
    {
        NSIndexPath *indexPath = [self.tableView indexPathForCell:visibleCell];
        [self configureCell:visibleCell forRowAtIndexPath:indexPath];
    }
}

-(void)startEditing
{
    [self resetLocalCustomerDetails];
    [self setEditing:YES animated:YES];
    [self updateViewStateAnimated:YES];
}

-(void)cancelEditing
{
    [self resetLocalCustomerDetails];
    [self setEditing:NO animated:YES];
    if ([self.customer.customerId length] == 0)
    {
        self.customer = nil;
    }
    [self updateViewStateAnimated:YES];
}

-(void)saveEditing
{
    
    BOOL customerHasChanged = NO;
    for (NSString *entryIdentifier in self.details)
    {
        [self.customer setValue:self.details[entryIdentifier] forKeyPath:entryIdentifier];
        customerHasChanged = YES;
    }
    
    NSString *notes = [self.notes componentsJoinedByString:@"\n"];
    if (notes != nil && ![notes isEqualToString:self.customer.notes])
    {
        self.customer.notes = notes;
        customerHasChanged = YES;
    }
    
    if (customerHasChanged)
    {
        [[CustomerController sharedInstance] synchronizeChangesForCustomer:self.customer];
    }
    else
    {
        [self setEditing:NO animated:YES];
    }
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [self.layout count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.layout[section] count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *entryIdentifier = [self.layout objectForIndexPath:indexPath];

    NSString *CellIdentifier = kCustomerDefault;
    if ([entryIdentifier isEqual:kCustomerCard])
    {
        CellIdentifier = entryIdentifier;
    }
    
    OCGTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        UITableViewCellStyle style = UITableViewCellStyleValue2;
        if ([entryIdentifier isEqual:kCustomerCard])
        {
            style = UITableViewCellStyleSubtitle;
        }
        cell = [[OCGTableViewCell alloc] initWithStyle:style reuseIdentifier:CellIdentifier];
    }
    cell.detailTextField.delegate = self;
    
    return cell;
}

#define setKeyStyle(view) ({view.textColor = [UIColor colorWithRed:0.32 green:0.4 blue:0.57 alpha:1.0]; \
    view.font = [UIFont boldSystemFontOfSize:12.];})

#define setValueStyle(view) ({view.textColor = [UIColor blackColor]; \
    view.font =[UIFont systemFontOfSize:14.];})


-(void)configureCell:(OCGTableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *entryIdentifier = [self.layout objectForIndexPath:indexPath];

    cell.tag = kEditableTableCellSkinningTag;
    cell.textLabel.tag = kEditableCellKeySkinningTag;
    cell.detailTextField.tag = kEditableCellValueSkinningTag;
    cell.detailTextField.enabled = self.editing;
    cell.detailTextField.clearButtonMode = UITextFieldViewModeWhileEditing;
    cell.detailTextField.returnKeyType = UIReturnKeyNext;
    cell.detailTextField.keyboardType = UIKeyboardTypeDefault;
    cell.detailTextField.secureTextEntry = NO;
    cell.detailTextField.autocapitalizationType = UITextAutocapitalizationTypeWords;
    cell.detailTextField.autocorrectionType = UITextAutocorrectionTypeNo;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.accessoryType = UITableViewCellAccessoryNone;
    cell.textLabelWidth = 90;
    
    setKeyStyle(cell.textLabel);
    setValueStyle(cell.detailTextField);
    
    if ([entryIdentifier isEqual:kCustomerAssign])
    {
        cell.textLabel.text = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁖󠁭󠁋󠁧󠀫󠁔󠁬󠀸󠁨󠁗󠁳󠁒󠀯󠁚󠁬󠁓󠀳󠀰󠀷󠁩󠀲󠀸󠀸󠀱󠁴󠀱󠁫󠁿*/ @"Assign to basket", nil);
        cell.textLabel.tag = kTableCellKeySkinningTag;
        cell.selectionStyle = UITableViewCellSelectionStyleBlue;
        cell.detailTextField.enabled = NO;
        cell.detailTextField.text = @"";
    }
    else if ([entryIdentifier isEqual:kCustomerEmail])
    {
        cell.textLabel.text = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠀳󠁊󠀳󠁸󠁭󠁇󠁩󠁗󠀸󠁧󠁓󠁨󠁄󠁫󠁧󠁷󠁍󠀹󠀲󠀫󠁙󠀶󠀲󠁅󠁺󠁣󠁧󠁿*/ @"Email", nil);
        cell.detailTextField.enabled = [self.customer.customerId length] == 0;
        cell.detailTextField.text = self.details[entryIdentifier];
        if (self.details[entryIdentifier] == nil)
        {
            cell.detailTextField.text = self.customer.email;
        }
    }
    else if ([entryIdentifier isEqual:kCustomerLastName])
    {
        cell.textLabel.text = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁅󠀱󠁍󠁤󠁉󠁂󠀯󠁬󠁇󠁅󠁘󠁈󠁊󠁆󠁃󠁃󠀹󠁲󠁚󠁍󠁧󠀹󠁐󠁳󠁘󠀫󠁉󠁿*/ @"Last Name", nil);
        cell.detailTextField.text = self.details[entryIdentifier];
        if (self.details[entryIdentifier] == nil)
        {
            cell.detailTextField.text = self.customer.lastName;
        }
    }
    else if ([entryIdentifier isEqual:kCustomerFirstName])
    {
        cell.textLabel.text = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁱󠁉󠀫󠁌󠁮󠁶󠁡󠁉󠀹󠁉󠀰󠀵󠁡󠁦󠁂󠁎󠁦󠁦󠁵󠀴󠁩󠀯󠁦󠁡󠁅󠁲󠁉󠁿*/ @"First Name", nil);
        cell.detailTextField.text = self.details[entryIdentifier];
        if (self.details[entryIdentifier] == nil)
        {
            cell.detailTextField.text = self.customer.firstName;
        }
    }
    else if ([entryIdentifier isEqual:kCustomerGender])
    {
        cell.textLabel.text = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁕󠁤󠁓󠁪󠁡󠀫󠀱󠁩󠁗󠁢󠁦󠁥󠁣󠁰󠀯󠁢󠀶󠁐󠀳󠀵󠁂󠁋󠁙󠀴󠁴󠁸󠁳󠁿*/ @"Gender", nil);
        cell.detailTextField.enabled = NO;
        NSString *value = self.details[entryIdentifier];
        if (value == nil)
        {
            value = self.customer.gender;
        }
        
        if ([value length] > 0)
        {
            cell.detailTextField.text = NSLocalizedStringFromTable(value, @"Genders", nil);
        }
        else
        {
            cell.detailTextField.text = @"";
        }
        if (self.editing)
        {
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            cell.selectionStyle = UITableViewCellSelectionStyleBlue;
        }
    }
    else if ([entryIdentifier isEqual:kCustomerSalutation])
    {
        cell.textLabel.text = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠀵󠁌󠁫󠀱󠁮󠀳󠁍󠁆󠁕󠁨󠁒󠁴󠁺󠀰󠁤󠀸󠁫󠁎󠁵󠁚󠁂󠁰󠁤󠁥󠁧󠁊󠁉󠁿*/ @"Salutation", nil);
        cell.detailTextField.enabled = NO;
        NSString *value = self.details[entryIdentifier];
        if (value == nil)
        {
            value = self.customer.salutation;
        }
        
        if ([value length] > 0)
        {
            cell.detailTextField.text = NSLocalizedStringFromTable(value, @"Salutations", nil);
        }
        else
        {
            cell.detailTextField.text = @"";
        }
        if (self.editing)
        {
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            cell.selectionStyle = UITableViewCellSelectionStyleBlue;
        }
    }
    else if ([entryIdentifier isEqual:kCustomerPhone1])
    {
        cell.textLabel.text = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁋󠁊󠁢󠁨󠁙󠁸󠀴󠁭󠁹󠁗󠁍󠁦󠁹󠀵󠀱󠀰󠁊󠁒󠀴󠁪󠁓󠁦󠁌󠁘󠁆󠁮󠁳󠁿*/ @"Phone", nil);
        cell.detailTextField.keyboardType = UIKeyboardTypePhonePad;
        cell.detailTextField.text = self.details[entryIdentifier];
        if (self.details[entryIdentifier] == nil)
        {
            cell.detailTextField.text = self.customer.phone1;
        }
    }
    else if ([entryIdentifier isEqual:kCustomerAddressLine1])
    {
        cell.textLabel.text = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠀳󠀵󠁊󠀷󠁇󠁙󠁮󠁫󠁎󠁚󠁡󠁉󠁘󠀴󠁯󠁷󠀷󠁡󠁷󠀯󠁪󠁷󠁊󠁢󠁺󠁪󠁳󠁿*/ @"Address line 1", nil);
        cell.detailTextField.text = self.details[entryIdentifier];
        if (self.details[entryIdentifier] == nil)
        {
            cell.detailTextField.text = self.customer.address.line1;
        }
    }
    else if ([entryIdentifier isEqual:kCustomerAddressLine2])
    {
        cell.textLabel.text = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁡󠀰󠁪󠁯󠁑󠁶󠁦󠁯󠁮󠁧󠁡󠁆󠀷󠁱󠀵󠁅󠁨󠁏󠁨󠁨󠁡󠀲󠁬󠁱󠁯󠁦󠀴󠁿*/ @"Address line 2", nil);
        cell.detailTextField.text = self.details[entryIdentifier];
        if (self.details[entryIdentifier] == nil)
        {
            cell.detailTextField.text = self.customer.address.line2;
        }
    }
    else if ([entryIdentifier isEqual:kCustomerAddressCity])
    {
        cell.textLabel.text = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁢󠁒󠁆󠁨󠁍󠁭󠁁󠁗󠁱󠁘󠁢󠁮󠁋󠁺󠁗󠁂󠁫󠁔󠁥󠁐󠀯󠁊󠁬󠁉󠁱󠁁󠁅󠁿*/ @"City", nil);
        cell.detailTextField.text = self.details[entryIdentifier];
        if (self.details[entryIdentifier] == nil)
        {
            cell.detailTextField.text = self.customer.address.city;
        }
    }
    else if ([entryIdentifier isEqual:kCustomerAddressTerritory])
    {
        cell.textLabel.text = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁏󠀵󠁙󠁃󠁸󠀲󠀫󠁩󠁷󠁶󠀸󠁳󠀳󠁔󠁫󠀲󠁸󠀸󠀸󠁆󠁒󠁐󠁌󠁇󠀶󠁊󠁣󠁿*/ @"Territory", nil);
        cell.detailTextField.text = self.details[entryIdentifier];
        if (self.details[entryIdentifier] == nil)
        {
            cell.detailTextField.text = self.customer.address.territory;
        }
    }
    else if ([entryIdentifier isEqual:kCustomerAddressPostcode])
    {
        cell.textLabel.text = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁰󠁧󠁰󠁑󠀱󠁲󠁩󠁶󠁑󠁒󠁅󠀱󠀱󠁒󠁎󠀯󠁍󠁳󠀰󠁑󠀴󠁣󠁅󠁱󠀵󠁚󠁉󠁿*/ @"Postcode", nil);
        cell.detailTextField.text = self.details[entryIdentifier];
        if (self.details[entryIdentifier] == nil)
        {
            cell.detailTextField.text = self.customer.address.postcode;
        }
    }
    else if ([entryIdentifier isEqual:kCustomerAddressCountry])
    {
        cell.textLabel.text = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁌󠁯󠀹󠀲󠁫󠁉󠁙󠁥󠁦󠁢󠁱󠁹󠀴󠁦󠀲󠁙󠀳󠁌󠁍󠁒󠁕󠁍󠁦󠁬󠁎󠁲󠁁󠁿*/ @"Country", nil);
        cell.detailTextField.enabled = NO;
        cell.detailTextField.text = self.details[entryIdentifier];
        if (self.details[entryIdentifier] == nil)
        {
            cell.detailTextField.text = self.customer.address.country;
        }
        
//        NSString *localeCountryCode = [[NSLocale currentLocale] displayNameForKey:NSLocaleCountryCode value:self.customer.address.country];
//        if (localeCountryCode)
//        {
//            cell.detailTextField.text = localeCountryCode;
//        }
//        else
//        {
//            cell.detailTextField.text = self.customer.address.country;
//        }
        if (self.editing)
        {
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            cell.selectionStyle = UITableViewCellSelectionStyleBlue;
        }
    }
    else if ([entryIdentifier isEqual:kCustomerNote])
    {
        NSString *note = self.notes[indexPath.row];
        cell.textLabel.text = nil;
        cell.detailTextField.text = note;
    }
    else if ([entryIdentifier isEqual:kCustomerNoteAdd])
    {
        cell.textLabel.text = nil;
        cell.detailTextField.text = @"add new note";
        cell.detailTextField.enabled = NO;
        setKeyStyle(cell.detailTextField);
        cell.selectionStyle = UITableViewCellSelectionStyleBlue;
    }
    else if ([entryIdentifier isEqual:kCustomerCard])
    {
        MPOSCustomerCard *customerCard = self.customer.customerCards[indexPath.row];
        cell.textLabel.text = customerCard.cardName;
        cell.detailTextField.text = customerCard.cardNumber;
        cell.detailTextField.enabled = NO;
        
        BOOL needsButton = cell.accessoryView == nil;
        for (MPOSBasketCustomerCard *basketCustomerCard in BasketController.sharedInstance.basket.basketItems)
        {
            if ([basketCustomerCard isKindOfClass:[MPOSBasketCustomerCard class]])
            {
                if ([basketCustomerCard.customerCardNumber isEqualToString:customerCard.cardNumber])
                {
                    needsButton = NO;
                    break;
                }
            }
        }
        
        if (needsButton)
        {
            
            
            CRSGlossyButton *button = [[CRSGlossyButton alloc] init];
            button.tag = kPrimaryButtonSkinningTag;
            button.frame = CGRectMake(0, 0, 70, 30);
            [button setTitle:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠀹󠁂󠀹󠀹󠀹󠁲󠁺󠁷󠁈󠁴󠁉󠀴󠀸󠁉󠁐󠁪󠁈󠁋󠀫󠁥󠁄󠁷󠁥󠁅󠁈󠁚󠁅󠁿*/ @"Assign", nil) forState:UIControlStateNormal];
            [button addTarget: self
                       action: @selector(accessoryButtonTapped:withEvent:)
             forControlEvents: UIControlEventTouchUpInside];
            cell.accessoryView = button;
            cell.editingAccessoryView = nil;
        }
    }
    
    CGSize size = [[CRSSkinning currentSkin] sizeForString:cell.textLabel.text withTag:cell.textLabel.tag];
    cell.textLabelWidth = MAX(self.textLabelWidth, size.width);
}

- (void) accessoryButtonTapped: (UIControl *) button withEvent: (UIEvent *) event
{
    NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint: [[[event touchesForView: button] anyObject] locationInView: self.tableView]];
    if ( indexPath == nil )
        return;
    
    [self.tableView.delegate tableView: self.tableView accessoryButtonTappedForRowWithIndexPath: indexPath];
}

// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *entryIdentifier = [self.layout objectForIndexPath:indexPath];
    
    if ([entryIdentifier isEqual:kCustomerNote])
    {
        return YES;
    }
    else if ([entryIdentifier isEqual:kCustomerNoteAdd])
    {
        return YES;
    }
    else if ([entryIdentifier isEqual:kCustomerCard])
    {
        return YES;
    }
    else
    {
        return NO;
    }
}

//- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
//{
//    if (section == 1)
//    {
//        return NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁙󠁇󠀱󠁶󠁚󠀴󠁄󠁫󠀷󠁢󠁣󠁹󠁰󠁔󠀴󠁢󠁹󠀱󠁪󠁦󠁉󠀶󠁎󠁢󠁤󠀴󠁣󠁿*/ @"Notes", nil);
//    }
//    else if (section == 2)
//    {
//        return NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁎󠁕󠁢󠁋󠁡󠁮󠁡󠀹󠁏󠁸󠁚󠁑󠁑󠁄󠀹󠀸󠁉󠁧󠁉󠁩󠁧󠁰󠀳󠁬󠁥󠁣󠁙󠁿*/ @"Cards", nil);
//    }
//    else
//    {
//        return nil;
//    }
//}
//
//- (UIView *) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
//{
//    if (section == 0 && [tableView numberOfSections] == 1)
//    {
//        return [[UIView alloc] init];
//    }
//    else
//    {
//        return nil;
//    }
//}
//
//- (CGFloat) tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
//{
//    if (section == 0 && [tableView numberOfSections] == 1)
//    {
//        return 0.001;
//    }
//    else
//    {
//        return 0;
//    }
//}


- (void)tableView:(UITableView *)tableView willDisplayCell:(OCGTableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Configure the cell...
    
    [self configureCell:cell forRowAtIndexPath:indexPath];

    [[CRSSkinning currentSkin] applyCellSkin:cell
                                    forStyle:tableView.style];
}

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark - Table view delegate

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *entryIdentifier = [self.layout objectForIndexPath:indexPath];
    
    if ([entryIdentifier isEqual:kCustomerNote])
    {
        return UITableViewCellEditingStyleDelete;
    }
    else if ([entryIdentifier isEqual:kCustomerNoteAdd])
    {
        return UITableViewCellEditingStyleInsert;
    }
    else
    {
        return UITableViewCellEditingStyleNone;
    }
}

- (BOOL)tableView:(UITableView *)tableView shouldIndentWhileEditingRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *entryIdentifier = [self.layout objectForIndexPath:indexPath];
    
    if ([entryIdentifier isEqual:kCustomerNote])
    {
        return YES;
    }
    else if ([entryIdentifier isEqual:kCustomerNoteAdd])
    {
        return YES;
    }
    else
    {
        return NO;
    }
}

// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        
        NSString *entryIdentifier = [self.layout objectForIndexPath:indexPath];
        
        if ([entryIdentifier isEqual:kCustomerNote])
        {
            [self.notes removeObjectAtIndex:indexPath.row];
            NSMutableArray *notesLayout = self.layout[indexPath.section];
            [notesLayout removeObjectAtIndex:indexPath.row];
        }
        else
        {
            NSAssert(NO, @"You should not be deleting %@ at indexPath ", entryIdentifier, indexPath);
        }
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        NSString *entryIdentifier = [self.layout objectForIndexPath:indexPath];
        
        if ([entryIdentifier isEqual:kCustomerNoteAdd])
        {
            [self addNewNote];
        }
        else
        {
            NSAssert(NO, @"You should not be inserting %@ at indexPath ", entryIdentifier, indexPath);
        }
    }
}

- (NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *entryIdentifier = [self.layout objectForIndexPath:indexPath];
    
    if (self.editing || [entryIdentifier isEqual:kCustomerAssign])
    {
        return indexPath;
    }
    else
    {
        return nil;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *entryIdentifier = [self.layout objectForIndexPath:indexPath];
    
    if ([entryIdentifier isEqual:kCustomerAssign])
    {
        [[CustomerController sharedInstance] assignCustomerCardToBasket:self.customer.customerId];
    }
    else if ([entryIdentifier isEqual:kCustomerSalutation])
    {
        OCGSelectViewController *selectViewController = [[OCGSelectViewController alloc] init];
        
        selectViewController.options = @[@[@""],
                                         @[@"Miss",
                                           @"Mr",
                                           @"Mrs",
                                           @"Ms",
                                           ],
                                         ];
        
        selectViewController.titleForOption = ^(OCGSelectViewController *selectViewController, NSIndexPath *indexPath)
        {
            NSString *option = [selectViewController.options objectForIndexPath:indexPath];
            return NSLocalizedStringFromTable(option, @"Salutations", nil);
        };
        
        selectViewController.didSelectOption = ^(OCGSelectViewController *selectViewController, NSIndexPath *indexPath)
        {
            self.details[kCustomerSalutation] = [selectViewController.options objectForIndexPath:indexPath];
            [self.tableView reloadRowsAtIndexPaths:[self.layout indexPathsForObject:entryIdentifier] withRowAnimation:UITableViewRowAnimationNone];
            [self.navigationController popViewControllerAnimated:YES];
        };
        
        selectViewController.title = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠀵󠁌󠁫󠀱󠁮󠀳󠁍󠁆󠁕󠁨󠁒󠁴󠁺󠀰󠁤󠀸󠁫󠁎󠁵󠁚󠁂󠁰󠁤󠁥󠁧󠁊󠁉󠁿*/ @"Salutation", nil);
        
        [self.view endEditing:YES];
        [self.navigationController pushViewController:selectViewController
                                             animated:YES];
    }
    else if ([entryIdentifier isEqual:kCustomerGender])
    {
        OCGSelectViewController *selectViewController = [[OCGSelectViewController alloc] init];
        
        selectViewController.options = @[@[@""],@[@"F", @"M"]];
        
        
        selectViewController.titleForOption = ^(OCGSelectViewController *selectViewController, NSIndexPath *indexPath)
        {
            NSString *option = [selectViewController.options objectForIndexPath:indexPath];
            return NSLocalizedStringFromTable(option,@"Genders", nil);
        };
        
        selectViewController.didSelectOption = ^(OCGSelectViewController *selectViewController, NSIndexPath *indexPath)
        {
            self.details[kCustomerGender] = [selectViewController.options objectForIndexPath:indexPath];
            [self.tableView reloadRowsAtIndexPaths:[self.layout indexPathsForObject:entryIdentifier] withRowAnimation:UITableViewRowAnimationNone];
           [self.navigationController popViewControllerAnimated:YES];
        };
        
        selectViewController.title = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁕󠁤󠁓󠁪󠁡󠀫󠀱󠁩󠁗󠁢󠁦󠁥󠁣󠁰󠀯󠁢󠀶󠁐󠀳󠀵󠁂󠁋󠁙󠀴󠁴󠁸󠁳󠁿*/ @"Gender", nil);
        
        [self.view endEditing:YES];
        [self.navigationController pushViewController:selectViewController
                                             animated:YES];
    }
    else if ([entryIdentifier isEqual:kCustomerAddressCountry])
    {
        OCGSelectViewController *selectViewController = [[OCGSelectViewController alloc] init];
        
        NSArray *isoCountryCodes = [[NSLocale ISOCountryCodes] sortedArrayUsingComparator:^NSComparisonResult(id countryCode1, id countryCode2) {
            NSString *displayName1 = [[NSLocale currentLocale] displayNameForKey:NSLocaleCountryCode value:countryCode1];
            NSString *displayName2 = [[NSLocale currentLocale] displayNameForKey:NSLocaleCountryCode value:countryCode2];
            return [displayName1 compare:displayName2];
        }];
        
        NSArray *commonlyUsedISOCountryCodes = @[@"AUS", @"GBR", @"USA"];
        
        selectViewController.options = @[@[@""], commonlyUsedISOCountryCodes,isoCountryCodes];
                
        selectViewController.titleForOption = ^(OCGSelectViewController *selectViewController, NSIndexPath *indexPath)
        {
            NSString *countryCode = [selectViewController.options objectForIndexPath:indexPath];
            if ([countryCode isEqual:@""])
            {
                countryCode = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠀰󠁐󠁍󠁚󠁇󠁡󠁇󠁡󠁋󠁨󠁔󠁯󠀲󠁶󠁱󠁍󠀲󠁯󠀳󠁙󠁍󠁷󠁪󠀫󠁍󠁪󠁑󠁿*/ @"None", nil);
            }
            else
            {
                countryCode = [[NSLocale currentLocale] displayNameForKey:NSLocaleCountryCode value:countryCode];
            }
            return countryCode;
        };

        selectViewController.didSelectOption = ^(OCGSelectViewController *selectViewController, NSIndexPath *indexPath)
        {
            self.details[kCustomerAddressCountry] = [selectViewController.options objectForIndexPath:indexPath];
            [self.tableView reloadRowsAtIndexPaths:[self.layout indexPathsForObject:entryIdentifier] withRowAnimation:UITableViewRowAnimationNone];
            [self.navigationController popViewControllerAnimated:YES];
        };
        
        selectViewController.title = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁌󠁯󠀹󠀲󠁫󠁉󠁙󠁥󠁦󠁢󠁱󠁹󠀴󠁦󠀲󠁙󠀳󠁌󠁍󠁒󠁕󠁍󠁦󠁬󠁎󠁲󠁁󠁿*/ @"Country", nil);
        
        [self.view endEditing:YES];
        [self.navigationController pushViewController:selectViewController
                                             animated:YES];
    }
    else if ([entryIdentifier isEqual:kCustomerNoteAdd])
    {
        [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
        [self addNewNote];
    }
    else
    {
        OCGTableViewCell *cell = (OCGTableViewCell*) [self.tableView cellForRowAtIndexPath:indexPath];
        [cell.detailTextField becomeFirstResponder];
    }
}

- (void)tableView:(UITableView *)tableView accessoryButtonTappedForRowWithIndexPath:(NSIndexPath *)indexPath
{
    NSString *entryIdentifier = [self.layout objectForIndexPath:indexPath];
    
    if ([entryIdentifier isEqual:kCustomerCard])
    {
        MPOSCustomerCard *customerCard = self.customer.customerCards[indexPath.row];
        [[CustomerController sharedInstance] assignCustomerCardToBasket:customerCard.cardNumber];
    }

}

-(void)addNewNote
{
    NSInteger notesSection = 1;
    NSMutableArray *notesLayout = self.layout[notesSection];
    NSIndexPath* newIndexPath = [NSIndexPath indexPathForRow:[self.notes count] inSection:notesSection];
    [notesLayout insertObject:kCustomerNote atIndex:newIndexPath.row];
    [self.notes addObject:@""];
    [self.tableView insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
    OCGTableViewCell *nextCell = (OCGTableViewCell*) [self.tableView cellForRowAtIndexPath:newIndexPath];
    [nextCell.detailTextField becomeFirstResponder];
    NSIndexPath *nextIndexPath = [NSIndexPath indexPathForRow:newIndexPath.row+1 inSection:newIndexPath.section];
    [self.tableView scrollToRowAtIndexPath:nextIndexPath atScrollPosition:UITableViewScrollPositionMiddle animated:YES];
}

#pragma mark - Actions

/** @brief Called whenever the user touched the button to dismiss this view controller.
 *  @param sender A reference to the object that triggered the action.
 */
- (IBAction)dismissViewController:(id)sender
{
    if (self.delegate != nil) {
        if ([self.delegate respondsToSelector: @selector(dismissCustomerSearchViewController:)]) {
            [self.delegate dismissCustomerSearchViewController: (id)self];
        }
    }
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    return YES;
}

- (BOOL)textFieldShouldClear:(UITextField *)textField
{
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    NSIndexPath *indexPath = [self.tableView indexPathForCell:textField.OCGTableViewCell_tableViewCell];
    NSIndexPath *nextIndexPath = indexPath;
    do
    {
        nextIndexPath = [self.tableView indexPathFromIndexPath:nextIndexPath offset:1 wrap:YES];
        
        [self.tableView scrollToRowAtIndexPath:nextIndexPath atScrollPosition:UITableViewScrollPositionMiddle animated:NO];
        OCGTableViewCell *nextCell = (OCGTableViewCell*) [self.tableView cellForRowAtIndexPath:nextIndexPath];

        if ([nextCell respondsToSelector:@selector(detailTextField)] && [nextCell.detailTextField becomeFirstResponder])
        {
            return NO;
        }
    } while (![indexPath isEqual:nextIndexPath]);
    
    return NO;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSIndexPath *indexPath = [self.tableView indexPathForCell:textField.OCGTableViewCell_tableViewCell];
    NSString *entryIdentifier = [self.layout objectForIndexPath:indexPath];
    NSString *text = [textField.text stringByReplacingCharactersInRange:range withString:string];

    NSDictionary *maxLengths = @{
        kCustomerFirstName : @(40),
        kCustomerLastName : @(80),
        kCustomerSalutation : @(80),
        kCustomerGender : @(40),
        kCustomerAddressLine1 : @(80),
        kCustomerAddressLine2 : @(80),
        kCustomerAddressCity : @(80),
        kCustomerAddressTerritory : @(40),
        kCustomerAddressPostcode : @(40),
        kCustomerAddressCountry : @(40),
        kCustomerPhone1 : @(40),
        kCustomerEmail : @(80),
    };
    
    if (maxLengths[entryIdentifier] != nil && [text length] > [maxLengths[entryIdentifier] integerValue])
    {
        return NO;
    }
    
    if ([entryIdentifier isEqual:kCustomerGender]
        ||[entryIdentifier isEqual:kCustomerAddressCountry])
    {
        NSAssert(NO, @"You should not be editing %@", entryIdentifier);
    }
    else if ([entryIdentifier isEqual:kCustomerNote])
    {
        [self.notes replaceObjectAtIndex:indexPath.row withObject:text];
    }
    else
    {
        self.details[entryIdentifier] = text;
    }
    return YES;
}

#pragma mark Notifications

- (void) managedObjectContextDidSave:(NSNotification*)notification
{

    self.notes = [[self.customer.notes componentsSeparatedByString:@"\n"] mutableCopy];
    [self updateViewStateAnimated:YES];
    if (self.customer.hasCustomerChangedValue)
    {
        [OCGProgressOverlay show];
    }
    else
    {
        [OCGProgressOverlay hide];
        if (!self.customer.customerChangeError && self.editing)
        {
            [self setEditing:NO animated:YES];
        }
    }
    
    if (self.customer.customerChangeError)
    {
        ServerErrorHandler *serverErrorHandler = [[ServerErrorHandler alloc] init];
        [serverErrorHandler handleServerError:self.customer.customerChangeError
                               transportError:self.customer.customerChangeError.transportError
                                  withContext:self.customer.customerChangeError.context
                                 dismissBlock:^{
            if (self.customer.customerChangeError != nil)
            {
                self.customer.customerChangeError = nil;
                [BasketController.sharedInstance kick];
            }
        }
                                  repeatBlock:nil];
    }
    
    if (!self.editing)
    {
        self.navigationItem.rightBarButtonItem.enabled = ![[BasketController sharedInstance] hasCustomerAssociated];
    }
}

@end
