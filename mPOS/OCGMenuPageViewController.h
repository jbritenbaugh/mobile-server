//
//  OCGMenuPageViewController.h
//  MenuTest
//
//  Created by Antonio Strijdom on 18/03/2013.
//  Copyright (c) 2013 Antonio Strijdom. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OCGMenuCell.h"
#import "RESTController.h"

/** @file OCGMenuPageViewController.h */

@class OCGMenuPageViewController;

/** @brief The datasource protocol for menus.
 */
@protocol OCGMenuPageViewControllerDataSource <NSObject>
@optional

/** @brief The number of rows to display on the specified page.
 *  @param page The page we are getting number of rows for.
 *  @return The number of rows.
 */
- (NSUInteger) numberOfRowsOnPage:(OCGMenuPageViewController *)page;

/** @brief The number of columns to display on the specified page.
 *  @param page The page we are getting number of columns for.
 *  @return The number of columns.
 */
- (NSUInteger) numberOfColumnsOnPage:(OCGMenuPageViewController *)page;

/** @brief The cell that represents the item at the given index path.
 *  @param page The page the menu item is being displayed on.
 *  @param menuItem The menu item of the cell the controller is requesting.
 *  @param indexPath The index path for the cell the controller is requesting.
 */
- (OCGMenuCell *) menuPageViewController:(OCGMenuPageViewController *)page
                         cellForMenuItem:(SoftKey *)menuItem
                             atIndexPath:(NSIndexPath *)indexPath;

/** @brief Allows the datasource to specify the item to display in the navigation bar on the specified menu page (if it is being displayed.
 *  @param page The page the navigation bar item is being displayed on.
 *  @return The item that will appear on the right of the navigation bar at the top of the menu page.
 */
- (UIBarButtonItem *) rightNavigationItemForMenuPageViewController:(OCGMenuPageViewController *)page;

/** @brief Allows the datasource to specify the items to display in the tool bar on the specified menu page (if it is being displayed.
 *  @param page The page the tool bar items are being displayed on.
 *  @return The items that will appear on the tool bar at the bottom of the menu page.
 */
- (NSArray *) toolbarItemsForMenuPageViewController:(OCGMenuPageViewController *)page;

@end

/** @brief The delegate protocol for menus.
 */
@protocol OCGMenuPageViewControllerDelegate <NSObject>
@optional
/** @brief Called when the user highlights a menu item.
 *  @param page The page the menu item is being displayed on.
 *  @param menuItem The menu item the user tapped on.
 */
- (void) menuPageViewController:(OCGMenuPageViewController *)page
               didHighlightItem:(SoftKey *)menuItem;

/** @brief Called when the user selects a menu item.
 *  @param page The page the menu item is being displayed on.
 *  @param menuItem The menu item the user tapped on.
 */
- (void) menuPageViewController:(OCGMenuPageViewController *)page
                  didSelectItem:(SoftKey *)menuItem;

/** @brief Called when the user deselects a menu item.
 *  @param page The page the menu item is being displayed on.
 *  @param menuItem The menu item the user released.
 */
- (void) menuPageViewController:(OCGMenuPageViewController *)page
             didUnhighlightItem:(SoftKey *)menuItem;

/** @brief Allows the delegate to override the default padding used for cells.
 *  @param page The page being displayed.
 *  @return The padding as a CGPoint. x represents the horizontal padding and y represents the vertical padding.
 */
- (CGPoint) cellPaddingForMenuPageViewController:(OCGMenuPageViewController *)page;

/** @brief Allows the delegate to override the default height used for cells (44).
 *  @param page The page being deisplayed.
 *  @return The height as a CGFloat.
 */
- (CGFloat) cellHeightForMenuPageViewController:(OCGMenuPageViewController *)page;

/** @brief Allows the delegate to override the default width used for cells.
 *  @param menuItem The menu item being displayed.
 *  @param page The page being displayed.
 *  @return The width as a CGFloat.
 */
- (CGFloat) cellWidthForMenuItem:(SoftKey *)menuItem
                          onPage:(OCGMenuPageViewController *)page;

@end

/** @brief View controller that wraps collection view controller to display menus.
 */
@interface OCGMenuPageViewController : UICollectionViewController <UICollectionViewDelegateFlowLayout>

/** @property menu
 *  @brief The menu currently being displayed.
 */
@property (nonatomic, strong) id menu;

/** @property cellRegistryDict
 *  @brief A dictionary containing the cell reuse ids that the collection view will have registered.
 *  @note The reuse ids are only registered when the view is loaded.
 */
@property (nonatomic, strong) NSDictionary *cellRegistryDict;

/** @property dataSource
 *  @brief The data source for this menu.
 *  @see OCGMenuViewControllerDataSource
 */
@property (nonatomic, strong) id<OCGMenuPageViewControllerDataSource> menuDataSource;

/** @property delegate
 *  @brief The delegate for this menu.
 *  @see OCGMenuViewControllerDelegate
 */
@property (nonatomic, strong) id<OCGMenuPageViewControllerDelegate> menuDelegate;

/** @property calculatedKeySize
 *  @brief The final key size, as displayed on the page.
 */
@property (nonatomic, readonly) CGSize calculatedKeySize;

/** @property numberOfColumns
 *  @brief The number of columns on this page.
 */
@property (nonatomic, readonly) NSInteger numberOfColumns;

/** @property keys
 *  @brief The keys being displayed on this page.
 *  @note This is the final key list constructed
 *  from the menu. This array may not contain every
 *  key defined in the context.
 */
@property (nonatomic, strong) NSArray *keys;

/** @propery numberOfRows
 *  @brief The number of rows on this page.
 */
@property (nonatomic, readonly) NSInteger numberOfRows;

/** @property horizontalScrolling
 *  @brief Enables/disables horizontal scrolling.
 *  @note This defaults to NO (vertical scrolling).
 */
@property (nonatomic, assign) BOOL horizontalScrolling;

/** @brief Initialiser - creates a menu page for the specified menu, in the parent with the cell reuse ids specified, using the layout specified.
 *  @param layout The layout to use.
 *  @param menu The menu this page will represent.
 *  @param controller The controller this page will be presented by.
 *  @param cellDict Dictionary containing the cell reuseids.
 */
- (id) initWithCollectionViewLayout:(UICollectionViewLayout *)layout
                               menu:(SoftKey *)menu
                   withCellReuseIds:(NSDictionary *)cellDict;

/** @brief Initialiser - creates a menu page for the specified menu, in the parent with the cell reuse ids specified.
 *  @param menu The menu this page will represent.
 *  @param controller The controller this page will be presented by.
 *  @param cellDict Dictionary containing the cell reuseids.
 */
- (id) initWithMenu:(id)menu
   withCellReuseIds:(NSDictionary *)cellDict;

/** @brief Initialiser - creates a menu page for the specified menu, in the parent with the cell reuse ids specified.
 *  @param menu The menu this page will represent.
 *  @param controller The controller this page will be presented by.
 *  @param cellDict Dictionary containing the cell reuseids.
 *  @param menuDataSource The object to use as the datasource for the menu.
 *  @param menuDelegate The object to use as the delegate for the menu.
 */
- (id) initWithMenu:(id)menu
   withCellReuseIds:(NSDictionary *)cellDict
     withDataSource:(id<OCGMenuPageViewControllerDataSource>)menuDataSource
        andDelegate:(id<OCGMenuPageViewControllerDelegate>)menuDelegate;

/** @brief Dequeues a menu item cell for a menu item.
 *  @param reuseId The reuse id for the cell to dequeue.
 *  @param indexPath The index path to dequeue a cell for.
 *  @note Reuse ids must be present in the cell registry dictionary.
 *  @see cellRegistryDict
 */
- (OCGMenuCell *) dequeueMenuItemCellWithReuseId:(NSString *)reuseId
                                    forIndexPath:(NSIndexPath *)indexPath;

/** @brief Calculates metrics (cell height/width, etc) for this page.
 */
- (void) calculatePageMetricsAndMenu;

/** @brief Reloads the menu.
 *  @param recalculate Whether or not to recalculate page metrics.
 */
- (void) reloadMenu:(BOOL)recalculate;

@end
