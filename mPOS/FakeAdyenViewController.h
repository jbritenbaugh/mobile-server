//
//  FakeAdyenViewController.h
//  mPOS
//
//  Created by Antonio Strijdom on 21/01/2015.
//  Copyright (c) 2015 Clarity. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AdyenToolkit/AdyenToolkit.h>

typedef NS_ENUM(NSUInteger, FakeAdyenViewControllerStep) {
    FakeAdyenViewControllerStepLogin = 0,
    FakeAdyenViewControllerStepConfigured = 1,
    FakeAdyenViewControllerStepBoarded = 2,
    FakeAdyenViewControllerStepSignatureRequired = 3,
    FakeAdyenViewControllerStepTransactionResult = 4
};

@interface FakeADYDevice : NSObject
@property (nonatomic, strong) NSString* name;
@property (nonatomic, assign) ADYDeviceStatus status;
@property (nonatomic, strong) ADYDeviceData* deviceData;
@property (nonatomic, strong) NSError* error;
@end

@interface FakeAdyenTransitioningDelegate : NSObject <UIViewControllerTransitioningDelegate>

@end

@interface FakeAdyenPresentationController : UIPresentationController

@end

@interface FakeAdyenViewController : UIViewController
@property (nonatomic, assign) FakeAdyenViewControllerStep step;
@property (nonatomic, strong) dispatch_block_t okBlock;
@property (nonatomic, strong) dispatch_block_t cancelBlock;
@property (nonatomic, strong) dispatch_block_t errorBlock;
@end
