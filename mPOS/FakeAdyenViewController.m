//
//  FakeAdyenViewController.m
//  mPOS
//
//  Created by Antonio Strijdom on 21/01/2015.
//  Copyright (c) 2015 Clarity. All rights reserved.
//

#import "FakeAdyenViewController.h"

@implementation FakeADYDevice

@end

@implementation FakeAdyenTransitioningDelegate

- (UIPresentationController *) presentationControllerForPresentedViewController:(UIViewController *)presented
                                                       presentingViewController:(UIViewController *)presenting
                                                           sourceViewController:(UIViewController *)source
{
    return [[FakeAdyenPresentationController alloc] initWithPresentedViewController:presented
                                                           presentingViewController:presenting];
}

@end

@implementation FakeAdyenPresentationController

#pragma mark - UIPresentationController

- (instancetype) initWithPresentedViewController:(UIViewController *)presentedViewController
                        presentingViewController:(UIViewController *)presentingViewController
{
    self = [super initWithPresentedViewController:presentedViewController
                         presentingViewController:presentingViewController];
    if (self) {
        
    }
    
    return self;
}

- (UIModalPresentationStyle) adaptivePresentationStyle
{
    return UIModalPresentationCustom;
}

- (void) containerViewDidLayoutSubviews
{
    self.presentedView.frame = [self frameOfPresentedViewInContainerView];
}

- (BOOL) shouldPresentInFullscreen
{
    return YES;
}

- (CGRect) frameOfPresentedViewInContainerView
{
    CGRect result = CGRectInset(self.containerView.bounds, 5.0f, 200.0f);
    return result;
}

@end

@interface FakeAdyenViewController ()
@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UIButton *okButton;
@property (nonatomic, strong) UIButton *cancelButton;
@property (nonatomic, strong) UIButton *errorButton;
- (IBAction) okButtonTapped:(id)sender;
- (IBAction) cancelButtonTapped:(id)sender;
- (IBAction) errorButtonTapped:(id)sender;
- (IBAction) crashButtonTapped:(id)sender;
@end

@implementation FakeAdyenViewController

#pragma mark - UIViewController

- (void) viewDidLoad
{
    self.view.backgroundColor = [UIColor clearColor];
    // add dimming view
    UIBlurEffect *blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleLight];
//    UIVibrancyEffect *effect = [UIVibrancyEffect effectForBlurEffect:blurEffect];
    UIVisualEffectView *dimmingView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
    dimmingView.translatesAutoresizingMaskIntoConstraints = NO;
    [self.view addSubview:dimmingView];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:dimmingView
                                                          attribute:NSLayoutAttributeTop
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:self.view
                                                          attribute:NSLayoutAttributeTop
                                                         multiplier:1.0f
                                                           constant:0.0f]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:dimmingView
                                                          attribute:NSLayoutAttributeBottom
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:self.view
                                                          attribute:NSLayoutAttributeBottom
                                                         multiplier:1.0f
                                                           constant:0.0f]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:dimmingView
                                                          attribute:NSLayoutAttributeLeft
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:self.view
                                                          attribute:NSLayoutAttributeLeft
                                                         multiplier:1.0f
                                                           constant:0.0f]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:dimmingView
                                                          attribute:NSLayoutAttributeRight
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:self.view
                                                          attribute:NSLayoutAttributeRight
                                                         multiplier:1.0f
                                                           constant:0.0f]];
    // add title
    self.titleLabel = [[UILabel alloc] init];
    self.titleLabel.translatesAutoresizingMaskIntoConstraints = NO;
    self.titleLabel.textAlignment = NSTextAlignmentCenter;
    self.titleLabel.font = [UIFont systemFontOfSize:[UIFont systemFontSize]];
    self.titleLabel.textColor = [UIColor darkTextColor];
    self.titleLabel.numberOfLines = 0;
    [dimmingView.contentView addSubview:self.titleLabel];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.titleLabel
                                                          attribute:NSLayoutAttributeTop
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:dimmingView
                                                          attribute:NSLayoutAttributeTop
                                                         multiplier:1.0f
                                                           constant:20.0f]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.titleLabel
                                                          attribute:NSLayoutAttributeCenterX
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:dimmingView
                                                          attribute:NSLayoutAttributeCenterX
                                                         multiplier:1.0f
                                                           constant:0.0f]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.titleLabel
                                                          attribute:NSLayoutAttributeWidth
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:dimmingView
                                                          attribute:NSLayoutAttributeWidth
                                                         multiplier:1.0f
                                                           constant:-20.0f]];
    // add buttons
    // ok
    self.okButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    self.okButton.translatesAutoresizingMaskIntoConstraints = NO;
    [self.okButton setTitle:@"OK" forState:UIControlStateNormal];
    [self.okButton addTarget:self
                      action:@selector(okButtonTapped:)
            forControlEvents:UIControlEventTouchUpInside];
    [dimmingView.contentView addSubview:self.okButton];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.okButton
                                                          attribute:NSLayoutAttributeTop
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:self.titleLabel
                                                          attribute:NSLayoutAttributeBottom
                                                         multiplier:1.0f
                                                           constant:20.0f]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.okButton
                                                          attribute:NSLayoutAttributeCenterX
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:dimmingView
                                                          attribute:NSLayoutAttributeCenterX
                                                         multiplier:1.0f
                                                           constant:0.0f]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.okButton
                                                          attribute:NSLayoutAttributeWidth
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:dimmingView
                                                          attribute:NSLayoutAttributeWidth
                                                         multiplier:1.0f
                                                           constant:-20.0f]];
    // cancel
    self.cancelButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    self.cancelButton.translatesAutoresizingMaskIntoConstraints = NO;
    [self.cancelButton setTitle:@"Cancel" forState:UIControlStateNormal];
    [self.cancelButton addTarget:self
                          action:@selector(cancelButtonTapped:)
                forControlEvents:UIControlEventTouchUpInside];
    [dimmingView.contentView addSubview:self.cancelButton];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.cancelButton
                                                          attribute:NSLayoutAttributeTop
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:self.okButton
                                                          attribute:NSLayoutAttributeBottomMargin
                                                         multiplier:1.0f
                                                           constant:0.0f]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.cancelButton
                                                          attribute:NSLayoutAttributeCenterX
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:dimmingView
                                                          attribute:NSLayoutAttributeCenterX
                                                         multiplier:1.0f
                                                           constant:0.0f]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.cancelButton
                                                          attribute:NSLayoutAttributeWidth
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:dimmingView
                                                          attribute:NSLayoutAttributeWidth
                                                         multiplier:1.0f
                                                           constant:-20.0f]];
    // error
    self.errorButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    self.errorButton.translatesAutoresizingMaskIntoConstraints = NO;
    [self.errorButton setTitle:@"Error" forState:UIControlStateNormal];
    [self.errorButton addTarget:self
                         action:@selector(errorButtonTapped:)
                forControlEvents:UIControlEventTouchUpInside];
    [dimmingView.contentView addSubview:self.errorButton];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.errorButton
                                                          attribute:NSLayoutAttributeTop
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:self.cancelButton
                                                          attribute:NSLayoutAttributeBottomMargin
                                                         multiplier:1.0f
                                                           constant:0.0f]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.errorButton
                                                          attribute:NSLayoutAttributeCenterX
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:dimmingView
                                                          attribute:NSLayoutAttributeCenterX
                                                         multiplier:1.0f
                                                           constant:0.0f]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.errorButton
                                                          attribute:NSLayoutAttributeWidth
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:dimmingView
                                                          attribute:NSLayoutAttributeWidth
                                                         multiplier:1.0f
                                                           constant:-20.0f]];
    // crash
    UIButton *crashButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    crashButton.translatesAutoresizingMaskIntoConstraints = NO;
    [crashButton setTitle:@"Crash" forState:UIControlStateNormal];
    [crashButton addTarget:self
                    action:@selector(crashButtonTapped:)
          forControlEvents:UIControlEventTouchUpInside];
    [dimmingView.contentView addSubview:crashButton];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:crashButton
                                                          attribute:NSLayoutAttributeTop
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:self.errorButton
                                                          attribute:NSLayoutAttributeBottomMargin
                                                         multiplier:1.0f
                                                           constant:0.0f]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:crashButton
                                                          attribute:NSLayoutAttributeCenterX
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:dimmingView
                                                          attribute:NSLayoutAttributeCenterX
                                                         multiplier:1.0f
                                                           constant:0.0f]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:crashButton
                                                          attribute:NSLayoutAttributeWidth
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:dimmingView
                                                          attribute:NSLayoutAttributeWidth
                                                         multiplier:1.0f
                                                           constant:-20.0f]];
}

- (void) viewWillAppear:(BOOL)animated
{
    self.titleLabel.text = nil;
    switch (self.step) {
        case FakeAdyenViewControllerStepLogin: {
            self.titleLabel.text = @"Login";
            [self.okButton setTitle:@"Login"
                           forState:UIControlStateNormal];
            [self.cancelButton setTitle:@"Fail"
                               forState:UIControlStateNormal];
            break;
        }
        case FakeAdyenViewControllerStepConfigured: {
            self.titleLabel.text = @"Device Configured?";
            [self.okButton setTitle:@"Yes"
                           forState:UIControlStateNormal];
            [self.cancelButton setTitle:@"No"
                               forState:UIControlStateNormal];
            break;
        }
        case FakeAdyenViewControllerStepBoarded: {
            self.titleLabel.text = @"Device Boarded?";
            [self.okButton setTitle:@"Yes"
                           forState:UIControlStateNormal];
            [self.cancelButton setTitle:@"No"
                               forState:UIControlStateNormal];
            break;
        }
        case FakeAdyenViewControllerStepSignatureRequired: {
            self.titleLabel.text = @"Signature Required?";
            [self.okButton setTitle:@"Yes"
                           forState:UIControlStateNormal];
            [self.cancelButton setTitle:@"No"
                               forState:UIControlStateNormal];
            break;
        }
        case FakeAdyenViewControllerStepTransactionResult: {
            self.titleLabel.text = @"Transaction Result";
            [self.okButton setTitle:@"Approved"
                           forState:UIControlStateNormal];
            [self.cancelButton setTitle:@"Declined"
                               forState:UIControlStateNormal];
            break;
        }
        default:
            break;
    }
    [super viewWillAppear:animated];
}

#pragma mark - Actions

- (IBAction) okButtonTapped:(id)sender
{
    [self dismissViewControllerAnimated:YES
                             completion:^{
                                 if (self.okBlock) self.okBlock();
                             }];
}

- (IBAction) cancelButtonTapped:(id)sender
{
    [self dismissViewControllerAnimated:YES
                             completion:^{
                                 if (self.cancelBlock) self.cancelBlock();
                             }];
}

- (IBAction) errorButtonTapped:(id)sender
{
    [self dismissViewControllerAnimated:YES
                             completion:^{
                                 if (self.errorBlock) self.errorBlock();
                             }];
}

- (IBAction) crashButtonTapped:(id)sender
{
    abort();
}

@end
