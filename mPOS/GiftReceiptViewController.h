//
//  GiftReceiptViewController.h
//  mPOS
//
//  Created by Antonio Strijdom on 19/02/2014.
//  Copyright (c) 2014 Clarity. All rights reserved.
//

#import "BasketMultiSelectViewController.h"

@interface GiftReceiptViewController : BasketMultiSelectViewController

@end
