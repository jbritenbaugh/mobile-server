//
//  BasketHistoryViewController.m
//  mPOS
//
//  Created by Antonio Strijdom on 21/11/2012.
//  Copyright (c) 2012 Clarity. All rights reserved.
//

#import "BasketHistoryViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "RESTClient.h"
#import "RESTController.h"
#import "CRSSkinning.h"
#import "HistoryTableViewCell.h"
#import "HistoryMoreViewCell.h"
#import "CRSLocationController.h"
#import "BasketHistoryFilterOptionsTableViewController.h"
#import "LocationDataSource.h"
#import "ServerErrorHandler.h"

@interface BasketHistoryViewController () <BasketHistoryFilterOptionsTableViewControllerDelegate>
@property (strong, nonatomic) NSMutableDictionary *history;
@property (strong, nonatomic) NSArray *dates;
@property (strong, nonatomic) NSArray *locations;
@property (assign, nonatomic) BOOL loading;
- (NSDate *) trimTimeFromDate:(NSDate *)date;
- (void) loadBasketsWithFilter;
- (void) loadBasketsForDate:(NSDate *)date;
- (void) dismissButtonTapped:(id)sender;
- (void) displayFilterOptions:(id)sender;
@end

@implementation BasketHistoryViewController

#pragma mark - Private

static NSString *CellIdentifier = @"Cell";
static NSString *MoreCellIdentifier = @"MoreCell";
static NSString *kOutXMLDateFormat = @"yyyy-MM-dd'T'HH:mm:ss.SSS";
static NSString *kInXMLDateFormat = @"yyyy-MM-dd HH:mm:ss.SSS";

- (NSDate *) trimTimeFromDate:(NSDate *)date
{
    NSCalendar *calendar = [NSCalendar autoupdatingCurrentCalendar];
    NSUInteger preservedComponents = (NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay);
    return [calendar dateFromComponents:[calendar components:preservedComponents
                                                    fromDate:date]];
}


- (void) loadBasketsWithAssociateId:(NSString*)associateId
                               from:(NSDate*)from
                                 to:(NSDate*)to
{
    ocg_async_background_overlay(^{
        [RESTController.sharedInstance getAssociateSalesHistoryWithAssociateId:associateId
                                                                          from:from
                                                                    locationId:[CRSLocationController getLocationKey]
                                                                        status:@"COMPLETE"
                                                                            to:to
                                                                      complete:^(NSArray *results, NSError *error, ServerError *serverError)
         {
             ocg_sync_main(^{
                 if (results)
                 {
                     NSSortDescriptor *sort =
                     [NSSortDescriptor sortDescriptorWithKey:@"created"
                                                   ascending:NO];
                     NSArray *baskets =
                     [results sortedArrayUsingDescriptors:[NSArray arrayWithObject:sort]];
                     if (baskets.count == 0) {
                         // no results, just put in a dummy record
                         // fix the date string
                         NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
                         [dateFormatter setDateFormat:kOutXMLDateFormat];
                         [dateFormatter setDateFormat:kInXMLDateFormat];
                         NSString *fromDateString = [dateFormatter stringFromDate:from];
                         [self.history setValue:[NSArray array]
                                         forKey:fromDateString];
                     } else {
                         // update the model
                         NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
                         [dateFormatter setDateFormat:kInXMLDateFormat];
                         for (MPOSBasket *basket in baskets) {
                             // get the date
                             NSDate *date = basket.created;
                             date = [self trimTimeFromDate:date];
                             NSString *dateKey = [dateFormatter stringFromDate:date];
                             // get the existing baskets
                             NSArray *existing = [self.history valueForKey:dateKey];
                             if (existing != nil) {
                                 existing = [existing arrayByAddingObject:basket];
                             } else {
                                 existing = [NSArray arrayWithObject:basket];
                             }
                             [self.history setValue:existing
                                             forKey:dateKey];
                         }
                     }
                     // reset the date cache
                     self.dates = nil;
                     // update the table view
                     [self.tableView reloadData];
                     // make sure the latest basket from the earliest day is visible.
                     if (self.dates.count > 0) {
                         NSIndexPath *path =
                         [NSIndexPath indexPathForRow:0
                                            inSection:self.dates.count - 1];
                         [self.tableView scrollToRowAtIndexPath:path
                                               atScrollPosition:UITableViewScrollPositionTop
                                                       animated:YES];
                     }
                     [self.tableView flashScrollIndicators];
                 }
                 
                 if (error != nil || serverError != nil)
                 {
                     ServerErrorMessage *message = [serverError.messages lastObject];
                     enum MShopperExceptionCode exceptionCode = [message.code integerValue];
                     
                     [[ServerErrorHandler sharedInstance] handleServerError:serverError
                                                             transportError:error
                                                                withContext:nil
                                                               dismissBlock:nil
                                                                repeatBlock:nil];
                 }
             });
         }];
    });
    
}

- (void) loadBasketsWithFilter
{
    NSString *associateId = nil;
    
    if ([self.filter[kFieldAssociate] boolValue])
    {
        // filter to just the current associate's transactions
        associateId = BasketController.sharedInstance.credentials.username;
    }
    
    [self loadBasketsWithAssociateId:associateId
                                from:self.filter[kFieldFromDate]
                                  to:self.filter[kFieldToDate]];
}

- (void) loadBasketsForDate:(NSDate *)date
{
    [self loadBasketsWithAssociateId:nil
                                from:date
                                  to:[date dateByAddingTimeInterval:24*60*60]];
}

- (void) dismissButtonTapped:(id)sender
{
    [self.delegate basketHistoryDidCancel:self];
}

- (void) displayFilterOptions:(id)sender
{
    if (self.locations != nil) {
        BasketHistoryFilterOptionsTableViewController *filterVC =
        [[BasketHistoryFilterOptionsTableViewController alloc] initWithNibName:@"BasketHistoryOptionsView"
                                                                        bundle:nil];
        filterVC.delegate = self;
        filterVC.selectedOptions = self.filter;
        filterVC.locations = self.locations;
        UINavigationController *optionsNav =
        [[UINavigationController alloc] initWithRootViewController:filterVC];
        filterVC.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
        [self presentViewController:optionsNav animated:YES completion:NULL];
    } else {
        [self showModalProgressOverlay:YES
                              animated:YES];
        LocationDataSource *locationDS = [[LocationDataSource alloc] init];
        [locationDS getLocationListWithBlock:^(id data, NSError *error) {
            self.locations = data;
            BasketHistoryFilterOptionsTableViewController *filterVC =
            [[BasketHistoryFilterOptionsTableViewController alloc] initWithNibName:@"BasketHistoryOptionsView"
                                                                            bundle:nil];
            filterVC.delegate = self;
            filterVC.selectedOptions = self.filter;
            filterVC.locations = self.locations;
            UINavigationController *optionsNav =
            [[UINavigationController alloc] initWithRootViewController:filterVC];
            filterVC.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
            [self presentViewController:optionsNav animated:YES completion:NULL];
            [self showModalProgressOverlay:NO
                                  animated:NO];
        }];
    }
}

#pragma mark - Properties

@synthesize history = _history;
- (void) setHistory:(NSMutableDictionary *)history
{
    _history = history;
    self.dates = nil;
}

@synthesize dates = _dates;
- (NSArray *) dates
{
    NSArray *result = _dates;
    if (result == nil) {
        // lazy sort the dates
        if (self.history != nil) {
            NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
            [dateFormatter setDateFormat:kInXMLDateFormat];
            
            NSArray *unsorted = self.history.allKeys;
            if (unsorted.count > 0) {
                if (unsorted.count == 1) {
                    _dates = unsorted;
                } else {
                    _dates = [unsorted sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2) {
                        NSString *date1String = (NSString *)obj1;
                        NSString *date2String = (NSString *)obj2;
                        
                        NSDate *date1 = [dateFormatter dateFromString:date1String];
                        NSDate *date2 = [dateFormatter dateFromString:date2String];
                        
                        return [date2 compare:date1];
                    }];
                }
            } else {
                _dates = [NSArray array];
            }
            
            result = _dates;
        }
    }
    
    return result;
}

#pragma mark - UIViewController

- (void) viewDidLoad
{
    [super viewDidLoad];
    
    // register the cell xibs
    UINib *nib = [UINib nibWithNibName:@"HistoryTableViewCell"
                                bundle:nil];
    [self.tableView registerNib:nib
         forCellReuseIdentifier:CellIdentifier];
    nib = [UINib nibWithNibName:@"HistoryMoreViewCell"
                         bundle:nil];
    [self.tableView registerNib:nib
         forCellReuseIdentifier:MoreCellIdentifier];
    // start with an empty history
    self.history = [NSMutableDictionary dictionary];
    // and no filter
    self.filter = nil;
    // clear locations
    self.locations = nil;
    // get todays baskets - starting from midnight
    NSDate *date = [NSDate date];
    date = [self trimTimeFromDate:date];
    [self loadBasketsForDate:date];
    
    // skin
    self.tableView.tag = kTableSkinningTag;
}

- (void) viewWillAppear:(BOOL)animated
{
    // localise
    self.title = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠀷󠀵󠁣󠁅󠁚󠁺󠁇󠁭󠁋󠁩󠁲󠀯󠁦󠀶󠁥󠀫󠀫󠁮󠁵󠁵󠁸󠀹󠁊󠁉󠁮󠁥󠁣󠁿*/ @"Sales History", nil);
    
    // add the dismiss button
    UIBarButtonItem *dismiss =
    [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁡󠁩󠁡󠁍󠁰󠀸󠁷󠁣󠁹󠁩󠁕󠀷󠁪󠁶󠀱󠀸󠁐󠁖󠀵󠁥󠀳󠁫󠁕󠁚󠁄󠁶󠁙󠁿*/ @"Dismiss", nil)
                                     style:UIBarButtonItemStylePlain
                                    target:self
                                    action:@selector(dismissButtonTapped:)];
    self.navigationItem.leftBarButtonItem = dismiss;
    // add the filter button
    UIBarButtonItem *filter =
    [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁰󠁑󠁤󠁉󠁴󠀳󠀴󠁚󠁑󠁗󠁢󠁸󠀵󠁄󠁂󠁘󠁰󠁙󠁉󠁪󠁶󠀷󠁌󠁨󠁳󠀰󠁅󠁿*/ @"Filter", nil)
                                     style:UIBarButtonItemStylePlain
                                    target:self
                                    action:@selector(displayFilterOptions:)];
    self.navigationItem.rightBarButtonItem = filter;
    
    [[CRSSkinning currentSkin] applyViewSkin:self];
    [super viewWillAppear:animated];
}

- (void) viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

#pragma mark - UITableViewDataSource

- (NSInteger) numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    NSInteger result = self.dates.count;
    
    if (result == 0) {
        // no data yet
        if (self.filter == nil) {
            result = 1;
        } else {
            // no data for filter
            result = 1;
        }
    }
    
    return result;
}

- (NSString *) tableView:(UITableView *)tableView
 titleForHeaderInSection:(NSInteger)section
{
    NSString *result = nil;
    
    if (self.dates.count > 0) {
        // convert the whole XML date format to a nice readable one for display
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:kInXMLDateFormat];
        NSDateFormatter *displayFormatter = [[NSDateFormatter alloc] init];
        displayFormatter.dateStyle = NSDateFormatterMediumStyle;
        NSString *dateString = [self.dates objectAtIndex:section];
        NSDate *date = [dateFormatter dateFromString:dateString];
        result = [displayFormatter stringFromDate:date];
    }
    
    return result;
}

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    NSInteger result = 0;
    
    if (self.dates.count > 0) {
        // get the basket count for this day
        NSDate *date = [self.dates objectAtIndex:section];
        if (date != nil) {
            NSArray *sales = [self.history objectForKey:date];
            if (sales != nil) {
                result = sales.count;
            }
            if (result == 0) {
                if (self.filter != nil) {
                    // no data for filter
                    result = 1;
                }
            }
        }
    }
    
    // add one for the more cell
    if (self.filter == nil) {
        // but only if this is the last section
        if (section == self.dates.count - 1) {
            result++;
        }
    }
    
    return result;
}

- (UITableViewCell *) tableView:(UITableView *)tableView
          cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *result = nil;
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:kInXMLDateFormat];
    NSDateFormatter *displayFormatter = [[NSDateFormatter alloc] init];
    displayFormatter.dateStyle = NSDateFormatterMediumStyle;
    NSDateFormatter *timeFormatter = [[NSDateFormatter alloc] init];
    timeFormatter.dateStyle = NSDateFormatterNoStyle;
    timeFormatter.timeStyle = NSDateFormatterShortStyle;
    NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
    numberFormatter.numberStyle = NSNumberFormatterCurrencyStyle;
    
    if (self.dates.count > 0) {
        // get the date
        NSString *dateString = [self.dates objectAtIndex:indexPath.section];
        if (dateString != nil) {
            NSArray *baskets = [self.history objectForKey:dateString];
            if (baskets != nil) {
                if (indexPath.row < baskets.count) {
                    // we are inside the basket list, show the basket
                    HistoryTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
                    
                    // get the basket
                    MPOSBasket *basket = [baskets objectAtIndex:indexPath.row];
                    if (basket != nil) {
                        // display the basket
                        cell.basketReferenceLabel.text = basket.barcode;
                        if (self.filter != nil) {
                            cell.basketLocationLabel.text = [self.filter objectForKey:kFieldLocation];
                        } else {
                            cell.basketLocationLabel.text = [CRSLocationController getLocationKey];
                        }
                        cell.basketAmountLabel.hidden = YES;
                        // total not available
                        //                        NSNumber *total = [NSNumber numberWithFloat:basket.amountDue.floatValue];
                        //                        cell.basketAmountLabel.text = [numberFormatter stringFromNumber:total];
                        NSDate *basketDate = basket.created;
                        cell.basketTimeLabel.text = [timeFormatter stringFromDate:basketDate];
                    }
                    // skin
                    cell.tag = kTableCellSkinningTag;
                    cell.basketReferenceLabel.tag = kTableCellTextSkinningTag;
                    cell.basketAmountLabel.tag = kTableCellValueSkinningTag;
                    cell.basketLocationLabel.tag = kTableCellKeySkinningTag;
                    cell.basketTimeLabel.tag = kTableCellValueSkinningTag;
                    
                    result = cell;
                } else {
                    if (self.filter == nil) {
                        if (indexPath.section == self.dates.count - 1) {
                            // out of bounds, must be the more cell
                            HistoryMoreViewCell *cell = [tableView dequeueReusableCellWithIdentifier:MoreCellIdentifier];
                            // convert the date to something displayable
                            NSDate *date = [dateFormatter dateFromString:dateString];
                            cell.moreLabel.text = [NSString stringWithFormat:@"%@-%@",
                                                   NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠀹󠁱󠁧󠁄󠁂󠁯󠁔󠁓󠁓󠀴󠁧󠁹󠁖󠁙󠁑󠁳󠁐󠁥󠁥󠁍󠀵󠁯󠁦󠁅󠁸󠁩󠁳󠁿*/ @"pre", nil),
                                                   [displayFormatter stringFromDate:date]];
                            if (self.loading) {
                                [cell.moreActivity startAnimating];
                                cell.moreImage.hidden = YES;
                            } else {
                                [cell.moreActivity stopAnimating];
                                if (cell.moreImage.hidden) {
                                    cell.moreImage.opaque = NO;
                                    cell.moreImage.alpha = 0.0f;
                                    cell.moreImage.hidden = NO;
                                    [UIView animateWithDuration:0.2
                                                     animations:^{
                                                         cell.moreImage.alpha = 1.0f;
                                                     }];
                                }
                            }
                            // add a gradient
                            CAGradientLayer *gradient = [CAGradientLayer layer];
                            gradient.type = kCAGradientLayerAxial;
                            gradient.frame = cell.moreGradient.frame;
                            // start and end points
                            // default to a vertical gradient
                            gradient.startPoint = CGPointMake(0.5f, 0.0f);
                            gradient.endPoint = CGPointMake(0.5f, 1.0f);
                            // locations
                            gradient.locations = [NSArray arrayWithObjects:
                                                  [NSNumber numberWithDouble:0.0f],
                                                  [NSNumber numberWithDouble:0.5f],
                                                  [NSNumber numberWithDouble:1.0f],
                                                  nil];
                            // colors
                            UIColor *veryLightGrey = [UIColor colorWithRed:205.0f/255.0f
                                                                     green:205.0f/255.0f
                                                                      blue:205.0f/255.0f
                                                                     alpha:255.0f/255.0f];
                            gradient.colors = [NSArray arrayWithObjects:
                                               (id)[veryLightGrey CGColor],
                                               (id)[[UIColor whiteColor] CGColor],
                                               (id)[veryLightGrey CGColor],
                                               nil];
                            [cell.moreGradient.layer insertSublayer:gradient
                                                            atIndex:0];
                            // skin
                            cell.tag = kTableCellSkinningTag;
                            cell.moreLabel.tag = kTableCellTextSkinningTag;
                            
                            result = cell;
                        }
                    } else {
                        // no data, just display a no data message
                        HistoryMoreViewCell *cell = [tableView dequeueReusableCellWithIdentifier:MoreCellIdentifier];
                        cell.moreLabel.text = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁌󠁡󠁐󠁔󠁐󠀯󠁗󠁤󠀵󠁓󠁌󠀫󠀴󠀳󠁖󠁸󠀸󠁪󠁋󠁘󠁥󠁮󠁴󠁷󠁐󠀲󠁯󠁿*/ @"No data found for this filter", nil);
                        [cell.moreActivity stopAnimating];
                        cell.moreImage.hidden = YES;
                        // skin
                        cell.tag = kTableCellSkinningTag;
                        cell.moreLabel.tag = kTableCellTextSkinningTag;
                        
                        result = cell;
                    }
                }
                
            }
        }
    } else {
        // no data, just display the loading cell
        HistoryMoreViewCell *cell = [tableView dequeueReusableCellWithIdentifier:MoreCellIdentifier];
        cell.moreLabel.text = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁅󠀴󠁁󠁡󠁦󠁒󠁐󠁹󠁷󠁧󠁦󠀳󠁪󠁂󠀹󠁏󠁥󠁂󠀲󠁙󠁧󠁧󠀳󠁁󠁢󠁑󠁅󠁿*/ @"Loading, please wait...", nil);
        [cell.moreActivity startAnimating];
        cell.moreImage.hidden = YES;
        // skin
        cell.tag = kTableCellSkinningTag;
        cell.moreLabel.tag = kTableCellTextSkinningTag;
        
        result = cell;
    }
    
    return result;
}

- (void) tableView:(UITableView *)tableView
   willDisplayCell:(UITableViewCell *)cell
 forRowAtIndexPath:(NSIndexPath *)indexPath
{
    [[CRSSkinning currentSkin] applyCellSkin:cell
                                    forStyle:tableView.style];
}

#pragma mark - UITableViewDelegate

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // get the date
    NSString *dateString = [self.dates objectAtIndex:indexPath.section];
    if (dateString != nil) {
        NSArray *baskets = [self.history objectForKey:dateString];
        if (baskets != nil) {
            // bounds check
            if (indexPath.row < baskets.count) {
                // user selected a basket
                // get the basket
                BasketDTO *basket = [baskets objectAtIndex:indexPath.row];
                if (basket != nil) {
                    // let the delegate know we selected a basket
                    [self.delegate basketHistory:self
                                 didSelectBasket:basket];
                }
            } else {
                if (self.filter == nil) {
                    // user selected load more
                    if (!self.loading) {
                        // convert the date so we can manipulate it
                        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
                        [dateFormatter setDateFormat:kInXMLDateFormat];
                        NSDate *date = [dateFormatter dateFromString:dateString];
                        // go back a day
                        NSDate *from = [date dateByAddingTimeInterval:24*60*60*-1];
                        [self loadBasketsForDate:from];
                    }
                }
            }
        }
    }
    [tableView deselectRowAtIndexPath:indexPath
                             animated:YES];
}

#pragma mark - BasketHistoryFilterOptionsTableViewControllerDelegate

/** @brief Informs the delegate of the users selected filter options.
 *  @param vc The view controller.
 *  @param options Dictionary containing the selected filter options.
 */
- (void) basketHistoryFilterOptionsVC:(BasketHistoryFilterOptionsTableViewController *)vc
                       didApplyFilter:(NSDictionary *)options
{
    [self dismissViewControllerAnimated:YES
                             completion:^{
                                 self.filter = options;
                                 self.history = nil;
                                 self.dates = nil;
                                 [self.tableView reloadData];
                                 self.history = [NSMutableDictionary dictionary];
                                 if (self.filter != nil) {
                                     [self loadBasketsWithFilter];
                                 } else {
                                     NSDate *date = [NSDate date];
                                     date = [self trimTimeFromDate:date];
                                     [self loadBasketsForDate:date];
                                 }
                             }];
}

@end
