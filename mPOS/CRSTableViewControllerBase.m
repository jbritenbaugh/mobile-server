//
//  CRSTableViewControllerBase.m
//  mPOS
//
//  Created by Antonio Strijdom on 23/11/2012.
//  Copyright (c) 2012 Clarity. All rights reserved.
//

#import "CRSTableViewControllerBase.h"
#import "MBProgressHUD.h"
#import "AppDelegate.h"

@interface CRSTableViewControllerBase ()

@end

@implementation CRSTableViewControllerBase

/** @brief Called to show/hide the overlay
 *  @param show YES, if the overlay should be shown, NO otherwise.
 *  @param animated YES, if the overlay should be shown/hidden using a visual transition.
 */
- (void) showModalProgressOverlay:(BOOL)show
                         animated:(BOOL)animated
{
    // check requirements
    
    if ((self.navigationController == nil) || (self.navigationController.view == nil))
        return;
    
    AppDelegate *applicationDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    UIView *keyView = applicationDelegate.window;
    
    MBProgressHUD *progressHUD = [MBProgressHUD HUDForView: keyView];
    if (progressHUD == nil) {
        progressHUD = [[MBProgressHUD alloc] initWithView: keyView];
        [keyView addSubview: progressHUD];
    }
    
    if (show) {
        [progressHUD setAnimationType: (animated ? MBProgressHUDAnimationZoomIn : MBProgressHUDAnimationFade)];
        progressHUD.mode = MBProgressHUDModeIndeterminate;
        progressHUD.labelText = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁷󠁎󠁊󠁦󠀫󠁉󠀲󠀶󠁫󠀰󠁍󠀯󠁣󠁪󠁪󠁳󠀷󠁃󠀶󠁨󠀫󠁎󠁪󠁑󠁺󠁑󠁙󠁿*/ @"syncing", nil);
        [progressHUD show: NO];
    }
    else {
        [progressHUD setAnimationType: (animated ? MBProgressHUDAnimationZoomOut : MBProgressHUDAnimationFade)];
        [progressHUD hide: NO];
    }
}

@end
