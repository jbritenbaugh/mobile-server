//
//  ReceiptItemViewController.m
//  mPOS
//
//  Created by John Scott on 05/02/2014.
//  Copyright (c) 2014 Clarity. All rights reserved.
//

#import "ReceiptItemViewController.h"
#import "NSArray+OCGIndexPath.h"

#import "UICollectionView+OCGExtensions.h"
#import "FNKCollectionViewLayout.h"

#import "OCGCollectionViewCell.h"
#import "BasketItemCollectionViewCell.h"
#import "OCGTextField.h"
#import "OCGFirstResponderAccessoryView.h"

#import "RESTController.h"
#import "ServerErrorHandler.h"
#import "UIAlertView+OCGBlockAdditions.h"
#import "BarcodeScannerController.h"
#import "OCGSelectViewController.h"
#import "BasketController.h"
#import "CRSSkinning.h"
#import "OCGNumberTextFieldValidator.h"
#import "CRSLocationController.h"
#import "OCGKeyboardTypeButtonItem.h"

@interface ReceiptItemViewController () <UITextFieldDelegate, OCGFirstResponderAccessoryViewDelegate>

@end

@implementation ReceiptItemViewController
{
    NSMutableArray *_visibleDetails;
    NSMutableDictionary *_itemDetails;
    OCGFirstResponderAccessoryView *_firstResponderAccessoryView;
    UIDatePicker *_datePicker;
    NSDateFormatter *_dateFormatter;
    NSDateFormatter *_displayDateFormatter;
    OCGNumberTextFieldValidator *_priceValidator;
    Item *_scannedItem;
    BOOL _hasHandledInitialItemDetailItemScanId;
    BasketItemCollectionViewCell *_fakeBasketItemTableViewCell;
    BOOL _startScan;
}

#pragma mark - Private

static NSString *CellIdentifierBasketItem = @"CellBasketItem";

- (void) submitReturn
{
    void (^handleResultBlock)(id basket, NSError *error, ServerError *serverError) = ^void(id basket, NSError *error, ServerError *serverError) {
        if (error != nil || serverError != nil)
        {
            if (serverError.overrideErrorMessageCheck) {
                [self dismissViewControllerAnimated:YES
                                         completion:^{
                                             [[NSNotificationCenter defaultCenter] postNotificationName:kBasketControllerOverrideErrorNotification
                                                                                                 object:self
                                                                                               userInfo:@{kBasketControllerGeneralErrorKey:serverError}];
                                         }];
            } else {
                ServerErrorMessage *message = [serverError.messages lastObject];
                enum MShopperExceptionCode exceptionCode = [message.code integerValue];
                
                [[ServerErrorHandler sharedInstance] handleServerError:serverError
                                                        transportError:error
                                                           withContext:nil
                                                          dismissBlock:^{
                                                              if ([message isKindOfClass:[POSErrorMessage class]]) {
                                                                  [self dismissViewController];
                                                                  if (self.sucessfulReturn)
                                                                  {
                                                                      self.sucessfulReturn();
                                                                  }
                                                              } else {
                                                                  if ([self handleExceptionCode:exceptionCode])
                                                                  {
                                                                      [self reloadData];
                                                                  }
                                                              }
                                                              
                                                          }
                                                           repeatBlock:nil];
            }
        }
        else
        {
            [self dismissViewController];
            if (self.sucessfulReturn)
            {
                self.sucessfulReturn();
            }
        }
    };
    
    ocg_async_background_overlay(^{
        NSString *price = [_itemDetails[@(ReceiptItemDetailPrice)] description];
        
        if ([_itemDetails[@(ReceiptItemDetailPrice)] isEqual:[_priceValidator numberFromString:_scannedItem.price]])
        {
            price = nil;
        }
        
        switch (_type)
        {
            case ReceiptItemViewControllerTypeNoReceiptReturn:
                [BasketController.sharedInstance noReceiptWithItemScanId:_itemDetails[@(ReceiptItemDetailItemScanId)]
                                                                   price:price
                                                                reasonId:_itemDetails[@(ReceiptItemDetailReasonId)]
                                                       reasonDescription:_itemDetails[@(ReceiptItemDetailReasonDescription)]
                                                        itemSerialNumber:_itemDetails[@(ReceiptItemDetailItemSerialNumber)]
                                                      originalItemScanId:_itemDetails[@(ReceiptItemDetailOriginalItemScanId)]
                                                 originalItemDescription:_itemDetails[@(ReceiptItemDetailOriginalItemDescription)]
                                                                complete:handleResultBlock];
                break;
                
            case ReceiptItemViewControllerTypeInternetReturn:
                [BasketController.sharedInstance internetReturnWithItemScanId:_itemDetails[@(ReceiptItemDetailItemScanId)]
                                                                        price:price
                                                                     reasonId:_itemDetails[@(ReceiptItemDetailReasonId)]
                                                            reasonDescription:_itemDetails[@(ReceiptItemDetailReasonDescription)]
                                                             itemSerialNumber:_itemDetails[@(ReceiptItemDetailItemSerialNumber)]
                                                           originalItemScanId:_itemDetails[@(ReceiptItemDetailOriginalItemScanId)]
                                                      originalItemDescription:_itemDetails[@(ReceiptItemDetailOriginalItemDescription)]
                                                          originalReferenceId:_itemDetails[@(ReceiptItemDetailOriginalReferenceId)]
                                                                     complete:handleResultBlock];
                break;
                
                
            case ReceiptItemViewControllerTypeUnlinkedReturn:
                [BasketController.sharedInstance unlinkedReturnWithItemScanId:_itemDetails[@(ReceiptItemDetailItemScanId)]
                                                      originalItemDescription:_itemDetails[@(ReceiptItemDetailOriginalItemDescription)]
                                                   originalTransactionStoreId:_itemDetails[@(ReceiptItemDetailOriginalTransactionStoreId)]
                                                      originalTransactionTill:_itemDetails[@(ReceiptItemDetailOriginalTransactionTillNumber)]
                                                        originalTransactionId:_itemDetails[@(ReceiptItemDetailOriginalTransactionId)]
                                                      originalTransactionDate:_itemDetails[@(ReceiptItemDetailOriginalTransactionDate)]
                                                                        price:price
                                                                     reasonId:_itemDetails[@(ReceiptItemDetailReasonId)]
                                                            reasonDescription:_itemDetails[@(ReceiptItemDetailReasonDescription)]
                                                             itemSerialNumber:_itemDetails[@(ReceiptItemDetailItemSerialNumber)]
                                                           originalItemScanId:_itemDetails[@(ReceiptItemDetailOriginalItemScanId)]
                                                          originalSalesPerson:_itemDetails[@(ReceiptItemDetailOriginalSalesPerson)]
                                                         originalCustomerCard:_itemDetails[@(ReceiptItemDetailOriginalCustomerCard)]
                                                                     complete:handleResultBlock];
                break;
                
            case ReceiptItemViewControllerTypeLinkedReturn:
                [BasketController.sharedInstance linkedReturnWithItemScanId:_itemDetails[@(ReceiptItemDetailItemScanId)]
                                                                     lineId:_itemDetails[@(ReceiptItemDetailLineId)]
                                                    originalItemDescription:_itemDetails[@(ReceiptItemDetailOriginalItemDescription)]
                                                 originalTransactionStoreId:_itemDetails[@(ReceiptItemDetailOriginalTransactionStoreId)]
                                                    originalTransactionTill:_itemDetails[@(ReceiptItemDetailOriginalTransactionTillNumber)]
                                                      originalTransactionId:_itemDetails[@(ReceiptItemDetailOriginalTransactionId)]
                                                    originalTransactionDate:_itemDetails[@(ReceiptItemDetailOriginalTransactionDate)]
                                                                   reasonId:_itemDetails[@(ReceiptItemDetailReasonId)]
                                                          reasonDescription:_itemDetails[@(ReceiptItemDetailReasonDescription)]
                                                           itemSerialNumber:_itemDetails[@(ReceiptItemDetailItemSerialNumber)]
                                                         originalItemScanId:_itemDetails[@(ReceiptItemDetailOriginalItemScanId)]
                                                                   complete:handleResultBlock];
                break;
                
            default:
                break;
        }
    });
}

#pragma mark - Properties

-(void)setType:(ReceiptItemViewControllerType)type
{
    _type = type;
    
    [_visibleDetails removeAllObjects];
    
    NSArray *layout = nil;
    
    switch (_type)
    {
        case ReceiptItemViewControllerTypeNoReceiptReturn:
            layout = @[
                       @(ReceiptItemDetailItemScanId),
                       @(ReceiptItemDetailReasonDescription),
                       @(ReceiptItemDetailPrice),
                       ];
            break;
            
        case ReceiptItemViewControllerTypeInternetReturn:
            layout = @[
                       @(ReceiptItemDetailItemScanId),
                       @(ReceiptItemDetailReasonDescription),
                       @(ReceiptItemDetailPrice),
                       @(ReceiptItemDetailOriginalReferenceId),
                       ];
            break;
            
        case ReceiptItemViewControllerTypeUnlinkedReturn:
            layout = @[
                       @(ReceiptItemDetailItemScanId),
                       @(ReceiptItemDetailReasonDescription),
                       @(ReceiptItemDetailPrice),
                       @(ReceiptItemDetailOriginalSalesPerson), // opt
                       @(ReceiptItemDetailOriginalCustomerCard), // opt
                       ];
            break;
            
        case ReceiptItemViewControllerTypeLinkedReturn:
            layout = @[
                       @(ReceiptItemDetailBastketItem),
                       @(ReceiptItemDetailReasonDescription),
                       ];
            break;
            
        default:
            break;
    }
    
    [_visibleDetails addObjectsFromArray:layout];
    
    [self reloadData];
}

-(void)setInitialItemDetails:(NSDictionary *)initialItemDetails
{
    _initialItemDetails = initialItemDetails;
    [_itemDetails removeAllObjects];
    [_itemDetails addEntriesFromDictionary:_initialItemDetails];
    [self reloadData];
}

#pragma mark - init

- (id)init
{
    FNKCollectionViewLayout *layout = [[FNKCollectionViewLayout alloc] init];
    self = [super initWithCollectionViewLayout:layout];
    if (self) {
        _itemDetails = [NSMutableDictionary dictionary];
        _visibleDetails = [NSMutableArray array];
        _priceValidator = [[OCGNumberTextFieldValidator alloc] init];
        _priceValidator.maxValue = INT_MAX;
        _priceValidator.minValue = 0;
        _priceValidator.currencyCode = BasketController.sharedInstance.currencyCode;
        _startScan = YES;
    }
    return self;
}

#pragma mark - UIViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    _fakeBasketItemTableViewCell = [[BasketItemCollectionViewCell alloc] init];
    
    [OCGCollectionViewCell registerForDetailClass:[OCGTextField class]
                                          options:OCGCollectionViewCellDetailOptionUseTextLabelBaseline
                                   collectionView:self.collectionView];
    
    [self.collectionView registerClass:[BasketItemCollectionViewCell class]
            forCellWithReuseIdentifier:CellIdentifierBasketItem];
    
    self.collectionView.delaysContentTouches = NO;
    
    _firstResponderAccessoryView = [[OCGFirstResponderAccessoryView alloc] init];
    
    _firstResponderAccessoryView.delegate = self;
    
    _datePicker = [[UIDatePicker alloc] initWithFrame:CGRectMake(0, 0, 500, 200)];
    _datePicker.datePickerMode = UIDatePickerModeDate;
    
    _dateFormatter = [[NSDateFormatter alloc] init];
    _dateFormatter.dateFormat = @"yyyy-MM-dd";
    
    _displayDateFormatter = [[NSDateFormatter alloc] init];
    _displayDateFormatter.dateStyle = NSDateFormatterMediumStyle;
    _displayDateFormatter.timeStyle = NSDateFormatterNoStyle;
    
    [_datePicker addTarget:self action:@selector(dateChanged) forControlEvents:UIControlEventValueChanged];
    
    self.collectionView.backgroundColor = [UIColor colorWithRed:0.937 green:0.937 blue:0.957 alpha:1.000];
}

-(void)viewWillAppear:(BOOL)animated
{
    self.title = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁲󠁭󠁵󠁶󠁘󠁹󠀰󠁴󠁨󠀫󠁇󠁄󠁩󠁱󠀶󠁴󠁃󠁔󠀳󠁋󠁄󠁹󠁑󠁗󠁔󠁣󠀸󠁿*/ @"Return", nil);
    
    [super viewWillAppear:animated];
    [self reloadData];
    
    if (self.initialItemDetails[@(ReceiptItemDetailItemScanId)] != nil && !_hasHandledInitialItemDetailItemScanId && self.initialItemDetails[@(ReceiptItemDetailLineId)] == nil)
    {
        _hasHandledInitialItemDetailItemScanId = YES;
        [self handleBarcode:self.initialItemDetails[@(ReceiptItemDetailItemScanId)] itemDetail:ReceiptItemDetailItemScanId];
    }
    [[CRSSkinning currentSkin] applyViewSkin:self];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    if (_startScan) {
        dispatch_async(dispatch_get_main_queue(), ^{
            NSIndexPath *indexPath = [NSIndexPath indexPathForItem:0 inSection:0];
            [self.collectionView selectItemAtIndexPath:indexPath
                                              animated:NO
                                        scrollPosition:UICollectionViewScrollPositionTop];
            [self collectionView:self.collectionView didSelectItemAtIndexPath:indexPath];
        });
    }
    _startScan = NO;
}

-(void)viewWillDisappear:(BOOL)animated
{
    BarcodeScannerController *controller = [BarcodeScannerController sharedInstance];
    // are we scanning?
    if (controller.scanning)
    {
        [controller stopScanning];
    }
    [super viewWillDisappear:animated];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView
     numberOfItemsInSection:(NSInteger)section
{
    return [_visibleDetails count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView
                  cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    ReceiptItemDetail itemDetail = [@[_visibleDetails] integerForIndexPath:indexPath];
    UICollectionViewCell *cell = nil;
    if (itemDetail == ReceiptItemDetailBastketItem) {
        cell = [collectionView dequeueReusableCellWithReuseIdentifier:CellIdentifierBasketItem
                                                         forIndexPath:indexPath];
        [self updateBasketItemTableViewCell:(BasketItemCollectionViewCell *)cell
                          forRowAtIndexPath:indexPath];
    } else {
        NSString *cellIdentifier =
        [OCGCollectionViewCell reuseIdentifierForDetailClass:[OCGTextField class]
                                                     options:OCGCollectionViewCellDetailOptionUseTextLabelBaseline];
        cell = [collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier
                                                         forIndexPath:indexPath];
        [self updateTextFieldTableViewCell:(OCGCollectionViewCell *)cell
                         forRowAtIndexPath:indexPath];
    }
    if ([cell isKindOfClass:[OCGCollectionViewCell class]])
    {
        ((OCGCollectionViewCell *)cell).selectionStyle = UITableViewCellSelectionStyleNone;
    }
    cell.backgroundColor = [UIColor whiteColor];
    cell.layer.borderWidth = 1.;
    cell.layer.borderColor = [[UIColor colorWithRed:0.89 green:0.89 blue:0.90 alpha:1.000] CGColor];
    
    [[CRSSkinning currentSkin] applyViewSkin:cell
                             withTagOverride:cell.tag];
    
    return cell;
}

#pragma mark - FNKCollectionViewDataSourceDelegate

- (NSArray * /* FNKSupplementaryItem */)collectionView:(UICollectionView *)collectionView
                                                layout:(UICollectionViewLayout*)collectionViewLayout
               supplementaryItemsForHeadersAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.item == 0) {
        FNKSupplementaryItem *result = [[FNKSupplementaryItem alloc] init];
        result.title = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁩󠁆󠁭󠀰󠁵󠁐󠁙󠁬󠁹󠁘󠁥󠁩󠁭󠁧󠁧󠁗󠁹󠀹󠁋󠁁󠁸󠁪󠀫󠁇󠁐󠁡󠁧󠁿*/ @"Item details", nil);
        
        return @[result];
    } else {
        return nil;
    }
}

- (CGFloat)collectionView:(UICollectionView *)collectionView
 heightForItemAtIndexPath:(NSIndexPath *)indexPath
{
    ReceiptItemDetail itemDetail = [@[_visibleDetails] integerForIndexPath:indexPath];
    if (itemDetail == ReceiptItemDetailBastketItem) {
        ReturnableSaleItem *basketItem = _itemDetails[@(ReceiptItemDetailBastketItem)];
        [_fakeBasketItemTableViewCell updateDisplayForBasketItem:(MPOSBasketItem *)basketItem];
        
        CGSize size = [_fakeBasketItemTableViewCell systemLayoutSizeFittingSize:UILayoutFittingCompressedSize];
        return size.height;
    } else {
        return 44.f;
    }
}

#pragma mark - UICollectionViewDelegate

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    ReceiptItemDetail itemDetail = [@[_visibleDetails] integerForIndexPath:indexPath];
    if (itemDetail == ReceiptItemDetailReasonDescription) {
        // present the reason list
        OCGSelectViewController *reasonVC =
        [[OCGSelectViewController alloc] init];
        reasonVC.title = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁮󠁘󠁒󠁉󠀰󠁊󠁙󠁑󠁊󠁡󠀷󠁎󠁡󠁦󠁷󠁲󠁳󠁬󠀱󠁭󠀸󠀯󠁳󠁫󠁄󠁋󠁫󠁿*/ @"Return Reason", nil);
        NSArray *returnReasons = BasketController.sharedInstance.returnReasons ?: @[];
        reasonVC.options = @[returnReasons];
        reasonVC.titleForOption = ^(OCGSelectViewController *selectViewController, NSIndexPath *indexPath) {
            DiscountReason *returnReason =
            [selectViewController.options objectForIndexPath:indexPath];
            return returnReason.text;
        };
        reasonVC.didSelectOption = ^(OCGSelectViewController *selectViewController, NSIndexPath *indexPath) {
            DiscountReason *returnReason =
            [selectViewController.options objectForIndexPath:indexPath];
            _itemDetails[@(ReceiptItemDetailReasonId)] = returnReason.reasonId;
            _itemDetails[@(ReceiptItemDetailReasonDescription)] = returnReason.text;
            [self.navigationController popViewControllerAnimated:YES];
            NSArray *indexPaths = [@[_visibleDetails] indexPathsForObject:@(ReceiptItemDetailReasonDescription)];
            [self.collectionView reloadItemsAtIndexPaths:indexPaths];
        };
        [self.navigationController pushViewController:reasonVC
                                             animated:YES];
    } else if (itemDetail == ReceiptItemDetailDumpCodeDescription && [BasketController.sharedInstance.dumpCodes count] > 1) {
        // present the reason list
        OCGSelectViewController *reasonVC =
        [[OCGSelectViewController alloc] init];
        reasonVC.title = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁕󠀯󠁹󠁬󠁈󠀹󠁮󠁹󠁑󠁐󠁨󠁃󠀷󠁋󠁤󠁩󠀱󠁃󠁨󠁯󠁥󠁅󠁄󠁕󠁄󠁔󠁳󠁿*/ @"Dump code", nil);
        reasonVC.options = @[[[BasketController sharedInstance] dumpCodes]];
        reasonVC.titleForOption = ^(OCGSelectViewController *selectViewController, NSIndexPath *indexPath) {
            DumpCode *dumpCode =
            [selectViewController.options objectForIndexPath:indexPath];
            return dumpCode.desc ?: dumpCode.itemSellingCode;
        };
        reasonVC.didSelectOption = ^(OCGSelectViewController *selectViewController, NSIndexPath *indexPath) {
            DumpCode *dumpCode =
            [selectViewController.options objectForIndexPath:indexPath];
            _itemDetails[@(ReceiptItemDetailDumpCodeDescription)] = dumpCode.desc ?: dumpCode.itemSellingCode;
            _itemDetails[@(ReceiptItemDetailItemScanId)] = dumpCode.itemSellingCode;
            [self.navigationController popViewControllerAnimated:YES];
            NSArray *indexPaths = [@[_visibleDetails] indexPathsForObject:@(ReceiptItemDetailDumpCodeDescription)];
            [self.collectionView reloadItemsAtIndexPaths:indexPaths];
        };
        [self.navigationController pushViewController:reasonVC
                                             animated:YES];
    } else if (itemDetail == ReceiptItemDetailItemScanId) {
        [self startScan:itemDetail];
    } else {
        OCGCollectionViewCell *cell = (OCGCollectionViewCell *)[self.collectionView cellForItemAtIndexPath:indexPath];
        if ([cell isKindOfClass:[OCGCollectionViewCell class]]) {
            [cell.detailTextField becomeFirstResponder];
        }
    }
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    NSIndexPath *indexPath = [self.collectionView indexPathForCell:textField.OCGCollectionViewCell_collectionViewCell];
    ReceiptItemDetail itemDetail = [@[_visibleDetails] integerForIndexPath:indexPath];
    
    BOOL textFieldShouldBeginEditing = YES;
    switch (itemDetail) {
        case ReceiptItemDetailItemScanId:
        case ReceiptItemDetailReasonDescription:
        case ReceiptItemDetailOriginalItemScanId:
        case ReceiptItemDetailDumpCodeDescription:
            textFieldShouldBeginEditing = NO;
            break;
        default:
            textFieldShouldBeginEditing = YES;
            break;
    }
    return textFieldShouldBeginEditing;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    NSIndexPath *indexPath = [self.collectionView indexPathForCell:textField.OCGCollectionViewCell_collectionViewCell];
    _firstResponderAccessoryView.currentPosition = indexPath;
    
    ReceiptItemDetail itemDetail = [@[_visibleDetails] integerForIndexPath:indexPath];
    
    OCGCollectionViewCell *cell = (OCGCollectionViewCell *)[self.collectionView cellForItemAtIndexPath:indexPath];
    
    if (itemDetail == ReceiptItemDetailOriginalTransactionDate) {
        _datePicker.date = [_dateFormatter dateFromString:_itemDetails[@(itemDetail)]] ?: [NSDate date];
        _itemDetails[@(itemDetail)] = [_dateFormatter stringFromDate:_datePicker.date];
        cell.detailTextField.text = [_displayDateFormatter stringFromDate:_datePicker.date];
    } else if (itemDetail == ReceiptItemDetailPrice) {
        cell.detailTextField.text = [_priceValidator stringFromNumber:_itemDetails[@(itemDetail)]];
    }
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    _firstResponderAccessoryView.currentPosition = nil;
}

- (BOOL)textFieldShouldClear:(UITextField *)textField
{
    NSIndexPath *indexPath = [self.collectionView indexPathForCell:textField.OCGCollectionViewCell_collectionViewCell];
    
    ReceiptItemDetail itemDetail = [@[_visibleDetails] integerForIndexPath:indexPath];
    [_itemDetails removeObjectForKey:@(itemDetail)];
    
    if (itemDetail == ReceiptItemDetailReasonDescription)
    {
        [_itemDetails removeObjectForKey:@(ReceiptItemDetailReasonId)];
    }
    
    [self updateNavigationItem];
    
    OCGCollectionViewCell *cell = (OCGCollectionViewCell *)[self.collectionView cellForItemAtIndexPath:indexPath];
    cell.detailTextField.text = nil;
    
    return NO;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if ([self canSubmitItem]) {
        [self submitButtonTapped];
    } else {
        [_firstResponderAccessoryView moveFirstResponderByOffset:1];
    }
    return NO;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSIndexPath *indexPath = [self.collectionView indexPathForCell:textField.OCGCollectionViewCell_collectionViewCell];
    NSString *text = [textField.text stringByReplacingCharactersInRange:range withString:string];
    
    ReceiptItemDetail itemDetail = [@[_visibleDetails] integerForIndexPath:indexPath];
    
    if (itemDetail == ReceiptItemDetailOriginalItemDescription && [text length] > 255) {
        return NO;
    }
    
    if (itemDetail == ReceiptItemDetailPrice) {
        if ([_priceValidator numberFromString:text]) {
            _itemDetails[@(itemDetail)] = [_priceValidator numberFromString:text];
        } else {
            [_itemDetails removeObjectForKey:@(itemDetail)];
        }
        
        textField.text = [_priceValidator stringFromNumber:_itemDetails[@(itemDetail)]];
        [self updateNavigationItem];
        
        return NO;
    } else {
        _itemDetails[@(itemDetail)] = text;
        
        [self updateNavigationItem];
        
        return YES;
    }
}

#pragma mark - OCGFirstResponderAccessoryViewDelegate

- (UIResponder *)OCGFirstResponderAccessoryView:(OCGFirstResponderAccessoryView *)firstResponderAccessoryView
                  firstResponderViewForPosition:(id)position
{
    [self.collectionView scrollToItemAtIndexPath:position
                                atScrollPosition:UICollectionViewScrollPositionCenteredVertically
                                        animated:NO];
    OCGCollectionViewCell *cell = (OCGCollectionViewCell *)[self.collectionView cellForItemAtIndexPath:position];
    if ([cell respondsToSelector:@selector(detailTextField)]) {
        return cell.detailTextField;
    } else {
        return nil;
    }
}

- (id)OCGFirstResponderAccessoryView:(OCGFirstResponderAccessoryView*)firstResponderAccessoryView
                positionFromPosition:(id)position offset:(NSInteger)offset
{
    return [self.collectionView indexPathFromIndexPath:position offset:offset wrap:YES];
}

#pragma mark - Date picker

- (void)dateChanged
{
    NSIndexPath *indexPath = _firstResponderAccessoryView.currentPosition;
    
    NSString *itemDetail = [@[_visibleDetails] objectForIndexPath:indexPath];
    _itemDetails[itemDetail] = [_dateFormatter stringFromDate:_datePicker.date];
    
    OCGCollectionViewCell *cell = (OCGCollectionViewCell *)[self.collectionView cellForItemAtIndexPath:indexPath];
    cell.detailTextField.text = [_displayDateFormatter stringFromDate:_datePicker.date];
    
    [self updateNavigationItem];
}

#pragma mark - Methods

-(void)reloadData
{
    [self.collectionView reloadData];
    [self updateNavigationItem];
    self.collectionView.tag = kEditableTableSkinningTag;
    [[CRSSkinning currentSkin] applyViewSkin:self];
}

-(void)updateBasketItemTableViewCell:(BasketItemCollectionViewCell *)cell
                   forRowAtIndexPath:(NSIndexPath *)indexPath
{
    ReturnableSaleItem *basketItem = _itemDetails[@(ReceiptItemDetailBastketItem)];
    [cell updateDisplayForBasketItem:(MPOSBasketItem *)basketItem];
    //    cell.accessoryType = UITableViewCellAccessoryNone;
    //    cell.selectionStyle = UITableViewCellSelectionStyleNone;
}

-(void)updateTextFieldTableViewCell:(OCGCollectionViewCell *)cell
                  forRowAtIndexPath:(NSIndexPath *)indexPath
{
    cell.detailTextField.delegate = self;
    cell.detailTextField.inputAccessoryView = _firstResponderAccessoryView;
    cell.textLabelWidth = 120;
    cell.detailTextField.textAlignment = NSTextAlignmentRight;
    cell.detailTextField.autocapitalizationType = UITextAutocapitalizationTypeNone;
    cell.detailTextField.autocorrectionType = UITextAutocorrectionTypeNo;
    [OCGKeyboardTypeButtonItem setKeyboardType:kOCGKeyboardTypeNumberPad forTarget:cell.detailTextField];
    cell.tag = kEditableTableCellSkinningTag;
    cell.textLabel.tag = kEditableCellKeySkinningTag;
    cell.detailTextField.tag = kEditableCellValueSkinningTag;
    cell.detailTextField.clearButtonMode = UITextFieldViewModeAlways;
    cell.detailRequirement = OCGCollectionViewCellDetailRequirementRequired;
    
    ReceiptItemDetail itemDetail = [@[_visibleDetails] integerForIndexPath:indexPath];
    
    BOOL textFieldShouldAllowEditing = YES;
    switch (itemDetail) {
        case ReceiptItemDetailItemScanId:
        case ReceiptItemDetailReasonDescription:
        case ReceiptItemDetailOriginalItemScanId:
        case ReceiptItemDetailDumpCodeDescription:
            textFieldShouldAllowEditing = NO;
            break;
        default:
            textFieldShouldAllowEditing = YES;
            break;
    }
    cell.detailTextField.userInteractionEnabled = textFieldShouldAllowEditing;
    
    cell.textLabel.text = [self titleforItemDetail:itemDetail];
    
    switch (itemDetail) {
        case ReceiptItemDetailReasonDescription:
            break;
            
        case ReceiptItemDetailOriginalCustomerCard:
            cell.detailTextField.placeholder = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠀱󠁢󠁱󠁒󠁷󠁁󠁅󠁄󠁈󠀳󠁌󠁑󠀯󠀱󠁃󠁭󠁎󠁩󠁓󠁋󠀹󠁣󠁬󠀷󠁏󠁪󠁙󠁿*/ @"Original Customer", nil);
            cell.detailTextField.inputView = nil;
            cell.detailRequirement = OCGCollectionViewCellDetailRequirementNone;
            break;
            
        case ReceiptItemDetailOriginalTransactionStoreId:
            cell.detailTextField.placeholder = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠀳󠁶󠀹󠁣󠁱󠁡󠀰󠀴󠁤󠁎󠀷󠁍󠁇󠁹󠁏󠁒󠁷󠁢󠁁󠀸󠁌󠁔󠁒󠁲󠁐󠀷󠁣󠁿*/ @"Original store number", nil);
            cell.detailTextField.inputView = nil;
            break;
            
        case ReceiptItemDetailOriginalTransactionTillNumber:
            cell.detailTextField.placeholder = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁬󠁭󠁰󠀸󠁵󠁕󠁥󠁯󠁺󠁹󠁥󠁰󠁷󠁇󠁱󠁊󠁚󠀶󠁆󠁢󠁚󠀰󠁗󠁥󠁹󠁏󠁧󠁿*/ @"Original till or operator #", nil);
            cell.detailTextField.inputView = nil;
            break;
            
        case ReceiptItemDetailOriginalTransactionId:
            cell.detailTextField.placeholder = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁁󠁯󠁍󠀴󠀯󠁭󠁙󠁓󠁅󠁳󠁷󠁷󠁔󠁴󠁦󠁆󠁶󠁨󠀵󠀳󠁘󠁯󠁭󠁏󠁅󠁙󠁳󠁿*/ @"Original transaction #", nil);
            cell.detailTextField.inputView = nil;
            break;
            
        case ReceiptItemDetailOriginalTransactionDate:
            cell.detailTextField.placeholder = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁆󠀲󠁎󠁏󠁐󠁅󠀹󠀸󠁴󠀱󠁥󠁐󠀹󠁦󠁹󠁡󠁕󠁴󠁡󠁥󠁕󠁘󠁊󠁒󠀸󠁨󠁧󠁿*/ @"Original transaction date", nil);
            cell.detailTextField.inputView = _datePicker;
            break;
            
        case ReceiptItemDetailOriginalItemDescription:
            cell.detailTextField.placeholder = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁑󠁙󠀶󠀱󠁰󠁷󠁖󠁌󠁁󠁏󠁹󠁦󠁷󠁲󠁣󠀳󠁋󠀰󠀴󠁲󠁅󠀲󠁑󠁷󠁫󠁖󠀴󠁿*/ @"Item Description", nil);
            cell.detailTextField.inputView = nil;
            cell.detailTextField.keyboardType = UIKeyboardTypeDefault;
            break;
            
        case ReceiptItemDetailOriginalItemScanId:
            cell.detailTextField.clearButtonMode = UITextFieldViewModeNever;
            
        case ReceiptItemDetailItemScanId:
            cell.detailTextField.placeholder = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁐󠁋󠀸󠀱󠁵󠁷󠁐󠀶󠁢󠁗󠀴󠁨󠁥󠁔󠁔󠁏󠁦󠁸󠁂󠁘󠁯󠁤󠀸󠁙󠁚󠁵󠁉󠁿*/ @"Refund Item", nil);
            cell.detailTextField.inputView = nil;
            break;
            
        case ReceiptItemDetailOriginalReferenceId:
            cell.detailTextField.placeholder = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁚󠁦󠁫󠁑󠀰󠁡󠁰󠁪󠁲󠁯󠁓󠁯󠁴󠁔󠁦󠁌󠁑󠁵󠀲󠁶󠁔󠁖󠁺󠁘󠁓󠀷󠁯󠁿*/ @"Original Reference", nil);
            cell.detailTextField.inputView = nil;
            cell.detailTextField.keyboardType = UIKeyboardTypeDefault;
            break;
            
        case ReceiptItemDetailOriginalSalesPerson:
            cell.detailTextField.placeholder = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁁󠀶󠀸󠀲󠁙󠀵󠁅󠁭󠀱󠁑󠀵󠀫󠁃󠁨󠁒󠀲󠁓󠁰󠁦󠁯󠁸󠁡󠁙󠁚󠁍󠁩󠀸󠁿*/ @"Associate ID", nil);
            cell.detailTextField.inputView = nil;
            cell.detailRequirement = OCGCollectionViewCellDetailRequirementNone;
            break;
            
        case ReceiptItemDetailPrice:
            cell.detailTextField.placeholder = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠀵󠁫󠁸󠁢󠁐󠁱󠀫󠁣󠁏󠁣󠁒󠁚󠁇󠁳󠁐󠁹󠁉󠀸󠀯󠁈󠁥󠁏󠁤󠁺󠁔󠁉󠀰󠁿*/ @"Price", nil);
            [OCGKeyboardTypeButtonItem setKeyboardType:kOCGKeyboardTypeNumberPad forTarget:cell.detailTextField];
            break;
            
        case ReceiptItemDetailDumpCodeDescription:
            cell.detailTextField.placeholder = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁕󠀯󠁹󠁬󠁈󠀹󠁮󠁹󠁑󠁐󠁨󠁃󠀷󠁋󠁤󠁩󠀱󠁃󠁨󠁯󠁥󠁅󠁄󠁕󠁄󠁔󠁳󠁿*/ @"Dump code", nil);
            cell.detailTextField.inputView = nil;
            cell.detailTextField.clearButtonMode = UITextFieldViewModeNever;
            break;
            
        default:
            
            break;
    }
    
    if (itemDetail == ReceiptItemDetailOriginalTransactionDate && _itemDetails[@(itemDetail)] != nil) {
        NSDate *date = [_dateFormatter dateFromString:_itemDetails[@(itemDetail)]];
        cell.detailTextField.text = [_displayDateFormatter stringFromDate:date];
    } else if (itemDetail == ReceiptItemDetailPrice) {
        cell.detailTextField.text = [_priceValidator stringFromNumber:_itemDetails[@(itemDetail)]];
    } else {
        cell.detailTextField.text = _itemDetails[@(itemDetail)] ?: @"";
    }
    
    if (itemDetail == ReceiptItemDetailReasonDescription ||
        (itemDetail == ReceiptItemDetailDumpCodeDescription && [BasketController.sharedInstance.dumpCodes count] > 1)) {
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        cell.selectionStyle = UITableViewCellSelectionStyleBlue;
    } else {
        cell.accessoryType = UITableViewCellAccessoryNone;
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    
    [[CRSSkinning currentSkin] applyViewSkin:cell
                             withTagOverride:cell.tag];
}

-(void)updateNavigationItem
{
    if ([self.navigationController.viewControllers[0] isEqual:self])
    {
        UIBarButtonItem *cancelButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁩󠁢󠁺󠁬󠁏󠁶󠁣󠁧󠁫󠁍󠁋󠁧󠁫󠀱󠁶󠁐󠁐󠀹󠁘󠀴󠁘󠁡󠁕󠁭󠁹󠁷󠀰󠁿*/ @"Cancel", nil)
                                                                         style:UIBarButtonItemStyleDone
                                                                        target:self
                                                                        action:@selector(dismissViewController)];
        
        self.navigationItem.leftBarButtonItem = cancelButton;
    }
    
    UIBarButtonItem *submitButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁈󠁳󠁲󠁲󠁌󠀴󠀱󠁍󠁘󠁗󠁐󠁔󠁘󠁏󠁕󠀱󠁚󠁣󠁮󠁏󠁴󠀲󠁖󠁹󠁣󠁄󠁉󠁿*/ @"Submit", nil)
                                                                     style:UIBarButtonItemStyleDone
                                                                    target:self
                                                                    action:@selector(submitButtonTapped)];
    
    submitButton.enabled = [self canSubmitItem];
    
    self.navigationItem.rightBarButtonItem = submitButton;
}

-(void)dismissViewController
{
    if (_type == ReceiptItemViewControllerTypeLinkedReturn)
    {
        [self.navigationController popViewControllerAnimated:YES];
    }
    else
    {
        [self dismissViewControllerAnimated:YES completion:NULL];
    }
}

-(void)submitButtonTapped
{
    NSString *price = [_itemDetails[@(ReceiptItemDetailPrice)] description];
    if (price.length == 0) {
        UIAlertView *alert =
        [[UIAlertView alloc] initWithTitle:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁖󠁔󠁪󠁐󠁍󠁓󠁧󠁢󠀳󠁩󠁯󠁹󠁬󠁮󠁳󠁉󠁸󠀯󠁐󠁅󠁈󠁡󠁥󠁌󠁢󠁪󠀰󠁿*/ @"Warning", nil)
                                   message:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁧󠁖󠁚󠁲󠁗󠁁󠁹󠁥󠀯󠁅󠀴󠀫󠁗󠁏󠁥󠁙󠁵󠁥󠀹󠁇󠀵󠀵󠁩󠁌󠁤󠀰󠁯󠁿*/ @"No price has been entered. The default price of the item will be used.", nil)
                                  delegate:[UIAlertView class]
                         cancelButtonTitle:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁩󠁢󠁺󠁬󠁏󠁶󠁣󠁧󠁫󠁍󠁋󠁧󠁫󠀱󠁶󠁐󠁐󠀹󠁘󠀴󠁘󠁡󠁕󠁭󠁹󠁷󠀰󠁿*/ @"Cancel", nil)
                         otherButtonTitles:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁄󠁴󠁍󠁉󠁣󠀫󠁺󠁔󠁗󠁦󠁹󠁡󠁯󠀷󠁨󠁔󠁩󠀸󠁯󠀫󠁨󠁘󠀹󠁱󠁌󠀶󠀸󠁿*/ @"OK", nil), nil];
        alert.dismissBlock = ^(int buttonIndex) {
            if (buttonIndex == 0) {
                [self submitReturn];
            }
        };
        [alert show];
    } else {
        [self submitReturn];
    }
}

-(BOOL)handleExceptionCode:(enum MShopperExceptionCode)exceptionCode
{
    NSArray *requiredDetails = nil;
    
    switch (exceptionCode) {
        case MShopperItemDescriptionRequiredExceptionCode:
        case MShopperSellItemRequiresOriginalItemCodeExceptionCode:
            requiredDetails = @[
                                @(ReceiptItemDetailOriginalItemScanId),
                                @(ReceiptItemDetailOriginalItemDescription)];
            
            break;
            
        case MShopperItemSerialNumberRequiredExceptionCode:
            requiredDetails = @[
                                @(ReceiptItemDetailItemSerialNumber),
                                ];
            break;
            
        case MShopperItemNotFoundExceptionCode:
            
            if (_type == ReceiptItemViewControllerTypeLinkedReturn)
            {
                requiredDetails = @[
                                    @(ReceiptItemDetailDumpCodeDescription),
                                    ];
                
                ReturnableSaleItem *basketItem = _itemDetails[@(ReceiptItemDetailBastketItem)];
                
                _itemDetails[@(ReceiptItemDetailOriginalItemScanId)] = basketItem.itemID;
                _itemDetails[@(ReceiptItemDetailOriginalItemDescription)] = basketItem.desc;
                if ([BasketController.sharedInstance.dumpCodes count] == 1)
                {
                    DumpCode *dumpCode = [BasketController.sharedInstance.dumpCodes lastObject];
                    _itemDetails[@(ReceiptItemDetailDumpCodeDescription)] = dumpCode.desc ?: dumpCode.itemSellingCode;
                    _itemDetails[@(ReceiptItemDetailItemScanId)] = dumpCode.itemSellingCode;
                }
            }
            break;
            
        default:
            break;
    }
    
    BOOL didAddItemDetail = NO;
    
    for (id itemDetail in requiredDetails)
    {
        if (![_visibleDetails containsObject:itemDetail])
        {
            [_visibleDetails addObject:itemDetail];
            didAddItemDetail = YES;
        }
    }
    return didAddItemDetail;
}

-(BOOL)canSubmitItem
{
    BOOL canSubmitItem = YES;
    
    NSArray *optionalDetails = @[@(ReceiptItemDetailPrice),
                                 @(ReceiptItemDetailOriginalSalesPerson),
                                 @(ReceiptItemDetailOriginalCustomerCard),
                                 @(ReceiptItemDetailBastketItem),
                                 ];
    
    for (id itemDetail in _visibleDetails)
    {
        if (![optionalDetails containsObject:itemDetail])
        {
            if ([_itemDetails[itemDetail] isKindOfClass:[NSNumber class]])
            {
                if ([_itemDetails[itemDetail] doubleValue] == 0)
                {
                    canSubmitItem = NO;
                    break;
                }
            }
            else if ([_itemDetails[itemDetail] length] == 0)
            {
                canSubmitItem = NO;
                break;
            }
        }
    }
    
    return canSubmitItem;
}

-(NSString*)titleforItemDetail:(ReceiptItemDetail)itemDetail
{
    NSString *title = nil;
    
    switch (itemDetail)
    {
        case ReceiptItemDetailReasonDescription:
            title = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁰󠁧󠁙󠁔󠁶󠁉󠁉󠁵󠁳󠁕󠁒󠀳󠁊󠁫󠁅󠁺󠁅󠀶󠀶󠁈󠀹󠁲󠁏󠁫󠁬󠁹󠁣󠁿*/ @"Reason", nil);
            break;
            
        case ReceiptItemDetailOriginalCustomerCard:
            title = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠀱󠁢󠁱󠁒󠁷󠁁󠁅󠁄󠁈󠀳󠁌󠁑󠀯󠀱󠁃󠁭󠁎󠁩󠁓󠁋󠀹󠁣󠁬󠀷󠁏󠁪󠁙󠁿*/ @"Original Customer", nil);
            break;
            
        case ReceiptItemDetailOriginalTransactionStoreId:
            title = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁹󠁣󠁫󠁆󠁑󠁷󠁗󠁋󠁺󠁳󠀳󠁪󠁫󠀸󠀶󠁅󠁃󠁧󠁢󠁶󠁙󠁱󠁄󠁷󠁃󠁥󠀴󠁿*/ @"Store", nil);
            break;
            
        case ReceiptItemDetailOriginalTransactionTillNumber:
            title = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁙󠁖󠁏󠁆󠁌󠁌󠁁󠀫󠁕󠁰󠁯󠁹󠁧󠁤󠁓󠁴󠀴󠁕󠁒󠁬󠁫󠁘󠁎󠁁󠁅󠁹󠁫󠁿*/ @"Till", nil);
            break;
            
        case ReceiptItemDetailOriginalTransactionId:
            title = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁦󠁕󠁗󠁕󠁦󠀯󠁷󠁺󠁓󠁫󠁌󠁔󠁭󠁓󠁉󠀷󠁑󠁭󠁇󠁳󠁣󠀷󠁒󠁅󠁤󠁱󠁫󠁿*/ @"Tx Id", nil);
            break;
            
        case ReceiptItemDetailOriginalTransactionDate:
            title = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠀱󠁏󠁂󠁂󠁑󠁇󠁧󠁑󠁫󠁕󠁐󠀲󠁷󠁊󠁩󠀷󠁮󠁈󠁶󠁈󠁒󠁆󠁈󠁷󠁊󠁺󠀸󠁿*/ @"Date", nil);
            break;
            
        case ReceiptItemDetailItemScanId:
        case ReceiptItemDetailOriginalItemScanId:
            title = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁐󠁋󠀸󠀱󠁵󠁷󠁐󠀶󠁢󠁗󠀴󠁨󠁥󠁔󠁔󠁏󠁦󠁸󠁂󠁘󠁯󠁤󠀸󠁙󠁚󠁵󠁉󠁿*/ @"Refund Item", nil);
            break;
            
        case ReceiptItemDetailOriginalItemDescription:
            title = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁑󠁙󠀶󠀱󠁰󠁷󠁖󠁌󠁁󠁏󠁹󠁦󠁷󠁲󠁣󠀳󠁋󠀰󠀴󠁲󠁅󠀲󠁑󠁷󠁫󠁖󠀴󠁿*/ @"Item Description", nil);
            break;
            
        case ReceiptItemDetailOriginalReferenceId:
            title = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁚󠁦󠁫󠁑󠀰󠁡󠁰󠁪󠁲󠁯󠁓󠁯󠁴󠁔󠁦󠁌󠁑󠁵󠀲󠁶󠁔󠁖󠁺󠁘󠁓󠀷󠁯󠁿*/ @"Original Reference", nil);
            break;
            
        case ReceiptItemDetailOriginalSalesPerson:
            title = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁴󠁷󠁖󠁑󠁎󠁅󠁊󠀱󠁚󠀱󠁨󠁧󠁵󠀫󠁥󠁱󠁡󠁯󠁯󠁗󠁎󠁃󠁄󠀫󠁎󠁁󠁙󠁿*/ @"Sales person", nil);
            break;
            
        case ReceiptItemDetailPrice:
            title = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠀵󠁫󠁸󠁢󠁐󠁱󠀫󠁣󠁏󠁣󠁒󠁚󠁇󠁳󠁐󠁹󠁉󠀸󠀯󠁈󠁥󠁏󠁤󠁺󠁔󠁉󠀰󠁿*/ @"Price", nil);
            break;
            
        case ReceiptItemDetailDumpCodeDescription:
            title = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁕󠀯󠁹󠁬󠁈󠀹󠁮󠁹󠁑󠁐󠁨󠁃󠀷󠁋󠁤󠁩󠀱󠁃󠁨󠁯󠁥󠁅󠁄󠁕󠁄󠁔󠁳󠁿*/ @"Dump code", nil);
            break;
            
        case ReceiptItemDetailItemSerialNumber:
            title = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠀳󠀰󠀫󠁢󠀲󠀷󠀴󠁊󠁭󠁧󠁪󠀯󠁊󠁓󠁈󠁌󠁐󠁹󠁇󠁪󠁤󠁌󠁚󠁳󠁩󠀰󠁅󠁿*/ @"Serial number", nil);
            break;
            
        default:
            title = [@(itemDetail) description];
            
            break;
    }
    return title;
}

- (void) startScan:(ReceiptItemDetail)itemDetail
{
    BarcodeScannerController *controller = [BarcodeScannerController sharedInstance];
    // are we scanning?
    if (!controller.scanning) {
        // no, start scanning
        [controller beginScanningInViewController:self
                                        forReason:[self titleforItemDetail:itemDetail]
                                   withButtonSize:CGSizeZero
                                     withAddTitle:nil
                                         mustScan:NO
                              alphaNumericAllowed:YES
                                        withBlock:^(NSString *barcode, ScannerEntryMethod entryMethod, SMBarcodeType barcodeType, BOOL *continueScanning) {
                                            // debug assertions
                                            NSAssert(barcode != nil,
                                                     @"The reference to the scanned barcode has not been initialized.");
                                            NSAssert([[barcode stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length] > 0,
                                                     @"The returned barcode does not contain any characters.");
                                            // we only want to scan the one barcode
                                            *continueScanning = NO;
                                            // grab the barcode we scanned
                                            [self handleBarcode:barcode itemDetail:itemDetail];
                                        }
                                       terminated:^{
                                           //                                           if (_receiptBarcode != nil) {
                                           //                                               // process the barcode
                                           //                                               [self handleReceiptBarcode:_receiptBarcode];
                                           //                                               _itemDetails[@"itemScanId"] = nil;
                                           //                                           } else {
                                           //                                               [self cancelButtonTapped];
                                           //                                           }
                                       }
                                            error:^(NSString *errorDesc) {
                                                if (errorDesc) {
                                                    // display an alert
                                                    UIAlertView *alert =
                                                    [[UIAlertView alloc] initWithTitle:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁯󠀫󠁋󠁆󠁢󠁚󠁱󠁩󠁈󠁕󠀲󠀸󠁫󠁲󠁧󠁦󠀲󠁴󠁘󠁣󠁈󠁎󠀶󠀲󠁯󠀶󠁳󠁿*/ @"Scanning Error", nil)
                                                                               message:errorDesc
                                                                              delegate:nil
                                                                     cancelButtonTitle:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁄󠁴󠁍󠁉󠁣󠀫󠁺󠁔󠁗󠁦󠁹󠁡󠁯󠀷󠁨󠁔󠁩󠀸󠁯󠀫󠁨󠁘󠀹󠁱󠁌󠀶󠀸󠁿*/ @"OK", nil)
                                                                     otherButtonTitles:nil];
                                                    [alert show];
                                                }
                                            }];
    }
    else
    {
        [controller stopScanning];
    }
}

-(void)handleBarcode:(NSString *)barcode itemDetail:(ReceiptItemDetail)itemDetail
{
    ocg_async_background_overlay(^{
        
        _itemDetails[@(itemDetail)] =
        [barcode stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        
        if (itemDetail == ReceiptItemDetailItemScanId)
        {
            NSString *locationKey = [CRSLocationController getLocationKey];
            
            [RESTController.sharedInstance findItemWithScanID:barcode
                                                      storeID:locationKey
                                                     complete:^(Item *item, NSError *error, ServerError *serverError)
             {
                 _scannedItem = item;
                 
                 dispatch_async(dispatch_get_main_queue(), ^{
                     
                     if (error != nil || serverError != nil)
                     {
                         [_itemDetails removeObjectForKey:@(itemDetail)];
                         
                         ServerErrorMessage *message = [serverError.messages lastObject];
                         
                         if ([message.code integerValue] == MShopperItemNotFoundExceptionCode && [BasketController.sharedInstance.dumpCodes count] > 0)
                         {
                             UIAlertView *alert =
                             [[UIAlertView alloc] initWithTitle:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁚󠀳󠁖󠀶󠁙󠁶󠀱󠁂󠁎󠁍󠀸󠁎󠁢󠀴󠁋󠁳󠁚󠁮󠁏󠁦󠁆󠁥󠁶󠁖󠀲󠁉󠁉󠁿*/ @"Item not found", nil)
                                                        message:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁺󠀫󠁒󠁷󠁙󠁐󠁅󠁵󠀴󠀰󠁦󠀫󠁙󠁗󠁺󠁆󠁋󠀱󠁢󠁇󠁨󠁰󠀰󠁁󠁭󠀴󠁑󠁿*/ @"Would you like to substitute a dump code?", nil)
                                                       delegate:[UIAlertView class]
                                              cancelButtonTitle:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁩󠁢󠁺󠁬󠁏󠁶󠁣󠁧󠁫󠁍󠁋󠁧󠁫󠀱󠁶󠁐󠁐󠀹󠁘󠀴󠁘󠁡󠁕󠁭󠁹󠁷󠀰󠁿*/ @"Cancel", nil)
                                              otherButtonTitles:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁄󠁴󠁍󠁉󠁣󠀫󠁺󠁔󠁗󠁦󠁹󠁡󠁯󠀷󠁨󠁔󠁩󠀸󠁯󠀫󠁨󠁘󠀹󠁱󠁌󠀶󠀸󠁿*/ @"OK", nil), nil];
                             
                             alert.dismissBlock = ^(int buttonIndex) {
                                 if (buttonIndex == 0)
                                 {
                                     [_itemDetails removeObjectForKey:@(ReceiptItemDetailPrice)];
                                     _itemDetails[@(ReceiptItemDetailOriginalItemScanId)] = barcode;
                                     if ([BasketController.sharedInstance.dumpCodes count] == 1)
                                     {
                                         DumpCode *dumpCode = [BasketController.sharedInstance.dumpCodes lastObject];
                                         _itemDetails[@(ReceiptItemDetailDumpCodeDescription)] = dumpCode.desc ?: dumpCode.itemSellingCode;
                                         _itemDetails[@(ReceiptItemDetailItemScanId)] = dumpCode.itemSellingCode;
                                     }
                                     [_visibleDetails replaceObjectAtIndex:[_visibleDetails indexOfObject:@(ReceiptItemDetailItemScanId)] withObject:@(ReceiptItemDetailDumpCodeDescription)];
                                     [self handleExceptionCode:MShopperSellItemRequiresOriginalItemCodeExceptionCode];
                                     [self reloadData];
                                 }
                             };
                             [alert show];
                         }
                         else
                         {
                             [[ServerErrorHandler sharedInstance] handleServerError:serverError
                                                                     transportError:error
                                                                        withContext:nil
                                                                       dismissBlock:nil
                                                                        repeatBlock:nil];
                         }
                         
                     }
                     else
                     {
                         if (_scannedItem.price != nil) {
                             _priceValidator.currencyCode = BasketController.sharedInstance.currencyCode;
                             _itemDetails[@(ReceiptItemDetailPrice)] = [_priceValidator numberFromString:_scannedItem.price];
                         } else {
                             _itemDetails[@(ReceiptItemDetailPrice)] = @"";
                         }
                     }
                     
                     [self reloadData];
                 });
             }];
        }
        else
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                [self reloadData];
            });
        }
    });
}

@end
