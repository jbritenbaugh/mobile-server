//
//  OCGEFTManagerCurrentPayment.m
//  mPOS
//
//  Created by Antonio Strijdom on 25/02/2015.
//  Copyright (c) 2015 Clarity. All rights reserved.
//

#import "OCGEFTManagerCurrentPayment.h"
#import "OCGEFTManager.h"
#import "CRSLocationController.h"
#import "TillSetupController.h"
#import "NSString+NSString_OCGExtensions.h"

@interface OCGEFTManagerCurrentPayment () {
    dispatch_semaphore_t generateTenderIdLock;
}
@end

@implementation OCGEFTManagerCurrentPayment

#pragma mark - Private

- (NSString *) generateTenderId
{
    NSInteger result = NSIntegerMin;
    
    // get the current value
    result = [[NSUserDefaults standardUserDefaults] integerForKey:@"OCGEFTManagerCurrentPaymentTenderId"];
    if (0 == dispatch_semaphore_wait(generateTenderIdLock, DISPATCH_TIME_FOREVER)) {
        // increment
        result++;
        [[NSUserDefaults standardUserDefaults] setInteger:result
                                                   forKey:@"OCGEFTManagerCurrentPaymentTenderId"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        dispatch_semaphore_signal(generateTenderIdLock);
    }
    
    return [NSString stringWithFormat:@"%i", result];
}

- (NSString *) padRight:(NSString *)source
             withString:(NSString *)padding
               toLength:(NSUInteger)newLength
{
    NSUInteger length = source.length;
    if (length < newLength) {
        NSUInteger paddingLength = newLength - length;
        NSString *paddingString = [@"" stringByPaddingToLength:paddingLength
                                                    withString:padding
                                               startingAtIndex:0];
        return [paddingString stringByAppendingString:source];
    } else {
        return source;
    }
}

#pragma mark - Properties

@synthesize uniqueMerchantReference = _uniqueMerchantReference;
- (NSString *) uniqueMerchantReference
{
    // if we don't have a unique merchant reference
    if (_uniqueMerchantReference == nil) {
        // generate one
        //        [Location ID (padded to 4 digits)][Terminal ID (padded to 4 digits)][Basket ID (padded to 4 digits)][Business date (in YYYYMMDD format)][Basket tender ID (padded to 4 digits)]
        // AFMS 2015-03-09 format is now YYMMDDSSSSTTTTTTTTLLLL
        // Date, store ID (4 digits), terminal ID (4 digits), transaction ID (4 digits), ATLAS counter (starts at 1 when POS starts, up to 9999).
        NSString *locationId = [self padRight:[CRSLocationController getLocationKey]
                                   withString:@"0"
                                     toLength:4];
        NSString *terminalId = [self padRight:[TillSetupController getTillNumber]
                                   withString:@"0"
                                     toLength:4];
        NSString *basketId = [self padRight:self.basket.transactionNumber
                                 withString:@"0"
                                   toLength:4];
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        formatter.dateFormat = @"yyMMdd";
        NSString *businessDate = [formatter stringFromDate:[NSDate date]];
        NSString *tenderId = [self padRight:[self generateTenderId]
                                 withString:@"0"
                                   toLength:4];
        _uniqueMerchantReference = [NSString stringWithFormat:@"%@%@%@%@%@",
                                    businessDate,
                                    locationId,
                                    terminalId,
                                    basketId,
                                    tenderId];
        DebugLog(@"EFT unique merchant reference is %@", _uniqueMerchantReference);
        // convert to hex
        Device *eftDevice = [[BasketController sharedInstance] deviceForSettingWithName:kOCGEFTManagerConfiguredPaymentProviderKey];
        NSString *compactReferenceSetting = eftDevice.propertyDict[@"HexEncodePaymentReference"];
        if ([compactReferenceSetting.lowercaseString isEqualToString:@"true"]) {
            NSString *hexString = [_uniqueMerchantReference hexFromDecimalString];
            _uniqueMerchantReference = hexString;
            DebugLog(@"Compacted : %@", hexString);
        }
    }
    
    return _uniqueMerchantReference;
}

#pragma mark - Init

- (instancetype) init
{
    self = [super init];
    if (self) {
        _uniqueMerchantReference = nil;
        generateTenderIdLock = dispatch_semaphore_create(1);
    }
    
    return self;
}

#pragma mark - NSCoding

- (id) initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        self.transactionType = [aDecoder decodeIntegerForKey:@"transactionType"];
        self.customerEmail = [aDecoder decodeObjectForKey:@"customerEmail"];
        self.paymentStatus = [aDecoder decodeIntegerForKey:@"paymentStatus"];
        self.paymentCompleteReason = [aDecoder decodeIntegerForKey:@"paymentCompleteReason"];
        self.signatureConfirmed = [aDecoder decodeBoolForKey:@"signatureConfirmed"];
        _uniqueMerchantReference = [aDecoder decodeObjectForKey:@"uniqueMerchantReference"];
        if ([aDecoder containsValueForKey:@"transaction"]) {
            self.transaction = [aDecoder decodeObjectForKey:@"transaction"];
        }
        self.originalMerchantReference = [aDecoder decodeObjectForKey:@"originalMerchantReference"];
        self.originalBasketItemId = [aDecoder decodeObjectForKey:@"originalBasketItemId"];
        self.currencyCode = [aDecoder decodeObjectForKey:@"originalCurrencyCode"];
    }
    
    return self;
}

- (void) encodeWithCoder:(NSCoder *)aCoder
{
    [super encodeWithCoder:aCoder];
    [aCoder encodeInteger:self.transactionType forKey:@"transactionType"];
    [aCoder encodeObject:self.customerEmail forKey:@"customerEmail"];
    [aCoder encodeInteger:self.paymentStatus forKey:@"paymentStatus"];
    [aCoder encodeInteger:self.paymentCompleteReason forKey:@"paymentCompleteReason"];
    [aCoder encodeBool:self.signatureConfirmed forKey:@"signatureConfirmed"];
    [aCoder encodeObject:self.uniqueMerchantReference forKey:@"uniqueMerchantReference"];
    Protocol *codingProtocol = @protocol(NSCoding);
    if ([[self.transaction class] conformsToProtocol:codingProtocol]) {
        [aCoder encodeObject:self.transaction forKey:@"transaction"];
    }
    [aCoder encodeObject:self.originalMerchantReference forKey:@"originalMerchantReference"];
    [aCoder encodeObject:self.originalBasketItemId forKey:@"originalBasketItemId"];
    [aCoder encodeObject:self.currencyCode forKey:@"originalCurrencyCode"];
}

#pragma mark - Methods

- (NSString *) localizedStringForCurrentStatus
{
    NSString *result = nil;
    
    switch (self.paymentStatus) {
        case kEFTPaymentStatusInitializing:
            result = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠀶󠁢󠁢󠁑󠀵󠁺󠁒󠁦󠁂󠁷󠁵󠀷󠀰󠀯󠀱󠁬󠁣󠀫󠁯󠀲󠀵󠁆󠁘󠁮󠀰󠀷󠁣󠁿*/ @"Initialising", nil);
            break;
        case kEFTPaymentStatusProcessing:
            result = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁍󠁅󠁎󠀹󠁦󠁃󠀱󠁄󠀫󠀳󠁗󠁒󠁡󠁺󠀹󠁉󠁙󠁔󠁆󠁷󠁱󠁸󠀲󠁴󠀳󠀶󠁙󠁿*/ @"Processing", nil);
            break;
        case kEFTPaymentStatusSignatureRequired:
            result = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁨󠁭󠁖󠁕󠀸󠁴󠀸󠁩󠁫󠁙󠁏󠁵󠁉󠀫󠀸󠁎󠁷󠁮󠁐󠁱󠁨󠁪󠁥󠁰󠀳󠁭󠀸󠁿*/ @"Signature required", nil);
            break;
        case kEFTPaymentStatusFinishing:
            result = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁁󠁍󠁒󠁕󠀳󠁒󠁢󠁇󠀱󠁗󠁍󠁹󠁶󠁖󠀫󠁈󠁚󠁈󠁸󠁋󠁇󠁵󠁴󠁨󠁧󠀰󠁳󠁿*/ @"Finishing", nil);
            break;
        case kEFTPaymentStatusComplete:
            result = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁓󠁃󠁢󠀵󠁲󠁃󠁢󠀸󠁹󠁊󠀹󠁅󠁩󠁑󠁆󠀫󠁎󠁥󠁪󠀵󠁎󠁋󠁔󠁆󠁬󠁃󠀸󠁿*/ @"Complete", nil);
            break;
    }
    
    return result;
}

@end
