//
//  POSInputView.h
//  mPOS
//
//  Created by John Scott on 15/10/2014.
//  Copyright (c) 2014 Clarity. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, POSInputViewLayout) {
    POSInputViewNumberPadLayout,
    POSInputViewPOSLayout,
};

@interface POSInputView : UIInputView

+(void)setLayout:(POSInputViewLayout)layout;

+(NSString*)layoutTitle;

-(instancetype)initWithKeyboardType:(UIKeyboardType)keyboardType;

@end
