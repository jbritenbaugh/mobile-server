//
//  TestModeController.h
//  mPOS
//
//  Created by Antonio Strijdom on 25/07/2012.
//  Copyright (c) 2012 Clarity. All rights reserved.
//

#import <Foundation/Foundation.h>


typedef NS_ENUM(NSInteger, ServerMode)
{
    ServerModeOnline,
    ServerModeOffline,
};

/** @brief The online server address config key. */
extern NSString * const kTestModeControllerLiveServerConfigKey;
/** @brief The offline server address config key. */
extern NSString * const kTestModeControllerOfflineServerConfigKey;
/** @brief The server mode config key. */
extern NSString * const kTestModeControllerServerModeConfigKey;
/** @brief Notification sent when the server mode switches between online and offline. */
extern NSString * const kTestModeControllerServerModeDidChangeNotification;

/** @file TestModeController.h */

@interface TestModeController : NSObject

/** @brief Gets the configured server address depending on on/offline mode.
 *  @return The server address or nil.
 */
+ (NSString *) getServerAddress;

/** @brief Gets the configured online server address.
 *  @return The server address or nil.
 */
+ (NSString *) getOnlineServerAddress;

/** @brief Gets the configured offline server address.
 *  @return The server address or nil.
 */
+ (NSString *) getOfflineServerAddress;

/** @brief Gets the configured server mode.
 *  @return The server mode.
 */
+ (ServerMode) serverMode;

/** @brief Sets the configured server mode.
 *  @param The server mode.
 */
+ (void) setServerMode:(ServerMode)serverMode;

@end
