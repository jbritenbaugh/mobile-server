//
//  MPOSEditableTableFieldsDataSource.m
//  mPOS
//
//  Created by Antonio Strijdom on 24/10/2012.
//  Copyright (c) 2012 Clarity. All rights reserved.
//

#import "ServerErrorHandler.h"
#import "BasketController.h"
#import "TestModeController.h"
#import "TillSetupController.h"
#import <objc/runtime.h>

@implementation ServerErrorHandler

static char SHARED_INSTANCE_IDENTIFER;

+ (void) setSharedInstance:(ServerErrorHandler *) sharedInstance
{
    objc_setAssociatedObject(self, &SHARED_INSTANCE_IDENTIFER, sharedInstance, OBJC_ASSOCIATION_RETAIN);
}

+ (ServerErrorHandler *) sharedInstance
{
    __block id sharedInstance = objc_getAssociatedObject(self, &SHARED_INSTANCE_IDENTIFER);
    if (sharedInstance != nil) {
        return sharedInstance;
    }
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[self alloc] init];
        self.sharedInstance = sharedInstance;
    });
    
    return sharedInstance;
}

+ (void) topMostControllerComplete:(void (^)(UIViewController *viewController))complete
{
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.25 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        UIViewController *topController = [UIApplication sharedApplication].keyWindow.rootViewController;
        
        while (topController.presentedViewController) {
            topController = topController.presentedViewController;
            if ([topController isKindOfClass:[UINavigationController class]]) {
                topController = [(UINavigationController *)topController visibleViewController];
            }
        }
        
        complete(topController);
    });
}

#pragma mark Error Management

/*
 LocalizedStringWithDefaultNil is the same as NSLocalizedString but returns nil if the key is not found.
*/

NSString *LocalizedStringWithDefaultNil(NSString *key)
{
    NSString *localizedString = NSLocalizedStringWithDefaultValue(key, @"MShopperExceptions", [NSBundle mainBundle], (NSString*) [NSNull null], nil);
    
    if ([localizedString isEqual:[NSNull null]])
    {
        return nil;
    }
    else
    {
        return localizedString;
    }
}

- (NSString *) translateServerErrorMessage:(ServerErrorMessage *)message withContext:(NSString *)context
{
    NSAssert(message != nil, @"The server error message object reference has not been initialized.");
    NSAssert(message.code != 0, @"The server error message code has an unexpected value.");
    
    NSString *translatedMessage = nil;
    
    // for POS error messages
    if ([message isKindOfClass:[POSErrorMessage class]]) {
        // just use the message text provided
        translatedMessage = message.value;
    } else {
        NSString *exceptionName = MShopperExceptionNameFromCode([message.code integerValue]);
    
        if (exceptionName != nil
#if DEBUG
            && [message.code integerValue] != MShopperUnexpectedErrorExceptionCode
#endif
            )
        {
            NSString *exceptionNameForEntityId = exceptionName;//[exceptionName stringByAppendingFormat:@"_%@", context];
            
            translatedMessage = LocalizedStringWithDefaultNil(exceptionNameForEntityId);
            
            if (translatedMessage == nil)
            {
                translatedMessage = LocalizedStringWithDefaultNil(exceptionName);
            }
        }
    }
    
    if (translatedMessage == nil)
    {
        ErrorLog(@"We've received an server error message that has not yet been translated. The default error message (from the server) will be used instead.\n  code: %@\n  value: %@\n  client: %@", message.code, message.value, context);
        
        NSString *messageTemplate = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁓󠁘󠀶󠁉󠁓󠁣󠁙󠁰󠁲󠁢󠁑󠁶󠁃󠁆󠁌󠁴󠁢󠁗󠁴󠁕󠁬󠁯󠁉󠁘󠁏󠁱󠁍󠁿*/ @"Unexpected error %@: %@", nil);
        translatedMessage = [NSString stringWithFormat:messageTemplate, message.code, message.value];
    }
    
#if DEBUG
    translatedMessage = [translatedMessage stringByAppendingFormat:@" (%@)", [context length] ? context : NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁘󠁐󠁄󠀯󠁚󠁣󠁒󠁹󠁌󠁚󠁁󠁮󠁵󠁵󠁗󠁓󠁱󠁭󠁷󠁣󠀱󠀫󠁄󠁑󠁖󠁉󠀰󠁿*/ @"no context", nil)];
#endif
    
    return translatedMessage;
}

/**
 *  @brief Determines if the operation can be repeated after an error.
 *  @param serverError The marshalled HTTP body error message received from the web service.
 *  @param transportError The low-level error message from the networking layer.
 *  @return YES, if the operation can be repeated, NO otherwise.
 */
- (BOOL) canRetryOperationAfterMPOSError:(ServerError *)serverError transportError:(NSError *)error
{
    if ((serverError != nil) || (error != nil)) {
        // Request Timed Out
        return YES;
    }
    
    return NO;
}

- (NSString *) buildServerErrorMessage:(ServerError *)serverError
                        transportError:(NSError *)error
                           withContext:(NSString *)context
{
    // give priority to the messages delivered by the web service
    
    NSMutableArray *errorTexts = [NSMutableArray array];
    
    for (ServerErrorMessage *message in serverError.messages)
    {
        NSString *errorText = [self translateServerErrorMessage:message withContext:context];
        if (errorText.length > 0)
        {
            [errorTexts addObject:errorText];
        }
    }
    
    if (errorTexts.count == 0)
    {
        NSString *errorText = [error localizedDescription];
        if (errorText.length > 0)
        {
            [errorTexts addObject:errorText];
        }
    }
    
    if (errorTexts.count == 0)
    {
        NSString *errorText = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁇󠁷󠁎󠁋󠁪󠁴󠁭󠁊󠁶󠁉󠁭󠁐󠁑󠁤󠀵󠁒󠁡󠀱󠁡󠁑󠁧󠁕󠀷󠀵󠁱󠁦󠁍󠁿*/ @"Unexpected Empty Error", nil);
        if (errorText.length > 0)
        {
            [errorTexts addObject:errorText];
        }
    }
    
    return [errorTexts componentsJoinedByString:@"\r\n"];
}

//- (void) handleServerError:(ServerError *)mposError
//              dismissBlock:(void (^)())dismissedBlock
//               repeatBlock:(void (^)())repeatBlock
//{
//    [self handleServerError:mposError
//             transportError:mposError.transportError
//                withContext:mposError.context
//               dismissBlock:dismissedBlock
//                repeatBlock:repeatBlock];
//}


- (void) handleServerError:(ServerError *)serverError
            transportError:(NSError*)error
               withContext:(NSString *)context
              dismissBlock:(void (^)())dismissedBlock
               repeatBlock:(void (^)())repeatBlock
{
    NSString *messageText = @"";
    BOOL isServerDown = NO;
    BOOL invalidatesBasket = NO;
    
    /*
     After any tranport error we assume the server is toast.
     */
    
    if (error != nil)
    {
        isServerDown = YES;
    }
    
    if ((serverError != nil)
        && (serverError.messages != nil)
        && ([serverError.messages count] > 0)) {
        for (ServerErrorMessage *message in serverError.messages) {
            if (!invalidatesBasket && [message isKindOfClass:[POSErrorMessage class]]) {
                invalidatesBasket = ((POSErrorMessage *)message).invalidatesBasket;
            }
            if ([message.code integerValue] == MShopperServiceUnavailableExceptionCode)
            {
                isServerDown = YES;
            }
        }
    }
    
    // build the message text
    messageText = [self buildServerErrorMessage:serverError
                                 transportError:error
                                    withContext:context];
    
    NSString *tillNumber = nil;
    if ([TestModeController serverMode] == ServerModeOnline)
    {
        tillNumber = [TillSetupController getOfflineTillNumber];
    }
    else if ([TestModeController serverMode] == ServerModeOffline)
    {
        tillNumber = [TillSetupController getOnlineTillNumber];
    }
    
    if (isServerDown && [tillNumber length] > 0)
    {
        NSString *stayTitle;
        NSString *goTitle;
        if ([TestModeController serverMode] == ServerModeOnline)
        {
            stayTitle = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁋󠁁󠀸󠁏󠁬󠁇󠀹󠁈󠀰󠁌󠁷󠁴󠁢󠀲󠁑󠁫󠁆󠀫󠁆󠁦󠁶󠁊󠁳󠁋󠀯󠁸󠁫󠁿*/ @"Stay online", nil);
            goTitle = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠀫󠁇󠁓󠁚󠀸󠁵󠁳󠀴󠁑󠁱󠁇󠁮󠁱󠁓󠁖󠀯󠁶󠀹󠁖󠁐󠁫󠁦󠀸󠁢󠁎󠁅󠁅󠁿*/ @"Go offline", nil);
        }
        else if ([TestModeController serverMode] == ServerModeOffline)
        {
            stayTitle = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁐󠁇󠁐󠁭󠁡󠁨󠁌󠁇󠀶󠁷󠁅󠁺󠁢󠁗󠁑󠁗󠀯󠁴󠁁󠁓󠁹󠁢󠀷󠁓󠁯󠁬󠁅󠁿*/ @"Stay offline", nil);
            goTitle = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠀸󠁸󠀯󠁫󠁹󠁒󠁳󠀴󠁆󠁖󠀴󠁏󠁌󠁪󠁖󠁥󠁉󠁈󠁯󠀳󠀷󠁤󠀳󠁈󠁸󠀵󠁁󠁿*/ @"Go online", nil);
        }
        
        UIAlertController *alert =
        [UIAlertController alertControllerWithTitle:@""
                                            message:messageText
                                     preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *stayAction =
        [UIAlertAction actionWithTitle:stayTitle
                                 style:UIAlertActionStyleCancel
                               handler:^(UIAlertAction *action) {
                                   if (dismissedBlock != nil) {
                                       dismissedBlock();
                                   }
                                   if (![[BasketController sharedInstance] hasConfiguration]) {
                                       [[BasketController sharedInstance] refreshConfiguration:YES];
                                   } else {
                                       [[BasketController sharedInstance] getActiveBasket];
                                   }
                               }];
        [alert addAction:stayAction];
        UIAlertAction *goAction =
        [UIAlertAction actionWithTitle:goTitle
                                 style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction *action) {
                                   if (dismissedBlock != nil) {
                                       dismissedBlock();
                                   }
                                   if ([TestModeController serverMode] == ServerModeOnline) {
                                       [TestModeController setServerMode:ServerModeOffline];
                                   } else if ([TestModeController serverMode] == ServerModeOffline) {
                                       [TestModeController setServerMode:ServerModeOnline];
                                   }
                               }];
        [alert addAction:goAction];
        
        [ServerErrorHandler topMostControllerComplete:^(UIViewController *viewController) {
            if (![viewController isKindOfClass:[UIAlertController class]]) {
                [viewController presentViewController:alert
                                             animated:YES
                                           completion:nil];
            }
        }];
    }
    else
    {
        UIAlertController *alert =
        [UIAlertController alertControllerWithTitle:@""
                                            message:messageText
                                     preferredStyle:UIAlertControllerStyleAlert];
        
        // check if the user can repeat the operation after this error message.
        BOOL canRepeat = ((repeatBlock != nil) && ([self canRetryOperationAfterMPOSError:serverError
                                                                          transportError:error]));
        NSString *dismissButtonTitle = nil;
        if (!canRepeat) {
            dismissButtonTitle = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁄󠁴󠁍󠁉󠁣󠀫󠁺󠁔󠁗󠁦󠁹󠁡󠁯󠀷󠁨󠁔󠁩󠀸󠁯󠀫󠁨󠁘󠀹󠁱󠁌󠀶󠀸󠁿*/ @"OK", nil);
        } else if (dismissedBlock) {
            dismissButtonTitle = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁡󠁩󠁡󠁍󠁰󠀸󠁷󠁣󠁹󠁩󠁕󠀷󠁪󠁶󠀱󠀸󠁐󠁖󠀵󠁥󠀳󠁫󠁕󠁚󠁄󠁶󠁙󠁿*/ @"Dismiss", nil);
        } else {
            dismissButtonTitle = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁩󠁢󠁺󠁬󠁏󠁶󠁣󠁧󠁫󠁍󠁋󠁧󠁫󠀱󠁶󠁐󠁐󠀹󠁘󠀴󠁘󠁡󠁕󠁭󠁹󠁷󠀰󠁿*/ @"Cancel", nil);
        }
        UIAlertAction *dismissAction =
        [UIAlertAction actionWithTitle:dismissButtonTitle
                                 style:UIAlertActionStyleCancel
                               handler:^(UIAlertAction *action) {
                                   if (invalidatesBasket) {
                                       [BasketController.sharedInstance getActiveBasket];
                                   }
                                   if (dismissedBlock != nil)
                                       dismissedBlock();
                               }];
        [alert addAction:dismissAction];
        
        if (canRepeat) {
            UIAlertAction *otherAction =
            [UIAlertAction actionWithTitle:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁓󠁬󠁹󠀱󠀫󠁘󠁳󠁎󠁏󠁩󠁇󠁰󠀵󠁄󠁄󠁚󠁉󠁪󠁥󠁯󠁪󠀷󠁌󠁭󠁢󠁺󠁳󠁿*/ @"Retry", nil)
                                     style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction *action) {
                                       if (repeatBlock != nil)
                                           repeatBlock();
                                   }];
            [alert addAction:otherAction];
        }
        
        // show the error alert view
        [ServerErrorHandler topMostControllerComplete:^(UIViewController *viewController) {
            [viewController presentViewController:alert
                                         animated:YES
                                       completion:nil];
        }];
    }
}

@end
