//
//  TillSetupController.m
//  mPOS
//
//  Created by Antonio Strijdom on 14/09/2012.
//  Copyright (c) 2012 Clarity. All rights reserved.
//

#import "TillSetupController.h"
#import "UIDevice+OCGIdentifier.h"
#import "CRSLocationController.h"
#import "TestModeController.h"
#import "RESTController.h"
#import "OCGSettingsController.h"

@implementation TillSetupController

#pragma mark - Private

NSString * const kTillSetupOnlineTillNumberConfigKey = @"config_tillnumber";
NSString * const kTillSetupOfflineTillNumberConfigKey = @"config_offline_tillnumber";

#pragma mark - Methods

+ (void) setDeviceProfileTillNumber:(NSString *)value
                        locationKey:(NSString *)locationKey
                     serviceBaseURL:(NSString*)serviceBaseURL
                           complete:(void (^) (NSError *error, ServerError *serverError))complete
{
    
    // update this device's profile
    DeviceProfile *profile = [[DeviceProfile alloc] init];
    profile.storeId = locationKey;
    // make sure there's a store configured
    if (profile.storeId) {
        profile.tillNumber = value;
        profile.deviceId = [[UIDevice currentDevice] uniqueDeviceIdentifier];
        // send to server
        ocg_async_background_overlay(^{
            [RESTController.sharedInstance updateDeviceWithDevice:profile
                                                           server:serviceBaseURL
                                                         complete:^(NSError *error, ServerError *serverError)
             {
                 if ((error == nil) && (serverError == nil)) {
                     dispatch_async(dispatch_get_main_queue(), ^{
                         complete(nil, nil);
                     });
                 } else {
                     ErrorLog(@"Error updating device profile: %@\n%@\n%@",
                              profile,
                              error,
                              serverError);
                     dispatch_async(dispatch_get_main_queue(), ^{
                         complete(error, serverError);
                     });
                 }
             }];
            
        });
    } else {
        complete(nil, nil);
    }
}

+ (NSString *) getTillNumber
{
    NSString *tillNumer = nil;
    switch ([TestModeController serverMode])
    {
        case ServerModeOnline:
            tillNumer = [self getOnlineTillNumber];
            break;
            
        case ServerModeOffline:
            tillNumer = [self getOfflineTillNumber];
            break;
            
        default:
            break;
    }
    return tillNumer;
}

+ (NSString *) getOnlineTillNumber
{
    // get the configured location
    NSString *result = [[OCGSettingsController sharedInstance] valueForSettingWithName:kTillSetupOnlineTillNumberConfigKey];
    return result;
}

+(void)setOnlineTillNumber:(NSString*)value
{
    // update the setting
    [[OCGSettingsController sharedInstance] setValue:value
                                  forSettingWithName:kTillSetupOnlineTillNumberConfigKey];
}

+ (NSString *) getOfflineTillNumber
{
    // get the configured location
    NSString *result = [[OCGSettingsController sharedInstance] valueForSettingWithName:kTillSetupOfflineTillNumberConfigKey];
    return result;
}

+(void)setOfflineTillNumber:(NSString*)value
{
    // update the setting
    [[OCGSettingsController sharedInstance] setValue:value
                                  forSettingWithName:kTillSetupOfflineTillNumberConfigKey];
}

+ (void) resetTillNumber
{
    switch ([TestModeController serverMode])
    {
        case ServerModeOnline:
            [self resetOnlineTillNumber];
            break;
            
        case ServerModeOffline:
            [self resetOfflineTillNumber];
            break;
            
        default:
            break;
    }
}

+ (void) resetOnlineTillNumber
{
    [[OCGSettingsController sharedInstance] removeSettingWithName:kTillSetupOnlineTillNumberConfigKey];
}

+ (void) resetOfflineTillNumber
{
    [[OCGSettingsController sharedInstance] removeSettingWithName:kTillSetupOfflineTillNumberConfigKey];
}

@end
