//
//  BasketItemDetailViewController.h
//  mPOS
//
//  Created by John Scott on 27/01/2014.
//  Copyright (c) 2014 Clarity. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BasketItemDetailViewController : UICollectionViewController

@property (nonatomic, strong) MPOSBasketItem* basketItem;

@end
