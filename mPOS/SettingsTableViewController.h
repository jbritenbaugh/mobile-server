//
//  SettingsTableViewController.h
//  mPOS
//
//  Created by Antonio Strijdom on 25/07/2012.
//  Copyright (c) 2012 Clarity. All rights reserved.
//

#import <UIKit/UIKit.h>

@class SettingsTableViewController;

/** @brief Protocol for the delegate of the settings table view controller.
 */
@protocol SettingsTableViewControllerDelegate <NSObject>
@optional

/** @brief Called whenever the settings table view controller should be dismissed.
 *  @param controller A reference to the settings table view controller.
 */
- (void) dismissSettingsTableViewController:(SettingsTableViewController*)controller;

@end

@interface SettingsTableViewController : UICollectionViewController

/** @brief Reference to the delegate object.
 */
@property (strong, nonatomic) id<SettingsTableViewControllerDelegate> delegate;

-(void)checkView;
-(void)resetAdmin;

@end
