//
//  MoreOptionsTableViewController.h
//  mPOS
//
//  Created by Antonio Strijdom on 21/11/2012.
//  Copyright (c) 2012 Clarity. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "FormViewController.h"

@class SoftKey;
@class SoftKeyContext;

/** @file MoreOptionsTableViewController.h */

@protocol MoreOptionsTableViewControllerDelegate;

/** @brief Displays more menu options.
 */
@interface MoreOptionsTableViewController : FormViewController

/** @property delegate
 *  @brief The delegate of this menu.
 */
@property (nonatomic, weak) id<MoreOptionsTableViewControllerDelegate> delegate;

/** @property softKey
 *  @brief The menu key to use as a datasource.
 */
@property (strong, nonatomic) SoftKey *softKey;

@end

/** @brief Delegate protocol for the more options table view controller.
 */
@protocol MoreOptionsTableViewControllerDelegate <NSObject>
@required
/** @brief Asks the delegate if the menu option should be enabled or not.
 *  @param key The key to get information for.
 *  @return YES if the option is valid, otherwise NO.
 */
- (BOOL) isMenuOptionActive:(SoftKey*)key;
/** @brief Asks the delegate is the menu option should be locked or not.
 *  @param key The key to get information for.
 *  @return YES if the key is locked, otherwise NO.
 */
- (BOOL) isMenuOptionLocked:(SoftKey*)key;
/** @brief Informs the delegate that the user selected a menu option.
 *  @param moreVC The view controller.
 *  @param optionKey The option the user selected.
 */
- (void) moreOptionsController:(MoreOptionsTableViewController *)moreVC
               didSelectOption:(SoftKey*)optionKey;
@end
