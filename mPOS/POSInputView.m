//
//  POSInputView.m
//  mPOS
//
//  Created by John Scott on 15/10/2014.
//  Copyright (c) 2014 Clarity. All rights reserved.
//

#import "POSInputView.h"

@interface POSInputViewInternal : UIView

@property (nonatomic, assign) UIKeyboardType keyboardType;

@end

@implementation POSInputView
{
    POSInputViewInternal *_posInternal;
}

static POSInputViewLayout _layout;

+(void)load
{
    _layout = POSInputViewNumberPadLayout;
}

+(void)setLayout:(POSInputViewLayout)layout
{
    _layout = layout;
}

+(NSString*)layoutTitle
{
    NSString *layoutTitle = nil;
    switch (_layout) {
        case POSInputViewNumberPadLayout:
            layoutTitle = @"\uF002";
            break;
            
        case POSInputViewPOSLayout:
            layoutTitle = @"\uF007";
            break;
            
        default:
            break;
    }
    return layoutTitle;
}

CGFloat _POSInputViewKeyboardHeight()
{
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
    {
        return 216;
    }
    else
    {
        return 264;
    }
}

-(instancetype)initWithKeyboardType:(UIKeyboardType)keyboardType
{
    self = [super initWithFrame:CGRectMake(0, 0, 200, _POSInputViewKeyboardHeight())];
    
    if (self)
    {
        self.userInteractionEnabled = YES;
        
        _posInternal = [[POSInputViewInternal alloc] initWithFrame:self.bounds];
        _posInternal.keyboardType = keyboardType;
        _posInternal.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        _posInternal.clearsContextBeforeDrawing = YES;
        _posInternal.backgroundColor = [UIColor clearColor];
        
        [self addSubview:_posInternal];
    }
    return self;
}

-(instancetype)init
{
    return [self initWithKeyboardType:UIKeyboardTypeNumberPad];
}

- (BOOL)isAccessibilityElement
{
    return NO;
}

@end

@interface POSInputViewKey : UIAccessibilityElement

@property (nonatomic, assign) BOOL invertBackground;
@property (nonatomic, assign) CGRect frame;

@end

@implementation POSInputViewInternal
{
    NSMutableArray *_accessibilityElements;
    POSInputViewKey *_highlightedElement;
}

-(instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    
    if (self)
    {
        self.userInteractionEnabled = YES;
    }
    return self;
}

- (BOOL)isAccessibilityElement
{
    return NO;
}

-(UIKeyboardAppearance)keyboardAppearance
{
    id responder = [self nextResponder];
    
    while (responder)
    {
        if ([responder respondsToSelector:@selector(keyboardAppearance)])
        {
            return [responder keyboardAppearance];
        }
        
        responder = [responder nextResponder];
    }
    return UIKeyboardAppearanceLight;
}

- (void)drawRect:(CGRect)rect
{
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    UIKeyboardAppearance keyboardAppearance = [self keyboardAppearance];
    
    if (keyboardAppearance == UIKeyboardAppearanceDark)
    {
        CGContextSetRGBFillColor(context, 1, 1, 1, 0.1);
    }
    else
    {
        CGContextSetRGBFillColor(context, 0, 0, 0, 0.1);
    }
    
    CGContextFillRect(context, self.bounds);
    
    CGFloat lineWidth = 1./self.window.screen.scale;
    
    CGContextSetLineWidth(context, lineWidth);
    
    for (POSInputViewKey *element in self.accessibilityElements)
    {
        CGContextAddRect(context, element.frame);
        BOOL isElementSelected = _highlightedElement == element;
        if (element.invertBackground)
        {
            isElementSelected = !isElementSelected;
        }
        
        if (isElementSelected)
        {
            CGContextSetRGBFillColor(context, 1, 1, 1, 0.0);
        }
        else
        {
            if (keyboardAppearance == UIKeyboardAppearanceDark)
            {
                CGContextSetRGBFillColor(context, 1, 1, 1, 0.23);
            }
            else
            {
                CGContextSetRGBFillColor(context, 1, 1, 1, 0.95);
            }
        }
        
        CGContextSetAllowsAntialiasing(context, NO);
        
        if (keyboardAppearance == UIKeyboardAppearanceDark)
        {
            CGContextSetRGBStrokeColor(context, 0.27, 0.27, 0.27, 1.0);
        }
        else
        {
            CGContextSetRGBStrokeColor(context, 0.72, 0.73, 0.75, 1.0);
        }


        CGContextDrawPath(context, kCGPathFillStroke);
        
        NSMutableDictionary *textAttributes = [NSMutableDictionary dictionary];
        if (keyboardAppearance == UIKeyboardAppearanceDark)
        {
            textAttributes[NSForegroundColorAttributeName] = [UIColor whiteColor];
        }
        else
        {
            textAttributes[NSForegroundColorAttributeName] = [UIColor blackColor];
        }
        
        if ([element.accessibilityLabel isEqual:@"\u232B"])
        {
            textAttributes[NSFontAttributeName] = [UIFont fontWithName:@"OmnicoGroup-Regular" size:28];
        }
        else
        {
            textAttributes[NSFontAttributeName] = [UIFont fontWithName:@"HelveticaNeue" size:28];
        }
        CGSize textSize = [element.accessibilityLabel sizeWithAttributes:textAttributes];
        CGPoint textCenter = CGPointMake(CGRectGetMidX(element.frame), CGRectGetMidY(element.frame));
        textCenter.x -= textSize.width/2.;
        textCenter.y -= textSize.height/2.;
        
//        textCenter.y += -5;

        
        
        CGContextSetAllowsAntialiasing(context, YES);
        [element.accessibilityLabel drawAtPoint:textCenter withAttributes:textAttributes];
    }

}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    UITouch *touch = touches.anyObject;
    
    id highlightedElement = [self keyAtPoint:[touch locationInView:self]];
    
    if (![_highlightedElement isEqual:highlightedElement])
    {
        _highlightedElement = highlightedElement;
        [self setNeedsDisplay];
    }
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
    UITouch *touch = touches.anyObject;
    
    id highlightedElement = [self keyAtPoint:[touch locationInView:self]];
    
    if (![_highlightedElement isEqual:highlightedElement])
    {
        _highlightedElement = highlightedElement;
        [self setNeedsDisplay];
    }
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    if (_highlightedElement)
    {
        [self activateKey:_highlightedElement];
    }
    _highlightedElement = nil;
    [self setNeedsDisplay];
}

- (void)touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event
{
    _highlightedElement = nil;
    [self setNeedsDisplay];
}


- (BOOL)activateKey:(POSInputViewKey *)element
{
    if (element.accessibilityElementIsFocused || _highlightedElement == element)
    {
        id responder = [self nextResponder];
        
        while (responder)
        {
            if ([responder conformsToProtocol:@protocol(UITextInput)])
            {
                [[responder inputDelegate] textWillChange:responder];
                UITextRange *range = [responder selectedTextRange];
                NSString *replacementText = @"";
                if ([element.accessibilityLabel isEqual:@"\u232B"])
                {
                    if (range.isEmpty)
                    {
                        range = [responder textRangeFromPosition:[responder positionFromPosition:range.end offset:-1] toPosition:range.end];
                    }
                }
                else
                {
                    replacementText = element.accessibilityLabel;
                }
                
                BOOL shouldChangeText = YES;
                
                if ([responder respondsToSelector:@selector(shouldChangeTextInRange:replacementText:)])
                {
                    shouldChangeText = [responder shouldChangeTextInRange:range replacementText:replacementText];
                }
                
                if ([responder isKindOfClass:[UITextField class]]
                    && [[responder delegate] respondsToSelector:@selector(textField:shouldChangeCharactersInRange:replacementString:)])
                {
                    NSUInteger startOffset = [responder offsetFromPosition:[responder beginningOfDocument] toPosition:range.start];
                    NSUInteger endOffset = [responder offsetFromPosition:[responder beginningOfDocument] toPosition:range.end];
                    
                    
                    shouldChangeText = [(id <UITextFieldDelegate>)[responder delegate] textField:responder
                                                                   shouldChangeCharactersInRange:NSMakeRange(startOffset, endOffset - startOffset)
                                                                               replacementString:replacementText];
                }
                
                if (shouldChangeText)
                {
                    [responder replaceRange:range withText:replacementText];
                }
                [[responder inputDelegate] textDidChange:responder];
                return YES;
            }
            else if ([responder conformsToProtocol:@protocol(UIKeyInput)])
            {
                if ([element.accessibilityLabel isEqual:@"\u232B"])
                {
                    [responder deleteBackward];
                }
                else
                {
                    [responder insertText:element.accessibilityLabel];
                }
                return YES;
            }
            
            responder = [responder nextResponder];
        }
    }
    return NO;
}

-(id)keyAtPoint:(CGPoint)point
{
    for (POSInputViewKey *element in self.accessibilityElements)
    {
        if (CGRectContainsPoint(element.frame, point))
        {
            if (element.accessibilityTraits & UIAccessibilityTraitNotEnabled)
            {
                return nil;
            }
            else
            {
                return element;
            }
        }
    }
    return nil;
}

-(NSArray *)accessibilityElements
{
    if (!_accessibilityElements)
    {
        NSArray *layout = nil;
        
        NSString *extraKey = @"";
        if (_keyboardType == UIKeyboardTypeDecimalPad)
        {
            extraKey = @".";
        }
        
        if (_layout == POSInputViewPOSLayout)
        {
            layout = @[
                       @[@"7", @"8", @"9"],
                       @[@"4", @"5", @"6"],
                       @[@"1", @"2", @"3"],
                       @[extraKey, @"0", @"\u232B"],
                       ];
        }
        else if (_layout == POSInputViewNumberPadLayout)
        {
            layout = @[
                       @[@"1", @"2", @"3"],
                       @[@"4", @"5", @"6"],
                       @[@"7", @"8", @"9"],
                       @[extraKey, @"0", @"\u232B"],
                       ];
        }
        else
        {
            NSAssert(NO, @"%d is not a valid POS input layout", _layout);
        }
        
        _accessibilityElements = [NSMutableArray array];
        
        CGFloat lineWidth = 1./self.window.screen.scale;
        
        CGRect bounds = CGRectInset(self.bounds, -lineWidth, -lineWidth);
                
        NSInteger yCount = layout.count;
        
        CGFloat rowHeight = CGRectGetHeight(bounds) / yCount;
        
        for (NSInteger yIndex=0; yIndex<yCount; yIndex++)
        {
            NSInteger xCount = [layout[yIndex] count];
            
            for (NSInteger xIndex=0; xIndex<xCount; xIndex++)
            {
                NSString *label = layout[yIndex][xIndex];
                
                if (label.length)
                {
                    POSInputViewKey *element = [[POSInputViewKey alloc] initWithAccessibilityContainer:self];
                    element.frame = CGRectMake(CGRectGetMinX(bounds) + CGRectGetWidth(bounds) * xIndex / xCount, CGRectGetMinY(bounds) + rowHeight * yIndex, CGRectGetWidth(bounds) / xCount, rowHeight);
                    
                    element.accessibilityLabel = label;
                    
                    element.accessibilityTraits = UIAccessibilityTraitKeyboardKey;
                    
                    if ([element.accessibilityLabel isEqual:@"\u232B"] || [element.accessibilityLabel isEqual:@"."])
                    {
                        element.invertBackground = YES;
                    }
                    
                    [_accessibilityElements addObject:element];
                }
            }
        }
    }
    return _accessibilityElements;
}

- (NSInteger)accessibilityElementCount
{
    return self.accessibilityElements.count;
}

-(id)accessibilityElementAtIndex:(NSInteger)index
{
    return self.accessibilityElements[index];
}

- (NSInteger)indexOfAccessibilityElement:(id)element
{
    return [self.accessibilityElements indexOfObject:element];
}

- (BOOL)accessibilityActivate
{
    for (POSInputViewKey *element in self.accessibilityElements)
    {
        if (element.accessibilityElementIsFocused)
        {
            return [self activateKey:element];
        }
    }
    
    return NO;
}

@end

@implementation POSInputViewKey

-(CGRect)accessibilityFrame
{
    return UIAccessibilityConvertFrameToScreenCoordinates(_frame, self.accessibilityContainer);
}

@end
