//
//  PasswordResetViewController.h
//  mPOS
//
//  Created by John Scott on 02/09/2013.
//  Copyright (c) 2013 Clarity. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol PasswordResetViewControllerDelegate;

@interface PasswordResetViewController : UITableViewController

- (id)initWithOperatorID:(NSString*)operatorID;

@property (weak, nonatomic) id <PasswordResetViewControllerDelegate> delegate;

@end

@protocol PasswordResetViewControllerDelegate <NSObject>

-(void)passwordResetViewController:(PasswordResetViewController *)passwordResetViewController password:(NSString*)password;

@end