//
//  CustomerSearchViewController.m
//  mPOS
//
//  Created by Antonio Strijdom on 25/09/2013.
//  Copyright (c) 2013 Clarity. All rights reserved.
//

#import "CustomerSearchViewController.h"
#import "CRSSkinning.h"
#import "CustomerController.h"
#import "OCGProgressOverlay.h"
#import "ServerErrorHandler.h"
#import "BasketController.h"
#import "OCGFirstResponderAccessoryView.h"

@interface CustomerSearchViewController () <UISearchBarDelegate, OCGFirstResponderAccessoryViewDelegate, CustomerViewControllerDelegate>
@property (strong, nonatomic) UISearchBar *searchBar;
@property (strong, nonatomic) OCGFirstResponderAccessoryView *accessoryView;
- (void) performSearchForCustomer:(NSString *)searchText;
- (void) managedObjectContextDidSave:(NSNotification *)notification;
- (void) presentCustomer:(MPOSCustomer *)customer;
- (IBAction) addCustomer:(id)sender;
- (IBAction) dismissViewController:(id)sender;
@end

@implementation CustomerSearchViewController

#pragma mark - Private

/** @brief Performs the server roundtrip to search for the specified customer.
 *  @param searchText The search text to identify the requested customer.
 */
- (void) performSearchForCustomer:(NSString *)searchText
{
    if ((searchText != nil) && ([searchText length] >= 3)) {
        [CustomerController.sharedInstance getCustomerDetailByEmail:searchText];
    } else {
        UIAlertView *alert =
        [[UIAlertView alloc] initWithTitle:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁣󠁤󠁹󠁧󠁳󠁓󠁫󠁫󠁰󠁓󠁧󠁕󠁏󠁶󠁅󠁡󠁹󠁩󠁒󠁴󠁧󠁰󠁅󠀶󠁪󠁭󠁣󠁿*/ @"Customer Search", nil)
                                   message:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁹󠁴󠁸󠁡󠁯󠁓󠀯󠁚󠁰󠁅󠁓󠁉󠁁󠁘󠀰󠁕󠁕󠁐󠁥󠁖󠁑󠁎󠁐󠁦󠁄󠁌󠁫󠁿*/ @"You must enter at least three characters in order to perform a search", nil)
                                  delegate:nil
                         cancelButtonTitle:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁄󠁴󠁍󠁉󠁣󠀫󠁺󠁔󠁗󠁦󠁹󠁡󠁯󠀷󠁨󠁔󠁩󠀸󠁯󠀫󠁨󠁘󠀹󠁱󠁌󠀶󠀸󠁿*/ @"OK", nil)
                         otherButtonTitles:nil];
        [alert show];
    }
}

- (void) managedObjectContextDidSave:(NSNotification *)notification
{
    CustomerController *controller = [CustomerController sharedInstance];
    
    // update the progress spinner state
    if (controller.customerSearch.result == nil) {
        [OCGProgressOverlay show];
    } else {
        [OCGProgressOverlay hide];
        // if only one record came back, just display it
        if (controller.currentCustomers.count == 1) {
            MPOSCustomer *customer = controller.currentCustomers[0];
            if (customer != nil) {
                [self presentCustomer:customer];
            }
        } else {
            [self.tableView reloadData];
        }
    }
    
    // check for search errors
    if (controller.customerSearch.error) {
        // handle the error
        ServerError *error = controller.customerSearch.error;
        ServerErrorHandler *serverErrorHandler = [[ServerErrorHandler alloc] init];
        [serverErrorHandler handleServerError:error
                               transportError:error.transportError
                                  withContext:error.context
                                 dismissBlock:^{
                                     // TFS60374 check for user not signed on errors
                                     BOOL isLoggedOff = NO;
                                     for (ServerErrorMessage *message in ((ServerError *)error).messages)
                                     {
                                         if (message.codeValue == MShopperOperatorNotSignedOnExceptionCode) {
                                             isLoggedOff = YES;
                                             break;
                                         }
                                     }
                                     // clear the error
                                     if (error != nil) {
                                         // clear out any search results
                                         [[CustomerController sharedInstance] clearCustomerSearch];
                                     }
                                     
                                     if (isLoggedOff) {
                                         // dismiss the view controller
                                         [self dismissViewControllerAnimated:YES
                                                                  completion:^{
                                                                      [[BasketController sharedInstance] hasLoggedOut];
                                                                  }];
                                     } else {
                                         [self dismissViewController:nil];
                                     }
                                 }
                                  repeatBlock:nil];
    }
}

- (void) presentCustomer:(MPOSCustomer *)customer
{
    self.parentViewController.navigationItem.backBarButtonItem =
    [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁗󠀫󠀹󠁗󠁊󠁈󠁅󠀰󠁮󠁸󠀰󠁆󠁺󠀷󠁨󠀲󠁈󠁢󠁍󠁚󠁏󠁰󠁈󠁸󠁴󠁇󠁉󠁿*/ @"Customer", nil)
                                     style:UIBarButtonItemStylePlain
                                    target:nil
                                    action:nil];
    
    CustomerViewController *customerVC = [[CustomerViewController alloc] init];
    customerVC.customer = customer;
    customerVC.delegate = self;
    customerVC.presentedFromSearch = YES;
//    self.title = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁗󠀫󠀹󠁗󠁊󠁈󠁅󠀰󠁮󠁸󠀰󠁆󠁺󠀷󠁨󠀲󠁈󠁢󠁍󠁚󠁏󠁰󠁈󠁸󠁴󠁇󠁉󠁿*/ @"Customer", nil);
    
    [self.navigationController pushViewController:customerVC
                                         animated:YES];
    [CustomerController.sharedInstance filloutAllValuesForCustomer:customer];
}

#pragma mark - UIViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:UITableViewStyleGrouped];
    if (self) {
    }
    return self;
}

- (void) viewDidLoad
{
    [super viewDidLoad];
    
    // MPOS-322 clear the last search
    [[CustomerController sharedInstance] clearCustomerSearch];
    
    // create the search bar
    self.searchBar = [[UISearchBar alloc] init];
    self.searchBar.tag = kSearchBarSkinningTag;
    self.searchBar.keyboardType = UIKeyboardTypeASCIICapable;
    self.searchBar.delegate = self;
    self.searchBar.frame = CGRectMake(0.0, 0.0, self.tableView.frame.size.width, 44.0f);
    self.searchBar.showsCancelButton = YES;
    
    // create the keyboard accessory
    self.accessoryView = [[OCGFirstResponderAccessoryView alloc] init];
    self.accessoryView.delegate = self;
    self.accessoryView.frame = CGRectMake(0.0, 0.0, self.tableView.frame.size.width, 44.0f);
    self.searchBar.inputAccessoryView = self.accessoryView;
    
    // skin
    self.tableView.tag = kTableSkinningTag;
}

- (void) viewWillDisappear:(BOOL)animated
{
    self.navigationItem.title = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁮󠁰󠁎󠁩󠁧󠁑󠁒󠁭󠁗󠁗󠁊󠁵󠁗󠁏󠁏󠀯󠁱󠁃󠁘󠁪󠀲󠁗󠁘󠁣󠁈󠁲󠁫󠁿*/ @"Search", nil);
    [super viewWillDisappear:animated];
}

- (void) viewDidDisappear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
    [super viewDidDisappear:animated];
}

- (void) viewWillAppear:(BOOL)animated
{
    self.title = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁣󠁤󠁹󠁧󠁳󠁓󠁫󠁫󠁰󠁓󠁧󠁕󠁏󠁶󠁅󠁡󠁹󠁩󠁒󠁴󠁧󠁰󠁅󠀶󠁪󠁭󠁣󠁿*/ @"Customer Search", nil);

    // add the dismiss button
    UIBarButtonItem *dismissButton =
    [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁡󠁩󠁡󠁍󠁰󠀸󠁷󠁣󠁹󠁩󠁕󠀷󠁪󠁶󠀱󠀸󠁐󠁖󠀵󠁥󠀳󠁫󠁕󠁚󠁄󠁶󠁙󠁿*/ @"Dismiss", nil)
                                     style:UIBarButtonItemStylePlain
                                    target:self
                                    action:@selector(dismissViewController:)];
    self.navigationItem.leftBarButtonItem = dismissButton;
    
    // add the add customer button
    self.addButton =
    [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠀷󠁊󠁉󠁎󠁴󠁎󠁖󠁰󠁧󠁨󠁡󠁵󠁙󠁣󠁧󠀳󠀹󠁌󠁊󠁉󠁸󠁨󠁬󠀰󠀹󠀲󠁅󠁿*/ @"Create", nil)
                                     style:UIBarButtonItemStyleDone
                                    target:self
                                    action:@selector(addCustomer:)];

    [[CRSSkinning currentSkin] applyViewSkin:self];
    
    self.navigationItem.title = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁣󠁤󠁹󠁧󠁳󠁓󠁫󠁫󠁰󠁓󠁧󠁕󠁏󠁶󠁅󠁡󠁹󠁩󠁒󠁴󠁧󠁰󠁅󠀶󠁪󠁭󠁣󠁿*/ @"Customer Search", nil);
    self.searchBar.showsCancelButton = NO;
    // display the search bar
    if (self.allowSearch) {
        self.tableView.tableHeaderView = self.searchBar;
        NSOrderedSet *customers = [[CustomerController sharedInstance] currentCustomers];
        if ((customers == nil) || (customers.count == 0)) {
#if DEBUG
            self.searchBar.text = @"a@b.com";
#else
            self.searchBar.text = nil;
#endif
            [self.searchBar becomeFirstResponder];
            
            // make sure the search bar is visible
            [self.tableView scrollRectToVisible:CGRectMake(0, 0, 1, 1)
                                       animated:NO];
        }
    } else {
        self.tableView.tableHeaderView = nil;
    }
    [self.tableView reloadData];
    
    [super viewWillAppear:animated];
}

- (void) viewDidAppear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(managedObjectContextDidSave:)
                                                 name:kBasketControllerKickedNotification
                                               object:nil];
    [super viewDidAppear:animated];
}

#pragma mark - Table view data source

- (NSInteger) numberOfSectionsInTableView:(UITableView *)tableView
{
    NSInteger result = 0;
    
    MPOSCustomerSearch *search = [[CustomerController sharedInstance] customerSearch];
    if ((search != nil) && (search.resultValue)) {
        result = 1;
    }
    
    return result;
}

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSInteger result = 0;
    
    NSOrderedSet *customers = [[CustomerController sharedInstance] currentCustomers];
    if (customers != nil) {
        result = customers.count;
        // show a single row if no records found
        if (result == 0) {
            result = 1;
        }
    }
    
    return result;
}

- (UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                      reuseIdentifier:CellIdentifier];
    }
    
    NSOrderedSet *customers = [[CustomerController sharedInstance] currentCustomers];
    if (customers != nil) {
        if (customers.count == 0) {
            cell.textLabel.text = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁌󠀱󠁊󠁓󠀰󠁋󠁏󠁰󠁇󠁘󠁩󠁹󠀶󠀲󠀯󠁗󠁗󠁅󠀴󠁮󠁫󠁒󠁉󠁆󠁂󠁘󠁙󠁿*/ @"No customers found", nil);
            cell.accessoryType = UITableViewCellAccessoryNone;
        } else {
            MPOSCustomer *customer = customers[indexPath.row];
            if (customer != nil) {
                cell.textLabel.text = [NSString stringWithFormat:@"%@ %@ %@, %@",
                                       customer.salutation == nil ? @"" : customer.salutation,
                                       customer.firstName == nil ? @"" : customer.firstName,
                                       customer.lastName == nil ? @"" : customer.lastName,
                                       customer.address.postcode == nil ? @"" : customer.address.postcode];
                cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            }
        }
    }
    
    cell.tag = kTableCellSkinningTag;
    cell.textLabel.tag = kTableCellTextSkinningTag;
    
    return cell;
}

- (void) tableView:(UITableView *)tableView
   willDisplayCell:(UITableViewCell *)cell
 forRowAtIndexPath:(NSIndexPath *)indexPath
{
    [[CRSSkinning currentSkin] applyCellSkin:cell
                                    forStyle:self.tableView.style];
}

- (UIView *) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if (section == 0 && [tableView numberOfSections] == 1)
    {
        return [[UIView alloc] init];
    }
    else
    {
        return nil;
    }
}

- (CGFloat) tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (section == 0 && [tableView numberOfSections] == 1)
    {
        return 0.001;
    }
    else
    {
        return 0;
    }
}

#pragma mark - Table view delegate

- (NSIndexPath *) tableView:(UITableView *)tableView
   willSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSIndexPath *result = indexPath;
    
    NSOrderedSet *customers = [[CustomerController sharedInstance] currentCustomers];
    if ((customers == nil) || (customers.count == 0)) {
        result = nil;
    }
    
    return result;
}

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSOrderedSet *customers = [[CustomerController sharedInstance] currentCustomers];
    if (customers != nil) {
        if (customers.count > 0) {
            MPOSCustomer *customer = customers[indexPath.row];
            if (customer != nil) {
                [self presentCustomer:customer];
            }
        }
    }
}

#pragma mark UISearchBarDelegate methods

- (void) searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    [self performSearchForCustomer:searchBar.text];
    [searchBar resignFirstResponder];
}

- (BOOL) searchBar:(UISearchBar *)searchBar shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    BOOL result = YES;
    
    NSCharacterSet *disallowedCharecters = [NSCharacterSet characterSetWithCharactersInString:@"\\%"];
    NSRange disallowedCharectersRange = [text rangeOfCharacterFromSet:disallowedCharecters
                                                              options:NSCaseInsensitiveSearch];
    if (disallowedCharectersRange.location != NSNotFound)
    {
        result = NO;
    }
    
    return result;
}

#pragma mark - OCGFirstResponderAccessoryViewDelegate

- (UIView *) OCGFirstResponderAccessoryView:(OCGFirstResponderAccessoryView *)firstResponderAccessoryView
              firstResponderViewForPosition:(id)position
{
    // just return the search bar
    return self.searchBar;
}

#pragma mark - CustomerViewControllerDelegate

- (void) customerAssignedController:(CustomerViewController *)customerController
                             adding:(BOOL)adding
{
    // dismiss
    if (self.delegate != nil) {
        if ([self.delegate respondsToSelector:@selector(dismissCustomerSearchViewController:)]) {
            [self.delegate dismissCustomerSearchViewController:(id)self];
        }
    }
}

- (void) dismissCustomerSearchViewController:(CustomerViewController *)customerController
{
    [customerController dismissViewControllerAnimated:YES
                                           completion:NULL];
}

#pragma mark - Actions

- (IBAction) addCustomer:(id)sender
{
    CustomerViewController *customerVC = [[CustomerViewController alloc] init];
    customerVC.presentedFromSearch = NO;
    customerVC.customer = [CustomerController.sharedInstance addCustomer];
    customerVC.delegate = self;
    customerVC.editing = YES;
    customerVC.modalPresentationStyle = UIModalPresentationFormSheet;
    
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:customerVC];
    navigationController.modalPresentationStyle = UIModalPresentationFormSheet;
    [self.parentViewController presentViewController:navigationController
                                            animated:YES
                                          completion:NULL];
}

- (IBAction) dismissViewController:(id)sender
{
    if (self.delegate != nil) {
        if ([self.delegate respondsToSelector: @selector(dismissCustomerSearchViewController:)]) {
            [self.delegate dismissCustomerSearchViewController:(id)self];
        }
    }
}

@end
