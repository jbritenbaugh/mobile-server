//
//  BasketViewController.m
//  mPOS
//
//  Created by Meik Schuetz on 09/08/2012.
//  Copyright (c) 2012 Clarity. All rights reserved.
//
#import "BasketViewController.h"
#import "BasketController.h"
#import "CRSSkinning.h"
#import "BarcodeScannerController.h"
#import "ServerErrorHandler.h"
#import "OCGPeripheralManager.h"
#import "ItemDetailViewController.h"
#import "UIAlertView+OCGBlockAdditions.h"
#import "ReturnOptionsTableViewController.h"
#import "OCGSplitViewController.h"
#import "POSFuncController.h"
#import "BasketEditViewController.h"
#import "ContextViewController.h"
#import "OCGSelectViewController.h"
#import "NSArray+OCGIndexPath.h"
#import "OCGContextCollectionViewDataSource.h"
#import "NSArray+OCGExtensions.h"
#import "OCGKeyboardTypeButtonItem.h"

typedef NS_OPTIONS(NSInteger, RequiredParameter) {
    RequiredParameterNone    = 0,
    RequiredParameterBarcode = 1 << 0,
    RequiredParameterPrice   = 1 << 1,
    RequiredParameterMeasure = 1 << 2,
};

typedef void (^DumpCodeCompleteBlockType)(NSInteger buttonIndex);

@interface BasketViewController () <UITextFieldDelegate, UIAlertViewDelegate>
@property (strong, nonatomic) ServerErrorHandler *errorHandler;
@property (atomic) NSArray* syncStateTransitions;
@property (strong, nonatomic) NSMutableArray* menuKeys;
@property (strong, nonatomic) DumpCodeCompleteBlockType dumpCodeCompleteBlock;
@property (strong, nonatomic) DumpCode *selectedDumpCode;
@property (assign, nonatomic) BOOL returning;
- (void) displayDumpCodePrompt:(DumpCodeCompleteBlockType)complete;
- (UIAlertView *) buildItemDescriptionAlert;
- (void) handleActiveBasketErrorNotification:(NSNotification *)notification;
- (void) handleBasketChangeNotification:(NSNotification *)notification;
- (void) handleBasketItemChangeNotification:(NSNotification *)notification;
- (void) handleToggleTrainingModeErrorNotification:(NSNotification *)notification;
- (void) handleProductSearchCompleteNotification:(NSNotification *)note;
- (void) handleProductSearchErrorNotification:(NSNotification *)note;
@end

@implementation BasketViewController

#pragma mark - BasketController Notifications

- (void) handleActiveBasketErrorNotification:(NSNotification *)notification
{
    // get the error
    ServerError *error = notification.userInfo[kBasketControllerActiveBasketErrorKey];
    
    NSAssert(error != nil, @"The reference to the error object has not been specified.");
    NSAssert([error isKindOfClass:[ServerError class]], @"The specified error object is inherited from an unexpected BasketController.sharedInstance class.");
    
    // hide the activity view
        
    BOOL isBasketDead = NO;
    BOOL isLoggedOut = NO;
    BOOL isDeviceNotFound = NO;
    
    for (ServerErrorMessage *message in error.messages) {
        if (message.codeValue == MShopperInvalidBasketStatusExceptionCode) {
            [[BasketController sharedInstance] resetBasketToServer];
        } else if (message.codeValue == MShopperSaleBasketNotFoundExceptionCode) {
            [[BasketController sharedInstance] resetBasketToServer];
        } else if (message.codeValue == MShopperOperatorNotSignedOnExceptionCode) {
            isLoggedOut = YES;
        } else if (message.codeValue == MShopperDeviceNotFoundExceptionCode) {
            isDeviceNotFound = YES;
        }
    }
    
    [self.errorHandler handleServerError:error
                          transportError:error.transportError
                             withContext:error.context
                            dismissBlock:^{
                                if (isDeviceNotFound) {
                                    [[BasketController sharedInstance] deviceNotFound];
                                } else if (isLoggedOut) {
                                    [[BasketController sharedInstance] hasLoggedOut];
                                } else if (isBasketDead) {
                                    [[BasketController sharedInstance] resetBasketToServer];
                                }
                            }
                             repeatBlock: isBasketDead || isLoggedOut || isDeviceNotFound ? nil : ^{
                                 [[BasketController sharedInstance] getActiveBasket];
                             }];
}

-(void) handleBasketControllerPaymentChangeNotification:(NSNotification *)notification
{
    [self handleBasketChangeNotification:notification];
}

- (void) handleBasketChangeNotification:(NSNotification *)notification
{
    [self reloadTableDataAndScroll:[notification.name isEqualToString:kBasketControllerBasketCheckedOutChangeNotification]||
                                   [notification.name isEqualToString:kBasketControllerBasketChangeNotification]];
    
    if ([BasketController.sharedInstance.basket.status isEqualToString:@"COMPLETE"] &&
        [BasketController.sharedInstance.basket.resolution isEqualToString:@"EXPIRED"])
    {
        UIAlertView *alertView =
        [[UIAlertView alloc] initWithTitle:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁗󠀶󠁳󠁋󠁗󠁡󠁄󠁅󠁚󠁶󠁐󠁃󠀷󠁎󠁫󠁲󠁡󠁡󠁵󠁡󠁂󠁐󠀯󠁁󠀯󠁡󠁕󠁿*/ @"Basket expired", nil)
                                   message:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁺󠁔󠁳󠁚󠁢󠁅󠁣󠁲󠁑󠁔󠁕󠁉󠁧󠁄󠁎󠀯󠀷󠁅󠀸󠀯󠁦󠁦󠁉󠁢󠁉󠁲󠁍󠁿*/ @"This basket has expired", nil)
                                  delegate:[UIAlertView class]
                         cancelButtonTitle:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁄󠁴󠁍󠁉󠁣󠀫󠁺󠁔󠁗󠁦󠁹󠁡󠁯󠀷󠁨󠁔󠁩󠀸󠁯󠀫󠁨󠁘󠀹󠁱󠁌󠀶󠀸󠁿*/ @"OK", nil)
                         otherButtonTitles:nil];
        
        alertView.cancelBlock = ^
        {
            [BasketController.sharedInstance resetBasketToServer];
        };
        
        [alertView show];
    }
}

- (void) handleBasketItemChangeNotification:(NSNotification *)notification
{
    [self reloadTableDataAndScroll:NO];
}

- (UIAlertView *) buildItemDescriptionAlert
{
    UIAlertView *alert =
    [[UIAlertView alloc] initWithTitle:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠀲󠁹󠁧󠁪󠁗󠁊󠁌󠁅󠁲󠁲󠁡󠁣󠁌󠁒󠀯󠁱󠁬󠁊󠁶󠁉󠁬󠁣󠁖󠀰󠁕󠁂󠁯󠁿*/ @"Description Required", nil)
                               message:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁨󠀳󠀰󠀰󠁬󠀵󠀰󠁩󠁗󠁏󠁶󠁈󠁖󠁑󠁓󠁦󠁊󠀲󠁦󠁲󠁫󠁵󠁅󠁙󠁘󠁍󠁑󠁿*/ @"Please specify a description for this item", nil)
                              delegate:self
                     cancelButtonTitle:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁩󠁢󠁺󠁬󠁏󠁶󠁣󠁧󠁫󠁍󠁋󠁧󠁫󠀱󠁶󠁐󠁐󠀹󠁘󠀴󠁘󠁡󠁕󠁭󠁹󠁷󠀰󠁿*/ @"Cancel", nil)
                     otherButtonTitles:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁖󠁒󠁨󠁭󠁪󠁳󠀶󠁏󠁴󠀱󠁣󠁮󠁩󠁨󠁦󠁑󠁑󠁚󠀵󠁗󠁁󠁖󠁋󠁺󠁰󠁴󠁙󠁿*/ @"Enter description", nil), nil];
    alert.alertViewStyle = UIAlertViewStylePlainTextInput;
    // setup the text view
    UITextField *textView = [alert textFieldAtIndex:0];
    textView.keyboardType = UIKeyboardTypeASCIICapable;
    textView.placeholder = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁶󠁍󠁡󠀰󠁓󠁧󠀹󠁩󠁊󠁏󠀶󠁓󠁸󠁒󠁄󠁉󠀯󠁄󠁯󠀴󠁏󠁏󠁎󠁹󠁹󠁐󠁍󠁿*/ @"Description", nil);
    textView.delegate = self;
    textView.text = @"";
    
    return alert;
}

- (void) handleGenericBasketControllerErrorNotification:(NSNotification *)notification
{
    // get the error
    ServerError *error = notification.userInfo[kBasketControllerGeneralErrorKey];
    
    ServerErrorAffiliationRequiredMessage *affiliationRequiredMessage = nil;
    BOOL multiEditErrors = NO;
    enum MPOSUIState state = [ContextViewController sharedInstance].currentUIState;

    for (ServerErrorMessage *message in error.messages)
    {
        if (message.codeValue == MShopperAffiliationRequiredExceptionCode)
        {
            affiliationRequiredMessage = (ServerErrorAffiliationRequiredMessage *) message;
            break;
            
        }
        
        // hopefully we will never have to assign customers to multiple basket lines
        if ((state == MPOSUIStateEditBasket) && (message.basketItemId.length > 0)) {
            multiEditErrors = error.transportError == nil;
        }
    }
    
    NSAssert(error != nil, @"The reference to the error object has not been specified.");
    NSAssert([error isKindOfClass:[ServerError class]], @"The specified error object is inherited from an unexpected BasketController.sharedInstance class.");
        
    // show the default error alert view
    
    if (affiliationRequiredMessage != nil)
    {
        NSDictionary *affiliationsByType = [BasketController.sharedInstance.affiliations OCGExtensions_dictionaryWithKeyPath:@"affiliationType"];
        
        Affiliation *affiliation = affiliationsByType[affiliationRequiredMessage.affiliationType];
        
        // affiliation required error, show the affiliation alert
        
        [[POSFuncController sharedInstance] assignAffiliation:affiliation
                                                   completion:notification.userInfo[kBasketControllerRetryBlockKey]];
    }
    else if (!multiEditErrors) {
        [self.errorHandler handleServerError:error
                              transportError:error.transportError
                                 withContext:error.context
                                dismissBlock: ^{
                                    for (ServerErrorMessage *message in ((ServerError *)error).messages)
                                    {
                                        if (message.codeValue == MShopperDeviceNotFoundExceptionCode) {
                                            [[BasketController sharedInstance] deviceNotFound];
                                        }
                                        else if (message.codeValue == MShopperOperatorNotSignedOnExceptionCode)
                                        {
                                            [[BasketController sharedInstance] hasLoggedOut];
                                        }
                                        else if (message.codeValue == MShopperInvalidBasketStatusExceptionCode)
                                        {
                                            [[BasketController sharedInstance] resetBasketToServer];
                                        }
                                        else if (message.codeValue == MShopperSaleBasketNotFoundExceptionCode)
                                        {
                                            [[BasketController sharedInstance] resetBasketToServer];
                                        }
                                        else if (message.codeValue == MShopperUnmappedClarityBusinessExceptionExceptionCode)
                                        {
                                            BasketController.sharedInstance.credentials = nil;
                                            [[BasketController sharedInstance] hasLoggedOut];
                                        }
                                    }
                                }
                                 repeatBlock: nil];
    }    
}

- (void) handleToggleTrainingModeErrorNotification:(NSNotification *)notification
{
    NSError *error = notification.userInfo[kBasketControllerTrainingModeErrorKey];
    ServerError *serverError = notification.userInfo[kBasketControllerTrainingModeServerErrorKey];
    
    NSMutableString *messageText = [NSMutableString string];
    
    if (serverError != nil) {
        for (ServerErrorMessage *message in serverError.messages) {
            if (message.value != nil) {
                [messageText appendString:message.value];
            }
        }
    }
    
    if (error != nil) {
        [messageText appendString:[error localizedDescription]];
    }
        
    UIAlertView *alert =
    [[UIAlertView alloc] initWithTitle:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁫󠀵󠁉󠁤󠁘󠁖󠁏󠁁󠁆󠀷󠁬󠀯󠁯󠁇󠀲󠀳󠁵󠁢󠀫󠁘󠁦󠀯󠁩󠁁󠁦󠁒󠁑󠁿*/ @"Error", nil)
                               message:messageText
                              delegate:nil
                     cancelButtonTitle:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁄󠁴󠁍󠁉󠁣󠀫󠁺󠁔󠁗󠁦󠁹󠁡󠁯󠀷󠁨󠁔󠁩󠀸󠁯󠀫󠁨󠁘󠀹󠁱󠁌󠀶󠀸󠁿*/ @"OK", nil)
                     otherButtonTitles:nil];
    [alert show];
}

- (void) handleProductSearchCompleteNotification:(NSNotification *)note
{
    NSOrderedSet *items = note.userInfo[kBasketControllerProductSearchResultsKey];
    NSNumber *reason = note.userInfo[kBasketControllerProductSearchReasonKey];
    
    if ((items != nil) && (items.count > 0)) {
        if ((reason == nil) || (reason.integerValue == ProductSearchReasonItemDetails)) {
            // display item info
            ItemDetailViewController *detailVC =
            [[ItemDetailViewController alloc] initWithStyle:UITableViewStyleGrouped];
            detailVC.item = items[0];
            [self.navigationController pushViewController:detailVC
                                                 animated:YES];
        } else if (reason.integerValue == ProductSearchReasonReturnItem) {
            // present the return view
            ReturnOptionsTableViewController *vc =
            [[ReturnOptionsTableViewController alloc] initWithStyle:UITableViewStyleGrouped];
            vc.item = items[0];
            vc.originalDescription = nil;
            vc.originalScanId = nil;
            UINavigationController *nav =
            [[UINavigationController alloc] initWithRootViewController:vc];
            nav.modalPresentationStyle = UIModalPresentationFormSheet;
            nav.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
            [self presentViewController:nav
                               animated:YES
                             completion:nil];
        }
    }
}

- (void) handleProductSearchErrorNotification:(NSNotification *)note
{
    // get the original search term
    NSString *search = note.userInfo[kBasketControllerProductSearchRequestKey];
    // check the reason
    NSNumber *reasonNumber = note.userInfo[kBasketControllerProductSearchReasonKey];
    if (reasonNumber != nil) {
        enum ProductSearchReason reason = reasonNumber.integerValue;
        if (reason == ProductSearchReasonReturnItem) {
            // display the dump code prompt
            [self displayDumpCodePrompt:^(NSInteger buttonIndex) {
                // if the user didn't cancel
                if (buttonIndex != 0) {
                    // if the user selected a dump code
                    if (self.selectedDumpCode != nil) {
                        // returning the dump code
                        MPOSItem *returnItem = [[MPOSItem alloc] init];
                        returnItem.itemRefId = self.selectedDumpCode.itemSellingCode;
                        returnItem.shortDesc = self.selectedDumpCode.desc;
                        returnItem.longDesc = self.selectedDumpCode.desc;
                        [BasketController.sharedInstance kick];
                        // get the description
                        UIAlertView *alert = [self buildItemDescriptionAlert];
                        self.returning = YES;
                        UITextField *textView = [alert textFieldAtIndex:0];
                        alert.delegate = [UIAlertView class];
                        alert.dismissBlock = ^(int buttonIndex) {
                            if ((buttonIndex == 0) && (textView.text.length > 0)) {
                                // build the return view
                                ReturnOptionsTableViewController *vc =
                                [[ReturnOptionsTableViewController alloc] initWithStyle:UITableViewStyleGrouped];
                                vc.item = returnItem;
                                vc.originalScanId = search;
                                vc.originalDescription = textView.text;
                                // display the return view
                                UINavigationController *nav =
                                [[UINavigationController alloc] initWithRootViewController:vc];
                                nav.modalPresentationStyle = UIModalPresentationFormSheet;
                                nav.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
                                [self presentViewController:nav
                                                   animated:YES
                                                 completion:nil];
                            }
                            self.returning = NO;
                        };
                        [alert show];
                    }
                }
            }];
        } else {
            // not a return error, pass on to the generic error handler
            [self handleGenericBasketControllerErrorNotification:note];
        }
    } else {
        // no reason info, pass on to the generic error handler
        [self handleGenericBasketControllerErrorNotification:note];
    }
}

/** @brief Called when the app configuration changes.
 */
- (void) handleConfigurationChangeNotification:(NSNotification *)note
{
    [self reloadTableDataAndScroll:NO];
}

#pragma mark - Properties

- (ServerErrorHandler *) errorHandler
{
    if (_errorHandler == nil)
    {
        _errorHandler = [[ServerErrorHandler alloc] init];
    }
    return _errorHandler;
}

#pragma mark - UIViewController

- (void) viewDidLoad
{
    [super viewDidLoad];

    self.view.backgroundColor = [UIColor whiteColor];
    
    if ([UIDevice currentDevice].systemVersion.floatValue >= 7.0)
    {
        self.edgesForExtendedLayout=UIRectEdgeNone;
        self.extendedLayoutIncludesOpaqueBars=NO;
        self.automaticallyAdjustsScrollViewInsets=YES;
    }
    
    // register for BasketController notifications
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(handleGenericBasketControllerErrorNotification:)
                                                 name:kBasketControllerGeneralErrorNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(handleActiveBasketErrorNotification:)
                                                 name:kBasketControllerActiveBasketErrorNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(handleBasketChangeNotification:)
                                                 name:kBasketControllerBasketChangeNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(handleBasketItemChangeNotification:)
                                                 name:kBasketControllerBasketAddItemNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(handleBasketItemChangeNotification:)
                                                 name:kBasketControllerBasketItemChangeNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(handleBasketItemChangeNotification:)
                                                 name:kBasketControllerDiscountChangeNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(handleToggleTrainingModeErrorNotification:)
                                                 name:kBasketControllerTrainingModeErrorNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(handleProductSearchCompleteNotification:)
                                                 name:kBasketControllerProductSearchCompleteNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(handleProductSearchErrorNotification:)
                                                 name:kBasketControllerProductSearchErrorNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(handleConfigurationChangeNotification:)
                                                 name:kBasketControllerConfigurationDidChangeNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(handleBasketChangeNotification:)
                                                 name:kBasketControllerBasketCheckedOutChangeNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(handleBasketControllerPaymentChangeNotification:)
                                                 name:kBasketControllerPaymentChangeNotification
                                               object:nil];
    
    // skin
    [[CRSSkinning currentSkin] applyViewSkin:self];
    
    // update the current view state
    
    [self reloadTableDataAndScroll:NO];
}

-(void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

/*
 @brief This delegate method is used to check it we've got a valid, non-zero number for our price
 */

- (void)didPresentAlertView:(UIAlertView *)alertView
{
    if (alertView.alertViewStyle != UIAlertViewStyleDefault)
    {
        UITextField *textField = [alertView textFieldAtIndex:0];
        UIView *blankingView = [[UIView alloc] initWithFrame:textField.bounds];
        //blankingView.backgroundColor = [UIColor redColor];
        blankingView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        blankingView.userInteractionEnabled = YES;
        [textField addSubview:blankingView];
        [textField bringSubviewToFront:blankingView];
    }
    
}

#pragma mark - Dump code

- (void) displayDumpCodePrompt:(DumpCodeCompleteBlockType)complete
{
    self.selectedDumpCode = nil;
    
    OCGSelectViewController *selectViewController = [[OCGSelectViewController alloc] init];
    
    selectViewController.title = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁚󠀳󠁖󠀶󠁙󠁶󠀱󠁂󠁎󠁍󠀸󠁎󠁢󠀴󠁋󠁳󠁚󠁮󠁏󠁦󠁆󠁥󠁶󠁖󠀲󠁉󠁉󠁿*/ @"Item not found", nil);
    
    selectViewController.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:selectViewController action:@selector(cancelSelection)];
    
    selectViewController.options = @[BasketController.sharedInstance.dumpCodes];
    
    selectViewController.titleForOption = ^(OCGSelectViewController *selectViewController, NSIndexPath *indexPath)
    {
        DumpCode *dumpCode = [selectViewController.options objectForIndexPath:indexPath];
        return dumpCode.desc;
    };
    
    selectViewController.didSelectOption = ^(OCGSelectViewController *selectViewController, NSIndexPath *indexPath)
    {
        [selectViewController dismissViewControllerAnimated:YES completion:^{
            if (indexPath != nil)
            {
                DumpCode *dumpCode = [selectViewController.options objectForIndexPath:indexPath];
                
                self.selectedDumpCode = dumpCode;
                
                complete(1);
            }
            else
            {
                complete(0);
            }
        }];
        
    };
    
    UINavigationController *nav =
    [[UINavigationController alloc] initWithRootViewController:selectViewController];
    nav.modalPresentationStyle = UIModalPresentationFormSheet;
    nav.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
    [[[[[UIApplication sharedApplication] delegate] window] rootViewController] presentViewController:nav
                                                                                             animated:YES
                                                                                           completion:nil];
}

@end
