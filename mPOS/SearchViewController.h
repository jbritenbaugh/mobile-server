//
//  SearchViewController.h
//  mPOS
//
//  Created by Antonio Strijdom on 29/01/2014.
//  Copyright (c) 2014 Clarity. All rights reserved.
//

#import <UIKit/UIKit.h>

/** @file SearchViewController.h */

@interface SearchViewController : UIViewController

@end
