//
//  OCGSwitchTableViewCell.h
//  mPOS
//
//  Created by Antonio Strijdom on 14/08/2013.
//  Copyright (c) 2013 Clarity. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol OCGSwitchTableViewCellDelegate;

@interface OCGSwitchTableViewCell : UITableViewCell

@property (nonatomic, assign) id<OCGSwitchTableViewCellDelegate> delegate;
@property (nonatomic, assign) BOOL on;
@property (nonatomic, assign) BOOL readonly;
@property (nonatomic, strong) NSNumber *labelXPosition;

@end

@protocol OCGSwitchTableViewCellDelegate <NSObject>
@optional
- (void) switchTableViewCell:(OCGSwitchTableViewCell *)cell
                 didToggleTo:(BOOL)state;
@end
