//
//  CustomerController.m
//  mPOS
//
//  Created by Meik Schuetz on 21/09/2012.
//  Copyright (c) 2012 Clarity. All rights reserved.
//

#import "CustomerController.h"
#import "BasketController.h"
#import <objc/runtime.h>
#import "RESTController.h"

@interface CustomerController ()

/** @brief The data source that feeds the view with data.
 */
//@property (nonatomic, strong, readwrite) CustomerDataSource *dataSource;

- (void) performSearch;
@end

@implementation CustomerController

- (void) performSearch
{
    NSLog(@"Retrieved customerSearch %@", self.customerSearch);
    ocg_async_background_overlay(^{
        void (^complete)(NSArray*, NSError*, ServerError*) = ^(NSArray *customers, NSError *restError, ServerError *restServerError)
        {
            DebugLog(@"customers %@ restError: %@ restServerError: %@", customers, restError, restServerError);
            
            ocg_sync_main(^{
                MPOSCustomer *customer = nil;
                
                self.customerSearch.customers  = [customers copy];
                self.customerSearch.error = restServerError;
                self.customerSearch.resultValue = YES;
                customer.hasCustomerChangedValue = NO;
                customer.editableValue = YES;
                [BasketController.sharedInstance kick];
            });
        };
        
        if (self.customerSearch.email != nil) {
            [RESTController.sharedInstance customersWithEmail:self.customerSearch.email
                                                     complete:complete];
        } else if (self.customerSearch.customerId != nil) {
            [RESTController.sharedInstance customerWithCustomerId:self.customerSearch.customerId
                                                         complete:^(Customer *restCustomer, NSError *error, ServerError *serverError)
             {
                 complete(@[restCustomer], error, serverError);
             }];
        } else if (self.customerSearch.cardNo != nil) {
            [RESTController.sharedInstance customerWithCardNo:self.customerSearch.cardNo
                                                     cardType:self.customerSearch.cardType
                                                     complete:^(Customer *restCustomer, NSError *error, ServerError *serverError)
             {
                 complete(@[restCustomer], error, serverError);
             }];
        }
    });
}

@synthesize customerSearch = _customerSearch;

static char SHARED_INSTANCE_IDENTIFER;

+ (void) setSharedInstance:(CustomerController *) sharedInstance
{
    objc_setAssociatedObject(self, &SHARED_INSTANCE_IDENTIFER, sharedInstance, OBJC_ASSOCIATION_RETAIN);
}

+ (CustomerController *) sharedInstance
{
    __block id sharedInstance = objc_getAssociatedObject(self, &SHARED_INSTANCE_IDENTIFER);
    if (sharedInstance != nil) {
        return sharedInstance;
    }
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[self alloc] init];
        self.sharedInstance = sharedInstance;
    });
    
    return sharedInstance;
}

/** @brief Gets a reference to the customer search request managed object.
 */
- (MPOSCustomerSearch*)customerSearch
{
    if (_customerSearch == nil) {
        _customerSearch = [[MPOSCustomerSearch alloc] init];
        _customerSearch.resultValue = YES;
    }
    
    return _customerSearch;
}

- (NSOrderedSet *) currentCustomers
{
    return [NSOrderedSet orderedSetWithArray:self.customerSearch.customers];
}

#pragma mark Backend methods

- (void) clearCustomerSearch
{
    self.customerSearch.error = nil;
    self.customerSearch.email = nil;
    self.customerSearch.resultValue = NO;
    self.customerSearch.customers = nil;
    [BasketController.sharedInstance kick];
}

- (MPOSCustomer *) addCustomer
{
    //    MPOSCustomerSearch *customerSearch = self.customerSearch;
    //
    //    for (MPOSCustomer *customer in customerSearch.customers)
    //    {
    //        if ([customer.baskets count] == 0)
    //        {
    //            [managedObjectContext deleteObject:customer];
    //        }
    //    }
    
    MPOSCustomer *customer = [[MPOSCustomer alloc] init];
    customer.address = [[MPOSCustomerAddress alloc] init];
    //    customerSearch.customers = [NSOrderedSet orderedSetWithObject:customer];
    //    [BasketController.sharedInstance kick];
    return customer;
}


/** @brief Retrieves a customer record by using email as key.
 *  @param customerEmail The customer email to be used as search key.
 */
- (void) getCustomerDetailByEmail:(NSString *)customerEmail
{
    MPOSCustomerSearch *customerSearch = self.customerSearch;
    
    customerSearch.customers = nil;
    
    customerSearch.email = customerEmail;
    customerSearch.customerId = nil;
    customerSearch.cardNo = nil;
    customerSearch.cardType = nil;
    customerSearch.result = nil;
    [BasketController.sharedInstance kick];
    [self performSearch];
}

/** @brief Retrieves a customer record by using email as key.
 *  @param customerId The customer id to be used as search key.
 */
- (void) getCustomerDetailById:(NSString *)customerId
{
    MPOSCustomerSearch *customerSearch = self.customerSearch;
    
    customerSearch.customers = nil;
    
    
    customerSearch.email = nil;
    customerSearch.customerId = customerId;
    customerSearch.cardNo = nil;
    customerSearch.cardType = nil;
    customerSearch.result = nil;
    [BasketController.sharedInstance kick];
    [self performSearch];
}

/** @brief Retrieves a customer record by using email as key.
 */

- (void) getCustomerDetailByCardNo:(NSString *)cardNo
                          cardType:(NSString *)cardType
{
    MPOSCustomerSearch *customerSearch = self.customerSearch;
    
    customerSearch.customers = nil;
    
    customerSearch.email = nil;
    customerSearch.customerId = nil;
    customerSearch.cardNo = cardNo;
    customerSearch.cardType = cardType;
    customerSearch.result = nil;
    [BasketController.sharedInstance kick];
    [self performSearch];
}

- (void) getCustomerDetails:(MPOSCustomer *)customer
                      cards:(BOOL)cards
                      notes:(BOOL)notes
{
    DebugLog(@"Retrieve data for: %@", customer.customerId);
    // retrieve notes, if required
    if (notes) {
        ocg_async_background_overlay(^{
            Customer *restCustomer = customer;
            [RESTController.sharedInstance  notesForCustomerWithCustomerId:restCustomer.customerId
                                                                  complete:^(NSArray *restNotes, NSError *restError, ServerError *restServerError)
             {
                 ocg_sync_main(^{
                     //              NSArray *restNotes = @[@"Note 1", @"Note 2", @"Note 3"];
                     if ((restError == nil) && (restServerError == nil)) {
                         // no error, update notes
                         customer.notes = [restNotes componentsJoinedByString:@"\n"];
                     } else {
                         NSLog(@"error: %@ serverError: %@", restError, restServerError);
                         customer.notesRefreshError =
                         restServerError;
                     }
                 });
                 
             }];
        });
    }
    
    // retreive cards, if required
    if (cards) {
        ocg_async_background_overlay(^{
            Customer *restCustomer = customer;
            [RESTController.sharedInstance  cardsForCustomerWithCustomerId:restCustomer.customerId
                                                                  complete:^(NSArray *restCards, NSError *restError, ServerError *restServerError)
             {
                 ocg_sync_main(^{
                     if ((restError == nil) && (restServerError == nil)) {
                         // no error
                         
                         customer.customerCards = restCards;
                     } else {
                         NSLog(@"error: %@ serverError: %@", restError, restServerError);
                         customer.cardsRefreshError =
                         restServerError;
                     }
                 });
             }];
            
            
        });
    }
}

- (void) assignCustomerCardToBasket:(NSString *)cardNumber
{
    MPOSBasket *basket = BasketController.sharedInstance.basket;
    MPOSBasketCustomerCard *basketCustomerCard = [[MPOSBasketCustomerCard alloc] init];
    [BasketController.sharedInstance.basket addBasketItem:basketCustomerCard];
    basketCustomerCard.customerCardNumber = cardNumber;
    [BasketController.sharedInstance kick];
    [[BasketController sharedInstance] addItem:basketCustomerCard completion:NULL];
}

/** @brief Updates the customer record on the server.
 */
- (void) synchronizeChangesForCustomer:(MPOSCustomer *)customer
{
    customer.hasCustomerChangedValue = YES;
    [BasketController.sharedInstance kick];
    ocg_async_background_overlay(^{
        NSLog(@"update %@", customer.email);
        
        Customer *restCustomer = customer;
        
        void (^complete)(NSError*, ServerError*) = ^(NSError *restError, ServerError *restServerError)
        {
            NSLog(@"error: %@ serverError: %@", restError, restServerError);
            
            customer.customerChangeError = restServerError;
            
            customer.hasCustomerChangedValue = NO;
            customer.editableValue = YES;
            [BasketController.sharedInstance kick];
        };
        
        if ([restCustomer.customerId length] > 0) {
            [RESTController.sharedInstance updateCustomerWithCustomer:restCustomer
                                                           customerId:restCustomer.customerId
                                                             complete:^(NSError *error, ServerError *serverError) {
                                                                 ocg_sync_main(^{
                                                                     complete(error, serverError);
                                                                 });
                                                             }];
        } else {
            [RESTController.sharedInstance registerCustomerWithCustomer:restCustomer
                                                               complete:^(Customer *restCustomer, NSError *restError, ServerError *restServerError) {
                                                                   ocg_sync_main(^{
                                                                       customer.customerId = restCustomer.customerId;
                                                                       complete(restError, restServerError);
                                                                   });
                                                               }];
        }
    });
}

-(void)filloutAllValuesForCustomer:(MPOSCustomer*)customer
{
    NSString *customerId = customer.customerId;
    ocg_async_background_overlay(^{
        [RESTController.sharedInstance customerWithCustomerId:customerId
                                                     complete:^(Customer *restCustomer, NSError *error, ServerError *serverError)
         {
             dispatch_async(dispatch_get_main_queue(), ^{
                 if (error == nil && serverError == nil)
                 {
                     [customer copyValuesFromObject:restCustomer];
                     [BasketController.sharedInstance kick];
                 }
                 else
                 {
                     NSMutableDictionary *userinfo = [NSMutableDictionary dictionary];
                     ServerError *mposerror = serverError;
                     [BasketController.sharedInstance kick];
                     [userinfo setValue:mposerror
                                 forKey:kBasketControllerGeneralErrorKey];
                     [[NSNotificationCenter defaultCenter] postNotificationName:kBasketControllerControlErrorNotification
                                                                         object:self
                                                                       userInfo:userinfo];
                 }
             });
         }];
    });
}

@end
