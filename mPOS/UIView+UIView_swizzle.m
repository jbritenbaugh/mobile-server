//
//  UIView+UIView_swizzle.m
//  mPOS
//
//  Created by Antonio Strijdom on 14/03/2014.
//  Copyright (c) 2014 Clarity. All rights reserved.
//

#import "UIView+UIView_swizzle.h"
#import <objc/runtime.h>

@implementation UIView (UIView_swizzle)

+ (void) load
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        Class class = [self class];
        
        // When swizzling a class method, use the following:
        // Class class = object_getClass((id)self);
        
        SEL originalSelector = @selector(willMoveToSuperview:);
        SEL swizzledSelector = @selector(szwillMoveToSuperview:);
        
        Method originalMethod = class_getInstanceMethod(class, originalSelector);
        Method swizzledMethod = class_getInstanceMethod(class, swizzledSelector);
        
        BOOL didAddMethod =
        class_addMethod(class,
                        originalSelector,
                        method_getImplementation(swizzledMethod),
                        method_getTypeEncoding(swizzledMethod));
        
        if (didAddMethod) {
            class_replaceMethod(class,
                                swizzledSelector,
                                method_getImplementation(originalMethod),
                                method_getTypeEncoding(originalMethod));
        } else {
            method_exchangeImplementations(originalMethod, swizzledMethod);
        }
    });
}

- (void) szwillMoveToSuperview:(UIView *)newSuperview
{
    DebugLog(@"Self = %@", self);
    DebugLog(@"Superview = %@", self);
    [self szwillMoveToSuperview:newSuperview];
}

@end
