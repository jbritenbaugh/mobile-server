//
//  BasketHistoryViewController.h
//  mPOS
//
//  Created by Antonio Strijdom on 21/11/2012.
//  Copyright (c) 2012 Clarity. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CRSTableViewControllerBase.h"

/** @file BasketHistoryViewController.h */

@class MPOSBasket;

@protocol BasketHistoryViewControllerDelegate;

/** @brief Table view controller for displaying sales history.
 */
@interface BasketHistoryViewController : CRSTableViewControllerBase

/** @property delegate
 */
@property (nonatomic, weak) id<BasketHistoryViewControllerDelegate>delegate;

/** @property managedObjectContext
 */
@property (nonatomic, strong) NSManagedObjectContext *managedObjectContext;

/** @property filter
 *  @brief The current applied filter.
 */
@property (nonatomic, strong) NSDictionary *filter;

@end

/** @brief Basket history view controller delegate protocol.
 */
@protocol BasketHistoryViewControllerDelegate <NSObject>
@required
/** @brief Informs the delegate the user selected a basket.
 *  @param historyVC The history view controller.
 *  @param basket The basket selected.
 */
- (void) basketHistory:(BasketHistoryViewController *)historyVC
       didSelectBasket:(BasketDTO *)basket;
/** @brief Informs the delegate the user dismissed the history view controller without picking anything.
 *  @param historyVC The history view controller.
 */
- (void) basketHistoryDidCancel:(BasketHistoryViewController *)historyVC;
@end
