//
//  BasketViewCell.m
//  mPOS
//
//  Created by John Scott on 23/10/2013.
//  Copyright (c) 2013 Clarity. All rights reserved.
//

#import "BasketViewCell.h"

#import "NSDate+OCGExtensions.h"

@interface BasketViewCell ()

@end

@implementation BasketViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:reuseIdentifier];
    if (self) {
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)updateWithBasket:(MPOSBasket*)basket
{
    if (basket.created != nil) {
        
        NSDate *basketDate = basket.created;
        if (basketDate != nil) {
            self.detailTextLabel.text = [basketDate OCGExtensions_humanReadableDescription];
        }
        else
        {
            self.detailTextLabel.text = @"";
        }
    }
    
    //self.basketID.text = [NSString stringWithFormat: NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁉󠁖󠁐󠁎󠁧󠀷󠀸󠁁󠁣󠁴󠁷󠁯󠁮󠁰󠁁󠁙󠁘󠀳󠁤󠁃󠁷󠀶󠁭󠁋󠁃󠁭󠀸󠁿*/ @"Basket #%@", nil), basket.basketID];
    self.textLabel.text = [NSString stringWithFormat: NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠀹󠁶󠁆󠁥󠁴󠁑󠁗󠁭󠁦󠁸󠁁󠁚󠀫󠁦󠁗󠀷󠁮󠁅󠁶󠁚󠁈󠁌󠁦󠁄󠀹󠁁󠁙󠁿*/ @"till %@", nil), basket.tillNumber];
}

-(BOOL)x:(NSString*)string formatter:(NSFormatter*)formatter
{
    for (NSInteger i=1; i<[string length]; i++)
    {
        NSString *partialString = [string substringWithRange:NSMakeRange(0, i)];
        NSRange proposedSelectedRange = NSMakeRange(0, i);
        NSString *originalString = [string substringWithRange:NSMakeRange(0, i-1)];
        NSRange originalSelectedRange = NSMakeRange(0, i-1);
        NSString *errorDescription = nil;
        
        
        
        BOOL result = [formatter isPartialStringValid:&partialString
                                proposedSelectedRange:&proposedSelectedRange
                                       originalString:originalString
                                originalSelectedRange:originalSelectedRange
                                     errorDescription:&errorDescription];
        
        NSLog(@"\"%@\" %@", partialString, result ? @"is a valid date" : @"");
    }
    return YES;
}

@end
