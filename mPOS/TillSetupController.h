//
//  TillSetupController.h
//  mPOS
//
//  Created by Antonio Strijdom on 14/09/2012.
//  Copyright (c) 2012 Clarity. All rights reserved.
//

#import <Foundation/Foundation.h>

/** @file TillSetupController.h */

/** @brief The online till configuration key. */
extern NSString * const kTillSetupOnlineTillNumberConfigKey;
/** @brief The offline till configuration key. */
extern NSString * const kTillSetupOfflineTillNumberConfigKey;

@class ServerError;

/** @brief Controller class for managing the app's till config. */
@interface TillSetupController : NSObject

/** @brief Sets the device profile of the device.
 *  @param tillNumber The new till number.
 *  @param locationKey The key of the location
 *  @param serviceBaseURL The RESTClient's serviceBaseURL
 *  @param complete The block that is executed when the till number is updated.
 */
+ (void) setDeviceProfileTillNumber:(NSString *)value
                        locationKey:(NSString *)locationKey
                     serviceBaseURL:(NSString*)serviceBaseURL
                           complete:(void (^) (NSError *error, ServerError *serverError))complete;

/** @brief Gets the current online till number of the device.
 *  @return The current till number.
 */
+ (NSString *) getOnlineTillNumber;

/** @brief Gets the current offline till number of the device.
 *  @return The current till number.
 */
+ (NSString *) getOfflineTillNumber;

/** @brief Sets the online till number of the device.
 *  @param tillNumber The new till number.
*/
+(void)setOnlineTillNumber:(NSString*)value;

/** @brief Sets the offline till number of the device.
 *  @param tillNumber The new till number.
 */
+(void)setOfflineTillNumber:(NSString*)value;

/** @brief Resets the online till number depending on on/offline mode
 */
+ (void) resetOnlineTillNumber;

/** @brief Resets the offline till number
 */
+ (void) resetOfflineTillNumber;


/** @brief Gets the current till number of the device depending on on/offline mode
 *  @return The current till number.
 */
+ (NSString *) getTillNumber;

/** @brief Resets the till number depending on on/offline mode
 */
+ (void) resetTillNumber;

@end
