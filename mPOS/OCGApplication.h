//
//  OCGApplication.h
//  mPOS
//
//  Created by John Scott on 20/05/2014.
//  Copyright (c) 2014 Clarity. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OCGApplication : UIApplication

@property (readonly) NSTimeInterval inactivityPeriod;
@property (assign) BOOL inactivityPeriodTrackingEnabled;

@end
