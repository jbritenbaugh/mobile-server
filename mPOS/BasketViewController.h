//
//  BasketViewController.h
//  mPOS
//
//  Created by Meik Schuetz on 09/08/2012.
//  Copyright (c) 2012 Clarity. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BasketTableViewController.h"

@class MPOSBasketItem;

/** @file BasketViewController.h */

/** @brief View controller that manages the view that displays the active basket.
 */
@interface BasketViewController : BasketTableViewController

- (void) handleGenericBasketControllerErrorNotification:(NSNotification *)notification;

@end
