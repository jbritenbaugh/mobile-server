//
//  BasketMultiSelectViewController.h
//  mPOS
//
//  Created by John Scott on 21/02/2013.
//  Copyright (c) 2013 Clarity. All rights reserved.
//

#import <UIKit/UIKit.h>

/** @file BasketMultiSelectViewController.h */

/** @brief UI for selecting multiple basket items.
 */
@interface BasketMultiSelectViewController : UITableViewController

/** @property selectedBasketItems
 *  @brief The current item selections.
 */
@property (strong, nonatomic) NSMutableSet *selectedBasketItems;

/** @property validBasketItems
 *  @brief The items displayed in the table.
 */
@property (strong, nonatomic) NSMutableArray *validBasketItems;

/** @brief Returns the selectedBasketItems basketItemIds.
 */
- (NSArray *) selectedBasketItemIdArray;

/** @brief Reloads the basket table.
 */
- (void) reloadTable;

@end
