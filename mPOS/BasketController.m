//
//  BasketController.m
//  mPOS
//
//  Created by Meik Schuetz on 09/08/2012.
//  Copyright (c) 2012 Clarity. All rights reserved.
//

#import "BasketController.h"
#import "RESTController.h"
#import "CRSLocationController.h"
#import "PrinterQueueController.h"
#import "UIDevice+OCGIdentifier.h"
#import "TillSetupController.h"
#import "LocationDataSource.h"
#import "OCGPeripheralManager.h"
#import "OCGEFTManager.h"
#import "TestModeController.h"
#import "POSFunc.h"
#import "OCGIntegrationController.h"
#import "UIAlertView+MKBlockAdditions.h"
#import "ServerErrorHandler.h"
#import "VersionDetails.h"
#import "NSLocale+OCGAdditions.h"
#import "OCGApplication.h"
#import "OCGReceiptManager.h"
#import "CrashReporter.h"
#import "PrinterController.h"
#import "POSInputView.h"

@interface BasketController () <RESTControllerDelegate> {
    BOOL _sendBasketChangeNotification;
    BOOL _isFetchingBasketData;
    BOOL _isChangingBasketItem;
    NSManagedObjectContext *_managedObjectContext;
    NSManagedObjectModel *_managedObjectModel;
    NSPersistentStoreCoordinator *_persistentStoreCoordinator;
    dispatch_group_t _configGroup;
    dispatch_block_t _configBlock;
}
@property (nonatomic, strong) PermissionGroup *currentPermissionGroup;
@property (nonatomic, strong) PrinterQueueController *printQueue;

@end

@implementation BasketController
{
    Configuration* _configuration;
    NSMutableDictionary *_localizableTables;
    NSTimer *_activityTimer;
}

#pragma mark - Private

#define kServerErrorMessageBasketNotFound 40

NSString * const kBasketControllerFakeBasketID = @"kBasketControllerFakeBasketID";


#pragma mark Notifications

NSString * const kBasketControllerConfigurationDidChangeNotification = @"kBasketControllerConfigurationDidChangeNotification";
NSString * const kBasketControllerActiveBasketChangeNotification = @"kBasketControllerActiveBasketChangeNotification";
NSString * const kBasketControllerTaxFreeChangeNotification = @"kBasketControllerTaxFreeChangeNotification";
NSString * const kBasketControllerBasketCompleteNotification = @"kBasketControllerBasketCompleteNotification";
NSString * const kBasketControllerBasketCompleteResultKey = @"kBasketControllerBasketCompleteResultKey";
NSString * const kBasketControllerActiveBasketErrorNotification = @"kBasketControllerActiveBasketErrorNotification";
NSString * const kBasketControllerActiveBasketErrorKey = @"kBasketControllerActiveBasketErrorKey";
NSString * const kBasketControllerBasketChangeNotification = @"kBasketControllerBasketChangeNotification";
NSString * const kBasketControllerBasketItemChangeNotification = @"kBasketControllerBasketItemChangeNotification";
NSString * const kBasketControllerBasketItemAddedNotification = @"kBasketControllerBasketItemAddedNotification";
NSString * const kBasketControllerBasketAddItemNotification = @"kBasketControllerBasketAddItemNotification";
NSString * const kBasketControllerBasketAddItemErrorNotification = @"kBasketControllerBasketAddItemErrorNotification";
NSString * const kBasketControllerBasketItemErrorKey = @"kBasketControllerBasketItemErrorKey";
NSString * const kBasketControllerCustomerAffiliationAddedNotification = @"kBasketControllerCustomerAffiliationAddedNotification";
NSString * const kBasketControllerCustomerCardAddedNotification = @"kBasketControllerCustomerCardAddedNotification";
NSString * const kBasketControllerBasketCheckedOutChangeNotification = @"kBasketControllerBasketCheckedOutChangeNotification";
NSString * const kBasketControllerBasketCheckedOutErrorNotification = @"kBasketControllerBasketCheckedOutErrorNotification";
NSString * const kBasketControllerDiscountChangeNotification = @"kBasketControllerDiscountChangeNotification";
NSString * const kBasketControllerPaymentChangeNotification = @"kBasketControllerPaymentChangeNotification";
NSString * const kBasketControllerCustomerCreatedNotification = @"kBasketControllerCustomerCreatedNotification";
NSString * const kBasketControllerCustomerChangedNotification = @"kBasketControllerCustomerChangedNotification";
NSString * const kBasketControllerUserLoggedOnNotification = @"kBasketControllerUserLoggedOnNotification";
NSString * const kBasketControllerHasLoggedOutNotification = @"kBasketControllerHasLoggedOutNotification";
NSString * const kBasketControllerTrainingModeToggledNotification = @"kBasketControllerTrainingModeToggledNotification";
NSString * const kBasketControllerTrainingModeErrorNotification = @"kBasketControllerTrainingModeErrorNotification";
NSString * const kBasketControllerTrainingModeErrorKey = @"kBasketControllerTrainingModeErrorKey";
NSString * const kBasketControllerTrainingModeServerErrorKey = @"kBasketControllerTrainingModeServerErrorKey";
NSString * const kBasketControllerActivityChangeNotification = @"kBasketControllerActivityChangeNotification";
NSString * const kBasketControllerGeneralErrorNotification = @"kBasketControllerGeneralErrorNotification";
NSString * const kBasketControllerGeneralErrorKey = @"kBasketControllerGeneralErrorKey";
NSString * const kBasketControllerDeviceNotFoundNotification = @"kBasketControllerDeviceNotFoundNotification";
NSString * const kBasketControllerOverrideErrorNotification = @"kBasketControllerOverrideErrorNotification";
NSString * const kBasketControllerProductSearchCompleteNotification = @"kBasketControllerProductSearchCompleteNotification";
NSString * const kBasketControllerProductSearchErrorNotification = @"kBasketControllerProductSearchErrorNotification";
NSString * const kBasketControllerProductSearchRequestKey = @"kBasketControllerProductSearchRequestKey";
NSString * const kBasketControllerProductSearchResultsKey = @"kBasketControllerProductSearchResultsKey";
NSString * const kBasketControllerProductSearchReasonKey = @"kBasketControllerProductSearchReasonKey";
NSString * const kBasketControllerItemAvailabilityGetCompleteNotification = @"kBasketControllerItemAvailabilityGetCompleteNotification";
NSString * const kBasketControllerItemAvailabilityResultsKey = @"kBasketControllerItemAvailabilityResultsKey";
NSString * const kBasketControllerControlErrorNotification = @"kBasketControllerControlErrorNotification";
NSString * const kBasketControllerControlRetryBlockKey = @"kBasketControllerControlRetryBlockKey";
NSString * const kBasketControllerRetryBlockKey = @"kBasketControllerRetryBlockKey";
NSString * const kBasketControllerControlCancelBlockKey = @"kBasketControllerControlCancelBlockKey";
NSString * const kBasketControllerKickedNotification = @"kBasketControllerKickedNotification";


#pragma mark Transaction completion reasons

NSString *const kBasketControllerBasketCompleteResultCompleted = @"COMPLETED";
NSString *const kBasketControllerBasketCompleteResultSuspended = @"SUSPENDED";
NSString *const kBasketControllerBasketCompleteResultCancelled = @"CANCELLED";

#pragma mark Core Data

static NSString * const kGIFT_CARD_SWIPE = @"kGIFT_CARD_SWIPE";
static NSString * const kCONFIGURATION_DATA_KEY = @"kCONFIGURATION_DATA_KEY";
static NSString * const kCONFIGURATION_ETAG_KEY = @"kCONFIGURATION_ETAG_KEY";

#pragma mark - Properties

@synthesize hasConfiguration;
- (BOOL) hasConfiguration
{
    return _configuration != nil;
}

@synthesize basket = _basket;
/** @brief Sets the current basket, if it isn't already the current basket.
 *  @param basket The new basket.
 */
- (void) setBasket:(MPOSBasket *)basket
{
    id taxfreeDocument = nil;
    
    if ([basket isKindOfClass:BasketTaxFreeDocumentDTO.class])
    {
        taxfreeDocument = basket;
        basket = [[BasketAssociateDTO alloc] init];
        basket.status = @"IN_PROGRESS";
    }
    
    // only set this if it is different
    // because it would fire off loads of ManagedObjectDidChange noifications
    // which would cause all sorts of problems
    _basket = basket;
    
    // post the basket changed notification
    if (_sendBasketChangeNotification) {
        [[NSNotificationCenter defaultCenter] postNotificationName:kBasketControllerBasketChangeNotification
                                                            object:self];
        
        [[NSNotificationCenter defaultCenter] postNotificationName:kBasketControllerTaxFreeChangeNotification
                                                            object:taxfreeDocument];
        
    }
    _sendBasketChangeNotification = YES;
}

/** @brief Gets the active basket.
 *  If the active basket isn't the one in the core data store, it is
 *  set and returns that.
 */
- (MPOSBasket *) basket
{
    // more KVO nonsense
    // TODO kill KVO, kill the basket property and just use core data
    // notifications
    return _basket;
}

@synthesize basketTotal;
/** @brief Returns the basket's total, chargable amount.
 *  @return The basket's total, chargable amount.
 */
- (NSNumber *) basketTotal
{
    if (self.basket == nil)
        return [NSNumber numberWithDouble:0.0];
    
    return [NSNumber numberWithDouble:self.basket.amountDue.amount.doubleValue];
}

@synthesize canAssignCustomerToBasket;
/** @brief Flat that indicates if a customer can be associated with the active basket.
 */
- (BOOL) canAssignCustomerToBasket
{
    return self.isActive;
}

@synthesize canCheckOut;
/** @brief Flag that indicates if the current basket can be checked out.
 */
- (BOOL) canCheckOut
{
    return [self.basket.status isEqualToString:@"CHECKOUT"] || (self.isActive && ([self.basket.basketItems count] > 0));
}

@synthesize canClearBasket;
/** @brief Flag that indicates if the current basket can be cleared.
 */
- (BOOL) canClearBasket
{
    return [self.basket.basketID length] != 0;
}

@synthesize canDiscountBasket;
/** @brief Flag that indicates if the user can apply a basket level discount.
 */
- (BOOL) canDiscountBasket
{
    return (([self.basket.basketID length] != 0) && (!self.isPartiallyTendered));
}

@synthesize canDisassociateDrawerInsert;
/** @brief Flag that indicates if the user can disassociate a till drawer insert.
 */
- (BOOL) canDisassociateDrawerInsert
{
    BOOL result = NO;
    
    // allow disassociate if...
    if ([self.basket.basketID length] != 0) {
        result = NO;
    } else {
        result = YES;
    }
    
    return result;
}

@synthesize canEdit;
/** @brief Flag that indicates if the current basket can be edited.
 */
- (BOOL) canEdit
{
    return (self.isActive && ([self.basket.basketItems count] > 0));
}

@synthesize canLogout;
/** @brief Flag that indicates whether the user can log out.
 */
- (BOOL) canLogout
{
    BOOL result = NO;
    
    // MPOS-638 Allow logout if...
    if ([self.basket.basketID length] != 0) {
        result = NO;
        // we don't have any lines
        if (self.basket.basketItems.count == 0) {
            result = YES;
        }
    } else {
        result = YES;
    }
    
    return result;
}

@synthesize canRecallBaskets;
/** @brief Flag that indicates if the user can recall suspended baskets
 */
- (BOOL) canRecallBaskets
{
    return [self.basket.basketID length] == 0;
}

- (BOOL) canReloadOptions
{
    return [self.basket.basketID length] == 0;
}

@synthesize canReprintReceipts;
/** @brief Flag that indicates if the user can reprint receipts.
 */
- (BOOL) canReprintReceipts
{
    return [self.basket.basketID length] == 0;
}

@synthesize canScanToBasket;
/** @brief Flag that indicates if the user is allowed to scan new items to the basket.
 */
- (BOOL) canScanToBasket
{
    return self.isActive && !self.isAddingBasketItem && ![OCGEFTManager sharedInstance].processing;
}

@synthesize canSuspendBasket;
/** @brief Flag that indicates if the current basket can be suspended.
 */
- (BOOL) canSuspendBasket
{
    BOOL result = YES;
    
    if ((self.isActive || self.isCheckedOut) && ([self.basket.basketItems count] > 0)) {
        result = !self.isPartiallyTendered;
        if (result) {
            // check for return items
            for (BasketItem *item in self.basket.basketItems) {
                if ([item isKindOfClass:[ReturnItem class]]) {
                    result = NO;
                    break;
                }
            }
        }
    } else {
        result = NO;
    }
    
    return result;
}

@synthesize currentPermissionGroup = _currentPermissionGroup;

@synthesize externalAuthEnabled;
/** @brief Determines if external auth is enabled by checking for the SSO
 *  sign on method.
 *  @return YES if using external auth, otherwise NO.
 */
- (BOOL) externalAuthEnabled
{
    return [self.signOnMethodProperties containsObject:@"SSO"];
}

@synthesize externalAuthURL;
- (NSURL *) externalAuthURL
{
    if ((self.clientSettings.authURL != nil) &&
        (![self.clientSettings.authURL isEqualToString:@""])) {
        return [NSURL URLWithString:self.clientSettings.authURL];
    } else {
        return nil;
    }
}

/** @brief Flag that indicates if a customer has been associated with the active basket.
 */
- (BOOL) hasCustomerAssociated
{
    BOOL hasCustomerAssociated = NO;
    for (id basketCustomerCard in self.basket.basketItems)
    {
        if ([basketCustomerCard isKindOfClass:[MPOSBasketCustomerCard class]])
        {
            hasCustomerAssociated = YES;
        }
    }
    
    return hasCustomerAssociated;
}

@synthesize isActive;
/** @brief Flag indicating is the current basket is the active basket (in progress).
 */
- (BOOL) isActive
{
    return ((self.basket != nil) &&
            ([self.basket.status isEqualToString:@"IN_PROGRESS"]));
}

@synthesize isAddingBasketItem = _isAddingBasketItem;
/** @brief Sets whether we are currently processing an add item request.
 *  Set to YES at the start of adding an item.
 *  Set to NO at the end of adding an item.
 */
- (void) setIsAddingBasketItem:(BOOL)isAddingBasketItem
{
    _isAddingBasketItem = isAddingBasketItem;
    [[NSNotificationCenter defaultCenter] postNotificationName:kBasketControllerBasketAddItemNotification
                                                        object:self];
}

@synthesize isCheckedOut;
/** @brief Flag that indicates if the current basket can is checked out.
 */
- (BOOL) isCheckedOut
{
    return ((self.basket != nil) &&
            ([self.basket.status isEqualToString:@"CHECKOUT"]));
}

@synthesize isPrinting;
/** @brief Flag that indicates if the current basket is printing.
 */
- (BOOL) isPrinting
{
    return ((self.basket != nil) &&
            ([self.basket.status isEqualToString:@"PRINTING"]));
}

@synthesize isFinished;
/** @brief Flag that indicates if the current basket is finished.
 */
- (BOOL) isFinished
{
    return ((self.basket != nil) &&
            ([self.basket.status isEqualToString:@"COMPLETE"]));
}

@synthesize isGiftReceiptAvailable;
/** @brief Flag indicating if a gift receipt is available
 */
- (BOOL) isGiftReceiptAvailable
{
    return (self.basket.giftReceipt != nil);
}

@synthesize isGiftReceiptRequired;
/** @brief Flag indicating if a gift receipt is required
 */
- (BOOL) isGiftReceiptRequired
{
    return self.basket.giftReceiptRequired;
}

@synthesize isInTrainingMode;
/** @brief Flag indicating if we are in training mode.
 */
- (BOOL) isInTrainingMode
{
    BOOL result = NO;
    
    // get the training mode from the current user credentials
    if (self.credentials != nil) {
        result = self.credentials.trainingMode;
    }
    
    return result;
}

@synthesize isReturnItem = _isReturnItem;

@synthesize isTendered;
/** @brief Flag indicating is the current basket is checked out and ready for tender.
 */
- (BOOL) isTendered
{
    return ((self.basket != nil) &&
            ([self.basket.status isEqualToString:@"TENDERED"]));
}

@synthesize isPartiallyTendered;
/** @brief Flag that indicates if the current basket is partially tendered.
 */
- (BOOL) isPartiallyTendered
{
    BOOL result = NO;
    
    for (int i = 0; i < self.basket.basketItems.count; i++) {
        if ([self.basket.basketItems[i] isKindOfClass:[MPOSBasketTender class]]) {
            result = YES;
            break;
        }
    }
    
    return result;
}

@synthesize printQueue = _printQueue;
/** @brief The printer controller that manages the print job queue.
 */
- (PrinterQueueController *) printQueue
{
    if (_printQueue == nil) {
        _printQueue = [[PrinterQueueController alloc] init];
    }
    
    return _printQueue;
}

@synthesize promptForBasketDescription = _promptForBasketDescription;
- (BOOL) promptForBasketDescription
{
    BOOL result = NO;
    
    // prompt is determined by the BasketSuspendDescriptionPrompt client setting
    if (self.configuration) {
        result = self.configuration.clientSettings.basketSuspendDescriptionPrompt;
    }
    
    return result;
}

@synthesize signOnMethodProperties = _signOnMethodProperties;
/** @brief The signon method properties specified in configuration settings.
 */
- (NSArray *) signOnMethodProperties
{
    return [self.clientSettings.signOnMethod componentsSeparatedByString:@","];
}

/** @brief Whether or not printdatalines are automatically printed.
 */
- (BOOL) autoPrintingEnabled
{
    BOOL result = NO;
    
    if (self.configuration.clientSettings) {
        result = self.configuration.clientSettings.autoPrint;
    }
    
    return result;
}

#pragma mark - Object Lifecycle

static NSString * const kBasketControllerGitHash = @"kBasketControllerGitHash";

/** @brief The designated initializer.
 *  @return A newly initialized object of this class.
 */
- (id) init
{
    if (self = [super init]) {
        _configGroup = dispatch_group_create();
        _sendBasketChangeNotification = YES;
        
        
        NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
        
        NSString *currentVersionDetailsGitHash = [standardUserDefaults stringForKey:kBasketControllerGitHash];
        if (![currentVersionDetailsGitHash isEqualToString:VERSION_DETAILS_GIT_HASH] && !DEBUG)
        {
            ErrorLog(@"Changed git commit from %@ to %@.\nResetting the credentials.\n", currentVersionDetailsGitHash, VERSION_DETAILS_GIT_HASH);
            self.credentials = nil;
            
            [standardUserDefaults setObject:VERSION_DETAILS_GIT_HASH forKey:kBasketControllerGitHash];
            [standardUserDefaults synchronize];
        }
        
#if DEBUG
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        [defaults removeObjectForKey:kCONFIGURATION_ETAG_KEY];
        [defaults removeObjectForKey:kCONFIGURATION_DATA_KEY];
        [defaults synchronize];
#endif
        
        if ([CrashReporter.sharedInstance didCrashLastTime])
        {
            self.credentials = nil;
        }
        
        RESTController.sharedInstance.delegate = self;
        
        if (self.clientSettings.DRAFT_numberPadIsReversed)
        {
            [POSInputView setLayout:POSInputViewPOSLayout];
        }
        else
        {
            [POSInputView setLayout:POSInputViewNumberPadLayout];
        }
        [[NSNotificationCenter defaultCenter] addObserver: self
                                                 selector: @selector(applicationWillTerminate:)
                                                     name: UIApplicationWillTerminateNotification
                                                   object: nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(serverModeDidChange:)
                                                     name:kTestModeControllerServerModeDidChangeNotification
                                                   object:nil];
        
        _activityTimer = [NSTimer scheduledTimerWithTimeInterval:1
                                                          target:self
                                                        selector:@selector(checkApplicationActivity)
                                                        userInfo:nil
                                                         repeats:YES];
        
        if ([_activityTimer respondsToSelector:@selector(tolerance)])
        {
            _activityTimer.tolerance = 10;
        }
    }
    
    return self;
}

/** @brief Returns the singleton instance of the class.
 */
+ (BasketController *) sharedInstance
{
    static BasketController *sharedInstance = nil;
    if (sharedInstance != nil) {
        return sharedInstance;
    }
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[BasketController alloc] init];
    });
    
    return sharedInstance;
}

- (void) applicationWillTerminate:(NSNotification*)notification
{
}

#pragma mark - Utility

- (void) deviceNotFound
{
    self.credentials = nil;
    [[NSNotificationCenter defaultCenter] postNotificationName:kBasketControllerDeviceNotFoundNotification
                                                        object:self];
}

-(void)checkApplicationActivity
{
    //    NSLog(@"%@", NSStringFromSelector(_cmd));
    OCGApplication *application = (OCGApplication*) UIApplication.sharedApplication;
    NSInteger logoutAfterInactivityPeriod = _configuration.clientSettings.logoutAfterInactivityPeriod;
    //    NSLog(@"%f / %ld", application.inactivityPeriod, (long)logoutAfterInactivityPeriod);
    //    logoutAfterInactivityPeriod = 20;
    if (_configuration != nil
        && logoutAfterInactivityPeriod > 0
        && logoutAfterInactivityPeriod < application.inactivityPeriod
        && !self.needsAuthentication
        && self.basket.basketID.length == 0
        )
    {
        [self logoutSucceeded:^(BasketController *controller) {
            [self hasLoggedOut];
        } failed:^(BasketController *controller, NSError *error, ServerError *serverError) {
            [self hasLoggedOut];
        }];
    }
}

#pragma mark - RESTclient methods

#pragma mark User

@synthesize credentials = _credentials;
-(void)setCredentials:(Credentials *)credentials
{
    NSData *data = nil;
    @autoreleasepool {
        data = [NSKeyedArchiver archivedDataWithRootObject:credentials];
    }
    
    NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
    [standardUserDefaults setObject:data forKey:@"RESTClientCredentials"];
    [standardUserDefaults synchronize];
    _credentials = credentials;
}

-(Credentials *)credentials
{
    if (_credentials == nil)
    {
        NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
        
        NSData *data = [standardUserDefaults objectForKey:@"RESTClientCredentials"];
        @autoreleasepool {
            if (data != nil)
            {
                _credentials = [NSKeyedUnarchiver unarchiveObjectWithData:data];
            }
        }
        
    }
    return _credentials;
}

- (BOOL)needsAuthentication
{
    return self.credentials.token == nil || [self.credentials.token length] == 0;
}

-(void)setNeedsAuthentication
{
    self.credentials.token = nil;
    self.credentials = self.credentials;
}

-(NSString*)associateUserName
{
    return self.credentials.username;
}

- (void) authenticateWithCredentials:(Credentials *)credentials
             signOffExistingOperator:(BOOL)signOffExistingOperator
                           succeeded:(void (^)(BasketController *controller))succeeded
                              failed:(void (^)(BasketController *controller, NSError *error, ServerError *serverError))failed
{
    ocg_async_background_overlay(^{
        // start the server request
        [RESTController.sharedInstance loginAssociateWithCredentials:credentials
                                             signOffExistingOperator:signOffExistingOperator
                                                            complete:^(Credentials *result, NSError *error, ServerError *serverError)
         {
             // set the basic HTTP authentication credentials
             self.credentials = result;
             
             NSLocale *runtimeLocale = [[NSLocale alloc] initWithLocaleIdentifier:result.preferredLanguage];
             [NSLocale OCGAdditions_setRuntimeLocale:runtimeLocale];
             
             
             // call the succeeded block, if given.
             if ([self needsAuthentication]) {
                 ocg_sync_main(^{
                     NSMutableString *messageText = [NSMutableString string];
                     BOOL signOffExistingOperator = NO;
                     
                     for (ServerErrorMessage *message in serverError.messages) {
                         if (message.value != nil) {
                             [messageText appendString:message.value];
                         }
                         if ([message.code intValue] == MShopperDifferentAssociateLoggedInExceptionCode) {
                             signOffExistingOperator = YES;
                         }
                     }
                     
                     if (error != nil) {
                         [messageText appendString:[error localizedDescription]];
                     }
                     
                     if (signOffExistingOperator) {
                         [UIAlertView alertViewWithTitle:nil
                                                 message:messageText
                                       cancelButtonTitle:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁩󠁢󠁺󠁬󠁏󠁶󠁣󠁧󠁫󠁍󠁋󠁧󠁫󠀱󠁶󠁐󠁐󠀹󠁘󠀴󠁘󠁡󠁕󠁭󠁹󠁷󠀰󠁿*/ @"Cancel", nil)
                                       otherButtonTitles:@[NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁋󠁨󠀶󠁗󠁣󠁰󠁃󠁯󠁩󠀵󠁭󠁐󠁰󠁤󠁆󠁍󠁕󠀶󠁘󠁶󠀰󠁇󠀰󠁡󠁖󠀱󠁳󠁿*/ @"Force", nil)]
                                               onDismiss:^(int buttonIndex) {
                                                   // try again with force
                                                   [self authenticateWithCredentials:credentials
                                                             signOffExistingOperator:YES
                                                                           succeeded:succeeded
                                                                              failed:failed];
                                               }
                                                onCancel:^{
                                                    
                                                }];
                     } else if (failed != nil)
                     {
                         failed(self, error, serverError);
                     }
                 });
             } else {
                 self.currentPermissionGroup = nil;
                 ocg_sync_main(^{
                     [[NSNotificationCenter defaultCenter] postNotificationName:kBasketControllerUserLoggedOnNotification
                                                                         object:self];
                     [self refreshConfiguration:NO];
                     if (succeeded != nil) {
                         succeeded(self);
                     }
                 });
             }
         }];
    });
}

- (void) logoutSucceeded: (void (^)(BasketController *controller))succeeded
                  failed: (void (^)(BasketController *controller, NSError *error, ServerError *serverError))failed
{
    // start the server request
    if (![self needsAuthentication])
    {
        if ([self.clientSettings.releaseAuthSessionUrl length] > 0)
        {
            NSError *restError = nil;
            ServerError *restServerError = nil;
            NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:self.clientSettings.releaseAuthSessionUrl]];
            
            [RESTController.sharedInstance sendSynchronousRequest:request
                                                       returnType:nil
                                                            error:&restError
                                                      serverError:&restServerError
                                                             etag:NULL];
        }
        
        __block Credentials *credentials = self.credentials;
        credentials.storeId = [CRSLocationController getLocationKey];
        credentials.tillNumber = [TillSetupController getTillNumber];
        
        [RESTController.sharedInstance logoutAssociateWithCredentials:credentials
                                                             complete:^(Credentials *result, NSError *error, ServerError *serverError)
         {
             credentials = result;
             // set the basic HTTP authentication credentials
             self.credentials = credentials;
             [NSLocale OCGAdditions_setRuntimeLocale:nil];
             // call the succeeded block, if given.
             if ([self needsAuthentication])
             {
                 if (succeeded != nil)
                     succeeded(self);
             }
             else
             {
                 if (failed != nil)
                     failed(self, error, serverError);
             }
         }];
    } else {
        // call the succeeded block, if given.
        if (succeeded != nil)
            succeeded(self);
    }
}

- (void) hasLoggedOut
{
    [self setNeedsAuthentication];
    [[NSNotificationCenter defaultCenter] postNotificationName:kBasketControllerHasLoggedOutNotification
                                                        object:self];
}

- (BOOL) doesUserHavePermissionTo:(POSPermission)permission
{
    BOOL result = NO;
    
    // get the current user credentials
    Credentials *userCred = self.credentials;
    // if there is a user logged on
    if (![self needsAuthentication]) {
        // and they have a permission group
        if ((userCred.permissionsGroup != nil) &&
            (![userCred.permissionsGroup isEqualToString:@""])) {
            // if we haven't got a permissions group yet
            if ((self.currentPermissionGroup == nil) ||
                // or user permissions don't match
                (![self.currentPermissionGroup.groupName isEqualToString:userCred.permissionsGroup])) {
                // look up the user's permissions group
                Configuration *configuration = [self configuration];
                
                if (configuration != nil)
                {
                    for (PermissionGroup *permissionsGroup in configuration.permissionGroups) {
                        if ([permissionsGroup.groupName isEqualToString:userCred.permissionsGroup]) {
                            self.currentPermissionGroup = permissionsGroup;
                        }
                    }
                }
            }
        }
        // make sure we have a permissions group
        if (self.currentPermissionGroup) {
            // determine if the user has permission
            NSString *permissionString = NSStringForPOSPermission(permission);
            for (Permission *permissionToCheck in self.currentPermissionGroup.permissions) {
                if ([permissionToCheck.permission isEqualToString:permissionString]) {
                    // the user has permission
                    result = YES;
                    break;
                }
            }
        } else {
            // user's group wasn't found, give them full access
            result = YES;
        }
    } else {
        // clear the current permission group
        self.currentPermissionGroup = nil;
        result = NO;
    }
    
    return result;
}

#pragma mark Basket

/** @brief Requests the active basket from the web service.
 */
- (void) getActiveBasket
{
    // make sure we're logged in
    if ([self needsAuthentication]) {
        // not logged on, fail silently
        ErrorLog(@"Authentication failed for active basket check");
    } else {
        NSString *existingBasketId = self.basket.basketID;
        ocg_async_background_overlay(^{
            // initialise
            __block BasketDTO *restBasket = nil;
            
            RESTControllerCompleteBlockType processActiveBasket =
            ^(RESTInvocation *invocation, NSString*server, id result, NSError *error, ServerError *serverError, NSString *etag) {
                // check for basket not found errors
                for (ServerErrorMessage *restServerErrorMessage in serverError.messages) {
                    if ((restServerErrorMessage != nil) &&
                        ([restServerErrorMessage.code intValue] == MShopperSaleBasketNotFoundExceptionCode)) {
                        // clear the error
                        serverError = nil;
                        break;
                    }
                }
                ocg_sync_main(^{
                    // if we errored, clear the existing basket
                    if ((existingBasketId != nil) &&
                        (serverError != nil)) {
                        self.basket = nil;
                    }
                    
                    self.basket = restBasket;
                    
                    // save
                    [self kick];
                    // send notification
                    if (serverError == nil) {
                        // no errors, assign the new basket
                        // send the notification
                        [[NSNotificationCenter defaultCenter] postNotificationName:kBasketControllerActiveBasketChangeNotification
                                                                            object:self];
                    } else {
                        // there was an error, clear the current basket
                        self.basket = nil;
                        // send the notification
                        if (serverError.overrideErrorMessageCheck) {
                            [[NSNotificationCenter defaultCenter] postNotificationName:kBasketControllerOverrideErrorNotification
                                                                                object:self
                                                                              userInfo:@{kBasketControllerGeneralErrorKey:serverError}];
                        } else {
                            [[NSNotificationCenter defaultCenter] postNotificationName:kBasketControllerActiveBasketErrorNotification
                                                                                object:self
                                                                              userInfo:@{kBasketControllerActiveBasketErrorKey:serverError}];
                        }
                    }
                    
                });
            };
            
            dispatch_block_t getActiveBasket = ^()
            {
                // if not, just get active
                NSString *uniqueDeviceIdentifier = [[UIDevice currentDevice] uniqueDeviceIdentifier];
                [RESTController.sharedInstance getActiveBasketWithDeviceID:uniqueDeviceIdentifier
                                                                  complete:^(BasketAssociateDTO *result, NSError *error, ServerError *serverError)
                 {
                     restBasket = result;
                     processActiveBasket(nil, nil, result, error, serverError, nil);
                 }];
            };
            
            // if we already have a basket
            if (existingBasketId != nil && ![existingBasketId isEqual:kBasketControllerFakeBasketID])
            {
                // refresh it
                [RESTController.sharedInstance getBasketWithBasketID:existingBasketId
                                                            complete:^(BasketAssociateDTO *result, NSError *error, ServerError *serverError)
                 {
                     restBasket = result;
                     NSString *locationKey = [CRSLocationController getLocationKey];
                     
                     if (!restBasket && [locationKey length] > 0) {
                         getActiveBasket();
                     } else {
                         processActiveBasket(nil, nil, result, error, serverError, nil);
                     }
                 }];
            }
            else
            {
                getActiveBasket();
            }
        });
    }
}

/** @brief Removes the local copy of the basket and retreives the active basket from the web service.
 */
- (void) resetBasketToServer
{
    BOOL finished = NO;
    if (self.basket != nil)
    {
        // if the basket is finished
        if (self.isFinished) {
            if (self.clientSettings.logoutAfterSaleComplete)
            {
                [self logoutSucceeded:^(BasketController *controller) {
                    [self hasLoggedOut];
                } failed:^(BasketController *controller, NSError *error, ServerError *serverError) {
                    [self hasLoggedOut];
                }];
            }
            else
            {
                finished = YES;
                // make a note of whether we were launched from an URL
                BOOL wasLaunchedFromURL = [OCGIntegrationController sharedInstance].wasLauchedFromURL;
                // send the completed notification
                [[NSNotificationCenter defaultCenter] postNotificationName:kBasketControllerBasketCompleteNotification
                                                                    object:self
                                                                  userInfo:
                 @{kBasketControllerBasketCompleteResultKey: kBasketControllerBasketCompleteResultCompleted}];
                // refresh config, but only if we weren't launched from a URL
                if (!wasLaunchedFromURL) {
                    [self refreshConfiguration:NO];
                }
            }
        }
        
        
        self.basket = nil;
    }
    // refresh config, but only if we weren't launched from a URL
    if ((!finished) && (![OCGIntegrationController sharedInstance].wasLauchedFromURL)) {
        [self refreshConfiguration:NO];
    }
    [self getActiveBasket];
}

/** @brief Removes the local copy of the basket but DOES NOT retreive the active basket from the web service.
 */
- (void) wipeLocalBasket
{
    if (self.basket != nil)
    {
        self.basket = nil;
    }
    [self kick];
}

/** @brief Finishes the basket off.
 *  @note Calling this method will clear the basket from the server.
 */
- (void) finishBasketWithDeviceStationWithId:(NSString *)deviceStationId
{
    if (self.basket != nil) {
        BOOL isBasketFinishable = YES;
        if (self.clientSettings.DRAFT_finishPredicate)
        {
            NSPredicate *finishPredicate = [NSPredicate predicateWithFormat:self.clientSettings.DRAFT_finishPredicate];
            isBasketFinishable = [finishPredicate evaluateWithObject:self.basket];
        }
        
        if (isBasketFinishable)
        {
            ocg_async_background_overlay(^{
                [RESTController.sharedInstance finishBasketWithBasketID:self.basket.basketID
                                                        deviceStationId:deviceStationId
                                                           emailAddress:self.basket.emailAddress
                                                           emailReceipt:self.basket.emailReceipt
                                                            enablePrint:self.basket.enablePrint || self.clientSettings.autoPrint
                                                    printSummaryReceipt:self.basket.printSummaryReceipt
                                                             tillNumber:self.basket.tillNumber
                                                               complete:^(BasketAssociateDTO *result, NSError *error, ServerError *serverError)
                 {
                     ocg_sync_main(^{
                         // update the basket
                         if (result)
                         {
                             self.basket = result;
                         }
                         self.basket.deviceStationId = deviceStationId;
                         [self kick];
                         
                         if (error != nil || serverError != nil)
                         {
                             // there was an error
                             NSMutableDictionary *userinfo = [NSMutableDictionary dictionary];
                             ServerError *mposerror = serverError;
                             [self kick];
                             
                             [userinfo setValue:mposerror
                                         forKey:kBasketControllerGeneralErrorKey];
                             [userinfo setValue:^(NSString *deviceStationId, NSUInteger errorCode) {
                                 [[BasketController sharedInstance] finishBasketWithDeviceStationWithId:deviceStationId];
                             }
                                         forKey:kBasketControllerControlRetryBlockKey];
                             [[NSNotificationCenter defaultCenter] postNotificationName:kBasketControllerControlErrorNotification
                                                                                 object:self
                                                                               userInfo:userinfo];
                         }
                     });
                 }];
            });
        }
        else
        {
            NSString *message = NSLocalizedString(self.clientSettings.DRAFT_finishPredicateMessage, nil);
            
            if (!message)
            {
                message = [NSString stringWithFormat:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠀫󠁆󠀸󠁏󠁫󠁹󠀵󠁔󠁩󠀹󠀰󠀫󠁚󠁂󠁸󠁂󠁳󠁐󠁉󠁧󠁌󠁕󠁙󠀸󠁉󠁭󠁅󠁿*/ @"Test failed on basket: %@", nil), self.clientSettings.DRAFT_finishPredicate];
            }
            
            UIAlertController *alert =
            [UIAlertController alertControllerWithTitle:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁷󠁣󠀱󠁖󠁪󠁆󠁪󠁲󠀹󠁄󠁙󠁎󠁦󠁖󠁡󠁮󠁺󠁅󠁵󠁵󠁣󠀱󠁇󠀳󠁕󠁅󠁑󠁿*/ @"Finish", nil)
                                                message:message
                                         preferredStyle:UIAlertControllerStyleAlert];
            [alert addAction:[UIAlertAction actionWithTitle:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁄󠁴󠁍󠁉󠁣󠀫󠁺󠁔󠁗󠁦󠁹󠁡󠁯󠀷󠁨󠁔󠁩󠀸󠁯󠀫󠁨󠁘󠀹󠁱󠁌󠀶󠀸󠁿*/ @"OK", nil)
                                                      style:UIAlertActionStyleDefault
                                                    handler:nil]];
            [ServerErrorHandler topMostControllerComplete:^(UIViewController *viewController) {
                [viewController presentViewController:alert
                                             animated:YES
                                           completion:nil];
            }];
        }
    }
}

/** @brief Assigns an email address to the basket.
 *  @param emailAddress The email address to assign to the basket.
 */
- (void) assignEmail:(NSString *)emailAddress
{
    if (!self.isAddingBasketItem) {
        self.isAddingBasketItem = YES;
    }
    
    // call out to the rest client to add the email to the basket
    // create the basket, if required
    [self ensureBasketComplete:^(BasketDTO *basket, NSError *error, ServerError *serverError)
     {
         // check for errors
         if ((error != nil) || (serverError != nil)) {
             // there was an error
             ServerError *mposError = serverError;
             [self kick];
             // send the notification
             [[NSNotificationCenter defaultCenter] postNotificationName:(serverError.overrideErrorMessageCheck) ? kBasketControllerOverrideErrorNotification : kBasketControllerGeneralErrorNotification
                                                                 object:self
                                                               userInfo:@{kBasketControllerGeneralErrorKey:mposError}];
         } else {
             // no errors
             if (self.basket.basketID == nil) {
                 [self.basket copyValuesFromObject:basket];
             }
             // now send the add email request
             ocg_async_background_overlay(^{
                 [RESTController.sharedInstance assignEmailAddressWithBasketID:self.basket.basketID
                                                                  emailAddress:emailAddress
                                                                      complete:^(BasketAssociateDTO *result, NSError *error, ServerError *serverError)
                  {
                      // parse the results on the main queue
                      ocg_sync_main(^{
                          // check for errors
                          if ((error != nil) || (serverError != nil)) {
                              // there was an error
                              ServerError *mposError = serverError;
                              [self kick];
                              // send the notification
                              [[NSNotificationCenter defaultCenter] postNotificationName:(serverError.overrideErrorMessageCheck) ? kBasketControllerOverrideErrorNotification : kBasketControllerGeneralErrorNotification
                                                                                  object:self
                                                                                userInfo:@{kBasketControllerGeneralErrorKey:mposError}];
                          } else {
                              // no errors
                              [self.basket copyValuesFromObject:result];
                              // save
                              [self kick];
                              self.isAddingBasketItem = NO;
                              // send the notification
                              [[NSNotificationCenter defaultCenter] postNotificationName:kBasketControllerBasketItemChangeNotification
                                                                                  object:self];
                              [[NSNotificationCenter defaultCenter] postNotificationName:kBasketControllerBasketItemAddedNotification
                                                                                  object:self];
                          }
                      });
                  }];
             });
         }
     }];
}

- (void) addItem:(MPOSBasketItem *)basketItem completion:(void (^)(void))completion
{
    if (!self.isAddingBasketItem) {
        self.isAddingBasketItem = YES;
    }
    // send the notification
    [[NSNotificationCenter defaultCenter] postNotificationName:kBasketControllerBasketItemChangeNotification
                                                        object:self];
    
    // call out to the rest client to add the item to the basket
    [self ensureBasketComplete:^(BasketDTO *basket, NSError *error, ServerError *serverError)
     {
         // check for errors
         if ((error != nil) || (serverError != nil)) {
             // there was an error
             basketItem.basketItemCreatedError = serverError;
             [self kick];
             // send the notification
             if (serverError.overrideErrorMessageCheck) {
                 [[NSNotificationCenter defaultCenter] postNotificationName:kBasketControllerOverrideErrorNotification
                                                                     object:self
                                                                   userInfo:@{kBasketControllerGeneralErrorKey:serverError}];
             } else {
                 [[NSNotificationCenter defaultCenter] postNotificationName:kBasketControllerBasketAddItemErrorNotification
                                                                     object:self
                                                                   userInfo:@{kBasketControllerBasketItemErrorKey:basketItem.basketItemCreatedError}];
             }
         } else {
             // no errors
             self.basket = basket;
             // now send the add item request
             ocg_async_background_overlay(^{
                 RESTControllerCompleteBlockType processAddItem =
                 ^(RESTInvocation *invocation, NSString*server, id result, NSError *error, ServerError *serverError, NSString *etag)
                 {
                     // parse the results on the main queue
                     ocg_sync_main(^{
                         // check for errors
                         if ((error != nil) || (serverError != nil)) {
                             // there was an error
                             basketItem.basketItemCreatedError = serverError;
                             basketItem.basketItemCreatedError.createdBasketItem = basketItem;
                             [self kick];
                             // send the notification
                             if (serverError.overrideErrorMessageCheck) {
                                 [[NSNotificationCenter defaultCenter] postNotificationName:kBasketControllerOverrideErrorNotification
                                                                                     object:self
                                                                                   userInfo:@{kBasketControllerGeneralErrorKey:serverError}];
                             } else {
                                 [[NSNotificationCenter defaultCenter] postNotificationName:kBasketControllerBasketAddItemErrorNotification
                                                                                     object:self
                                                                                   userInfo:@{kBasketControllerBasketItemErrorKey:basketItem.basketItemCreatedError}];
                             }
                         } else {
                             // no errors
                             self.basket = result;
                             // save
                             [self kick];
                             self.isAddingBasketItem = NO;
                             // send the notification
                             [[NSNotificationCenter defaultCenter] postNotificationName:kBasketControllerBasketItemChangeNotification
                                                                                 object:self];
                             [[NSNotificationCenter defaultCenter] postNotificationName:kBasketControllerBasketItemAddedNotification
                                                                                 object:self];
                             if ([basketItem isKindOfClass:[MPOSBasketCustomerAffiliation class]]) {
                                 // send the affiliation notification
                                 [[NSNotificationCenter defaultCenter] postNotificationName:kBasketControllerCustomerAffiliationAddedNotification
                                                                                     object:self];
                             } else if ([basketItem isKindOfClass:[BasketCustomerCard class]]) {
                                 // send the customer notification
                                 [[NSNotificationCenter defaultCenter] postNotificationName:kBasketControllerCustomerCardAddedNotification
                                                                                     object:self];
                             }
                             
                             if (completion)
                             {
                                 completion();
                             }
                         }
                     });
                 };
                 
                 if ([basketItem isKindOfClass:[MPOSSaleItem class]]) {
                     [RESTController.sharedInstance addItemWithAgeRestriction:basketItem.ageRestriction
                                                                  barcodeType:basketItem.barcodeType
                                                                     basketID:self.basket.basketID
                                                                  entryMethod:basketItem.entryMethod
                                                              itemDescription:nil
                                                                   itemScanID:basketItem.itemScanId
                                                             itemSerialNumber:basketItem.itemSerialNumber
                                                                      measure:basketItem.measure
                                                      originalItemDescription:basketItem.originalItemDescription
                                                           originalItemScanId:basketItem.originalItemScanId
                                                                        price:basketItem.netAmount
                                                                     quantity:basketItem.quantity
                                                                       track1:basketItem.track1
                                                                       track2:basketItem.track2
                                                                       track3:basketItem.track3
                                                                     complete:^(BasketAssociateDTO *result, NSError *error, ServerError *serverError)
                      {
                          processAddItem(nil, nil, result, error, serverError, nil);
                      }];
                 } else if ([basketItem isKindOfClass:[MPOSReturnItem class]]) {
                     MPOSReturnItem *returnItem = (MPOSReturnItem *)basketItem;
                     [RESTController.sharedInstance returnBasketItemWithBasketID:self.basket.basketID
                                                                      itemScanID:returnItem.itemScanId
                                                                itemSerialNumber:returnItem.itemSerialNumber
                                                         originalItemDescription:returnItem.originalItemDescription
                                                              originalItemScanId:returnItem.originalItemScanId
                                                      originalTransactionBarcode:returnItem.originalTransactionBarcode
                                                         originalTransactionDate:returnItem.originalTransactionDate
                                                           originalTransactionId:returnItem.originalTransactionId
                                                      originalTransactionStoreId:returnItem.originalTransactionStoreId
                                                         originalTransactionTill:returnItem.originalTransactionTill
                                                                           price:returnItem.netAmount
                                                                        reasonId:returnItem.reasonId
                                                                        complete:^(BasketAssociateDTO *result, NSError *error, ServerError *serverError)
                      {
                          processAddItem(nil, nil, result, error, serverError, nil);
                      }];
                 } else if ([basketItem isKindOfClass:[MPOSBasketCustomerCard class]]) {
                     MPOSBasketCustomerCard *basketCustomerCard = (MPOSBasketCustomerCard*) basketItem;
                     [RESTController.sharedInstance assignCustomerWithAffiliationType:basketCustomerCard.affiliationType
                                                                             basketID:self.basket.basketID
                                                                             customer:basketCustomerCard.customerCardNumber
                                                                             complete:^(BasketAssociateDTO *result, NSError *error, ServerError *serverError)
                      {
                          processAddItem(nil, nil, result, error, serverError, nil);
                      }];
                 }
             });
         }
     }];
}

- (BOOL) addItemsWithScanIds:(NSArray *)itemScanIds
             andDescriptions:(NSArray *)itemDescriptions
{
    if (itemScanIds.count > 0) {
        if (!self.isAddingBasketItem) {
            self.isAddingBasketItem = YES;
        }
        // send the notification
        [[NSNotificationCenter defaultCenter] postNotificationName:kBasketControllerBasketItemChangeNotification
                                                            object:self];
        // call out to the rest client to add the items to the basket
        [self ensureBasketComplete:^(BasketDTO *basket, NSError *error, ServerError *serverError)
         {
             // check for errors
             if ((error != nil) || (serverError != nil)) {
                 self.isAddingBasketItem = NO;
                 // there was an error
                 ServerError *mposError = serverError;
                 [self kick];
                 // send the notification
                 [[NSNotificationCenter defaultCenter] postNotificationName:(serverError.overrideErrorMessageCheck) ? kBasketControllerOverrideErrorNotification : kBasketControllerGeneralErrorNotification
                                                                     object:self
                                                                   userInfo:@{kBasketControllerGeneralErrorKey:mposError}];
             } else {
                 // no errors
                 if (self.basket.basketID == nil) {
                     [self.basket copyValuesFromObject:basket];
                 }
                 // now send the add items request
                 ocg_async_background_overlay(^{
                     [[RESTController sharedInstance] addItemsWithBasketID:self.basket.basketID
                                                               entryMethod:@"KEYED"
                                                           itemDescription:itemDescriptions
                                                                itemScanID:itemScanIds
                                                                  complete:^(BasketAssociateDTO *result, NSError *error, ServerError *serverError)
                      {
                          // parse the results on the main queue
                          ocg_sync_main(^{
                              // check for errors
                              if ((error != nil) || (serverError != nil)) {
                                  self.isAddingBasketItem = NO;
                                  // there was an error
                                  ServerError *basketItemCreatedError = serverError;
                                  [self kick];
                                  // send the notification
                                  [[NSNotificationCenter defaultCenter] postNotificationName:(serverError.overrideErrorMessageCheck) ? kBasketControllerOverrideErrorNotification : kBasketControllerGeneralErrorNotification
                                                                                      object:self
                                                                                    userInfo:@{kBasketControllerGeneralErrorKey:basketItemCreatedError}];
                              } else {
                                  // no errors
                                  [self.basket copyValuesFromObject:result];
                                  // save
                                  [self kick];
                                  self.isAddingBasketItem = NO;
                                  // send the notification
                                  [[NSNotificationCenter defaultCenter] postNotificationName:kBasketControllerBasketItemChangeNotification
                                                                                      object:self];
                                  [[NSNotificationCenter defaultCenter] postNotificationName:kBasketControllerBasketItemAddedNotification
                                                                                      object:self];
                              }
                          });
                      }];
                 });
             }
         }];
    }
    
    return YES;
}

/** @brief Adds the item with the specified item scan identifier to the specified basket.
 *  @param itemScanId The item scan identifier of the item to be added to the basket.
 *  @param price The price of the item.
 */
- (BOOL) addItemWithScanId: (NSString *)itemScanId
                     price: (NSString *) price
                   measure: (NSString *) measure
                  quantity: (NSString *) quantity
               entryMethod: (NSString *) entryMethod
                    track1: (NSString *) track1
                    track2: (NSString *) track2
                    track3: (NSString *) track3
               barcodeType: (NSString *) barcodeType
{
    NSAssert(self != nil,
             @"The data source has not yet been initialized.");
    NSAssert(self.basket != nil,
             @"The active basket has not yet been initialized.");
    
    // leave if we do not have the requirements to add a item to the basket.
    
    if ((self == nil)
        || (!self.canScanToBasket))
        return NO;
    
    // clear out old unprocessed items
    for (MPOSBasketItem *basketItem in self.basket.basketItems) {
        if ([basketItem.basketItemId length] == 0) {
            ErrorLog(@"Found unprocessed basket item %@", basketItem.itemScanId);
            [self.basket removeBasketItem:basketItem];
        }
    }
    
    MPOSBasketItem *basketItem = [[MPOSSaleItem alloc] init];
    basketItem.itemScanId = itemScanId;
    basketItem.netAmount = price;
    basketItem.measure = measure;
    basketItem.quantity = quantity;
    basketItem.entryMethod = entryMethod;
    basketItem.track1 = track1;
    basketItem.track2 = track2;
    basketItem.track3 = track3;
    basketItem.barcodeType = barcodeType;
    [self.basket addBasketItem:basketItem];
    [self kick];
    
    [self addItem:basketItem completion:NULL];
    return YES;
}

/** @brief Adds the return item with the specified item scan identifier to the specified basket.
 *  @param itemScanId The item scan identifier of the item to be added to the basket.
 *  @param price The price of the item.
 *  @param reason The reason the user selected for the return.
 *  @param originalScanId When returning a dump code item, the original scan id needs to be specified.
 *  @param originalItemDescription When returning a dump code item, the description needs to be specified.
 *  @param barcode The barcode of the original transaction.
 *  @param originalTransactionStoreId The id of the store the item was originally sold in.
 *  @param originalTransactionTill The id of the till the item was originally sold on.
 *  @param originalTransactionId The id of the transaction the item was originally sold in.
 *  @param originalTransactionDate The date the original item was sold on.
 *  @note If the barcode parameter is specified, the return will be processed by RMS.
 *  If the barcode parameter is not specified, the originalTransaction* fields are used.
 *  If neither are specified, the return is not linked back to the original transaction.
 */
- (void) returnItemWithScanId:(NSString *)itemScanId
                     andPrice:(NSNumber *)price
                    forReason:(DiscountReason *)reason
               originalScanId:(NSString *)originalScanId
      originalItemDescription:(NSString *)originalItemDescription
   originalTransactionBarcode:(NSString *)barcode
   originalTransactionStoreId:(NSNumber *)originalTransactionStoreId
      originalTransactionTill:(NSNumber *)originalTransactionTill
        originalTransactionId:(NSNumber *)originalTransactionId
      originalTransactionDate:(NSDate *)originalTransactionDate
{
    NSAssert(self != nil,
             @"The data source has not yet been initialized.");
    NSAssert(self.basket != nil,
             @"The active basket has not yet been initialized.");
    NSAssert(itemScanId != nil,
             @"The item scan id has not been initialized.");
    NSAssert([[itemScanId stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length] > 0,
             @"The item scan id is empty");
    
    // leave if we do not have the requirements to add a item to the basket.
    
    if ((self == nil)
        || (!self.canScanToBasket)
        || (itemScanId == nil)
        || ([[itemScanId stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length] == 0))
        return;
    
    // trigget the service call to request the active basket for the current location.
    
    MPOSReturnItem *basketItem = [[MPOSReturnItem alloc] init];
    basketItem.itemScanId = itemScanId;
    [self.basket addBasketItem:basketItem];
    if (price != nil) {
        // store the price
        basketItem.netAmount = price.stringValue;
    }
    if (reason != nil ) {
        // store the reason
        basketItem.reasonId = reason.reasonId;
    }
    basketItem.originalItemScanId = originalScanId;
    basketItem.originalItemDescription = originalItemDescription;
    if (barcode != nil) {
        // send the barcode
        basketItem.originalTransactionBarcode = barcode;
    } else {
        // no barcode provided, just send each param that's filled out
        if (originalTransactionStoreId != nil) {
            basketItem.originalTransactionStoreId = originalTransactionStoreId;
        }
        if (originalTransactionTill != nil) {
            basketItem.originalTransactionTill = originalTransactionTill;
        }
        if (originalTransactionId != nil) {
            basketItem.originalTransactionId = originalTransactionId;
        }
        if (originalTransactionDate != nil) {
            basketItem.originalTransactionDate = originalTransactionDate;
        }
    }
    
    [self addItem:basketItem completion:NULL];
    
    [self kick];
}

/** @brief Gets the list of return reasons.
 *  @return The list of return reasons or nil if not configured.
 */
- (NSArray *) returnReasons
{
    return [self discountReasonsForGroup:@"Item return reasons"];
}

/** @brief Removes the specified item from the current basket.
 *  @param basketItems An array of items to remove from the basket.
 */
- (void) removeBasketItems:(NSArray *)basketItems
                  complete:(dispatch_block_t)complete
{
    [self removeBasketItems:basketItems
        withDeviceStationId:nil
                   complete:complete];
}

- (void) removeBasketItems:(NSArray *)basketItems
       withDeviceStationId:(NSString *)deviceStationId
                  complete:(dispatch_block_t)complete
{
    NSAssert(self != nil, @"The data source has not yet been initialized.");
    NSAssert(self.basket != nil, @"The active basket has not yet been initialized.");
    NSAssert(self.basket.basketItems != nil, @"The active basket's set of items has not yet been initialized");
    
    if (basketItems.count > 0) {
        // call out to the rest client to delete the items
        ocg_async_background_overlay(^{
            // build a dummy basket
            [RESTController.sharedInstance removeBasketItemWithBasketID:self.basket.basketID
                                                           basketItemID:basketItems
                                                        deviceStationId:deviceStationId
                                                               complete:^(BasketAssociateDTO *result, NSError *error, ServerError *serverError)
             {
                 ocg_sync_main(^{
                     // check for errors
                     if ((error != nil) || (serverError != nil)) {
                         // there was an error
                         if ([serverError deviceStationErrorMessage]) {
                             // there was a device station error
                             NSMutableDictionary *userinfo = [NSMutableDictionary dictionary];
                             [userinfo setValue:serverError
                                         forKey:kBasketControllerGeneralErrorKey];
                             [userinfo setValue:^(NSString *deviceStationId, NSUInteger errorCode) {
                                 [[BasketController sharedInstance] removeBasketItems:basketItems
                                                                  withDeviceStationId:deviceStationId
                                                                             complete:complete];
                             }
                                         forKey:kBasketControllerControlRetryBlockKey];
                             [userinfo setValue:@(NO)
                                         forKey:kBasketControllerControlCancelBlockKey];
                             [[NSNotificationCenter defaultCenter] postNotificationName:kBasketControllerControlErrorNotification
                                                                                 object:self
                                                                               userInfo:userinfo];
                         } else {
                             ServerError *mposerror = serverError;
                             // send the notification
                             [[NSNotificationCenter defaultCenter] postNotificationName:(serverError.overrideErrorMessageCheck) ? kBasketControllerOverrideErrorNotification : kBasketControllerGeneralErrorNotification
                                                                                 object:self
                                                                               userInfo:@{kBasketControllerGeneralErrorKey:mposerror}];
                             // get the active basket
                             if (error == nil) {
                                 [self getActiveBasket];
                             }
                         }
                     } else {
                         // no errors, update the basket
                         self.basket = result;
                         NSAssert(result != nil, @"A basket should always exist, even if it has a nil basketID.");
                         // save
                         [self kick];
                         // send the notification
                         [[NSNotificationCenter defaultCenter] postNotificationName:kBasketControllerBasketItemChangeNotification
                                                                             object:self];
                         // call complete block
                         if (complete) complete();
                     }
                 });
             }];
        });
    }
}

/** @brief Changes the quantity of the specified basket item.
 *  @param basketItem A reference to the basket item of which the quantity should be changed.
 *  @param quantity The new item quantity.
 */
- (void) changeQuantityOfItem: (MPOSBasketItem *)basketItem
                   toQuantity: (int) quantity
{
    ocg_async_background_overlay(^{
        [RESTController.sharedInstance adjustBasketItemQuantityWithBasketID:basketItem.basket.basketID
                                                               basketItemID:basketItem.basketItemId
                                                                   quantity:quantity
                                                                   complete:^(BasketAssociateDTO *result, NSError *error, ServerError *serverError)
         {
             ocg_sync_main(^{
                 if (error != nil || serverError != nil)
                 {
                     ServerError *mposError = serverError;
                     
                     // there was an error
                     // send the notification
                     [[NSNotificationCenter defaultCenter] postNotificationName:(serverError.overrideErrorMessageCheck) ? kBasketControllerOverrideErrorNotification : kBasketControllerGeneralErrorNotification
                                                                         object:self
                                                                       userInfo:@{kBasketControllerGeneralErrorKey:mposError}];
                 }
                 
                 if (result != nil)
                 {
                     // no errors, update the basket
                     // import the new basket
                     [self kick];
                     self.basket = result;
                     // send the notification
                     [[NSNotificationCenter defaultCenter] postNotificationName:kBasketControllerBasketItemChangeNotification
                                                                         object:self];
                     
                 }
             });
         }];
    });
}

/** @brief Changes the operator id of the basket items specified.
 *  @param basketItems The items to change.
 *  @param operatorId The new operator id to assign to the items.
 */
- (void) modifyBasketItems:(NSArray *)basketItems
               salesPerson:(NSString *)operatorId
{
    ocg_async_background_overlay(^{
        [RESTController.sharedInstance adjustBasketItemWithBasketID:self.basket.basketID
                                                       basketItemID:basketItems
                                                         operatorID:operatorId
                                                           complete:^(BasketAssociateDTO *result, NSError *error, ServerError *serverError)
         {
             ocg_sync_main(^{
                 // check for errors
                 if (result != nil) {
                     // no errors
                     [self.basket copyValuesFromObject:result];
                     [self kick];
                     self.basket = result;
                     // send the notification
                     [[NSNotificationCenter defaultCenter] postNotificationName:kBasketControllerBasketItemChangeNotification
                                                                         object:self];
                 } else {
                     // there was an error
                     [self kick];
                     // send the notification
                     [[NSNotificationCenter defaultCenter] postNotificationName:(serverError.overrideErrorMessageCheck) ? kBasketControllerOverrideErrorNotification : kBasketControllerGeneralErrorNotification
                                                                         object:self
                                                                       userInfo:@{kBasketControllerGeneralErrorKey:serverError}];
                     // get the active basket
                     if (error == nil) {
                         [self getActiveBasket];
                     }
                 }
             });
         }];
    });
}

/** @brief Marks the specified basket items to be printed on the gift receipt.
 */
- (void) markBasketItemsForGiftReceipt:(NSArray *)basketItems
{
    static dispatch_semaphore_t _basketEditSemaphore;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _basketEditSemaphore = dispatch_semaphore_create(1);
    });
    if (dispatch_semaphore_wait(_basketEditSemaphore, DISPATCH_TIME_NOW) == 0) {
        DebugLog(@"markBasketItemsForGiftReceipt:");
        
        // update the basket items
        MPOSBasket *basket = self.basket;
        for (MPOSSaleItem *basketItem in basket.basketItems) {
            if ([basketItem respondsToSelector:@selector(generateGiftReceiptValue)]) {
                BOOL selected = [basketItems containsObject:basketItem.basketItemId];
                if (basketItem.generateGiftReceiptValue != selected) {
                    basketItem.generateGiftReceiptValue = selected;
                }
            }
        }
        [self kick];
        // call out to the rest client to discount the basket
        ocg_async_background_overlay(^{
            // send the request
            [RESTController.sharedInstance updateGiftReceiptWithBasket:(BasketAssociateDTO*)basket
                                                              complete:^(BasketAssociateDTO *result, NSError *error, ServerError *serverError)
             {
                 // parse the results on the main queue
                 ocg_sync_main(^{
                     // check for errors
                     if ((error != nil) || (serverError != nil)) {
                         // there was an error
                         ServerError *mposerror = serverError;
                         // send the notification
                         [[NSNotificationCenter defaultCenter] postNotificationName:(serverError.overrideErrorMessageCheck) ? kBasketControllerOverrideErrorNotification : kBasketControllerGeneralErrorNotification
                                                                             object:self
                                                                           userInfo:@{kBasketControllerGeneralErrorKey:mposerror}];
                         // get the active basket
                         if (error == nil) {
                             [self getActiveBasket];
                         }
                     } else {
                         // no errors, update the basket
                         // import the new basket
                         // save
                         [self kick];
                         self.basket = result;
                         NSAssert(result != nil, @"A basket should always exist, even if it has a nil basketID.");
                         // send the notification
                         [[NSNotificationCenter defaultCenter] postNotificationName:kBasketControllerBasketItemChangeNotification
                                                                             object:self];
                     }
                     dispatch_semaphore_signal(_basketEditSemaphore);
                 });
             }];
        });
    }
}

/** @brief Returns an instance of a number formatter to be used on quantity values.
 *  @return An instance of a number formatter to be used on quantity values.
 */
- (NSNumberFormatter *) quantityNumberFormatter
{
    NSNumberFormatter *valueFormatter = [[NSNumberFormatter alloc] init];
    [valueFormatter setMaximumFractionDigits: 0];
    [valueFormatter setPositiveFormat: @"0"];
    
    return valueFormatter;
}

/** @brief Applies a discount to the current basket.
 *  @param amount The discount value.
 *  @param type The type of the discount (currency, percentage).
 *  @param reason The discount reason associated to the discount.
 *  @param description The reason description for the discount.
 */
- (void) applyDiscountWithAmount:(NSString *)amount
                          ofType:(NSString *)type
                        withCode:(DiscountCode *)discountCode
                      withReason:(DiscountReason *)discountReason
                  andDescription:(NSString *)description
{
    NSAssert(self != nil, @"The data source has not yet been initialized.");
    NSAssert(self.basket != nil, @"The active basket has not yet been initialized.");
    
    if (!self.isAddingBasketItem) {
        self.isAddingBasketItem = YES;
    }
    // send the notification
    [[NSNotificationCenter defaultCenter] postNotificationName:kBasketControllerBasketItemChangeNotification
                                                        object:self];
    
    // call out to the rest client to discount the basket
    ocg_async_background_overlay(^{
        [self ensureBasketComplete:^(BasketDTO *basket, NSError *error, ServerError *serverError) {
            // only continue if there were no errors
            if (serverError == nil && error == nil) {
                [RESTController.sharedInstance applyBasketDiscountWithAmount:amount
                                                                    basketID:self.basket.basketID
                                                                discountCode:discountCode.discountCodeID
                                                                discountType:type
                                                           reasonDescription:description
                                                                    reasonID:discountReason.reasonId
                                                                    complete:^(BasketAssociateDTO *result, NSError *error, ServerError *serverError)
                 {
                     // parse the results on the main queue
                     ocg_sync_main(^{
                         // check for errors
                         if ((error != nil) || (serverError != nil)) {
                             self.isAddingBasketItem = NO;
                             // there was an error
                             NSMutableDictionary *userinfo = [NSMutableDictionary dictionary];
                             ServerError *mposerror = serverError;
                             [userinfo setValue:mposerror
                                         forKey:kBasketControllerGeneralErrorKey];
                             [userinfo setValue:^{
                                 [[BasketController sharedInstance] applyDiscountWithAmount:amount
                                                                                     ofType:type
                                                                                   withCode:discountCode
                                                                                 withReason:discountReason
                                                                             andDescription:description];
                             }
                                         forKey:kBasketControllerRetryBlockKey];
                             
                             
                             // send the notification
                             [[NSNotificationCenter defaultCenter] postNotificationName:(serverError.overrideErrorMessageCheck) ? kBasketControllerOverrideErrorNotification : kBasketControllerGeneralErrorNotification
                                                                                 object:self
                                                                               userInfo:userinfo];
                         } else {
                             // no errors, update the basket
                             // import the new basket
                             self.basket = result;
                             NSAssert(result != nil, @"A basket should always exist, even if it has a nil basketID.");
                             // save
                             [self kick];
                             self.isAddingBasketItem = NO;
                             // send the notification
                             [[NSNotificationCenter defaultCenter] postNotificationName:kBasketControllerDiscountChangeNotification
                                                                                 object:self];
                             [[NSNotificationCenter defaultCenter] postNotificationName:kBasketControllerBasketItemChangeNotification
                                                                                 object:self];
                             [[NSNotificationCenter defaultCenter] postNotificationName:kBasketControllerBasketItemAddedNotification
                                                                                 object:self];
                         }
                     });
                 }];
            }
        }];
    });
}

/** @brief Removes a discount from the basket.
 *  @param discount The reference to the discount to remove.
 */
- (void) removeDiscount:(MPOSBasketRewardSale *)discount
{
    // don't allow promotion discount deletion
    if (![discount isKindOfClass:[MPOSBasketRewardSalePromotionAmount class]]) {
        [self removeBasketItems:@[discount.basketItemId]
                       complete:nil];
    }
}

/** @brief Applies a discount to the specified basket item.
 *  @param basketItem A reference to the basket item the discount refers to.
 *  @param amount The discount value.
 *  @param type The type of the discount (currency, percentage).
 *  @param reason The discount reason associated to the discount.
 *  @param description The reason description for the discount.
 */
- (void) adjustBasketItem:(MPOSBasketItem *)basketItem
                withPrice:(NSString *)price
               withReason:(DiscountReason *)discountReason
           andDescription:(NSString *)reasonDescription
{
    ocg_async_background_overlay(^{
        [RESTController.sharedInstance adjustBasketItemPriceWithBasketID:basketItem.basket.basketID
                                                            basketItemID:basketItem.basketItemId
                                                                   price:price
                                                       reasonDescription:reasonDescription
                                                                reasonID:discountReason.reasonId
                                                                complete:^(BasketAssociateDTO *result, NSError *error, ServerError *serverError)
         {
             ocg_sync_main(^{
                 if (error != nil || serverError != nil)
                 {
                     ServerError *mposError = serverError;
                     
                     // there was an error
                     // send the notification
                     [[NSNotificationCenter defaultCenter] postNotificationName:(serverError.overrideErrorMessageCheck) ? kBasketControllerOverrideErrorNotification : kBasketControllerGeneralErrorNotification
                                                                         object:self
                                                                       userInfo:@{kBasketControllerGeneralErrorKey:mposError}];
                 }
                 
                 if (result != nil)
                 {
                     // no errors, update the basket
                     // import the new basket
                     self.basket = result;
                     // save
                     [self kick];
                     // send the notification
                     [[NSNotificationCenter defaultCenter] postNotificationName:kBasketControllerBasketItemChangeNotification
                                                                         object:self];
                     
                 }
             });
         }];
    });
}

/** @brief Applies a discount to the specified basket items.
 *  @param basketItems An array of basket items to apply the discount to.
 *  @param amount The discount value.
 *  @param type The type of the discount (currency, percentage).
 *  @param reason The discount reason associated to the discount.
 *  @param description The reason description for the discount.
 */
- (void) applyDiscountToBasketItems:(NSArray *)basketItems
                         withAmount:(NSString *)amount
                             ofType:(NSString *)type
                           withCode:(DiscountCode *)discountCode
                         withReason:(DiscountReason *)discountReason
                     andDescription:(NSString *)description
{
    NSAssert(self != nil, @"The data source has not yet been initialized.");
    NSAssert(self.basket != nil, @"The active basket has not yet been initialized.");
    NSAssert(self.basket.basketItems != nil, @"The active basket's set of items has not yet been initialized");
    
    
    // call out to the rest client to discount the basket
    ocg_async_background_overlay(^{
        [RESTController.sharedInstance applyBasketItemDiscountWithAmount:amount
                                                                basketID:self.basket.basketID
                                                            basketItemID:basketItems
                                                            discountCode:discountCode.discountCodeID
                                                            discountType:type
                                                       reasonDescription:description
                                                                reasonID:discountReason.reasonId
                                                                complete:^(BasketAssociateDTO *result, NSError *error, ServerError *serverError)
         {
             // parse the results on the main queue
             ocg_sync_main(^{
                 // check for errors
                 if ((error != nil) || (serverError != nil)) {
                     // there was an error
                     ServerError *mposerror = serverError;
                     // send the notification
                     [[NSNotificationCenter defaultCenter] postNotificationName:(serverError.overrideErrorMessageCheck) ? kBasketControllerOverrideErrorNotification : kBasketControllerGeneralErrorNotification
                                                                         object:self
                                                                       userInfo:@{kBasketControllerGeneralErrorKey:mposerror}];
                     // get the active basket
                     if (error == nil) {
                         [self getActiveBasket];
                     }
                 } else {
                     // no errors, update the basket
                     // import the new basket
                     self.basket = result;
                     NSAssert(result != nil, @"A basket should always exist, even if it has a nil basketID.");
                     // save
                     [self kick];
                     // send the notification
                     [[NSNotificationCenter defaultCenter] postNotificationName:kBasketControllerDiscountChangeNotification
                                                                         object:self];
                 }
             });
         }];
    });
}

/** @brief Removes the discount from the specified basket item.
 *  @param discount A reference to the discount to remove.
 *  @param basketItem A reference to the basket item.
 */
- (void) removeDiscount:(MPOSBasketRewardLine *)discount
         fromBasketItem:(MPOSBasketItem *)basketItem
{
    NSAssert(self != nil, @"The data source has not yet been initialized.");
    NSAssert(self.basket != nil, @"The active basket has not yet been initialized.");
    NSAssert(self.basket.basketItems != nil, @"The active basket's set of items has not yet been initialized");
    NSAssert(discount != nil, @"The reference to the discount to be removed has not been specified.");
    NSAssert(discount.basketItem != nil, @"The discount has no reference to the basketItem.");
    NSAssert(discount.basketItem.basketItemId != nil, @"The basketItem referenced by the discount has not unique identifier.");
    NSAssert([self.basket.basketID isEqualToString:discount.basketItem.basket.basketID], @"The basket referenced by the discount does not match the basket id in the data source.");
    NSAssert(discount.basketItem == basketItem, @"The basket item referenced by the discount does not match the basket item specified in the method call.");
    
    
    // call out to the rest client remove the discount from the basket
    ocg_async_background_overlay(^{
        [RESTController.sharedInstance removeBasketItemDiscountWithBasketID:self.basket.basketID
                                                       basketItemDiscountID:@[discount.rewardId]
                                                               basketItemID:basketItem.basketItemId
                                                                   complete:^(BasketAssociateDTO *result, NSError *error, ServerError *serverError)
         {
             // parse the results on the main queue
             ocg_sync_main(^{
                 // check for errors
                 if ((error != nil) || (serverError != nil)) {
                     // there was an error
                     ServerError *mposerror = serverError;
                     // send the notification
                     [[NSNotificationCenter defaultCenter] postNotificationName:(serverError.overrideErrorMessageCheck) ? kBasketControllerOverrideErrorNotification : kBasketControllerGeneralErrorNotification
                                                                         object:self
                                                                       userInfo:@{kBasketControllerGeneralErrorKey:mposerror}];
                 } else {
                     // no errors, update the basket
                     // import the new basket
                     self.basket = result;
                     NSAssert(result != nil, @"A basket should always exist, even if it has a nil basketID.");
                     // save
                     [self kick];
                     // send the notification
                     [[NSNotificationCenter defaultCenter] postNotificationName:kBasketControllerDiscountChangeNotification
                                                                         object:self];
                 }
             });
         }];
    });
}

/** @brief Clears all items from the currently active basket.
 */
- (void) clearBasket
{
    NSAssert(self != nil, @"The data source has not yet been initialized.");
    NSAssert(self.basket != nil, @"The active basket has not yet been initialized.");
    NSAssert(self.basket.basketItems != nil, @"The active basket's set of items has not yet been initialized");
    
    // roll back EFT transactions
    BOOL foundEFT = NO;
    if ([OCGEFTManager sharedInstance].configured) {
        DebugLog(@"Clearing basket, checking for EFT payments to void");
        for (BasketItem *item in self.basket.basketItems) {
            if ([item isKindOfClass:[BasketTender class]]) {
                BasketTender *payment = (BasketTender *)item;
                Tender *tender = self.tenders[payment.tenderType];
                if (tender.isEFTTender && payment.authorization && !payment.authorization.providerVoided) {
                    DebugLog(@"Found EFT payment to void: %@", payment);
                    foundEFT = YES;
                    // roll back
                    [[OCGEFTManager sharedInstance] voidTransaction:(EFTPayment *)payment
                                                           complete:^(EFTProviderErrorCode resultCode, NSError *error, BOOL suppressError) {
                                                               if (kEFTProviderErrorCodeSuccess == resultCode) {
                                                                   payment.authorization = nil;
                                                                   dispatch_async(dispatch_get_main_queue(), ^{
                                                                       [self clearBasket];
                                                                   });
                                                               } else {
                                                                   UIAlertController *alert =
                                                                   [UIAlertController alertControllerWithTitle:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁫󠀵󠁉󠁤󠁘󠁖󠁏󠁁󠁆󠀷󠁬󠀯󠁯󠁇󠀲󠀳󠁵󠁢󠀫󠁘󠁦󠀯󠁩󠁁󠁦󠁒󠁑󠁿*/ @"Error", nil)
                                                                                                       message:[NSString stringWithFormat:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁙󠀹󠁖󠁄󠀲󠁲󠁕󠁒󠁫󠀷󠁗󠁁󠁗󠁣󠁢󠀹󠁨󠁗󠁐󠁅󠁐󠁘󠁖󠁺󠁓󠁦󠁫󠁿*/ @"Could not void payment %@", nil), payment.authorization.authorizationCode]
                                                                                                preferredStyle:UIAlertControllerStyleAlert];
                                                                   [alert addAction:[UIAlertAction actionWithTitle:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁄󠁴󠁍󠁉󠁣󠀫󠁺󠁔󠁗󠁦󠁹󠁡󠁯󠀷󠁨󠁔󠁩󠀸󠁯󠀫󠁨󠁘󠀹󠁱󠁌󠀶󠀸󠁿*/ @"OK", nil)
                                                                                                             style:UIAlertActionStyleDefault
                                                                                                           handler:nil]];
                                                                   [ServerErrorHandler topMostControllerComplete:^(UIViewController *viewController) {
                                                                       [viewController presentViewController:alert
                                                                                                    animated:YES
                                                                                                  completion:nil];
                                                                   }];
                                                               }
                                                           }];
                    break;
                }
            }
        }
    }
    
    if (!foundEFT) {
        // delete voided payments
        if ([OCGEFTManager sharedInstance].configured) {
            NSMutableArray *tenderItemsToDelete = [NSMutableArray array];
            for (BasketItem *item in self.basket.basketItems) {
                if ([item isKindOfClass:[BasketTender class]]) {
                    BasketTender *payment = (BasketTender *)item;
                    Tender *tender = self.tenders[payment.tenderType];
                    if (tender.isEFTTender && payment.authorization.providerVoided) {
                        DebugLog(@"Found voided EFT payment %@", payment);
                        [tenderItemsToDelete addObject:payment.basketItemId];
                    }
                }
            }
            if (tenderItemsToDelete.count > 0) {
                [[BasketController sharedInstance] removeBasketItems:tenderItemsToDelete
                                                            complete:^{
                                                                dispatch_async(dispatch_get_main_queue(), ^{
                                                                    [self clearBasket];
                                                                });
                                                            }];
                foundEFT = YES;
            }
        }
        if (!foundEFT) {
            // call out to the rest client to cancel the basket
            ocg_async_background_overlay(^{
                // cancel
                [RESTController.sharedInstance cancelBasketWithBasketID:self.basket.basketID
                                                               complete:^(BasketAssociateDTO *result, NSError *error, ServerError *serverError)
                 {
                     // parse the results on the main queue
                     ocg_sync_main(^{
                         // check for errors
                         if ((error != nil) || (serverError != nil)) {
                             // there was an error
                             ServerError *mposerror = serverError;
                             // send the notification
                             [[NSNotificationCenter defaultCenter] postNotificationName:(serverError.overrideErrorMessageCheck) ? kBasketControllerOverrideErrorNotification : kBasketControllerGeneralErrorNotification
                                                                                 object:self
                                                                               userInfo:@{kBasketControllerGeneralErrorKey:mposerror}];
                         }
                         else
                         {
                             // no errors, update the basket
                             // import the new basket
                             self.basket = result;
                             NSAssert(result != nil, @"A basket should always exist, even if it has a nil basketID.");
                             // save
                             [self kick];
                         }
                     });
                 }];
            });
        }
    }
}

/** @brief Suspends the current basket.
 */
- (void) suspendBasketWithDescription:(NSString *)basketDescription
{
    NSAssert(self != nil, @"The data source has not yet been initialized.");
    NSAssert(self.basket != nil, @"The active basket has not yet been initialized.");
    NSAssert(self.basket.basketItems != nil, @"The active basket's set of items has not yet been initialized");
    
    // call out to the rest client to suspend the basket
    ocg_async_background_overlay(^{
        RESTControllerCompleteBlockType parseSuspendResult = ^(RESTInvocation *invocation, NSString*server, id result, NSError *error, ServerError *serverError, NSString *etag)
        {
            // parse the results on the main queue
            ocg_sync_main(^{
                // check for errors
                if ((error != nil) || (serverError != nil)) {
                    // there was an error
                    ServerError *mposerror = serverError;
                    // send the notification
                    [[NSNotificationCenter defaultCenter] postNotificationName:(serverError.overrideErrorMessageCheck) ? kBasketControllerOverrideErrorNotification : kBasketControllerGeneralErrorNotification
                                                                        object:self
                                                                      userInfo:@{kBasketControllerGeneralErrorKey:mposerror}];
                }
                else
                {
                    self.basket = result;
                    [self kick];
                }
            });
        };
        
        // if we have items in the basket
        if ([self.basket.basketItems count] > 0) {
            // suspend
            [RESTController.sharedInstance suspendBasketWithBasketDescription:basketDescription
                                                                     basketID:self.basket.basketID
                                                                     complete:^(BasketAssociateDTO *result, NSError *error, ServerError *serverError)
             {
                 parseSuspendResult(nil, nil, result, error, serverError, nil);
             }];
        } else {
            // just cancel
            [RESTController.sharedInstance cancelBasketWithBasketID:self.basket.basketID
                                                           complete:^(BasketAssociateDTO *result, NSError *error, ServerError *serverError)
             {
                 parseSuspendResult(nil, nil, result, error, serverError, nil);
             }];
        }
    });
}

/** @brief Resume the basket with the specified basket key.
 *  @param basketId The unique identifier of the basket to be recalled.
 *  @param basketBarcode The unique barcode number of the basket to be recalled.
 *  @param tillNumber The till the basket was suspended on.
 */
- (void) resumeBasket:(NSString *)basketId
          fromBarcode:(NSString *)basketBarcode
       withTillNumber:(NSString *)tillNumber
              success:(dispatch_block_t)successBlock
{
    NSAssert(self != nil, @"The data source has not yet been initialized.");
    
    
    // call out to the rest client to suspend the basket
    ocg_async_background_overlay(^{
        NSString *uniqueDeviceIdentifier = [[UIDevice currentDevice] uniqueDeviceIdentifier];
        // resume the basket
        [RESTController.sharedInstance resumeBasketWithBasketBarcode:basketBarcode
                                                            basketID:basketId
                                                            deviceID:uniqueDeviceIdentifier
                                                          tillNumber:tillNumber
                                                            complete:^(BasketAssociateDTO *result, NSError *error, ServerError *serverError)
         {
             // parse the results on the main queue
             dispatch_async(dispatch_get_main_queue(), ^{
                 // check for errors
                 if ((error != nil) || (serverError != nil)) {
                     // there was an error
                     ServerError *mposerror = serverError;
                     // send the notification
                     [[NSNotificationCenter defaultCenter] postNotificationName:(serverError.overrideErrorMessageCheck) ? kBasketControllerOverrideErrorNotification : kBasketControllerGeneralErrorNotification
                                                                         object:self
                                                                       userInfo:@{kBasketControllerGeneralErrorKey:mposerror}];
                 } else {
                     // no errors
                     self.basket = result;
                     // send the notification
                     [[NSNotificationCenter defaultCenter] postNotificationName:kBasketControllerBasketChangeNotification
                                                                         object:self];
                     if (successBlock)
                     {
                         successBlock();
                     }
                 }
             });
         }];
    });
}

/** @brief Checks out the current basket.
 */
- (void) checkoutBasket
{
    NSAssert(self != nil, @"The data source has not yet been initialized.");
    NSAssert(self.basket != nil, @"The active basket has not yet been initialized.");
    NSAssert(self.canCheckOut, @"The active basket is not marked as being available for check/out");
    
    // call out to the rest client to check out the basket
    ocg_async_background_overlay(^{
        [RESTController.sharedInstance checkoutBasketWithBasketID:self.basket.basketID
                                                     currencyCode:self.basket.currencyCode
                                                         complete:^(BasketAssociateDTO *result, NSError *error, ServerError *serverError)
         {
             // parse the results on the main queue
             ocg_sync_main(^{
                 
                 // check for errors
                 if ((error != nil) || (serverError != nil)) {
                     // there was an error
                     ServerError *mposerror = serverError;
                     // send the notification
                     if (serverError.overrideErrorMessageCheck) {
                         [[NSNotificationCenter defaultCenter] postNotificationName:kBasketControllerOverrideErrorNotification
                                                                             object:self
                                                                           userInfo:@{kBasketControllerGeneralErrorKey:mposerror}];
                     } else {
                         [[NSNotificationCenter defaultCenter] postNotificationName:kBasketControllerGeneralErrorNotification
                                                                             object:self
                                                                           userInfo:@{kBasketControllerGeneralErrorKey:mposerror}];
                         [[NSNotificationCenter defaultCenter] postNotificationName:kBasketControllerBasketCheckedOutErrorNotification
                                                                             object:self];
                     }
                 } else {
                     // no errors, update the basket
                     // import the new basket
                     self.basket = result;
                     NSAssert(result != nil, @"A basket should always exist, even if it has a nil basketID.");
                     // save
                     [self kick];
                     // send the notification
                     [[NSNotificationCenter defaultCenter] postNotificationName:kBasketControllerBasketCheckedOutChangeNotification
                                                                         object:self];
                 }
             });
         }];
    });
}

/** @brief Cancels the checkout state for the specified basket id.
 */
- (void) cancelBasketCheckout
{
    NSAssert(self != nil, @"The data source has not yet been initialized.");
    NSAssert(self.basket != nil, @"The active basket has not yet been initialized.");
    NSAssert(self.isCheckedOut, @"The active basket is not marked as being available for undo check/out");
    
    // call out to the rest client to cancel check out
    ocg_async_background_overlay(^{
        [RESTController.sharedInstance cancelCheckoutBasketWithBasketID:self.basket.basketID
                                                               complete:^(BasketAssociateDTO *restBasket, NSError *error, ServerError *serverError)
         {
             // parse the results on the main queue
             ocg_sync_main(^{
                 
                 // check for errors
                 if ((error != nil) || (serverError != nil)) {
                     // there was an error
                     ServerError *mposerror = serverError;
                     // send the notification
                     [[NSNotificationCenter defaultCenter] postNotificationName:(serverError.overrideErrorMessageCheck) ? kBasketControllerOverrideErrorNotification : kBasketControllerGeneralErrorNotification
                                                                         object:self
                                                                       userInfo:@{kBasketControllerGeneralErrorKey:mposerror}];
                 } else {
                     // no errors, update the basket
                     // import the new basket
                     self.basket = restBasket;
                     NSAssert(restBasket != nil, @"A basket should always exist, even if it has a nil basketID.");
                     // save
                     [self kick];
                     // send the notification
                     [[NSNotificationCenter defaultCenter] postNotificationName:kBasketControllerBasketCheckedOutChangeNotification
                                                                         object:self];
                 }
             });
         }];
    });
}

/** @brief Sets whether the user requires a gift receipt and
 *  gets the gift receipt for the basket (if not already loaded).
 *  @param required Whether the receipt is required or not.
 */
- (void) setGiftReceiptRequired:(BOOL)required
{
    NSAssert(self.basket != nil, @"The active basket has not yet been initialized.");
    
    
    if (self.basket.giftReceiptRequired != required)
    {
        self.basket.giftReceiptRequired = required;
    }
    
    [self kick];
}

/** @brief Sends the receipt to the specified email.
 *  @param emailAddress The email address to which the receipt should be send.
 *  @param complete The block to execute after the email request is sent.
 */
- (void) sendReceiptToEmail:(NSString *)emailAddress
                   complete:(dispatch_block_t)complete
{
    NSAssert(self != nil, @"The data source has not yet been initialized.");
    NSAssert(self.basket != nil, @"The active basket has not yet been initialized.");
    
    
    // call out to the rest client to send email
    ocg_async_background_overlay(^{
        [RESTController.sharedInstance createCustomerEmailWithEmailAddress:emailAddress
                                                        includeGiftReceipt:self.basket.giftReceiptRequired
                                                                 receiptID:self.basket.receipt
                                                                  complete:^(NSError *error, ServerError *serverError)
         {
             // parse the results on the main queue
             ocg_sync_main(^{
                 // check for errors
                 if ((error != nil) || (serverError != nil)) {
                     // there was an error
                     ServerError *mposerror = serverError;
                     // send the notification
                     [[NSNotificationCenter defaultCenter] postNotificationName:(serverError.overrideErrorMessageCheck) ? kBasketControllerOverrideErrorNotification : kBasketControllerGeneralErrorNotification
                                                                         object:self
                                                                       userInfo:@{kBasketControllerGeneralErrorKey:mposerror}];
                 } else {
                     if (complete) {
                         complete();
                     }
                 }
             });
         }];
    });
}

/** @brief Reprints the last receipt.
 */
- (void) reprintLastTxToDeviceStation:(NSString *)deviceStationId
{
    ocg_async_background_overlay(^{
        [RESTController.sharedInstance reprintLastTxWithDeviceStationId:deviceStationId
                                                               complete:^(BasketAssociateDTO *result, NSError *error, ServerError *serverError)
         {
             // check for errors
             ocg_sync_main(^{
                 
                 if ((error != nil) || (serverError != nil)) {
                     // there was an error
                     NSMutableDictionary *userinfo = [NSMutableDictionary dictionary];
                     ServerError *mposerror = serverError;
                     [userinfo setValue:mposerror
                                 forKey:kBasketControllerGeneralErrorKey];
                     [userinfo setValue:^(NSString *deviceStationId, NSUInteger errorCode) {
                         [[BasketController sharedInstance] reprintLastTxToDeviceStation:deviceStationId];
                     }
                                 forKey:kBasketControllerControlRetryBlockKey];
                     [[NSNotificationCenter defaultCenter] postNotificationName:kBasketControllerControlErrorNotification
                                                                         object:self
                                                                       userInfo:userinfo];
                 }
                 else {
                     // no errors, update the basket
                     // import the new basket
                     [self kick];
                     self.basket = result;
                     NSAssert(result != nil, @"A basket should always exist, even if it has a nil basketID.");
                     // save
                     // send the notification
                     [[NSNotificationCenter defaultCenter] postNotificationName:kBasketControllerBasketChangeNotification
                                                                         object:self];
                 }
             });
         }];
    });
}

/** @brief Opens the cash drawer on the device station specified.
 *  @param deviceStationId The device station to open the cash drawer on.
 */
- (void) openCashDrawerOnDeviceStation:(NSString *)deviceStationId
{
    ocg_async_background_overlay(^{
        [RESTController.sharedInstance openCashDrawerWithDeviceStationId:deviceStationId
                                                                complete:^(NSError *error, ServerError *serverError)
         {
             // check for errors
             if ((error != nil) || (serverError != nil)) {
                 ocg_sync_main(^{
                     // there was an error
                     self.basket.deviceStationId = deviceStationId;
                     NSMutableDictionary *userinfo = [NSMutableDictionary dictionary];
                     ServerError *mposerror = serverError;
                     [userinfo setValue:mposerror
                                 forKey:kBasketControllerGeneralErrorKey];
                     [userinfo setValue:^(NSString *deviceStationId, NSUInteger errorCode) {
                         // TFS67011 - turns out not calling open cash drawer when a till insert is not assigned is
                         // a bad idea, even if we do fire the drawer twice
                         //                    if (errorCode != MShopperDeviceStationDrawerNotAssignedExceptionCode) {
                         [[BasketController sharedInstance] openCashDrawerOnDeviceStation:deviceStationId];
                         //                    }
                     }
                                 forKey:kBasketControllerControlRetryBlockKey];
                     [[NSNotificationCenter defaultCenter] postNotificationName:kBasketControllerControlErrorNotification
                                                                         object:self
                                                                       userInfo:userinfo];
                 });
             }
         }];
    });
}

- (void)ensureBasketComplete:(void (^)(BasketDTO *basket, NSError *error, ServerError *serverError))complete
{
    BasketDTO *result = nil;
    NSString *basketID = self.basket.basketID;
    
    if (basketID == nil)
    {
        ocg_async_background_overlay(^{
            [RESTController.sharedInstance createBasketWithBasketID:nil
                                                        businessDay:nil
                                                           deviceID:[[UIDevice currentDevice] uniqueDeviceIdentifier]
                                                    issuerCountryId:nil
                                                    lastTransaction:nil
                                                           scanData:nil
                                                            storeID:[CRSLocationController getLocationKey]
                                                    transactionType:self.basket.basketType
                                                      workstationID:[TillSetupController getTillNumber]
                                                           complete:^(BasketAssociateDTO *result, NSError *error, ServerError *serverError)
             {
                 if (result)
                 {
                     ocg_sync_main(^{
                         self.basket = result;
                         [self kick];
                         if (complete) complete(result, error, serverError);
                     });
                 }
                 else if ((error != nil) || (serverError != nil))
                 {
                     // there was an error, check if it is a basket not found
                     BOOL basketNotFound = NO;
                     for (ServerErrorMessage *restServerErrorMessage in serverError.messages) {
                         if ((restServerErrorMessage != nil) &&
                             ([restServerErrorMessage.code intValue] == kServerErrorMessageBasketNotFound)) {
                             basketNotFound = YES;
                             break;
                         }
                     }
                     // it is
                     if (basketNotFound) {
                         // try get active basket
                         NSString *uniqueDeviceIdentifier = [[UIDevice currentDevice] uniqueDeviceIdentifier];
                         [RESTController.sharedInstance getActiveBasketWithDeviceID:uniqueDeviceIdentifier
                                                                           complete:^(BasketAssociateDTO *result, NSError *error, ServerError *serverError)
                          {
                              ocg_sync_main(^{
                                  self.basket = result;
                                  [self kick];
                                  if (complete) complete(result, error, serverError);
                              });
                          }];
                     } else {
                         ocg_sync_main(^{
                             if (complete) complete(nil, error, serverError);
                         });
                     }
                 }
             }];
        });
    } else {
        if (complete) complete(self.basket, nil, nil);
    }
}

- (void) noReceiptWithItemScanId:(NSString*)itemScanId
                           price:(NSString*)price
                        reasonId:(NSString*)reasonId
               reasonDescription:(NSString*)reasonDescription
                itemSerialNumber:(NSString*)itemSerialNumber
              originalItemScanId:(NSString*)originalItemScanId
         originalItemDescription:(NSString*)originalItemDescription
                        complete:(void (^)(BasketAssociateDTO* result, NSError* error, ServerError* serverError))complete
{
    __block BasketDTO *result = nil;
    __block NSString *basketID = self.basket.basketID;
    __block NSError *error = nil;
    __block ServerError *serverError = nil;
    
    NSLock *lock = [NSLock new];
    
    if (basketID == nil)
    {
        [lock lock];
        [RESTController.sharedInstance createBasketWithBasketID:nil
                                                    businessDay:nil
                                                       deviceID:[[UIDevice currentDevice] uniqueDeviceIdentifier]
                                                issuerCountryId:nil
                                                lastTransaction:nil
                                                       scanData:nil
                                                        storeID:[CRSLocationController getLocationKey]
                                                transactionType:self.basket.basketType
                                                  workstationID:[TillSetupController getTillNumber]
                                                       complete:^(id restResult, NSError *restError, ServerError *restServerError)
         {
             if (restResult)
             {
                 result = restResult;
                 basketID = result.basketID;
             }
             if (error)
             {
                 error = restError;
             }
             if (serverError)
             {
                 serverError = restServerError;
             }
             [lock unlock];
         }];
        [lock lock];
        [lock unlock];
    }
    
    if (serverError == nil && error == nil)
    {
        [RESTController.sharedInstance noReceiptWithBasketId:basketID
                                                  itemScanId:itemScanId
                                            itemSerialNumber:itemSerialNumber
                                     originalItemDescription:originalItemDescription
                                          originalItemScanId:originalItemScanId
                                                       price:price
                                           reasonDescription:reasonDescription
                                                    reasonId:reasonId
                                                    complete:^(BasketAssociateDTO *basket, NSError *error, ServerError *serverError) {
                                                        ocg_sync_main(^{
                                                            if (basket)
                                                            {
                                                                BasketController.sharedInstance.basket = basket;
                                                                [BasketController.sharedInstance kick];
                                                            }
                                                            
                                                            complete(basket, error, serverError);
                                                        });
                                                    }];
    }
}

- (void) internetReturnWithItemScanId:(NSString*)itemScanId
                                price:(NSString*)price
                             reasonId:(NSString*)reasonId
                    reasonDescription:(NSString*)reasonDescription
                     itemSerialNumber:(NSString*)itemSerialNumber
                   originalItemScanId:(NSString*)originalItemScanId
              originalItemDescription:(NSString*)originalItemDescription
                  originalReferenceId:(NSString*)originalReferenceId
                             complete:(void (^)(BasketAssociateDTO* result, NSError* error, ServerError* serverError))complete
{
    __block BasketDTO *result = nil;
    __block NSString *basketID = self.basket.basketID;
    __block NSError *error = nil;
    __block ServerError *serverError = nil;
    
    NSLock *lock = [NSLock new];
    
    if (basketID == nil)
    {
        [lock lock];
        [RESTController.sharedInstance createBasketWithBasketID:nil
                                                    businessDay:nil
                                                       deviceID:[[UIDevice currentDevice] uniqueDeviceIdentifier]
                                                issuerCountryId:nil
                                                lastTransaction:nil
                                                       scanData:nil
                                                        storeID:[CRSLocationController getLocationKey]
                                                transactionType:self.basket.basketType
                                                  workstationID:[TillSetupController getTillNumber]
                                                       complete:^(id restResult, NSError *restError, ServerError *restServerError)
         {
             if (restResult)
             {
                 result = restResult;
                 basketID = result.basketID;
             }
             if (error)
             {
                 error = restError;
             }
             if (serverError)
             {
                 serverError = restServerError;
             }
             [lock unlock];
         }];
        [lock lock];
        [lock unlock];
    }
    
    if (serverError == nil && error == nil)
    {
        [RESTController.sharedInstance internetReturnWithBasketId:basketID
                                                       itemScanId:itemScanId
                                                 itemSerialNumber:itemSerialNumber
                                          originalItemDescription:originalItemDescription
                                               originalItemScanId:originalItemScanId
                                              originalReferenceId:originalReferenceId
                                                            price:price
                                                reasonDescription:reasonDescription
                                                         reasonId:reasonId
                                                         complete:^(BasketAssociateDTO *basket, NSError *error, ServerError *serverError) {
                                                             ocg_sync_main(^{
                                                                 if (basket)
                                                                 {
                                                                     BasketController.sharedInstance.basket = basket;
                                                                     [BasketController.sharedInstance kick];
                                                                 }
                                                                 
                                                                 complete(basket, error, serverError);
                                                             });
                                                         }];
    }
}

- (void) unlinkedReturnWithItemScanId:(NSString*)itemScanId
              originalItemDescription:(NSString*)originalItemDescription
           originalTransactionStoreId:(NSString*)originalTransactionStoreId
              originalTransactionTill:(NSString*)originalTransactionTill
                originalTransactionId:(NSString*)originalTransactionId
              originalTransactionDate:(NSString*)originalTransactionDate
                                price:(NSString*)price
                             reasonId:(NSString*)reasonId
                    reasonDescription:(NSString*)reasonDescription
                     itemSerialNumber:(NSString*)itemSerialNumber
                   originalItemScanId:(NSString*)originalItemScanId
                  originalSalesPerson:(NSString*)originalSalesPerson
                 originalCustomerCard:(NSString*)originalCustomerCard
                             complete:(void (^)(BasketAssociateDTO* result, NSError* error, ServerError* serverError))complete
{
    __block BasketDTO *result = nil;
    __block NSString *basketID = self.basket.basketID;
    __block NSError *error = nil;
    __block ServerError *serverError = nil;
    
    NSLock *lock = [NSLock new];
    
    if (basketID == nil)
    {
        [lock lock];
        [RESTController.sharedInstance createBasketWithBasketID:nil
                                                    businessDay:nil
                                                       deviceID:[[UIDevice currentDevice] uniqueDeviceIdentifier]
                                                issuerCountryId:nil
                                                lastTransaction:nil
                                                       scanData:nil
                                                        storeID:[CRSLocationController getLocationKey]
                                                transactionType:self.basket.basketType
                                                  workstationID:[TillSetupController getTillNumber]
                                                       complete:^(id restResult, NSError *restError, ServerError *restServerError)
         {
             if (restResult)
             {
                 result = restResult;
                 basketID = result.basketID;
             }
             if (error)
             {
                 error = restError;
             }
             if (serverError)
             {
                 serverError = restServerError;
             }
             [lock unlock];
         }];
        [lock lock];
        [lock unlock];
    }
    
    if (serverError == nil && error == nil)
    {
        [RESTController.sharedInstance unlinkedReturnWithBasketId:basketID
                                                       itemScanId:itemScanId
                                                 itemSerialNumber:itemSerialNumber
                                             originalCustomerCard:originalCustomerCard
                                          originalItemDescription:originalItemDescription
                                               originalItemScanId:originalItemScanId
                                              originalSalesPerson:originalSalesPerson
                                          originalTransactionDate:originalTransactionDate
                                            originalTransactionId:originalTransactionId
                                       originalTransactionStoreId:originalTransactionStoreId
                                          originalTransactionTill:originalTransactionTill
                                                            price:price
                                                reasonDescription:reasonDescription
                                                         reasonId:reasonId
                                                         complete:^(BasketAssociateDTO *basket, NSError *error, ServerError *serverError) {
                                                             ocg_sync_main(^{
                                                                 if (basket)
                                                                 {
                                                                     BasketController.sharedInstance.basket = basket;
                                                                     [BasketController.sharedInstance kick];
                                                                 }
                                                                 
                                                                 complete(basket, error, serverError);
                                                             });
                                                         }];
    }
}

- (void) linkedReturnWithItemScanId:(NSString*)itemScanId
                             lineId:(NSString*)lineId
            originalItemDescription:(NSString*)originalItemDescription
         originalTransactionStoreId:(NSString*)originalTransactionStoreId
            originalTransactionTill:(NSString*)originalTransactionTill
              originalTransactionId:(NSString*)originalTransactionId
            originalTransactionDate:(NSString*)originalTransactionDate
                           reasonId:(NSString*)reasonId
                  reasonDescription:(NSString*)reasonDescription
                   itemSerialNumber:(NSString*)itemSerialNumber
                 originalItemScanId:(NSString*)originalItemScanId
                           complete:(void (^)(BasketAssociateDTO* result, NSError* error, ServerError* serverError))complete
{
    __block BasketDTO *result = nil;
    __block NSString *basketID = self.basket.basketID;
    __block NSError *error = nil;
    __block ServerError *serverError = nil;
    
    NSLock *lock = [NSLock new];
    
    if (basketID == nil)
    {
        [lock lock];
        [RESTController.sharedInstance createBasketWithBasketID:nil
                                                    businessDay:nil
                                                       deviceID:[[UIDevice currentDevice] uniqueDeviceIdentifier]
                                                issuerCountryId:nil
                                                lastTransaction:nil
                                                       scanData:nil
                                                        storeID:[CRSLocationController getLocationKey]
                                                transactionType:self.basket.basketType
                                                  workstationID:[TillSetupController getTillNumber]
                                                       complete:^(id restResult, NSError *restError, ServerError *restServerError)
         {
             if (restResult)
             {
                 result = restResult;
                 basketID = result.basketID;
             }
             if (error)
             {
                 error = restError;
             }
             if (serverError)
             {
                 serverError = restServerError;
             }
             [lock unlock];
         }];
        [lock lock];
        [lock unlock];
    }
    
    if (serverError == nil && error == nil)
    {
        [RESTController.sharedInstance linkedReturnWithBasketId:basketID
                                                     itemScanId:itemScanId
                                               itemSerialNumber:itemSerialNumber
                                                         lineId:lineId
                                        originalItemDescription:originalItemDescription
                                             originalItemScanId:originalItemScanId
                                        originalTransactionDate:originalTransactionDate
                                          originalTransactionId:originalTransactionId
                                     originalTransactionStoreId:originalTransactionStoreId
                                        originalTransactionTill:originalTransactionTill
                                              reasonDescription:reasonDescription
                                                       reasonId:reasonId
                                                       complete:^(BasketAssociateDTO *basket, NSError *error, ServerError *serverError) {
                                                           ocg_sync_main(^{
                                                               if (basket)
                                                               {
                                                                   BasketController.sharedInstance.basket = basket;
                                                                   [BasketController.sharedInstance kick];
                                                               }
                                                               
                                                               complete(basket, error, serverError);
                                                           });
                                                       }];
    }
}

- (BasketDTO*)adjustSale:(BasketDTO*)basket
                   error:(NSError **)error
             serverError:(ServerError **)serverError
{
    __block BasketDTO *result = basket;
    __block NSString *basketID = self.basket.basketID;
    
    NSLock *lock = [NSLock new];
    
    if (basketID == nil)
    {
        [lock lock];
        [RESTController.sharedInstance createBasketWithBasketID:nil
                                                    businessDay:nil
                                                       deviceID:[[UIDevice currentDevice] uniqueDeviceIdentifier]
                                                issuerCountryId:nil
                                                lastTransaction:nil
                                                       scanData:nil
                                                        storeID:[CRSLocationController getLocationKey]
                                                transactionType:self.basket.basketType
                                                  workstationID:[TillSetupController getTillNumber]
                                                       complete:^(id restResult, NSError *restError, ServerError *restServerError)
         {
             if (restResult)
             {
                 result = restResult;
                 basketID = result.basketID;
             }
             if (error)
             {
                 *error = restError;
             }
             if (serverError)
             {
                 *serverError = restServerError;
             }
             [lock unlock];
         }];
        [lock lock];
        [lock unlock];
    }
    
    if (*serverError == nil && *error == nil)
    {
        [lock lock];
        [RESTController.sharedInstance adjustSaleWithBasket:(basket == nil ? result : basket)
                                                   complete:^(BasketDTO *restResult, NSError *restError, ServerError *restServerError)
         {
             if (restResult)
             {
                 result = restResult;
                 basketID = result.basketID;
             }
             if (error)
             {
                 *error = restError;
             }
             if (serverError)
             {
                 *serverError = restServerError;
             }
             [lock unlock];
         }];
        [lock lock];
        [lock unlock];
    }
    return result;
}

- (void) printPrintDataLine:(MPOSPrintDataLine *)printLine
                    skipped:(BOOL)skipped
                  newStatus:(NSString *)status
{
    NSUInteger linesToPrint = 0;
    MPOSPrintDataLine *lineToPrint = nil;
    for (MPOSBasketItem *item in self.basket.basketItems) {
        if ([item isKindOfClass:[MPOSPrintDataLine class]])
        {
            linesToPrint++;
        }
    }
    
    ocg_async_background_overlay(^{
        
        __block BOOL adjusted = NO;
        OCGReceiptManager *receiptManager = [OCGReceiptManager configuredManager];
        
        if (!skipped){
            NSError *error = nil;
            BOOL printedValue = NO;
            if ((printLine.printData != nil) && (printLine.printData.length > 0)) {
                printedValue = [receiptManager printRTIPrintData:printLine.printData error:&error];
            } else {
                ErrorLog(@"No print data");
            }
            
            if (self.clientSettings.autoPrint && self.clientSettings.printJobDelay > 0 && linesToPrint > 1)
            {
                usleep(self.clientSettings.printJobDelay * 1000000);
            }
            
            ocg_sync_main(^{
                printLine.printed = printedValue;
                if (printLine.printed) {
                    printLine.printResult = status;
                    if ([self.basket.basketID isEqual:kBasketControllerFakeBasketID])
                    {
                        [self.basket removeBasketItem:printLine];
                        [self kick];
                        
                        if (self.basket.basketItems.count == 0)
                        {
                            [self resetBasketToServer];
                        }
                        else
                        {
                            [[NSNotificationCenter defaultCenter] postNotificationName:kBasketControllerBasketChangeNotification
                                                                                object:self];
                        }
                        
                    }
                    else
                    {
                        adjusted = YES;
                    }
                }
                else
                {
                    if (error != nil) {
                        printLine.printResult = error.localizedDescription;
                    }
                    printLine.uiState = @"SECONDARY";
                    [self kick];
                    [[NSNotificationCenter defaultCenter] postNotificationName:kBasketControllerBasketItemChangeNotification
                                                                        object:self];
                }
            });
        } else {
            ocg_sync_main(^{
                if ([self.basket.basketID isEqual:kBasketControllerFakeBasketID])
                {
                    [self.basket removeBasketItem:printLine];
                    [self kick];
                    
                    
                    if (self.basket.basketItems.count == 0)
                    {
                        [self resetBasketToServer];
                    }
                    else
                    {
                        [[NSNotificationCenter defaultCenter] postNotificationName:kBasketControllerBasketChangeNotification
                                                                            object:self];
                    }
                    
                }
                else
                {
                    adjusted = YES;
                    printLine.printed = YES;
                    printLine.printResult = status;
                }
            });
        }
        
        
        
        if (adjusted) {
            [RESTController.sharedInstance adjustSaleWithBasket:self.basket
                                                       complete:^(BasketDTO *result, NSError *error, ServerError *serverError)
             {
                 ocg_sync_main(^{
                     if (result) {
                         self.basket = result;
                         [self kick];
                         [[NSNotificationCenter defaultCenter] postNotificationName:kBasketControllerBasketChangeNotification
                                                                             object:self];
                     }
                     
                     if (error != nil || serverError != nil) {
                         // there was an error
                         ServerError *mposerror = serverError;
                         [self kick];
                         // send the notification
                         [[NSNotificationCenter defaultCenter] postNotificationName:(serverError.overrideErrorMessageCheck) ? kBasketControllerOverrideErrorNotification : kBasketControllerGeneralErrorNotification
                                                                             object:self
                                                                           userInfo:@{kBasketControllerGeneralErrorKey:mposerror}];
                     }
                 });
             }];
        }
    });
}

#pragma mark - EFT methods

- (NSString *) localizedEFTPaymentStatus
{
    NSString *result = nil;
    OCGEFTManager *manager = [OCGEFTManager sharedInstance];
    
    if (manager.processing) {
        if (manager.currentPayment.paymentStatus != kEFTPaymentStatusComplete) {
            result = [manager.currentPayment localizedStringForCurrentStatus];
            NSString *providerStatus = [manager additionalProviderStatus];
            if (providerStatus.length > 0) {
                result = [NSString stringWithFormat:@"%@ - %@", result, providerStatus];
            }
        } else {
            switch (manager.currentPayment.paymentCompleteReason) {
                case kEFTPaymentCompleteReasonNotComplete:
                    result = nil;
                    break;
                case kEFTPaymentCompleteReasonApproved:
                    result = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁙󠁍󠀷󠁰󠀵󠁩󠁍󠀯󠁫󠀹󠀳󠀵󠁱󠀸󠀲󠁵󠁱󠁬󠁋󠁐󠀷󠁤󠁺󠁱󠁐󠁊󠀰󠁿*/ @"Approved", nil);
                    break;
                case kEFTPaymentCompleteReasonDeclined:
                    result = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠀸󠁯󠁣󠁇󠁍󠀷󠀹󠁏󠁌󠁧󠁵󠁏󠁸󠁚󠁷󠁐󠀶󠁸󠁲󠁳󠀰󠀴󠁋󠁔󠁐󠁹󠁳󠁿*/ @"Declined", nil);
                    break;
                case kEFTPaymentCompleteReasonCancelled:
                    result = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁚󠀯󠁩󠁸󠁁󠀹󠁔󠁪󠀹󠁣󠀷󠁴󠁲󠁦󠁍󠀶󠁴󠀵󠁙󠁘󠀷󠁨󠀲󠁤󠁙󠁅󠁧󠁿*/ @"Cancelled", nil);
                    break;
                case kEFTPaymentCompleteReasonVoided:
                    result = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁭󠁥󠁒󠁥󠀸󠀹󠁕󠁯󠁭󠁬󠀹󠁵󠁪󠁒󠁁󠁴󠁆󠀱󠁫󠁈󠀳󠁣󠁌󠀵󠁗󠁌󠁧󠁿*/ @"Voided", nil);
                    break;
                case kEFTPaymentCompleteReasonError:
                    result = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁫󠀵󠁉󠁤󠁘󠁖󠁏󠁁󠁆󠀷󠁬󠀯󠁯󠁇󠀲󠀳󠁵󠁢󠀫󠁘󠁦󠀯󠁩󠁁󠁦󠁒󠁑󠁿*/ @"Error", nil);
                    break;
            }
        }
    }
    
    return result;
}

- (void) cardTender:(Tender *)tender
         withAmount:(NSNumber *)amount
{
    // prepare and send the payment request to EFT provider
    NSNumber *tenderAmount = [NSNumber numberWithDouble:self.basket.amountDue.amount.doubleValue];
    if (amount) {
        tenderAmount = amount;
    }
    
    // determine if this is a sale or refund
    EFTPaymentTransactionType transactionType = kEFTPaymentTransactionTypeSale;
    NSString *currency = nil;
    // TFS59375 - since we enabled prompt for amount on EFT tenders,
    // we cannot rely on the tender amount being negative for returns
    // so we have to check the basket total
    if (self.basket.amountDue.amount.doubleValue < 0.0f) {
        // this is a refund
        tenderAmount = [NSNumber numberWithDouble:fabs(tenderAmount.doubleValue)];
        transactionType = kEFTPaymentTransactionTypeRefund;
        // todo get DCC information (if applicable)
    }
    // send the request
    [[OCGEFTManager sharedInstance] EFTPaymentForTransactionType:transactionType
                                                      withAmount:tenderAmount
                                                      inCurrency:currency
                                                       forTender:tender
                                                       forBasket:(BasketAssociateDTO*)self.basket];
}

#pragma mark - Giftcard

- (enum PaymentCardCaptureMethod) paymentCardCaptureMethodForTender:(Tender *)tender
{
    // default to scan
    enum PaymentCardCaptureMethod result = PaymentCardCaptureMethodScan;
    
    // get the setting
    NSString *captureMethodString = tender.promptForCardNumberCaptureMethod;
    if (captureMethodString == nil) {
        captureMethodString = @"SCAN";
    }
    
    // convert to enum
    if ([captureMethodString isEqualToString:@"SCAN"]) {
        result = PaymentCardCaptureMethodScan;
    } else if ([captureMethodString isEqualToString:@"SWIPE"]) {
        result = PaymentCardCaptureMethodSwipe;
    } else {
        result = PaymentCardCaptureMethodScan;
    }
    
    return result;
}

- (void) handleEFTPaymentResult:(BasketTender *)payment
                      forBasket:(BasketDTO *)restBasket
                      withError:(NSError *)restError
                 andServerError:(ServerError *)restServerError
{
    
    
    BOOL callFailed = NO;
    
    /*
     Error code 71 (Duplicate payment found) is deemed to be a successful result, in the sense that the
     data we want on the server is on the server.
     */
    
    for (ServerErrorMessage *message in restServerError.messages) {
        if ([message.code intValue] != MShopperDuplicatePaymentFoundExceptionCode) {
            callFailed = YES;
        }
        [self getActiveBasket];
    }
    
    if (callFailed || restError != nil) {
        
        dispatch_block_t repeatBlock = ^{
            [self addPaymentTender:payment];
        };
        if ([OCGEFTManager sharedInstance].hasPaymentBeenAdded) {
            repeatBlock = nil;
        }
        [ServerErrorHandler.sharedInstance handleServerError:restServerError
                                              transportError:restError
                                                 withContext:nil
                                                dismissBlock:nil
                                                 repeatBlock:repeatBlock];
    } else {
        // no errors, update the basket
        // import the new basket
        self.basket = restBasket;
        [self kick];
        
        if (self.isTendered) {
            [[OCGEFTManager sharedInstance] endCurrentPayment];
        }
        
        DebugLog(@"%@ response with basket id (%@) and (status %@).",
                 NSStringFromClass(payment.class),
                 payment.basket.basketID,
                 payment.basket.status);
        // send the notification
        [[NSNotificationCenter defaultCenter] postNotificationName:kBasketControllerPaymentChangeNotification
                                                            object:self];
    }
    
    
}

- (void) addPaymentTender:(id)tender
{
    if (![OCGEFTManager sharedInstance].hasPaymentBeenAdded) {
        if ([tender isKindOfClass:[YespayPayment class]]) {
            [self addYespayPaymentTender:tender];
        } else if ([tender isKindOfClass:[VerifonePayment class]]) {
            [self addVerifonePaymentTender:tender];
        } else if ([tender isKindOfClass:[EFTPayment class]]) {
            [self addEFTPaymentTender:tender
                             complete:nil];
        }
    }
}

- (void) addVerifonePaymentTender:(VerifonePayment *)payment
{
    ocg_async_background_overlay(^{
        [RESTController.sharedInstance createVerifoneTenderWithTender:payment
                                                             complete:^(BasketAssociateDTO *result, NSError *error, ServerError *serverError)
         {
             ocg_sync_main(^{
                 [self handleEFTPaymentResult:payment
                                    forBasket:result
                                    withError:error
                               andServerError:serverError];
             });
         }];
    });
}

- (void) addYespayPaymentTender:(YespayPayment *)payment
{
    ocg_async_background_overlay(^{
        [RESTController.sharedInstance createYespayTenderWithTender:payment
                                                           complete:^(BasketAssociateDTO *result, NSError *error, ServerError *serverError)
         {
             ocg_sync_main(^{
                 [self handleEFTPaymentResult:payment
                                    forBasket:result
                                    withError:error
                               andServerError:serverError];
             });
         }];
    });
}

- (void) addEFTPaymentTender:(EFTPayment *)payment
                    complete:(dispatch_block_t)complete
{
    [self addEFTPaymentTender:payment
          withDeviceStationId:nil
                     complete:complete];
}

- (void) addEFTPaymentTender:(EFTPayment *)payment
         withDeviceStationId:(NSString *)deviceStationId
                    complete:(dispatch_block_t)complete
{
    ocg_async_background_overlay(^{
        [RESTController.sharedInstance createEFTTenderWithDeviceStationId:deviceStationId
                                                                   tender:payment
                                                                 complete:^(BasketAssociateDTO *result, NSError *error, ServerError *serverError)
         {
             ocg_sync_main(^{
                 if ([serverError deviceStationErrorMessage]) {
                     // there was a device station error
                     NSMutableDictionary *userinfo = [NSMutableDictionary dictionary];
                     [userinfo setValue:serverError
                                 forKey:kBasketControllerGeneralErrorKey];
                     [userinfo setValue:^(NSString *deviceStationId, NSUInteger errorCode) {
                         [[BasketController sharedInstance] addEFTPaymentTender:payment
                                                            withDeviceStationId:deviceStationId
                                                                       complete:complete];
                     }
                                 forKey:kBasketControllerControlRetryBlockKey];
                     [userinfo setValue:@(NO)
                                 forKey:kBasketControllerControlCancelBlockKey];
                     [[NSNotificationCenter defaultCenter] postNotificationName:kBasketControllerControlErrorNotification
                                                                         object:self
                                                                       userInfo:userinfo];
                 } else {
                     [self handleEFTPaymentResult:payment
                                        forBasket:result
                                        withError:error
                                   andServerError:serverError];
                     if (complete) complete();
                 }
             });
         }];
    });
}

/** @brief Tender the balance of the transaction
 *  to the provided gift card.
 *  @param cardNumber The card number of the gift card.
 */
- (void) standardPaymentWithTender:(Tender *)tender
                            amount:(NSNumber *)amount
                         reference:(NSString *)reference
                          authCode:(NSString *)authCode
                        cardNumber:(NSString *)cardNumber
{
    NSAssert(self != nil, @"The data source has not yet been initialized.");
    NSAssert(self.basket != nil, @"The active basket has not yet been initialized.");
    
    // create the payment
    StandardPayment *payment = [[StandardPayment alloc] init];
    //    payment.basket = self.basket;
    payment.reference = reference;
    payment.authCode = authCode;
    payment.cardNumber = cardNumber;
    payment.amount = [amount description];
    payment.tender = tender;
    payment.basketID = self.basket.basketID;
    payment.tenderType = payment.tender.tenderType;
    
    ocg_async_background_overlay(^{
        if ([[BasketController sharedInstance] needsAuthentication])
        {
            return;
        }
        
        // get the parameters
        if (payment.basketID != nil)
        {
            [RESTController.sharedInstance createStandardTenderWithTender:payment
                                                                 complete:^(BasketAssociateDTO *result, NSError *error, ServerError *serverError)
             {
                 ocg_sync_main(^{
                     if (error != nil || serverError != nil)
                     {
                         ServerError *mposError = serverError;
                         
                         // there was an error
                         // send the notification
                         [[NSNotificationCenter defaultCenter] postNotificationName:(serverError.overrideErrorMessageCheck) ? kBasketControllerOverrideErrorNotification : kBasketControllerGeneralErrorNotification
                                                                             object:self
                                                                           userInfo:@{kBasketControllerGeneralErrorKey:mposError}];
                     }
                     
                     if (result)
                     {
                         self.basket = result;
                         
                         [self kick];
                         // send the notification
                         [[NSNotificationCenter defaultCenter] postNotificationName:kBasketControllerBasketChangeNotification
                                                                             object:self];
                     }
                 });
             }];
        }
    });
}

- (void) forceServerConfigurationCacheRefresh
{
    [self refreshConfiguration:YES
              clearServerCache:YES
                      complete:nil];
}

- (void) refreshConfiguration:(BOOL)clearLocalCache
{
    [self refreshConfiguration:clearLocalCache
              clearServerCache:NO
                      complete:nil];
}

- (void) refreshConfiguration:(BOOL)clearLocalCache
                     complete:(dispatch_block_t)complete
{
    [self refreshConfiguration:clearLocalCache
              clearServerCache:NO
                      complete:complete];
}

- (void) refreshConfiguration:(BOOL)clearLocalCache
             clearServerCache:(BOOL)clearServerCache
                     complete:(dispatch_block_t)complete
{
    NSString *locationId = [CRSLocationController getLocationKey];
    NSString *tillId = [TillSetupController getTillNumber];
    
    if ((locationId != nil) && (tillId != nil))
    {
        if (dispatch_group_wait(_configGroup, DISPATCH_TIME_NOW) == 0) {
            dispatch_group_enter(_configGroup);
            if (complete) {
                _configBlock = complete;
            }
            ocg_async_background_overlay(^{
                
                NSString *etag = [[NSUserDefaults standardUserDefaults] stringForKey:kCONFIGURATION_ETAG_KEY];
                
                if (clearLocalCache)
                {
                    etag = nil;
                }
                
                RESTControllerCompleteBlockType processConfigResults = ^(RESTInvocation *invocation, NSString*server, id result, NSError *error, ServerError *serverError, NSString *etag)
                {
                    ocg_sync_main(^{
                        if (error != nil || serverError != nil)
                        {
                            ServerError *mposError = serverError;
                            
                            // there was an error
                            // if we aren't forcing a refresh show the error, otherwise fail silently
                            if (clearLocalCache) {
                                // send the notification
                                [[NSNotificationCenter defaultCenter] postNotificationName:(serverError.overrideErrorMessageCheck) ? kBasketControllerOverrideErrorNotification : kBasketControllerGeneralErrorNotification
                                                                                    object:self
                                                                                  userInfo:@{kBasketControllerGeneralErrorKey:mposError}];
                            }
                        }
                        
                        if (result != nil)
                        {
                            Configuration *configuration = (Configuration *)result;
                            if (configuration.softKeyContexts.count == 0)
                            {
                                [UIAlertView alertViewWithTitle:nil
                                                        message:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁸󠁶󠁺󠀵󠁏󠁑󠀳󠁵󠁂󠁥󠁒󠀵󠁺󠁸󠁮󠁭󠀯󠁧󠁧󠁁󠁊󠁮󠀴󠀹󠁙󠁅󠁍󠁿*/ @"The configuration was malformed, please contact your administrator", nil)
                                              cancelButtonTitle:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁄󠁴󠁍󠁉󠁣󠀫󠁺󠁔󠁗󠁦󠁹󠁡󠁯󠀷󠁨󠁔󠁩󠀸󠁯󠀫󠁨󠁘󠀹󠁱󠁌󠀶󠀸󠁿*/ @"OK", nil)
                                              otherButtonTitles:NULL
                                                      onDismiss:NULL
                                                       onCancel:^{
                                                           if (![self needsAuthentication])
                                                           {
                                                               [self hasLoggedOut];
                                                           }
                                                       }];
                                
                            }
                            else
                            {
                                _configuration = nil;
                                _localizableTables = nil;
                                NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                                [defaults setObject:etag forKey:kCONFIGURATION_ETAG_KEY];
                                NSData *data = [NSKeyedArchiver archivedDataWithRootObject:configuration];
                                [defaults setObject:data forKey:kCONFIGURATION_DATA_KEY];
                                [defaults synchronize];
                                
                                [[NSNotificationCenter defaultCenter] postNotificationName:kBasketControllerConfigurationDidChangeNotification
                                                                                    object:self];
                                
                                if (_configBlock) {
                                    _configBlock();
                                }
                                _configBlock = nil;
                            }
                        }
                        
                        dispatch_group_leave(_configGroup);
                    });
                };
                
                [RESTController.sharedInstance getConfigurationWithClearServerCache:clearServerCache
                                                                            storeID:locationId
                                                                tillNumber:tillId
                                                                      etag:etag
                                                                  complete:^(Configuration *configuration, NSError *error, ServerError *serverError, NSString *etag)
                 {
                     if (configuration != nil)
                     {
                         DeviceProfile *profile = [[DeviceProfile alloc] init];
                         profile.storeId = locationId;
                         profile.tillNumber = tillId;
                         profile.deviceId = [[UIDevice currentDevice] uniqueDeviceIdentifier];
                         
                         [RESTController.sharedInstance updateDeviceWithDevice:profile
                                                                        server:nil
                                                                      complete:^(NSError *error, ServerError *serverError)
                          {
                              processConfigResults(nil, nil, configuration, error, serverError, etag);
                          }];
                     } else {
                         processConfigResults(nil, nil, configuration, error, serverError, etag);
                     }
                 }];
            });
        }
    }
}

-(Configuration*)configuration
{
    if (_configuration == nil)
    {
        if (NO)
        {
            NSData *localConfiguration = [NSData dataWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"Configuration" ofType:@"xml"]];
            _configuration = [RESTController.sharedInstance objectForData:localConfiguration type:@"Configuration"];
        }
        else
        {
            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
            
            NSData *data = [defaults dataForKey:kCONFIGURATION_DATA_KEY];
            
            if ([data length])
            {
                _configuration = [NSKeyedUnarchiver unarchiveObjectWithData:data];
            }
        }
    }
    
    if (_configuration == nil)
    {
        [self refreshConfiguration:YES];
    }
    return _configuration;
}

-(NSArray *)customerIdentificationTypes
{
    return [[self configuration] customerIdentificationTypes];
}

- (NSDictionary*)tenders
{
    Configuration *configuration = [self configuration];
    
    if (configuration != nil)
    {
        NSMutableDictionary *tenders = [NSMutableDictionary dictionary];
        
        for (Tender *tender in configuration.tenders)
        {
            if ((tender.isEFTTender == NO ||
                 ([OCGEFTManager sharedInstance].configured &&
                  self.basket.amountDue.amount.doubleValue &&
                  tender.isEFTTender == YES)) &&
                (!self.isInTrainingMode || tender.trainingModeTender))
            {
                [tenders setObject:tender forKey:tender.tenderType];
            }
        }
        return tenders;
    }
    else
    {
        return nil;
    }
}

- (NSArray*)passwordRules
{
    Configuration *configuration = [self configuration];
    
    if (configuration != nil)
    {
        return [configuration.passwordRules copy];
    }
    else
    {
        return nil;
    }
}

- (NSArray*)discountCodesWithType:(NSString*)discountType
{
    Configuration *configuration = [self configuration];
    
    if (configuration != nil)
    {
        NSMutableArray *discountCodes = [NSMutableArray array];
        
        for (DiscountCode *discountCode in configuration.discountCodes)
        {
            if ([discountCode.discountType isEqualToString:discountType])
            {
                [discountCodes addObject:discountCode];
            }
        }
        
        return discountCodes;
    }
    else
    {
        return nil;
    }
}

-(ClientSettings*)clientSettings
{
    Configuration *configuration = [self configuration];
    
    ClientSettings *clientSettings = configuration.clientSettings;
    
    static ClientSettings *localClientSettings = nil;
    
    if (localClientSettings == nil)
    {
        NSData *data = [NSData dataWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"CompanyAssets/ClientSettings" ofType:@"xml"]];
        localClientSettings = [RESTController.sharedInstance objectForData:data type:@"ClientSettings"];
    }
    
    if (clientSettings)
    {
        for (NSString *key in localClientSettings.xmlAttributes)
        {
            if ([clientSettings valueForKey:key] == nil || [key hasPrefix:@"DRAFT_"])
            {
                [clientSettings setValue:[localClientSettings valueForKey:key] forKey:key];
            }
        }
    }
    else
    {
        clientSettings = localClientSettings;
    }
    
    return clientSettings;
}

- (NSArray*)affiliations
{
    Configuration *configuration = [self configuration];
    
    if (configuration != nil)
    {
        return [configuration.affiliations copy];
    }
    else
    {
        return nil;
    }
}


- (void) paymentWithTender:(Tender *)tender
                    amount:(NSNumber *)amount
                 reference:(NSString *)reference
                  authCode:(NSString *)authCode
                cardNumber:(NSString *)cardNumber
{
    if (amount == nil)
    {
        amount = self.basketTotal;
    }
    
    DebugLog(@"payment amount: %@\nreference:%@\ntender: %@", amount, reference, tender);
    
    if (tender.isEFTTender)
    {
        [self cardTender:tender
              withAmount:amount];
    }
    else
    {
        [self standardPaymentWithTender:tender
                                 amount:amount
                              reference:reference
                               authCode:authCode
                             cardNumber:cardNumber];
    }
}

- (SoftKeyContext *) getLayoutForContextNamed:(NSString *)name
{
    DebugLog2(@"Requestinng layout: {}", name);
    SoftKeyContext *result = nil;
    
    Configuration *configuration = [self configuration];
    
    for (SoftKeyContext *keyContext in configuration.softKeyContexts)
    {
        if ([keyContext.contextType isEqual:name])
        {
            result = keyContext;
            break;
        }
    }
    
    return result;
}

- (SoftKeyContext *) getLayoutForObject:(id)object
                           basketStatus:(NSString*)basketStatus
                                element:(NSString*)element
                                  state:(NSString*)state
{
    SoftKeyContext *result = nil;
    
    NSString *type = [[object class] contextName];
    
    // both UIElement and UIState can have / in.
    // basketStatus will default to DEFAULT
    
    while (1)
    {
        NSString *shrinkableState = state;
        
        while (1)
        {
            NSString *shrinkableElement = element;
            
            while (1)
            {
                NSMutableString *contextName = [NSMutableString stringWithString:type];
                
                if (basketStatus.length > 0)
                {
                    [contextName appendString:@" "];
                    [contextName appendString:basketStatus];
                }
                
                [contextName appendString:@" "];
                [contextName appendString:shrinkableElement];
                
                if (shrinkableState.length > 0)
                {
                    [contextName appendString:@" "];
                    [contextName appendString:shrinkableState];
                }
                
                result = [self getLayoutForContextNamed:[contextName uppercaseString]];
                
                if (result)
                {
                    break;
                }
                
                // next element
                
                if ([shrinkableElement containsString:@"/"])
                {
                    shrinkableElement = [shrinkableElement stringByDeletingLastPathComponent];
                }
                else
                {
                    break;
                }
            }
            
            if (result)
            {
                break;
            }
            
            // next state
            
            if ([shrinkableState containsString:@"/"])
            {
                shrinkableState = [shrinkableState stringByDeletingLastPathComponent];
            }
            else
            {
                break;
            }
        }
        
        if (result)
        {
            break;
        }
        
        // next basketStatus
        
        
        if (basketStatus.length == 0)
        {
            break;
        }
        else
        {
            basketStatus = nil;
        }
    }
    
    return result;
}

-(SoftKey*)softKeyForKeyId:(NSString*)keyId softKeyContext:(SoftKeyContext*)softKeyContext
{
    keyId = [keyId stringByTrimmingCharactersInSet:NSCharacterSet.whitespaceAndNewlineCharacterSet];
    NSArray *parts = [keyId componentsSeparatedByString:@":"];
    
    SoftKey* softKey = [self __softKeyForKeyId:parts[0] softKeyContext:softKeyContext];
    if (parts.count > 1)
    {
        softKey.layoutStyle = parts[1];
    }
    return softKey;
}


-(SoftKey*)__softKeyForKeyId:(NSString*)keyId softKeyContext:(SoftKeyContext*)softKeyContext
{
    
    for (SoftKey *softKey in softKeyContext.softKeys) {
        NSString *softKeyId = [softKey.keyId stringByTrimmingCharactersInSet:NSCharacterSet.whitespaceAndNewlineCharacterSet];
        if ([keyId isEqualToString:softKeyId])
        {
            SoftKey *copy = [softKey copy];
            copy.context = softKeyContext;
            return copy;
        }
    }
    
    for (SoftKey *softKey in self.configuration.softKeys) {
        NSString *softKeyId = [softKey.keyId stringByTrimmingCharactersInSet:NSCharacterSet.whitespaceAndNewlineCharacterSet];
        if ([keyId isEqualToString:softKeyId])
        {
            SoftKey *copy = [softKey copy];
            copy.context = softKeyContext;
            return copy;
        }
    }
    
    if (softKeyContext == nil)
    {
        for (SoftKeyContext *softKeyContext in self.configuration.softKeyContexts)
        {
            for (SoftKey *softKey in softKeyContext.softKeys) {
                NSString *softKeyId = [softKey.keyId stringByTrimmingCharactersInSet:NSCharacterSet.whitespaceAndNewlineCharacterSet];
                if ([keyId isEqualToString:softKeyId])
                {
                    SoftKey *copy = [softKey copy];
                    copy.context = softKeyContext;
                    return copy;
                }
            }
        }
    }
    
    return nil;
}

-(KeyValueGroup*)keyValueGroupWithName:(NSString*)groupName
{
    for (KeyValueGroup* keyValueGroup in self.configuration.keyValueGroups)
    {
        if (NSOrderedSame == [keyValueGroup.groupName caseInsensitiveCompare:groupName])
        {
            return keyValueGroup;
        }
    }
    return nil;
}

- (void)enumerateSoftKeysForKeyMenu:(NSString*)keyMenu
                     softKeyContext:(SoftKeyContext*)softKeyContext
                         usingBlock:(void (^)(SoftKey*softKey, BOOL *stop))block
{
    NSMutableSet *enumeratedSoftKeyIds = [NSMutableSet set];
    BOOL stop = NO;
    [self enumerateSoftKeysForKeyMenu:keyMenu
                       softKeyContext:softKeyContext
                           usingBlock:block
                 enumeratedSoftKeyIds:enumeratedSoftKeyIds
                                 stop:&stop];
}

- (void)enumerateSoftKeysForKeyMenu:(NSString*)keyMenu
                     softKeyContext:(SoftKeyContext*)softKeyContext
                         usingBlock:(void (^)(SoftKey*softKey, BOOL *stop))block
               enumeratedSoftKeyIds:(NSMutableSet*)enumeratedSoftKeyIds
                               stop:(BOOL *)stop
{
    NSArray *keyIds = [keyMenu componentsSeparatedByCharactersInSet:NSCharacterSet.whitespaceAndNewlineCharacterSet];
    
    for (NSString *keyId in keyIds)
    {
        SoftKey *softKey = [self softKeyForKeyId:keyId softKeyContext:softKeyContext];
        if (softKey.keyId.length > 0 && ![enumeratedSoftKeyIds containsObject:softKey.keyId])
        {
            [enumeratedSoftKeyIds addObject:softKey.keyId];
            if ([softKey.keyType hasSuffix:@"MENU"])
            {
                [self enumerateSoftKeysForKeyMenu:softKey.softKeyMenu
                                   softKeyContext:softKeyContext
                                       usingBlock:block
                             enumeratedSoftKeyIds:enumeratedSoftKeyIds
                                             stop:stop];
            }
            else
            {
                block(softKey, stop);
            }
        }
        if (*stop)
        {
            break;
        }
    }
}

- (BOOL)menuContainsSoftKeys:(NSString*)keyMenu
              softKeyContext:(SoftKeyContext*)softKeyContext
{
    __block BOOL menuContainsSoftKeys = NO;
    [BasketController.sharedInstance enumerateSoftKeysForKeyMenu:keyMenu
                                                  softKeyContext:softKeyContext
                                                      usingBlock:^(SoftKey *softKey, BOOL *stop)
     {
         menuContainsSoftKeys = YES;
         *stop = YES;
     }];
    return menuContainsSoftKeys;
}

/** @brief Returns an array of the discount reasons for the specified group.
 *  @param reasonGroup the reason group name to query.
 *  @return An array of all discount reasons currently stored in the persistant data store.
 */
- (NSArray *) discountReasonsForGroup:(NSString *)reasonGroup
{
    NSArray *result = nil;
    
    Configuration *configuration = [self configuration];
    
    for (DiscountReasonGroup *discountReasonGroup in configuration.reasonGroups)
    {
        if ([discountReasonGroup.name isEqual:reasonGroup])
        {
            result = [discountReasonGroup.reasons copy];
            break;
        }
    }
    return result;
}

- (NSArray *) dumpCodes
{
    NSArray *result = nil;
    
    Configuration *configuration = [self configuration];
    
    result = configuration.dumpCodes;
    return result;
}

- (BOOL) isDumpCodeItem:(NSString *)itemId
{
    BOOL result = NO;
    
    for (DumpCode *dumpCode in [self dumpCodes]) {
        if ([dumpCode.itemSellingCode isEqualToString:itemId]) {
            result = YES;
            break;
        }
    }
    
    return result;
}

#pragma mark Public Methods

/** @brief Returns the single character currency symbol for the basket.
 */
- (NSString *) currencyCode
{
    return self.basket.currencyCode ?: self.clientSettings.nativeCurrencyCode ?: [NSLocale.currentLocale objectForKey:NSLocaleCurrencyCode];
}

- (void) toggleTrainingMode
{
    // show spinner
    
    ocg_async_background_overlay(^{
        [RESTController.sharedInstance toggleTrainingModeComplete:^(BOOL result, NSError *error, ServerError *serverError) {
            if ((error == nil) && (serverError == nil)) {
                ocg_sync_main(^{
                    // update the credentials object
                    Credentials *cred = self.credentials;
                    cred.trainingMode = result;
                    self.credentials = cred;
                    // hide the spinner
                    // post the notification
                    [[NSNotificationCenter defaultCenter] postNotificationName:kBasketControllerTrainingModeToggledNotification
                                                                        object:self];
                });
            } else {
                // there was an error
                ocg_sync_main(^{
                    // send the notification
                    if (serverError.overrideErrorMessageCheck) {
                        [[NSNotificationCenter defaultCenter] postNotificationName:kBasketControllerOverrideErrorNotification
                                                                            object:self
                                                                          userInfo:@{ kBasketControllerGeneralErrorKey : serverError }];
                    } else {
                        NSMutableDictionary *userinfo = [NSMutableDictionary dictionary];
                        if (error != nil)
                            userinfo[kBasketControllerTrainingModeErrorKey] = error;
                        if (serverError != nil)
                            userinfo[kBasketControllerTrainingModeServerErrorKey] = serverError;
                        [[NSNotificationCenter defaultCenter] postNotificationName:kBasketControllerTrainingModeErrorNotification
                                                                            object:self
                                                                          userInfo:userinfo];
                    }
                });
            }
        }];
    });
}

- (void) findProductForSKU:(NSString *)sku
                 forReason:(enum ProductSearchReason)reason
{
    
    MPOSProductSearch *search = [[MPOSProductSearch alloc] init];
    // delete the existing items on the search
    search.name = nil;
    search.sku = sku;
    search.location = [CRSLocationController getLocationKey];
    search.reasonValue = reason;
    search.items = nil;
    [self kick];
    ocg_async_background_overlay(^{
        [RESTController.sharedInstance findItemWithScanID:search.sku
                                                  storeID:search.location
                                                 complete:^(Item *result, NSError *error, ServerError *serverError)
         {
             ocg_sync_main(^{
                 search.items = @[result];
                 search.error = serverError;
                 if (search.error) {
                     search.error.context = @"ProductSearch";
                     [self kick];
                     // there was an error
                     // send the notification
                     if (serverError.overrideErrorMessageCheck) {
                         [[NSNotificationCenter defaultCenter] postNotificationName:kBasketControllerOverrideErrorNotification
                                                                             object:self
                                                                           userInfo:@{kBasketControllerGeneralErrorKey:serverError}];
                     } else {
                         [[NSNotificationCenter defaultCenter] postNotificationName:kBasketControllerProductSearchErrorNotification
                                                                             object:self
                                                                           userInfo:@{kBasketControllerGeneralErrorKey:search.error, kBasketControllerProductSearchRequestKey:search.sku,                                                                        kBasketControllerProductSearchReasonKey:search.reason}];
                     }
                 } else {
                     [self kick];
                     // build the user info
                     NSDictionary *userInfo =
                     @{ kBasketControllerProductSearchResultsKey : search.items,
                        kBasketControllerProductSearchReasonKey  : search.reason };
                     // send the notification
                     [[NSNotificationCenter defaultCenter] postNotificationName:kBasketControllerProductSearchCompleteNotification
                                                                         object:self
                                                                       userInfo:userInfo];
                 }
             });
         }];
    });
}

- (void) getAvailabilityForItem:(MPOSItem *)item
{
    ocg_async_background_overlay(^{
        [RESTController.sharedInstance getItemAvailabilityWithScanID:item.itemRefId
                                                             storeID:item.productSearch.location
                                                            complete:^(NSArray *result, NSError *error, ServerError *serverError)
         {
             ocg_sync_main(^{
                 NSMutableOrderedSet *availability = [NSMutableOrderedSet orderedSet];
                 for (ItemAvailabilityRecord *restAvail in result) {
                     MPOSItemAvailability *avail = [[MPOSItemAvailability alloc] init];
                     [avail copyValuesFromObject:restAvail];
                     [availability addObject:avail];
                 }
                 
                 item.availabilityError = serverError;
                 item.availability = availability.array;
                 [self kick];
                 
                 if (item.availabilityError) {
                     // there was an error
                     // send the notification
                     [[NSNotificationCenter defaultCenter] postNotificationName:(serverError.overrideErrorMessageCheck) ? kBasketControllerOverrideErrorNotification : kBasketControllerGeneralErrorNotification
                                                                         object:self
                                                                       userInfo:@{kBasketControllerGeneralErrorKey:item.availabilityError}];
                 } else {
                     // build the user info
                     NSDictionary *userInfo =
                     @{ kBasketControllerItemAvailabilityResultsKey : item.availability };
                     // send the notification
                     [[NSNotificationCenter defaultCenter] postNotificationName:kBasketControllerItemAvailabilityGetCompleteNotification
                                                                         object:self
                                                                       userInfo:userInfo];
                 }
             });
         }];
    });
}

- (ItemModifierGroup *)modifierGroupWithId:(NSString *)modifierGroupId
{
    ItemModifierGroup *result = nil;
    
    for (ItemModifierGroup *group in self.configuration.itemModifierGroups) {
        if ([group.itemModifierGroupId isEqualToString:modifierGroupId]) {
            result = group;
            break;
        }
    }
    
    return result;
}

- (void) prepareForServerSwitch
{
    // log off
    [self setNeedsAuthentication];
    [self hasLoggedOut];
}

-(void)kick
{
    [NSNotificationCenter.defaultCenter postNotificationName:kBasketControllerKickedNotification
                                                      object:self];
}

#pragma mark - Application's Documents directory

// Returns the URL to the application's Documents directory.
- (NSURL *)applicationDocumentsDirectory
{
    NSFileManager *manager = [NSFileManager defaultManager];
    NSURL *result = [[manager URLsForDirectory:NSApplicationSupportDirectory inDomains:NSUserDomainMask] lastObject];
    if (result != nil) {
        result = [result URLByAppendingPathComponent:@"mPOS"
                                         isDirectory:YES];
        NSError *error = nil;
        if (![manager createDirectoryAtURL:result
               withIntermediateDirectories:YES
                                attributes:nil
                                     error:&error]) {
            ErrorLog(@"COULD NOT CREATE APPLICATION SUPPORT PATH:\n%@", result);
            result = nil;
        }
    } else {
        ErrorLog(@"Could not get application support path");
    }
    
    return result;
}

#pragma mark - Handle on/offline changes

-(void)serverModeDidChange:(NSNotification*)ntofication
{
    // drop the old config as it is for the old server
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults removeObjectForKey:kCONFIGURATION_DATA_KEY];
    [defaults synchronize];
    _configuration = nil;
    // get the latest for the new server
    [self refreshConfiguration:YES
                      complete:^{
                          [[BasketController sharedInstance] hasLoggedOut];
                      }];
}

#pragma mark - Runtime localization of NSLocalizedString* calls

- (NSString *)localizedStringForString:(NSString *)string comment:(NSString*)comment table:(NSString *)tableName
{
    if (_localizableTables == nil)
    {
        _localizableTables = [NSMutableDictionary dictionary];
        
        for (LocalizableTable *localizableTable in [[self configuration] localizableTables])
        {
            _localizableTables[localizableTable.table] = localizableTable.localizables;
        }
    }
    
    NSString *localizedString = string;
    if (_localizableTables[tableName])
    {
        for (Localizable *localizable in _localizableTables[tableName])
        {
            BOOL foundLocalizableByKey = NO;
            
            if (([localizable.key isEqualToString:string ?: @""] || [localizable.primitiveValue isEqualToString:string ?: @""]) && (comment == nil ||[localizable.comment isEqualToString:comment]))
            {
                localizedString = localizable.value;
                foundLocalizableByKey = YES;
                break;
            }
            
            if(foundLocalizableByKey == NO)
            {
                /*
                    jb 11/Feb/16 TFS 83891 Unable to lookup the localizable based on the key above. The key may not be set in the DB as it
                    is not required. Attempting to lookup string based on the default value string
                */
                for (id language in localizable.valueLanguages)
                {
                    NSString *defaultLangString = [language valueForKey:@"value"];
                    if ([[language valueForKey:@"code"] isEqual:@"Default"] && [defaultLangString isEqualToString:string ?: @""])
                    {
                        localizedString = localizable.value;
                        break;
                    }
                }
            }

        }
    }
    return localizedString;
}

#pragma mark - OCGSettingsControllerDelegate

- (BOOL) isDeviceSettingWithName:(NSString *)settingName
{
    return ([settingName isEqualToString:kOCGPeripheralManagerConfiguredScannerDeviceKey] ||
            [settingName isEqualToString:kPrinterControllerPrinterConfigKey] ||
            [settingName isEqualToString:kPrinterControllerPrinterConnectionTypeKey] ||
            [settingName isEqualToString:kPrinterControllerPrinterConnectionKey] ||
            [settingName isEqualToString:kOCGEFTManagerConfiguredPaymentProviderKey]);
}

- (Device *) deviceForSettingWithName:(NSString *)settingName
{
    Device *result = nil;
    
    Configuration *config = [self configuration];
    if (config) {
        for (Device *device in config.devices) {
            if ([device.deviceName isEqualToString:kDeviceTypeScanner] &&
                [settingName isEqualToString:kOCGPeripheralManagerConfiguredScannerDeviceKey]) {
                result = device;
                break;
            } else if ([device.deviceName isEqualToString:kDeviceTypePrinter] &&
                       [settingName isEqualToString:kPrinterControllerPrinterConfigKey]) {
                result = device;
                break;
            } else if ([device.deviceName isEqualToString:kDeviceTypeEFT] &&
                       [settingName isEqualToString:kOCGEFTManagerConfiguredPaymentProviderKey]) {
                result = device;
                break;
            }
        }
    }
    
    return result;
}

- (BOOL)    controller:(OCGSettingsController *)controller
settingEnabledWithName:(NSString *)settingName
{
    BOOL result = YES;
    
    // if we are dealing with a device setting
    if ([self isDeviceSettingWithName:settingName]) {
        // get the device entry in config
        Device *device = [self deviceForSettingWithName:settingName];
        if (device) {
            result = device.enabled;
        }
    }
    
    return result;
}

- (BOOL)         controller:(OCGSettingsController *)controller
settingConfigurableWithName:(NSString *)settingName
{
    BOOL result = YES;
    
    // if we are dealing with a device setting
    if ([self isDeviceSettingWithName:settingName]) {
        // get the device entry in config
        Device *device = [self deviceForSettingWithName:settingName];
        if (device) {
            result = device.userSelectable;
        }
    }
    
    return result;
}

- (id)       controller:(OCGSettingsController *)controller
overrideSettingWithName:(NSString *)settingName
{
    id result = nil;
    
    // if we are dealing with a device setting
    if ([self isDeviceSettingWithName:settingName]) {
        // get the device entry in config
        Device *device = [self deviceForSettingWithName:settingName];
        if (device) {
            if ([settingName isEqualToString:kOCGEFTManagerConfiguredPaymentProviderKey]) {
                if (device.deviceName.length > 0) {
                    OCGEFTManagerProvider provider = [[OCGEFTManager sharedInstance] providerForName:device.manager];
                    result = @(provider);
                }
            } else if ([settingName isEqualToString:kOCGPeripheralManagerConfiguredScannerDeviceKey]) {
                if (device.deviceName.length > 0) {
                    OCGPeripheralManagerDevice scanner = [[OCGPeripheralManager sharedInstance] deviceForName:device.manager];
                    result = @(scanner);
                }
            } else if ([settingName isEqualToString:kPrinterControllerPrinterConfigKey]) {
                if (device.deviceName.length > 0) {
                    PRINTER_TYPE printer = [[OCGPeripheralManager sharedInstance] deviceForName:device.manager];
                    result = @(printer);
                }
            } else if ([settingName isEqualToString:kPrinterControllerPrinterConnectionTypeKey]) {
                result = device.propertyDict[kDevicePropertyPrinterConnectionType];
            } else if ([settingName isEqualToString:kPrinterControllerPrinterConnectionKey]) {
                result = device.propertyDict[kDevicePropertyPrinterConnection];
            }
        }
    }
    
    return result;
}

#pragma mark - RESTControllerDelegate

/** @brief Sanitizes the invocation before being sent to the server
 *  Often the invocation needs extra info, like the authentication token
 *  and server address which we set here.
 */

-(void)RESTController:(RESTController *)controller
           willInvoke:(RESTInvocation *)invocation
               server:(NSString *)server
                 etag:(NSString*)etag
             complete:(RESTControllerCompleteBlockType)complete
                 send:(RESTControllerSendBlockType)send
{
    if ([invocation.RESTPath hasPrefix:@"/private/"])
    {
        invocation.token = self.credentials.token;
    }
    if (server == nil)
    {
        server = [TestModeController getServerAddress];
    }
    send(invocation, server, etag);
}

/** @brief Sanitizes the response from the server
 *  Sometimes, the response may not be quite what we want to put through mPOS. So we
 *  fiddle with it here before it cand get anywhere near CoreData.
 */

-(void)RESTController:(RESTController *)controller didInvoke:(RESTInvocation *)invocation
               server:(NSString *)server
               result:(id)result
                error:(NSError *)error
          serverError:(ServerError *)serverError
                 etag:(NSString *)etag
             complete:(RESTControllerCompleteBlockType)complete
{
    if (error)
    {
        if (!serverError)
        {
            serverError = [[ServerError alloc] init];
        }
        
        serverError.transportError = error;
    }
    
    if (serverError)
    {
        serverError.invocation = invocation;
    }
    
    
    // check for basket not found errors
    for (ServerErrorMessage *restServerErrorMessage in serverError.messages)
    {
        if ((restServerErrorMessage != nil) &&
            ([restServerErrorMessage.code intValue] == MShopperSaleBasketNotFoundExceptionCode))
        {
            // clear the error
            serverError = nil;
            break;
        }
    }
    
    BOOL shouldCallComplete = YES;
    if ([result isKindOfClass:[BasketDTO class]])
    {
        BasketDTO *basketAssociate = result;
        
        if ([basketAssociate.status isEqual:@"PRINTING"] && basketAssociate.basketID.length == 0)
        {
            basketAssociate.basketID = kBasketControllerFakeBasketID;
        }
        
        
        if ([basketAssociate.status isEqual:@"COMPLETE"])
        {
            if ([invocation isKindOfClass:GetActiveBasketInvocation.class])
            {
                result = nil;
            }
            else if (![invocation isKindOfClass:LookupSaleInvocation.class])
            {
                shouldCallComplete = NO;
                NSString *uniqueDeviceIdentifier = [[UIDevice currentDevice] uniqueDeviceIdentifier];
                [RESTController.sharedInstance getActiveBasketWithDeviceID:uniqueDeviceIdentifier
                                                                  complete:^(BasketAssociateDTO *result, NSError *error, ServerError *serverError)
                 {
                     complete(invocation, server, result, error, serverError, nil);
                 }];
            }
            
        }
    }
    
    if (result == nil && serverError == nil && error == nil && [invocation.RESTReturnType isEqualToString:NSStringFromClass(BasketAssociateDTO.class)])
    {
        BasketDTO *basketAssociate = result;
        basketAssociate = [[BasketAssociateDTO alloc] init];
        basketAssociate.status = @"IN_PROGRESS";
        result = basketAssociate;
    }
    
    if (shouldCallComplete)
    {
        complete(invocation, server, result, error, serverError, etag);
    }
}

@end
