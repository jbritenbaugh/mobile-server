//
//  ItemDetailOtherQuantityViewController.h
//  mPOS
//
//  Created by Antonio Strijdom on 28/05/2013.
//  Copyright (c) 2013 Clarity. All rights reserved.
//

#import <UIKit/UIKit.h>

/** @file ItemDetailOtherQuantityViewController.h */

/** @brief Table view controller for displaying item availability.
 */
@interface ItemDetailOtherQuantityViewController : UITableViewController

/** @property availability
 *  @brief The availability records to display.
 */
@property (nonatomic, strong) NSArray *availability;

@end
