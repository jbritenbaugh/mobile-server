//
//  ItemDetailOtherQuantityViewController.m
//  mPOS
//
//  Created by Antonio Strijdom on 28/05/2013.
//  Copyright (c) 2013 Clarity. All rights reserved.
//

#import "ItemDetailOtherQuantityViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "CRSSkinning.h"
#import "ItemDetailOtherQuantityCell.h"
#import "LocationDataSource.h"

@implementation ItemDetailOtherQuantityViewController

#pragma mark - Private

static NSString *kItemDetailOtherQuantityCell = @"kItemDetailOtherQuantityCell";

- (id) initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void) viewDidLoad
{
    [super viewDidLoad];
    UINib *nib = [UINib nibWithNibName:@"ItemDetailOtherQuantityCell"
                                bundle:nil];
    [self.tableView registerNib:nib
         forCellReuseIdentifier:kItemDetailOtherQuantityCell];
    
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.tag = kTableSkinningTag;
}

- (void) didReceiveMemoryWarning
{
    self.title = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠀳󠀯󠁖󠀴󠁂󠁺󠁮󠀷󠁇󠀴󠁍󠁆󠁷󠁮󠁤󠁃󠁨󠁱󠀹󠁷󠁡󠁘󠁭󠁔󠁎󠁣󠁳󠁿*/ @"Availability", nil);

    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) viewWillAppear:(BOOL)animated
{
    [[CRSSkinning currentSkin] applyViewSkin:self];
    [super viewWillAppear:animated];
}

#pragma mark - Table view data source

- (NSInteger) numberOfSectionsInTableView:(UITableView *)tableView
{
    NSInteger result = 0;
    
    if (self.availability) {
        result = self.availability.count;
    }
    
    return result;
}

- (NSInteger) tableView:(UITableView *)tableView
  numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

- (UITableViewCell *) tableView:(UITableView *)tableView
          cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ItemDetailOtherQuantityCell *cell =
    [tableView dequeueReusableCellWithIdentifier:kItemDetailOtherQuantityCell
                                    forIndexPath:indexPath];
    
    MPOSItemAvailability *availability = self.availability[indexPath.section];
    if (availability) {
        cell.storeId.text = availability.storeId;
        cell.storeName.text = availability.storeName;
        cell.distance.text = [NSString stringWithFormat:@"%@ %@", availability.distance, NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁧󠁐󠁁󠁆󠁫󠀸󠁘󠀯󠁱󠁧󠁄󠁩󠁹󠁤󠀷󠀰󠁕󠁍󠀶󠁭󠀵󠁍󠀵󠁺󠁆󠁲󠁉󠁿*/ @"meters", nil)];
        cell.quantity.text = availability.quantity;
        cell.distanceDesc.text = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁑󠁷󠀱󠁐󠁱󠁃󠁮󠁌󠀷󠀫󠁣󠁌󠁈󠁇󠁧󠁧󠀯󠁹󠁙󠁏󠁔󠁡󠁒󠁭󠁐󠁳󠁷󠁿*/ @"Distance", nil);
        cell.quantityDesc.text = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁑󠁄󠁏󠁴󠁩󠁔󠁈󠀫󠁌󠁵󠁔󠁆󠁧󠁤󠀸󠁭󠀸󠁱󠀷󠁢󠁓󠁣󠁎󠁡󠁯󠁇󠁙󠁿*/ @"Quantity", nil);
        // skin
        cell.tag = kTableCellSkinningTag;
        cell.storeName.tag = kTableCellKeySkinningTag;
        cell.storeId.tag = kTableCellValueSkinningTag;
        cell.distance.tag = kTableCellValueSkinningTag;
        cell.quantity.tag = kTableCellValueSkinningTag;
        cell.distanceDesc.tag = kTableCellKeySkinningTag;
        cell.quantityDesc.tag = kTableCellKeySkinningTag;
        cell.headerBackground.tag = kSecondaryButtonSkinningTag;
    }
    
    return cell;
}

- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 88.0f;
}

- (void) tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    [[CRSSkinning currentSkin] applyCellSkin:cell
                                    forStyle:tableView.style];
}

#pragma mark - Table view delegate

- (NSIndexPath *) tableView:(UITableView *)tableView
   willSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    return nil;
}

@end
