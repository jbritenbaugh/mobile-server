//
//  ReceiptListViewController.m
//  mPOS
//
//  Created by John Scott on 05/02/2014.
//  Copyright (c) 2014 Clarity. All rights reserved.
//

#import "ReceiptListViewController.h"

#import "FNKCollectionViewLayout.h"

#import "BasketItemCollectionViewCell.h"

#import "RESTController.h"
#import "ReceiptItemViewController.h"
#import "BarcodeScannerController.h"
#import "ServerErrorHandler.h"
#import "BasketController.h"
#import "UIAlertView+MKBlockAdditions.h"
#import "CRSSkinning.h"

@interface ReceiptListViewController ()

@end

@implementation ReceiptListViewController
{
    NSString *_scannedBarcode;
    BasketItemCollectionViewCell *_fakeBasketItemTableViewCell;
}

#pragma mark - Private

static NSString *CellIdentifier = @"Cell";

#pragma mark - Properties

-(void)setBasket:(BasketDTO *)basket
{
    _basket = basket;
    [self.collectionView reloadData];
}

#pragma mark - init

- (instancetype)init
{
    FNKCollectionViewLayout *layout = [[FNKCollectionViewLayout alloc] init];
    self = [super initWithCollectionViewLayout:layout];
    if (self) {
        
    }
    return self;
}

#pragma mark - UIViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
    _fakeBasketItemTableViewCell = [[BasketItemCollectionViewCell alloc] init];
    
    [self.collectionView registerClass:[BasketItemCollectionViewCell class]
            forCellWithReuseIdentifier:CellIdentifier];
    
    self.collectionView.delaysContentTouches = NO;
    
    self.collectionView.backgroundColor = [UIColor colorWithRed:0.937 green:0.937 blue:0.957 alpha:1.000];
}

- (void)viewWillAppear:(BOOL)animated
{
    self.title = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁲󠁭󠁵󠁶󠁘󠁹󠀰󠁴󠁨󠀫󠁇󠁄󠁩󠁱󠀶󠁴󠁃󠁔󠀳󠁋󠁄󠁹󠁑󠁗󠁔󠁣󠀸󠁿*/ @"Return", nil);
    
    self.navigationItem.backBarButtonItem =
    [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁌󠁈󠀹󠁕󠁸󠁃󠁖󠀯󠁡󠁉󠁹󠀲󠁹󠁅󠁐󠁙󠁌󠀲󠁡󠀶󠁙󠁖󠁊󠁧󠁕󠁳󠁍󠁿*/ @"Back", nil)
                                     style:UIBarButtonItemStylePlain
                                    target:nil
                                    action:nil];
    
    [super viewWillAppear:animated];
    [self updateNavigationItem];
    [[CRSSkinning currentSkin] applyViewSkin:self];
}

- (void)viewWillDisappear:(BOOL)animated
{
    BarcodeScannerController *controller = [BarcodeScannerController sharedInstance];
    // are we scanning?
    if (controller.scanning) {
        [controller stopScanning];
    }
    [super viewWillDisappear:animated];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView
     numberOfItemsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return [self.basket.basketItems count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView
                  cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:CellIdentifier
                                                                           forIndexPath:indexPath];
    
    ReturnableSaleItem *basketItem = [self.basket.basketItems objectAtIndex:indexPath.row];
    
    [(BasketItemCollectionViewCell *)cell updateDisplayForBasketItem:(MPOSBasketItem *)basketItem];
    
    if ([basketItem isKindOfClass:[ReturnableSaleItem class]] &&
        ![basketItem.quantity isEqual:basketItem.returnedQuantity]) {
        //        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        //        cell.selectionStyle = UITableViewCellSelectionStyleBlue;
    } else {
        //        cell.accessoryType = UITableViewCellAccessoryNone;
        //        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    
    cell.backgroundColor = [UIColor whiteColor];
    cell.layer.borderWidth = 1.;
    cell.layer.borderColor = [[UIColor colorWithRed:0.89 green:0.89 blue:0.90 alpha:1.000] CGColor];
    
    [[CRSSkinning currentSkin] applyViewSkin:cell
                             withTagOverride:cell.tag];
    
    return cell;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView
 heightForItemAtIndexPath:(NSIndexPath *)indexPath
{
    ReturnableSaleItem *basketItem = [self.basket.basketItems objectAtIndex:indexPath.row];
    [_fakeBasketItemTableViewCell updateDisplayForBasketItem:(MPOSBasketItem *)basketItem];
    
    CGSize size = [_fakeBasketItemTableViewCell systemLayoutSizeFittingSize:UILayoutFittingCompressedSize];
    
    return size.height;
}

//- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    ReturnableSaleItem *basketItem = [self.basket.basketItems objectAtIndex:indexPath.row];
//    [_fakeBasketItemTableViewCell updateDisplayForBasketItem:(MPOSBasketItem *)basketItem tableStyle:tableView.style];
//
//    CGSize size = [_fakeBasketItemTableViewCell systemLayoutSizeFittingSize:UILayoutFittingCompressedSize];
//
//
//
//    return size.height;
//}

#pragma mark - UICollectionViewDelegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    ReturnableSaleItem *basketItem = [self.basket.basketItems objectAtIndex:indexPath.row];
    if ([basketItem isKindOfClass:[ReturnableSaleItem class]] &&
        ![basketItem.quantity isEqual:basketItem.returnedQuantity]) {
        [self selectItem:basketItem];
    }
}

#pragma mark - Methods

- (void)updateNavigationItem
{
    UIBarButtonItem *cancelButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁃󠀲󠁕󠁲󠁤󠀲󠁯󠁮󠁊󠀸󠁘󠁓󠁌󠁕󠁆󠁨󠁲󠁤󠁅󠁣󠁃󠁯󠁙󠁵󠁂󠀹󠁁󠁿*/ @"Done", nil)
                                                                     style:UIBarButtonItemStyleDone
                                                                    target:self
                                                                    action:@selector(cancelButtonTapped)];
    
    UIBarButtonItem *submitButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁦󠁦󠁳󠁊󠁍󠁇󠁁󠁂󠁷󠁋󠁊󠁙󠁦󠁉󠁚󠁂󠁉󠁡󠀲󠀷󠁗󠁡󠁷󠀸󠁨󠁅󠁕󠁿*/ @"Scan", nil)
                                                                     style:UIBarButtonItemStylePlain
                                                                    target:self
                                                                    action:@selector(startScan)];
    
    self.navigationItem.leftBarButtonItem = cancelButton;
    
    self.navigationItem.rightBarButtonItem = submitButton;
}

- (void)cancelButtonTapped
{
    [self dismissViewControllerAnimated:YES completion:NULL];
}

- (void)refreshBasket
{
    ocg_async_background_overlay(^{
        
        [RESTController.sharedInstance lookupSaleWithBarcode:nil
                                            originalBasketId:_basket.basketID
                                             originalStoreId:_basket.store.storeId
                                          originalTillNumber:_basket.tillNumber
                                     originalTransactionDate:_basket.transactionDate
                                                    complete:^(BasketAssociateDTO *basket, NSError *error, ServerError *serverError)
         {
             dispatch_async(dispatch_get_main_queue(), ^{
                 if (error != nil || serverError != nil)
                 {
                     [[ServerErrorHandler sharedInstance] handleServerError:serverError
                                                             transportError:error
                                                                withContext:nil
                                                               dismissBlock:nil
                                                                repeatBlock:nil];
                 }
                 else
                 {
                     self.basket = basket;
                 }
             });
         }];
    });
}

- (void)selectItem:(ReturnableSaleItem*)basketItem
{
    ReceiptItemViewController *viewController = [[ReceiptItemViewController alloc] init];
    viewController.type = ReceiptItemViewControllerTypeLinkedReturn;
    NSMutableDictionary *initialItemDetails = [NSMutableDictionary dictionary];
    initialItemDetails[@(ReceiptItemDetailLineId)] = basketItem.basketItemId;
    initialItemDetails[@(ReceiptItemDetailPrice)] = [basketItem.pricing.amount description];
    initialItemDetails[@(ReceiptItemDetailOriginalTransactionStoreId)] = _basket.store.storeId;
    initialItemDetails[@(ReceiptItemDetailOriginalTransactionTillNumber)] = _basket.tillNumber;
    initialItemDetails[@(ReceiptItemDetailOriginalTransactionId)] = _basket.basketID;
    initialItemDetails[@(ReceiptItemDetailOriginalTransactionDate)] = _basket.transactionDate;
    initialItemDetails[@(ReceiptItemDetailItemScanId)] = basketItem.itemID;
    initialItemDetails[@(ReceiptItemDetailBastketItem)] = basketItem;
    
    if (basketItem.serialNumber)
    {
        initialItemDetails[@(ReceiptItemDetailItemSerialNumber)] = basketItem.serialNumber;
    }
    
    if (basketItem.originalItemScanId)
    {
        initialItemDetails[@(ReceiptItemDetailOriginalItemScanId)] = basketItem.originalItemScanId;
    }
    
    if (basketItem.originalItemDescription)
    {
        initialItemDetails[@(ReceiptItemDetailOriginalItemDescription)] = basketItem.originalItemDescription;
    }
    
    viewController.initialItemDetails = initialItemDetails;
    
    viewController.sucessfulReturn = ^{
        [self refreshBasket];
    };
    
    [self.navigationController pushViewController:viewController animated:YES];
}

- (void) startScan
{
    BarcodeScannerController *controller = [BarcodeScannerController sharedInstance];
    // are we scanning?
    if (!controller.scanning) {
        // no, start scanning
        [controller beginScanningInViewController:self.navigationController
                                        forReason:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁓󠁔󠀹󠀹󠀯󠁒󠁡󠁲󠁰󠁪󠀹󠁶󠁁󠁙󠁹󠀴󠁲󠀫󠁎󠁕󠁈󠀯󠁱󠁒󠁨󠁴󠁁󠁿*/ @"Receipt", nil)
                                   withButtonSize:CGSizeZero
                                     withAddTitle:nil
                                         mustScan:NO
                              alphaNumericAllowed:YES
                                        withBlock:^(NSString *barcode, ScannerEntryMethod entryMethod, SMBarcodeType barcodeType, BOOL *continueScanning) {
                                            // debug assertions
                                            NSAssert(barcode != nil,
                                                     @"The reference to the scanned barcode has not been initialized.");
                                            NSAssert([[barcode stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length] > 0,
                                                     @"The returned barcode does not contain any characters.");
                                            // we only want to scan the one barcode
                                            *continueScanning = NO;
                                            // grab the barcode we scanned
                                            _scannedBarcode =
                                            [barcode stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                                        }
                                       terminated:^{
                                           if (_scannedBarcode != nil) {
                                               // process the barcode
                                               [self handleScannedBarcode:_scannedBarcode];
                                               _scannedBarcode = nil;
                                           }
                                       }
                                            error:^(NSString *errorDesc) {
                                                if (errorDesc) {
                                                    // display an alert
                                                    UIAlertView *alert =
                                                    [[UIAlertView alloc] initWithTitle:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁯󠀫󠁋󠁆󠁢󠁚󠁱󠁩󠁈󠁕󠀲󠀸󠁫󠁲󠁧󠁦󠀲󠁴󠁘󠁣󠁈󠁎󠀶󠀲󠁯󠀶󠁳󠁿*/ @"Scanning Error", nil)
                                                                               message:errorDesc
                                                                              delegate:nil
                                                                     cancelButtonTitle:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁄󠁴󠁍󠁉󠁣󠀫󠁺󠁔󠁗󠁦󠁹󠁡󠁯󠀷󠁨󠁔󠁩󠀸󠁯󠀫󠁨󠁘󠀹󠁱󠁌󠀶󠀸󠁿*/ @"OK", nil)
                                                                     otherButtonTitles:nil];
                                                    [alert show];
                                                }
                                            }];
    }
}

-(void)handleScannedBarcode:(NSString*)scannedBarcode
{
    ocg_async_background_overlay(^{
        [RESTController.sharedInstance findLineIdWithItemScanId:scannedBarcode
                                               originalBasketId:_basket.basketID
                                                originalStoreId:_basket.store.storeId
                                             originalTillNumber:_basket.tillNumber
                                        originalTransactionDate:_basket.transactionDate
                                                       complete:^(ReturnableSaleItem *basketItem, NSError *error, ServerError *serverError)
         {
             dispatch_async(dispatch_get_main_queue(), ^{
                 if (error != nil || serverError != nil)
                 {
                     ServerErrorMessage *message = [serverError.messages lastObject];
                     enum MShopperExceptionCode exceptionCode = [message.code integerValue];
                     
                     if (exceptionCode == MShopperItemNotFoundExceptionCode && [[BasketController sharedInstance] doesUserHavePermissionTo:POSPermissionAddReturnItemUnlinked])
                     {
                         UIAlertView *alert =
                         [[UIAlertView alloc] initWithTitle:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁚󠀳󠁖󠀶󠁙󠁶󠀱󠁂󠁎󠁍󠀸󠁎󠁢󠀴󠁋󠁳󠁚󠁮󠁏󠁦󠁆󠁥󠁶󠁖󠀲󠁉󠁉󠁿*/ @"Item not found", nil)
                                                    message:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁖󠁵󠁨󠁈󠁥󠀰󠀳󠁌󠀱󠁨󠁨󠁋󠀶󠁡󠁘󠁬󠀸󠀫󠁃󠀹󠀷󠁨󠀷󠁬󠁶󠁊󠁫󠁿*/ @"Continue as an unlinked return?", nil)
                                                   delegate:[UIAlertView class]
                                          cancelButtonTitle:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁩󠁢󠁺󠁬󠁏󠁶󠁣󠁧󠁫󠁍󠁋󠁧󠁫󠀱󠁶󠁐󠁐󠀹󠁘󠀴󠁘󠁡󠁕󠁭󠁹󠁷󠀰󠁿*/ @"Cancel", nil)
                                          otherButtonTitles:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁄󠁴󠁍󠁉󠁣󠀫󠁺󠁔󠁗󠁦󠁹󠁡󠁯󠀷󠁨󠁔󠁩󠀸󠁯󠀫󠁨󠁘󠀹󠁱󠁌󠀶󠀸󠁿*/ @"OK", nil), nil];
                         
                         alert.dismissBlock = ^(int buttonIndex) {
                             if (buttonIndex == 0)
                             {
                                 ReceiptItemViewController *viewController = [[ReceiptItemViewController alloc] init];
                                 viewController.type = ReceiptItemViewControllerTypeUnlinkedReturn;
                                 NSMutableDictionary *initialItemDetails = [NSMutableDictionary dictionary];
                                 initialItemDetails[@(ReceiptItemDetailItemScanId)] = scannedBarcode;
                                 initialItemDetails[@(ReceiptItemDetailOriginalTransactionStoreId)] = _basket.store.storeId;
                                 initialItemDetails[@(ReceiptItemDetailOriginalTransactionTillNumber)] = _basket.tillNumber;
                                 initialItemDetails[@(ReceiptItemDetailOriginalTransactionId)] = _basket.basketID;
                                 initialItemDetails[@(ReceiptItemDetailOriginalTransactionDate)] = _basket.transactionDate;
                                 if (basketItem.originalItemScanId)
                                 {
                                     initialItemDetails[@(ReceiptItemDetailOriginalItemScanId)] = basketItem.originalItemScanId;
                                 }
                                 
                                 if (basketItem.originalItemDescription)
                                 {
                                     initialItemDetails[@(ReceiptItemDetailOriginalItemDescription)] = basketItem.originalItemDescription;
                                 }
                                 
                                 viewController.initialItemDetails = initialItemDetails;
                                 
                                 viewController.sucessfulReturn = ^{
                                     [self refreshBasket];
                                 };
                                 
                                 [self.navigationController pushViewController:viewController animated:YES];
                             }
                         };
                         [alert show];
                     }
                     else
                     {
                         [[ServerErrorHandler sharedInstance] handleServerError:serverError
                                                                 transportError:error
                                                                    withContext:nil
                                                                   dismissBlock:^{/* Needed to show dismiss button*/}
                                                                    repeatBlock:^{
                                                                        [self startScan];
                                                                    }];
                     }
                 }
                 else
                 {
                     [self selectItem:basketItem];
                 }
             });
         }];
    });
}

@end
