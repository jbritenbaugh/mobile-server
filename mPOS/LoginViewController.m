//
//  LoginViewController.m
//  mPOS
//
//  Created by Meik Schuetz on 05/09/2012.
//  Copyright (c) 2012 Clarity. All rights reserved.
//

#import "LoginViewController.h"

#import "FNKCollectionViewLayout.h"

#import "OCGCollectionViewCell.h"

#import "BasketController.h"
#import "CRSSkinning.h"
#import "RESTController.h"
#import "UIAlertView+MKBlockAdditions.h"
#import "AppDelegate.h"
#import "OCGPeripheralManager.h"
#import "OCGPeripheralDevice.h"
#import "NSArray+OCGIndexPath.h"
#import <QuartzCore/QuartzCore.h>
#import "TestModeController.h"
#import "PasswordResetViewController.h"
#import "UIDevice+OCGIdentifier.h"
#import "ServerErrorHandler.h"
#import "OCGPOSKeyboardToolbar.h"
#import "FNKInputView.h"
#import "TillSetupController.h"
#import "CRSLocationController.h"

#define kAuthUsernameKey @"kAuthUsernameKey"
#define kAuthPasswordKey @"kAuthPasswordKey"

#define kTableEntryUsername @"kTableEntryUsername"
#define kTableEntryCard @"kTableEntryCard"
#define kTableEntryPassword @"kTableEntryPassword"
#define kTableEntryLoginButton @"kTableEntryLoginButton"

#define PAYWARE_KEY @"loginSwipe"

@interface LoginViewController () <UITextFieldDelegate, PasswordResetViewControllerDelegate>
@property (strong, nonatomic) NSArray* layout;
@property (strong, nonatomic) NSDictionary *storedSwipeDetails;
- (void) updateImage:(UIInterfaceOrientation)orientation;
@end

@implementation LoginViewController

#pragma mark - Private

static NSInteger kTableViewBackgroundImageTag = 101077;

- (void) updateImage:(UIInterfaceOrientation)orientation
{
    // get the image
    UIImage *backImage = [UIImage imageNamed:@"CompanyAssets/logon_background"];
    // set the image
    if (backImage) {
        // try and find an existing image view
        UIView *tempView = self.collectionView.backgroundView;
        if ((![tempView isKindOfClass:[UIImageView class]]) ||
            (tempView.tag != kTableViewBackgroundImageTag)) {
            // not our view, create a new one
            tempView = [[UIImageView alloc] initWithFrame:self.collectionView.frame];
            tempView.contentMode = UIViewContentModeTop;
            tempView.tag = kTableViewBackgroundImageTag;
        }
        ((UIImageView *)tempView).image = backImage;
        [self.collectionView setBackgroundView:tempView];
    }
}

#pragma mark - Init

- (instancetype)init
{
    FNKCollectionViewLayout *layout = [[FNKCollectionViewLayout alloc] init];
    self = [super initWithCollectionViewLayout:layout];
    if (self) {
        
    }
    return self;
}

#pragma mark - UIViewController

/** @brief Called after the controller’s view is loaded into memory.
 */
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // make sure that we can navigate back to the main view
    
    [self.navigationController setNavigationBarHidden: NO
                                             animated: NO];
    
    self.collectionView.tag = kEditableTableSkinningTag;
    self.collectionView.backgroundColor = [UIColor colorWithRed:0.937 green:0.937 blue:0.957 alpha:1.000];
    self.collectionView.delaysContentTouches = NO;
    [OCGCollectionViewCell registerForDetailClass:[UITextField class]
                                          options:OCGCollectionViewCellDetailOptionUseTextLabelBaseline
                                   collectionView:self.collectionView];
//    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 320.0f, 10.0f)];
    
    // load the table background image
    [self updateImage:[UIApplication sharedApplication].statusBarOrientation];
    
    [[OCGPeripheralManager sharedInstance] registerForNotifications:self
                                                       withSelector:@selector(swipeNotification:)];
}


-(void)setupTable
{
    NSMutableArray *signOnMethodPropertyRows = [NSMutableArray array];
    
    for (NSString *signOnMethodProperty in [BasketController sharedInstance].signOnMethodProperties)
    {
        if ([signOnMethodProperty isEqualToString:@"SWIPE"])
        {
            [signOnMethodPropertyRows addObject:kTableEntryCard];
        }
        else if ([signOnMethodProperty isEqualToString:@"USERNAME"])
        {
            [signOnMethodPropertyRows addObject:kTableEntryUsername];
        }
        else if ([signOnMethodProperty isEqualToString:@"PASSWORD"])
        {
            [signOnMethodPropertyRows addObject:kTableEntryPassword];
        }
    }
    
    self.layout = @[
                    signOnMethodPropertyRows,
                    ];
    
    [self.collectionView reloadData];
}

-(IBAction)presentSettingsView:(id)sender
{
    AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    [appDelegate presentSettingsViewAnimated:YES viewController:self];
}

-(void)viewWillAppear:(BOOL)animated
{
#if !DEBUG
    [NSUserDefaults.standardUserDefaults removeObjectForKey:kAuthUsernameKey];
    [NSUserDefaults.standardUserDefaults removeObjectForKey:kAuthPasswordKey];
    [NSUserDefaults.standardUserDefaults synchronize];
#endif
    
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁏󠁐󠀵󠁖󠁨󠁭󠁭󠁑󠁁󠁉󠁚󠁁󠁃󠁭󠁚󠁓󠁅󠀵󠁘󠀴󠀱󠁂󠀫󠁤󠀱󠁃󠁯󠁿*/ @"Settings", nil) style:UIBarButtonItemStylePlain  target:self action:@selector(presentSettingsView:)];

    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁦󠀯󠁢󠁘󠁈󠁡󠁰󠁥󠁧󠁯󠁥󠁓󠁨󠁁󠁯󠁧󠁧󠀯󠁣󠁧󠁍󠁵󠁑󠁈󠁓󠁥󠁫󠁿*/ @"Sign in", nil) style:UIBarButtonItemStyleDone  target:self action:@selector(tappedSigninButton)];
    self.navigationItem.rightBarButtonItem.tag = kPrimaryButtonInvertedSkinningTag;

#if 0
    /*
     This can be enables to allow direct access to the password reset view for developement purposes.
     */
    self.navigationItem.rightBarButtonItems = @[self.navigationItem.rightBarButtonItem,  [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠀱󠁆󠁴󠁒󠁣󠁅󠁐󠁵󠁊󠁵󠀴󠁩󠁵󠁍󠁐󠀲󠁪󠀴󠁚󠁒󠁨󠁏󠀯󠀱󠀵󠁒󠀸󠁿*/ @"CP", nil) style:UIBarButtonItemStyleDone  target:self action:@selector(passwordResetRequested)]];
    
    [self.navigationItem.rightBarButtonItem setTitleTextAttributes:@{UITextAttributeTextColor: [UIColor whiteColor]} forState:UIControlStateNormal];
#endif

    // apply skin
    [[CRSSkinning currentSkin] applyViewSkin:self];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(setupTable)
                                                 name:kBasketControllerConfigurationDidChangeNotification
                                               object:nil];
    [self setupTable];
    [self updateImage:[UIApplication sharedApplication].statusBarOrientation];
    [super viewWillAppear:animated];
}


/** @brief Notifies the view controller that its view was added to a view hierarchy.
 *  @param animated If YES, the view was added to the window using an animation.
 */
- (void) viewDidAppear:(BOOL)animated
{
    // set the view's title
    self.navigationItem.title = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁦󠀯󠁢󠁘󠁈󠁡󠁰󠁥󠁧󠁯󠁥󠁓󠁨󠁁󠁯󠁧󠁧󠀯󠁣󠁧󠁍󠁵󠁑󠁈󠁓󠁥󠁫󠁿*/ @"Sign in", nil);

    // make sure that we can navigate back to the main view
    
    [self.navigationController setNavigationBarHidden: NO
                                             animated: animated];
        
    
    
    // make sure that the user name becomes first responder
   
    dispatch_async(dispatch_get_main_queue(), ^(void) {
        
        NSArray *signOnMethodProperties = [BasketController.sharedInstance.clientSettings.signOnMethod componentsSeparatedByString:@","];
        
        
        if ([signOnMethodProperties containsObject:@"USERNAME"]) {
            NSArray *indexPaths = [self.layout indexPathsForObject:kTableEntryUsername];
            if (indexPaths.count > 0) {
                OCGCollectionViewCell *userNameFieldCell = (OCGCollectionViewCell *)[self.collectionView cellForItemAtIndexPath:indexPaths[0]];
                [userNameFieldCell becomeFirstResponder];
            }
        }
        else if ([signOnMethodProperties containsObject:@"PASSWORD"])
        {
            NSArray *indexPaths = [self.layout indexPathsForObject:kTableEntryPassword];
            if (indexPaths.count > 0) {
                OCGCollectionViewCell *passwordFieldCell = (OCGCollectionViewCell *)[self.collectionView cellForItemAtIndexPath:indexPaths[0]];
                [passwordFieldCell becomeFirstResponder];
            }
        }
    });
}

- (void)viewDidDisappear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:kBasketControllerConfigurationDidChangeNotification
                                                  object:nil];
    
    // call super
    
    [super viewDidAppear: animated];
}

- (void) swipeNotification:(NSNotification *)note
{
    DebugLog(@"Got notification");
    // only listen for card swipes
    if ([note.name isEqualToString:kPeripheralNotificationCardSwiped]) {
        DebugLog(@"Got card swipe notification: %@", note.userInfo);
        // check if we are listening for this
        if ([note.userInfo[kPeripheralNotificationCardContextKey] isEqualToString:PAYWARE_KEY]) {
            // get the swipe cell
            NSArray *indexPaths = [self.layout indexPathsForObject:kTableEntryCard];
            if (indexPaths.count > 0) {
                OCGCollectionViewCell *swipeCardCell = (OCGCollectionViewCell *)[self.collectionView cellForItemAtIndexPath:indexPaths[0]];
                if (note.userInfo[kPeripheralNotificationCardTrack2Key] != nil) {
                    self.storedSwipeDetails = note.userInfo;
                    swipeCardCell.detailTextField.text = @"●●●●●●●●●●";
                } else {
                    self.storedSwipeDetails = nil;
                    swipeCardCell.detailTextField.text = @"";
                }
                [self.collectionView reloadItemsAtIndexPaths:indexPaths];
                // get the password cell
                indexPaths = [self.layout indexPathsForObject:kTableEntryPassword];
                if (indexPaths.count > 0) {
                    OCGCollectionViewCell *passwordFieldCell = (OCGCollectionViewCell *)[self.collectionView cellForItemAtIndexPath:indexPaths[0]];
                    // make it first responder
                    [passwordFieldCell becomeFirstResponder];
                }
            }
            
            [[OCGPeripheralManager sharedInstance].currentScannerDevice stopMSRSwipe];
        }
    }
}

/** @brief Called when the controller’s view is released from memory.
 */
- (void)viewDidUnload
{
    [super viewDidUnload];

    [[OCGPeripheralManager sharedInstance] unregisterForNotifications:self];
    
    self.delegate = nil;
}

- (void) willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    [self updateImage:toInterfaceOrientation];
    [super willRotateToInterfaceOrientation:toInterfaceOrientation duration:duration];
}

- (void)viewWillLayoutSubviews
{
    [super viewWillLayoutSubviews];
    
    self.collectionView.scrollEnabled = NO;
    [self.collectionView scrollRectToVisible:CGRectZero
                                    animated:NO];
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return [self.layout count];
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [self.layout[section] count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView
                  cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *reuseId = [OCGCollectionViewCell registerForDetailClass:[UITextField class]
                                                              options:OCGCollectionViewCellDetailOptionUseTextLabelBaseline
                                                       collectionView:collectionView];
    OCGCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:reuseId
                                                                            forIndexPath:indexPath];
    NSString *authUserName = [[BasketController sharedInstance] associateUserName];
    NSString *authPassword = @"";
    
#if DEBUG
    authUserName = [NSUserDefaults.standardUserDefaults objectForKey:kAuthUsernameKey];
    authPassword = [NSUserDefaults.standardUserDefaults objectForKey:kAuthPasswordKey];
#endif
    NSString *entryIdentifier = [self.layout objectForIndexPath:indexPath];
    if ([entryIdentifier isEqualToString:kTableEntryUsername]) {
        // initialize the user name field
        UITextField *textField = cell.detailTextField;
        cell.textLabel.tag = kEditableCellKeySkinningTag;
        cell.detailView.tag = kEditableCellValueSkinningTag;
        cell.tag = kEditableTableCellSkinningTag;
        cell.detailRequirement = OCGCollectionViewCellDetailRequirementRequired;
        
        // initialize the user name table cell
        cell.textLabel.text = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁴󠁱󠁂󠁖󠀰󠁩󠀯󠁘󠁨󠀫󠁤󠁌󠁋󠁐󠁁󠁷󠁫󠁲󠁷󠁊󠀯󠁰󠁯󠁚󠁂󠀴󠁧󠁿*/ @"User", nil);
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        textField.text = authUserName;
        textField.clearButtonMode = UITextFieldViewModeAlways;
        textField.autocapitalizationType = UITextAutocapitalizationTypeNone;
        textField.autocorrectionType = UITextAutocorrectionTypeNo;
        textField.delegate = self;
        textField.secureTextEntry = NO;
        textField.FNKInputView_keyboardTypes = @[@(UIKeyboardTypeASCIICapable), @(UIKeyboardTypeNumberPad)];
        
        // add the toolbar
        textField.inputAccessoryView = [[FNKInputView alloc] initWithFrame:CGRectMake(0, 0, 200, 44) inputViewStyle:UIInputViewStyleKeyboard];
    } else if ([entryIdentifier isEqualToString:kTableEntryCard]) {
        UITextField *textField = cell.detailTextField;
        cell.textLabel.tag = kEditableCellKeySkinningTag;
        cell.detailView.tag = kEditableCellValueSkinningTag;
        cell.tag = kEditableTableCellSkinningTag;
        cell.detailRequirement = OCGCollectionViewCellDetailRequirementRequired;
        
        // initialize the swipe table cell
        cell.textLabel.text = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁃󠁶󠁐󠁧󠁅󠁇󠁇󠁴󠁘󠀯󠀲󠁹󠁨󠁺󠁲󠁃󠁗󠁙󠀲󠁗󠁫󠁍󠀱󠁇󠀳󠁁󠁅󠁿*/ @"Card", nil);
        cell.selectionStyle = UITableViewCellSelectionStyleBlue;
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        textField.clearButtonMode = UITextFieldViewModeAlways;
        textField.delegate = self;
        textField.secureTextEntry = YES;
    } else if ([entryIdentifier isEqualToString:kTableEntryPassword]) {
        UITextField *textField = cell.detailTextField;
        cell.textLabel.tag = kEditableCellKeySkinningTag;
        cell.detailView.tag = kEditableCellValueSkinningTag;
        cell.tag = kEditableTableCellSkinningTag;
        cell.detailRequirement = OCGCollectionViewCellDetailRequirementRequired;
        
        // initialize the password table cell
        cell.textLabel.text = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁫󠁏󠀱󠀯󠁲󠁕󠁇󠁁󠁥󠁘󠀵󠀲󠀲󠁙󠁡󠁇󠁌󠁇󠁖󠀵󠁭󠁅󠀶󠁢󠁮󠁬󠁣󠁿*/ @"Password", nil);
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        textField.text = authPassword;
        
        textField.clearButtonMode = UITextFieldViewModeAlways;
        textField.delegate = self;
        textField.autocapitalizationType = UITextAutocapitalizationTypeNone;
        textField.autocorrectionType = UITextAutocorrectionTypeNo;
        textField.secureTextEntry = YES;
        textField.FNKInputView_keyboardTypes = @[@(UIKeyboardTypeASCIICapable), @(UIKeyboardTypeNumberPad)];
        
        // add the toolbar
        textField.inputAccessoryView = [[FNKInputView alloc] initWithFrame:CGRectMake(0, 0, 200, 44) inputViewStyle:UIInputViewStyleKeyboard];
;
    } else {
        NSAssert(NO, @"Can't find a table entry for %@", indexPath);
    }
    
    cell.backgroundColor = [UIColor whiteColor];
    [[CRSSkinning currentSkin] applyViewSkin:cell
                             withTagOverride:cell.tag];
    
    return cell;
}

#pragma mark - UICollectionViewDelegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *entryIdentifier = [self.layout objectForIndexPath:indexPath];

    if ([entryIdentifier isEqualToString:kTableEntryUsername]) {
        OCGCollectionViewCell *userNameFieldCell = (OCGCollectionViewCell *)[collectionView cellForItemAtIndexPath:indexPath];
        [userNameFieldCell.detailTextField becomeFirstResponder];
    } else if ([entryIdentifier isEqualToString:kTableEntryCard]) {
        OCGPeripheralManagerDevice deviceType = [[OCGPeripheralManager sharedInstance] getConfiguredDevice];
        if (deviceType != OCGPeripheralManagerDeviceNone) {
            [[OCGPeripheralManager sharedInstance].currentScannerDevice startMSRSwipeWithContext:PAYWARE_KEY];
        } else {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁯󠀴󠁳󠁊󠁉󠁸󠀱󠀰󠀶󠁤󠁬󠁧󠁖󠁯󠁤󠀹󠁱󠀳󠀷󠁎󠁕󠁊󠀱󠁐󠁇󠀵󠁙󠁿*/ @"Card swipe", nil)
                                                            message:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁔󠁍󠁷󠁓󠁂󠁆󠁗󠁐󠁫󠀷󠀱󠁡󠁈󠀵󠁶󠁎󠁍󠁢󠀸󠁩󠁰󠀶󠁎󠁮󠀶󠁒󠀴󠁿*/ @"No payment device has been set up in Settings.", nil)
                                                           delegate:self
                                                  cancelButtonTitle:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁄󠁴󠁍󠁉󠁣󠀫󠁺󠁔󠁗󠁦󠁹󠁡󠁯󠀷󠁨󠁔󠁩󠀸󠁯󠀫󠁨󠁘󠀹󠁱󠁌󠀶󠀸󠁿*/ @"OK", nil)
                                                  otherButtonTitles:nil];
            [alert show];
        }        
    } else if ([entryIdentifier isEqualToString:kTableEntryPassword]) {
        OCGCollectionViewCell *passwordFieldCell = (OCGCollectionViewCell *)[collectionView cellForItemAtIndexPath:indexPath];
        [passwordFieldCell.detailView becomeFirstResponder];
    }
}

#pragma mark - FNKCollectionViewDataSourceDelegate

- (NSArray * /* FNKSupplementaryItem */)collectionView:(UICollectionView *)collectionView
                                                layout:(UICollectionViewLayout*)collectionViewLayout
               supplementaryItemsForHeadersAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.item == 0) {
        NSString *sectionTitle = nil;
        if ([TestModeController serverMode] == ServerModeOffline) {
            sectionTitle = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁖󠁯󠁔󠁺󠁏󠁪󠁨󠁉󠁍󠁢󠁰󠁯󠀹󠁡󠀴󠀵󠁩󠁒󠁎󠁱󠁲󠁌󠁘󠁅󠁶󠁦󠁷󠁿*/ @"Secondary Server", nil);
        } else {
            sectionTitle = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁆󠁱󠁐󠀯󠁕󠁸󠁹󠁸󠀶󠁈󠁥󠁇󠁡󠁯󠁭󠁧󠁚󠁶󠁏󠁴󠁴󠁣󠁪󠁦󠁷󠁗󠁅󠁿*/ @"Primary Server", nil);
        }
        FNKSupplementaryItem *result = [[FNKSupplementaryItem alloc] init];
        result.title = sectionTitle;
        
        return @[result];
    } else {
        return nil;
    }
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    // TFS70058 - default switching keyboard to NO, otherwise CAPS doesn't stay on in iOS 7
    textField.FNKInputView_switchingKeyboardType = NO;
    NSArray *indexPaths = [self.layout indexPathsForObject:kTableEntryCard];
    if (indexPaths.count > 0) {
        OCGCollectionViewCell *swipeCardCell = (OCGCollectionViewCell *)[self.collectionView cellForItemAtIndexPath:indexPaths[0]];
        if (textField == swipeCardCell.detailTextField) {
            return NO;
        }
    }
    
    return YES;
}

- (BOOL)textFieldShouldClear:(UITextField *)textField
{
    NSArray *indexPaths = [self.layout indexPathsForObject:kTableEntryCard];
    if (indexPaths.count > 0) {
        OCGCollectionViewCell *swipeCardCell = (OCGCollectionViewCell *)[self.collectionView cellForItemAtIndexPath:indexPaths[0]];
        if (textField == swipeCardCell.detailTextField) {
            self.storedSwipeDetails = nil;
            swipeCardCell.detailTextField.text = @"";
            return NO;
        }
    }
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    OCGCollectionViewCell *userNameFieldCell = nil;
    NSArray *indexPaths = [self.layout indexPathsForObject:kTableEntryUsername];
    if (indexPaths.count > 0) {
        userNameFieldCell = (OCGCollectionViewCell *)[self.collectionView cellForItemAtIndexPath:indexPaths[0]];
    }
    OCGCollectionViewCell *passwordFieldCell = nil;
    indexPaths = [self.layout indexPathsForObject:kTableEntryPassword];
    if (indexPaths.count > 0) {
        passwordFieldCell = (OCGCollectionViewCell *)[self.collectionView cellForItemAtIndexPath:indexPaths[0]];
    }

    if (textField == userNameFieldCell.detailTextField) {
        [passwordFieldCell.detailTextField becomeFirstResponder];
    } else if (textField == passwordFieldCell.detailView) {
        [self tappedSigninButton];
    }
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    textField.FNKInputView_switchingKeyboardType = NO;
}

#pragma mark - PasswordResetViewControllerDelegate

-(void)passwordResetViewController:(PasswordResetViewController *)passwordResetViewController password:(NSString*)password
{
    [passwordResetViewController.navigationController dismissViewControllerAnimated:YES
                                                                         completion:NULL];
    if (password != nil) {
        [self loginSignOffExistingOperator:NO newPassword:password];
    } else {
        OCGCollectionViewCell *passwordFieldCell = nil;
        NSArray *indexPaths = [self.layout indexPathsForObject:kTableEntryPassword];
        if (indexPaths.count > 0) {
            passwordFieldCell = (OCGCollectionViewCell *)[self.collectionView cellForItemAtIndexPath:indexPaths[0]];
            passwordFieldCell.detailTextField.text = @"";
        }
    }
}

#pragma mark - Methods

-(void) tappedSigninButton
{
    [self loginSignOffExistingOperator:NO newPassword:nil];
}

-(void)loginSignOffExistingOperator:(BOOL)signOffExistingOperator newPassword:(NSString*)newPassword
{
    NSArray *signOnMethodProperties = [BasketController.sharedInstance.clientSettings.signOnMethod componentsSeparatedByString:@","];
    
    Credentials *credentials = [[Credentials alloc] init];
    
    NSArray *indexPaths = [self.layout indexPathsForObject:kTableEntryUsername];
    OCGCollectionViewCell *userNameFieldCell = nil;
    if (indexPaths.count > 0) {
        userNameFieldCell = (OCGCollectionViewCell *)[self.collectionView cellForItemAtIndexPath:indexPaths[0]];
    }
    credentials.username = userNameFieldCell.detailTextField.text;
    indexPaths = [self.layout indexPathsForObject:kTableEntryPassword];
    OCGCollectionViewCell *passwordFieldCell = nil;
    if (indexPaths.count > 0) {
        passwordFieldCell = (OCGCollectionViewCell *)[self.collectionView cellForItemAtIndexPath:indexPaths[0]];
    }
    credentials.password = passwordFieldCell.detailTextField.text;
    credentials.updatedPassword = newPassword;
    
    if ([signOnMethodProperties containsObject:@"SWIPE"])
    {
        credentials.track1 = self.storedSwipeDetails[kPeripheralNotificationCardTrack1Key];
        credentials.track2 = self.storedSwipeDetails[kPeripheralNotificationCardTrack2Key];
        credentials.track3 = self.storedSwipeDetails[kPeripheralNotificationCardTrack3Key];
    }
    
    credentials.deviceId = [[UIDevice currentDevice] uniqueDeviceIdentifier];
    credentials.storeId = [CRSLocationController getLocationKey];
    credentials.tillNumber = [TillSetupController getTillNumber];
    
    [[BasketController sharedInstance] authenticateWithCredentials:credentials
                                           signOffExistingOperator: signOffExistingOperator
                                                         succeeded: ^(BasketController *controller) {
                                                             if (self.delegate != nil) {
                                                                 if ([self.delegate respondsToSelector:@selector(dismissModalLoginViewController:)]) {
                                                                     [self.delegate dismissModalLoginViewController: self];
                                                                 }
                                                             } else {
                                                                 [self dismissViewControllerAnimated:YES
                                                                                          completion:nil];
                                                             }
                                                             
#if DEBUG
                                                             [NSUserDefaults.standardUserDefaults setObject:credentials.username forKey:kAuthUsernameKey];
                                                             [NSUserDefaults.standardUserDefaults setObject:credentials.password forKey:kAuthPasswordKey];
                                                             [NSUserDefaults.standardUserDefaults synchronize];
#endif
                                                             
                                                             // authentication succeeded; pop the view controller
                                                             
                                                             [self.navigationController popViewControllerAnimated: YES];
                                                         }
                                                            failed:^(BasketController *controller, NSError *error, ServerError *serverError) {
                                                                BOOL passwordExpired = NO;
                                                                for (ServerErrorMessage *message in serverError.messages)
                                                                {
                                                                    if ([message.code intValue] == MShopperOperatorPasswordExpiredExceptionCode ||
                                                                        [message.code intValue] == MShopperOperatorNewPasswordNotComplexExceptionCode ||
                                                                        [message.code intValue] == MShopperOperatorPasswordAlreadyUsedExceptionCode) {
                                                                        passwordExpired = YES;
                                                                    }
                                                                }
                                                                
                                                                if (passwordExpired)
                                                                {
                                                                    [UIAlertView alertViewWithTitle: nil
                                                                                            message: NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁹󠁇󠁏󠁲󠀲󠁹󠁒󠁄󠁤󠁡󠁶󠁶󠀸󠀰󠁈󠁬󠁅󠀫󠁒󠁋󠁺󠁱󠁳󠁷󠁉󠁢󠀸󠁿*/ @"Password expired", nil)
                                                                                  cancelButtonTitle: NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁩󠁢󠁺󠁬󠁏󠁶󠁣󠁧󠁫󠁍󠁋󠁧󠁫󠀱󠁶󠁐󠁐󠀹󠁘󠀴󠁘󠁡󠁕󠁭󠁹󠁷󠀰󠁿*/ @"Cancel", nil)
                                                                                  otherButtonTitles: @[NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁅󠁺󠁷󠀲󠁮󠀶󠁢󠁅󠁂󠁊󠀹󠁐󠁤󠀸󠁘󠁺󠁹󠁱󠀰󠁳󠁨󠁱󠀳󠁐󠁳󠁫󠁯󠁿*/ @"Reset", nil)]
                                                                                          onDismiss: ^(int buttonIndex){
                                                                                              [self passwordResetRequested];
                                                                                          }
                                                                                           onCancel:^{
                                                                                               passwordFieldCell.detailTextField.text = @"";
                                                                                           }];
                                                                }
                                                                else
                                                                {
                                                                    [[ServerErrorHandler sharedInstance] handleServerError:serverError
                                                                                                            transportError:error
                                                                                                               withContext:nil dismissBlock:nil repeatBlock:nil];
                                                                }
                                                            }];
}

-(void) passwordResetRequested
{
    NSString *userName = nil;
    NSArray *indexPaths = [self.layout indexPathsForObject:kTableEntryUsername];
    OCGCollectionViewCell *userNameFieldCell = nil;
    if (indexPaths.count > 0) {
        userNameFieldCell = (OCGCollectionViewCell *)[self.collectionView cellForItemAtIndexPath:indexPaths[0]];
        userName = userNameFieldCell.detailTextField.text;
    }
    
    if (userName == nil)
    {
        userName = @"";
    }
    PasswordResetViewController *passwordResetViewController = [[PasswordResetViewController alloc] initWithOperatorID:userName];
    passwordResetViewController.delegate = self;
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:passwordResetViewController];
    [self presentViewController:navigationController
                       animated:YES
                     completion:NULL];
}

@end
