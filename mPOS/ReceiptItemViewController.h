//
//  ReceiptItemViewController.h
//  mPOS
//
//  Created by John Scott on 05/02/2014.
//  Copyright (c) 2014 Clarity. All rights reserved.
//

typedef NS_ENUM(NSInteger, ReceiptItemDetail)
{
    ReceiptItemDetailBasketId,
    ReceiptItemDetailItemScanId,
    ReceiptItemDetailItemSerialNumber,
    ReceiptItemDetailLineId,
    ReceiptItemDetailOriginalCustomerCard,
    ReceiptItemDetailOriginalItemDescription,
    ReceiptItemDetailOriginalItemScanId,
    ReceiptItemDetailOriginalReferenceId,
    ReceiptItemDetailOriginalSalesPerson,
    ReceiptItemDetailOriginalTransactionDate,
    ReceiptItemDetailOriginalTransactionId,
    ReceiptItemDetailOriginalTransactionStoreId,
    ReceiptItemDetailOriginalTransactionTillNumber,
    ReceiptItemDetailPrice,
    ReceiptItemDetailReasonDescription,
    ReceiptItemDetailReasonId,

    ReceiptItemDetailBastketItem,
    ReceiptItemDetailDumpCodeDescription,
};

typedef NS_ENUM(NSInteger, ReceiptItemViewControllerType)
{
    ReceiptItemViewControllerTypeNoReceiptReturn,
    ReceiptItemViewControllerTypeInternetReturn,
    ReceiptItemViewControllerTypeUnlinkedReturn,
    ReceiptItemViewControllerTypeLinkedReturn,
};

#import <UIKit/UIKit.h>

@interface ReceiptItemViewController : UICollectionViewController

@property (assign, nonatomic) ReceiptItemViewControllerType type;
@property (strong, nonatomic) NSDictionary *initialItemDetails;

@property (copy) void (^sucessfulReturn)();

@end
