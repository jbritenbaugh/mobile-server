//
//  SearchViewController.m
//  mPOS
//
//  Created by Antonio Strijdom on 29/01/2014.
//  Copyright (c) 2014 Clarity. All rights reserved.
//

#import "SearchViewController.h"
#import "CRSSkinning.h"
#import "CustomerController.h"
#import "CustomerSearchViewController.h"
#import "ItemSearchViewController.h"
#import "BasketController.h"

@interface SearchViewController () <CustomerViewControllerDelegate>
@property (nonatomic, strong) UIView *headerView;
@property (nonatomic, strong) UISegmentedControl *searchSwitcher;
@property (nonatomic, strong) CustomerSearchViewController *customerViewController;
@property (nonatomic, strong) ItemSearchViewController *itemViewController;
- (IBAction) searchSwitchChanged:(id)sender;
- (void) resetView;
@end

@implementation SearchViewController

#pragma mark - Private

static const CGFloat kSearchHeaderBarHeight = 40.0f;

- (IBAction) searchSwitchChanged:(id)sender
{
    if ((self.searchSwitcher.selectedSegmentIndex == 0) &&
        ([[BasketController sharedInstance] doesUserHavePermissionTo:POSPermissionLookupCustomer])) {
        // remove the item view
        [self.itemViewController willMoveToParentViewController:nil];
        [self.view removeConstraints:self.itemViewController.view.constraints];
        [self.itemViewController.view removeFromSuperview];
        [self.itemViewController removeFromParentViewController];
        // clear the last search
        [[CustomerController sharedInstance] clearCustomerSearch];
        // show the customer view
        [self.customerViewController willMoveToParentViewController:self];
        [self addChildViewController:self.customerViewController];
        self.customerViewController.view.translatesAutoresizingMaskIntoConstraints = NO;
        [self.customerViewController viewWillAppear:NO];
        [self.view addSubview:self.customerViewController.view];
        [self.customerViewController.view constrain:@"top = bottom"
                                                 to:self.headerView];
        [self.customerViewController.view constrain:@"left = left"
                                                 to:self.view];
        [self.customerViewController.view constrain:@"bottom = bottom"
                                                 to:self.view];
        [self.customerViewController.view constrain:@"right = right"
                                                      to:self.view];
        [self.customerViewController didMoveToParentViewController:self];
        [[CRSSkinning currentSkin] applyViewSkin:self.customerViewController];
        // display the add button if required
        self.navigationItem.rightBarButtonItem = nil;//self.customerViewController.addButton;
//        self.navigationItem.rightBarButtonItem.enabled = self.customerViewController.allowAdd;
    } else {
        // remove the customer view
        [self.customerViewController willMoveToParentViewController:nil];
        [self.view removeConstraints:self.customerViewController.view.constraints];
        [self.customerViewController.view removeFromSuperview];
        [self.customerViewController removeFromParentViewController];
        // show the item view
        [self.itemViewController willMoveToParentViewController:self];
        [self addChildViewController:self.itemViewController];
        self.itemViewController.view.translatesAutoresizingMaskIntoConstraints = NO;
        [self.itemViewController viewWillAppear:NO];
        [self.view addSubview:self.itemViewController.view];
        [self.itemViewController.view constrain:@"top = bottom"
                                             to:self.headerView];
        [self.itemViewController.view constrain:@"left = left"
                                             to:self.view];
        [self.itemViewController.view constrain:@"bottom = bottom"
                                             to:self.view];
        [self.itemViewController.view constrain:@"right = right"
                                             to:self.view];
        [self.itemViewController didMoveToParentViewController:self];
        // hide the add customer button
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
            self.navigationItem.rightBarButtonItem = nil;
        } else {
            self.navigationItem.leftBarButtonItem = nil;
        }
        [[CRSSkinning currentSkin] applyViewSkin:self.itemViewController];
    }
}

- (void) resetView
{
    // default to customer search
    if (self.searchSwitcher.numberOfSegments > 0) {
        self.searchSwitcher.selectedSegmentIndex = 0;
        [self searchSwitchChanged:self];
    } else {
        [[[UIAlertView alloc] initWithTitle:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁑󠀰󠁊󠁔󠁔󠁶󠁖󠁯󠀶󠁲󠁢󠁎󠁶󠁤󠁕󠁏󠀴󠁄󠁇󠁳󠁎󠁚󠁥󠁥󠁹󠀯󠁁󠁿*/ @"Search unavailable", nil)
                                   message:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁓󠁆󠁄󠁱󠁱󠁴󠁆󠁱󠁑󠀶󠁴󠁬󠁡󠁅󠁶󠁒󠀯󠁋󠁅󠁩󠀰󠁩󠀴󠁧󠁙󠁍󠀴󠁿*/ @"You do not have the required permissions to perform a search", nil)
                                  delegate:nil
                         cancelButtonTitle:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁄󠁴󠁍󠁉󠁣󠀫󠁺󠁔󠁗󠁦󠁹󠁡󠁯󠀷󠁨󠁔󠁩󠀸󠁯󠀫󠁨󠁘󠀹󠁱󠁌󠀶󠀸󠁿*/ @"OK", nil)
                         otherButtonTitles:nil] show];
    }
}

#pragma mark - UIViewController

- (void) viewDidLoad
{
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    
    // create the child views
    // customer
    self.customerViewController = [[CustomerSearchViewController alloc] initWithStyle:UITableViewStylePlain];
//    self.customerViewController.extendedLayoutIncludesOpaqueBars = YES;
    self.customerViewController.delegate = self;
    self.customerViewController.allowSearch = YES;
    self.customerViewController.allowAdd = YES;
    // item
    self.itemViewController = [[ItemSearchViewController alloc] initWithStyle:UITableViewStylePlain];
	
    // setup the tabs
    self.headerView = [[UIView alloc] init];
    self.headerView.tag = kSearchSkinningTag;
    self.headerView.translatesAutoresizingMaskIntoConstraints = NO;
    [self.view addSubview:self.headerView];
    [self.headerView constrain:@"top = top" to:self.view];
    [self.headerView constrain:@"left = left" to:self.view];
    [self.headerView constrain:@"right = right" to:self.view];
    [self.headerView constrain:[NSString stringWithFormat:@"height = %f", kSearchHeaderBarHeight]
                                                       to:nil];
}

- (void) viewWillAppear:(BOOL)animated
{
    self.title = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁮󠁰󠁎󠁩󠁧󠁑󠁒󠁭󠁗󠁗󠁊󠁵󠁗󠁏󠁏󠀯󠁱󠁃󠁘󠁪󠀲󠁗󠁘󠁣󠁈󠁲󠁫󠁿*/ @"Search", nil);
    
    // add the tabs
    NSMutableArray *items = [NSMutableArray array];
    if ([[BasketController sharedInstance] doesUserHavePermissionTo:POSPermissionLookupCustomer]) {
        [items addObject:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁗󠀫󠀹󠁗󠁊󠁈󠁅󠀰󠁮󠁸󠀰󠁆󠁺󠀷󠁨󠀲󠁈󠁢󠁍󠁚󠁏󠁰󠁈󠁸󠁴󠁇󠁉󠁿*/ @"Customer", nil)];
    }
    if ([[BasketController sharedInstance] doesUserHavePermissionTo:POSPermissionItemSearch]) {
        [items addObject:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠀶󠁤󠁑󠁔󠁕󠁎󠁂󠁍󠀸󠁩󠁥󠀷󠁮󠁢󠁩󠁋󠁨󠁶󠁷󠀴󠁄󠁷󠁯󠁳󠁗󠁑󠀸󠁿*/ @"Product", nil)];
    }
    self.searchSwitcher =
    [[UISegmentedControl alloc] initWithItems:items];
    self.searchSwitcher.tag = kSearchSkinningTag;
    self.searchSwitcher.translatesAutoresizingMaskIntoConstraints = NO;
    [self.headerView addSubview:self.searchSwitcher];
    [self.searchSwitcher constrain:@"left = left + 5" to:self.headerView];
    [self.searchSwitcher constrain:@"right = right - 5" to:self.headerView];
    [self.searchSwitcher constrain:@"top = top + 5" to:self.headerView];
    [self.searchSwitcher constrain:@"bottom = bottom - 5" to:self.headerView];
    [self.searchSwitcher addTarget:self
                            action:@selector(searchSwitchChanged:)
                  forControlEvents:UIControlEventValueChanged];
    
    [[CRSSkinning currentSkin] applyViewSkin:self];
    [self resetView];
    [super viewWillAppear:animated];
}

#pragma mark - CustomerViewControllerDelegate

- (void) dismissCustomerSearchViewController:(CustomerViewController *)customerController
{
    [self.navigationController popViewControllerAnimated:YES];
}

@end
