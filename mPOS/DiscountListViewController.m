//
//  DiscountListViewController.m
//  mPOS
//
//  Created by Meik Schuetz on 20/08/2012.
//  Copyright (c) 2012 Clarity. All rights reserved.
//

#import "DiscountListViewController.h"
#import "BasketDiscountViewController.h"
#import "ItemDiscountTableViewCell.h"
#import "BasketController.h"
#import "CRSSkinning.h"

@interface DiscountListViewController ()
@property (strong, nonatomic) NSArray *assignedDiscounts;
@property (strong, nonatomic) UIView *messageView;
@property (strong, nonatomic) UILabel *messageLabel;

/** @brief Navigation bar button to dismiss the view controller.
 */
@property (strong, nonatomic) UIBarButtonItem *dismissButton;

/** @brief Navigation bar button to add a new discount.
 */
@property (strong, nonatomic) UIBarButtonItem *addDiscountButton;

/** @brief YES, if the basket operations should be shown.
 */
@property (atomic, readonly) BOOL showBasketDiscounts;

/** @brief YES, if the basket item operations should be shown.
 */
@property (atomic, readonly) BOOL showBasketItemDiscounts;

@end

@implementation DiscountListViewController
@synthesize showDismissNavigationButton = _showDismissNavigationButton;
@synthesize showAddDiscountNavigationButton = _showAddDiscountNavigationButton;

#pragma mark Object Lifecycle

/** @brief Called after the controller’s view is loaded into memory.
 */
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // set the table view to edit mode
    
    [self.tableView setEditing: YES animated: NO];
    
    // apply skin
    self.tableView.tag = kTableSkinningTag;
}

/** @brief Notifies the view controller that its view is about to be added to a view hierarchy.
 *  @param animated If YES, the view is being added to the window using an animation.
 */
- (void)viewWillAppear:(BOOL)animated
{
    // set the view's title
    
    self.title = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠀷󠁩󠁋󠁑󠁁󠁗󠁢󠁺󠁳󠁒󠁗󠁥󠁚󠁕󠁲󠀶󠁖󠀯󠁲󠀵󠁗󠁌󠁊󠁥󠁉󠀷󠁑󠁿*/ @"Discounts", nil);
    
    // initialize the dismiss button
    
    self.dismissButton = [[UIBarButtonItem alloc] initWithTitle: NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁡󠁩󠁡󠁍󠁰󠀸󠁷󠁣󠁹󠁩󠁕󠀷󠁪󠁶󠀱󠀸󠁐󠁖󠀵󠁥󠀳󠁫󠁕󠁚󠁄󠁶󠁙󠁿*/ @"Dismiss", nil)
                                                          style: UIBarButtonItemStylePlain
                                                         target: self
                                                         action: @selector(dismissViewController:)];
    
    // initialize the add discount button
    
    self.addDiscountButton = [[UIBarButtonItem alloc] initWithTitle: NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁧󠁙󠁊󠁁󠁔󠁑󠁹󠁨󠀫󠁬󠁦󠀫󠁮󠁫󠁂󠁃󠁪󠁬󠁧󠁸󠁋󠁇󠁱󠁴󠁕󠀵󠁍󠁿*/ @"Add", nil)
                                                              style: UIBarButtonItemStyleDone
                                                             target: self
                                                             action: @selector(addDiscount:)];
    
    [[CRSSkinning currentSkin] applyViewSkin: self];
    // call the super class
    
    self.assignedDiscounts = nil;
    [self.tableView reloadData];
    
    [super viewWillAppear: animated];
    
    // subscribe to discount chnage notifications
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(handleBasketControllerDiscountChangeNotification:)
                                                 name:kBasketControllerDiscountChangeNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(handleBasketControllerDiscountChangeNotification:)
                                                 name:kBasketControllerKickedNotification
                                               object:nil];
    
    // update the buttons that should appear in the navigation bar
    
    [self updateNavigationBarButtons];
}

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

/** @brief Called when the controller’s view is released from memory.
 */
- (void)viewDidUnload
{
    [super viewDidUnload];
    
    self.tableView.delegate = nil;
    self.tableView.dataSource = nil;
}

#pragma mark Properties

/** @brief YES, if a Add Discount button should be shown in the navigation bar.
 */
- (BOOL) showAddDiscountNavigationButton
{
    return _showAddDiscountNavigationButton;
}

/** @brief YES, if a Add Discount button should be shown in the navigation bar.
 */
- (void) setShowAddDiscountNavigationButton:(BOOL)showAddDiscountNavigationButton
{
    _showAddDiscountNavigationButton = showAddDiscountNavigationButton;
    [self updateNavigationBarButtons];
}

/** @brief YES, if a Dismisss button should be shown in the navigation bar.
 */
- (BOOL) showDismissNavigationButton
{
    return _showDismissNavigationButton;
}

/** @brief YES, if a Dismisss button should be shown in the navigation bar.
 */
- (void) setShowDismissNavigationButton:(BOOL)showDismissNavigationButton
{
    _showDismissNavigationButton = showDismissNavigationButton;
    [self updateNavigationBarButtons];
}

/** @brief YES, if the basket operations should be shown.
 */
- (BOOL) showBasketDiscounts
{
    return (self.basketItem == nil);
}

/** @brief YES, if the basket item operations should be shown.
 */
- (BOOL) showBasketItemDiscounts
{
    return (self.basketItem != nil);
}

/** @brief Sets the basket item.
 */
- (void) setBasketItem:(MPOSBasketItem *)basketItem
{
    _basketItem = basketItem;
    // reset discount list
    self.assignedDiscounts = nil;
    [self.tableView reloadData];
}

@synthesize assignedDiscounts = _assignedDiscounts;
- (NSArray *) assignedDiscounts
{
    if (_assignedDiscounts == nil) {
        if (self.showBasketDiscounts) {
            NSMutableArray *discounts = [NSMutableArray array];
            for (MPOSBasketItem *basketItem in BasketController.sharedInstance.basket.basketItems) {
                NSString *entity = NSStringFromClass([basketItem class]);
                if ([entity hasPrefix:@"BasketRewardSale"]) {
                    [discounts addObject:basketItem];
                }
            }
            _assignedDiscounts = discounts;
        } else {
            NSMutableArray *discounts = [NSMutableArray array];
            for (MPOSBasketRewardLine *reward in self.basketItem.basketRewards) {
                // MPOS-391 exclude price adjustments from the reward array
                if (![reward isKindOfClass:[MPOSBasketRewardLinePriceAdjust class]]) {
                    [discounts addObject:reward];
                }
            }
            _assignedDiscounts = discounts;
        }
    }
    
    return _assignedDiscounts;
}

#pragma mark Private Methods

/** @brief Called to initialize/ update the buttons that appear in the navigation bar.
 */
- (void) updateNavigationBarButtons
{
    self.navigationItem.leftBarButtonItem = (self.showDismissNavigationButton ? self.dismissButton : nil);
    self.navigationItem.rightBarButtonItem = (self.showAddDiscountNavigationButton ? self.addDiscountButton : nil);
}

/** @brief Sets up the custom view with a label that is used to display
 *  non modal information messages as table section header.
 *  @param message The message text that should be displayed.
 *  @return A reference to the view that should be used as section header.
 */
- (UIView *) setupTableInfoViewWithMessage:(NSString *)message
{
    if (self.messageView == nil) {
        self.messageView = [[UIView alloc] initWithFrame:CGRectMake(20.0f, 0.0f, self.view.frame.size.width - 40.0f, 60.0f)];
        self.messageView.backgroundColor = [UIColor clearColor];
        
        self.messageLabel = [[UILabel alloc] initWithFrame: self.messageView.frame];
        [self.messageView addSubview: self.messageLabel];
        self.messageLabel.textColor = [UIColor lightTextColor];
        self.messageLabel.shadowOffset = CGSizeMake(0.0f, -1.0f);
        self.messageLabel.shadowColor = [UIColor blackColor];
        self.messageLabel.textAlignment = NSTextAlignmentCenter;
        self.messageLabel.backgroundColor = [UIColor clearColor];
        self.messageLabel.numberOfLines = 0;
    }
        
    self.messageLabel.text = message;
    return self.messageView;
}

/** @brief Sets up a message to be displayed in the section header view.
 *  @discussion Dependent on the current state, this method should construct
 *  any messages necessary to be displayed.
 *  @return A message that should be displayed non-modally in the table view.
 */
- (NSString *) setupMessageForView
{
    if (self.assignedDiscounts.count <= 0)
    {
        if (self.basketItem == nil)
        {
            return NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁪󠀫󠁎󠀹󠁧󠁓󠁙󠁘󠁩󠁚󠁹󠁒󠁱󠁹󠁴󠁐󠁡󠁯󠁄󠁸󠁢󠁪󠁢󠁳󠁮󠁆󠁧󠁿*/ @"There are no discounts applied to this basket.", nil);
        }
        else
        {
            return NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁩󠁣󠁮󠁺󠀫󠁌󠁤󠁪󠀹󠁥󠀲󠁥󠁳󠁣󠁃󠁳󠀵󠁚󠁪󠁋󠁩󠀶󠀰󠀯󠁴󠁏󠁫󠁿*/ @"There are no discounts applied to this item.", nil);
        }
    }
    
    return nil;
}

/** @brief Dismisses the view controller.
 */
- (void) dismissViewController
{
    BOOL viewControllerHasBeenDismissed = NO;
    if (self.delegate != nil) {
        if ([self.delegate respondsToSelector:@selector(dismissDiscountListViewController:)]) {
            
            [self.delegate dismissDiscountListViewController: self];
        }
    }
    
    if (!viewControllerHasBeenDismissed) {

        [self.navigationController popViewControllerAnimated: YES];
    }
}

#pragma mark Actions

/** @brief Tries to dismiss the view controller.
 *  @param sender A reference to the object that sent the message.
 */
- (void) dismissViewController:(id)sender
{
    // dissmiss the view
    
    [self dismissViewController];
}

/** @brief Adds a new discount to the list of discounts
 */
- (void) addDiscount:(id)sender
{
    BasketDiscountViewController* discountViewController = [[BasketDiscountViewController alloc] initWithNibName: @"BasketDiscountView"
                                                                                     bundle: nil];
    discountViewController.discountType = (self.showBasketDiscounts ? DiscountTypeBasketDiscount : DiscountTypeBasketItemDiscount);
    if (self.basketItem != nil) {
        discountViewController.basketItems = @[self.basketItem];
    } else {
        discountViewController.basketItems = nil;
    }
    [self.navigationController pushViewController: discountViewController animated: YES];
}

#pragma mark - Table view delegate

/** @brief Asks the delegate for the height to use for the header of a particular section.
 *  @param The table-view object requesting this information.
 *  @param An index number identifying a section of tableView.
 *  @return A floating-point value that specifies the height (in points) of the header for section.
 */
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (section == 0)
        return 0.0f;
    
    NSString *messageString = [self setupMessageForView];
    if (messageString == nil)
        return 0.0f;
    
    UIView *sectionHeaderView = [self setupTableInfoViewWithMessage: messageString];
    if (sectionHeaderView == nil)
        return 0.0f;
    
    return sectionHeaderView.frame.size.height;
}

/** @brief Asks the delegate for a view object to display in the header of the specified section of the table view.
 *  @param tableView The table-view object asking for the view object.
 *  @param section An index number identifying a section of tableView. 
 *  @return A view object to be displayed in the header of section.
 */
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if ((section == 0) && (self.assignedDiscounts.count > 0))
        return nil;
    
    NSString *messageString = [self setupMessageForView];
    if (messageString == nil)
        return nil;
    
    return [self setupTableInfoViewWithMessage: messageString];
}

/** @brief Asks the data source to return the number of sections in the table view.
 *  @param tableView An object representing the table view requesting this information.
 *  @return The number of sections in tableView. The default value is 1.
 */
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

/** @brief Tells the data source to return the number of rows in a given section of a table view.
 *  @param tableView The table-view object requesting this information.
 *  @param section An index number identifying a section in tableView.
 *  @return The number of rows in section.
 */
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.assignedDiscounts.count;
}

/** @brief Asks the data source to verify that the given row is editable.
 *  @param tableView The table-view object requesting this information.
 *  @param indexPath An index path locating a row in tableView.
 *  @return YES if the row indicated by indexPath is editable; otherwise, NO.
 */
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.showBasketItemDiscounts) {
        return NO;
    }
    
    RESTObject *itemDiscount = [self.assignedDiscounts objectAtIndex: indexPath.row];
    NSString *entity = NSStringFromClass([itemDiscount class]);

    if ([entity isEqualToString:@"BasketRewardSalePromotionAmount"])
    {
        return NO;
    }
    else
    {
        return YES;
    }
}

/** @brief Asks the data source to commit the insertion or deletion of a specified row in the receiver.
 *  @param tableView The table-view object requesting the insertion or deletion.
 *  @param editingStle The cell editing style corresponding to a insertion or deletion requested for
 *  the row specified by indexPath. Possible editing styles are UITableViewCellEditingStyleInsert or
 *  UITableViewCellEditingStyleDelete.
 *  @param indexPath An index path locating the row in tableView.
 */
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{    
    // leave if we're not in the correct section
    
    if (editingStyle != UITableViewCellEditingStyleDelete)
        return;
    
    // query the specified basket 
    
    id discount = [self.assignedDiscounts objectAtIndex: indexPath.row];
    NSAssert(discount != nil, @"The basket items discount has a nil reference.");
    
    // remove the reward from the basket/ basket item
    
    if (self.showBasketItemDiscounts) {
        [[BasketController sharedInstance] removeDiscount: discount
                                           fromBasketItem: self.basketItem];
    }
    else if (self.showBasketDiscounts) {
        [[BasketController sharedInstance] removeDiscount: discount];
    }
}

/** @brief Asks the data source for a cell to insert in a particular location of the table view.
 *  @param tableView A table-view object requesting the cell.
 *  @param indexPath An index path locating a row in tableView.
 *  @return An object inheriting from UITableViewCellthat the table view can use for the specified row.
 */
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *resultCell = nil;
        
    // get a reference to the current item discount.
    
    id itemDiscount = [self.assignedDiscounts objectAtIndex: indexPath.row];
    
    // create a cell that displays the basket items data in the table view.
    
    static NSString *tableViewCellIdentifier = @"ItemDiscountTableViewCell";
    ItemDiscountTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier: tableViewCellIdentifier];
    if (cell == nil) {
        [tableView registerNib:[UINib nibWithNibName:@"ItemDiscountTableViewCell" bundle:nil]
        forCellReuseIdentifier: tableViewCellIdentifier];
        cell = [tableView dequeueReusableCellWithIdentifier: tableViewCellIdentifier];
    }
    
    cell.discount = itemDiscount;
    
    // skin the cell
    cell.tag = kTableCellSkinningTag;
    cell.discountDescription.tag = kTableCellTextSkinningTag;
    cell.discountValue.tag = kTableCellValueSkinningTag;
    
    // set as resulting cell.
    
    resultCell = cell;

    return resultCell;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    [[CRSSkinning currentSkin] applyCellSkin:cell
                                    forStyle:tableView.style];
}

/** @brief Handle discount changes
 *  @brief notification Reference to the notification object.
 */
- (void) handleBasketControllerDiscountChangeNotification:(NSNotification *)notification
{
    self.assignedDiscounts = nil;
    [self.tableView reloadData];
}

@end
