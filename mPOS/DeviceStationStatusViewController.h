//
//  DeviceStationStatusViewController.h
//  mPOS
//
//  Created by Antonio Strijdom on 23/10/2013.
//  Copyright (c) 2013 Clarity. All rights reserved.
//

#import <UIKit/UIKit.h>

@class DeviceStation;

typedef void(^DeviceStationAssociationCompleteBlockType)(DeviceStation *deviceStation);

@interface DeviceStationStatusViewController : UITableViewController

/** @property station
 *  @brief The device station to show.
 */
@property (nonatomic, strong) DeviceStation *station;

/** @brief Prompts the user to enter a till id and then assigns that till id to the device station specified.
 *  @param deviceStationId The device station to associate.
 *  @param complete The block to execute when the deviceStation is associated.
 */
- (void) associateStationWithId:(NSString *)deviceStationId
                       complete:(DeviceStationAssociationCompleteBlockType)complete;

@end
