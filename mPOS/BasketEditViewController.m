//
//  BasketEditViewController.m
//  mPOS
//
//  Created by Antonio Strijdom on 19/02/2014.
//  Copyright (c) 2014 Clarity. All rights reserved.
//

#import "BasketEditViewController.h"
#import "BasketController.h"
#import "UIAlertView+OCGBlockAdditions.h"
#import "BasketDiscountViewController.h"
#import "ServerErrorHandler.h"

@interface BasketEditViewController ()
@property (nonatomic, strong) NSMutableArray *lineErrors;
- (void) handleBasketChangeNotification:(NSNotification *)notification;
- (void) handleGenericBasketControllerErrorNotification:(NSNotification *)notification;
@end

@implementation BasketEditViewController

#pragma mark - Private

- (void) handleBasketChangeNotification:(NSNotification *)notification
{
    // reload the table
    [self reloadTable];
}

- (void) handleGenericBasketControllerErrorNotification:(NSNotification *)notification
{
    // dismiss any modal view
    if ([self.navigationController.visibleViewController isKindOfClass:[BasketDiscountViewController class]]) {
        [self dismissViewControllerAnimated:YES
                                 completion:nil];
    }
    // get the error
    ServerError *error = notification.userInfo[kBasketControllerGeneralErrorKey];
    // check for error messages that apply to the lines
    self.lineErrors = [NSMutableArray array];
    for (ServerErrorMessage *message in error.messages) {
        if (message.basketItemId.length > 0) {
            // only add unique errors
            BOOL add = YES;
            for (int i = 0; i < self.lineErrors.count; i++) {
                ServerErrorMessage *existingError = (ServerErrorMessage *)self.lineErrors[i];
                if ([message.basketItemId isEqualToString:existingError.basketItemId] &&
                    (message.codeValue == existingError.codeValue)) {
                    add = NO;
                    break;
                }
            }
            if (add) {
                [self.lineErrors addObject:message];
            }
            // we have errored, clear the selected lines
            [self.selectedBasketItems removeAllObjects];
        }
    }
    // only allow editing if there are no errors
    self.tableView.editing = (self.lineErrors.count == 0);
    [self.tableView reloadData];
}

#pragma mark - Properties

@synthesize errored;
- (BOOL) errored
{
    return ((self.lineErrors != nil) && (self.lineErrors.count > 0));
}

#pragma mark - UIViewController

- (void) viewDidLoad
{
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(handleBasketChangeNotification:)
                                                 name:kBasketControllerActiveBasketChangeNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(handleBasketChangeNotification:)
                                                 name:kBasketControllerBasketItemChangeNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(handleBasketChangeNotification:)
                                                 name:kBasketControllerDiscountChangeNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(handleGenericBasketControllerErrorNotification:)
                                                 name:kBasketControllerGeneralErrorNotification
                                               object:nil];
    self.lineErrors = [NSMutableArray array];
}

- (void) viewWillAppear:(BOOL)animated
{
    self.title = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁆󠁡󠁑󠁘󠁁󠁵󠁭󠁧󠁖󠁄󠀵󠁰󠀱󠁥󠁎󠁖󠁱󠁋󠁺󠁱󠁒󠁖󠀱󠁬󠁓󠁋󠁧󠁿*/ @"Edit Basket", nil);
    [super viewWillAppear:animated];
}

- (void) dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - UITableViewDatasource

- (void) tableView:(UITableView *)tableView
   willDisplayCell:(UITableViewCell *)cell
 forRowAtIndexPath:(NSIndexPath *)indexPath
{
    [super tableView:tableView
     willDisplayCell:cell
   forRowAtIndexPath:indexPath];
    
    // find the error for this line
    if ([UIDevice currentDevice].systemVersion.floatValue >= 7.0) {
        cell.tintColor = self.view.tintColor;
    }
    // get the basket item
    cell.accessoryType = UITableViewCellAccessoryNone;
    MPOSSaleItem *basketItem = [self.validBasketItems objectAtIndex:indexPath.row];
    for (ServerErrorMessage *message in self.lineErrors) {
        if ([basketItem.basketItemId isEqualToString:message.basketItemId]) {
            cell.accessoryType = UITableViewCellAccessoryDetailButton;
            if ([UIDevice currentDevice].systemVersion.floatValue >= 7.0) {
                cell.tintColor = [UIColor redColor];
            }
            break;
        }
    }
}

#pragma mark - UITableViewDelegate

- (void) tableView:(UITableView *)tableView accessoryButtonTappedForRowWithIndexPath:(NSIndexPath *)indexPath
{
    // get the basket item
    MPOSSaleItem *basketItem = [self.validBasketItems objectAtIndex:indexPath.row];
    ServerErrorMessage *message = nil;
    int i = 0;
    for (i = 0; i < self.lineErrors.count; i++) {
        if ([basketItem.basketItemId isEqualToString:((ServerErrorMessage *)self.lineErrors[i]).basketItemId]) {
            message = self.lineErrors[i];
            break;
        }
    }
    
    if (message != nil) {
        
        [self.lineErrors removeObjectAtIndex:i];
        NSString *localizedError = [[ServerErrorHandler sharedInstance] translateServerErrorMessage:message
                                                                                        withContext:nil];
        [[UIAlertView alertViewWithTitle:@""
                                 message:localizedError
                       cancelButtonTitle:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁄󠁴󠁍󠁉󠁣󠀫󠁺󠁔󠁗󠁦󠁹󠁡󠁯󠀷󠁨󠁔󠁩󠀸󠁯󠀫󠁨󠁘󠀹󠁱󠁌󠀶󠀸󠁿*/ @"OK", nil)
                       otherButtonTitles:nil
                               onDismiss:nil
                                onCancel:^{
                                    if (self.lineErrors.count == 0) {
                                        [self.tableView reloadRowsAtIndexPaths:@[indexPath]
                                                              withRowAnimation:UITableViewRowAnimationNone];
                                        self.tableView.editing = YES;
                                    } else {
                                        [self.tableView reloadRowsAtIndexPaths:@[indexPath]
                                                              withRowAnimation:UITableViewRowAnimationAutomatic];
                                    }
                                }] show];
    }
}

@end
