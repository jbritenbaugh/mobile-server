//
//  BasketListViewController.m
//  mPOS
//
//  Created by John Scott on 07/09/2012.
//  Copyright (c) 2012 Clarity. All rights reserved.
//

#import "BasketListViewController.h"
#import "CRSLocationController.h"
#import "CRSSkinning.h"
#import "BasketViewCell.h"
#import "ServerErrorHandler.h"
#import "FNKCollectionViewLayout.h"
#import "OCGContextCollectionViewDataSource.h"
#import "ContextViewController.h"
#import "BarcodeScannerController.h"

@interface BasketListViewController () {
    NSString *_receiptBarcode;
}
@property (strong, nonatomic) OCGContextCollectionViewDataSource *dataSource;
@property (strong, nonatomic) NSArray *baskets;
- (void)startScan;
@end

@implementation BasketListViewController

#pragma mark - Private

static NSString *CellIdentifier = @"BasketListViewControllerCell";

- (void)startScan
{
    //    return;
    BarcodeScannerController *controller = [BarcodeScannerController sharedInstance];
    // are we scanning?
    if (!controller.scanning) {
        // no, start scanning
        [controller beginScanningInViewController:self
                                        forReason:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁓󠁔󠀹󠀹󠀯󠁒󠁡󠁲󠁰󠁪󠀹󠁶󠁁󠁙󠁹󠀴󠁲󠀫󠁎󠁕󠁈󠀯󠁱󠁒󠁨󠁴󠁁󠁿*/ @"Receipt", nil)
                                   withButtonSize:CGSizeZero
                                     withAddTitle:nil
                                         mustScan:NO
                              alphaNumericAllowed:YES
                                        withBlock:^(NSString *barcode, ScannerEntryMethod entryMethod, SMBarcodeType barcodeType, BOOL *continueScanning) {
                                            // debug assertions
                                            NSAssert(barcode != nil,
                                                     @"The reference to the scanned barcode has not been initialized.");
                                            NSAssert([[barcode stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length] > 0,
                                                     @"The returned barcode does not contain any characters.");
                                            // we only want to scan the one barcode
                                            *continueScanning = NO;
                                            // grab the barcode we scanned
                                            _receiptBarcode =
                                            [barcode stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                                        }
                                       terminated:^{
                                           if (_receiptBarcode != nil) {
                                               // process the barcode
                                               if (self.delegate != nil) {
                                                   if ([self.delegate respondsToSelector:@selector(basketListViewController:recallBarcode:)]) {
                                                       [self.delegate basketListViewController:self
                                                                                 recallBarcode:_receiptBarcode];
                                                   }
                                               }
                                               _receiptBarcode = nil;
                                           }
                                       }
                                            error:^(NSString *errorDesc) {
                                                if (errorDesc) {
                                                    // display an alert
                                                    UIAlertView *alert =
                                                    [[UIAlertView alloc] initWithTitle:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁯󠀫󠁋󠁆󠁢󠁚󠁱󠁩󠁈󠁕󠀲󠀸󠁫󠁲󠁧󠁦󠀲󠁴󠁘󠁣󠁈󠁎󠀶󠀲󠁯󠀶󠁳󠁿*/ @"Scanning Error", nil)
                                                                               message:errorDesc
                                                                              delegate:nil
                                                                     cancelButtonTitle:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁄󠁴󠁍󠁉󠁣󠀫󠁺󠁔󠁗󠁦󠁹󠁡󠁯󠀷󠁨󠁔󠁩󠀸󠁯󠀫󠁨󠁘󠀹󠁱󠁌󠀶󠀸󠁿*/ @"OK", nil)
                                                                     otherButtonTitles:nil];
                                                    [alert show];
                                                }
                                            }];
    }
}

#pragma mark - Init

- (instancetype)init
{
    FNKCollectionViewLayout *layout = [[FNKCollectionViewLayout alloc] init];
    self = [super initWithCollectionViewLayout:layout];
    if (self) {
        _receiptBarcode = nil;
    }
    return self;
}

#pragma mark - UIViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.collectionView.delaysContentTouches = NO;
    self.collectionView.tag = kTableSkinningTag;
    self.dataSource = [[OCGContextCollectionViewDataSource alloc] init];
    self.collectionView.dataSource = self.dataSource;
    [self.dataSource registerCellReuseIdsWithCollectionView:self.collectionView];
    self.collectionView.backgroundColor = [UIColor colorWithRed:0.937 green:0.937 blue:0.957 alpha:1.000];
}

-(void)viewWillAppear:(BOOL)animated
{
    //
    
    self.title =  NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁎󠁑󠁩󠁔󠁢󠁥󠁢󠁒󠁥󠁐󠁑󠁧󠀸󠁔󠀰󠁎󠁥󠁥󠁷󠀴󠁲󠀷󠁤󠁮󠁺󠁫󠁍󠁿*/ @"Suspended", nil);
    
    //
    
    UIBarButtonItem *dismissButton = [[UIBarButtonItem alloc] initWithTitle: NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁡󠁩󠁡󠁍󠁰󠀸󠁷󠁣󠁹󠁩󠁕󠀷󠁪󠁶󠀱󠀸󠁐󠁖󠀵󠁥󠀳󠁫󠁕󠁚󠁄󠁶󠁙󠁿*/ @"Dismiss", nil)
                                                                      style: UIBarButtonItemStylePlain
                                                                     target: self
                                                                     action: @selector(dismissViewController:)];
    self.navigationItem.leftBarButtonItem = dismissButton;
    
    ocg_async_background_overlay(^{
        [RESTController.sharedInstance getAssociateSalesHistoryWithAssociateId:nil
                                                                          from:nil
                                                                    locationId:[CRSLocationController getLocationKey]
                                                                        status:@"SUSPENDED"
                                                                            to:nil
                                                                      complete:^(NSArray *baskets, NSError *error, ServerError *serverError)
         {
             ocg_sync_main(^{
                 self.baskets = baskets;
                 SoftKeyContext *layout =
                 [[BasketController sharedInstance] getLayoutForContextNamed:NSStringForSoftKeyContextType(SoftKeyContextTypeSuspendedBasket)];
                 self.dataSource = [OCGContextCollectionViewDataSource dataSourceForContext:layout
                                                                                   withData:self.baskets];
                 self.collectionView.dataSource = self.dataSource;
                 [self.collectionView reloadData];
                 
                 if (error != nil || serverError != nil)
                 {
                     ServerErrorMessage *message = [serverError.messages lastObject];
                     enum MShopperExceptionCode exceptionCode = [message.code integerValue];
                     
                     [[ServerErrorHandler sharedInstance] handleServerError:serverError
                                                             transportError:error
                                                                withContext:nil
                                                               dismissBlock:nil
                                                                repeatBlock:nil];
                 }
             });
         }];
    });
    [[CRSSkinning currentSkin] applyViewSkin:self];
}

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - Actions

/** @brief Called whenever the user wishes to dismiss the view controller without any change.
 *  @param sender A reference to the sender of this message.
 */
- (void) dismissViewController:(id)sender
{
    if (self.delegate != nil) {
        if ([self.delegate respondsToSelector:@selector(dismissBasketListViewController:)]) {
            [self.delegate dismissBasketListViewController: self];
        }
    }
}

#pragma mark - UICollectionViewDelegate

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    OCGContextCollectionViewDataSource *item = [self.dataSource dataSourceForIndexPath:indexPath];
    if (item != nil) {
        if ([item.softKey.keyType isEqualToString:@"POSFUNC"]) {
            if ([item.softKey.posFunc isEqualToString:@"CONTEXT_MENU_ITEM_SCAN"]) {
                [self startScan];
            }
        } else {
            if ((self.delegate != nil) && ([item.displayItem isKindOfClass:[BasketDTO class]])) {
                if ([self.delegate respondsToSelector:@selector(basketListViewController:recallBasket:)]) {
                    BasketDTO *basket = item.displayItem;
                    [self.delegate basketListViewController:self recallBasket:basket];
                }
            }
        }
    }
}

- (void)collectionView:(UICollectionView *)collectionView didHighlightItemAtIndexPath:(NSIndexPath *)indexPath
{
    // get the basket item from the current basket
    OCGContextCollectionViewDataSource *displayItem = [((OCGContextCollectionViewDataSource *)self.collectionView.dataSource) dataSourceForIndexPath:indexPath];
    // highlight all siblings
    for (OCGContextCollectionViewDataSource *sibling in displayItem.parentDataSource.childDataSources) {
        UICollectionViewCell *cell = [collectionView cellForItemAtIndexPath:sibling.indexPath];
        cell.highlighted = YES;
    }
}

- (void)collectionView:(UICollectionView *)collectionView didUnhighlightItemAtIndexPath:(NSIndexPath *)indexPath
{
    // get the basket item from the current basket
    OCGContextCollectionViewDataSource *displayItem = [((OCGContextCollectionViewDataSource *)self.collectionView.dataSource) dataSourceForIndexPath:indexPath];
    // highlight all siblings
    for (OCGContextCollectionViewDataSource *sibling in displayItem.parentDataSource.childDataSources) {
        UICollectionViewCell *cell = [collectionView cellForItemAtIndexPath:sibling.indexPath];
        cell.highlighted = NO;
    }
}

@end
