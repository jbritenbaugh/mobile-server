//
//  BarcodeInputView.h
//  mPOS
//
//  Created by John Scott on 13/10/2014.
//  Copyright (c) 2014 Clarity. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "BarcodeInput.h"

@interface CameraInputView : UIInputView
@property (nonatomic, assign) UIInterfaceOrientation orientation;
@end
