//
//  ModifierSelection.m
//  mPOS
//
//  Created by Antonio Strijdom on 12/09/2014.
//  Copyright (c) 2014 Omnico. All rights reserved.
//

#import "GroupSelection.h"

@implementation GroupSelection

- (ModifierSelection *)selectionForModifier:(ItemModifier *)modifier
{
    ModifierSelection *result = nil;
    
    for (ModifierSelection *selection in self.selections) {
        if ([selection.itemModifierId isEqualToString:modifier.itemModifierId] &&
            // don't consolidate free text items
            (selection.freeText.length == 0)) {
            result = selection;
            break;
        }
    }
    
    return result;
}

@end
