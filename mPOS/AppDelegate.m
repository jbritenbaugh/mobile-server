//
//  AppDelegate.m
//  mPOS
//
//  Created by Antonio Strijdom on 24/07/2012.
//  Copyright (c) 2012 Clarity. All rights reserved.
//

#import "AppDelegate.h"
#import "TestModeController.h"
#import "PAYwareController.h"
#import "OCGEFTManager.h"
#import "BasketController.h"
#import "OCGIntegrationController.h"
#import "OCGSplitViewController.h"
#import "StatusViewController.h"
#import "ContextViewController.h"
#import "CRSSkinning.h"
#import "POSFuncController.h"
#import "StatusNavigationBar.h"
#import "OCGReceiptManager.h"
#import "UIAlertView+OCGBlockAdditions.h"

#import "OCGDebugger.h"

NSString * const kAppDelegateSettingsPresentedNotification = @"kAppDelegateSettingsPresentedNotification";
NSString * const kAppDelegateSettingsDismissedNotification = @"kAppDelegateSettingsDismissedNotification";

@interface AppDelegate ( )
@property (nonatomic, strong) OCGSplitViewController *rootSplitVC;
@property (nonatomic, strong) StatusViewController *statusVC;
@property (nonatomic, strong) ContextViewController *contextViewController;
@end

@implementation AppDelegate

@synthesize window = _window;

- (BOOL) application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    OCGDebuggerDidFinishLaunchingWithOptions(launchOptions);
    
    // sign up for keyboard notifications
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(textFieldDidBeginEditing:)
                                                 name:UITextFieldTextDidBeginEditingNotification
                                               object:nil];
    
    // setup RestClient
    DebugLog(@"StartupController/ service base URL: %@", [TestModeController getServerAddress]);
    
    /*
     The next three lines are taken from http://stackoverflow.com/a/26040298/542244. Seems like this is a better way
     to get the window up and running. Sheesh.
     */
    self.window = [UIWindow new];
    [self.window makeKeyAndVisible];
    self.window.frame = [[UIScreen mainScreen] bounds];

        
    [self performSelector:@selector(startupWithOptions:) withObject:launchOptions afterDelay:0.01];
    return YES;
}

-(void)startupWithOptions:(NSDictionary *)launchOptions
{
    // start syncing

    [BasketController sharedInstance];
    [OCGEFTManager sharedInstance];
    [OCGSettingsController sharedInstance].delegate = [BasketController sharedInstance];
        
    // just make sure an instance of the integration controller is created
    [OCGIntegrationController sharedInstance];
    
    // start
    // create the skin from the plist file
    [CRSSkinning skinWithDictionary:SkinningDictionary()];
    // build nav controller
    UINavigationController *navigationController = [[UINavigationController alloc] init];
//    UINavigationController *navigationController = [[UINavigationController alloc] initWithNavigationBarClass:[StatusNavigationBar class]
//                                                                                                 toolbarClass:[UIToolbar class]];
    // create the context view controller
    self.contextViewController = [[ContextViewController alloc] initWithMainViewController:navigationController];
    self.contextViewController.contextNavigationController = navigationController;
    // setup the pos function controller
    [POSFuncController sharedInstance].splitViewController = self.contextViewController;
    // create the status bar
    self.rootSplitVC = [[OCGSplitViewController alloc] init];
    self.rootSplitVC.displayMode = OCGSplitViewSideDisplayModeTop;
    self.rootSplitVC.mainViewController = self.contextViewController;
    self.statusVC = [[StatusViewController alloc] init];
    self.statusVC.view.tag = self.statusVC.view.tag;
    [self.rootSplitVC setSideViewControllers:@[self.statusVC]];
    
    // show
    if ([UIDevice currentDevice].systemVersion.floatValue >= 7.0) {
        self.window.rootViewController = self.rootSplitVC;
    } else {
        self.window.rootViewController = self.contextViewController;
    }
}

// TFS74724 - ensure the keyboard always appears, even if a hardware
// keyboard is attached.
// from http://stackoverflow.com/questions/3326189/
- (void) textFieldDidBeginEditing:(NSNotification *)note
{
    UITextField *textField = [note object];
    
    if (textField.inputAccessoryView == nil) {
        textField.inputAccessoryView =
        [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, self.window.frame.size.width, 1.0f)];
    }

    dispatch_async(dispatch_get_main_queue(), ^{
        CGRect frame = textField.inputAccessoryView.superview.frame;
        frame.origin.y = [UIScreen mainScreen].bounds.size.height - frame.size.height;
        textField.inputAccessoryView.superview.frame = frame;
    });
}

- (BOOL) application:(UIApplication *)application
             openURL:(NSURL *)url
   sourceApplication:(NSString *)sourceApplication
          annotation:(id)annotation
{
    BOOL result = NO;
    
    // check if this URL is from the configured payment provider
    result = [[OCGEFTManager sharedInstance] application:application
                                                 openURL:url
                                       sourceApplication:sourceApplication
                                              annotation:annotation];
    if (!result) {
        result = [[PAYwareController sharedInstance] application:application
                                                         openURL:url
                                               sourceApplication:sourceApplication
                                                      annotation:annotation];
        if (!result) {
            // url couldn't be handled by payware, see if it is an integration URL
            result =
            [[OCGIntegrationController sharedInstance] application:application
                                                           openURL:url
                                                 sourceApplication:sourceApplication
                                                        annotation:annotation];
        }
    }
    
    return result;
}

-  (void) applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void) applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    [UIAlertView OCGAdditions_cancelAnimated:NO];
}

- (void) applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void) applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    [[BasketController sharedInstance] getActiveBasket];
}

- (void)applicationWillTerminate:(UIApplication *)application
{
}

- (NSUInteger)application:(UIApplication *)application supportedInterfaceOrientationsForWindow:(UIWindow *)window
{
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
    {
        return UIInterfaceOrientationMaskPortrait;
    }
    else
    {
        return UIInterfaceOrientationMaskAll;
    }
}


#pragma mark Methods

/** @brief Called to present the settings view modally.
 */
- (void) presentSettingsViewAnimated:(BOOL)animated viewController:(UIViewController*)viewController
{
    SettingsTableViewController *settingsViewController = [[SettingsTableViewController alloc] init];

    settingsViewController.delegate = self;
    // TFS67626 - Only reset adminEnabled when the view is being presented initially
    [settingsViewController resetAdmin];

    UINavigationController *settingsNavigationController = [[UINavigationController alloc] initWithRootViewController: settingsViewController];
    settingsNavigationController.modalPresentationStyle = UIModalPresentationFormSheet;
    
    [viewController presentViewController:settingsNavigationController
                                                 animated:animated
                                               completion:^{
                                                   [[NSNotificationCenter defaultCenter] postNotificationName:kAppDelegateSettingsPresentedNotification
                                                                                                       object:settingsViewController];
                                               }];
}

#pragma mark SettingsTableViewControllerDelegate

/** @brief Called whenever the settings table view controller should be dismissed.
 *  @param controller A reference to the settings table view controller.
 */
- (void) dismissSettingsTableViewController:(SettingsTableViewController*)controller
{
    [controller dismissViewControllerAnimated:YES
                                                       completion:^{
                                                           [[NSNotificationCenter defaultCenter] postNotificationName:kAppDelegateSettingsDismissedNotification
                                                       object:controller];
                                                       }];
}

@end
