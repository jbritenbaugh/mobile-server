//
//  OCGEFTProviderPayware.h
//  mPOS
//
//  Created by Antonio Strijdom on 15/04/2013.
//  Copyright (c) 2013 Clarity. All rights reserved.
//

#import "OCGURLEFTProvider.h"

/** @file OCGEFTProviderPayware.h */

/** @brief OCGEFTProvider for talking to the Payware EFT application.
 */
@interface OCGEFTProviderPayware : OCGURLEFTProvider

@end
