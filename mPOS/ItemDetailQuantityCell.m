//
//  ItemDetailQuantityCell.m
//  mPOS
//
//  Created by Antonio Strijdom on 23/05/2013.
//  Copyright (c) 2013 Clarity. All rights reserved.
//

#import "ItemDetailQuantityCell.h"

@interface ItemDetailQuantityCell () {
    UIColor *quantityDescColor;
    UIColor *quantityColor;
}
@end

@implementation ItemDetailQuantityCell

#pragma mark - UITableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:NO];

    // Configure the view for the selected state
    if (selected) {
        if (!self.bodyView.highlighted) {
            quantityDescColor = self.quantityDescLabel.textColor;
            quantityColor = self.quantityLabel.textColor;
            [UIView animateWithDuration:0.5
                             animations:^{
                                 self.quantityDescLabel.textColor = [UIColor whiteColor];
                                 self.quantityLabel.textColor = [UIColor whiteColor];
                                 self.bodyView.highlighted = YES;
                             }];
        }
    } else {
        if (self.bodyView.highlighted) {
            [UIView animateWithDuration:0.5
                             animations:^{
                                 self.quantityDescLabel.textColor = quantityDescColor;
                                 self.quantityLabel.textColor = quantityColor;
                                 self.bodyView.highlighted = NO;
                             }];
        }
    }
}

@end
