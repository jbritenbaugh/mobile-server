//
//  ContextController.h
//  mPOS
//
//  Created by Antonio Strijdom on 15/01/2014.
//  Copyright (c) 2014 Clarity. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "OCGSplitViewController.h"
#import "SoftKeyContextType.h"
#import "POSFunc.h"
#import "BarcodeScannerController.h"

/** @file ContextController.h */

#pragma mark Notifications

/** @enum MPOSContext
 *  @brief Enumerates the valid contexts for mPOS to be in.
 */
NS_ENUM(NSInteger, MPOSContext) {
    /** @brief mPOS is in an unknown state. */
    MPOSContextUnknown = NSIntegerMin,
    /** @brief mPOS is currently not in a sale. */
    MPOSContextNoActiveSale = 0,
    /** @brief mPOS is current in a sale, but it is empty. */
    MPOSContextEmptySale = 1,
    /** @brief mPOS is currently in a sale. */
    MPOSContextSale = 2,
    /** @brief The current sale has been checked out. */
    MPOSContextCheckedOut = 3,
    /** @brief The current sale is printing. */
    MPOSContextSalePrinting = 4,
    /** @brief The current sale is complete. */
    MPOSContextSaleComplete = 5
};

/** @enum MPOSUIState
 *  @brief Enumerates the valid UI states for mPOS to be in.
 */
NS_ENUM(NSInteger, MPOSUIState) {
    /** @brief mPOS is in an unknown state. */
    MPOSUIStateUnknown = NSIntegerMin,
    /** @brief mPOS is showing the basket. */
    MPOSUIStateBasket = 0,
    /** @brief mPOS is showing the sales menu. */
    MPOSUIStateMenu = 1,
    /** @brief mPOS is showing the search view. */
    MPOSUIStateSearch = 2,
    /** @brief mPOS is showing the more menu. */
    MPOSUIStateMore = 3,
    /** @brief mPOS is scanning for barcodes. */
    MPOSUIStateScanning = 4,
    /** @brief mPOS is showing an sale item detail. */
    MPOSUIStateSaleItemDetail = 5,
    /** @brief mPOS is showing the edit basket view. */
    MPOSUIStateEditBasket = 6
};

/** @brief Class for agregatting and broadcasting mPOS state. */
@interface ContextViewController : OCGSplitViewController 

/** @property contextNavigationController
 *  @brief The navigation controller hosting all basket view controllers.
 *  @note This property must be set in order for the context updates to be reflected in the UI.
 */
@property (nonatomic, strong) UINavigationController *contextNavigationController;

/** @property currentContext
 *  @brief The current context of mPOS.
 */
@property (nonatomic, readonly) enum MPOSContext currentContext;

/** @property currentUIState
 *  @brief The current UI state of mPOS, based on the view controller currently being presented.
 */
@property (nonatomic, readonly) enum MPOSUIState currentUIState;

/** @brief Returns the singleton instance of the mPOS context controller.
 */
+ (ContextViewController *) sharedInstance;

/** @brief Updates the context ui.
 *  @param contextChanged Whether the context has changed. If it has, the context menu is updated.
 */
- (void) updateUIContextChanged:(BOOL)contextChanged;

/** @brief Handles menu item selections.
 *  @param selectedMenuItem The menu item selected.
 */
- (void) didSelectContextMenuItem:(SoftKey *)selectedMenuItem;

/** @brief Reset, then check the common requirements for all views.
 *  @return YES, if all shared requirements are fullfilled, NO otherwise.
 */
- (BOOL) resetSharedRequirements;

/** @brief Check the common requirements for all views.
 *  @return YES, if all shared requirements are fullfilled, NO otherwise.
 */
- (BOOL) checkSharedRequirements;

/** @brief Presents the login view.
 *  @param animated Whether or not to animate the presentation transition.
 */
- (void) presentLoginViewAnimated:(BOOL)animated;

/** @brief Starts the barcode capture process.
 *  @param barcodeUseType What the client is going to do with the barcode.
 *  @see BarcodeUseType
 */
- (void) showBarcodeCaptureWithBarcodeUseType:(BarcodeUseType)barcodeUseType;

/** @brief Shows the item detail for the specified item.
 *  @param basketItem The item to display details for.
 */
- (void) showDetailsForItem:(MPOSBasketItem *)basketItem;

/** @brief Gets the layout menu for the current context
 *  @return The soft key menu for the current context.
 */
- (SoftKeyContext *) contextLayoutMenuForCurrentContext;

/** @brief Gets the layout menu for the specified basket item type.
 *  @param item The item to get the layout for.
 *  @return The layout for the basket item type specified or nil.
 */
- (SoftKeyContext *) contextLayoutMenuForBasketItem:(MPOSBasketItem *)item;

/** @brief Checks the current basket for items with mandatory modifiers.
 */
- (void) mandatoryModifierCheck;

/** Presents optional modifiers for the item specified.
 *  @param basketItem The item to display the optional modifiers for.
 */
- (void) presentOptionalModifiersForItem:(MPOSBasketItem *)basketItem;

@end
