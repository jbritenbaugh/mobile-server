//
//  ReturnOptionsTableViewController.m
//  mPOS
//
//  Created by Antonio Strijdom on 01/11/2013.
//  Copyright (c) 2013 Clarity. All rights reserved.
//

#import "ReturnOptionsTableViewController.h"
#import "CRSSkinning.h"
#import "BarcodeScannerController.h"
#import "OCGTableViewCell.h"
#import "OCGTextField.h"
#import "BasketController.h"
#import "OCGSelectViewController.h"
#import "NSArray+OCGIndexPath.h"
#import "ReturnPriceTableViewController.h"
#import "BasketItemTableViewCell.h"
#import "UIAlertView+OCGBlockAdditions.h"
#import "OCGNumberTextFieldValidator.h"
#import "OCGKeyboardTypeButtonItem.h"

@interface ReturnOptionsTableViewController () <UITextFieldDelegate, ReturnPriceTableViewControllerDelegate>
@property (nonatomic, strong) NSArray *layout;
@property (nonatomic, strong) OCGTableViewCell *originalStoreNumberCell;
@property (nonatomic, strong) OCGTableViewCell *originalTillCell;
@property (nonatomic, strong) OCGTableViewCell *originalTransactionIdCell;
@property (nonatomic, strong) OCGTableViewCell *originalTransactionDateCell;
@property (nonatomic, strong) OCGNumberTextFieldValidator *validator;
@property (nonatomic, strong) UIToolbar *inputView;
@property (nonatomic, strong) UISegmentedControl *navControl;
@property (nonatomic, weak) UITextField *currentTextField;
@property (nonatomic, strong) DiscountReason *reason;
@property (nonatomic, strong) NSString *originalPrice;
@property (nonatomic, strong) NSString *price;
@property (nonatomic, strong) NSString *originalBarcode;
@property (nonatomic, strong) NSDate *originalDate;
@property (nonatomic, readonly) NSString *formattedOriginalDate;
@property (nonatomic, readonly) BOOL hasBarcode;
@property (nonatomic, readonly) BOOL hasTxInfo;
@property (nonatomic, assign) BOOL errored;
- (void) startScan;
- (BOOL) validateEntries;
- (void) performReturn;
- (void) handleBasketItemChangeNotification:(NSNotification *)note;
- (void) handleBasketAddItemErrorNotification:(NSNotification *)note;
- (void) cancelTapped:(id)sender;
- (void) returnTapped:(id)sender;
- (void) navControlChanged:(id)sender;
- (void) doneTapped:(id)sender;
- (void) datePickerValueChanged:(id)sender;
@end

@implementation ReturnOptionsTableViewController

#pragma mark - Private

static NSString *kBasketItemTableViewCell = @"BasketItemTableViewCell";
static NSString *kValueCell = @"ValueCell";
const NSInteger kSectionIndexItem = 0;
const NSInteger kSectionIndexOriginalTx = 1;
// item section
const NSInteger kRowIndexItemInfo = 0;
const NSInteger kRowIndexReason = 1;
const NSInteger kRowIndexPrice = 2;
// original tx section
const NSInteger kRowIndexBarcode = 3;
const NSInteger kRowIndexStoreId = 4;
const NSInteger kRowIndexTillId = 5;
const NSInteger kRowIndexTxId = 6;
const NSInteger kRowIndexDate = 7;

- (void) startScan
{
    BarcodeScannerController *controller = [BarcodeScannerController sharedInstance];
    // are we scanning?

    if (!controller.scanning) {
        // no, start scanning
        [controller beginScanningInViewController:self
                                        forReason:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁓󠁔󠀹󠀹󠀯󠁒󠁡󠁲󠁰󠁪󠀹󠁶󠁁󠁙󠁹󠀴󠁲󠀫󠁎󠁕󠁈󠀯󠁱󠁒󠁨󠁴󠁁󠁿*/ @"Receipt", nil)
                                   withButtonSize:CGSizeZero
                                     withAddTitle:nil
                                         mustScan:NO
                              alphaNumericAllowed:YES
                                        withBlock:^(NSString *barcode, ScannerEntryMethod entryMethod, SMBarcodeType barcodeType, BOOL *continueScanning) {
                                            // debug assertions
                                            NSAssert(barcode != nil,
                                                     @"The reference to the scanned barcode has not been initialized.");
                                            NSAssert([[barcode stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length] > 0,
                                                     @"The returned barcode does not contain any characters.");
                                            // we only want to scan the one barcode
                                            *continueScanning = NO;
                                            // grab the barcode we scanned
                                            self.originalBarcode =
                                            [barcode stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                                        }
                                       terminated:^{
                                           [self.tableView reloadData];
                                       }
                                            error:^(NSString *errorDesc) {
                                                if (errorDesc) {
                                                    // display an alert
                                                    UIAlertView *alert =
                                                    [[UIAlertView alloc] initWithTitle:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁯󠀫󠁋󠁆󠁢󠁚󠁱󠁩󠁈󠁕󠀲󠀸󠁫󠁲󠁧󠁦󠀲󠁴󠁘󠁣󠁈󠁎󠀶󠀲󠁯󠀶󠁳󠁿*/ @"Scanning Error", nil)
                                                                               message:errorDesc
                                                                              delegate:nil
                                                                     cancelButtonTitle:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁄󠁴󠁍󠁉󠁣󠀫󠁺󠁔󠁗󠁦󠁹󠁡󠁯󠀷󠁨󠁔󠁩󠀸󠁯󠀫󠁨󠁘󠀹󠁱󠁌󠀶󠀸󠁿*/ @"OK", nil)
                                                                     otherButtonTitles:nil];
                                                    [alert show];
                                                }
                                            }];
    }
}

- (BOOL) validateEntries
{
    BOOL result = YES;
    
    UIAlertView *validationAlert = nil;
    // make sure we have an item
    if ((self.item == nil) || ([self.item.itemRefId isEqualToString:@""])) {
        validationAlert =
        [[UIAlertView alloc] initWithTitle:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁫󠀵󠁉󠁤󠁘󠁖󠁏󠁁󠁆󠀷󠁬󠀯󠁯󠁇󠀲󠀳󠁵󠁢󠀫󠁘󠁦󠀯󠁩󠁁󠁦󠁒󠁑󠁿*/ @"Error", nil)
                                   message:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁦󠁲󠁇󠀳󠀱󠁈󠁈󠀷󠁌󠁏󠁈󠁁󠁷󠁎󠁡󠁱󠀫󠁶󠁷󠀶󠁁󠁁󠀵󠁩󠀳󠁖󠁯󠁿*/ @"Invalid item.", nil)
                                  delegate:nil
                         cancelButtonTitle:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁄󠁴󠁍󠁉󠁣󠀫󠁺󠁔󠁗󠁦󠁹󠁡󠁯󠀷󠁨󠁔󠁩󠀸󠁯󠀫󠁨󠁘󠀹󠁱󠁌󠀶󠀸󠁿*/ @"OK", nil)
                         otherButtonTitles:nil];
        result = NO;
    }
    // make sure we have a reason
    if (result && (self.reason == nil)) {
        validationAlert =
        [[UIAlertView alloc] initWithTitle:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁫󠀵󠁉󠁤󠁘󠁖󠁏󠁁󠁆󠀷󠁬󠀯󠁯󠁇󠀲󠀳󠁵󠁢󠀫󠁘󠁦󠀯󠁩󠁁󠁦󠁒󠁑󠁿*/ @"Error", nil)
                                   message:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁮󠁱󠀶󠁌󠁙󠁭󠁨󠁉󠀯󠀹󠁉󠀹󠁢󠁪󠁮󠁲󠁦󠁂󠁑󠁍󠁔󠁨󠁄󠁰󠁷󠁆󠁁󠁿*/ @"You must select a reason.", nil)
                                  delegate:[UIAlertView class]
                         cancelButtonTitle:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁄󠁴󠁍󠁉󠁣󠀫󠁺󠁔󠁗󠁦󠁹󠁡󠁯󠀷󠁨󠁔󠁩󠀸󠁯󠀫󠁨󠁘󠀹󠁱󠁌󠀶󠀸󠁿*/ @"OK", nil)
                         otherButtonTitles:nil];
        validationAlert.cancelBlock = ^() {
            [self tableView:self.tableView didSelectRowAtIndexPath:[self.layout indexPathsForObject:@(kRowIndexReason)][0]];
        };
        result = NO;
    }
    // if the user is entering tx info manually
    if (self.hasTxInfo) {
        if ((self.originalStoreNumberCell.detailTextField.text == nil) ||
            ([self.originalStoreNumberCell.detailTextField.text isEqualToString:@""])) {
            validationAlert =
            [[UIAlertView alloc] initWithTitle:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁫󠀵󠁉󠁤󠁘󠁖󠁏󠁁󠁆󠀷󠁬󠀯󠁯󠁇󠀲󠀳󠁵󠁢󠀫󠁘󠁦󠀯󠁩󠁁󠁦󠁒󠁑󠁿*/ @"Error", nil)
                                       message:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁎󠁰󠀴󠁰󠀷󠀱󠁴󠁰󠁣󠁦󠁧󠁵󠁮󠁣󠁣󠁏󠁗󠀯󠁯󠁢󠁓󠁫󠁊󠁹󠁚󠁒󠁯󠁿*/ @"You must specify the store number of the original transaction.", nil)
                                      delegate:[UIAlertView class]
                             cancelButtonTitle:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁄󠁴󠁍󠁉󠁣󠀫󠁺󠁔󠁗󠁦󠁹󠁡󠁯󠀷󠁨󠁔󠁩󠀸󠁯󠀫󠁨󠁘󠀹󠁱󠁌󠀶󠀸󠁿*/ @"OK", nil)
                             otherButtonTitles:nil];
            validationAlert.cancelBlock = ^() {
                [self tableView:self.tableView didSelectRowAtIndexPath:[self.layout indexPathsForObject:@(kRowIndexStoreId)][0]];
            };
            result = NO;
        }
        if ((self.originalTillCell.detailTextField.text == nil) ||
            ([self.originalTillCell.detailTextField.text isEqualToString:@""])) {
            validationAlert =
            [[UIAlertView alloc] initWithTitle:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁫󠀵󠁉󠁤󠁘󠁖󠁏󠁁󠁆󠀷󠁬󠀯󠁯󠁇󠀲󠀳󠁵󠁢󠀫󠁘󠁦󠀯󠁩󠁁󠁦󠁒󠁑󠁿*/ @"Error", nil)
                                       message:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁙󠁵󠀴󠀫󠁂󠁥󠁧󠁋󠀴󠀯󠁋󠁵󠀯󠁬󠁕󠁊󠁑󠁕󠁑󠁘󠀵󠁑󠁖󠁤󠁆󠁄󠁫󠁿*/ @"You must specify the till id or operator id of the original transaction.", nil)
                                      delegate:[UIAlertView class]
                             cancelButtonTitle:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁄󠁴󠁍󠁉󠁣󠀫󠁺󠁔󠁗󠁦󠁹󠁡󠁯󠀷󠁨󠁔󠁩󠀸󠁯󠀫󠁨󠁘󠀹󠁱󠁌󠀶󠀸󠁿*/ @"OK", nil)
                             otherButtonTitles:nil];
            validationAlert.cancelBlock = ^() {
                [self tableView:self.tableView didSelectRowAtIndexPath:[self.layout indexPathsForObject:@(kRowIndexTillId)][0]];
            };
            result = NO;
        }
        if ((self.originalTransactionIdCell.detailTextField.text == nil) ||
            ([self.originalTransactionIdCell.detailTextField.text isEqualToString:@""])) {
            validationAlert =
            [[UIAlertView alloc] initWithTitle:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁫󠀵󠁉󠁤󠁘󠁖󠁏󠁁󠁆󠀷󠁬󠀯󠁯󠁇󠀲󠀳󠁵󠁢󠀫󠁘󠁦󠀯󠁩󠁁󠁦󠁒󠁑󠁿*/ @"Error", nil)
                                       message:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠀹󠁃󠁢󠁔󠁂󠁮󠁆󠁯󠁮󠁅󠁘󠁢󠀸󠁡󠁲󠀸󠁰󠀵󠁘󠁸󠁂󠁐󠀸󠁚󠁘󠁧󠁫󠁿*/ @"You must specify the id of the original transaction.", nil)
                                      delegate:[UIAlertView class]
                             cancelButtonTitle:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁄󠁴󠁍󠁉󠁣󠀫󠁺󠁔󠁗󠁦󠁹󠁡󠁯󠀷󠁨󠁔󠁩󠀸󠁯󠀫󠁨󠁘󠀹󠁱󠁌󠀶󠀸󠁿*/ @"OK", nil)
                             otherButtonTitles:nil];
            validationAlert.cancelBlock = ^() {
                [self tableView:self.tableView didSelectRowAtIndexPath:[self.layout indexPathsForObject:@(kRowIndexTxId)][0]];
            };
            result = NO;
        }
        if (self.originalDate == nil) {
            validationAlert =
            [[UIAlertView alloc] initWithTitle:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁫󠀵󠁉󠁤󠁘󠁖󠁏󠁁󠁆󠀷󠁬󠀯󠁯󠁇󠀲󠀳󠁵󠁢󠀫󠁘󠁦󠀯󠁩󠁁󠁦󠁒󠁑󠁿*/ @"Error", nil)
                                       message:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁩󠁺󠁳󠁚󠁓󠁱󠁒󠁩󠁢󠁲󠁍󠁲󠁳󠀸󠁶󠁃󠁭󠁓󠁘󠁳󠁎󠀳󠁰󠁒󠁁󠁚󠁍󠁿*/ @"You must specify the date of the original transaction.", nil)
                                      delegate:[UIAlertView class]
                             cancelButtonTitle:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁄󠁴󠁍󠁉󠁣󠀫󠁺󠁔󠁗󠁦󠁹󠁡󠁯󠀷󠁨󠁔󠁩󠀸󠁯󠀫󠁨󠁘󠀹󠁱󠁌󠀶󠀸󠁿*/ @"OK", nil)
                             otherButtonTitles:nil];
            validationAlert.cancelBlock = ^() {
                [self tableView:self.tableView didSelectRowAtIndexPath:[self.layout indexPathsForObject:@(kRowIndexDate)][0]];
            };
            result = NO;
        }
    }
    
    // show the alert
    if (validationAlert != nil) {
        [validationAlert show];
    }
    
    return result;
}

- (void) performReturn
{
    // parse parameters
    NSNumber *price = nil;
    // if there is a custom price
    if ((self.price != nil) && (![self.price isEqualToString:@""])) {
        // use that
        price = @(self.price.floatValue);
    }
    // if price is nil at this point, that's ok, the server will just use the original
    // price anyway
    NSNumber *storeId = nil;
    NSNumber *tillId = nil;
    NSNumber *txId = nil;
    NSDate *txDate = nil;
    if ((!self.hasBarcode) && (self.hasTxInfo)) {
        if ((self.originalStoreNumberCell.detailTextField.text != nil) &&
            (![self.originalStoreNumberCell.detailTextField.text isEqualToString:@""])) {
            storeId = @(self.originalStoreNumberCell.detailTextField.text.integerValue);
        }
        if ((self.originalTillCell.detailTextField.text != nil) &&
            (![self.originalTillCell.detailTextField.text isEqualToString:@""])) {
            tillId = @(self.originalTillCell.detailTextField.text.integerValue);
        }
        if ((self.originalTransactionIdCell.detailTextField.text != nil) &&
            (![self.originalTransactionIdCell.detailTextField.text isEqualToString:@""])) {
            txId = @(self.originalTransactionIdCell.detailTextField.text.integerValue);
        }
        txDate = self.originalDate;
    }
    // kick off the return
    [[BasketController sharedInstance] returnItemWithScanId:self.item.itemRefId
                                                   andPrice:price
                                                  forReason:self.reason
                                             originalScanId:self.originalScanId
                                    originalItemDescription:self.originalDescription
                                 originalTransactionBarcode:self.originalBarcode
                                 originalTransactionStoreId:storeId
                                    originalTransactionTill:tillId
                                      originalTransactionId:txId
                                    originalTransactionDate:txDate];
}

- (void) handleBasketItemChangeNotification:(NSNotification *)note
{
    // dismiss the view only if we are no longer adding an item and there wasn't an error
    if ((!self.errored) && (![BasketController sharedInstance].isAddingBasketItem)) {
        [self dismissViewControllerAnimated:YES
                                 completion:nil];
    }
    self.errored = NO;
}

- (void) handleBasketAddItemErrorNotification:(NSNotification *)note
{
    // get the error
    ServerError *error = note.userInfo[kBasketControllerBasketItemErrorKey];
    
    NSAssert(error != nil, @"The reference to the error object has not been specified.");
    NSAssert([error isKindOfClass:[ServerError class]], @"The specified error object is inherited from an unexpected BasketController.sharedInstance class.");
    
    // check for return error messages
    ServerErrorMessage *lastReturnErrorMessage = nil;
    for (ServerErrorMessage *message in error.messages) {
        if ((message.codeValue == MShopperBarcodeItemCouldNotBeReturnedExceptionCode) ||
            (message.codeValue == MShopperBarcodeNotRecognizedExceptionCode)) {
            lastReturnErrorMessage = message;
            self.errored = YES;
            break;
        }
    }
    
    if (lastReturnErrorMessage != nil) {
        switch (lastReturnErrorMessage.codeValue) {
            case MShopperBarcodeItemCouldNotBeReturnedExceptionCode: {
                // process the error
                if ([lastReturnErrorMessage isKindOfClass:[ServerErrorBarcodeItemNotReturnedMessage class]]) {
                    ServerErrorBarcodeItemNotReturnedMessage *message = (ServerErrorBarcodeItemNotReturnedMessage *)lastReturnErrorMessage;
                    self.originalPrice = message.originalItemPrice;
                    self.originalBarcode = nil;
                    if (message.originalTransactionStoreId != nil) {
                        self.originalStoreNumberCell.detailTextField.text =
                        [NSString stringWithFormat:@"%i", message.originalTransactionStoreIdValue];
                    }
                    if (message.originalTransactionTill != nil) {
                        self.originalTillCell.detailTextField.text =
                        [NSString stringWithFormat:@"%i", message.originalTransactionTillValue];
                    }
                    if (message.originalTransactionId != nil) {
                        self.originalTransactionIdCell.detailTextField.text =
                        [NSString stringWithFormat:@"%i", message.originalTransactionIdValue];
                    }
                    if (message.originalTransactionStoreId != nil) {
                        self.originalStoreNumberCell.detailTextField.text =
                        [NSString stringWithFormat:@"%i", message.originalTransactionStoreIdValue];
                    }
                    if (message.originalTransactionDate != nil) {
                        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
                        formatter.dateFormat = @"yyyy-MM-dd";
                        self.originalDate = [formatter dateFromString:message.originalTransactionDate];
                    }
                }
                UIAlertView *alert =
                [[UIAlertView alloc] initWithTitle:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁫󠀵󠁉󠁤󠁘󠁖󠁏󠁁󠁆󠀷󠁬󠀯󠁯󠁇󠀲󠀳󠁵󠁢󠀫󠁘󠁦󠀯󠁩󠁁󠁦󠁒󠁑󠁿*/ @"Error", nil)
                                           message:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁅󠁳󠁨󠁧󠁰󠁡󠀶󠁶󠁶󠁁󠁂󠁅󠁆󠁧󠁩󠁉󠀰󠁏󠁃󠁫󠁷󠁪󠁉󠀹󠁗󠁊󠁍󠁿*/ @"The item could not be returned.", nil)
                                          delegate:[UIAlertView class]
                                 cancelButtonTitle:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁄󠁴󠁍󠁉󠁣󠀫󠁺󠁔󠁗󠁦󠁹󠁡󠁯󠀷󠁨󠁔󠁩󠀸󠁯󠀫󠁨󠁘󠀹󠁱󠁌󠀶󠀸󠁿*/ @"OK", nil)
                                 otherButtonTitles:nil];
                [alert show];
                alert.cancelBlock = ^() {
                    [self.tableView reloadData];
                };
                break;
            }
            case MShopperBarcodeNotRecognizedExceptionCode : {
                UIAlertView *alert =
                [[UIAlertView alloc] initWithTitle:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁫󠀵󠁉󠁤󠁘󠁖󠁏󠁁󠁆󠀷󠁬󠀯󠁯󠁇󠀲󠀳󠁵󠁢󠀫󠁘󠁦󠀯󠁩󠁁󠁦󠁒󠁑󠁿*/ @"Error", nil)
                                           message:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁴󠁫󠁕󠁃󠁬󠁲󠁋󠁖󠁍󠀶󠁖󠁢󠀶󠁚󠁐󠁏󠁘󠀰󠁍󠁌󠁰󠁒󠁫󠀸󠁃󠁐󠁯󠁿*/ @"The receipt barcode provided could not be found.", nil)
                                          delegate:[UIAlertView class]
                                 cancelButtonTitle:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁄󠁴󠁍󠁉󠁣󠀫󠁺󠁔󠁗󠁦󠁹󠁡󠁯󠀷󠁨󠁔󠁩󠀸󠁯󠀫󠁨󠁘󠀹󠁱󠁌󠀶󠀸󠁿*/ @"OK", nil)
                                 otherButtonTitles:nil];
                [alert show];
                alert.cancelBlock = ^() {
                    self.originalBarcode = nil;
                    [self.tableView reloadData];
                };
                break;
            }
            default:
                break;
        }
        // tidy up errors
        if (lastReturnErrorMessage != nil) {
            [error removeMessage:lastReturnErrorMessage];
            lastReturnErrorMessage = nil;
        }
        // remove the item
        if (error.createdBasketItem != nil) {
            [BasketController.sharedInstance.basket removeBasketItem:error.createdBasketItem];
        }
        [BasketController.sharedInstance kick];
    } else {
        // some other error occurred, just dismiss the view
        [self dismissViewControllerAnimated:YES
                                 completion:nil];
    }
}

#pragma mark - Properties

@synthesize item = _item;
- (void) setItem:(MPOSItem *)item
{
    _item = item;
    if (_item != nil) {
        // set no default price (ie, use current)
        self.price = nil;
        // set default reasons
        NSArray *reasons = [[BasketController sharedInstance] returnReasons];
        if (reasons.count > 0) {
            self.reason = reasons[0];
        }
    }
}
@synthesize reason = _reason;
- (void) setReason:(DiscountReason *)reason
{
    _reason = reason;
    // reload the cell
    [self.tableView reloadRowsAtIndexPaths:[self.layout indexPathsForObject:@(kRowIndexReason)]
                          withRowAnimation:UITableViewRowAnimationNone];
}
@synthesize price = _price;
- (void) setPrice:(NSString *)price
{
    _price = price;
    // reload the cell
    [self.tableView reloadRowsAtIndexPaths:[self.layout indexPathsForObject:@(kRowIndexPrice)]
                          withRowAnimation:UITableViewRowAnimationNone];
}
@synthesize originalBarcode = _originalBarcode;
- (void) setOriginalBarcode:(NSString *)originalBarcode
{
    _originalBarcode = originalBarcode;
    // if we have a barcode
    if (self.hasBarcode) {
        // reset the price
        self.price = nil;
        // reset the original tx details
        self.originalStoreNumberCell.detailTextField.text = nil;
        self.originalTillCell.detailTextField.text = nil;
        self.originalTransactionIdCell.detailTextField.text = nil;
        self.originalDate = nil;
        // reload the section
        [self.tableView reloadSections:[NSIndexSet indexSetWithIndex:kSectionIndexOriginalTx]
                      withRowAnimation:UITableViewRowAnimationNone];
    } else {
        // reload the cell
        [self.tableView reloadRowsAtIndexPaths:[self.layout indexPathsForObject:@(kRowIndexBarcode)]
                              withRowAnimation:UITableViewRowAnimationNone];
    }
}
@synthesize originalDate = _originalDate;
- (void) setOriginalDate:(NSDate *)originalDate
{
    _originalDate = originalDate;
    // update the cell
    self.originalTransactionDateCell.detailTextField.text = self.formattedOriginalDate;
}
@synthesize formattedOriginalDate;
- (NSString *) formattedOriginalDate
{
    NSString *result = nil;
    
    if (self.originalDate != nil) {
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        formatter.dateStyle = NSDateFormatterMediumStyle;
        formatter.timeStyle = NSDateFormatterNoStyle;
        result = [formatter stringFromDate:self.originalDate];
    }
    
    return result;
}
@synthesize hasBarcode;
- (BOOL) hasBarcode
{
    return ((self.originalBarcode != nil) && (![self.originalBarcode isEqualToString:@""]));
}
@synthesize hasTxInfo;
- (BOOL) hasTxInfo
{
    BOOL result = NO;
    
    // if we have a store number
    UITextField *textfield = self.originalStoreNumberCell.detailTextField;
    if ((textfield.text != nil) && (![textfield.text isEqualToString:@""])) {
        result = YES;
    }
    // or till id
    textfield = self.originalTillCell.detailTextField;
    if ((textfield.text != nil) && (![textfield.text isEqualToString:@""])) {
        result = YES;
    }
    // or tx id
    textfield = self.originalTransactionIdCell.detailTextField;
    if ((textfield.text != nil) && (![textfield.text isEqualToString:@""])) {
        result = YES;
    }
    // or date
    if (self.originalDate != nil) {
        result = YES;
    }
    
    return result;
}

#pragma mark - UIViewController

- (void) viewDidLoad
{
    [super viewDidLoad];
    // init
    self.originalBarcode = nil;
    self.originalDate = nil;
    self.originalPrice = nil;
    self.errored = NO;
    self.validator = [[OCGNumberTextFieldValidator alloc] init];
    self.validator.scale = 0;
}

- (void) viewWillAppear:(BOOL)animated
{
    // ui
    self.title = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁧󠀱󠁦󠁨󠀴󠁈󠀫󠁷󠁈󠀶󠁫󠁬󠀶󠀲󠀹󠀰󠁃󠁢󠀶󠁄󠀷󠁸󠁔󠁖󠁰󠀰󠁙󠁿*/ @"Return Item", nil);
    self.inputView = [[UIToolbar alloc] initWithFrame:CGRectMake(0.0f, 0.0f, self.view.frame.size.width, 44.0f)];
    self.navControl =
    [[UISegmentedControl alloc] initWithItems:@[NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁸󠁶󠁬󠁔󠁈󠁅󠁆󠁲󠁌󠁲󠁵󠁓󠁢󠀱󠁴󠁒󠁐󠁦󠁬󠁊󠁐󠁤󠁓󠁰󠁧󠀱󠁧󠁿*/ @"Previous", nil), NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁑󠁙󠀵󠀰󠀫󠁦󠁄󠁴󠁉󠁭󠁺󠁗󠁈󠁘󠁋󠁥󠁳󠁆󠁮󠁂󠁓󠁉󠁧󠀰󠁧󠀵󠁯󠁿*/ @"Next", nil)]];
    self.navControl.momentary = YES;
    [self.navControl addTarget:self
                        action:@selector(navControlChanged:)
              forControlEvents:UIControlEventValueChanged];
    UIBarButtonItem *doneButton =
    [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁃󠀲󠁕󠁲󠁤󠀲󠁯󠁮󠁊󠀸󠁘󠁓󠁌󠁕󠁆󠁨󠁲󠁤󠁅󠁣󠁃󠁯󠁙󠁵󠁂󠀹󠁁󠁿*/ @"Done", nil)
                                     style:UIBarButtonItemStyleDone
                                    target:self
                                    action:@selector(doneTapped:)];
    [self.inputView setItems:@[[[UIBarButtonItem alloc] initWithCustomView:self.navControl],
                               [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                               doneButton]];
    // cells
    self.layout =
    @[@[@(kRowIndexItemInfo), @(kRowIndexReason), @(kRowIndexPrice)],
      @[@(kRowIndexBarcode), @(kRowIndexStoreId), @(kRowIndexTillId), @(kRowIndexTxId), @(kRowIndexDate)]
      ];
    // item details
    [self.tableView registerClass:[BasketItemTableViewCell class] forCellReuseIdentifier:kBasketItemTableViewCell];
    // original store number
    self.originalStoreNumberCell =
    [[OCGTableViewCell alloc] initWithStyle:UITableViewCellStyleValue2
                                     reuseIdentifier:nil];
    self.originalStoreNumberCell.tag = kEditableTableCellSkinningTag;
    self.originalStoreNumberCell.selectionStyle = UITableViewCellSelectionStyleNone;
    self.originalStoreNumberCell.textLabel.tag = kEditableCellKeySkinningTag;
    self.originalStoreNumberCell.textLabel.text = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁹󠁣󠁫󠁆󠁑󠁷󠁗󠁋󠁺󠁳󠀳󠁪󠁫󠀸󠀶󠁅󠁃󠁧󠁢󠁶󠁙󠁱󠁄󠁷󠁃󠁥󠀴󠁿*/ @"Store", nil);
    UITextField *textfield = self.originalStoreNumberCell.detailTextField;
    textfield.textAlignment = NSTextAlignmentRight;
    textfield.delegate = self;
    textfield.text = nil;
    textfield.placeholder = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠀳󠁶󠀹󠁣󠁱󠁡󠀰󠀴󠁤󠁎󠀷󠁍󠁇󠁹󠁏󠁒󠁷󠁢󠁁󠀸󠁌󠁔󠁒󠁲󠁐󠀷󠁣󠁿*/ @"Original store number", nil);
    textfield.autocapitalizationType = UITextAutocapitalizationTypeNone;
    textfield.autocorrectionType = UITextAutocorrectionTypeNo;
    [OCGKeyboardTypeButtonItem setKeyboardType:kOCGKeyboardTypeNumberPad forTarget:textfield];
    textfield.tag = kEditableCellValueSkinningTag;
    // original terminal number
    self.originalTillCell =
    [[OCGTableViewCell alloc] initWithStyle:UITableViewCellStyleValue2
                                     reuseIdentifier:nil];
    self.originalTillCell.tag = kEditableTableCellSkinningTag;
    self.originalTillCell.selectionStyle = UITableViewCellSelectionStyleNone;
    self.originalTillCell.textLabel.tag = kEditableCellKeySkinningTag;
    self.originalTillCell.textLabel.text = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁙󠁖󠁏󠁆󠁌󠁌󠁁󠀫󠁕󠁰󠁯󠁹󠁧󠁤󠁓󠁴󠀴󠁕󠁒󠁬󠁫󠁘󠁎󠁁󠁅󠁹󠁫󠁿*/ @"Till", nil);
    textfield = self.originalTillCell.detailTextField;
    textfield.textAlignment = NSTextAlignmentRight;
    textfield.delegate = self;
    textfield.text = nil;
    textfield.placeholder = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁬󠁭󠁰󠀸󠁵󠁕󠁥󠁯󠁺󠁹󠁥󠁰󠁷󠁇󠁱󠁊󠁚󠀶󠁆󠁢󠁚󠀰󠁗󠁥󠁹󠁏󠁧󠁿*/ @"Original till or operator #", nil);
    textfield.autocapitalizationType = UITextAutocapitalizationTypeNone;
    textfield.autocorrectionType = UITextAutocorrectionTypeNo;
    [OCGKeyboardTypeButtonItem setKeyboardType:kOCGKeyboardTypeNumberPad forTarget:textfield];
    textfield.tag = kEditableCellValueSkinningTag;
    // original transaction number
    self.originalTransactionIdCell =
    [[OCGTableViewCell alloc] initWithStyle:UITableViewCellStyleValue2
                                     reuseIdentifier:nil];
    self.originalTransactionIdCell.tag = kEditableTableCellSkinningTag;
    self.originalTransactionIdCell.selectionStyle = UITableViewCellSelectionStyleNone;
    self.originalTransactionIdCell.textLabel.tag = kEditableCellKeySkinningTag;
    self.originalTransactionIdCell.textLabel.text = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁦󠁕󠁗󠁕󠁦󠀯󠁷󠁺󠁓󠁫󠁌󠁔󠁭󠁓󠁉󠀷󠁑󠁭󠁇󠁳󠁣󠀷󠁒󠁅󠁤󠁱󠁫󠁿*/ @"Tx Id", nil);
    textfield = self.originalTransactionIdCell.detailTextField;
    textfield.textAlignment = NSTextAlignmentRight;
    textfield.delegate = self;
    textfield.text = nil;
    textfield.placeholder = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁁󠁯󠁍󠀴󠀯󠁭󠁙󠁓󠁅󠁳󠁷󠁷󠁔󠁴󠁦󠁆󠁶󠁨󠀵󠀳󠁘󠁯󠁭󠁏󠁅󠁙󠁳󠁿*/ @"Original transaction #", nil);
    textfield.autocapitalizationType = UITextAutocapitalizationTypeNone;
    textfield.autocorrectionType = UITextAutocorrectionTypeNo;
    [OCGKeyboardTypeButtonItem setKeyboardType:kOCGKeyboardTypeNumberPad forTarget:textfield];
    textfield.tag = kEditableCellValueSkinningTag;
    // original transaction date
    self.originalTransactionDateCell =
    [[OCGTableViewCell alloc] initWithStyle:UITableViewCellStyleValue2
                                     reuseIdentifier:nil];
    self.originalTransactionDateCell.tag = kEditableTableCellSkinningTag;
    self.originalTransactionDateCell.selectionStyle = UITableViewCellSelectionStyleNone;
    self.originalTransactionDateCell.textLabel.tag = kEditableCellKeySkinningTag;
    self.originalTransactionDateCell.textLabel.text = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠀱󠁏󠁂󠁂󠁑󠁇󠁧󠁑󠁫󠁕󠁐󠀲󠁷󠁊󠁩󠀷󠁮󠁈󠁶󠁈󠁒󠁆󠁈󠁷󠁊󠁺󠀸󠁿*/ @"Date", nil);
    textfield = self.originalTransactionDateCell.detailTextField;
    textfield.textAlignment = NSTextAlignmentRight;
    textfield.delegate = self;
    textfield.text = nil;
    textfield.placeholder = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁆󠀲󠁎󠁏󠁐󠁅󠀹󠀸󠁴󠀱󠁥󠁐󠀹󠁦󠁹󠁡󠁕󠁴󠁡󠁥󠁕󠁘󠁊󠁒󠀸󠁨󠁧󠁿*/ @"Original transaction date", nil);
    textfield.autocapitalizationType = UITextAutocapitalizationTypeNone;
    textfield.autocorrectionType = UITextAutocorrectionTypeNo;
    [OCGKeyboardTypeButtonItem setKeyboardType:kOCGKeyboardTypeNumberPad forTarget:textfield];
    textfield.tag = kEditableCellValueSkinningTag;
    // setup a date picker as the input view
    UIDatePicker *datePicker = [[UIDatePicker alloc] init];
    datePicker.datePickerMode = UIDatePickerModeDate;
    [datePicker addTarget:self
                   action:@selector(datePickerValueChanged:)
         forControlEvents:UIControlEventValueChanged];
    textfield.inputView = datePicker;
    // ok and cancel buttons
    self.navigationItem.leftBarButtonItem =
    [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁩󠁢󠁺󠁬󠁏󠁶󠁣󠁧󠁫󠁍󠁋󠁧󠁫󠀱󠁶󠁐󠁐󠀹󠁘󠀴󠁘󠁡󠁕󠁭󠁹󠁷󠀰󠁿*/ @"Cancel", nil)
                                     style:UIBarButtonItemStylePlain
                                    target:self
                                    action:@selector(cancelTapped:)];
    self.navigationItem.leftBarButtonItem.tag = kSecondaryButtonSkinningTag;
    self.navigationItem.rightBarButtonItem =
    [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁲󠁭󠁵󠁶󠁘󠁹󠀰󠁴󠁨󠀫󠁇󠁄󠁩󠁱󠀶󠁴󠁃󠁔󠀳󠁋󠁄󠁹󠁑󠁗󠁔󠁣󠀸󠁿*/ @"Return", nil)
                                     style:UIBarButtonItemStylePlain
                                    target:self
                                    action:@selector(returnTapped:)];
    self.navigationItem.rightBarButtonItem.tag = kPrimaryButtonSkinningTag;
    // skin
    self.tableView.tag = kEditableTableSkinningTag;
    [[CRSSkinning currentSkin] applyViewSkin:self];

    [super viewWillAppear:animated];
    
    // init state
    self.errored = NO;
    
    // show the navigation bar
    [self.navigationController setNavigationBarHidden:NO
                                             animated:YES];
    
    // listen for notifications
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(handleBasketItemChangeNotification:)
                                                 name:kBasketControllerBasketItemChangeNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(handleBasketAddItemErrorNotification:)
                                                 name:kBasketControllerBasketAddItemErrorNotification
                                               object:nil];
}

- (void) viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - UITableViewDataSource

- (NSInteger) numberOfSectionsInTableView:(UITableView *)tableView
{
    return [self.layout count];
}

- (NSString *) tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    NSString *result = nil;
    
    switch (section) {
        case kSectionIndexOriginalTx:
            result = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁩󠁍󠁋󠁣󠁁󠁺󠁥󠁭󠁺󠁙󠁃󠁡󠁡󠁌󠁱󠁌󠁴󠁅󠁦󠁐󠁣󠁡󠀴󠀱󠀵󠁙󠁙󠁿*/ @"Original transaction details", nil);
            break;
        default:
            break;
    }
    
    return result;
}

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.layout[section] count];
}

- (UITableViewCell *) tableView:(UITableView *)tableView
          cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    OCGTableViewCell *cell = nil;
    
    NSNumber *rowIdentifier = [self.layout objectForIndexPath:indexPath];
    
    switch (rowIdentifier.integerValue) {
        case kRowIndexItemInfo: {
            // create a cell that displays the item's data in the table view
            cell = [tableView dequeueReusableCellWithIdentifier:kBasketItemTableViewCell];
            cell.backgroundColor = [UIColor colorWithRed:0.90f
                                                   green:0.90f
                                                    blue:0.90f
                                                   alpha:1.0f];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            break;
        }
        case kRowIndexBarcode: {
            cell = [tableView dequeueReusableCellWithIdentifier:@"BarcodeCell"];
            if (cell == nil) {
                cell = (OCGTableViewCell*) [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1
                                              reuseIdentifier:@"BarcodeCell"];
            }
            break;
        }
        case kRowIndexStoreId: 
            cell = self.originalStoreNumberCell;
            break;
        case kRowIndexTillId:
            cell = self.originalTillCell;
            break;
        case kRowIndexTxId:
            cell = self.originalTransactionIdCell;
            break;
        case kRowIndexDate :
            cell = self.originalTransactionDateCell;
            break;
        default: {
            cell = [tableView dequeueReusableCellWithIdentifier:kValueCell];
            if (cell == nil) {
                cell = (OCGTableViewCell*) [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1
                                              reuseIdentifier:kValueCell];
            }
            break;
        }
    }
    
    if (cell != nil) {
        switch (rowIdentifier.integerValue) {
            case kRowIndexItemInfo: {
                cell.tag = kEditableTableCellSkinningTag;
                [(BasketItemTableViewCell *)cell updateDisplayForItem:self.item tableStyle:tableView.style];
                break;
            }
            case kRowIndexReason: {
                cell.tag = kEditableTableCellSkinningTag;
                cell.textLabel.text = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁰󠁧󠁙󠁔󠁶󠁉󠁉󠁵󠁳󠁕󠁒󠀳󠁊󠁫󠁅󠁺󠁅󠀶󠀶󠁈󠀹󠁲󠁏󠁫󠁬󠁹󠁣󠁿*/ @"Reason", nil);
                cell.textLabel.text = self.reason.text;
                cell.textLabel.tag = kEditableCellKeySkinningTag;
                cell.textLabel.tag = kEditableCellValueSkinningTag;
                cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
                break;
            }
            case kRowIndexPrice: {
                cell.tag = kEditableTableCellSkinningTag;
                    cell.textLabel.text = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠀵󠁫󠁸󠁢󠁐󠁱󠀫󠁣󠁏󠁣󠁒󠁚󠁇󠁳󠁐󠁹󠁉󠀸󠀯󠁈󠁥󠁏󠁤󠁺󠁔󠁉󠀰󠁿*/ @"Price", nil);
                // check if the user has specified a price
                if ((self.price == nil) || ([self.price isEqualToString:@""])) {
                    // show original instead of just blank
                    cell.textLabel.text = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁙󠁊󠀹󠀱󠁈󠀳󠁙󠁚󠁎󠁏󠁆󠁪󠁡󠁵󠁁󠁔󠀹󠁘󠁉󠁷󠁏󠁪󠁐󠁈󠁯󠁕󠀸󠁿*/ @"Original", nil);
                } else {
                    // display the custom price
                    cell.textLabel.text = self.price;
                }
                cell.textLabel.tag = kEditableCellKeySkinningTag;
                cell.textLabel.tag = kEditableCellValueSkinningTag;
                if (self.hasBarcode) {
                    cell.accessoryType = UITableViewCellAccessoryNone;
                } else {
                    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
                }
                break;
            }
            case kRowIndexBarcode: {
                cell.textLabel.text = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠀱󠁗󠁒󠁃󠁣󠁵󠀸󠁱󠁢󠁐󠁥󠀹󠁚󠀱󠁘󠀷󠀫󠁊󠀹󠁡󠁨󠁐󠁙󠁦󠁏󠀹󠁣󠁿*/ @"Receipt Barcode", nil);
                cell.textLabel.text = self.originalBarcode;
                if (!self.hasBarcode) {
                    cell.textLabel.text = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠀲󠁇󠀯󠁂󠁋󠁔󠁯󠁗󠁌󠁩󠀷󠁮󠁉󠁋󠁪󠁑󠁐󠁎󠀫󠁣󠁲󠁡󠁬󠁫󠁡󠁱󠁳󠁿*/ @"(tap to scan)", nil);
                }
                cell.textLabel.tag = kEditableCellKeySkinningTag;
                cell.textLabel.tag = kEditableCellValueSkinningTag;
                cell.textLabel.opaque = YES;
                cell.textLabel.alpha = 1.0;
                if (!self.hasBarcode) {
                    cell.textLabel.alpha = 0.2;
                    cell.textLabel.opaque = NO;
                } else {
                    cell.textLabel.alpha = 1.0;
                    cell.textLabel.opaque = YES;
                }
                cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
                break;
            }
            default: {
                if (rowIdentifier.integerValue == kRowIndexDate) {
                    self.originalTransactionDateCell.detailTextField.text = self.formattedOriginalDate;
                }
                OCGTableViewCell *textCell = (OCGTableViewCell *)cell;
                textCell.textLabel.opaque = YES;
                textCell.textLabel.alpha = 1.0;
                textCell.detailTextField.alpha = 1.0;
                textCell.detailTextField.opaque = YES;
                break;
            }
        }
    }
    
    return cell;
}

- (void) tableView:(UITableView *)tableView
   willDisplayCell:(UITableViewCell *)cell
 forRowAtIndexPath:(NSIndexPath *)indexPath
{
    [[CRSSkinning currentSkin] applyCellSkin:cell
                                    forStyle:tableView.style];
}

#pragma mark - UITableViewDelegate

- (NSIndexPath *) tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSIndexPath *result = indexPath;
    
    NSNumber *rowIdentifier = [self.layout objectForIndexPath:indexPath];
    
    switch (rowIdentifier.integerValue) {
        case kRowIndexItemInfo: {
            result = nil;
            break;
        }
        case kRowIndexPrice: {
            if (self.hasBarcode) {
                result = nil;
            }
            break;
        }
    }
    
    return result;
}

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSNumber *rowIdentifier = [self.layout objectForIndexPath:indexPath];
    
    // stop scanning if the user selects a row that isn't the barcode scan row
    if (rowIdentifier.integerValue != kRowIndexBarcode) {
        BarcodeScannerController *controller = [BarcodeScannerController sharedInstance];
        // are we scanning?
        if (controller.scanning) {
            // stop
            [controller stopScanning];
        }
    }
    
    // hide any visible input views
    if (self.currentTextField != nil) {
        [self.currentTextField endEditing:YES];
    }
    
    switch (rowIdentifier.integerValue) {
        case kRowIndexReason: {
            // present the reason list
            OCGSelectViewController *reasonVC =
            [[OCGSelectViewController alloc] init];
            reasonVC.title = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁮󠁘󠁒󠁉󠀰󠁊󠁙󠁑󠁊󠁡󠀷󠁎󠁡󠁦󠁷󠁲󠁳󠁬󠀱󠁭󠀸󠀯󠁳󠁫󠁄󠁋󠁫󠁿*/ @"Return Reason", nil);
            reasonVC.options = @[[[BasketController sharedInstance] returnReasons]];
            reasonVC.titleForOption = ^(OCGSelectViewController *selectViewController, NSIndexPath *indexPath)
            {
                DiscountReason *returnReason =
                [selectViewController.options objectForIndexPath:indexPath];
                return returnReason.text;
            };
            reasonVC.didSelectOption = ^(OCGSelectViewController *selectViewController, NSIndexPath *indexPath)
            {
                DiscountReason *returnReason =
                [selectViewController.options objectForIndexPath:indexPath];
                if (![returnReason isEqual:self.reason]) {
                    self.reason = [selectViewController.options objectForIndexPath:indexPath];
                }
                [self.navigationController popViewControllerAnimated:YES];
            };
            [self.navigationController pushViewController:reasonVC
                                                 animated:YES];
            break;
        }
        case kRowIndexPrice: {
            ReturnPriceTableViewController *vc =
            [[ReturnPriceTableViewController alloc] initWithStyle:UITableViewStylePlain];
            vc.item = self.item;
            vc.price = nil;
            vc.delegate = self;
            [self.navigationController pushViewController:vc
                                                 animated:YES];
            break;
        }
        case kRowIndexBarcode: {
            // start/stop scanning
            BarcodeScannerController *controller = [BarcodeScannerController sharedInstance];
            // are we scanning?
            if (!controller.scanning) {
                [self startScan];
            } else {
                [controller stopScanning];
            }
            break;
        }
        case kRowIndexStoreId: {
            [self.originalStoreNumberCell.detailTextField becomeFirstResponder];
            break;
        }
        case kRowIndexTillId: {
            [self.originalTillCell.detailTextField becomeFirstResponder];
            break;
        }
        case kRowIndexTxId: {
            [self.originalTransactionIdCell.detailTextField becomeFirstResponder];
            break;
        }
        case kRowIndexDate: {
            [self.originalTransactionDateCell.detailTextField becomeFirstResponder];
            break;
        }
    }
}

- (BOOL) tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    BOOL result = NO;
    
    // allow the user to delete the barcode
    NSNumber *rowIdentifier = [self.layout objectForIndexPath:indexPath];
    if (rowIdentifier.integerValue == kRowIndexBarcode) {
        result = YES;
    }
    
    return result;
}

- (void) tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    // allow the user to delete the barcode
    NSNumber *rowIdentifier = [self.layout objectForIndexPath:indexPath];
    if (rowIdentifier.integerValue == kRowIndexBarcode) {
        if (editingStyle == UITableViewCellEditingStyleDelete) {
            self.originalBarcode = nil;
            [tableView reloadData];
        }
    }
}

#pragma mark - UITextFieldDelegate

- (BOOL) textFieldShouldBeginEditing:(UITextField *)textField
{
    // make a note of the text field
    self.currentTextField = textField;
    self.currentTextField.inputAccessoryView = self.inputView;
    // scroll to the field
    NSIndexPath *nextPath = nil;
    if (textField == self.originalStoreNumberCell.detailTextField) {
        nextPath = [self.layout indexPathsForObject:@(kRowIndexStoreId)][0];
        // disable the previous button
        [self.navControl setEnabled:NO
                  forSegmentAtIndex:0];
        // enable the next button
        [self.navControl setEnabled:YES
                  forSegmentAtIndex:1];
    } else if (textField == self.originalTillCell.detailTextField) {
        nextPath = [self.layout indexPathsForObject:@(kRowIndexTillId)][0];
        // enable the previous button
        [self.navControl setEnabled:YES
                  forSegmentAtIndex:0];
        // enable the next button
        [self.navControl setEnabled:YES
                  forSegmentAtIndex:1];
    } else if (textField == self.originalTransactionIdCell.detailTextField) {
        nextPath = [self.layout indexPathsForObject:@(kRowIndexTxId)][0];
        // enable the previous button
        [self.navControl setEnabled:YES
                  forSegmentAtIndex:0];
        // enable the next button
        [self.navControl setEnabled:YES
                  forSegmentAtIndex:1];
    } else if (textField == self.originalTransactionDateCell.detailTextField) {
        nextPath = [self.layout indexPathsForObject:@(kRowIndexDate)][0];
        // enable the previous button
        [self.navControl setEnabled:YES
                  forSegmentAtIndex:0];
        // disable the next button
        [self.navControl setEnabled:NO
                  forSegmentAtIndex:1];
        // default to today's date
        if (self.originalDate == nil) {
            self.originalDate = [NSDate date];
            self.originalTransactionDateCell.detailTextField.text = self.formattedOriginalDate;
        }
    }
    if (nextPath != nil) {
        [self.tableView scrollToRowAtIndexPath:nextPath
                              atScrollPosition:UITableViewScrollPositionTop
                                      animated:YES];
    }
    
    return YES;
}

- (BOOL) textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    BOOL result = [self.validator textField:textField
              shouldChangeCharactersInRange:range
                          replacementString:string];
    // clear out the barcode if we have tx details instead
    if (self.hasTxInfo) {
        self.originalBarcode = nil;
    }
    
    return result;
}

- (BOOL) textFieldShouldReturn:(UITextField *)textField
{
    // clear out the barcode if we have tx details instead
    if (self.hasTxInfo) {
        self.originalBarcode = nil;
    }
    return NO;
}

#pragma mark - ReturnPriceTableViewControllerDelegate

- (void) returnPriceTableViewController:(ReturnPriceTableViewController *)vc
                        didSpecifyPrice:(NSString *)price
{
    self.price = price;
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - Actions

- (void) cancelTapped:(id)sender
{
    [self dismissViewControllerAnimated:YES
                             completion:nil];
}

- (void) returnTapped:(id)sender
{
    // hide any visible input views
    [self.currentTextField endEditing:YES];
    // validate
    if ([self validateEntries]) {
        // check if the user has provided any original transaction details
        if ((!self.hasBarcode) && (!self.hasTxInfo)) {
            UIAlertView *alert =
            [[UIAlertView alloc] initWithTitle:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠀵󠀱󠁴󠁤󠀲󠁱󠀸󠀹󠁚󠁢󠁑󠀱󠁙󠁗󠁘󠀴󠁖󠀲󠁑󠁖󠁅󠁚󠁫󠁊󠁌󠁡󠁍󠁿*/ @"Unlinked Return", nil)
                                       message:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁳󠁇󠁩󠁣󠁗󠁰󠁆󠁮󠁎󠁂󠁺󠁃󠀱󠀸󠁰󠁦󠁔󠁳󠁖󠁏󠁧󠀳󠁤󠁡󠁷󠁴󠁣󠁿*/ @"You have not provided receipt details for the original sale transaction. This will perform an unlinked return. Are you sure you wish to proceed?", nil)
                                      delegate:[UIAlertView class]
                             cancelButtonTitle:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁩󠁢󠁺󠁬󠁏󠁶󠁣󠁧󠁫󠁍󠁋󠁧󠁫󠀱󠁶󠁐󠁐󠀹󠁘󠀴󠁘󠁡󠁕󠁭󠁹󠁷󠀰󠁿*/ @"Cancel", nil)
                             otherButtonTitles:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁫󠀹󠁮󠁺󠁮󠀰󠁗󠁄󠁖󠀷󠀲󠁔󠁖󠀴󠁤󠀴󠁫󠁶󠁯󠁚󠀹󠁖󠁷󠁨󠁸󠁙󠀰󠁿*/ @"Proceed", nil), nil];
            alert.dismissBlock = ^(int buttonIndex) {
                [self performReturn];
            };
            [alert show];
        } else {
            [self performReturn];
        }
    }
}

- (void) navControlChanged:(id)sender
{
    // get the current text field
    UITextField *textField = self.currentTextField;
    [textField resignFirstResponder];
    
    if (self.navControl.selectedSegmentIndex == 0) {
        // move to the previous field
        if (textField == self.originalTransactionDateCell.detailTextField) {
            [self.originalTransactionIdCell.detailTextField becomeFirstResponder];
        }
        if (textField == self.originalTransactionIdCell.detailTextField) {
            [self.originalTillCell.detailTextField becomeFirstResponder];
        }
        if (textField == self.originalTillCell.detailTextField) {
            [self.originalStoreNumberCell.detailTextField becomeFirstResponder];
        }
    } else if (self.navControl.selectedSegmentIndex == 1) {
        // move to the next field
        [textField resignFirstResponder];
        if (textField == self.originalStoreNumberCell.detailTextField) {
            [self.originalTillCell.detailTextField becomeFirstResponder];
        }
        if (textField == self.originalTillCell.detailTextField) {
            [self.originalTransactionIdCell.detailTextField becomeFirstResponder];
        }
        if (textField == self.originalTransactionIdCell.detailTextField) {
            [self.originalTransactionDateCell.detailTextField becomeFirstResponder];
        }
    }
    
    // clear out the barcode if we have tx details instead
    if (self.hasTxInfo) {
        self.originalBarcode = nil;
    }
}

- (void) doneTapped:(id)sender
{
    [self.currentTextField resignFirstResponder];
    // clear out the barcode if we have tx details instead
    if (self.hasTxInfo) {
        self.originalBarcode = nil;
    }
}

- (void) datePickerValueChanged:(id)sender
{
    UIDatePicker *picker = (UIDatePicker *)sender;
    // set the date
    self.originalDate = picker.date;
    // clear out the barcode if we have tx details instead
    if (self.hasTxInfo) {
        self.originalBarcode = nil;
    }
}

@end
