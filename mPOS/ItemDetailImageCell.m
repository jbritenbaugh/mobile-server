//
//  ItemDetailImageCell.m
//  mPOS
//
//  Created by Antonio Strijdom on 22/05/2013.
//  Copyright (c) 2013 Clarity. All rights reserved.
//

#import "ItemDetailImageCell.h"

@implementation ItemDetailImageCell

#pragma mark - UICollectionViewCell

- (id) initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

@end
