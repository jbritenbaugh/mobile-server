//
//  BasketItemAddViewController.h
//  mPOS
//
//  Created by John Scott on 03/02/2014.
//  Copyright (c) 2014 Clarity. All rights reserved.
//

#import <UIKit/UIKit.h>

/** @file BasketItemAddViewController.h */

/** @brief Table view controller for handling add item errors.
 */
@interface BasketItemAddViewController : UITableViewController

@property (nonatomic, strong) dispatch_block_t cancelBlock;

/** @brief Handles add item errors.
 */
- (BOOL) handleAddBasketItemError:(ServerError *)error;

@end
