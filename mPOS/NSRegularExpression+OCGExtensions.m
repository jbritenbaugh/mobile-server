//
//  NSRegularExpression+OCGExtensions.m
//  mPOS
//
//  Created by John Scott on 07/11/2014.
//  Copyright (c) 2014 Clarity. All rights reserved.
//

#import "NSRegularExpression+OCGExtensions.h"

@implementation NSRegularExpression (OCGExtensions)

- (NSArray *)OCGExtensions_matchesInString:(NSString *)string groupNames:(NSArray*)groupNames
{
    NSArray *matches = [self matchesInString:string options:0 range:NSMakeRange(0, [string length])];
    
    NSMutableArray *matchDictionaries = [NSMutableArray array];
    
    for (NSTextCheckingResult* match in matches)
    {
        NSAssert(match.numberOfRanges == groupNames.count, @"All ranges must have matching group names");
        NSMutableDictionary *matchDictionary = [NSMutableDictionary dictionary];
        for (NSUInteger rangeIndex=0; rangeIndex<match.numberOfRanges; rangeIndex++)
        {
            NSRange range = [match rangeAtIndex:rangeIndex];
            if (range.location != NSNotFound)
            {
                matchDictionary[groupNames[rangeIndex]] = [string substringWithRange:range];
            }
        }
        [matchDictionaries addObject:matchDictionary];
    }
    return [matchDictionaries copy];
}

@end
