//
//  OCGModifierCell.m
//  mPOS
//
//  Created by Antonio Strijdom on 17/09/2014.
//  Copyright (c) 2014 Clarity. All rights reserved.
//

#import "OCGModifierCell.h"

@implementation OCGModifierCell

-(void) setHighlighted:(BOOL)highlighted
{
    [super setHighlighted:highlighted];
    if (highlighted) {
        self.backgroundColor = [UIColor colorWithRed:0.85
                                               green:0.85
                                                blue:0.85
                                               alpha:1.0];
    } else {
        self.backgroundColor = [UIColor whiteColor];
    }
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        self.titleLabel = [[UILabel alloc] init];
        self.titleLabel.font = [UIFont systemFontOfSize:[UIFont systemFontSize]];
        self.titleLabel.translatesAutoresizingMaskIntoConstraints = NO;
        self.titleLabel.textAlignment = NSTextAlignmentCenter;
        self.titleLabel.backgroundColor = [UIColor clearColor];
        [self.contentView addSubview:self.titleLabel];
        [self.titleLabel constrain:@"left = left" to:self.contentView];
        [self.titleLabel constrain:@"top = top" to:self.contentView];
        [self.titleLabel constrain:@"right = right" to:self.contentView];
        [self.titleLabel constrain:@"bottom = bottom" to:self.contentView];
        self.priceLabel = [[UILabel alloc] init];
        self.priceLabel.font = [UIFont systemFontOfSize:[UIFont smallSystemFontSize]];
        self.priceLabel.translatesAutoresizingMaskIntoConstraints = NO;
        self.priceLabel.textAlignment = NSTextAlignmentRight;
        self.priceLabel.backgroundColor = [UIColor clearColor];
        [self.contentView addSubview:self.priceLabel];
        [self.priceLabel constrain:@"left = left" to:self.contentView];
        [self.priceLabel constrain:@"right = right" to:self.contentView];
        [self.priceLabel constrain:@"bottom = bottom" to:self.contentView];
    }
    return self;
}

- (void)prepareForReuse
{
    [super prepareForReuse];
    self.titleLabel.text = nil;
    self.priceLabel.text = nil;
}

@end
