//
//  OCGEFTProviderYespay.m
//  mPOS
//
//  Created by Antonio Strijdom on 19/04/2013.
//  Copyright (c) 2013 Clarity. All rights reserved.
//

#import "OCGEFTProviderYespay.h"
#import "OCGEFTManager.h"
#import "RESTController.h"
#import "BasketController.h"
#import <CommonCrypto/CommonHMAC.h>

@interface OCGEFTProviderYespay ()
- (NSString *) computeHashForString:(NSString *)input
                            withKey:(NSString *)key;
- (EFTProviderErrorCode) EFTProviderErrorCodeFromYespayTransactionResult:(NSInteger)transactionResult;
- (NSString *) localizedDescriptionForResult:(NSInteger)transactionResult;
@end

@implementation OCGEFTProviderYespay

#pragma mark - Private

static NSString * kYESPAY_CONFIG_BUNDLEURLNAME = @"YesPayControllerURLIdentifier";
static NSString * const kYESPAY_CONFIG_RESPONDURL = @"OpenPOSApp";

/** @enum YesPayRequestParam
 *  @brief Following is the list of input attributes for a transaction.
 */
NS_ENUM(NSInteger, YesPayRequestParam) {
    /** @brief Transaction reference, which uniquely identifies the transaction with a user defined tag. For cancel, use the transaction reference of original transaction to be cancelled.
     */
    kYesPayRequestParamTransactionReference = 1,
    /** @brief Transaction Type. Transaction Type indicates the type of financial transaction.
     *  @see YesPayTransactionType
     */
    kYesPayRequestParamTransactionType = 2,
    /** @brief Transaction Amount
     *  In case of gift card Activation transaction, transaction amount shall be specified as 0.
     */
    kYesPayRequestParamTransactionAmount = 3,
    /** @brief Cash-back Amount, present only if the transaction type is kYesPayTransactionTypeSaleWithCashback.
     *  @see kYesPayTransactionTypeSaleWithCashback
     */
    kYesPayRequestParamCashbackAmount = 4,
    /** @brief PAN, present in case of a cardholder not present (CNP) or keyed or pre sales completion transaction. Required for Cancel transaction unless a check card is cancelled.
     *  In case of E-Top Up transaction, the mobile number will be keyed in.
     */
    kYesPayRequestParamPAN = 5,
    /** @brief Card Expiry Date, present in case of a CNP or keyed or pre sales completion transaction in MMYY format. Required for Cancel transaction unless a check card is cancelled.
     */
    kYesPayRequestParamExpiryDate = 6,
    /** @brief Card Verification Value (CVV), present if address verification requested.
     */
    kYesPayRequestParamCVV = 7,
    /** @brief Address, present if address verification requested.
     */
    kYesPayRequestParamAddress = 8,
    /** @brief Post Code, present if address verification requested.
     */
    kYesPayRequestParamPostCode = 9,
    /** @brief Start Date, present in case of a CNP or keyed transaction and card scheme is UK Maestro/Solo. Should be in YYMM format.
     */
    kYesPayRequestParamStartDate = 10,
    /** @brief Issue Number, present in case of a CNP or keyed transaction and card scheme is UK Maestro/Solo.
     */
    kYesPayRequestParamIssueNumber = 11,
    /** @brief CNP indicator, specifies if the transaction is cardholder not present. 
     *  Value is 1 if CNP transaction otherwise 0.
     */
    kYesPayRequestParamCNP = 12,
    /** @brief Transaction reference. For Pre Sales, specifies the reference of the pre sales transaction to be completed. For Void, specifies the reference of the transaction to be Voided. (Returned in field 28 of output message. Refer output message details).
     *  @see
     */
    kYesPayRequestParamOriginalTransaction = 13,
    /** @brief Security Code (Gift Card) – 4 characters
     */
    kYesPayRequestParamGiftCardSecurityCode = 14,
    /** @brief Surcharge Amount
     */
    kYesPayRequestParamSurcharge = 15,
    /** @brief Retrieval Reference Number. 12 digits numeric number. To void Sale, Refund and Payment with Cash Back (PWCB) transaction of Debit.
     */
    kYesPayRequestParamRetreivalReference = 16,
    /** @brief Transaction Type. 2 digits numeric number. To void a Sale, Refund and PWCB transaction of Debit.
     */
    kYesPayRequestParamRetreivalTransactionType = 17,
    /** @brief Keyed transaction indicator, specifies if the transaction is keyed transaction.
     *  Value is 1 if Keyed transaction otherwise 0. 
     *  Keyed is allowed only for Sale and Refund transaction.
     */
    kYesPayRequestParamKeyedTransaction = 18,
    /** @brief Merchant Subscription Wallet Reference
     */
    kYesPayRequestParamWalletReference = 19,
    /** @brief Remove Card flag. This is Boolean field and mandatory in the request of Check Card functionality. 
     *  If this field is True then EVT will ask for remove card after returning card details.
     *  If this field is False then EVT will wait for a while for transaction request.
     */
    kYesPayRequestParamCardRemoveFlag = 25,
    /** @brief ProductInfo1
     *  (Information about the purchased item, Code/Description – 20 characters maximum, Amount – 7 digits maximum, Quantity – 5 digits maximum. pipe delimited)
     *  @example MOBILE-PHONE|9999|1
     */
    kYesPayRequestParamProductInfo1 = 31,
    /** @brief ProductInfo2
     */
    kYesPayRequestParamProductInfo2 = 32,
    /** @brief ProductInfo3
     */
    kYesPayRequestParamProductInfo3 = 33,
    /** @brief ProductInfo4
     */
    kYesPayRequestParamProductInfo4 = 34,
    /** @brief ProductInfo5
     */
    kYesPayRequestParamProductInfo5 = 35,
    /** @brief ProductInfo6
     */
    kYesPayRequestParamProductInfo6 = 36,
    /** @brief ProductInfo7
     */
    kYesPayRequestParamProductInfo7 = 37,
    /** @brief ProductInfo8
     */
    kYesPayRequestParamProductInfo8 = 38,
    /** @brief ProductInfo9
     */
    kYesPayRequestParamProductInfo9 = 39,
    /** @brief ProductInfo10
     */
    kYesPayRequestParamProductInfo10 = 40,
    /** @brief CallBackURL(The URL where the output shall be returned)
     */
    kYesPayRequestParamCallBackURL = 71,
    /** @brief A unique random number
     */
    kYesPayRequestParamSalt = 74,
    /** @brief A combination of (*UUID + Salt + Timestamp)
     */
    kYesPayRequestParamHash = 75,
    /** @brief Timestamp (Current or nearest date and time, Must be in ddMMyyyyHHmmss format)
     */
    kYesPayRequestParamTimestamp = 76,
    /** @brief End of message indicator, values always 0.
     */
    kYesPayRequestParamEndOfMessage = 99
};

/** @enum YesPayResponseParam
 *  @brief Following is the list of response attributes for a transaction.
 */
NS_ENUM(NSInteger, YesPayResponseParam) {
    /** @brief POS Entry Mode, card payment entry method.
     *  @see YesPayPOSEntryMode
     */
    kYesPayResponseParamEntryMethod = 1,
    /** @brief Transaction Type. 
     *  Transaction Type indicates the type of financial transaction, represented by the first two digits of ISO 8583:1987 Processing Code.
     *  @see YesPayResponseTransactionType
     */
    kYesPayResponseParamTransactionType = 2,
    /** @brief Transaction Result. Uniquely identifies the transaction response.
     *  @see YesPayResponseResult
     */
    kYesPayResponseParamTransactionResult = 3,
    /** @brief Authorisation Code, holds the value generated by the issuer for an approved transaction. Present only in case of a successful transaction.
     */
    kYesPayResponseParamAuthCode = 4,
    /** @brief EMV Card Data Element Application Primary Account Number (PAN) holds the valid cardholder account number.
     *  Masked PAN consists of first 6 digits and last 4 digits.
     *  @example 492949 XXXXXX 0002
     */
    kYesPayResponseParamPAN = 5,
    /** @brief EMV Card Data Element Application Label holds the mnemonic associated with the AID according to the ISO/IEC 7816-5.
     */
    kYesPayResponseParamLabel = 6,
    /** @brief EMV Card Data Element Application Effective Date which holds date from which the application may be used. The format is DDMMYYYY.
     */
    kYesPayResponseParamEffectiveDate = 7,
    /** @brief Transaction Date contains local date that the transaction was authorised. 
     *  The format is DDMMYYYY.
     */
    kYesPayResponseParamTransactionDate = 8,
    /** @brief Transaction Time contains local time that the transaction was authorised. 
     *  The format is HHMMSS.
     */
    kYesPayResponseParamTransactionTime = 9,
    /** @brief EMV Card Data Element Cardholder Name indicates cardholder name according to ISO 7813.
     */
    kYesPayResponseParamCardholderName = 10,
    /** @brief EMV Card Data Element Cardholder Name Extended indicates the whole cardholder name when greater than 26 characters using the same coding convention as in ISO 7813.
     */
    kYesPayResponseParamCardholderNameExtended = 11,
    /** @brief Merchant Identifier.
     */
    kYesPayResponseParamMerchantID = 12,
    /** @brief Terminal Identifier.
     */
    kYesPayResponseParamTerminalID = 13,
    /** @brief Card Verification Method
     *  @see YesPayCardVerificationMethod
     */
    kYesPayResponseParamCardVerificationMethod = 14,
    /** @brief Start Date, present only in case of a swiped UK Maestro/Solo card transaction.
     */
    kYesPayResponseParamStartDate = 15,
    /** @brief Total Number of Sale Counts
     */
    kYesPayResponseParamTotalSaleCount = 16,
    /** @brief Total Number of Refund Counts
     */
    kYesPayResponseParamTotalRefundCount = 17,
    /** @brief Total Sale Amount
     */
    kYesPayResponseParamTotalSaleAmount = 18,
    /** @brief Total Refund Amount
     */
    kYesPayResponseParamTotalRefundAmount = 19,
    /** @brief EFT Sequence number
     */
    kYesPayResponseParamEFTSequenceNumber = 21,
    /** @brief Merchant Address
     */
    kYesPayResponseParamMerchantAddress = 22,
    /** @brief Merchant Name
     */
    kYesPayResponseParamMerchantName = 23,
    /** @brief Batch Number
     */
    kYesPayResponseParamBatchNumber = 25,
    /** @brief Referral telephone number 1
     */
    kYesPayResponseParamReferralTelephoneNumber1 = 26,
    /** @brief Referral telephone number 2
     */
    kYesPayResponseParamReferralTelephoneNumber2 = 27,
    /** @brief PGTR, Payment gateway transaction reference.
     */
    kYesPayResponseParamPGTR = 28,
    /** @brief EMV Card Data Element Application Identifier (AID), which identifies the application as described in ISO/IEC 7816-5.
     */
    kYesPayResponseParamAID = 29,
    /** @brief PAN Sequence number or Issue Number. 
     *  PAN Sequence number in case of an ICC transaction.
     *  Issue number in case of a Swiped UK Maestro/Solo card transaction.
     */
    kYesPayResponseParamIssueNumberOrPAN = 30,
    /** @brief Transaction Status Information (TSI), present only in case of an ICC transaction. Used for debug purpose only.
     */
    kYesPayResponseParamTSI = 31,
    /** @brief Terminal Verification Results (TVR), present only in case of an ICC transaction. Used for debug purpose only.
     */
    kYesPayResponseParamTVR = 32,
    /** @brief Retention reminder
     */
    kYesPayResponseParamRetentionReminder = 33,
    /** @brief Customer Declaration
     */
    kYesPayResponseParamCustomerDeclaration = 34,
    /** @brief Additional Response Data, the CVV response
     */
    kYesPayResponseParamCVVResponse = 35,
    /** @brief Receipt Number
     */
    kYesPayResponseParamReceiptNumber = 36,
    /** @brief Card Expiry Date
     */
    kYesPayResponseParamCardExpiryDate = 37,
    /** @brief Total Amount. Present if different from amount in request (possible if cash back requested during transaction processing). Amount is returned in lowest currency denomination.
     */
    kYesPayResponseParamTotalAmount = 38,
    /** @brief Cash Amount. Present if cash back amount is entered when requested. Same case as above. Amount is returned in lowest currency denomination.
     */
    kYesPayResponseParamCashAmount = 39,
    /** @brief Gratuity Amount
     */
    kYesPayResponseParamGratuityAmount = 40,
    /** @brief SHA1 hash value for the PAN number. The hash will be in ASCII Hex format and a field of 40 alpha-numeric characters.
     */
    kYesPayResponseParamHashedPAN = 59,
    /** @brief Card Issuer Code, this is the 3 digit EMBOSS card issuer code. Should be used to identify the type of the card.
     *  @see YesPayCardIssuerCodeEUR or YesPayCardIssuerCodeUS depending on the country.
     */
    kYesPayResponseParamCIC = 60,
    /** @brief Merchant Subscription Wallet Reference: This will be 20 characters long and alphanumeric field.
     */
    kYesPayResponseParamWalletReference = 61,
    /** @brief 9 Digit Loyalty Code returned in case of US processor and Bank Of America card.
     */
    kYesPayResponseParamBOACardLoyaltyCode = 62,
    /** @brief Account Type in case of Canadian Debit transactions. 
     *  Checking = 1 & Saving = 2
     */
    kYesPayResponseParamCADDebit = 63,
    /** @brief Converted currency name for DCC transactions like HKD – HK Dollar.
     */
    kYesPayResponseParamConvertedCurrencyName = 70,
    /** @brief Amount converted into accepted currency for DCC transactions.
     */
    kYesPayResponseParamConvertedCurrencyAmount = 71,
    /** @brief Currency conversion rate.
     */
    kYesPayResponseParamCurrencyConversion = 72,
    /** @brief Balance Amount for Gift Card. This field will be present only in the response of Gift Card (Sale, Refund and Load) transaction.
     */
    kYesPayResponseParamGiftCardBalance = 73,
    /** @brief Pennies Donation Amount: This field will be present only in the response of Pennies Donation transaction.
     */
    kYesPayResponseParamPenniesDonation = 74,
    /** @brief Retrieval Reference Number: 12 digits numeric number
     This field will be present for all transactions except Void, Completion, Check Card and Check Status transaction.
     */
    kYesPayResponseParamRetrievalReferenceNumber = 80,
    /** @brief Transaction reference. This field will be present in the response of a transaction if field 1 is present in transaction request.
     */
    kYesPayResponseParamTransactionReference = 98,
    /** @brief End of message indicator, values always 0.
     */
    kYesPayResponseParamEndOfMessage = 99
};

/** @brief ￼￼￼￼Transaction Type. Transaction Type indicates the type of financial transaction.
 */
NS_ENUM(NSInteger, YesPayTransactionType) {
    /** @brief Sale (Goods and Services) 
     */
    kYesPayTransactionTypeSale = 0,
    /** @brief PreAuth (FU)
     */
    kYesPayTransactionTypePreAuth = 1,
    /** @brief Sales Completion(FU)
     */
    kYesPayTransactionTypeSalesCompletion = 2,
    /** @brief Cancel Transaction ###(FU)
     */
    kYesPayTransactionTypeCancel = 3,
    /** @brief TaxFree Voucher (VAT Refund Voucher) for Cash Transaction ##(FU)
     */
    kYesPayTransactionTypeTaxFreeVoucher = 8,
    /** @brief Sale with cash disbursement(FU)
     */
    kYesPayTransactionTypeSaleWithCashback = 9,
    /** @brief Void Transaction #(FU)
     */
    kYesPayTransactionTypeVoid = 10,
    /** @brief Create Merchant Subscription Wallet available only for EVT Global version.(FU)
     */
    kYesPayTransactionTypeCreateWallet = 11,
    /** @brief Sale with Merchant Subscription Wallet(FU)
     */
    kYesPayTransactionTypeSaleWithWallet = 12,
    /** @brief Refund with Merchant Subscription Wallet(FU)
     */
    kYesPayTransactionTypeRefundWithWallet = 13,
    /** @brief Returns
     */
    kYesPayTransactionTypeReturn = 20,
    /** @brief CheckCard(FU)
     */
    kYesPayTransactionTypeCheckCard = 30,
    /** @brief CheckStatus(FU)
     */
    kYesPayTransactionTypeCheckStatus = 34,
    /** @brief Last transaction result(FU)
     */
    kYesPayTransactionTypeLastTransactionResult = 35,
    /** @brief X Report(FU)
     */
    kYesPayTransactionTypeXReport = 36,
    /** @brief Z Report(FU)
     */
    kYesPayTransactionTypeZReport = 37,
    /** @brief Sale (Gift Card) (FU)
     */
    kYesPayTransactionTypeGiftCardSale = 51,
    /** @brief Refund (Gift Card)(FU)
     */
    kYesPayTransactionTypeGiftCardRefund = 52,
    /** @brief Load (Gift Card)
     *  Equivalent to a normal refund / return transaction.(FU)
     */
    kYesPayTransactionTypeGiftCardLoad = 53,
    /** @brief Activation (Gift Card)
     *  This will enable activation of a gift card after purchase. 
     *  No activation code will be required if activation is done using Easy V Terminal.(FU)
     */
    kYesPayTransactionTypeGiftCardActivation = 54,
    /** @brief Balance Enquiry (Gift Card)(FU)
     */
    kYesPayTransactionTypeGiftCardBalanceEnquiry = 55,
    /** @brief Purchase (E-Top Up) E-Top Up transaction.(FU)
     */
    kYesPayTransactionTypeETopUpPurchase = 61,
    /** @brief Hide EVT Window(FU)
     */
    kYesPayTransactionTypeHideEVT = 90,
    /** @brief Show EVT Window(FU)
     */
    kYesPayTransactionTypeShowEVT = 91,
    /** @brief Close EVT(FU)
     */
    kYesPayTransactionTypeCloseEVT = 99
};

/** @enum YesPayPOSEntryMode
 *  @brief POS Entry Mode, card payment entry method.
 */
NS_ENUM(NSInteger, YesPayPOSEntryMode) {
    /** @brief Chip transaction.
     */
    kYesPayPOSEntryModeChip = 5,
    /** @brief Stripe transaction.
     */
    kYesPayPOSEntryModeStripe = 2,
    /** @brief Keyed transaction.
     */
    kYesPayPOSEntryModeKeyed = 1,
    /** @brief Card holder not present.
     */
    kYesPayPOSEntryModeCNP = 81
};

/** @enum YesPayResponseTransactionType
 *  @brief Transaction Type indicates the type of financial transaction, represented by the first two digits of ISO 8583:1987 Processing Code.
 */
NS_ENUM(NSInteger, YesPayResponseTransactionType) {
    /** @brief Sale (Goods and Services)
     */
    kYesPayResponseTransactionTypeSale = 0,
    /** @brief Cancel
     */
    kYesPayResponseTransactionTypeCancel = 3,
    /** @brief Sale with cash disbursement
     */
    kYesPayResponseTransactionTypeSaleWithCashback = 9,
    /** @brief Returns
     */
    kYesPayResponseTransactionTypeReturn = 20
};

/** @enum YesPayResponseResult
 *  @brief Transaction Result. Uniquely identifies the transaction response.
 */
NS_ENUM(NSInteger, YesPayResponseResult) {
    /** @brief Approved Online
     */
    kYesPayResponseResultApprovedOnline = 1,
    /** @brief Approved Offline
     */
    kYesPayResponseResultApprovedOffline = 2,
    /** @brief Approved Manual (Referral)
     */
    kYesPayResponseResultApprovedManual = 3,
    /** @brief Declined Online
     */
    kYesPayResponseResultDeclinedOnline = 4,
    /** @brief Declined Offline
     */
    kYesPayResponseResultDeclinedOffline = 5,
    /** @brief Cancelled
     */
    kYesPayResponseResultCancelled = 9,
    /** @brief Capture Card, declined online
     */
    kYesPayResponseResultCaptureCard = 16,
    /** @brief Transaction Aborted
     */
    kYesPayResponseResultTransactionAborted = 19,
    /** @brief Pre sales completed
     */
    kYesPayResponseResultPreSalesCompleted = 20,
    /** @brief Pre sales rejected
     */
    kYesPayResponseResultPreSalesRejected = 21,
    /** @brief Card number not matched
     */
    kYesPayResponseResultCardNumberNotMatched = 22,
    /** @brief Expiry date not matched
     */
    kYesPayResponseResultExpiryDateNotMatched = 23,
    /** @brief Invalid transaction state
     */
    kYesPayResponseResultInvalidTransactionState = 24,
    /** @brief Transaction not valid for requested operation
     */
    kYesPayResponseResultTransactionNotValid = 25,
    /** @brief Invalid PGTR
     */
    kYesPayResponseResultInvalidPGTR = 26,
    /** @brief Invalid Merchant
     */
    kYesPayResponseResultInvalidMerchant = 27,
    /** @brief Invalid Terminal
     */
    kYesPayResponseResultInvalidTerminal = 28,
    /** @brief Merchant status is not valid
     */
    kYesPayResponseResultInvalidMerchantStatus = 29,
    /** @brief Invalid card number
     */
    kYesPayResponseResultInvalidCardNumber = 30,
    /** @brief Expired Card
     */
    kYesPayResponseResultExpiredCard = 31,
    /** @brief Pre valid card
     */
    kYesPayResponseResultPreValidCard = 32,
    /** @brief Invalid issue number
     */
    kYesPayResponseResultInvalidIssueNumber = 33,
    /** @brief Invalid card expiry date
     */
    kYesPayResponseResultInvalidExpiryDate = 34,
    /** @brief Invalid start date
     */
    kYesPayResponseResultInvalidStartDate = 35,
    /** @brief Card not accepted
     */
    kYesPayResponseResultCardNotAccepted = 36,
    /** @brief Transaction not allowed
     */
    kYesPayResponseResultTransactionNotAllowed = 37,
    /** @brief Cash back not allowed
     */
    kYesPayResponseResultCashbackNotAllowed = 38,
    /** @brief Status Busy
     */
    kYesPayResponseResultStatusBusy = 42,
    /** @brief Status Not Busy
     */
    kYesPayResponseResultStatusNotBusy = 43,
    /** @brief AVS details required
     */
    kYesPayResponseResultAVSDetailsRequired = 50,
    /** @brief Card activated (Gift card activation)
     */
    kYesPayResponseResultGiftCardActivation = 60,
    /** @brief Card already activated (Gift card activation)
     */
    kYesPayResponseResultCardAlreadyActivated = 61,
    /** @brief Card not activated (For Gift Card)
     */
    kYesPayResponseResultGiftCardNotActivated = 62,
    /** @brief Hot Listed Card (for Gift Card)
     */
    kYesPayResponseResultHotListedCard = 63,
    /** @brief Insufficient Balance ( for Gift Card)
     */
    kYesPayResponseResultInsufficientBalance = 64,
    /** @brief Invalid Security Code ( for Gift Card)
     */
    kYesPayResponseResultInvalidSecurityCode = 65
};

/** @enum YesPayCardVerificationMethod
 *  @brief Card Verification Method
 */
NS_ENUM(NSInteger, YesPayCardVerificationMethod) {
    /** @brief Signature verified
     */
    kYesPayCardVerificationMethodSignatureVerified = 1,
    /** @brief Pin verified
     */
    kYesPayCardVerificationMethodPINVerified = 2,
    /** @brief Cardholder not present
     */
    kYesPayCardVerificationMethodCNP = 7,
    /** @brief No CVM
     */
    kYesPayCardVerificationMethodNoCVM = 8,
    /** @brief Signature and Pin verified
     */
    kYesPayCardVerificationMethodSignatureAndPINVerified = 10
};

/** @enum YesPayCardIssuerCodeEUR
 *  @brief Card Issuer Code, this is the 3 digit EMBOSS card issuer code. Should be used to identify the type of the card.
 *  Following is the list of issuer codes for UK & Europe.
 */
NS_ENUM(NSInteger, YesPayCardIssuerCodeEUR) {
    /** @brief Visa Debit
     */
    kYesPayCardIssuerCodeEURVisaDebit = 1,
    /** @brief Visa Electron
     */
    kYesPayCardIssuerCodeEURVisaElectron = 2,
    /** @brief Visa Purchasing
     */
    kYesPayCardIssuerCodeEURVisaPurchasing = 3,
    /** @brief Visa
     */
    kYesPayCardIssuerCodeEURVisa = 4,
    /** @brief Mastercard
     */
    kYesPayCardIssuerCodeEURMastercard = 5,
    /** @brief UK Maestro
     */
    kYesPayCardIssuerCodeEURUKMaestro = 6,
    /** @brief Solo
     */
    kYesPayCardIssuerCodeEURSolo = 7,
    /** @brief JCB
     */
    kYesPayCardIssuerCodeEURJCB = 8,
    /** @brief International Maestro
     */
    kYesPayCardIssuerCodeEURInternationalMaestro = 9,
    /** @brief Visa ATM
     */
    kYesPayCardIssuerCodeEURVisaATM = 10,
    /** @brief ARVAL PHH
     */
    kYesPayCardIssuerCodeEURARVALPHH = 11,
    /** @brief AMEX
     */
    kYesPayCardIssuerCodeEURAMEX = 12,
    /** @brief Diners
     */
    kYesPayCardIssuerCodeEURDiners = 13,
    /** @brief Laser
     */
    kYesPayCardIssuerCodeEURLaser = 14,
    /** @brief Duet
     */
    kYesPayCardIssuerCodeEURDuet = 15
};

/** @enum YesPayCardIssuerCodeEUR
 *  @brief Card Issuer Code, this is the 3 digit EMBOSS card issuer code. Should be used to identify the type of the card.
 *  Following is the list of issuer codes for North America.
 */
NS_ENUM(NSInteger, YesPayCardIssuerCodeUS) {
    /** @brief Visa
     */
    kYesPayCardIssuerCodeUSVisa = 1,
    /** @brief Visa Purchasing
     */
    kYesPayCardIssuerCodeUSVisaPurchasing = 2,
    /** @brief Mastercard
     */
    kYesPayCardIssuerCodeUSMastercard = 3,
    /** @brief Mastercard Purchasing
     */
    kYesPayCardIssuerCodeUSMastercardPurchasing = 4,
    /** @brief JCB
     */
    kYesPayCardIssuerCodeUSJCB = 5,
    /** @brief AMEX
     */
    kYesPayCardIssuerCodeUSAMEX = 6,
    /** @brief Diners
     */
    kYesPayCardIssuerCodeUSDiners = 7,
    /** @brief Discover
     */
    kYesPayCardIssuerCodeUSDiscover = 8,
    /** @brief Interac
     */
    kYesPayCardIssuerCodeUSInterac = 9,
    /** @brief US Debit
     */
    kYesPayCardIssuerCodeUSDebit = 10
};

/** @brief Hash a string.
 */
- (NSString *) computeHashForString:(NSString *)input
                            withKey:(NSString *)key
{
    const char *cKey  = [key cStringUsingEncoding:NSUTF8StringEncoding];
    const char *cData = [input cStringUsingEncoding:NSUTF8StringEncoding];
    unsigned char cHMAC[CC_SHA512_DIGEST_LENGTH];
    CCHmac(kCCHmacAlgSHA512, cKey, strlen(cKey), cData, strlen(cData), cHMAC);
    NSData *HMACData = [[NSData alloc] initWithBytes:cHMAC
                                              length:sizeof(cHMAC)];
    const unsigned char *buffer = (const unsigned char *)[HMACData bytes];
    NSString *result = [NSMutableString stringWithCapacity:HMACData.length * 2];
    for (int i = 0; i < HMACData.length; ++i)
        result = [result stringByAppendingFormat:@"%02lX", (unsigned long)buffer[i]];

    return result;
}

- (EFTProviderErrorCode) EFTProviderErrorCodeFromYespayTransactionResult:(NSInteger)transactionResult
{
    EFTProviderErrorCode result = kEFTProviderErrorCodeSuccess;
    
    if (self.status == kEFTProviderStatusNotAvailable) {
        result = kEFTProviderErrorCodeNotInstalled;
    } else if (self.manager.processing) {
        result = kEFTProviderErrorCodeBusy;
    } else if (transactionResult == kYesPayResponseResultApprovedOnline) {
        result = kEFTProviderErrorCodeSuccess;
    } else {
        result = kEFTProviderErrorCodeApplicationError;
    }
    
    return result;
}

- (NSString *) localizedDescriptionForResult:(NSInteger)transactionResult
{
    NSString *result = nil;
    
    switch (transactionResult) {
        case kYesPayResponseResultApprovedOnline:
            result = @"Approved";
            break;
        case kYesPayResponseResultApprovedOffline:
            result = @"Approved";
            break;
        case kYesPayResponseResultApprovedManual:
            result = @"Approved";
            break;
        case kYesPayResponseResultDeclinedOnline:
            result = @"Declined";
            break;
        case kYesPayResponseResultDeclinedOffline:
            result = @"Declined";
            break;
        case kYesPayResponseResultCancelled:
            result = @"Cancelled";
            break;
        case kYesPayResponseResultCaptureCard:
            result = @"Declined";
            break;
        case kYesPayResponseResultTransactionAborted:
            result = @"Transaction Aborted";
            break;
        case kYesPayResponseResultPreSalesCompleted:
            result = @"Pre sales completed";
            break;
        case kYesPayResponseResultPreSalesRejected:
            result = @"Pre sales rejected";
            break;
        case kYesPayResponseResultCardNumberNotMatched:
            result = @"Card number not matched";
            break;
        case kYesPayResponseResultExpiryDateNotMatched:
            result = @"Expiry date not matched";
            break;
        case kYesPayResponseResultInvalidTransactionState:
            result = @"Invalid transaction state";
            break;
        case kYesPayResponseResultTransactionNotValid:
            result = @"Transaction not valid for requested operation";
            break;
        case kYesPayResponseResultInvalidPGTR:
            result = @"Invalid PGTR";
            break;
        case kYesPayResponseResultInvalidMerchant:
            result = @"Invalid Merchant";
            break;
        case kYesPayResponseResultInvalidTerminal:
            result = @"Invalid Terminal";
            break;
        case kYesPayResponseResultInvalidMerchantStatus:
            result = @"Merchant status is not valid";
            break;
        case kYesPayResponseResultInvalidCardNumber:
            result = @"Invalid card number";
            break;
        case kYesPayResponseResultExpiredCard:
            result = @"Expired Card";
            break;
        case kYesPayResponseResultPreValidCard:
            result = @"Pre valid card";
            break;
        case kYesPayResponseResultInvalidIssueNumber:
            result = @"Invalid issue number";
            break;
        case kYesPayResponseResultInvalidExpiryDate:
            result = @"Invalid card expiry dat";
            break;
        case kYesPayResponseResultInvalidStartDate:
            result = @"Invalid start date";
            break;
        case kYesPayResponseResultCardNotAccepted:
            result = @"Card not accepted";
            break;
        case kYesPayResponseResultTransactionNotAllowed:
            result = @"Transaction not allowed";
            break;
        case kYesPayResponseResultCashbackNotAllowed:
            result = @"Cash back not allowed";
            break;
        case kYesPayResponseResultStatusBusy:
            result = @"Status Busy";
            break;
        case kYesPayResponseResultStatusNotBusy:
            result = @"Status Not Busy";
            break;
        case kYesPayResponseResultAVSDetailsRequired:
            result = @"AVS details required";
            break;
        case kYesPayResponseResultGiftCardActivation:
            result = @"Card activated (Gift card activation)";
            break;
        case kYesPayResponseResultCardAlreadyActivated:
            result = @"Card already activated (Gift card activation)";
            break;
        case kYesPayResponseResultGiftCardNotActivated:
            result = @"Card not activated (For Gift Card)";
            break;
        case kYesPayResponseResultHotListedCard:
            result = @"Hot Listed Card (for Gift Card)";
            break;
        case kYesPayResponseResultInsufficientBalance:
            result = @"Insufficient Balance (for Gift Card)";
            break;
        case kYesPayResponseResultInvalidSecurityCode:
            result = @"Invalid Security Code ( for Gift Card)";
            break;
        default:
            result = @"Unknown error";
            break;
    }
    
    return NSLocalizedString(result, nil);
}

#pragma mark - Methods

+ (NSString *) returnURLScheme
{
    //    // hard coded by yespay at the moment
    //    return @"OpenPOSApp";
    NSString *returnURLScheme = nil;
    
    for (NSDictionary *urlType in [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleURLTypes"])
    {
        if ([urlType[@"CFBundleURLName"] isEqualToString:kYESPAY_CONFIG_BUNDLEURLNAME])
        {
            returnURLScheme = urlType[@"CFBundleURLSchemes"][0];
            break;
        }
    }
    return returnURLScheme;
}

+ (NSString *) providerURL
{
    return @"Openwptotal";
}

- (ServerError *) getLastServerError
{
    ServerError *error = nil;
    
    // get error keys
    NSString *resultField = [NSString stringWithFormat:@"%i", kYesPayResponseParamTransactionResult];
    NSString *errorCodeVal = self.lastReplyDict[resultField];
    if (errorCodeVal != nil) {
        NSInteger errorCode = [errorCodeVal integerValue];
        NSString *errorText = [self localizedDescriptionForResult:errorCode];
        NSInteger EFTErrorCode = [self EFTProviderErrorCodeFromYespayTransactionResult:errorCode];
        
        // if there was an error
        if (EFTErrorCode != kEFTProviderErrorCodeSuccess) {
            // build the error
            ServerErrorMessage *message = [[ServerErrorMessage alloc] init];
            message.code = [NSNumber numberWithInteger:errorCode];
            message.value = errorText;
            error = [[ServerError alloc] init];
            error.messages = [NSArray arrayWithObject:message];
        }
    }
    
    // return
    return error;
}

- (BasketTender *) updatePaymentFromDictionary:(NSDictionary *)options
{    
    // create the payment
    YespayPayment *yespayPayment = [[YespayPayment alloc] init];
    yespayPayment.tender = self.manager.currentPayment.tender;
    yespayPayment.basketTenderId = self.manager.currentPayment.basketTenderId;
    yespayPayment.basket = self.manager.currentPayment.basket;
    yespayPayment.tenderType = self.manager.currentPayment.tender.tenderType;
    
    if (yespayPayment && options) {
        for (int i = 0; i < [options allKeys].count; i++) {
            NSString *name = [[options allKeys] objectAtIndex:i];
            switch (name.integerValue) {
                case kYesPayResponseParamAID:
                    yespayPayment.aid = [options objectForKey:name];
                    break;
                case kYesPayResponseParamAuthCode:
                    yespayPayment.authCode = [options objectForKey:name];
                    break;
                case kYesPayResponseParamBatchNumber:
                    yespayPayment.batchNumber = [options objectForKey:name];
                    break;
                case kYesPayResponseParamBOACardLoyaltyCode :
                    yespayPayment.loyaltyCode = [options objectForKey:name];
                    break;
                case kYesPayResponseParamCADDebit:
                    yespayPayment.accountType = [options objectForKey:name];
                    break;
                case kYesPayResponseParamCardExpiryDate:
                    yespayPayment.cardExpiryDate = [options objectForKey:name];
                    break;
                case kYesPayResponseParamCardholderName:
                    yespayPayment.emvCardHolderName = [options objectForKey:name];
                    break;
                case kYesPayResponseParamCardholderNameExtended:
                    yespayPayment.emvCardHolderNameExtended = [options objectForKey:name];
                    break;
                case kYesPayResponseParamCardVerificationMethod:
                    yespayPayment.cvm = [options objectForKey:name];
                    break;
                case kYesPayResponseParamCashAmount:
                    yespayPayment.cashAmount = [options objectForKey:name];
                    break;
                case kYesPayResponseParamCIC:
                    yespayPayment.cic = [options objectForKey:name];
                    break;
                case kYesPayResponseParamConvertedCurrencyAmount:
                    yespayPayment.convertedDCCAmount = [options objectForKey:name];
                    break;
                case kYesPayResponseParamConvertedCurrencyName:
                    yespayPayment.currencyName = [options objectForKey:name];
                    break;
                case kYesPayResponseParamCurrencyConversion:
                    yespayPayment.currencyConversionRate = [options objectForKey:name];
                    break;
                case kYesPayResponseParamCustomerDeclaration:
                    yespayPayment.customerDeclaration = [options objectForKey:name];
                    break;
                case kYesPayResponseParamCVVResponse:
                    yespayPayment.cvvResponse = [options objectForKey:name];
                    break;
                case kYesPayResponseParamEffectiveDate:
                    yespayPayment.emvEffectiveDate = [options objectForKey:name];
                    break;
                case kYesPayResponseParamEFTSequenceNumber:
                    yespayPayment.eftSequenceNumber = [options objectForKey:name];
                    break;
                case kYesPayResponseParamEntryMethod:
                    yespayPayment.pem = [options objectForKey:name];
                    break;
                case kYesPayResponseParamGiftCardBalance:
                    yespayPayment.gcBalance = [options objectForKey:name];
                    break;
                case kYesPayResponseParamGratuityAmount:
                    yespayPayment.gratuityAmount = [options objectForKey:name];
                    break;
                case kYesPayResponseParamHashedPAN:
                    yespayPayment.panSHA1 = [options objectForKey:name];
                    break;
                case kYesPayResponseParamIssueNumberOrPAN:
                    yespayPayment.panOrIssue = [options objectForKey:name];
                    break;
                case kYesPayResponseParamLabel:
                    yespayPayment.emvLabel = [options objectForKey:name];
                    break;
                case kYesPayResponseParamMerchantAddress:
                    yespayPayment.merchantAddress = [options objectForKey:name];
                    break;
                case kYesPayResponseParamMerchantID:
                    yespayPayment.merchantId = [options objectForKey:name];
                    break;
                case kYesPayResponseParamMerchantName:
                    yespayPayment.merchantName = [options objectForKey:name];
                    break;
                case kYesPayResponseParamPAN:
                    yespayPayment.maskedPAN = [options objectForKey:name];
                    break;
                case kYesPayResponseParamPenniesDonation:
                    yespayPayment.pdAmount = [options objectForKey:name];
                    break;
                case kYesPayResponseParamPGTR:
                    yespayPayment.pgtr = [options objectForKey:name];
                    break;
                case kYesPayResponseParamReceiptNumber:
                    yespayPayment.receiptNumber = [options objectForKey:name];
                    break;
                case kYesPayResponseParamReferralTelephoneNumber1:
                    yespayPayment.refPhone1 = [options objectForKey:name];
                    break;
                case kYesPayResponseParamReferralTelephoneNumber2:
                    yespayPayment.refPhone2 = [options objectForKey:name];
                    break;
                case kYesPayResponseParamRetentionReminder:
                    yespayPayment.retentionReminder = [options objectForKey:name];
                    break;
                case kYesPayResponseParamRetrievalReferenceNumber:
                    yespayPayment.rrn = [options objectForKey:name];
                    break;
                case kYesPayResponseParamStartDate:
                    yespayPayment.startDate = [options objectForKey:name];
                    break;
                case kYesPayResponseParamTerminalID:
                    yespayPayment.terminalId = [options objectForKey:name];
                    break;
                case kYesPayResponseParamTotalAmount:
                    yespayPayment.totalAmount = [options objectForKey:name];
                    break;
                case kYesPayResponseParamTotalRefundAmount:
                    yespayPayment.totalRefundAmount = [options objectForKey:name];
                    break;
                case kYesPayResponseParamTotalRefundCount:
                    yespayPayment.refundCounts = [options objectForKey:name];
                    break;
                case kYesPayResponseParamTotalSaleAmount:
                    yespayPayment.totalSaleAmount = [options objectForKey:name];
                    break;
                case kYesPayResponseParamTotalSaleCount:
                    yespayPayment.salesCount = [options objectForKey:name];
                    break;
                case kYesPayResponseParamTransactionDate:
                    yespayPayment.transDate = [options objectForKey:name];
                    break;
                case kYesPayResponseParamTransactionReference:
                    yespayPayment.transReference = [options objectForKey:name];
                    break;
                case kYesPayResponseParamTransactionResult:
                    yespayPayment.transResult = [options objectForKey:name];
                    break;
                case kYesPayResponseParamTransactionTime:
                    yespayPayment.transTime = [options objectForKey:name];
                    break;
                case kYesPayResponseParamTransactionType:
                    yespayPayment.transType = [options objectForKey:name];
                    break;
                case kYesPayResponseParamTSI:
                    yespayPayment.transStatusInfo = [options objectForKey:name];
                    break;
                case kYesPayResponseParamTVR:
                    yespayPayment.transVerificationResult = [options objectForKey:name];
                    break;
                case kYesPayResponseParamWalletReference:
                    yespayPayment.mswr = [options objectForKey:name];
                    break;
                default:
                    break;
            }
        }
    }
    
    return yespayPayment;
}

- (NSString *) buildOptionStringFromDict:(NSDictionary *)options
{
    NSString *result = nil;
    
    if (options) {
        NSMutableString *params = [NSMutableString string];
        for (int i = 0; i < 99; i++) {
            NSString *name = [NSString stringWithFormat:@"%i", i];
            NSString *value = [options objectForKey:name];
            if (value != nil) {
                NSString *param = [NSString stringWithFormat:@"%i=%@\n", i, value];
                [params appendString:param];
            }
        }
        // add the return URL
        NSString *returnURL = [[self class] returnURLScheme];
        NSString *param = [NSString stringWithFormat:@"%i=%@://%@\n", kYesPayRequestParamCallBackURL, returnURL, kYESPAY_CONFIG_RESPONDURL];
        [params appendString:param];
        // add the "salt"
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        formatter.dateFormat = @"ddMMyyyyHHmmss";
        NSString *timeStamp = [formatter stringFromDate:[NSDate date]];
        NSString *salt = [[NSUUID UUID] UUIDString];
        param = [NSString stringWithFormat:@"%i=%@\n", kYesPayRequestParamSalt, salt];
        [params appendString:param];
        // add the hash
        NSString *uuid = nil;
        Device *EFTDevice = [[BasketController sharedInstance] deviceForSettingWithName:kOCGEFTManagerConfiguredPaymentProviderKey];
        if (EFTDevice) {
            uuid = EFTDevice.propertyDict[@"WorldPayClientID"];
        }
        NSString *hashInput = [NSString stringWithFormat:@"%@%@%@", uuid, salt, timeStamp];
        NSString *hashOutput = [self computeHashForString:hashInput
                                                  withKey:uuid];
        param = [NSString stringWithFormat:@"%i=%@\n", kYesPayRequestParamHash, hashOutput];
        [params appendString:param];
        // add the timestamp
        param = [NSString stringWithFormat:@"%i=%@\n", kYesPayRequestParamTimestamp, timeStamp];
        [params appendString:param];
        // add the terminator
        result = [NSString stringWithFormat:@"%@%@", params, @"99=0"];
    }
    
    return result;
}

- (NSDictionary *) buildDictionaryFromURL:(NSURL *)url
{
    NSDictionary *result = nil;
    DebugLog(@"Parsing YesPay response");
    
    NSString *URLString = [url absoluteString];
    DebugLog(@"Received URL: %@", URLString);
    NSString *decodedURLString =
    [URLString stringByReplacingPercentEscapesUsingEncoding:NSASCIIStringEncoding];
    
    // split params and the URL address
    NSArray *URLComponents = [decodedURLString componentsSeparatedByString:@"?"];
    if ([URLComponents count] > 1) {
        NSString *addressString = [URLComponents objectAtIndex:0];
        NSString *paramString = [URLComponents objectAtIndex:1];
        // make sure data is from YesPay
        if ([addressString hasSuffix:kYESPAY_CONFIG_RESPONDURL]) {
            // process params
            paramString = [paramString stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
            // chop off the param name
            NSRange queryRange = [paramString rangeOfString:@"q="];
            if (queryRange.location != NSNotFound) {
                paramString = [paramString substringFromIndex:queryRange.location + queryRange.length];
            }
            NSArray *paramArray = [paramString componentsSeparatedByString:@"\n"];
            if ([paramArray count] > 0) {
                DebugLog(@"Processing %i name/value pairs", [paramArray count]);
                NSMutableDictionary *nvpDict = [[NSMutableDictionary alloc] initWithCapacity:[paramArray count]];
                for (NSString *nvpString in paramArray) {
                    // split into name and value
                    // get the pos of the first '='
                    NSRange equalsRange = [nvpString rangeOfString:@"="];
                    if (equalsRange.location != NSNotFound) {
                        NSString *name = [nvpString substringToIndex:equalsRange.location];
                        NSString *value = [nvpString substringFromIndex:equalsRange.location + 1];
                        [nvpDict setObject:value
                                    forKey:name];
                    } else {
                        DebugLog(@"Couldn't parse name/value pair from data: %@", nvpString);
                    }
                }
                result = nvpDict;
            } else {
                DebugLog(@"No name/value pairs to process");
                result = nil;
            }
        } else {
            DebugLog(@"Response isn't from YesPay!");
            result = nil;
        }
    } else {
        DebugLog(@"URL didn't contain complete reply");
        result = nil;
    }
    
    return result;
}

- (BOOL) isRetryRequest:(NSDictionary *)requestDict
{
    return NO;
}

- (EFTProviderErrorCode) checkForErrorInResponse:(NSDictionary *)params
{
    NSInteger result = kEFTProviderErrorCodeApplicationError;
    
    if (params) {
        NSString *resultField = [NSString stringWithFormat:@"%i", kYesPayResponseParamAuthCode];
        NSString *errorCodeVal = params[resultField];
        if (errorCodeVal != nil) {
            result = kEFTProviderErrorCodeSuccess;
        }
    }
    
    // error is any error code other than 0
    if (result != kEFTProviderErrorCodeSuccess) {
        DebugLog(@"Yespay request failed with error: %i", result);
    }
    
    return result;
}

- (EFTPaymentCompleteReason) updateStatusFromSuccessfulResponse:(NSDictionary *)response
{
    EFTPaymentCompleteReason result = kEFTPaymentCompleteReasonError;
    
    NSString *resultField = [NSString stringWithFormat:@"%i", kYesPayResponseParamTransactionResult];
    NSNumber *resultValue = response[resultField];
    switch (resultValue.integerValue) {
        case kYesPayResponseResultApprovedOnline:
        case kYesPayResponseResultApprovedOffline:
        case kYesPayResponseResultApprovedManual:
            result = kEFTPaymentCompleteReasonApproved;
            break;
        case kYesPayResponseResultDeclinedOnline:
        case kYesPayResponseResultDeclinedOffline:
        case kYesPayResponseResultCaptureCard:
            result = kEFTPaymentCompleteReasonDeclined;
            break;
        case kYesPayResponseResultCancelled:
        case kYesPayResponseResultTransactionAborted:
            result = kEFTPaymentCompleteReasonCancelled;
            break;
        default:
            result = kEFTPaymentCompleteReasonError;
            break;
    }
    
    return result;
}

- (void) updateStatusFromFailedResponse:(NSDictionary *)response
                          withErrorCode:(EFTProviderErrorCode)errorCode
{
    // there was an error
}

- (void) cardSaleWithTender:(Tender *)tender
                     amount:(NSNumber *)amount
            forBasketWithID:(NSString *)basketId
          withCustomerEmail:(NSString *)customerEmail
                   complete:(EFTProviderCompleteBlock)complete
{
    [super cardSaleWithTender:tender
                       amount:amount
              forBasketWithID:basketId
            withCustomerEmail:customerEmail
                     complete:^(EFTProviderErrorCode resultCode, NSError *error, BOOL suppressError) {
                         // init
                         EFTProviderErrorCode result = resultCode;
                         
                         if (result == kEFTProviderErrorCodeSuccess) {
                             // build request
                             NSMutableDictionary *options = [[NSMutableDictionary alloc] init];
                             
                             // required
                             [options setObject:basketId
                                         forKey:[NSString stringWithFormat:@"%i", kYesPayRequestParamTransactionReference]];
                             [options setObject:[NSString stringWithFormat:@"%i", kYesPayTransactionTypeSale]
                                         forKey:[NSString stringWithFormat:@"%i", kYesPayRequestParamTransactionType]];
                             [options setObject:[NSString stringWithFormat:@"%010.2f", amount.doubleValue]
                                         forKey:[NSString stringWithFormat:@"%i", kYesPayRequestParamTransactionAmount]];
                             // optional
                             
                             
                             result = [self sendRequestWithParams:options];
                         }
                         
                         // done
                         complete(result, error, NO);
                     }];
}

- (void) cardRefundWithTender:(Tender *)tender
                   withAmount:(NSNumber *)amount
              forBasketWithID:(NSString *)basketId
            withCustomerEmail:(NSString *)customerEmail
                     complete:(EFTProviderCompleteBlock)complete
{
    UIAlertView *alert =
    [[UIAlertView alloc] initWithTitle:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁆󠁮󠁪󠁈󠁩󠁰󠀫󠁷󠁅󠁡󠁪󠁪󠁧󠁁󠀯󠀫󠁳󠁒󠁲󠀫󠁪󠁵󠁮󠁢󠁑󠁆󠁁󠁿*/ @"Card refund error", nil)
                               message:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁭󠁂󠁇󠁈󠁬󠁯󠁧󠁖󠁋󠁥󠁦󠁈󠁳󠁲󠁕󠀷󠁈󠁏󠁂󠁐󠁉󠁥󠁦󠁱󠁆󠁎󠁣󠁿*/ @"Not supported", nil)
                              delegate:nil
                     cancelButtonTitle:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁄󠁴󠁍󠁉󠁣󠀫󠁺󠁔󠁗󠁦󠁹󠁡󠁯󠀷󠁨󠁔󠁩󠀸󠁯󠀫󠁨󠁘󠀹󠁱󠁌󠀶󠀸󠁿*/ @"OK", nil)
                     otherButtonTitles:nil];
    [alert show];
    complete(kEFTProviderErrorCodeApplicationError, nil, NO);
    // not currently supported by yespay
//    // init
//    *error = nil;
//    enum EFTProviderErrorCode result = [super cardRefundWithTender:tender
//                                                        withAmount:amount
//                                                   forBasketWithID:basketId
//                                                 withCustomerEmail:customerEmail
//                                                             error:error];
//    
//    if (result == kEFTProviderErrorCodeSuccess) {
//        // build request
//        NSMutableDictionary *options = [[NSMutableDictionary alloc] init];
//        
//        // required
//        [options setObject:basketId
//                    forKey:[NSString stringWithFormat:@"%i", kYesPayRequestParamTransactionReference]];
//        [options setObject:[NSString stringWithFormat:@"%i", kYesPayTransactionTypeReturn]
//                    forKey:[NSString stringWithFormat:@"%i", kYesPayRequestParamTransactionType]];
//        [options setObject:[NSString stringWithFormat:@"%010.2f", amount.doubleValue]
//                    forKey:[NSString stringWithFormat:@"%i", kYesPayRequestParamTransactionAmount]];
//        // optional
//        
//        
//        result = [self sendRequestWithParams:options];
//        if (result != kEFTProviderErrorCodeSuccess) {
//            // there was a problem, create and return an error
//            *error = [NSError errorWithDomain:kEFTProviderErrorDomain
//                                         code:result
//                                     userInfo:nil];
//            self.processing = NO;
//        }
//    }
//    
//    // done
//    return result;
}

- (BOOL) isProviderURL:(NSURL *)url
{
    BOOL result = NO;
    
    // log it
    [self logPaymentURL:url
               incoming:YES];
    
    DebugLog(@"Validating URL: %@", url);
    
    NSString *URLString = [url absoluteString];
    NSString *decodedURLString =
    [URLString stringByReplacingPercentEscapesUsingEncoding:NSASCIIStringEncoding];
    
    // split params and the URL address
    NSArray *URLComponents = [decodedURLString componentsSeparatedByString:@"?"];
    if ([URLComponents count] > 1) {
        NSString *addressString = [URLComponents objectAtIndex:0];
        // make sure data is from Payware
        if ([addressString hasSuffix:kYESPAY_CONFIG_RESPONDURL]) {
            DebugLog(@"URL is a valid yespay URL");
            result = YES;
        }
    }
    
    return result;
}

- (BasketTender *) updatePaymentFromTransaction
{
    NSURL *url = [NSURL URLWithString:self.manager.currentPayment.transaction];
    NSDictionary *params = [self buildDictionaryFromURL:url];
    return [self updatePaymentFromDictionary:params];
}

@end
