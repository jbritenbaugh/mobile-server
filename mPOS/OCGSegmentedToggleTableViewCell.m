//
//  OCGSegmentedToggleTableViewCell.m
//  mPOS
//
//  Created by Antonio Strijdom on 15/08/2013.
//  Copyright (c) 2013 Clarity. All rights reserved.
//

#import "OCGSegmentedToggleTableViewCell.h"

@interface OCGSegmentedToggleTableViewCell ()
@property (nonatomic, strong) UISegmentedControl *segments;
- (IBAction) segmentSelectionDidChange:(id)sender;
@end

@implementation OCGSegmentedToggleTableViewCell

#pragma mark - Properties

@synthesize selection;
- (NSInteger) selection
{
    NSInteger result = NSIntegerMin;
    
    if (self.segments != nil) {
        result = self.segments.selectedSegmentIndex;
    }
    
    return result;
}
- (void) setSelection:(NSInteger)value
{
    if (self.segments != nil) {
        self.segments.selectedSegmentIndex = value;
    }
}

#pragma mark - Init

- (id) initWithStyle:(UITableViewCellStyle)style
     reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style
                reuseIdentifier:reuseIdentifier];
    if (self) {
        self.accessoryType = UITableViewCellAccessoryNone;
        self.selectionStyle = UITableViewCellEditingStyleNone;
        self.segments = [[UISegmentedControl alloc] init];
        self.segments.translatesAutoresizingMaskIntoConstraints = NO;
        [self.segments addTarget:self
                          action:@selector(segmentSelectionDidChange:)
                forControlEvents:UIControlEventValueChanged];
        [self.contentView addConstraint:
         [NSLayoutConstraint constraintWithItem:self.segments
                                      attribute:NSLayoutAttributeCenterY
                                      relatedBy:NSLayoutRelationEqual
                                         toItem:self.contentView
                                      attribute:NSLayoutAttributeCenterY
                                     multiplier:1.0
                                       constant:0.0]];
        [self.contentView addConstraint:
         [NSLayoutConstraint constraintWithItem:self.segments
                                      attribute:NSLayoutAttributeRight
                                      relatedBy:NSLayoutRelationEqual
                                         toItem:self.contentView
                                      attribute:NSLayoutAttributeRight
                                     multiplier:1.0
                                       constant:-10.0]];
        [self.contentView addSubview:self.segments];
    }
    return self;
}

#pragma mark - Methods

- (void) setOptions:(NSArray *)options
{
    // update the segments
    [self.segments removeAllSegments];
    for (int i = options.count - 1; i >= 0; i--) {
        NSString *option = options[i];
        [self.segments insertSegmentWithTitle:option
                                      atIndex:0
                                     animated:NO];
    }
    [self setNeedsLayout];
}

#pragma mark - Actions

- (IBAction) segmentSelectionDidChange:(id)sender
{
    if (self.delegate != nil) {
        if ([self.delegate respondsToSelector:@selector(segmentTableViewCell:didSwitchTo:)]) {
            [self.delegate segmentTableViewCell:self
                                    didSwitchTo:self.segments.selectedSegmentIndex];
        }
    }
}

@end
