//
//  NSRegularExpression+OCGExtensions.h
//  mPOS
//
//  Created by John Scott on 07/11/2014.
//  Copyright (c) 2014 Clarity. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSRegularExpression (OCGExtensions)

/*
 Same as -[NSRegularExpression matchesInString:options:range:] but returns dictionaries of results instead of NSTextCheckingResults.
 
 Note that only found (ie not NSNotFound) ranges are added. Group names can be used muliple times, but only the last foundrange will be returned.
 */

- (NSArray * /* NSDictionary's */)OCGExtensions_matchesInString:(NSString *)string groupNames:(NSArray*)groupNames;

@end
