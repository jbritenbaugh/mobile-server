//
//  BasketDiscountViewController.m
//  mPOS
//
//  Created by Antonio Strijdom on 02/12/2013.
//  Copyright (c) 2013 Clarity. All rights reserved.
//

#import "BasketDiscountViewController.h"
#import "CRSSkinning.h"
#import "OCGTableViewCell.h"
#import "OCGTextField.h"
#import "OCGSegmentedToggleTableViewCell.h"
#import "BasketController.h"
#import "NSArray+OCGIndexPath.h"
#import "OCGNumberTextFieldValidator.h"
#import "OCGPOSKeyboardToolbar.h"
#import "OCGSelectViewController.h"
#import "RESTController.h"
#import "OCGKeyboardTypeButtonItem.h"

#define kDiscountUnitPercentage         @"PRC"
#define kDiscountUnitFixed              @"FIX"

NS_ENUM(NSInteger, DiscountUnitType) {
    DiscountUnitTypeUnknown = -1,
    DiscountUnitTypeFixed = 0,
    DiscountUnitTypePercent = 1
};

@interface BasketDiscountViewController () <OCGSegmentedToggleTableViewCellDelegate, UITextFieldDelegate>
@property (nonatomic, strong) NSArray *layout;
@property (nonatomic, strong) OCGTableViewCell *valueCell;
@property (nonatomic, strong) OCGNumberTextFieldValidator *validator;
@property (nonatomic, strong) UIBarButtonItem *addButton;
@property (nonatomic, strong) NSArray *availableCodes;
@property (nonatomic, strong) NSArray *availableReasons;
@property (nonatomic, strong) DiscountCode *selectedDiscountCode;
@property (nonatomic, strong) DiscountReason *selectedDiscountReason;
@property (nonatomic, assign) enum DiscountUnitType selectedDiscountUnit;
@property (nonatomic, assign) BOOL hasAddedDiscount;
- (void) handleDiscountChangeNotification:(NSNotification *)note;
- (void) handleBasketItemChangeNotification:(NSNotification *)note;
- (void) handleGeneralErrorNotification:(NSNotification *)note;
- (void) handleCustomerAffiliationAddedNotification:(NSNotification *)note;
- (void) loadCodes;
- (void) loadReasons;
- (void) updateViewState;
- (BOOL) validateDiscountSettingsWithAlert:(BOOL)showAlert;
- (IBAction) addButtonTapped:(id)sender;
@end

@implementation BasketDiscountViewController

#pragma mark - Private

static NSString *kDiscountCodeTableCellReuseId = @"kDiscountCodeTableCellReuseId";
static NSString *kDiscountReasonTableCellReuseId = @"kDiscountReasonTableCellReuseId";
static NSString *kDiscountUnitTableCellReuseId = @"kDiscountUnitTableCellReuseId";
const NSInteger kDiscountCodeTableSection = 0;
const NSInteger kDiscountReasonTableSection = 1;
const NSInteger kDiscountUnitTableSection = 2;
const NSInteger kDiscountValueTableSection = 3;

- (void) handleDiscountChangeNotification:(NSNotification *)note
{
    if (self.hasAddedDiscount) {
        self.hasAddedDiscount = NO;
        [self dismissSelf];
    }
}

- (void) handleBasketItemChangeNotification:(NSNotification *)note
{
    if (self.hasAddedDiscount) {
        self.hasAddedDiscount = NO;
        [self dismissSelf];
    }
}

- (void) handleGeneralErrorNotification:(NSNotification *)note
{
    self.hasAddedDiscount = NO;
}

- (void) handleCustomerAffiliationAddedNotification:(NSNotification *)note
{
    // tap the add button
    [self addButtonTapped:self];
}

- (void) loadCodes
{
    NSString *discountType = nil;
    
    switch (self.discountType) {
        case DiscountTypeBasketItemDiscount:
            discountType = @"ITEM";
            break;
        case DiscountTypeBasketDiscount:
            discountType = @"SALESBASKET";
            break;
        default:
            break;
    }
    
    if (discountType != nil) {
        self.availableCodes = [[BasketController sharedInstance] discountCodesWithType:discountType];
    } else {
        self.availableCodes = nil;
    }
}

- (void) loadReasons
{
    NSString *groupName = nil;
    
    switch (self.discountType) {
        case DiscountTypeBasketDiscount:
            groupName = @"Sale discount reasons";
            break;
        case DiscountTypeBasketItemDiscount:
            groupName = @"Item discount reasons";
            break;
        case DiscountTypeBasketItemPriceOverride:
            groupName = @"Item price override reasons";
            break;
        default:
            break;
    }
    
    if (groupName != nil) {
        self.availableReasons = [[BasketController sharedInstance] discountReasonsForGroup:groupName];
    } else {
        self.availableReasons = nil;
    }
}

- (void) updateViewState
{
    // update the view layout based on current state
    NSMutableArray *newLayout = [NSMutableArray array];
    // if we doing a price override
    if (self.discountType == DiscountTypeBasketItemPriceOverride) {
        // always show the reason and value fields
        if (self.availableReasons.count > 0) {
            [newLayout addObject:@(kDiscountReasonTableSection)];
        }
        [newLayout addObject:@(kDiscountValueTableSection)];
    } else {
        // always show the code field
        [newLayout addObject:@(kDiscountCodeTableSection)];
        // if a code has been selected
        if (self.selectedDiscountCode != nil) {
            // show the reason field (if required)
            if (self.selectedDiscountCode.isReasonRequired) {
                [newLayout addObject:@(kDiscountReasonTableSection)];
            }
            // show the unit field (if not set in the code)
            if (self.selectedDiscountCode.discountValueType == nil) {
                [newLayout addObject:@(kDiscountUnitTableSection)];
            }
            // if the user can enter a value
            if (self.selectedDiscountCode.isDiscountValueRequired) {
                // show the value field
                [newLayout addObject:@(kDiscountValueTableSection)];
            }
        }
    }
    // reload the table
    self.layout = @[newLayout];
    [self.tableView reloadData];
    // set the add button state
    self.addButton.enabled = [self validateDiscountSettingsWithAlert:NO];
}

- (BOOL) validateDiscountSettingsWithAlert:(BOOL)showAlert
{
    // check if we've got a code selected
    if ((self.selectedDiscountCode == nil) &&
        (self.discountType != DiscountTypeBasketItemPriceOverride)) {
        if (showAlert) {
            UIAlertView *alert =
            [[UIAlertView alloc] initWithTitle:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁆󠁅󠁈󠀲󠁤󠀫󠁤󠁅󠁑󠁬󠁨󠁺󠁚󠁂󠁩󠀴󠁘󠁳󠁥󠁸󠀶󠁚󠁷󠁵󠁕󠀯󠁷󠁿*/ @"Discount", nil)
                                       message:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠀯󠁫󠀫󠁁󠀱󠀳󠁴󠁶󠁏󠁌󠁇󠁡󠁰󠀷󠁴󠁐󠁯󠁏󠁩󠁣󠁡󠁡󠁫󠁒󠁣󠁓󠁅󠁿*/ @"Please pick a code for the discount from the list.", nil)
                                      delegate:nil
                             cancelButtonTitle:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁄󠁴󠁍󠁉󠁣󠀫󠁺󠁔󠁗󠁦󠁹󠁡󠁯󠀷󠁨󠁔󠁩󠀸󠁯󠀫󠁨󠁘󠀹󠁱󠁌󠀶󠀸󠁿*/ @"OK", nil)
                             otherButtonTitles:nil];
            [alert show];
        }
        return NO;
    }
    
    // check if we've got a reason selected
    if (((self.discountType == DiscountTypeBasketItemPriceOverride) ||
         (self.selectedDiscountCode.isReasonRequired)) &&
        ([self.availableReasons count] > 0) &&
        (self.selectedDiscountReason == nil)) {
        if (showAlert) {
            UIAlertView *alert = nil;
            if (self.discountType == DiscountTypeBasketItemPriceOverride) {
                alert =
                [[UIAlertView alloc] initWithTitle:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁚󠀷󠁴󠁯󠁳󠁗󠁧󠀫󠁰󠁑󠁃󠁫󠀴󠁣󠀷󠀰󠁐󠁓󠁹󠁗󠁊󠁵󠀵󠁺󠁋󠁍󠁕󠁿*/ @"Adjust Price", nil)
                                           message:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁴󠁋󠁇󠁣󠁑󠀸󠁲󠁆󠁸󠁂󠁷󠁖󠁮󠀱󠁣󠁎󠁸󠁮󠁚󠁑󠁰󠁤󠁑󠀱󠁷󠁲󠁅󠁿*/ @"Please pick a reason for the price adjustment from the list.", nil)
                                          delegate:nil
                                 cancelButtonTitle:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁄󠁴󠁍󠁉󠁣󠀫󠁺󠁔󠁗󠁦󠁹󠁡󠁯󠀷󠁨󠁔󠁩󠀸󠁯󠀫󠁨󠁘󠀹󠁱󠁌󠀶󠀸󠁿*/ @"OK", nil)
                                 otherButtonTitles:nil];
            } else {
                alert =
                [[UIAlertView alloc] initWithTitle:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁆󠁅󠁈󠀲󠁤󠀫󠁤󠁅󠁑󠁬󠁨󠁺󠁚󠁂󠁩󠀴󠁘󠁳󠁥󠁸󠀶󠁚󠁷󠁵󠁕󠀯󠁷󠁿*/ @"Discount", nil)
                                           message:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁋󠀹󠁥󠁪󠀳󠁯󠀫󠁇󠀳󠁩󠀰󠁢󠁺󠁐󠁕󠀳󠀯󠁑󠁩󠁍󠀸󠁨󠁙󠁁󠁷󠁚󠁍󠁿*/ @"Please pick a reason for the discount from the list.", nil)
                                          delegate:nil
                                 cancelButtonTitle:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁄󠁴󠁍󠁉󠁣󠀫󠁺󠁔󠁗󠁦󠁹󠁡󠁯󠀷󠁨󠁔󠁩󠀸󠁯󠀫󠁨󠁘󠀹󠁱󠁌󠀶󠀸󠁿*/ @"OK", nil)
                                 otherButtonTitles:nil];
            }
            [alert show];
        }
        return NO;
    }
    
    // check if we've got a discount value entered
    if (((self.discountType == DiscountTypeBasketItemPriceOverride) ||
         self.selectedDiscountCode.isDiscountValueRequired) &&
        ([[self.validator numberFromString:self.valueCell.detailTextField.text] doubleValue] < 0.001)) {
        if (showAlert) {
            UIAlertView *alert = nil;
            if (self.discountType == DiscountTypeBasketItemPriceOverride) {
                alert =
                [[UIAlertView alloc] initWithTitle:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠀶󠁭󠁅󠁨󠁫󠀳󠁋󠁃󠁚󠁌󠀸󠀱󠁨󠁫󠀫󠁵󠁷󠁷󠁕󠁐󠁄󠀳󠁕󠁹󠁧󠁬󠁙󠁿*/ @"Adjust price", nil)
                                           message:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁰󠁂󠁬󠁚󠁨󠁯󠀵󠀯󠁉󠁓󠁚󠁗󠀸󠁊󠀫󠁵󠁋󠁵󠁬󠁵󠁡󠀲󠁢󠁖󠁈󠀱󠁉󠁿*/ @"Please enter a new price.", nil)
                                          delegate:nil
                                 cancelButtonTitle:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁄󠁴󠁍󠁉󠁣󠀫󠁺󠁔󠁗󠁦󠁹󠁡󠁯󠀷󠁨󠁔󠁩󠀸󠁯󠀫󠁨󠁘󠀹󠁱󠁌󠀶󠀸󠁿*/ @"OK", nil)
                                 otherButtonTitles:nil];
            } else {
                alert =
                [[UIAlertView alloc] initWithTitle:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁆󠁅󠁈󠀲󠁤󠀫󠁤󠁅󠁑󠁬󠁨󠁺󠁚󠁂󠁩󠀴󠁘󠁳󠁥󠁸󠀶󠁚󠁷󠁵󠁕󠀯󠁷󠁿*/ @"Discount", nil)
                                           message:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁮󠁔󠁩󠁃󠀹󠁵󠁅󠁡󠁚󠀳󠁨󠁃󠁹󠁴󠁍󠁈󠁴󠁋󠁺󠀲󠀸󠁰󠁬󠁒󠁋󠁋󠀴󠁿*/ @"Please enter a discount value.", nil)
                                          delegate:nil
                                 cancelButtonTitle:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁄󠁴󠁍󠁉󠁣󠀫󠁺󠁔󠁗󠁦󠁹󠁡󠁯󠀷󠁨󠁔󠁩󠀸󠁯󠀫󠁨󠁘󠀹󠁱󠁌󠀶󠀸󠁿*/ @"OK", nil)
                                 otherButtonTitles:nil];
            }
            [alert show];
        }
        return NO;
    }
    
    return YES;
}

- (void) updateBasketItemWithDiscount
{
    if ([self validateDiscountSettingsWithAlert:NO]) {
        // parse discount unit
        NSString *discountUnit = nil;
        switch (self.selectedDiscountUnit) {
            case DiscountUnitTypeFixed:
                discountUnit = kDiscountUnitFixed;
                break;
            case DiscountUnitTypePercent:
                discountUnit = kDiscountUnitPercentage;
                break;
            default:
                break;
        }
        
        // make sure we only pass a value if required
        NSString *discountValue = nil;
        if (self.selectedDiscountCode.isDiscountValueRequired || self.discountType == DiscountTypeBasketItemPriceOverride) {
            discountValue = [[self.validator numberFromString:self.valueCell.detailTextField.text] description];
        }
        
        // submit the server request
        switch (self.discountType) {
            case DiscountTypeBasketItemDiscount: {
                [[BasketController sharedInstance] applyDiscountToBasketItems:self.basketItems
                                                                   withAmount:discountValue
                                                                       ofType:discountUnit
                                                                     withCode:self.selectedDiscountCode
                                                                   withReason:self.selectedDiscountReason
                                                               andDescription:self.selectedDiscountReason.text];
                break;
            }
            case DiscountTypeBasketDiscount: {
                [[BasketController sharedInstance] applyDiscountWithAmount:discountValue
                                                                    ofType:discountUnit
                                                                  withCode:self.selectedDiscountCode
                                                                withReason:self.selectedDiscountReason
                                                            andDescription:self.selectedDiscountReason.text];
                break;
            }
            case DiscountTypeBasketItemPriceOverride: {
                [[BasketController sharedInstance] adjustBasketItem:self.basketItems[0]
                                                          withPrice:discountValue
                                                         withReason:self.selectedDiscountReason
                                                     andDescription:self.selectedDiscountReason.text];
                break;
            }
            default:
                NSAssert(NO, @"Invalid discount type.");
                break;
        }
    }
}

#pragma mark - Properties

@synthesize selectedDiscountCode = _selectedDiscountCode;
- (void) setSelectedDiscountCode:(DiscountCode *)selectedDiscountCode
{
    if (_selectedDiscountCode != selectedDiscountCode) {
        _selectedDiscountCode = selectedDiscountCode;
        // clear out the selected reason
        self.selectedDiscountReason = nil;
        // the unit type in the code overrides the selection
        if (_selectedDiscountCode != nil) {
            if (_selectedDiscountCode.discountValueType != nil) {
                if ([_selectedDiscountCode.discountValueType isEqualToString:@"PERCENT"]) {
                    // overwrite the local state if it doesn't match
                    if (self.selectedDiscountUnit != DiscountUnitTypePercent) {
                        self.selectedDiscountUnit = DiscountUnitTypePercent;
                    }
                } else if ([_selectedDiscountCode.discountValueType isEqualToString:@"AMOUNT"]) {
                    // overwrite the local state if it doesn't match
                    if (self.selectedDiscountUnit != DiscountUnitTypeFixed) {
                        self.selectedDiscountUnit = DiscountUnitTypeFixed;
                    }
                }
            }
        }
        // reset the value
        self.valueCell.detailTextField.text = [self.validator stringFromNumber:@(0)];
        // update view
        [self updateViewState];
        // if we need a value
        if (self.selectedDiscountCode.isDiscountValueRequired) {
            // select the value entry
            [self.valueCell.detailTextField becomeFirstResponder];
        }
    }
}

@synthesize selectedDiscountReason = _selectedDiscountReason;
- (void) setSelectedDiscountReason:(DiscountReason *)selectedDiscountReason
{
    if (_selectedDiscountReason != selectedDiscountReason) {
        _selectedDiscountReason = selectedDiscountReason;
        // update view
        [self updateViewState];
        // if we need a value
        if (self.selectedDiscountCode.isDiscountValueRequired) {
            // select the value entry
            [self.valueCell.detailTextField becomeFirstResponder];
        }
    }
}

@synthesize selectedDiscountUnit = _selectedDiscountUnit;
- (void) setSelectedDiscountUnit:(enum DiscountUnitType)selectedDiscountUnit
{
    if (_selectedDiscountUnit != selectedDiscountUnit) {
        [self.valueCell.detailTextField resignFirstResponder];
        _selectedDiscountUnit = selectedDiscountUnit;
        // update the validator
        switch (_selectedDiscountUnit) {
            case DiscountUnitTypePercent:
                // load the number of significant digits from the setting
                self.validator.scale = BasketController.sharedInstance.clientSettings.DRAFT_discountUnitPercentageDecimalDigits;
                // load override for selected discount code
                if (self.selectedDiscountCode.numberOfDecimalPlaces) {
                    self.validator.scale = self.selectedDiscountCode.numberOfDecimalPlaces.integerValue;
                }
                self.validator.style = NSNumberFormatterPercentStyle;
                break;
            default:
                self.validator.currencyCode = [[[BasketController sharedInstance] basket] currencyCode];
                break;
        }
        // reset the value
        self.valueCell.detailTextField.text = [self.validator stringFromNumber:@(0)];
        // update view
        [self updateViewState];
        // if we need a value
        if (self.selectedDiscountCode.isDiscountValueRequired) {
            // select the value entry
            [self.valueCell.detailTextField becomeFirstResponder];
        }
    }
}

#pragma mark - UITableViewController

- (id) initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // default to item discount
        self.discountType = DiscountTypeBasketItemDiscount;
    }
    return self;
}

- (void) viewDidLoad
{
    [super viewDidLoad];
    // setup validator
    self.validator = [[OCGNumberTextFieldValidator alloc] init];
    // create text field cell
    self.valueCell = [[OCGTableViewCell alloc] initWithStyle:UITableViewCellStyleValue1
                                                      reuseIdentifier:nil];
    self.valueCell.detailTextField.delegate = self;
    self.valueCell.detailTextField.tag = kEditableCellValueSkinningTag;
    [OCGKeyboardTypeButtonItem setKeyboardType:kOCGKeyboardTypeNumberPad forTarget:self.valueCell.detailTextField];
    self.valueCell.detailTextField.inputAccessoryView = self.inputView;
    self.valueCell.detailTextField.textAlignment = NSTextAlignmentRight;
    self.valueCell.textLabelWidth = 100.0f;
    self.valueCell.textLabel.tag = kEditableCellKeySkinningTag;
    self.valueCell.textLabel.textAlignment = NSTextAlignmentLeft;
    // register cell classes
    [self.tableView registerClass:[OCGSegmentedToggleTableViewCell class]
           forCellReuseIdentifier:kDiscountUnitTableCellReuseId];
    // skin
    self.tableView.tag = kEditableTableSkinningTag;
    self.navigationItem.rightBarButtonItem.tag = kPrimaryButtonSkinningTag;
    // clear state
    self.selectedDiscountCode = nil;
    self.selectedDiscountReason = nil;
    self.selectedDiscountUnit = DiscountUnitTypeUnknown;
}

- (void) viewWillAppear:(BOOL)animated
{
    // set the add button title based on the discount type
    NSString *doneButtonText = nil;
    if (self.discountType == DiscountTypeBasketItemPriceOverride) {
        self.navigationItem.title = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁚󠀷󠁴󠁯󠁳󠁗󠁧󠀫󠁰󠁑󠁃󠁫󠀴󠁣󠀷󠀰󠁐󠁓󠁹󠁗󠁊󠁵󠀵󠁺󠁋󠁍󠁕󠁿*/ @"Adjust Price", nil);
        self.validator.currencyCode = BasketController.sharedInstance.currencyCode;
        self.valueCell.detailTextField.text = [self.validator stringFromNumber:@(0)];
        doneButtonText = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁐󠁸󠁑󠁐󠁊󠁤󠁁󠁰󠁖󠁇󠁶󠁑󠀱󠁁󠀸󠀲󠁱󠁡󠁆󠁁󠀵󠁋󠁢󠁇󠀴󠁗󠁳󠁿*/ @"Adjust", nil);
    } else {
        self.navigationItem.title = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁚󠁔󠀹󠁫󠀯󠀰󠁬󠁩󠁅󠁅󠁫󠁬󠁗󠁦󠁨󠁧󠁆󠁖󠀫󠁮󠁒󠁲󠁁󠁅󠁙󠁇󠁍󠁿*/ @"Add Discount", nil);
        doneButtonText = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁧󠁙󠁊󠁁󠁔󠁑󠁹󠁨󠀫󠁬󠁦󠀫󠁮󠁫󠁂󠁃󠁪󠁬󠁧󠁸󠁋󠁇󠁱󠁴󠁕󠀵󠁍󠁿*/ @"Add", nil);
    }
    // setup the add button
    self.addButton =
    [[UIBarButtonItem alloc] initWithTitle:doneButtonText
                                     style:UIBarButtonItemStyleDone
                                    target:self
                                    action:@selector(addButtonTapped:)];
    
    
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel
                                                                                          target:self
                                                                                          action:@selector(cancelButtonTapped)];
    self.navigationItem.rightBarButtonItem = self.addButton;

    // skin
    [[CRSSkinning currentSkin] applyViewSkin:self];
    
    // register for notifications
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(handleDiscountChangeNotification:)
                                                 name:kBasketControllerDiscountChangeNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(handleBasketItemChangeNotification:)
                                                 name:kBasketControllerBasketItemChangeNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(handleGeneralErrorNotification:)
                                                 name:kBasketControllerGeneralErrorNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(handleCustomerAffiliationAddedNotification:)
                                                 name:kBasketControllerCustomerAffiliationAddedNotification
                                               object:nil];
    
    // preload the discount codes
    [self loadCodes];
    // preload the reason list
    [self loadReasons];
    
    // update view
    [self updateViewState];
    
    [super viewWillAppear:animated];
}

- (void) viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    if (self.discountType == DiscountTypeBasketItemPriceOverride) {
        NSIndexPath *valueIndexPath = [self.layout firstIndexPathForObject:@(kDiscountValueTableSection)];
        [self.tableView selectRowAtIndexPath:valueIndexPath
                                    animated:YES
                              scrollPosition:UITableViewScrollPositionNone];
        [self.tableView.delegate tableView:self.tableView
                   didSelectRowAtIndexPath:valueIndexPath];
    }
}

- (void) didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITableViewDatasource

- (NSInteger) numberOfSectionsInTableView:(UITableView *)tableView
{
    return [self.layout count];
}

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.layout[section] count];
}

- (UITableViewCell *) tableView:(UITableView *)tableView
          cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    OCGTableViewCell *cell = nil;
    
    NSNumber *rowIdentifier = [self.layout objectForIndexPath:indexPath];
    
    // get/create cell for the current section
    switch (rowIdentifier.integerValue) {
        case kDiscountCodeTableSection: {
            cell = [tableView dequeueReusableCellWithIdentifier:kDiscountCodeTableCellReuseId];
            if (cell == nil) {
                cell = [[OCGTableViewCell alloc] initWithStyle:UITableViewCellStyleValue1
                                              reuseIdentifier:kDiscountCodeTableCellReuseId];
            }
            break;
        }
        case kDiscountReasonTableSection: {
            cell = [tableView dequeueReusableCellWithIdentifier:kDiscountReasonTableCellReuseId];
            if (cell == nil) {
                cell = [[OCGTableViewCell alloc] initWithStyle:UITableViewCellStyleValue1
                                              reuseIdentifier:kDiscountCodeTableCellReuseId];
            }
            break;
        }
        case kDiscountUnitTableSection: {
            cell = [tableView dequeueReusableCellWithIdentifier:kDiscountUnitTableCellReuseId];
            if (cell == nil) {
                cell = [[OCGTableViewCell alloc] initWithStyle:UITableViewCellStyleValue1
                                              reuseIdentifier:nil];
            }
            break;
        }
        case kDiscountValueTableSection: {
            cell = self.valueCell;
            break;
        }
    }
    
    if (cell != nil) {
        // skin
        cell.tag = kEditableTableCellSkinningTag;
        cell.textLabel.tag = kEditableCellKeySkinningTag;
        if ([cell isKindOfClass:[OCGTableViewCell class]]) {
            cell.detailRequirement = OCGTableViewCellDetailRequirementRequired;
        }
        
        switch (rowIdentifier.integerValue) {
            case kDiscountCodeTableSection: {
                cell.textLabel.text = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁒󠁴󠁮󠁴󠁡󠀱󠁓󠁸󠁃󠀳󠁹󠁰󠁱󠀵󠁎󠁑󠁵󠀹󠁯󠁙󠁭󠁯󠁃󠁂󠁁󠁒󠁕󠁿*/ @"Code", nil);
                cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
                if (self.selectedDiscountCode != nil) {
                    cell.detailTextLabel.text = self.selectedDiscountCode.discountDescription;
                } else {
                    cell.detailTextLabel.text = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠀰󠁐󠁍󠁚󠁇󠁡󠁇󠁡󠁋󠁨󠁔󠁯󠀲󠁶󠁱󠁍󠀲󠁯󠀳󠁙󠁍󠁷󠁪󠀫󠁍󠁪󠁑󠁿*/ @"None", nil);
                }
                // skin
                cell.detailTextLabel.tag = kEditableCellValueSkinningTag;
                break;
            }
            case kDiscountReasonTableSection: {
                cell.textLabel.text = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁰󠁧󠁙󠁔󠁶󠁉󠁉󠁵󠁳󠁕󠁒󠀳󠁊󠁫󠁅󠁺󠁅󠀶󠀶󠁈󠀹󠁲󠁏󠁫󠁬󠁹󠁣󠁿*/ @"Reason", nil);
                cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
                if (self.selectedDiscountReason != nil) {
                    cell.detailTextLabel.text = self.selectedDiscountReason.text;
                } else {
                    cell.detailTextLabel.text = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠀰󠁐󠁍󠁚󠁇󠁡󠁇󠁡󠁋󠁨󠁔󠁯󠀲󠁶󠁱󠁍󠀲󠁯󠀳󠁙󠁍󠁷󠁪󠀫󠁍󠁪󠁑󠁿*/ @"None", nil);
                }
                // skin
                cell.detailTextLabel.tag = kEditableCellValueSkinningTag;
                break;
            }
            case kDiscountUnitTableSection: {
                cell.textLabel.text = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁊󠁳󠁅󠁫󠁲󠁱󠁱󠁳󠁃󠁙󠁒󠁹󠁷󠁳󠁆󠁃󠀱󠁸󠁷󠁇󠁖󠁑󠁧󠁡󠁡󠁗󠁳󠁿*/ @"Discount Unit", nil);
                OCGSegmentedToggleTableViewCell *segmentCell = (OCGSegmentedToggleTableViewCell *)cell;
                segmentCell.delegate = self;
                [segmentCell setOptions:@[[[BasketController sharedInstance] currencyCode],
                                          NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁯󠁑󠁃󠁮󠁺󠁖󠁁󠁁󠁄󠁢󠁮󠁲󠁉󠀲󠁆󠁁󠁯󠁰󠀯󠁔󠀸󠁦󠁴󠁲󠁌󠁈󠁣󠁿*/ @"%", nil)]];
                segmentCell.selection = self.selectedDiscountUnit;
                break;
            }
            case kDiscountValueTableSection: {
                OCGTableViewCell *textCell = (OCGTableViewCell *)cell;
                if (self.discountType == DiscountTypeBasketItemPriceOverride) {
                    textCell.textLabel.text = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁙󠀯󠁐󠁖󠁆󠁘󠁱󠁰󠁍󠁂󠁆󠁰󠁏󠁓󠀲󠀯󠁗󠁕󠁕󠁊󠁳󠀸󠀶󠁏󠁨󠀱󠁅󠁿*/ @"New price", nil);
                } else {
                    textCell.textLabel.text = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁆󠁅󠁈󠀲󠁤󠀫󠁤󠁅󠁑󠁬󠁨󠁺󠁚󠁂󠁩󠀴󠁘󠁳󠁥󠁸󠀶󠁚󠁷󠁵󠁕󠀯󠁷󠁿*/ @"Discount", nil);
                }
                break;
            }
            default:
                break;
        }
    }
    
    return cell;
}

- (void) tableView:(UITableView *)tableView
   willDisplayCell:(UITableViewCell *)cell
 forRowAtIndexPath:(NSIndexPath *)indexPath
{
    [[CRSSkinning currentSkin] applyCellSkin:cell
                                    forStyle:tableView.style];
}

- (UIView *) tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    UIView *result = nil;
    
    if (self.discountType == DiscountTypeBasketItemPriceOverride) {
        if (self.basketItems.count > 0) {
            // display the original price of the first basket item
            MPOSBasketItem *basketItem = self.basketItems[0];
            UILabel *label = [[UILabel alloc] init];
            label.translatesAutoresizingMaskIntoConstraints = NO;
            label.font = [UIFont systemFontOfSize:[UIFont smallSystemFontSize]];
            label.textColor = [UIColor colorWithRed:0.298039 green:0.337255 blue:0.423529 alpha:1];
            label.textAlignment = NSTextAlignmentCenter;
            label.text = [NSString stringWithFormat:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠀵󠁧󠁚󠁌󠁱󠀴󠁺󠁦󠁔󠁺󠁔󠁥󠁈󠁑󠁯󠁎󠁯󠁦󠁴󠁇󠁹󠁴󠁣󠁒󠁂󠀳󠁑󠁿*/ @"Current price %@", nil), basketItem.pricing.display];
            result = [[UIView alloc] init];
            result.backgroundColor = [UIColor clearColor];
            [result addSubview:label];
            [label constrain:@"centerX = centerX" to:result];
            [label constrain:@"centerY = centerY" to:result];
        }
    }
    
    return result;
}

- (CGFloat) tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    CGFloat result = 0.0f;
    
    if (self.discountType == DiscountTypeBasketItemPriceOverride) {
        result = 44.0f;
    }
    
    return result;
}

#pragma mark - UITableViewDelegate

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSNumber *rowIdentifier = [self.layout objectForIndexPath:indexPath];
    
    // hide any visible input views
    [self.valueCell.detailTextField resignFirstResponder];
    
    switch (rowIdentifier.integerValue) {
        case kDiscountCodeTableSection: {
            // present the discount code list
            if ((self.availableCodes != nil) && (self.availableCodes.count > 0)) {
                OCGSelectViewController *discountCodeViewController =
                [[OCGSelectViewController alloc] initWithNibName:nil
                                                          bundle:nil];
                discountCodeViewController.title = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁮󠀯󠀴󠁘󠁋󠁆󠁶󠁭󠁕󠁧󠀵󠁷󠀶󠁉󠁣󠁱󠁹󠀱󠁹󠁊󠁱󠁪󠁭󠁃󠁬󠁨󠁧󠁿*/ @"Discount code", nil);
                discountCodeViewController.options = @[self.availableCodes];
                discountCodeViewController.titleForOption = ^(OCGSelectViewController *selectViewController, NSIndexPath *indexPath) {
                    DiscountCode *discountCode = [selectViewController.options objectForIndexPath:indexPath];
                    return discountCode.discountDescription;
                };
                discountCodeViewController.didSelectOption = ^(OCGSelectViewController *selectViewController, NSIndexPath *indexPath)
                {
                    DiscountCode *discountCode = [selectViewController.options objectForIndexPath:indexPath];
                    // update the selection
                    self.selectedDiscountCode = discountCode;
                    [self.navigationController popViewControllerAnimated:YES];
                };
                [self.navigationController pushViewController:discountCodeViewController
                                                     animated:YES];
            }
            break;
        }
        case kDiscountReasonTableSection: {
            if ((self.availableReasons != nil) && (self.availableReasons.count > 0)) {
                OCGSelectViewController *discountReasonViewController =
                [[OCGSelectViewController alloc] initWithNibName:nil
                                                          bundle:nil];
                discountReasonViewController.title = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁰󠁧󠁙󠁔󠁶󠁉󠁉󠁵󠁳󠁕󠁒󠀳󠁊󠁫󠁅󠁺󠁅󠀶󠀶󠁈󠀹󠁲󠁏󠁫󠁬󠁹󠁣󠁿*/ @"Reason", nil);
                discountReasonViewController.options = @[self.availableReasons];
                discountReasonViewController.titleForOption = ^(OCGSelectViewController *selectViewController, NSIndexPath *indexPath) {
                    DiscountReason *discountReason = [selectViewController.options objectForIndexPath:indexPath];
                    return discountReason.text;
                };
                discountReasonViewController.didSelectOption = ^(OCGSelectViewController *selectViewController, NSIndexPath *indexPath) {
                    DiscountReason *discountReason = [selectViewController.options objectForIndexPath:indexPath];
                    // update the selection
                    self.selectedDiscountReason = [selectViewController.options objectForIndexPath:indexPath];
                    [self.navigationController popViewControllerAnimated:YES];
                };
                [self.navigationController pushViewController:discountReasonViewController
                                                     animated:YES];
            }
            break;
        }
        case kDiscountValueTableSection: {
            // start editing
            [self.valueCell.detailTextField becomeFirstResponder];
            break;
        }
    }
    
    [tableView deselectRowAtIndexPath:indexPath
                             animated:YES];
}

#pragma mark - OCGSegmentedToggleTableViewCellDelegate

- (void) segmentTableViewCell:(OCGSegmentedToggleTableViewCell *)cell
                  didSwitchTo:(NSInteger)segmentIndex
{
    self.selectedDiscountUnit = segmentIndex;
}

#pragma mark - UITextFieldDelegate

- (BOOL) textFieldShouldBeginEditing:(UITextField *)textField
{
    // make sure the text cell is visible
    NSIndexPath *textPath = [self.layout indexPathsForObject:@(kDiscountValueTableSection)][0];
    [self.tableView scrollToRowAtIndexPath:textPath
                          atScrollPosition:UITableViewScrollPositionTop
                                  animated:YES];
    
    return YES;
}

- (BOOL) textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    [self.validator textField:textField
              shouldChangeCharactersInRange:range
                          replacementString:string];

    // update the add button state
    self.addButton.enabled = [self validateDiscountSettingsWithAlert:NO];
    
    return NO;
}

-(BOOL)textFieldShouldClear:(UITextField *)textField
{
    return [self textField:textField shouldChangeCharactersInRange:NSMakeRange(0, textField.text.length) replacementString:@""];
}

- (BOOL) textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    [self addButtonTapped:nil];
    return NO;
}

- (void) textFieldDidEndEditing:(UITextField *)textField
{
    [self updateViewState];
}

#pragma mark - Actions

- (IBAction) addButtonTapped:(id)sender
{
    if ([self validateDiscountSettingsWithAlert:YES]) {
        self.hasAddedDiscount = YES;
        // update the basket item with the configured discount.
        [self updateBasketItemWithDiscount];
    }
}


-(void)cancelButtonTapped
{
    [self dismissSelf];
}

-(void)dismissSelf
{
    [self.view endEditing:YES];
    [self dismissViewControllerAnimated:YES completion:NULL];
}

@end
    