//
//  ReceiptSearchViewController.m
//  mPOS
//
//  Created by John Scott on 04/02/2014.
//  Copyright (c) 2014 Clarity. All rights reserved.
//

#import "ReceiptSearchViewController.h"
#import "ReceiptItemViewController.h"
#import "NSArray+OCGIndexPath.h"

#import "UICollectionView+OCGExtensions.h"
#import "FNKCollectionViewLayout.h"

#import "OCGCollectionViewCell.h"
#import "OCGTextField.h"
#import "OCGFirstResponderAccessoryView.h"

#import "ServerErrorHandler.h"
#import "UIAlertView+MKBlockAdditions.h"
#import "ReceiptListViewController.h"
#import "CRSSkinning.h"
#import "BasketController.h"
#import "BasketViewController.h"
#import "POSFuncController.h"
#import "OCGKeyboardTypeButtonItem.h"

@interface ReceiptSearchViewController () <UITextFieldDelegate, OCGFirstResponderAccessoryViewDelegate>

@end

@implementation ReceiptSearchViewController
{
    NSArray *_layout;
    NSArray *_sectionTitles;
    NSMutableDictionary *_receiptDetails;
    OCGFirstResponderAccessoryView *_firstResponderAccessoryView;
    UIDatePicker *_datePicker;
    NSDateFormatter *_dateFormatter;
    NSDateFormatter *_displayDateFormatter;
}

#pragma mark - init

- (id)init
{
    FNKCollectionViewLayout *layout = [[FNKCollectionViewLayout alloc] init];
    self = [super initWithCollectionViewLayout:layout];
    if (self) {
        _receiptDetails = [NSMutableDictionary dictionary];
    }
    return self;
}

#pragma mark - UIViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //self.title = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁲󠁭󠁵󠁶󠁘󠁹󠀰󠁴󠁨󠀫󠁇󠁄󠁩󠁱󠀶󠁴󠁃󠁔󠀳󠁋󠁄󠁹󠁑󠁗󠁔󠁣󠀸󠁿*/ @"Return", nil);
    
    _layout = @[@[
                    @(ReceiptItemDetailOriginalTransactionStoreId),
                    @(ReceiptItemDetailOriginalTransactionTillNumber),
                    @(ReceiptItemDetailOriginalTransactionId),
                    @(ReceiptItemDetailOriginalTransactionDate)
                ]];

    [OCGCollectionViewCell registerForDetailClass:[UITextField class]
                                          options:OCGCollectionViewCellDetailOptionUseTextLabelBaseline
                                   collectionView:self.collectionView];
    self.collectionView.delaysContentTouches = NO;
    
    _firstResponderAccessoryView = [[OCGFirstResponderAccessoryView alloc] init];
    
    _firstResponderAccessoryView.delegate = self;
    
    _datePicker = [[UIDatePicker alloc] initWithFrame:CGRectMake(0, 0, 500, 200)];
    _datePicker.datePickerMode = UIDatePickerModeDate;
    _datePicker.maximumDate = [NSDate date];

    
    _dateFormatter = [[NSDateFormatter alloc] init];
    _dateFormatter.dateFormat = @"yyyy-MM-dd";
    
    _displayDateFormatter = [[NSDateFormatter alloc] init];
    _displayDateFormatter.dateStyle = NSDateFormatterMediumStyle;
    _displayDateFormatter.timeStyle = NSDateFormatterNoStyle;
    
    [_datePicker addTarget:self action:@selector(dateChanged) forControlEvents:UIControlEventValueChanged];

    [self updateNavigationItem];
    self.collectionView.tag = kEditableTableSkinningTag;
    [[CRSSkinning currentSkin] applyViewSkin:self];
    self.collectionView.backgroundColor = [UIColor colorWithRed:0.937 green:0.937 blue:0.957 alpha:1.000];
}

-(void)viewWillAppear:(BOOL)animated
{
    _sectionTitles = @[NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁓󠁩󠁬󠁇󠁇󠁥󠁑󠁷󠁂󠁸󠁧󠁪󠁪󠀸󠁁󠁕󠁰󠁍󠁦󠁃󠁫󠁒󠁡󠁯󠁲󠀫󠁯󠁿*/ @"Receipt details", nil)];
    
    [super viewWillAppear:animated];
    [[CRSSkinning currentSkin] applyViewSkin:self];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    NSIndexPath *firstIndexPath = [self.collectionView firstIndexPath];
    if (firstIndexPath) {
        OCGCollectionViewCell *cell = (OCGCollectionViewCell *)[self.collectionView cellForItemAtIndexPath:firstIndexPath];
        [cell.detailTextField becomeFirstResponder];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return [_layout count];
}

- (NSInteger)collectionView:(UICollectionView *)collectionView
     numberOfItemsInSection:(NSInteger)section
{
    return [_layout[section] count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView
                  cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *cellIdentifier =
    [OCGCollectionViewCell reuseIdentifierForDetailClass:[UITextField class]
                                                 options:OCGCollectionViewCellDetailOptionUseTextLabelBaseline];
    
    OCGCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier
                                                                            forIndexPath:indexPath];
    
    cell.detailTextField.delegate = self;
    cell.detailTextField.inputAccessoryView = _firstResponderAccessoryView;
    cell.textLabelWidth = 60;
    cell.detailTextField.textAlignment = NSTextAlignmentRight;
    cell.detailTextField.autocapitalizationType = UITextAutocapitalizationTypeNone;
    cell.detailTextField.autocorrectionType = UITextAutocorrectionTypeNo;
    [OCGKeyboardTypeButtonItem setKeyboardType:kOCGKeyboardTypeNumberPad forTarget:cell.detailTextField];
    cell.tag = kEditableTableCellSkinningTag;
    cell.textLabel.tag = kEditableCellKeySkinningTag;
    cell.detailTextField.tag = kEditableCellValueSkinningTag;
    cell.detailRequirement = OCGCollectionViewCellDetailRequirementRequired;
    cell.backgroundColor = [UIColor whiteColor];
    cell.layer.borderWidth = 1.;
    cell.layer.borderColor = [[UIColor colorWithRed:0.89 green:0.89 blue:0.90 alpha:1.000] CGColor];
    
    ReceiptItemDetail receiptDetail = [[_layout objectForIndexPath:indexPath] integerValue];
    
    switch (receiptDetail) {
        case ReceiptItemDetailOriginalTransactionStoreId:
            cell.textLabel.text = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁹󠁣󠁫󠁆󠁑󠁷󠁗󠁋󠁺󠁳󠀳󠁪󠁫󠀸󠀶󠁅󠁃󠁧󠁢󠁶󠁙󠁱󠁄󠁷󠁃󠁥󠀴󠁿*/ @"Store", nil);
            cell.detailTextField.placeholder = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠀳󠁶󠀹󠁣󠁱󠁡󠀰󠀴󠁤󠁎󠀷󠁍󠁇󠁹󠁏󠁒󠁷󠁢󠁁󠀸󠁌󠁔󠁒󠁲󠁐󠀷󠁣󠁿*/ @"Original store number", nil);
            cell.detailTextField.inputView = nil;
            break;
            
        case ReceiptItemDetailOriginalTransactionTillNumber:
            cell.textLabel.text = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁙󠁖󠁏󠁆󠁌󠁌󠁁󠀫󠁕󠁰󠁯󠁹󠁧󠁤󠁓󠁴󠀴󠁕󠁒󠁬󠁫󠁘󠁎󠁁󠁅󠁹󠁫󠁿*/ @"Till", nil);
            cell.detailTextField.placeholder = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁬󠁉󠁺󠁦󠁓󠁰󠁊󠁎󠁢󠀵󠁔󠁔󠀯󠁲󠁥󠁬󠀵󠀶󠀲󠁵󠁍󠁑󠁤󠁫󠀶󠁇󠁅󠁿*/ @"Original till", nil);
            cell.detailTextField.inputView = nil;
            break;
            
        case ReceiptItemDetailOriginalTransactionId:
            cell.textLabel.text = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁦󠁕󠁗󠁕󠁦󠀯󠁷󠁺󠁓󠁫󠁌󠁔󠁭󠁓󠁉󠀷󠁑󠁭󠁇󠁳󠁣󠀷󠁒󠁅󠁤󠁱󠁫󠁿*/ @"Tx Id", nil);
            cell.detailTextField.placeholder = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁁󠁯󠁍󠀴󠀯󠁭󠁙󠁓󠁅󠁳󠁷󠁷󠁔󠁴󠁦󠁆󠁶󠁨󠀵󠀳󠁘󠁯󠁭󠁏󠁅󠁙󠁳󠁿*/ @"Original transaction #", nil);
            cell.detailTextField.inputView = nil;
            break;
            
        case ReceiptItemDetailOriginalTransactionDate:
            cell.textLabel.text = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠀱󠁏󠁂󠁂󠁑󠁇󠁧󠁑󠁫󠁕󠁐󠀲󠁷󠁊󠁩󠀷󠁮󠁈󠁶󠁈󠁒󠁆󠁈󠁷󠁊󠁺󠀸󠁿*/ @"Date", nil);
            cell.detailTextField.placeholder = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁆󠀲󠁎󠁏󠁐󠁅󠀹󠀸󠁴󠀱󠁥󠁐󠀹󠁦󠁹󠁡󠁕󠁴󠁡󠁥󠁕󠁘󠁊󠁒󠀸󠁨󠁧󠁿*/ @"Original transaction date", nil);
            cell.detailTextField.inputView = _datePicker;
            break;
            
        default:
            break;
    }
    
    if (receiptDetail == ReceiptItemDetailOriginalTransactionDate && _receiptDetails[@(receiptDetail)] != nil) {
        NSDate *date = [_dateFormatter dateFromString:_receiptDetails[@(receiptDetail)]];
        cell.detailTextField.text = [_displayDateFormatter stringFromDate:date];
    } else {
        cell.detailTextField.text = _receiptDetails[@(receiptDetail)] ?: @"";
    }
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    [[CRSSkinning currentSkin] applyViewSkin:cell
                             withTagOverride:cell.tag];
    
    return cell;
}

#pragma mark - FNKCollectionViewDataSourceDelegate

- (NSArray * /* FNKSupplementaryItem */)collectionView:(UICollectionView *)collectionView
                                                layout:(UICollectionViewLayout*)collectionViewLayout
               supplementaryItemsForHeadersAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.item == 0) {
        FNKSupplementaryItem *result = [[FNKSupplementaryItem alloc] init];
        result.title = _sectionTitles[indexPath.section];
        
        return @[result];
    } else {
        return nil;
    }
}

#pragma mark - UICollectionViewDelegate

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    OCGCollectionViewCell *cell = (OCGCollectionViewCell *)[self.collectionView cellForItemAtIndexPath:indexPath];
    [cell.detailTextField becomeFirstResponder];
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    NSIndexPath *indexPath = [self.collectionView indexPathForCell:textField.OCGCollectionViewCell_collectionViewCell];
    _firstResponderAccessoryView.currentPosition = indexPath;
    
    ReceiptItemDetail receiptDetail = [[_layout objectForIndexPath:indexPath] integerValue];

    if (receiptDetail == ReceiptItemDetailOriginalTransactionDate) {
        _datePicker.date = [_dateFormatter dateFromString:_receiptDetails[@(receiptDetail)]] ?: [NSDate date];
        OCGCollectionViewCell *cell = (OCGCollectionViewCell *) [self.collectionView cellForItemAtIndexPath:indexPath];
        _receiptDetails[@(receiptDetail)] = [_dateFormatter stringFromDate:_datePicker.date];
        cell.detailTextField.text = [_displayDateFormatter stringFromDate:_datePicker.date];
    }
    [self updateNavigationItem];
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    _firstResponderAccessoryView.currentPosition = nil;
}

- (BOOL)textFieldShouldClear:(UITextField *)textField
{
    NSIndexPath *indexPath = [self.collectionView indexPathForCell:textField.OCGCollectionViewCell_collectionViewCell];
    
    NSString *receiptDetail = [_layout objectForIndexPath:indexPath];
    [_receiptDetails removeObjectForKey:receiptDetail];
    
    [self updateNavigationItem];

    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [_firstResponderAccessoryView moveFirstResponderByOffset:1];
    return NO;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSIndexPath *indexPath = [self.collectionView indexPathForCell:textField.OCGCollectionViewCell_collectionViewCell];
    NSString *text = [textField.text stringByReplacingCharactersInRange:range withString:string];
    ReceiptItemDetail receiptDetail = [[_layout objectForIndexPath:indexPath] integerValue];

    NSCharacterSet *validCharacters = nil;
    NSInteger maxLength = 1024;
    
    switch (receiptDetail) {
        case ReceiptItemDetailOriginalTransactionStoreId:
            validCharacters = [NSCharacterSet characterSetWithCharactersInString:@"0123456789abcdefghijklmnopqrstuvwxyz"];
            maxLength = 8;
            break;
        case ReceiptItemDetailOriginalTransactionTillNumber:
        case ReceiptItemDetailOriginalTransactionId:
            validCharacters = [NSCharacterSet characterSetWithCharactersInString:@"0123456789"];
            maxLength = 8;
            break;
            
        default:
            break;
    }
    
    if ([text length] > maxLength ||
        ([text length] > 0 && validCharacters != nil &&
         [text rangeOfCharacterFromSet:[validCharacters invertedSet]].location != NSNotFound)) {
        return NO;
    }
    
    _receiptDetails[@(receiptDetail)] = text;
    
    [self updateNavigationItem];
    
    return YES;
}

#pragma mark - OCGFirstResponderAccessoryViewDelegate

-(UIResponder*)OCGFirstResponderAccessoryView:(OCGFirstResponderAccessoryView *)firstResponderAccessoryView
                firstResponderViewForPosition:(id)position
{
    [self.collectionView scrollToItemAtIndexPath:position
                                atScrollPosition:UICollectionViewScrollPositionCenteredVertically
                                        animated:NO];
    OCGCollectionViewCell *cell = (OCGCollectionViewCell*)[self.collectionView cellForItemAtIndexPath:position];
    return cell.detailTextField;
}

- (id)OCGFirstResponderAccessoryView:(OCGFirstResponderAccessoryView*)firstResponderAccessoryView
                positionFromPosition:(id)position
                              offset:(NSInteger)offset
{
    return [self.collectionView indexPathFromIndexPath:position offset:offset wrap:YES];
}

#pragma mark - Date picker

- (void)dateChanged
{
    NSIndexPath *indexPath = _firstResponderAccessoryView.currentPosition;

    NSString *receiptDetail = [_layout objectForIndexPath:indexPath];
    _receiptDetails[receiptDetail] = [_dateFormatter stringFromDate:_datePicker.date];

    OCGCollectionViewCell *cell = (OCGCollectionViewCell *)[self.collectionView cellForItemAtIndexPath:indexPath];
    cell.detailTextField.text = [_displayDateFormatter stringFromDate:_datePicker.date];
    
    [self updateNavigationItem];
}

#pragma mark - Methods

-(void)updateNavigationItem
{
    UIBarButtonItem *cancelButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁩󠁢󠁺󠁬󠁏󠁶󠁣󠁧󠁫󠁍󠁋󠁧󠁫󠀱󠁶󠁐󠁐󠀹󠁘󠀴󠁘󠁡󠁕󠁭󠁹󠁷󠀰󠁿*/ @"Cancel", nil)
                                                                     style:UIBarButtonItemStyleDone
                                                                    target:self
                                                                    action:@selector(cancelButtonTapped)];
    self.navigationItem.leftBarButtonItem = cancelButton;
    
    NSInteger entriesWithLength = 0;
    
    NSArray *requiredReceiptDetails = @[
                                        @(ReceiptItemDetailOriginalTransactionStoreId),
                                        @(ReceiptItemDetailOriginalTransactionTillNumber),
                                        @(ReceiptItemDetailOriginalTransactionId),
                                        @(ReceiptItemDetailOriginalTransactionDate),
                                        ];
    
    for (NSString *receiptDetail in requiredReceiptDetails)
    {
        if ([_receiptDetails[receiptDetail] length] > 0)
        {
            entriesWithLength++;
        }
    }
    
    UIBarButtonItem *searchButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁮󠁰󠁎󠁩󠁧󠁑󠁒󠁭󠁗󠁗󠁊󠁵󠁗󠁏󠁏󠀯󠁱󠁃󠁘󠁪󠀲󠁗󠁘󠁣󠁈󠁲󠁫󠁿*/ @"Search", nil)
                                                                     style:UIBarButtonItemStyleDone
                                                                    target:self
                                                                    action:@selector(searchButtonTapped)];
    
    searchButton.enabled = entriesWithLength == [requiredReceiptDetails count];
    
    self.navigationItem.rightBarButtonItems = @[searchButton];
}

-(void)cancelButtonTapped
{
    [self dismissViewControllerAnimated:YES completion:NULL];
}

-(void)searchButtonTapped
{
    ocg_async_background_overlay(^{
        
        [RESTController.sharedInstance lookupSaleWithBarcode:nil
                                            originalBasketId:_receiptDetails[@(ReceiptItemDetailOriginalTransactionId)]
                                             originalStoreId:_receiptDetails[@(ReceiptItemDetailOriginalTransactionStoreId)]
                                          originalTillNumber:_receiptDetails[@(ReceiptItemDetailOriginalTransactionTillNumber)]
                                     originalTransactionDate:_receiptDetails[@(ReceiptItemDetailOriginalTransactionDate)]
                                                    complete:^(BasketAssociateDTO *basket, NSError *error, ServerError *serverError)
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                if (error != nil || serverError != nil)
                {
                    ServerErrorBarcodeItemNotReturnedMessage *message = [serverError.messages lastObject];
                    if ([message isKindOfClass:[ServerErrorBarcodeItemNotReturnedMessage class]] && [[BasketController sharedInstance] doesUserHavePermissionTo:POSPermissionAddReturnItemUnlinked])
                    {
                        if ([[BasketController sharedInstance] doesUserHavePermissionTo:POSPermissionAddReturnItemLinked])
                        {
                            NSString *title = nil;
                            if ([message.code integerValue] == MShopperBarcodeItemCouldNotBeReturnedExceptionCode)
                            {
                                title = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠀰󠁳󠁹󠁕󠁲󠀲󠁮󠁪󠁱󠁹󠁔󠁔󠀰󠁋󠁲󠀯󠁖󠁮󠁫󠁌󠁵󠁔󠀵󠁷󠁡󠁑󠀰󠁿*/ @"Receipt not found", nil);
                            }
                            else
                            {
                                title = [[ServerErrorHandler sharedInstance] buildServerErrorMessage:serverError
                                                                                      transportError:error
                                                                                         withContext:nil];
                            }
                            
                            UIAlertView *alert =
                            [[UIAlertView alloc] initWithTitle:title
                             
                                                       message:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁖󠁵󠁨󠁈󠁥󠀰󠀳󠁌󠀱󠁨󠁨󠁋󠀶󠁡󠁘󠁬󠀸󠀫󠁃󠀹󠀷󠁨󠀷󠁬󠁶󠁊󠁫󠁿*/ @"Continue as an unlinked return?", nil)
                                                      delegate:[UIAlertView class]
                                             cancelButtonTitle:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁩󠁢󠁺󠁬󠁏󠁶󠁣󠁧󠁫󠁍󠁋󠁧󠁫󠀱󠁶󠁐󠁐󠀹󠁘󠀴󠁘󠁡󠁕󠁭󠁹󠁷󠀰󠁿*/ @"Cancel", nil)
                                             otherButtonTitles:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁄󠁴󠁍󠁉󠁣󠀫󠁺󠁔󠁗󠁦󠁹󠁡󠁯󠀷󠁨󠁔󠁩󠀸󠁯󠀫󠁨󠁘󠀹󠁱󠁌󠀶󠀸󠁿*/ @"OK", nil), nil];
                            
                            alert.dismissBlock = ^(int buttonIndex) {
                                if (buttonIndex == 0)
                                {
                                    [self unlinkedReceiptReturn:message];
                                }
                            };
                            [alert show];
                        }
                        else
                        {
                            [self unlinkedReceiptReturn:message];
                        }
                    }
                    else
                    {
                        [[ServerErrorHandler sharedInstance] handleServerError:serverError
                                                                transportError:error
                                                                   withContext:nil
                                                                  dismissBlock:nil
                                                                   repeatBlock:nil];
                    }
                }
                else
                {
                    __weak UIViewController *presentingViewController = self.presentingViewController;
                    [self dismissViewControllerAnimated:YES completion:^{
                        ReceiptListViewController *viewController = [[ReceiptListViewController alloc] init];
                        viewController.basket = basket;
                        
                        UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:viewController];
                        navigationController.modalPresentationStyle = UIModalPresentationFormSheet;
                        [presentingViewController presentViewController:navigationController animated:YES completion:NULL];
                    }];
                }
            });
        }];
    });
}

-(void)unlinkedReceiptReturn:(ServerErrorBarcodeItemNotReturnedMessage*)message
{
    __weak UIViewController *presentingViewController = self.presentingViewController;
    [self dismissViewControllerAnimated:YES completion:^{
        ReceiptItemViewController *viewController = [[ReceiptItemViewController alloc] init];
        viewController.type = ReceiptItemViewControllerTypeUnlinkedReturn;
        NSMutableDictionary *receiptDetails = [NSMutableDictionary dictionary];
        //                                [receiptDetails setValue:message.originalItemPrice forKey:@(ReceiptItemDetailOriginalItemPrice)];
        [receiptDetails setObject:message.originalTransactionDate forKey:@(ReceiptItemDetailOriginalTransactionDate)];
        [receiptDetails setObject:message.originalTransactionId forKey:@(ReceiptItemDetailOriginalTransactionId)];
        [receiptDetails setObject:message.originalTransactionStoreId forKey:@(ReceiptItemDetailOriginalTransactionStoreId)];
        [receiptDetails setObject:message.originalTransactionTill forKey:@(ReceiptItemDetailOriginalTransactionTillNumber)];
        viewController.initialItemDetails = receiptDetails;
        
        
        UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:viewController];
        navigationController.modalPresentationStyle = UIModalPresentationFormSheet;
        [presentingViewController presentViewController:navigationController animated:YES completion:NULL];
    }];
}

@end
