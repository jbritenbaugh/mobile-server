//
//  LocationDataSource.m
//  mPOS
//
//  Created by Antonio Strijdom on 10/08/2012.
//  Copyright (c) 2012 Clarity. All rights reserved.
//

#import <CoreLocation/CoreLocation.h>
#import "LocationDataSource.h"
#import "CRSLocationController.h"
#import "CRSLocationSummary.h"
#import "RESTController.h"

@interface LocationDataSource () <CLLocationManagerDelegate> {
    
}
@property (nonatomic, strong) CLLocationManager *locationManager;
@property (nonatomic, assign) BOOL locationErrored;
@property (nonatomic, strong) CLLocation *location;
@property (nonatomic, readonly) NSString *brandId;
- (CRSLocationSummary *) convertStoreToLocation:(Store *)store;
@end

@implementation LocationDataSource

#pragma mark - Private

/** @brief Converts a Store object into a CRSLocationSummary object.
 *  @param store The store to convert.
 *  @return The CRSLocationSummary of the store.
 */
- (CRSLocationSummary *) convertStoreToLocation:(Store *)store
{
    CRSLocationSummary *result = [[CRSLocationSummary alloc] init];
    
    result.key = store.storeId;
    if (store.name.length > 0)
    {
        result.name = store.name;
    }
    else if (store.shortName.length > 0)
    {
        result.name = store.shortName;
    }
    result.lat = store.latitude;
    result.lon = store.longitude;
    
    result.tillNumbers = store.tills;
    
    return result;
}

#pragma mark - Properties

/** @property brandId
 *  @brief The configured brand id.
 */
@synthesize brandId;
- (NSString *) brandId
{
    NSString *result = nil;
    
    // load from plist
    NSBundle *bundle = [NSBundle bundleForClass:[self class]];
    NSURL *settingsURL = [bundle URLForResource:@"Settings"
                                  withExtension:@"plist"];
    if (settingsURL) {
        NSDictionary *settings = [NSDictionary dictionaryWithContentsOfURL:settingsURL];
        result = [settings objectForKey:@"brandId"];
    }
    
    return result;
}

#pragma mark - Init

- (id) init
{
    self = [super init];
    if (self) {
        // setup the location manager
        self.locationManager = [[CLLocationManager alloc] init];
        self.locationManager.distanceFilter = 100;
        self.locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters;
        self.locationManager.delegate = self;
        self.location = nil;
        self.locationErrored = NO;
    }
    
    return self;
}

#pragma mark - CRSLocationSettingTableViewControllerLocationDatasource

/** @brief Gets the summary for the currently configured location.
 *  @param complete The block to execute when the location is retreived.
 *  @note The location should be returned as a CRSLocationSummary object.
 *  @note If no store is currently configured, this method will simply return nil.
 */
- (void) getCurrentLocationSummaryWithBlock:(void (^)(id data, NSError *error))complete
{
    // get the current configured location
    NSString *key = [CRSLocationController getLocationKey];
    if (key) {
        // retreive the location details from the server
        ocg_async_background_overlay(^{
            [RESTController.sharedInstance getStoreDetailsWithStoreId:key
                                                               server:nil
                                                             complete:^(Store *store, NSError *error, ServerError *serverError)
             {
                 if (store) {
                     CRSLocationSummary *result = [self convertStoreToLocation:store];
                     ocg_sync_main(^{
                         complete(result, nil);
                     });
                 } else {
                     NSError *returnedError = error;
                     if (serverError != nil) {
                         returnedError =
                         [NSError errorWithDomain:kLocationDataSourceErrorDomain
                                             code:kLocationDataSourceErrorServerErrorCode
                                         userInfo:@{ kLocationDataSourceErrorServerErrorKey : serverError }];
                     }
                     ocg_sync_main(^{
                         complete(nil, returnedError);
                     });
                     
                 }
             }];
        });
    } else {
        // if no store is configured, just return nil
        complete(nil, nil);
    }
}

/** @brief Gets the current location list for displaying in the table.
 *  @param complete The block to execute when the location list is loaded.
 *  @note The location list should be an NSArray of CRSLocationSummary objects.
 */
- (void) getLocationListWithBlock:(void (^)(id data, NSError *error))complete
{
    [self updateLocationIfAvailable];
    ocg_async_background_overlay(^{
        [RESTController.sharedInstance listStoresWithBrandId:self.brandId
                                                 isHomeStore:YES
                                                    latitude:self.location.coordinate.latitude
                                                   longitude:self.location.coordinate.longitude
                                                      server:nil
                                                    complete:^(Stores *stores, NSError *error, ServerError *serverError)
         {
             if (stores) {
                 NSMutableArray *locations = [NSMutableArray array];
                 for (Store *store in stores.stores) {
                     CRSLocationSummary *location = [self convertStoreToLocation:store];
                     [locations addObject:location];
                 }
                 ocg_sync_main(^{
                     complete(locations, nil);
                 });
             } else {
                 NSError *returnedError = error;
                 if (serverError != nil) {
                     returnedError =
                     [NSError errorWithDomain:kLocationDataSourceErrorDomain
                                         code:kLocationDataSourceErrorServerErrorCode
                                     userInfo:@{ kLocationDataSourceErrorServerErrorKey : serverError }];
                 }
                 ocg_sync_main(^{
                     complete(nil, returnedError);
                 });
             }
         }];
        
    });
}

#pragma mark - CLLocationManagerDelegate

- (void) locationManager:(CLLocationManager *)manager
     didUpdateToLocation:(CLLocation *)newLocation
            fromLocation:(CLLocation *)oldLocation
{
    self.location = newLocation;
}

- (void) locationManager:(CLLocationManager *)manager
        didFailWithError:(NSError *)error
{
    self.locationErrored = YES;
}

#pragma mark - Utility functions

- (void) getLocationSummaryForLocationKey:(NSString*)key
                           serviceBaseURL:(NSString*)serviceBaseURL
                                    block:(void (^)(CRSLocationSummary *result, NSError *error))complete
{
    if (key) {
        // retreive the location details from the server
        ocg_async_background_overlay(^{
            NSString *cacheKey = [NSString stringWithFormat:@"LocationDataSource\x1F%@\x1F%@", key, serviceBaseURL];
            NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
            
            Store *store = nil;
            NSData *data = [standardUserDefaults objectForKey:cacheKey];
            @autoreleasepool {
                if (data != nil)
                {
                    store = [NSKeyedUnarchiver unarchiveObjectWithData:data];
                }
            }
            
            void (^completeWithStore)(Store*, NSError*, ServerError*) = ^(Store *store, NSError *error, ServerError *serverError)
            {
                if (store) {
                    NSData *data = nil;
                    @autoreleasepool {
                        data = [NSKeyedArchiver archivedDataWithRootObject:store];
                    }
                    
                    NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
                    [standardUserDefaults setObject:data forKey:cacheKey];
                    [standardUserDefaults synchronize];
                    
                    
                    CRSLocationSummary *result = [self convertStoreToLocation:store];
                    ocg_sync_main(^{
                        complete(result, nil);
                    });
                } else {
                    NSError *returnedError = error;
                    if (serverError != nil) {
                        returnedError =
                        [NSError errorWithDomain:kLocationDataSourceErrorDomain
                                            code:kLocationDataSourceErrorServerErrorCode
                                        userInfo:@{ kLocationDataSourceErrorServerErrorKey : serverError }];
                    }
                    ocg_sync_main(^{
                        complete(nil, returnedError);
                    });
                    
                }
            };

            if (store)
            {
                completeWithStore(store, nil, nil);
            }
            else
            {
                [RESTController.sharedInstance getStoreDetailsWithStoreId:key
                                                                   server:serviceBaseURL
                                                                 complete:completeWithStore];
            }
        });
    } else {
        // if no store is configured, just return nil
        complete(nil, nil);
    }
}

- (void) getLocationListForServiceBaseURL:(NSString*)serviceBaseURL
                                    block:(void (^)(NSMutableArray *locations, NSError *error))complete
{
    [self updateLocationIfAvailable];
    ocg_async_background_overlay(^{
        [RESTController.sharedInstance listStoresWithBrandId:self.brandId
                                                 isHomeStore:YES
                                                    latitude:self.location.coordinate.latitude
                                                   longitude:self.location.coordinate.longitude
                                                      server:serviceBaseURL
                                                    complete:^(Stores *stores, NSError *error, ServerError *serverError)
         {
             if (stores) {
                 NSMutableArray *locations = [NSMutableArray array];
                 for (Store *store in stores.stores) {
                     CRSLocationSummary *location = [self convertStoreToLocation:store];
                     [locations addObject:location];
                 }
                 ocg_sync_main(^{
                     complete(locations, nil);
                 });
             } else {
                 NSError *returnedError = error;
                 if (serverError != nil) {
                     returnedError =
                     [NSError errorWithDomain:kLocationDataSourceErrorDomain
                                         code:kLocationDataSourceErrorServerErrorCode
                                     userInfo:@{ kLocationDataSourceErrorServerErrorKey : serverError }];
                 }
                 ocg_sync_main(^{
                     complete(nil, returnedError);
                 });
             }
         }];
        
    });
}

-(void)updateLocationIfAvailable
{
#ifdef UNITTEST
    self.location = [[CLLocation alloc] initWithLatitude:50.0f
                                               longitude:-1.0f];
#else
    if ([CLLocationManager locationServicesEnabled] && ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusAuthorizedAlways ||[CLLocationManager authorizationStatus] == kCLAuthorizationStatusNotDetermined))
    {
#ifdef __IPHONE_8_0
        if ([UIDevice currentDevice].systemVersion.floatValue >= 8.0) {
            // request authorization (required in iOS 8)
            [self.locationManager requestWhenInUseAuthorization];
        }
#endif
        // get the current location
        [self.locationManager startUpdatingLocation];
        // wait for a location
        while ((self.location == nil) && (!self.locationErrored)) {
            [[NSRunLoop currentRunLoop] runUntilDate:[NSDate dateWithTimeIntervalSinceNow:1]];
        }
        [self.locationManager stopUpdatingLocation];
    }
    else
    {
        self.location = nil;
    }
#endif
}

@end
