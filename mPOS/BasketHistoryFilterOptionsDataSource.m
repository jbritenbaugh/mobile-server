//
//  BasketHistoryFilterOptionsDataSource.m
//  mPOS
//
//  Created by Antonio Strijdom on 22/11/2012.
//  Copyright (c) 2012 Clarity. All rights reserved.
//

#import "BasketHistoryFilterOptionsDataSource.h"
#import "CRSLocationSummary.h"
#import "CRSLocationController.h"

@implementation BasketHistoryFilterOptionsDataSource

#pragma mark - Properties

@synthesize locations = _locations;
- (void) setLocations:(NSArray *)locations
{
    _locations = locations;
    [self createTableFields];
}

@synthesize selectedOptions = _selectedOptions;

#pragma mark - CRSEditableTableFieldsDataSource

/** @brief Initializes the collection of editable fields in the data source.
 */
- (void) createTableFields
{
    [super removeAllObjects];
    
    if (self.locations != nil) {
        // from date
        NSDate *fromDate = nil;
        if (self.selectedOptions != nil) {
            fromDate = [self.selectedOptions objectForKey:kFieldFromDate];
        }
        if (fromDate == nil) {
            fromDate = [NSDate date];
        }
        [super addEditableTableField:
         [CRSEditableTableField editableTableFieldWithDate:fromDate
                                                   caption:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁉󠁵󠁑󠁘󠀫󠀷󠁁󠀯󠀶󠀶󠁚󠁉󠁇󠁁󠁧󠁌󠁑󠁹󠀳󠁹󠁭󠀴󠁊󠁭󠁚󠁰󠁣󠁿*/ @"From", nil)]
                                 key:kFieldFromDate];
        // to date
        NSDate *toDate = nil;
        if (self.selectedOptions != nil) {
            toDate = [self.selectedOptions objectForKey:kFieldToDate];
        }
        if (toDate == nil) {
            toDate = [NSDate date];
        }
        [super addEditableTableField:
         [CRSEditableTableField editableTableFieldWithDate:toDate
                                                   caption:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁰󠁳󠁕󠁲󠁮󠁍󠁪󠁓󠁶󠀯󠀴󠁗󠁘󠁓󠁥󠁵󠀷󠁯󠁬󠀲󠁵󠀹󠁣󠁕󠁎󠁋󠀰󠁿*/ @"To", nil)]
                                 key:kFieldToDate];
        
        // location
        NSString *currentLocation = nil;
        if (self.selectedOptions != nil) {
            currentLocation = [self.selectedOptions objectForKey:kFieldLocation];
        }
        if (currentLocation == nil) {
            currentLocation = [CRSLocationController getLocationKey];
        }
        // build the location array
        NSMutableArray *locationNames = [NSMutableArray arrayWithCapacity:self.locations.count];
        NSInteger selectedIndex = 0;
        for (int i = 0; i < self.locations.count; i++) {
            CRSLocationSummary *location = [self.locations objectAtIndex:i];
            if ([location.key isEqualToString:currentLocation]) {
                selectedIndex = i;
            }
            [locationNames addObject:location.name];
        }
        [super addEditableTableField:
         [CRSEditableTableField editableTableFieldWithSelectList:locationNames
                                              selectedValueIndex:selectedIndex
                                                         caption:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁱󠁉󠁰󠁄󠀲󠁪󠁥󠁒󠁆󠀯󠁫󠀰󠁭󠁗󠀯󠁳󠁌󠁤󠁊󠁹󠁄󠁣󠁒󠁖󠁕󠁐󠁷󠁿*/ @"Location", nil)]
                                 key:kFieldLocation];
        
        // associate
        NSNumber *associateValue = nil;
        if (self.selectedOptions != nil) {
            associateValue = [self.selectedOptions objectForKey:kFieldAssociate];
        }
        BOOL associate = YES;
        if (associateValue != nil) {
            associate = associateValue.boolValue;
        }
        [super addEditableTableField:
         [CRSEditableTableField editableTableFieldWithBoolean:associate
                                                      caption:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁮󠁏󠁸󠁐󠀷󠁁󠁯󠁂󠁉󠁲󠁊󠁸󠁍󠁤󠁂󠁅󠀯󠁲󠁺󠁔󠁨󠁚󠁪󠁕󠀲󠁵󠁣󠁿*/ @"Associate filter", nil)]
                                 key:kFieldAssociate];
    }
}

#pragma mark - Methods

- (NSDictionary *) getOptions
{
    NSMutableDictionary *result = [NSMutableDictionary dictionary];
    
    CRSEditableTableField *field = nil;
    
    // from date
    field = [super editableTableFieldWithKey:kFieldFromDate];
    [result setValue:[CRSEditableTableField dateFromFormattedString:field.value]
              forKey:kFieldFromDate];
    // to date
    field = [super editableTableFieldWithKey:kFieldToDate];
    [result setValue:[CRSEditableTableField dateFromFormattedString:field.value]
              forKey:kFieldToDate];
    // location
    field = [super editableTableFieldWithKey:kFieldLocation];
    CRSLocationSummary *location = [self.locations objectAtIndex:field.selectedValueIndex];
    [result setValue:location.key
              forKey:kFieldLocation];
    // associate
    field = [super editableTableFieldWithKey:kFieldAssociate];
    [result setValue:[NSNumber numberWithBool:[CRSEditableTableField booleanFromFormattedString:field.value]]
              forKey:kFieldAssociate];
    
    return result;
}

@end
