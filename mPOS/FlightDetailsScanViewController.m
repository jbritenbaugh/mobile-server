//
//  AirSideViewController.m
//  mPOS
//
//  Created by John Scott on 06/01/2015.
//  Copyright (c) 2015 Clarity. All rights reserved.
//

#import "FlightDetailsScanViewController.h"

#import "SoftKeyContextType.h"
#import "ServerErrorHandler.h"
#import "UIAlertView+MKBlockAdditions.h"

@interface FlightDetailsScanViewController ()

@end

@implementation FlightDetailsScanViewController

-(void)viewWillAppear:(BOOL)animated
{
    self.title = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁭󠁱󠁢󠁲󠁹󠁊󠁋󠁥󠁪󠁹󠁂󠁅󠁰󠁶󠁘󠀱󠁸󠁘󠁊󠁊󠀹󠀫󠀴󠁳󠁨󠁤󠁣󠁿*/ @"Flight Details", nil);
    
    // reload the layout
    SoftKeyContext *layoutContext =
    [BasketController.sharedInstance getLayoutForContextNamed:NSStringForSoftKeyContextType(SoftKeyContextTypeMenuFlightDetailsLayout)];
    self.softKeyContext = layoutContext;

    [super viewWillAppear:animated];
}

#pragma mark - FNKScanViewDelegate

-(void)FNKScanViewController:(FNKScanViewController *)collectionViewController
                  didSelectSoftKey:(SoftKey *)softKey
{
    
}

-(void)FNKScanViewController:(FNKScanViewController *)collectionViewController
              handleScannedBarcode:(NSString *)barcode
{
    ocg_async_background_overlay(^{
        
        NSError *error = nil;
        ServerError *serverError = nil;
        
        BasketDTO *originalBasket = BasketController.sharedInstance.basket;
        if (!originalBasket.flightDetails)
        {
            originalBasket.flightDetails = [[BasketFlightDetails alloc] init];
        }
        
        originalBasket.flightDetails.boardingPassBarcode = barcode;
        
        BasketDTO *basket =
        [BasketController.sharedInstance adjustSale:originalBasket
                                              error:&error
                                        serverError:&serverError];
                
        ocg_sync_main(^{
            if (basket)
            {
                BasketController.sharedInstance.basket = basket;
                [BasketController.sharedInstance kick];
            }
            
            if (error != nil || serverError != nil)
            {
                // there was an error
                ServerError *mposerror = serverError;
                [BasketController.sharedInstance kick];
                // send the notification
                [[NSNotificationCenter defaultCenter] postNotificationName:kBasketControllerGeneralErrorNotification
                                                                    object:self
                                                                  userInfo:@{kBasketControllerGeneralErrorKey:mposerror}];
            }
            else
            {
                [collectionViewController dismissViewController];
            }
        });
    });
}

@end
