//
//  ReturnPriceTableViewController.m
//  mPOS
//
//  Created by Antonio Strijdom on 01/11/2013.
//  Copyright (c) 2013 Clarity. All rights reserved.
//

#import "ReturnPriceTableViewController.h"
#import "CRSSkinning.h"
#import "OCGTableViewCell.h"
#import "OCGTextField.h"
#import "OCGNumberTextFieldValidator.h"
#import "UIAlertView+OCGBlockAdditions.h"
#import "NSArray+OCGIndexPath.h"

@interface ReturnPriceTableViewController ()
@property (nonatomic, strong) NSArray *layout;
@property (nonatomic, readonly) BOOL hasCustomPrice;
@property (nonatomic, strong) OCGNumberTextFieldValidator *priceValidator;
@end

@implementation ReturnPriceTableViewController

#pragma mark - Private

const NSInteger kRowIndexCurrent = 0;
const NSInteger kRowIndexOriginal = 1;
const NSInteger kRowIndexCustom = 2;

#pragma mark - Properties

- (BOOL) hasCustomPrice
{
    return ((self.price != nil) && (![self.price isEqualToString:@""]));
}

#pragma mark - UIViewController

- (void) viewDidLoad
{
    [super viewDidLoad];
    // cells
    // don't display current price option if the item has no price
    if (self.item.price != nil) {
        self.layout =
        @[@[@(kRowIndexCurrent),
            @(kRowIndexOriginal),
            @(kRowIndexCustom)]];
    } else {
        self.layout =
        @[@[@(kRowIndexOriginal),
            @(kRowIndexCustom)]];
    }
    // validator
    self.priceValidator = [[OCGNumberTextFieldValidator alloc] init];
    // skin
    self.tableView.tag = kEditableTableSkinningTag;
}

- (void) viewWillAppear:(BOOL)animated
{
    // ui
    self.title = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠀱󠁶󠀱󠀯󠁪󠁡󠁖󠁒󠁹󠁃󠁏󠁴󠁵󠁨󠁸󠁣󠁁󠀲󠁂󠁥󠁪󠁳󠁭󠁤󠁡󠀰󠁁󠁿*/ @"Return Price", nil);
    [[CRSSkinning currentSkin] applyViewSkin:self];
    [super viewWillAppear:animated];
    [self.tableView reloadData];
}

#pragma mark - UITableViewDataSource

- (NSInteger) numberOfSectionsInTableView:(UITableView *)tableView
{
    return [self.layout count];
}

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.layout[section] count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"TextCell"];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1
                                      reuseIdentifier:@"TextCell"];
    }
    
    NSNumber *rowIdent = [self.layout objectForIndexPath:indexPath];
    
    if (cell != nil) {
        switch (rowIdent.integerValue) {
            case kRowIndexCurrent: {
                cell.textLabel.text = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁱󠁈󠁤󠁫󠁯󠁅󠀷󠁒󠁍󠁳󠁺󠀳󠁤󠀷󠀫󠁦󠁸󠁰󠁭󠁐󠁳󠁫󠁗󠀲󠁇󠁈󠁣󠁿*/ @"Use current price", nil);
                cell.detailTextLabel.text = self.item.price;
                cell.textLabel.tag = kEditableCellKeySkinningTag;
                cell.detailTextLabel.tag = kEditableCellValueSkinningTag;
                break;
            }
            case kRowIndexOriginal: {
                cell.textLabel.text = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁳󠁩󠁔󠁢󠁹󠁶󠁦󠁓󠁙󠁆󠀲󠁎󠁴󠁧󠁗󠁫󠁆󠁓󠁄󠁴󠁃󠁈󠁎󠁔󠁦󠁂󠁅󠁿*/ @"Use original price", nil);
                cell.detailTextLabel.text = self.originalPrice;
                cell.textLabel.tag = kEditableCellKeySkinningTag;
                cell.detailTextLabel.tag = kEditableCellValueSkinningTag;
                break;
            }
            case kRowIndexCustom: {
                cell.textLabel.text = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁂󠁗󠁒󠀹󠁡󠁧󠁕󠁄󠀰󠀷󠀰󠀹󠁲󠁕󠁓󠁔󠀸󠁦󠁲󠁌󠁤󠁋󠁹󠀵󠀶󠁡󠁑󠁿*/ @"Custom price", nil);
                // TFS 59179 - Don't default the custom price to the current price
                cell.detailTextLabel.text = self.price;
                cell.textLabel.tag = kEditableCellKeySkinningTag;
                cell.detailTextLabel.tag = kEditableCellValueSkinningTag;
                break;
            }
            default:
                break;
        }
    }
    
    return cell;
}

- (void) tableView:(UITableView *)tableView
   willDisplayCell:(UITableViewCell *)cell
 forRowAtIndexPath:(NSIndexPath *)indexPath
{
    [[CRSSkinning currentSkin] applyCellSkin:cell
                                    forStyle:tableView.style];
}

#pragma mark - UITableViewDelegate

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSNumber *rowIdent = [self.layout objectForIndexPath:indexPath];
    
    switch (rowIdent.integerValue) {
        case kRowIndexCurrent: {
            if (self.delegate != nil) {
                [self.delegate returnPriceTableViewController:self
                                              didSpecifyPrice:self.item.price];
            }
            break;
        }
        case kRowIndexOriginal: {
            if (self.delegate != nil) {
                [self.delegate returnPriceTableViewController:self
                                              didSpecifyPrice:nil];
            }
            break;
        }
        case kRowIndexCustom: {
            UIAlertView *alert =
            [[UIAlertView alloc] initWithTitle:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁩󠁏󠁅󠁣󠁋󠁰󠁦󠀹󠀫󠁪󠁏󠁣󠁕󠁅󠁰󠁱󠁵󠀳󠀱󠀴󠁘󠁫󠁈󠁸󠁂󠁵󠁳󠁿*/ @"Custom Price", nil)
                                       message:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁪󠁢󠁕󠁋󠁊󠀰󠁬󠁬󠀴󠁯󠁱󠁉󠁣󠁘󠁵󠁯󠁢󠁇󠁋󠁵󠁸󠁔󠁱󠁨󠁰󠁣󠁙󠁿*/ @"Enter the price of the return", nil)
                                      delegate:[UIAlertView class]
                             cancelButtonTitle:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁩󠁢󠁺󠁬󠁏󠁶󠁣󠁧󠁫󠁍󠁋󠁧󠁫󠀱󠁶󠁐󠁐󠀹󠁘󠀴󠁘󠁡󠁕󠁭󠁹󠁷󠀰󠁿*/ @"Cancel", nil)
                             otherButtonTitles:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁄󠁴󠁍󠁉󠁣󠀫󠁺󠁔󠁗󠁦󠁹󠁡󠁯󠀷󠁨󠁔󠁩󠀸󠁯󠀫󠁨󠁘󠀹󠁱󠁌󠀶󠀸󠁿*/ @"OK", nil), nil];
            alert.alertViewStyle = UIAlertViewStylePlainTextInput;
            UITextField *textField = [alert textFieldAtIndex:0];
            textField.delegate = self.priceValidator;
            // TFS 59179 - Don't default the custom price to the current price
            textField.text = self.price;
            textField.placeholder = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁲󠁃󠀶󠀴󠁶󠀳󠀰󠁹󠁋󠁍󠁶󠁶󠀳󠁦󠁋󠁩󠁶󠁑󠁇󠁥󠀯󠁓󠁋󠁸󠁷󠁬󠁉󠁿*/ @"Return price", nil);
            textField.keyboardType = UIKeyboardTypeDecimalPad;
            alert.dismissBlock = ^(int buttonIndex) {
                self.price = textField.text;
                if (self.delegate != nil) {
                    [self.delegate returnPriceTableViewController:self
                                                  didSpecifyPrice:self.price];
                }
            };
            alert.cancelBlock = ^() {
                [self.tableView reloadData];
            };
            alert.shouldEnableFirstOtherButtonBlock = ^BOOL() {
                BOOL result = NO;
                
                if ([self.priceValidator isStringValid:textField.text]) {
                    result = textField.text.floatValue > 0.00f;
                }
                
                return result;
            };

            [alert show];
            break;
        }
        default:
            break;
    }
    [tableView deselectRowAtIndexPath:indexPath
                             animated:YES];
}

@end
