//
//  ReturnPriceTableViewController.h
//  mPOS
//
//  Created by Antonio Strijdom on 01/11/2013.
//  Copyright (c) 2013 Clarity. All rights reserved.
//

#import <UIKit/UIKit.h>

@class MPOSItem;

@protocol ReturnPriceTableViewControllerDelegate;

@interface ReturnPriceTableViewController : UITableViewController

@property (nonatomic, weak) id<ReturnPriceTableViewControllerDelegate> delegate;
@property (nonatomic, strong) MPOSItem *item;
@property (nonatomic, strong) NSString *originalPrice;
@property (nonatomic, strong) NSString *price;

@end

@protocol ReturnPriceTableViewControllerDelegate <NSObject>
@required
- (void) returnPriceTableViewController:(ReturnPriceTableViewController *)vc
                        didSpecifyPrice:(NSString *)price;

@end