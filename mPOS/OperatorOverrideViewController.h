//
//  OperatorOverrideViewController.h
//  mPOS
//
//  Created by Antonio Strijdom on 18/03/2015.
//  Copyright (c) 2015 Clarity. All rights reserved.
//

#import <UIKit/UIKit.h>

/** @file OperatorOverrideViewController.h */

/** @brief View controller for presenting operator overrides. */
@interface OperatorOverrideViewController : UICollectionViewController

/** @property serverError
 *  @brief The server error being overridden.
 */
@property (nonatomic, strong) ServerError *serverError;

@end
