//
//  BasketViewCell.h
//  mPOS
//
//  Created by John Scott on 23/10/2013.
//  Copyright (c) 2013 Clarity. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface BasketViewCell : UITableViewCell

-(void)updateWithBasket:(MPOSBasket*)basket;

@end
