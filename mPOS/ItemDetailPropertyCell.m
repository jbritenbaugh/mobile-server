//
//  ItemDetailPropertyCell.m
//  mPOS
//
//  Created by Antonio Strijdom on 29/05/2013.
//  Copyright (c) 2013 Clarity. All rights reserved.
//

#import "ItemDetailPropertyCell.h"

@implementation ItemDetailPropertyCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
