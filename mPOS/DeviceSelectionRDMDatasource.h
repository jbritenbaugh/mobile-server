//
//  DeviceSelectionRDMDatasource.h
//  mPOS
//
//  Created by Antonio Strijdom on 16/03/2015.
//  Copyright (c) 2015 Clarity. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DeviceSelectionViewController.h"

@interface DeviceSelectionRDMDatasource : NSObject <DeviceSelectionViewControllerDataSource>

@property (nonatomic, strong) NSArray *deviceStations;
@property (nonatomic, strong) NSString *deviceType;

@end
