//
//  OCGReceiptManager.m
//  mPOS
//
//  Created by Antonio Strijdom on 09/10/2013.
//  Copyright (c) 2013 Clarity. All rights reserved.
//

#import "OCGReceiptManager.h"
#import "BasketController.h"
#import "PrinterQueueController.h"
#import "UIAlertView+OCGBlockAdditions.h"
#import "OCGEmailTextFieldValidator.h"
#import "OCGProgressOverlay.h"
#import "PrinterController.h"
#import "NSRegularExpression+OCGExtensions.h"
#import "DPTextAttachment.h"

@interface OCGReceiptManager () <PrinterQueueControllerDelegate, UITextFieldDelegate> {
    OCGEmailTextFieldValidator *_emailTextFieldValidator;
    UIAlertView *_alertView;
    dispatch_block_t _printingCompleteBlock;
}
@property (nonatomic, assign) enum ReceiptManagerType managerType;
@end

@implementation OCGReceiptManager

#pragma mark - Private

static OCGReceiptManager *_currentManager = nil;
static dispatch_group_t _initGroup = nil;
static dispatch_group_t _printingGroup = nil;

#pragma mark - Properties

@synthesize managerType = _managerType;
@synthesize printing = _printing;

#pragma mark - Methods

+ (enum ReceiptManagerType) configuredReceiptManagerType
{
    // default to client
    enum ReceiptManagerType result = ReceiptManagerTypeClient;
    
    // get the client setting from basket controller
    ClientSettings *settings = [[BasketController sharedInstance] clientSettings];
    if (settings != nil) {
        if ((settings.receiptManager != nil) ||
            ([settings.receiptManager isEqualToString:@""])) {
            if ([settings.receiptManager isEqualToString:@"MOBILE_CLIENT"]) {
                result = ReceiptManagerTypeClient;
            } else if ([settings.receiptManager isEqualToString:@"REMOTE_SERVICE"]) {
                result = ReceiptManagerTypeServer;
            }
        }
    }
    
    return result;
}

+ (OCGReceiptManager *) configuredManager
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _initGroup = dispatch_group_create();
        _printingGroup = dispatch_group_create();
    });
    
    dispatch_group_wait(_initGroup, DISPATCH_TIME_FOREVER);
    dispatch_group_enter(_initGroup);
    // kill the old manager if the setting has changed
    if (_currentManager != nil) {
        if (_currentManager.managerType != [OCGReceiptManager configuredReceiptManagerType]) {
            _currentManager = nil;
        }
    }
    
    if (_currentManager == nil) {
        _currentManager = [[OCGReceiptManager alloc] init];
        _currentManager.managerType = [OCGReceiptManager configuredReceiptManagerType];
    }
    dispatch_group_leave(_initGroup);
    
    return _currentManager;
}

- (void) emailReceiptComplete:(dispatch_block_t)complete
{
    MPOSBasket *basket = [[BasketController sharedInstance] basket];
    if (basket != nil) {
        if (basket.emailReceipt) {
            basket.emailAddress = nil;
            basket.emailReceipt = NO;
            [BasketController.sharedInstance kick];
        } else {
            _alertView =
            [[UIAlertView alloc] initWithTitle:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁣󠁤󠁫󠁣󠁯󠁲󠁵󠁭󠁌󠀰󠀰󠀳󠁰󠁁󠁎󠁓󠁰󠀵󠁰󠁍󠁈󠁢󠁱󠁤󠁵󠁗󠀰󠁿*/ @"Email the receipt", nil)
                                       message:nil
                                      delegate:[UIAlertView class]
                             cancelButtonTitle:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁩󠁢󠁺󠁬󠁏󠁶󠁣󠁧󠁫󠁍󠁋󠁧󠁫󠀱󠁶󠁐󠁐󠀹󠁘󠀴󠁘󠁡󠁕󠁭󠁹󠁷󠀰󠁿*/ @"Cancel", nil)
                             otherButtonTitles:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁺󠁂󠁕󠀷󠁲󠁤󠁴󠁶󠁮󠁇󠁶󠁌󠁂󠁩󠁶󠁊󠁱󠀹󠁩󠁋󠁢󠁗󠀸󠁸󠁮󠁙󠁉󠁿*/ @"Send", nil), nil];
            
            _alertView.alertViewStyle = UIAlertViewStylePlainTextInput;
            
            UITextField *emailTextField = [_alertView textFieldAtIndex:0];
            emailTextField.keyboardType = UIKeyboardTypeEmailAddress;
            emailTextField.text = [BasketController sharedInstance].basket.emailAddress;
            emailTextField.placeholder = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁱󠁁󠁏󠀸󠁎󠁒󠁚󠀯󠁥󠁈󠁏󠀵󠀳󠀵󠁵󠀳󠁏󠁐󠁑󠁕󠁔󠀳󠁕󠁳󠁴󠁬󠁣󠁿*/ @"Email Address", nil);
            
            _emailTextFieldValidator = [[OCGEmailTextFieldValidator alloc] init];
            emailTextField.delegate = self;
            
            _alertView.dismissBlock = ^(int buttonIndex)
            {
                if (buttonIndex == 0) {
                    basket.emailReceipt = YES;
                    basket.emailAddress = emailTextField.text;
                    [BasketController.sharedInstance kick];
                }
                
                if (complete != nil) {
                    complete();
                }
            };
            
            _alertView.cancelBlock = ^() {
                if (complete != nil) {
                    complete();
                }
            };
            
            __weak OCGEmailTextFieldValidator *emailTextFieldValidator = _emailTextFieldValidator;
            _alertView.shouldEnableFirstOtherButtonBlock = ^
            {
                return [emailTextFieldValidator isStringValid: emailTextField.text];
            };
            
            [_alertView show];
        }
    }
}

- (void) printReceipt
{
    MPOSBasket *basket = [[BasketController sharedInstance] basket];
    if (basket != nil) {
        basket.enablePrint = !basket.enablePrint;
        [BasketController.sharedInstance kick];
    }
}

-(BOOL)printRTIPrintData:(NSString*)printData error:(NSError**)error
{
    BOOL result = NO;
    if (self.managerType == ReceiptManagerTypeClient) {
        dispatch_group_wait(_printingGroup, DISPATCH_TIME_FOREVER);
        dispatch_group_enter(_printingGroup);
        PrinterQueueController *printQueue = [BasketController sharedInstance].printQueue;
        printQueue.delegate = self;
        
        NSString *preprintReceiptImage = BasketController.sharedInstance.clientSettings.DRAFT_preprintReceiptImage;
        
        if (preprintReceiptImage.length > 0)
        {
            NSString *imageTag = [NSString stringWithFormat:@"[BITMAP:100:LEFT:CompanyAssets/%@]", preprintReceiptImage];
            printData = [imageTag stringByAppendingString:printData];
        }
        
        NSAttributedString *attributedString = [self attributedStringFromRTIPrintData:printData];
        
        result = [printQueue printAttributedString:attributedString error:error];
        dispatch_group_leave(_printingGroup);
        
        // wait for the print
    } else if (self.managerType == ReceiptManagerTypeServer) {
        
    }
    return result;
}


- (void) printSummaryReceipt
{
    if (self.managerType == ReceiptManagerTypeClient) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁆󠀶󠁨󠁡󠁡󠁯󠁸󠁏󠁴󠁰󠀹󠁗󠁭󠁖󠁧󠁸󠀴󠁊󠁋󠁲󠁚󠁅󠁖󠁃󠀸󠁣󠁉󠁿*/ @"Print summary receipt", nil)
                                                        message:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁩󠁐󠁕󠁋󠁥󠀱󠁲󠀯󠁱󠁙󠁶󠀯󠁮󠀸󠁳󠁡󠀸󠁘󠁒󠁖󠁹󠁥󠁢󠀫󠁎󠁂󠁷󠁿*/ @"Summary receipt printing is not supported on clients", nil)
                                                       delegate:self
                                              cancelButtonTitle:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁄󠁴󠁍󠁉󠁣󠀫󠁺󠁔󠁗󠁦󠁹󠁡󠁯󠀷󠁨󠁔󠁩󠀸󠁯󠀫󠁨󠁘󠀹󠁱󠁌󠀶󠀸󠁿*/ @"OK", nil)
                                              otherButtonTitles:nil];
        [alert show];
    } else if (self.managerType == ReceiptManagerTypeServer) {
        MPOSBasket *basket = [[BasketController sharedInstance] basket];
        if (basket != nil) {
            basket.printSummaryReceipt = !basket.printSummaryReceipt;
            [BasketController.sharedInstance kick];
        }
    }
}

-(NSAttributedString*)attributedStringFromRTIPrintData:(NSString*)printData
{
    NSMutableAttributedString *buffer = [[NSMutableAttributedString alloc] init];
    
    // BARCODE:<BARCODE TYPE>:<HEIGHT>:<WIDTH>:<ALIGNMENT>:<TEXT POSITION>:<DATA>
    
    // [BITMAP:<SIZE>:<ALIGNMENT>:<FILENAME>]
    
    NSString *regexString = @"\\[(BOLD)\\]"
    @"|"
    @"\\[(UNDERLINE)\\]"
    @"|"
    @"\\[(ITALIC)\\]"
    @"|"
    @"\\[(BIG)\\]"
    @"|"
    @"\\[(DOUBLE_WIDE)\\]"
    @"|"
    @"\\[(NORMAL)\\]"
    @"|"
    @"\\[(NORMAL_SIZE)\\]"
    @"|"
    @"\\[(CENTRE)\\]"
    @"|"
    @"(?:\\[(BARCODE):([^:\\]]+):(\\d+):(\\d+):(LEFT|CENTRE|CENTER|RIGHT):([^:\\]]+):([^\\]]+)\\])"
    @"|"
    @"(?:\\[(BITMAP):(\\d+):(LEFT|CENTRE|CENTER|RIGHT):([^\\]]+)\\])"
    @"|"
    @"(?:.|\\n)"
    ;
    
    
    NSArray *groupNames = @[
                            @"content",
                            @"bold",
                            @"underline",
                            @"italic",
                            @"big",
                            @"double_wide",
                            @"normal",
                            @"normal_size",
                            @"centre",
                            @"barcode",
                            @"barcode_type",
                            @"barcode_height",
                            @"barcode_width",
                            @"barcode_alignment",
                            @"barcode_text_position",
                            @"barcode_data",
                            @"bitmap",
                            @"bitmap_size",
                            @"bitmap_alignment",
                            @"bitmap_filename",
                            ];
    
    
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:regexString
                                                                           options:0
                                                                             error:NULL];
    
    NSArray *matches = [regex OCGExtensions_matchesInString:printData
                                                 groupNames:groupNames];
    
    NSInteger keyCount = 0;
    
    UIFontDescriptorSymbolicTraits symbolicTraits = 0;
    CGFloat underlineStyle = 0;
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    
    CGAffineTransform transform = CGAffineTransformIdentity;
    
    for (NSDictionary* match in matches)
    {
        if (match[@"normal"])
        {
            symbolicTraits = 0;
            underlineStyle = 0;
            paragraphStyle = [[NSMutableParagraphStyle alloc] init];
            transform = CGAffineTransformIdentity;
        }
        else if (match[@"bold"])
        {
            symbolicTraits |= UIFontDescriptorTraitBold;
        }
        else if (match[@"underline"])
        {
            underlineStyle = 1;
        }
        else if (match[@"italic"])
        {
            symbolicTraits |= UIFontDescriptorTraitItalic;
        }
        else if (match[@"big"])
        {
            transform = CGAffineTransformMakeScale(2, 2);
        }
        else if (match[@"double_wide"])
        {
            transform = CGAffineTransformMakeScale(2, 1);
        }
        else if (match[@"normal_size"])
        {
            transform = CGAffineTransformIdentity;
        }
        else if (match[@"left"])
        {
            paragraphStyle.alignment = NSTextAlignmentLeft;
        }
        else if (match[@"center"] || match[@"centre"])
        {
            paragraphStyle.alignment = NSTextAlignmentCenter;
        }
        else if (match[@"right"])
        {
            paragraphStyle.alignment = NSTextAlignmentRight;
        }
        else if (match[@"barcode"])
        {
            NSString *barcodeData = match[@"barcode_data"];
            
            if ([match[@"barcode_type"] isEqual:@"CODE128PARSED"])
            {
                barcodeData = [barcodeData stringByReplacingOccurrencesOfString:@"\\{(?:(\\{)|.)"
                                                                     withString:@"$1"
                                                                        options:NSRegularExpressionSearch
                                                                          range:NSMakeRange(0, barcodeData.length)];
                
                UIFontDescriptorSymbolicTraits symbolicTraits = 0;
                CGFloat underlineStyle = 0;
                NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
                
                if ([match[@"barcode_alignment"] isEqual:@"LEFT"])
                {
                    paragraphStyle.alignment = NSTextAlignmentLeft;
                }
                else if ([match[@"barcode_alignment"] isEqual:@"CENTER"] || [match[@"barcode_alignment"] isEqual:@"CENTRE"])
                {
                    paragraphStyle.alignment = NSTextAlignmentCenter;
                }
                else if ([match[@"barcode_alignment"] isEqual:@"RIGHT"])
                {
                    paragraphStyle.alignment = NSTextAlignmentRight;
                }
                
                DPTextAttachment *attachment = [[DPTextAttachment alloc] init];
                attachment.barcodeType = BarcodeType_code128;
                attachment.barcodeData = [barcodeData dataUsingEncoding:NSUTF8StringEncoding];
                
                NSDictionary *attributes = @{
                                             NSParagraphStyleAttributeName : [paragraphStyle copy],
                                             NSAttachmentAttributeName : attachment,
                                             };
                
                NSString *attachmentCharacter = [[NSString alloc] initWithCharacters:(unichar[]){NSAttachmentCharacter} length:1];
                
                NSAttributedString *content = [[NSAttributedString alloc] initWithString:attachmentCharacter
                                                                              attributes:attributes];
                
                [buffer appendAttributedString:content];
            }
        }
        else if (match[@"bitmap"])
        {
            NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
            
            if ([match[@"bitmap_alignment"] isEqual:@"LEFT"])
            {
                paragraphStyle.alignment = NSTextAlignmentLeft;
            }
            else if ([match[@"bitmap_alignment"] isEqual:@"CENTER"] || [match[@"bitmap_alignment"] isEqual:@"CENTRe"])
            {
                paragraphStyle.alignment = NSTextAlignmentCenter;
            }
            else if ([match[@"bitmap_alignment"] isEqual:@"RIGHT"])
            {
                paragraphStyle.alignment = NSTextAlignmentRight;
            }
            
            DPTextAttachment *attachment = [[DPTextAttachment alloc] init];
            attachment.imageName = match[@"bitmap_filename"];

            
            NSDictionary *attributes = @{
                                         NSParagraphStyleAttributeName : [paragraphStyle copy],
                                         NSAttachmentAttributeName : attachment,
                                         };
            
            NSString *attachmentCharacter = [[NSString alloc] initWithCharacters:(unichar[]){NSAttachmentCharacter} length:1];
            
            NSAttributedString *content = [[NSAttributedString alloc] initWithString:attachmentCharacter
                                                                          attributes:attributes];
            
            [buffer appendAttributedString:content];
        }
        else
        {
            NSDictionary *traitsAttributes = @{UIFontSymbolicTrait: @(symbolicTraits)};
            
            NSDictionary *fontAttributes = @{
                                             UIFontDescriptorTraitsAttribute : traitsAttributes,
                                             UIFontDescriptorFamilyAttribute : @"Menlo",
                                             UIFontDescriptorMatrixAttribute : [NSValue valueWithCGAffineTransform:transform],
                                            };
            
            
            UIFontDescriptor *fontDescriptor = [UIFontDescriptor fontDescriptorWithFontAttributes:fontAttributes];
            
            
            NSDictionary *attributes = @{
                                         NSUnderlineStyleAttributeName : @(underlineStyle),
                                         NSFontAttributeName : [UIFont fontWithDescriptor:fontDescriptor size:23],
                                         NSParagraphStyleAttributeName : [paragraphStyle copy],
                                         };
            
            NSAttributedString *content = [[NSAttributedString alloc] initWithString:match[@"content"]
                                                                          attributes:attributes];
            
            [buffer appendAttributedString:content];
        }
    }
    
    {
        DPTextAttachment *attachment = [[DPTextAttachment alloc] init];
        attachment.shouldCut = YES;
        
        
        NSDictionary *attributes = @{
                                     NSParagraphStyleAttributeName : [paragraphStyle copy],
                                     NSAttachmentAttributeName : attachment,
                                     };
        
        NSString *attachmentCharacter = [[NSString alloc] initWithCharacters:(unichar[]){NSAttachmentCharacter} length:1];
        
        NSAttributedString *content = [[NSAttributedString alloc] initWithString:attachmentCharacter
                                                                      attributes:attributes];
        
        [buffer appendAttributedString:content];

    }

    return buffer;
}

-(UIImage*)imageFromAttributedString:(NSAttributedString*)attributedString
{
    NSDictionary *fontAttributes = @{
                                     UIFontDescriptorFamilyAttribute : @"Menlo",
                                     };
    
    
    UIFontDescriptor *fontDescriptor = [UIFontDescriptor fontDescriptorWithFontAttributes:fontAttributes];
    
    
    UIFont *font = [UIFont fontWithDescriptor:fontDescriptor size:6];
    
    CGSize size = [@"M" sizeWithAttributes:@{ NSFontAttributeName : font }];
    
    NSStringDrawingContext *drawingContext = [[NSStringDrawingContext alloc] init];
    
    CGRect sizeRect = [attributedString boundingRectWithSize:CGSizeMake(size.width * 38, FLT_MAX)
                                                options:NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesDeviceMetrics
                                                context:drawingContext];
    
    CGSize xxx = [attributedString size];
    
    
    
    UIEdgeInsets insets = UIEdgeInsetsMake(size.width, size.width*30, 0, size.width*30);
    UIEdgeInsets outsets = UIEdgeInsetsMake(-insets.top, -insets.left, -insets.bottom, -insets.right);
    
    sizeRect = UIEdgeInsetsInsetRect(sizeRect, outsets);
    sizeRect.origin = CGPointMake(0, 0);
    
    UIGraphicsBeginImageContextWithOptions(sizeRect.size, YES, 0);
    
    sizeRect = UIEdgeInsetsInsetRect(sizeRect, insets);
    
    [[UIColor colorWithWhite:0.8 alpha:1] setFill];
    
    UIRectFill(CGRectInfinite);
    
    [[UIColor whiteColor] setFill];
    
    UIRectFill(sizeRect);
    
    [[UIColor blackColor] setFill];
    
    [attributedString drawWithRect:sizeRect
                           options:NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesDeviceMetrics
                           context:NULL];
    
    UIImage* image = UIGraphicsGetImageFromCurrentImageContext();

    UIGraphicsEndImageContext();
    
    return image;
}

-(void)testRTIReceipt
{
    NSString *printData = nil;
    
    NSArray *printLines = @[
                            @"[BITMAP:200:CENTER:PoS_app.png]",
                            @"",
                            @"[CENTRE][BOLD]Welcome to Our Store",
                            @"[NORMAL]",
                            @"      Item Number 1            1.00",
                            @"      1",
                            @"      1 item",
                            @"      SUBTOTAL                 1.00",
                            @"      TAX DUE                  0.00",
                            @"      AMOUNT DUE               1.00",
                            @"",
                            @"      CASH                     1.00",
                            @"      CHANGE                   0.00",
                            @"",
                            @"",
                            @"[CENTRE]---- VAT SUMMARY ----",
                            @"[NORMAL]      TOTAL VAT                0.00",
                            @"",
                            @"",
                            @"",
                            @"[BOLD]Bold text inline the transaction[NORMAL]",
                            @"[BOLD]receipt [NORMAL]",
                            @"",
                            @"     [BOLD]Bold[NORMAL]",
                            @"     [BIG]Big[NORMAL]",
                            @"     [ITALIC]Italic[NORMAL]",
                            @"     [DOUBLE_WIDE]Double wide[NORMAL]",
                            @"[BOLD]Bold[NORMAL], [BIG]Big[NORMAL], [ITALIC]Italic[NORMAL], [BOLD]Bold[NORMAL], [DOUBLE_WIDE]Double wide[NORMAL]",
                            @"",
                            @"[BARCODE:CODE128PARSED:100:200:CENTER:BELOW:R1500000]",
                            @"",
                            @"[CENTRE]Thank you and come again",
                            @"[NORMAL]",
                            @"[CENTRE]05/11/2014 11:02:59 1 100 6 MSTR 1",
                            @"[NORMAL]",
                            ];
    
    printData = [printLines componentsJoinedByString:@"\n"];
    
    [self printRTIPrintData:printData error:NULL];
    
    
//    NSAttributedString *attributedString = [self attributedStringFromRTIPrintData:printData];
    
    
//    UIImage *image = [self imageFromAttributedString:attributedString];
//    DebugLog2(@"{}", image);
    

    
    
    
    //     printData = @"[CENTRE][BOLD]Welcome to Our Store\n[NORMAL]\nItem Number 1            1.00\n1\n1 item\nSUBTOTAL                 1.00\nTAX DUE                  0.00\nAMOUNT DUE               1.00\n\nCASH                     1.00\nCHANGE                   0.00\n\n\n[CENTRE]---- VAT SUMMARY ----\n[NORMAL]      TOTAL VAT                0.00\n\n\n\n[BOLD] Bold text inline the transaction[NORMAL]\n[BOLD]receipt [NORMAL]\n\n[BARCODE:CODE128PARSED:100:200:CENTER:BELOW:{BR{C00010100000620141105]\n\n[CENTRE]Thank you and come again\n[NORMAL]\n[CENTRE]05/11/2014 11:02:59 1 100 6 MSTR 1\n[NORMAL]";
    
}

#pragma mark - UITextFieldDelegate

- (BOOL) textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    [_alertView dismissWithClickedButtonIndex:1
                                     animated:YES];
    return NO;
}

- (BOOL) textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    return [_emailTextFieldValidator textField:textField
                 shouldChangeCharactersInRange:range
                             replacementString:string];
}

@end
