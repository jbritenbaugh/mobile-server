//
//  BarcodeScannerController.h
//  mPOS
//
//  Created by Antonio Strijdom on 18/02/2013.
//  Copyright (c) 2013 Clarity. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SMScannerManager.h"

/** @file BarcodeScannerController.h */

@class OCGSplitViewController;

extern NSString * const kConfigScannerUserDefaultKeyPath;
/** @brief A barcode has been scanned.
 *  @note userInfo will contain:
 *  kScannerNotificationBarcodeScannedBarcodeKey set to the barcode data.
 */
extern NSString * const kScannerNotificationBarcodeScanned;
extern NSString * const kScannerNotificationBarcodeScannedBarcodeKey;

typedef NS_ENUM(NSInteger, ScannerEntryMethod) {
    ScannerEntryMethodUnknown,
    ScannerEntryMethodKeyed,
    ScannerEntryMethodScanned,
    ScannerEntryMethodSwiped
};

/** @enum The uses of barcodes. */
typedef enum BarcodeUseType {
    BarcodeUseTypeItem,
    BarcodeUseTypeReturn,
    BarcodeUseTypeLookup
} BarcodeUseType;

/** @brief Block called when a barcode scan event is received. 
 *  @param barcode The barcode that was scanned.
 *  @param entryMethod The entryMethod used to obtain the barcode.
 *  @param barcodeType The type of the barcode.
 *  @param continueScanning Set to YES to continue scanning.
 */
typedef void(^BarcodeScannedBlock)(NSString *barcode, ScannerEntryMethod entryMethod, SMBarcodeType barcodeType,BOOL *continueScanning);

/** @brief Block called when an error occurs when scanning a barcode.
 *  @param errorDesc The description of the error.
 */
typedef void(^BarcodeErroredBlock)(NSString *errorDesc);

/** @brief Block executed when scanning terminates.
 */
typedef void(^BarcodeScanTerminatedBlock)();

/** @brief Controller class for interfacing to the ScannerManager library.
 */
@interface BarcodeScannerController : UIResponder

@property (nonatomic, readonly) BOOL scanning;

@property (nonatomic, assign) SMScannerSourceType sourceType;

/** @brief Gets the shared barcode controller instanse.
 *  @return The shared barcode scanner controller.
 */
+ (BarcodeScannerController *) sharedInstance;

/** @brief Registers an object to receive scan notifications.
 *  @param registrant The object to register.
 *  @param selector The selector to send when the notification is sent.
 */
- (void) registerForNotifications:(id)registrant
                     withSelector:(SEL)selector;

/** @brief Unregisters an object. Stops it from receiving scan notifications.
 *  @param deregistrant The object to unregister.
 */
- (void) unregisterForNotifications:(id)deregistrant;

/** @brief Sets whether the hardware scanning buttons are enabled.
 *  @param enabled Set to YES to enable the buttons, otherwise NO.
 */
- (void) enableHardwareButtonScanning:(BOOL)enabled;

/** @brief Begins scanning.
 *  @param vc The view controller hosting the scanning process.
 *  @param scanReason The reason we are scanning a barcode or nil.
 *  @param buttonSize The size of the manual override button or CGSizeZero.
 *  @param addTitle The title to display on the add button.
 *  @param mustScan Set to YES to force the user to scan a barcode. If NO, the user can hit "Done" to abort scanning.
 *  @param alphaNumericAllowed Whether or not to allow alphanumberic barcode entry
 *  @param scanned The block to execute when a scan event is triggered.
 *  @param ended The block to execute when scanning is terminated.
 *  @param error The block to execute when a scan error is triggered.
 */
- (void) beginScanningInViewController:(UIViewController *)vc
                             forReason:(NSString *)scanReason
                        withButtonSize:(CGSize)buttonSize
                          withAddTitle:(NSString *)addTitle
                              mustScan:(BOOL)mustScan
                   alphaNumericAllowed:(BOOL)alphaNumericAllowed
                             withBlock:(BarcodeScannedBlock)scanned
                            terminated:(BarcodeScanTerminatedBlock)ended
                                 error:(BarcodeErroredBlock)error;

/** @brief Begins scanning.
 *  @param vc The view controller hosting the scanning process.
 *  @param scanReason The reason we are scanning a barcode or nil.
 *  @param buttonSize The size of the manual override button or CGSizeZero.
 *  @param addTitle The title to display on the add button.
 *  @param mustScan Set to YES to force the user to scan a barcode. If NO, the user can hit "Done" to abort scanning.
 *  @param alphaNumericAllowed Whether or not to allow alphanumberic barcode entry
 *  @param scanned The block to execute when a scan event is triggered.
 *  @param ended The block to execute when scanning is terminated.
 *  @param error The block to execute when a scan error is triggered.
 */
- (void) beginScanningWithBlock:(BarcodeScannedBlock)scanned
                     terminated:(BarcodeScanTerminatedBlock)ended
                          error:(BarcodeErroredBlock)error;

/** @brief Sends a manual barcode scan.
 *  @param barcode The barcode to send.
 */
- (void) sendManualEntry:(NSString *)barcode;

/** @brief Temporarily halts scanning.
 */
- (void) pauseScanning;

/** @brief Pauses scanning to go into manual entry mode.
 */
- (void) pauseScanningForManualEntry;

/** @brief Resumes scanning after a pause.
 *  @param delay Whether to wait a preset time before resuming.
 */
- (void) resumeScanningWithDelay:(BOOL)delay;

/** @brief Ends scanning.
 */
- (void) stopScanning;

/** @brief Compensate for device/camera/interface orientation in ZBarView
 */
- (void) updateCamerViewOrientation:(UIInterfaceOrientation)orientation;

/** @brief Gets the flag which determines if the user can scan barcodes in batch.
 *  @return YES, if the user can scan barcodes in batch.
 */
- (BOOL) canScanBarcodeInBatch;

@end
