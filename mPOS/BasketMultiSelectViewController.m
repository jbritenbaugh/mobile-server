//
//  BasketMultiSelectViewController.m
//  mPOS
//
//  Created by John Scott on 21/02/2013.
//  Copyright (c) 2013 Clarity. All rights reserved.
//

#import "BasketMultiSelectViewController.h"
#import "BasketController.h"
#import "BasketItemTableViewCell.h"
#import "CRSSkinning.h"

@implementation BasketMultiSelectViewController

#pragma mark - Private

static NSString *kBasketItemTableViewCellIdentifier = @"kBasketItemTableViewCellIdentifier";

#pragma mark - UIViewController

- (void) viewDidLoad
{
    [super viewDidLoad];
    
    [self.tableView registerClass:[BasketItemTableViewCell class]
           forCellReuseIdentifier:kBasketItemTableViewCellIdentifier];
    
    self.tableView.allowsMultipleSelectionDuringEditing = YES;
    self.tableView.editing = YES;
    self.tableView.rowHeight = 44.0f;
    
    // apply skin
    self.tableView.tag = kTableSkinningTag;
}

- (void) viewWillAppear:(BOOL)animated
{
    [[CRSSkinning currentSkin] applyViewSkin:self];
    [self reloadTable];
    [super viewWillAppear:animated];
}

#pragma mark - UITableViewDataSource

- (NSInteger) numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger) tableView:(UITableView *)tableView
  numberOfRowsInSection:(NSInteger)section
{
    return [self.validBasketItems count];
}

- (UITableViewCell *) tableView:(UITableView *)tableView
          cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    // create a cell that displays the basket items data in the table view.
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kBasketItemTableViewCellIdentifier];
    cell.selectionStyle = UITableViewCellSelectionStyleDefault;

    return cell;
}

- (void) tableView:(UITableView *)tableView
   willDisplayCell:(UITableViewCell *)cell
 forRowAtIndexPath:(NSIndexPath *)indexPath
{
    // get the basket item
    MPOSSaleItem *basketItem = [self.validBasketItems objectAtIndex:indexPath.row];
    NSAssert(basketItem != nil, @"The reference of the requested basket item equals nil.");
    // display it
    [(BasketItemTableViewCell *)cell updateDisplayForBasketItem:basketItem
                                        showSelectionBackground:NO
                                                     tableStyle:tableView.style];
    
    cell.editingAccessoryType = UITableViewCellAccessoryNone;
    
    // skin it
    [[CRSSkinning currentSkin] applyCellSkin:cell
                                    forStyle:tableView.style];
}

#pragma mark - UITableViewDelegate

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    MPOSSaleItem *basketItem = [self.validBasketItems objectAtIndex:indexPath.row];
    if (![self.selectedBasketItems containsObject:basketItem]) {
        [self.selectedBasketItems addObject:basketItem];
    }
}

- (void) tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath
{
    MPOSSaleItem *basketItem = [self.validBasketItems objectAtIndex:indexPath.row];
    if ([self.selectedBasketItems containsObject:basketItem]) {
        [self.selectedBasketItems removeObject:basketItem];
    }
}

#pragma mark - Methods

- (NSArray *) selectedBasketItemIdArray
{
    NSMutableArray *result = [NSMutableArray arrayWithCapacity:self.selectedBasketItems.count];
    
    for (MPOSSaleItem *item in self.selectedBasketItems) {
        [result addObject:item.basketItemId];
    }
    
    return result;
}

- (void) reloadTable
{
    // build the valid item array
    self.validBasketItems = [NSMutableArray array];
    self.selectedBasketItems = [NSMutableSet set];
    for (MPOSSaleItem *basketItem in BasketController.sharedInstance.basket.basketItems) {
        if ([basketItem isKindOfClass:[MPOSSaleItem class]]) {
            [self.validBasketItems addObject:basketItem];
        }
    }
    
    [self.tableView reloadData];
}

@end
