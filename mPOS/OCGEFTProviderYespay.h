//
//  OCGEFTProviderYespay.h
//  mPOS
//
//  Created by Antonio Strijdom on 19/04/2013.
//  Copyright (c) 2013 Clarity. All rights reserved.
//

#import "OCGURLEFTProvider.h"

/** @file OCGEFTProviderYespay.h */

/** @brief Provider class for talking to the YesPay MobileEVT EFT app.
 */
@interface OCGEFTProviderYespay : OCGURLEFTProvider

@end
