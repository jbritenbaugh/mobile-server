//
//  BasketItemTableViewCell.m
//  mPOS
//
//  Created by Meik Schuetz on 15/08/2012.
//  Copyright (c) 2012 Clarity. All rights reserved.
//
#import "BasketItemTableViewCell.h"
#import "RESTController.h"
#import "UIImage+CRSUniversal.h"
#import "UIImage+OCGExtensions.h"
#import "CRSSkinning.h"
#import "ClientSettings+MPOSExtensions.h"

#define CELL_LEFT_MARGIN        @" + 10"
#define CELL_TOP_MARGIN         @" + 5"
#define CELL_RIGHT_MARGIN       @" - 20"
#define CELL_BOTTOM_MARGIN      @" - 5"
#define CELL_HORZ_FIELD_MARGIN  @" + 5"
#define CELL_VERT_FIELD_MARGIN  @" - 2"

@implementation BasketItemTableViewCell
{
    BOOL _shouldShowItemQuantityLabel;
    NSLayoutConstraint *_itemInfoLabelHeightConstraint;
    NSLayoutConstraint *_itemInfoLabelRightConstraint;
    NSLayoutConstraint *_itemQuantityLabelWidthConstraint;
    NSLayoutConstraint *_giftItemImageViewWidthConstraint;
    UILabel *_itemQuantityLabel;
    UILabel *_itemDescriptionLabel;
    UILabel *_itemInfoLabel;
    UILabel *_itemAmountLabel;
    UIImageView *_giftItemImageView;
    UIView *_originalSelectionBackground;
    BOOL _alternativeSkinning;
    UIView *_containerView;
    NSMutableArray *_extraViews;
    NSDateFormatter *_displayDateFormatter;
}

-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self)
    {
        _displayDateFormatter = [[NSDateFormatter alloc] init];
        _displayDateFormatter.dateStyle = NSDateFormatterMediumStyle;
        _displayDateFormatter.timeStyle = NSDateFormatterNoStyle;
        
        _containerView = [[UIView alloc] init];
        _containerView.translatesAutoresizingMaskIntoConstraints = NO;
        [self.contentView addSubview:_containerView];
        [_containerView constrain:@"left = left" to:self.contentView];
        [_containerView constrain:@"right = right" to:self.contentView];
        [_containerView constrain:@"top = top" to:self.contentView];
        
        _originalSelectionBackground = self.selectedBackgroundView;
        
        _itemQuantityLabel = [[UILabel alloc] init];
        _itemQuantityLabel.translatesAutoresizingMaskIntoConstraints = NO;
        _itemQuantityLabel.tag = 4004;
        _itemQuantityLabel.textAlignment = NSTextAlignmentRight;
        [_containerView addSubview:_itemQuantityLabel];
        
        _itemDescriptionLabel = [[UILabel alloc] init];
        _itemDescriptionLabel.translatesAutoresizingMaskIntoConstraints = NO;
        _itemDescriptionLabel.tag = 4005;
        [_containerView addSubview:_itemDescriptionLabel];
        
        _itemInfoLabel = [[UILabel alloc] init];
        _itemInfoLabel.translatesAutoresizingMaskIntoConstraints = NO;
        _itemInfoLabel.tag = 4006;
        [_containerView addSubview:_itemInfoLabel];
        
        _itemAmountLabel = [[UILabel alloc] init];
        _itemAmountLabel.translatesAutoresizingMaskIntoConstraints = NO;
        _itemAmountLabel.tag = 4007;
        _itemAmountLabel.textAlignment = NSTextAlignmentRight;
        [_containerView addSubview:_itemAmountLabel];
        
        _giftItemImageView = [[UIImageView alloc] init];
        _giftItemImageView.hidden = YES;
        _giftItemImageView.opaque = NO;
        _giftItemImageView.translatesAutoresizingMaskIntoConstraints = NO;
        _giftItemImageView.contentMode = UIViewContentModeScaleAspectFill;
        [_containerView addSubview:_giftItemImageView];
        [_itemQuantityLabel constrain:@"left = left" CELL_LEFT_MARGIN to:_containerView];
        _itemQuantityLabelWidthConstraint = [_itemQuantityLabel constrain:@"width = 0" to:nil];
        [_itemQuantityLabel constrain:@"top = top" CELL_TOP_MARGIN to:_containerView];
        [_itemQuantityLabel constrain:@"bottom = top + 39" to:_containerView];
        [_itemDescriptionLabel constrain:@"top = top" CELL_TOP_MARGIN to:_containerView];
        [_itemDescriptionLabel constrain:@"left = right" CELL_HORZ_FIELD_MARGIN to:_itemQuantityLabel];
        [_itemDescriptionLabel constrain:@"bottom = top" CELL_VERT_FIELD_MARGIN to:_itemInfoLabel];
        [_itemInfoLabel constrain:@"left = right" CELL_HORZ_FIELD_MARGIN to:_itemQuantityLabel];
        [_itemDescriptionLabel constrain:@"right = right" CELL_VERT_FIELD_MARGIN to:_itemInfoLabel];
        [_itemInfoLabel constrain:@"bottom = top + 39" to:_containerView];
        [_containerView constrain:@"height > 44" to:nil];
        
        [_giftItemImageView constrain:@"height = 22" to:nil];
        _giftItemImageViewWidthConstraint = [_giftItemImageView constrain:@"width = 0" to:nil];
        [_giftItemImageView constrain:@"centerY = centerY" to:_containerView];
        [_giftItemImageView constrain:@"left = right" CELL_HORZ_FIELD_MARGIN to:_itemDescriptionLabel];
        
        [_itemAmountLabel constrain:@"left = right" CELL_HORZ_FIELD_MARGIN to:_giftItemImageView];
        if ([UIDevice currentDevice].systemVersion.floatValue >= 7.0) {
            // grouped table views are virtually identical in iOS 7, so this contraint isn't
            // neccessary (plus it breaks edit mode)
            _itemInfoLabelRightConstraint = nil;
            [self addConstraint:[_itemAmountLabel buildConstraint:@"right = right - 35"  with:self priority:UILayoutPriorityDefaultHigh]];
            [_itemAmountLabel constrain:@"right < right"  to:self.contentView];
        } else {
            _itemInfoLabelRightConstraint = [_itemAmountLabel constrain:@"right = right"  to:self];
        }
        [_itemAmountLabel constrain:@"top = top" CELL_TOP_MARGIN to:_containerView];
        [_itemAmountLabel constrain:@"bottom = top + 39" to:_containerView];
        [_itemAmountLabel constrain:@"width = 0.3 * width" to:_containerView];
        _itemInfoLabelHeightConstraint = [_itemInfoLabel constrain:@"height = 0" to:nil];
        
        _itemQuantityLabel.backgroundColor = [UIColor clearColor];
        _itemDescriptionLabel.backgroundColor = [UIColor clearColor];
        _itemInfoLabel.backgroundColor = [UIColor clearColor];
        _itemAmountLabel.backgroundColor = [UIColor clearColor];
        _giftItemImageView.backgroundColor = [UIColor clearColor];
        
        if ([UIDevice currentDevice].systemVersion.floatValue >= 7.0)
        {
            _itemQuantityLabel.highlightedTextColor = nil;
            _itemDescriptionLabel.highlightedTextColor = nil;
            _itemInfoLabel.highlightedTextColor = nil;
            _itemAmountLabel.highlightedTextColor = nil;
            _giftItemImageView.highlightedImage = nil;
        }
        else
        {
            _itemQuantityLabel.highlightedTextColor = [UIColor whiteColor];
            _itemDescriptionLabel.highlightedTextColor = [UIColor whiteColor];
            _itemInfoLabel.highlightedTextColor = [UIColor whiteColor];
            _itemAmountLabel.highlightedTextColor = [UIColor whiteColor];
        }
        
        _extraViews = [NSMutableArray array];
    }
    return self;
}

/** @brief Prepares the receiver for service after it has been loaded from an
 *  Interface Builder archive, or nib file.
 */
- (void) awakeFromNib
{
    // make sure we call the super class
    
    [super awakeFromNib];
    
}

#pragma mark Overridden Methods

-(void)updateDisplayForBasketItem:(MPOSBasketItem*)basketItem
                       tableStyle:(UITableViewStyle)tableStyle
{
    [self updateDisplayForBasketItem:basketItem
             showSelectionBackground:YES
                          tableStyle:tableStyle];
}

-(void)updateDisplayForBasketItem:(MPOSBasketItem*)basketItem
          showSelectionBackground:(BOOL)showSelection
                       tableStyle:(UITableViewStyle)tableStyle
{
    [self clearExtraViews];
    if (showSelection) {
        self.selectedBackgroundView = _originalSelectionBackground;
    } else {
        UIView *selectedBackground = [[UIView alloc] init];
        selectedBackground.backgroundColor = [UIColor clearColor];
        self.selectedBackgroundView = selectedBackground;
    }
    
    NSString *type = NSStringFromClass([basketItem class]);
    
    _shouldShowItemQuantityLabel = YES;
    _alternativeSkinning = NO;
    _itemAmountLabel.attributedText = nil;
    _giftItemImageView.hidden = YES;
    UIImage *giftItemImage = nil;
    
    if ([type isEqualToString:@"ReturnItem"]/* && [basketItem.quantity length] > 0 WTF?*/)
    {
        _itemQuantityLabel.text = basketItem.quantity;
        _itemDescriptionLabel.text = basketItem.desc;
        _itemInfoLabel.text = @"";
        _itemAmountLabel.text = basketItem.pricing.display;
        _alternativeSkinning = YES;
        giftItemImage = [UIImage OCGExtensions_imageNamed:@"ADD_RETURN_ITEM"
                                                     tint:[UIColor colorWithRed:1 green:0 blue:0 alpha:0.6]];
    }
    else if ([type isEqualToString:@"BasketCustomerCard"])
    {
        MPOSBasketCustomerCard *basketCustomerCard = (MPOSBasketCustomerCard*) basketItem;
        _itemQuantityLabel.text = @"";
        if ([basketCustomerCard.customerDisplayName length] > 0)
        {
            _itemDescriptionLabel.text = basketCustomerCard.customerDisplayName;
        }
        else
        {
            _itemDescriptionLabel.text = basketCustomerCard.cardName;
        }
        
        _itemInfoLabel.text = basketCustomerCard.customerCardNumber;
        _itemAmountLabel.text = @"";
    }
    else if ([type isEqualToString:@"BasketTender"])
    {
        MPOSBasketTender *basketTender = (MPOSBasketTender*) basketItem;
        _itemQuantityLabel.text = basketItem.quantity;
        _itemDescriptionLabel.text = basketItem.desc;
        _itemInfoLabel.text = basketItem.desc;
        if (basketTender.foreignCurrency != nil)
        {
            _itemAmountLabel.text = basketTender.foreignCurrency.displayAmount;
        }
        else
        {
            _itemAmountLabel.text = basketTender.tenderAmount.display;
        }
    }
    else if ([type isEqualToString:@"BasketCustomerAffiliation"])
    {
        MPOSBasketCustomerAffiliation *basketCustomerAffiliation = (MPOSBasketCustomerAffiliation *)basketItem;
        _itemQuantityLabel.text = @"";
        _itemDescriptionLabel.text = basketCustomerAffiliation.affiliationName;
        _itemInfoLabel.text = basketCustomerAffiliation.affiliationNumber;
        _itemAmountLabel.text = @"";
    }
    else if ([type isEqualToString:@"BasketAssociateInfo"])
    {
        BasketAssociateInfo *basketAssociateInfo = (BasketAssociateInfo *)basketItem;
        _itemQuantityLabel.text = @"";
        _itemDescriptionLabel.text = basketAssociateInfo.operatorDisplayName;
        _itemInfoLabel.text = basketAssociateInfo.operatorId;
        _itemAmountLabel.text = @"";
    }
    else if ([type isEqualToString:@"ReturnableSaleItem"])
    {
        ReturnableSaleItem *returnableSaleItem = (ReturnableSaleItem*) basketItem;

        _itemQuantityLabel.text = basketItem.quantity;

        _itemDescriptionLabel.text = returnableSaleItem.desc;
        _itemAmountLabel.text = returnableSaleItem.pricing.display;
        
        __block UIView *previousLineView = _itemInfoLabel;
        
        void (^extraLineBlock)(NSString *) = ^void(NSString *text)
        {
            UILabel *currentLineView = [[UILabel alloc] init];
            currentLineView.translatesAutoresizingMaskIntoConstraints = NO;
            currentLineView.tag = kBasketItemCellDetailSkinningTag;
            [_containerView addSubview:currentLineView];
            [_extraViews addObject:currentLineView];
            currentLineView.text = text;
            
            currentLineView.backgroundColor = [UIColor clearColor];
            
            [currentLineView constrain:@"top = bottom" to:previousLineView];
            [currentLineView constrain:@"height = 20" to:nil];
            [currentLineView constrain:@"left = left"  to:_itemInfoLabel];
            [currentLineView constrain:@"right = right"  to:_containerView];
            
            [_containerView constrain:@"bottom > bottom + 5" to:currentLineView];
            
            previousLineView = currentLineView;
            
            if ([UIDevice currentDevice].systemVersion.floatValue >= 7.0)
            {
                currentLineView.highlightedTextColor = nil;
            }
            else
            {
                currentLineView.highlightedTextColor = [UIColor whiteColor];
            }
        };
        
        [returnableSaleItem.returnInstances enumerateObjectsUsingBlock:^(ReturnInstance *returnInstance, NSUInteger idx, BOOL *stop) {
            extraLineBlock([NSString stringWithFormat:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠀲󠁪󠁑󠁥󠁡󠁗󠁹󠁯󠁧󠁚󠁵󠀹󠁨󠀹󠁸󠁩󠁭󠁒󠀯󠁓󠁋󠁰󠁗󠁰󠁣󠁴󠁙󠁿*/ @"Returned: %@", nil), [_displayDateFormatter stringFromDate:returnInstance.date]]);
        }];
        
        NSInteger availableForReturn = [returnableSaleItem.quantity integerValue] - [returnableSaleItem.returnedQuantity integerValue];
        extraLineBlock([NSString stringWithFormat:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁏󠁤󠁦󠀷󠁤󠁅󠁧󠁇󠀲󠁤󠁸󠁉󠁇󠁐󠁂󠁵󠁋󠁦󠀸󠁋󠁔󠁐󠁖󠁶󠁬󠁊󠁕󠁿*/ @"%d available for return", nil), availableForReturn]);
    }
    else if ([type isEqualToString:@"BasketTaxFree"])
    {
        BasketTaxFree *taxFree = (BasketTaxFree *)basketItem;
        _itemDescriptionLabel.text = taxFree.documentId;
        _itemInfoLabel.text = taxFree.documentType;
        giftItemImage = [UIImage OCGExtensions_imageNamed:@"TAX_FREE_SALE"
                                                      tint:[UIColor colorWithWhite:0 alpha:0.15]];
    }
    else if ([type hasPrefix:@"BasketRewardSale"]) {
        _itemQuantityLabel.text = @"";
        _itemDescriptionLabel.text = basketItem.desc;
        _itemInfoLabel.text = @"";
        _itemAmountLabel.text = ((MPOSBasketRewardSale *)basketItem).rewardAmount.display;
    }
    else
    {
        _itemQuantityLabel.text = basketItem.quantity;
        _itemDescriptionLabel.text = basketItem.desc;
        _itemInfoLabel.text = @"";
        _itemAmountLabel.text = basketItem.pricing.display;

        if ((basketItem.basketRewards != nil) && (basketItem.basketRewards.count > 0)) {
            _itemAmountLabel.attributedText =
            [[NSAttributedString alloc] initWithString:_itemAmountLabel.text
                                            attributes:@{ NSUnderlineStyleAttributeName: @(NSUnderlineStyleSingle)}];
        }
        
        if ([type isEqualToString:@"SaleItem"] && ((MPOSSaleItem *)basketItem).generateGiftReceiptValue)
        {
            giftItemImage = [UIImage OCGExtensions_imageNamed:@"PRINT_GIFT_RECEIPT"
                                                          tint:[UIColor colorWithWhite:0 alpha:0.15]];
        }
        
    }
    
    _giftItemImageView.hidden = !giftItemImage;
    _giftItemImageView.image = giftItemImage;
    if ([UIDevice currentDevice].systemVersion.floatValue < 7.0)
    {
        _giftItemImageView.highlightedImage = [giftItemImage imageTintedWithColor:[UIColor colorWithWhite:1 alpha:0.4]];
    }

    [self updateSkinningWithTableStyle:tableStyle];
}

-(void)updateDisplayForItem:(MPOSItem*)item
                 tableStyle:(UITableViewStyle)tableStyle
{
    [self clearExtraViews];
    _shouldShowItemQuantityLabel = NO;
    _alternativeSkinning = NO;
    _itemQuantityLabel.text = nil;
    _giftItemImageView.hidden = YES;
    _itemAmountLabel.attributedText = nil;
    _itemDescriptionLabel.text = item.shortDesc;
    if (![item.shortDesc isEqual:item.longDesc])
    {
        _itemInfoLabel.text = item.longDesc;
    }
    else
    {
        _itemInfoLabel.text = @"";
    }
    _itemAmountLabel.text = item.displayPrice;
    
    [self updateSkinningWithTableStyle:tableStyle];
}

-(void)updateWithRewardTotal:(NSString*)rewardTotal
                  tableStyle:(UITableViewStyle)tableStyle
{
    [self clearExtraViews];
    _shouldShowItemQuantityLabel = YES;
    _itemAmountLabel.attributedText = nil;
    _giftItemImageView.hidden = YES;
    _alternativeSkinning = YES;
    _itemQuantityLabel.text = @"";
    _itemDescriptionLabel.text = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠀷󠁩󠁋󠁑󠁁󠁗󠁢󠁺󠁳󠁒󠁗󠁥󠁚󠁕󠁲󠀶󠁖󠀯󠁲󠀵󠁗󠁌󠁊󠁥󠁉󠀷󠁑󠁿*/ @"Discounts", nil);
    _itemInfoLabel.text = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁓󠁭󠀳󠁔󠀸󠁺󠁩󠁹󠀰󠁤󠁺󠀷󠁧󠁧󠁍󠁉󠁅󠁪󠀲󠁩󠁳󠀷󠁋󠀯󠁔󠁍󠁕󠁿*/ @"including promotions", nil);
    _itemAmountLabel.text = rewardTotal;
    [self updateSkinningWithTableStyle:tableStyle];
}

-(void)updateSkinningWithTableStyle:(UITableViewStyle)tableStyle
{
    if (_giftItemImageView.hidden)
    {
        _giftItemImageViewWidthConstraint.constant = 0;
    }
    else
    {
        _giftItemImageViewWidthConstraint.constant = 22;
    }
    
    if ([_itemInfoLabel.text length] > 0)
    {
        _itemInfoLabelHeightConstraint.constant = 15;
    }
    else
    {
        _itemInfoLabelHeightConstraint.constant = 0;
    }
    
    if (_shouldShowItemQuantityLabel)
    {
        _itemQuantityLabelWidthConstraint.constant = 30;
    }
    else
    {
        _itemQuantityLabelWidthConstraint.constant = 0;
    }
    
    CGFloat rightCellIndent = 0;
    CGFloat accessoryViewWidth = 30;
    
    if (tableStyle == UITableViewStyleGrouped)
    {
        accessoryViewWidth = 5;
        if ([UIDevice currentDevice].systemVersion.floatValue < 7.0)
        {
            if (BasketLayoutGrid == BasketController.sharedInstance.clientSettings.parsedBasketLayout)
            {
                rightCellIndent = 45;
            }
            else
            {
                rightCellIndent = 9;
            }
        }
    }
    
    _itemInfoLabelRightConstraint.constant = -(rightCellIndent + accessoryViewWidth);
    
    if (_alternativeSkinning) {
        self.tag = kBasketItemAltCellSkinningTag;
        _itemQuantityLabel.tag = kBasketItemAltCellSkinningTag;
        _itemAmountLabel.tag = kBasketItemAltCellSkinningTag;
        _itemDescriptionLabel.tag = kBasketItemAltCellSkinningTag;
        _itemInfoLabel.tag = kBasketItemAltCellDetailSkinningTag;
    }
    else
    {
        self.tag = kBasketItemCellSkinningTag;
        _itemQuantityLabel.tag = kBasketItemCellSkinningTag;
        _itemAmountLabel.tag = kBasketItemCellSkinningTag;
        _itemDescriptionLabel.tag = kBasketItemCellSkinningTag;
        _itemInfoLabel.tag = kBasketItemCellDetailSkinningTag;
    }
}

-(void)clearExtraViews
{
    for (UIView *view in _extraViews)
    {
        [view removeFromSuperview];
    }
    [_extraViews removeAllObjects];
}

-(CGSize)systemLayoutSizeFittingSize:(CGSize)targetSize
{
    // Configure the cell with content for the given indexPath, for example:
    // cell.textLabel.text = someTextForThisCell;
    // ...
    
    // Make sure the constraints have been set up for this cell, since it may have just been created from scratch.
    // Use the following lines, assuming you are setting up constraints from within the cell's updateConstraints method:
    [self setNeedsUpdateConstraints];
    [self updateConstraintsIfNeeded];
    
    // Do the layout pass on the cell, which will calculate the frames for all the views based on the constraints.
    // (Note that you must set the preferredMaxLayoutWidth on multi-line UILabels inside the -[layoutSubviews] method
    // of the UITableViewCell subclass, or do it manually at this point before the below 2 lines!)
    [self setNeedsLayout];
    [self layoutIfNeeded];
    
    CGSize size= [super systemLayoutSizeFittingSize:targetSize];
    
    size.height += _containerView.frame.size.height - self.contentView.frame.size.height;
    
    return _containerView.frame.size;
}

@end
