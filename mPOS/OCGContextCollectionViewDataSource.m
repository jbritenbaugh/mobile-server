//
//  OCGContextCollectionViewDataSource.m
//  mPOS
//
//  Created by Antonio Strijdom on 21/07/2014.
//  Copyright (c) 2014 Omnico Group. All rights reserved.
//

#import "OCGContextCollectionViewDataSource.h"
#import "FNKSupplementaryView.h"
#import "ContextViewController.h"
#import "OCGCollectionViewCell.h"
#import "POSFuncController.h"
#import "NSIndexPath+OCGExtensions.h"
#import "CRSSkinning.h"
#import "UIImage+OCGExtensions.h"
#import "OCGTextField.h"
#import "ClientSettings+MPOSExtensions.h"
#import "NSDate+OCGExtensions.h"

@interface OCGContextCollectionViewDataSource () <UITextFieldDelegate> {
    NSMutableDictionary *_dataSourceCache;
}
/** @property level
 *  @brief The level the data source is in the data source hierarchy.
 */
@property (assign, nonatomic) FNKSupplementaryOptions level;
/** @property sectionTitle
 *  @brief The title of the section this data source represents.
 */
@property (strong, nonatomic) NSString *sectionTitle;
/** @property sectionImage
 *  @brief The icon of the section this data source represents.
 */
@property (strong, nonatomic) NSString *sectionImage;
/** @property sectionDataSources
 *  @brief An array that maps sections to data sources
 */
@property (strong, nonatomic) NSMutableArray *sectionDataSources;
@property (strong, nonatomic) NSArray *headerItems;
@property (strong, nonatomic) NSArray *footerItems;
- (BOOL)object:(id)object hasKeyPath:(NSString *)path;
- (UIImage *) imageForTenderClass:(NSString *)tenderClass;
- (UIColor *) colorFromRGBString:(NSString *)string;
- (OCGContextCollectionViewDataSource *)buildDataSourcesFromSoftKey:(SoftKey *)softKey
                                                    withDisplayData:(id)displayData;
- (SoftKey *)softKeyWithId:(NSString *)keyId
                 inContext:(SoftKeyContext *)context;
- (OCGContextCollectionViewDataSource *)dataSourceForIndexPath:(NSIndexPath *)indexPath
                                                   headerItems:(NSMutableArray **)headerItems
                                                   footerItems:(NSMutableArray **)footerItems;
- (OCGContextCollectionViewDataSource *)findChildDataSourceForIndexPath:(NSIndexPath *)indexPath
                                                       withRunningTotal:(NSUInteger)runningTotal
                                                            headerItems:(NSMutableArray **)headerItems
                                                            footerItems:(NSMutableArray **)footerItems;
- (NSUInteger)updateMappingForChildDataSourceWithRunningTotal:(NSUInteger)runningTotal;
- (BOOL)isDefaultKey;
@end

@implementation OCGContextCollectionViewDataSource

#pragma mark - Private

static NSString *viewReuseIdentifier = @"FUA";

- (BOOL)object:(id)object hasKeyPath:(NSString *)path
{
    BOOL result = NO;
    
    NSArray *pathComponents = [path componentsSeparatedByString:@"."];
    if (pathComponents.count > 1) {
        result = [object respondsToSelector:NSSelectorFromString(pathComponents[0])];
        if (result) {
            id propertyValue = [object valueForKeyPath:pathComponents[0]];
            NSRange propertyRange = NSMakeRange(1, pathComponents.count - 1);
            NSString *propertyPath = [[pathComponents subarrayWithRange:propertyRange] componentsJoinedByString:@"."];
            result = [self object:propertyValue hasKeyPath:propertyPath];
        }
    } else if (pathComponents.count == 1) {
        result = [object respondsToSelector:NSSelectorFromString(pathComponents[0])];
    }
    
    return result;
}

- (UIImage *) imageForTenderClass:(NSString *)tenderClass
{
    UIImage *result = nil;
    
    NSString *iconName = [NSString stringWithFormat:@"tender_class_%@", [tenderClass lowercaseString]];
    result = [UIImage OCGExtensions_imageNamed:iconName];
    
    return result;
}

- (UIColor *) colorFromRGBString:(NSString *)string
{
    UIColor *result = nil;
    
    if ((string != nil) && (![string isEqualToString:@""])) {
        // split the colour into RGB
        NSRange compRange = NSMakeRange(0, 2);
        NSString *r = [@"0x" stringByAppendingString:[string substringWithRange:compRange]];
        compRange.location = 2;
        NSString *g = [@"0x" stringByAppendingString:[string substringWithRange:compRange]];
        compRange.location = 4;
        NSString *b = [@"0x" stringByAppendingString:[string substringWithRange:compRange]];
        // convert
        NSScanner *rScanner = [NSScanner scannerWithString:r];
        float rValue = 0;
        NSScanner *gScanner = [NSScanner scannerWithString:g];
        float gValue = 0;
        NSScanner *bScanner = [NSScanner scannerWithString:b];
        float bValue = 0;
        if ([rScanner scanHexFloat:&rValue] &&
            [gScanner scanHexFloat:&gValue] &&
            [bScanner scanHexFloat:&bValue]) {
            // build the colour
            result = [UIColor colorWithRed:rValue / 255.0
                                     green:gValue / 255.0
                                      blue:bValue / 255.0
                                     alpha:1.0];
        }
    }
    
    return result;
}

- (OCGContextCollectionViewDataSource *)buildDataSourcesFromSoftKey:(SoftKey *)softKey
                                                    withDisplayData:(id)displayData
{
    OCGContextCollectionViewDataSource *result = [[OCGContextCollectionViewDataSource alloc] init];
    NSMutableArray *childDataSources = [NSMutableArray array];
    
    // if we are processing a menu
    if ([softKey.keyType hasSuffix:@"MENU"]) {
        // create a datasource for the menu
        result = [[OCGContextCollectionViewDataSource alloc] init];
        result.sectionTitle = softKey.keyDescription;
        result.sectionImage = softKey.upImage;
        result.softKey = softKey;
        // split up the root menu
        NSString *trimmmedKeys = [softKey.softKeyMenu stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        NSArray *menuKeys = [trimmmedKeys componentsSeparatedByCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        // iterate through the key ids
        for (NSString *menuKeyId in menuKeys) {
            if (menuKeyId.length > 0) {
                SoftKey *menuKey = [result softKeyWithId:menuKeyId
                                               inContext:softKey.context];
                if (menuKey) {
                    // if we are processing a menu
                    if ([menuKey.keyType hasSuffix:@"MENU"]) {
                        OCGContextCollectionViewDataSource *childDataSource =
                        [result buildDataSourcesFromSoftKey:menuKey
                                            withDisplayData:displayData];
                        childDataSource.parentDataSource = result;
                        if ([menuKey.keyType isEqualToString:@"TENDER_MENU"]) {
                            childDataSource.level = FNKSupplementaryOptionDropdown;
                            // display the default selection as the section title
                            if (result.sectionTitle.length == 0) {
                                for (OCGContextCollectionViewDataSource *tender in childDataSource.childDataSources) {
                                    if ([tender isDefaultKey]) {
                                        result.sectionTitle = tender.softKey.keyDescription;
                                    }
                                }
                            }
                        }
                        [childDataSources addObject:childDataSource];
                    } else {
                        // check if this is a collection display softkey
                        if (([menuKey.keyType isEqualToString:@"DISPLAY_INFO"]) &&
                            ([menuKey.data isEqualToString:@"basketItems"])) {
                            // at this point, displayData is assumed to be the array of basket items
                            NSArray *basketItems = nil;
                            if ([displayData isKindOfClass:[NSArray class]]) {
                                basketItems = (NSArray *)displayData;
                            }
                            NSUInteger numberOfChildDataSourcesAdded = 0;
                            if ((basketItems != nil) && (basketItems.count > 0)) {
                                for (MPOSBasketItem *basketItem in basketItems) {
                                    if (![basketItem isKindOfClass:[NSNull class]]) {
                                        // get the layout for the basket item
                                        SoftKeyContext *menu = [[ContextViewController sharedInstance] contextLayoutMenuForBasketItem:basketItem];
                                        if (menu) {
                                            OCGContextCollectionViewDataSource *basketItemDataSource =
                                            [OCGContextCollectionViewDataSource dataSourceForContext:menu
                                                                                            withData:basketItem];
                                            basketItemDataSource.parentDataSource = result;
                                            basketItemDataSource.level = FNKSupplementaryOptionTabbed;
                                            switch ([ContextViewController sharedInstance].currentContext) {
                                                case MPOSContextCheckedOut: {
                                                    if (![basketItem isKindOfClass:[BasketTender class]]) {
                                                        basketItemDataSource.level |= FNKSupplementaryOptionLockToDefault;
                                                    }
                                                    break;
                                                }
                                                case MPOSContextSaleComplete:
                                                    basketItemDataSource.level |= FNKSupplementaryOptionLockToDefault;
                                                    break;
                                                default:
                                                    break;
                                            }
                                            numberOfChildDataSourcesAdded++;
                                            [childDataSources addObject:basketItemDataSource];
                                        }
                                    }
                                }
                            }
                            if (numberOfChildDataSourcesAdded == 0)
                            {
                                result = nil;
                            }
                        } else if (([menuKey.keyType isEqualToString:@"DISPLAY_INFO"]) &&
                                   ([menuKey.data isEqualToString:@"basketRewards"])) {
                            // display rewards
                            if ([displayData isKindOfClass:[MPOSBasketItem class]]) {
                                // at this point displayData is assumed to be either a basket or a basket item
                                if (((MPOSBasketItem *)displayData).basketRewards != nil) {
                                    NSArray *rewards = ((MPOSBasketItem *)displayData).basketRewards;
                                    for (MPOSBasketRewardLine *reward in rewards) {
                                        // TODO : get the layout for the basket reward
                                        SoftKeyContext *menu =
                                        [[BasketController sharedInstance] getLayoutForContextNamed:NSStringForSoftKeyContextType(SoftKeyContextTypeMenuItemItemrewardLayout)];
                                        if (menu) {
                                            OCGContextCollectionViewDataSource *basketRewardDataSource =
                                            [OCGContextCollectionViewDataSource dataSourceForContext:menu
                                                                                            withData:reward];
                                            basketRewardDataSource.level = FNKSupplementaryOptionTabbed;
                                            basketRewardDataSource.parentDataSource = result;
                                            [childDataSources addObject:basketRewardDataSource];
                                        }
                                    }
                                    //TFS82743. Only display discount on refund items when basket rewards are present
                                    if([rewards count] == 0)
                                    {
                                        result = nil;
                                    }
                                }
                            }
                        } else if (([menuKey.keyType isEqualToString:@"DISPLAY_INFO"]) &&
                                   ([menuKey.data isEqualToString:@"selectedModifiers"])) {
                            // display modifiers
                            if ([displayData isKindOfClass:[MPOSBasketItem class]]) {
                                // at this point displayData is assumed to be either a basket or a basket item
                                if (((MPOSBasketItem *)displayData).basketRewards != nil) {
                                    NSArray *modifiers = ((MPOSBasketItem *)displayData).selectedModifiers;
                                    for (MPOSSelectedModifier *modifier in modifiers) {
                                        // get the layout for the modifier
                                        SoftKeyContext *menu =
                                        [[BasketController sharedInstance] getLayoutForContextNamed:NSStringForSoftKeyContextType(SoftKeyContextTypeMenuItemModifierLayout)];
                                        if (menu) {
                                            OCGContextCollectionViewDataSource *basketModifierDataSource =
                                            [OCGContextCollectionViewDataSource dataSourceForContext:menu
                                                                                            withData:modifier];
                                            basketModifierDataSource.level = FNKSupplementaryOptionTabbed;
                                            basketModifierDataSource.parentDataSource = result;
                                            [childDataSources addObject:basketModifierDataSource];
                                        }
                                    }
                                }
                            }
                        } else if (([menuKey.keyType isEqualToString:@"DISPLAY_INFO"]) &&
                                   ([menuKey.data isEqualToString:@"suspendedBaskets"])) {
                            // at this point, displayData is assumed to be the array of baskets
                            NSArray *baskets = nil;
                            if ([displayData isKindOfClass:[NSArray class]]) {
                                baskets = (NSArray *)displayData;
                            }
                            if ((baskets != nil) && (baskets.count > 0)) {
                                for (BasketDTO *basket in baskets) {
                                    if (![basket isKindOfClass:[NSNull class]]) {
                                        // get the layout for the basket item
                                        SoftKeyContext *menu =
                                        [[BasketController sharedInstance] getLayoutForContextNamed:NSStringForSoftKeyContextType(SoftKeyContextTypeSuspendedBasketLayout)];
                                        if (menu) {
                                            OCGContextCollectionViewDataSource *basketDataSource =
                                            [OCGContextCollectionViewDataSource dataSourceForContext:menu
                                                                                            withData:basket];
                                            basketDataSource.parentDataSource = result;
                                            basketDataSource.level = FNKSupplementaryOptionTabbed;
                                            [childDataSources addObject:basketDataSource];
                                        }
                                    }
                                }
                            } else {
                                result = nil;
                            }
                        } else {
                            if (![menuKey.keyType isEqualToString:@"POS_TENDER"] ||
                                [BasketController.sharedInstance.tenders.allKeys containsObject:menuKey.tenderType]) {
                                OCGContextCollectionViewDataSource *displayItem = [[OCGContextCollectionViewDataSource alloc] init];
                                displayItem.displayItem = displayData;
                                displayItem.softKey = menuKey;
                                displayItem.parentDataSource = result;
                                [childDataSources addObject:displayItem];
                            }
                        }
                    }
                } else {
//                    NSAssert(menuKey != nil, @"Could not find key for %@", menuKeyId);
                }
            }
        }
    }
    
    result.childDataSources = childDataSources;
    
    return result;
}

- (SoftKey *)softKeyWithId:(NSString *)keyId
                 inContext:(SoftKeyContext *)context
{
    SoftKey *result = [BasketController.sharedInstance softKeyForKeyId:keyId softKeyContext:context];
    return result;
}

- (OCGContextCollectionViewDataSource *)dataSourceForIndexPath:(NSIndexPath *)indexPath
                                                   headerItems:(NSMutableArray **)headerItems
                                                   footerItems:(NSMutableArray **)footerItems
{
    // initialise
    OCGContextCollectionViewDataSource *result = nil;
    NSInteger runningRowCount = 0;
    NSInteger section = indexPath.section;
    
    // check the cache
    result = [_dataSourceCache objectForKey:indexPath];
    if (result) {
        if (headerItems) {
            if (result.headerItems) {
                [*headerItems addObjectsFromArray:result.headerItems];
            } else {
                result = nil;
            }
        }
        if (footerItems) {
            if (result.footerItems) {
                [*footerItems addObjectsFromArray:result.footerItems];
            } else {
                result = nil;
            }
        }
    }
    // if there wasn't a suitable cached version
    if (result == nil) {
        // find the data source for the section
        result = [self dataSourceForSection:indexPath.section];
        
        if (result) {
            // add the section header (if applicable)
            if (headerItems) {
                if (result.childDataSources.count > 0) {
                    if (indexPath.row - runningRowCount == 0) {
                        NSString *sectionTitle = (result.sectionTitle == nil ? @"" : result.sectionTitle);
                        
                        FNKSupplementaryItem *sectionItem = [[FNKSupplementaryItem alloc] init];
                        sectionItem.title = sectionTitle;
                        sectionItem.image = [UIImage OCGExtensions_imageNamed:result.sectionImage];
                        sectionItem.options = result.level;
                        
                        [*headerItems addObject:sectionItem];
                    }
                }
            }
            // add the section footer (if applicable)
            if (footerItems) {
                if (result.childDataSources.count > 0) {
                    if (indexPath.row == result.totalRowCount - 1) {
                        FNKSupplementaryItem *sectionItem = [[FNKSupplementaryItem alloc] init];
                        [*footerItems addObject:sectionItem];
                    }
                }
            }
            // process children
            if (result.childDataSources.count > 0) {
                result = [result findChildDataSourceForIndexPath:indexPath
                                                withRunningTotal:runningRowCount
                                                    headerItems:headerItems
                                                    footerItems:footerItems];
            } else {
                // no children, we are done
                NSAssert(runningRowCount + result.totalRowCount >= indexPath.row, @"Insufficient data sources for row count!");
            }
            // update the cache
            [_dataSourceCache setObject:result
                                 forKey:indexPath];
            // make a note of the header items
            if (headerItems) {
                result.headerItems = [NSArray arrayWithArray:*headerItems];
            }
            // make a note of the footer items
            if (footerItems) {
                result.footerItems = [NSArray arrayWithArray:*footerItems];
            }
        }
    }
    
    // return the data source
    return result;
}

- (OCGContextCollectionViewDataSource *)findChildDataSourceForIndexPath:(NSIndexPath *)indexPath
                                                       withRunningTotal:(NSUInteger)runningTotal
                                                            headerItems:(NSMutableArray **)headerItems
                                                            footerItems:(NSMutableArray **)footerItems
{
    // init
    OCGContextCollectionViewDataSource *result = nil;
    
    // iterate through children till we find the datasource that contains
    // the row requested
    NSUInteger runningRowCount = runningTotal;
    for (OCGContextCollectionViewDataSource *dataSource in self.childDataSources) {
        // check if this data source's row count exceeds the row we're after
        if (runningRowCount + dataSource.totalRowCount > indexPath.row) {
            // make a note of the data source
            result = dataSource;
            break;
        } else {
            runningRowCount += dataSource.totalRowCount;
        }
    }
    if (result) {
        // add the section header (if applicable)
        if (headerItems) {
            if (result.childDataSources.count > 0) {
                if (indexPath.row - runningRowCount == 0) {
                    NSString *sectionTitle = (result.sectionTitle == nil ? @"" : result.sectionTitle);
                    FNKSupplementaryItem *sectionItem = [[FNKSupplementaryItem alloc] init];
                    sectionItem.title = sectionTitle;
                    sectionItem.image = [UIImage OCGExtensions_imageNamed:result.sectionImage];
                    sectionItem.options = result.level;
                    
                    [*headerItems addObject:sectionItem];
                }
            }
        }
        // add the section footer (if applicable)
        if (footerItems) {
            if (result.childDataSources.count > 0) {
                if (indexPath.row == runningRowCount + result.totalRowCount - 1) {
                    NSString *sectionTitle = (result.sectionTitle == nil ? @"" : result.sectionTitle);
                    FNKSupplementaryItem *sectionItem = [[FNKSupplementaryItem alloc] init];
                    [*footerItems addObject:sectionItem];
                }
            }
        }
        // if this data source has children
        if (result.childDataSources.count > 0) {
            // look for the datasource in this datasource
            result =
            [result findChildDataSourceForIndexPath:indexPath
                                   withRunningTotal:runningRowCount
                                        headerItems:headerItems
                                        footerItems:footerItems];
        } else {
            // we are done
            NSAssert(runningRowCount + result.totalRowCount >= indexPath.row, @"Insufficient data sources for row count!");
        }
    } else {
        result = self;
    }
    
    return result;
}

- (NSUInteger)updateMappingForChildDataSourceWithRunningTotal:(NSUInteger)runningTotal
{
    NSUInteger result = runningTotal;
    
    // resursively iterate through child data sources
    for (OCGContextCollectionViewDataSource *childDataSource in self.childDataSources) {
        childDataSource.sectionCount = childDataSource.childDataSources.count;
        // calculate the child row count (i.e. how many rows this child contributes to the section it is in)
        NSUInteger childRowCount = 0;
        // if this child has children
        if (childDataSource.childDataSources.count > 0) {
            // add its children's row count
            childRowCount = [childDataSource updateMappingForChildDataSourceWithRunningTotal:0];
        } else {
            // no children, this must be a display item, just add the one row
            childRowCount = 1;
        }
        childDataSource.totalRowCount = childRowCount;
        result += childRowCount;
    }
    
    return result;
}

- (BOOL)isDefaultKey
{
    BOOL result = NO;
    
    // check if this is a default key
    if (self.parentDataSource) {
        if (self.parentDataSource.softKey) {
            if ([self.parentDataSource.softKey.keyType hasSuffix:@"MENU"]) {
                if ([self.softKey.keyId isEqualToString:self.parentDataSource.softKey.defaultSelection]) {
                    result = YES;
                }
            }
        }
    }
    
    return result;
}

#pragma mark - Init

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.childDataSources = [NSArray array];
        self.sectionDataSources = [NSMutableArray array];
        self.sectionTitle = nil;
        self.sectionImage = nil;
        self.level = FNKSupplementaryOptionNone;
        _dataSourceCache = [NSMutableDictionary dictionary];
    }
    return self;
}

+ (OCGContextCollectionViewDataSource *)dataSourceForContext:(SoftKeyContext *)context
                                                    withData:(id)displayData
{
    // init root level
    OCGContextCollectionViewDataSource *result = [[OCGContextCollectionViewDataSource alloc] init];
    [result updataDataSourceWithContext:context
                               withData:displayData];
    
    return result;
}

+ (OCGContextCollectionViewDataSource *)dataSourceForSoftKey:(SoftKey *)softKey
                                                    withData:(id)displayData
{
    // init root level
    OCGContextCollectionViewDataSource *result = [[OCGContextCollectionViewDataSource alloc] init];
    [result updataDataSourceWithSoftKey:softKey
                               withData:displayData];
    
    return result;
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
	return self.sectionCount;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
	NSInteger result = 0;
    
    // get the applicable datasource
    OCGContextCollectionViewDataSource *sectionDataSource = [self dataSourceForSection:section];
    if (sectionDataSource) {
        result = sectionDataSource.totalRowCount;
    }
    
    return result;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
	OCGCollectionViewCell *cell = nil;
    NSUInteger section = indexPath.section;
    NSUInteger row = indexPath.row;
    
    // get the data source for this cell
    OCGContextCollectionViewDataSource *item = [self dataSourceForIndexPath:indexPath];
    // setup the cell based on softkey type
    
    if (item.softKey.data != nil && [self.textFieldDelegate OCGContextCollectionViewDataSource:self canEditDataLocation:item.softKey.data])
    {
        // display a key value pair
        NSString *reuseIdentifier =
        [OCGCollectionViewCell reuseIdentifierForDetailClass:[OCGTextField class]
                                                     options:OCGCollectionViewCellDetailOptionUseTextLabelBaseline];
        cell = [collectionView dequeueReusableCellWithReuseIdentifier:reuseIdentifier
                                                         forIndexPath:indexPath];
        
        cell.detailTextField.delegate = self;
        cell.detailTextField.enabled = YES;
        
        cell.textLabel.text = item.softKey.keyDescription;
        cell.detailTextField.text = nil;
        cell.detailTextField.adjustsFontSizeToFitWidth = YES;
        cell.minimumImageViewWidth = 0.0f;
        cell.imageView.image = nil;
        cell.accessoryType = UITableViewCellAccessoryNone;
        if (item.softKey.data != nil) {
            if ([item.softKey.data isEqualToString:@"rewardText"] &&
                [item.displayItem isKindOfClass:[MPOSBasketRewardLinePriceAdjust class]]) {
                cell.detailTextField.text = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁯󠁅󠁰󠀶󠁂󠁶󠁥󠁵󠁡󠁵󠁺󠁒󠁓󠁵󠁯󠁯󠁦󠁉󠁄󠁨󠁥󠁪󠁃󠁺󠁢󠁶󠁕󠁿*/ @"Price adjust", nil);
            } else if ([self object:item.displayItem hasKeyPath:item.softKey.data]) {
                cell.detailTextField.text = [[item.displayItem valueForKeyPath:item.softKey.data] description];
            }
        }
        // skin
        cell.tag = kEditableTableSkinningTag;
        cell.textLabel.tag = kEditableCellKeySkinningTag;
        cell.detailTextField.tag = kEditableCellValueSkinningTag;
        if ((BasketLayoutTable == BasketController.sharedInstance.clientSettings.parsedBasketLayout) &&
            [item dataSourceIsPartOfSummary]) {
            cell.detailTextField.textAlignment = NSTextAlignmentLeft;
        } else {
            if ([item.displayItem isKindOfClass:[MPOSBasketRewardLine class]] ||
                [item.displayItem isKindOfClass:[MPOSSelectedModifier class]]) {
                cell.detailTextField.textAlignment = NSTextAlignmentLeft;
            } else {
                cell.detailTextField.textAlignment = NSTextAlignmentRight;
            }
        }
        // force the display of the text label for basket cells
        if ([item.displayItem isKindOfClass:[BasketDTO class]]) {
            cell.forceTextLabelDisplay = YES;
            if ([item.softKey.data isEqualToString:@"created"]) {
                // format the created date time
                cell.detailTextField.text = [[(BasketDTO *)item.displayItem created] OCGExtensions_humanReadableDescription];
            }
        }
    }
    else if ([item.softKey.keyType isEqualToString:@"DISPLAY_INFO"]) {
        if ([item.softKey.data isEqualToString:@"generateGiftReceipt"] &&
            [item.displayItem isKindOfClass:[MPOSSaleItem class]]) {
            // display a switch
            NSString *reuseIdentifier =
            [OCGCollectionViewCell reuseIdentifierForDetailClass:[UILabel class]
                                                         options:OCGCollectionViewCellDetailOptionUseTextLabelBaseline];
            cell = [collectionView dequeueReusableCellWithReuseIdentifier:reuseIdentifier
                                                             forIndexPath:indexPath];
            cell.textLabel.text = item.softKey.keyDescription;
            cell.detailTextLabel.text = nil;
            if ([[item.displayItem valueForKeyPath:item.softKey.data] boolValue]) {
                cell.accessoryType = UITableViewCellAccessoryCheckmark;
            }
            cell.minimumImageViewWidth = 0.0f;
            cell.imageView.image = nil;
            // skin
            cell.tag = kEditableTableSkinningTag;
            cell.textLabel.tag = kEditableCellKeySkinningTag;
            cell.detailTextLabel.tag = kEditableCellValueSkinningTag;
            if ((BasketLayoutTable == BasketController.sharedInstance.clientSettings.parsedBasketLayout) &&
                [item dataSourceIsPartOfSummary]) {
                cell.detailTextLabel.textAlignment = NSTextAlignmentLeft;
            } else {
                if ([item.displayItem isKindOfClass:[MPOSBasketRewardLine class]] ||
                    [item.displayItem isKindOfClass:[MPOSSelectedModifier class]]) {
                    cell.detailTextLabel.textAlignment = NSTextAlignmentLeft;
                } else {
                    cell.detailTextLabel.textAlignment = NSTextAlignmentRight;
                }
            }
        } else {
            // display a key value pair
            NSString *reuseIdentifier =
            [OCGCollectionViewCell reuseIdentifierForDetailClass:[UILabel class]
                                                         options:OCGCollectionViewCellDetailOptionUseTextLabelBaseline];
            cell = [collectionView dequeueReusableCellWithReuseIdentifier:reuseIdentifier
                                                             forIndexPath:indexPath];
            cell.textLabel.text = item.softKey.keyDescription;
            cell.detailTextLabel.text = nil;
            cell.detailTextLabel.adjustsFontSizeToFitWidth = YES;
            cell.minimumImageViewWidth = 0.0f;
            cell.imageView.image = nil;
            cell.accessoryType = UITableViewCellAccessoryNone;
            if (item.softKey.data != nil) {
                if ([item.softKey.data isEqualToString:@"rewardText"] &&
                    [item.displayItem isKindOfClass:[MPOSBasketRewardLinePriceAdjust class]]) {
                    cell.detailTextLabel.text = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁯󠁅󠁰󠀶󠁂󠁶󠁥󠁵󠁡󠁵󠁺󠁒󠁓󠁵󠁯󠁯󠁦󠁉󠁄󠁨󠁥󠁪󠁃󠁺󠁢󠁶󠁕󠁿*/ @"Price adjust", nil);
                } else if ([self object:item.displayItem hasKeyPath:item.softKey.data]) {
                    cell.detailTextLabel.text = [[item.displayItem valueForKeyPath:item.softKey.data] description];
                }
            }
            // skin
            cell.tag = kEditableTableSkinningTag;
            cell.textLabel.tag = kEditableCellKeySkinningTag;
            cell.detailTextLabel.tag = kEditableCellValueSkinningTag;
            if ((BasketLayoutTable == BasketController.sharedInstance.clientSettings.parsedBasketLayout) &&
                [item dataSourceIsPartOfSummary]) {
                cell.detailTextLabel.textAlignment = NSTextAlignmentLeft;
            } else {
                if ([item.displayItem isKindOfClass:[MPOSBasketRewardLine class]] ||
                    [item.displayItem isKindOfClass:[MPOSSelectedModifier class]]) {
                    cell.detailTextLabel.textAlignment = NSTextAlignmentLeft;
                } else {
                    cell.detailTextLabel.textAlignment = NSTextAlignmentRight;
                }
            }
            // force the display of the text label for basket cells
            if ([item.displayItem isKindOfClass:[BasketDTO class]]) {
                cell.forceTextLabelDisplay = YES;
                if ([item.softKey.data isEqualToString:@"created"]) {
                    // format the created date time
                    cell.detailTextLabel.text = [[(BasketDTO *)item.displayItem created] OCGExtensions_humanReadableDescription];
                }
            }
            
            if ([item.softKey.data hasSuffix:@".display"]) {
                if ([item.displayItem isKindOfClass:[SaleItem class]]) {
                    SaleItem *saleItem = (SaleItem *)item.displayItem;
                    if (saleItem.basketRewards.count) {
                        cell.detailTextLabel.attributedText =
                        [[NSAttributedString alloc] initWithString:cell.detailTextLabel.text
                                                        attributes:@{ NSUnderlineStyleAttributeName : @(NSUnderlineStyleSingle) }];
                    }
                }
                cell.detailTextLabel.textAlignment = NSTextAlignmentRight;
            }
        }
    } else if ([item.softKey.keyType isEqualToString:@"POSFUNC"]) {
        POSFunc posFunc = POSFuncForString(item.softKey.posFunc);
        
        if ((posFunc == POSFuncEmailReceipt ||
             posFunc == POSFuncPrintPaperReceipt ||
             posFunc == POSFuncPrintSummaryReceipt) && [item.displayItem isKindOfClass:[NSArray class]])
        {
            NSString *reuseIdentifier =
            [OCGCollectionViewCell reuseIdentifierForDetailClass:[UISwitch class]
                                                         options:OCGCollectionViewCellDetailOptionNone];
            cell = [collectionView dequeueReusableCellWithReuseIdentifier:reuseIdentifier
                                                             forIndexPath:indexPath];
            
            UISwitch *detailSwitch = (UISwitch *) cell.detailView;
            
            
            // Ensure an previous addTargets are removed.
            [detailSwitch removeTarget:nil
                                action:@selector(printReceiptSwitchToggled:)
                      forControlEvents:UIControlEventValueChanged];
            [detailSwitch removeTarget:nil
                                action:@selector(printSummarySwitchToggled:)
                      forControlEvents:UIControlEventValueChanged];
            [detailSwitch removeTarget:nil
                                action:@selector(emailReceiptSwitchToggled:)
                      forControlEvents:UIControlEventValueChanged];
            
            MPOSBasket *basket = (MPOSBasket *) [[item.displayItem firstObject] basket];
            
            switch (posFunc) {
                case POSFuncPrintPaperReceipt:
                    detailSwitch.on = basket.enablePrint;
                    if (self.printOptionDelegate) {
                        [detailSwitch addTarget:self.printOptionDelegate
                                         action:@selector(printReceiptSwitchToggled:)
                               forControlEvents:UIControlEventValueChanged];
                    }
                    break;
                    
                case POSFuncPrintSummaryReceipt:
                    detailSwitch.on = basket.printSummaryReceipt;
                    if (self.printOptionDelegate) {
                        [detailSwitch addTarget:self.printOptionDelegate
                                         action:@selector(printSummarySwitchToggled:)
                               forControlEvents:UIControlEventValueChanged];
                    }
                    break;
                    
                case POSFuncEmailReceipt:
                    detailSwitch.on = basket.emailReceipt;
                    if (self.printOptionDelegate) {
                        [detailSwitch addTarget:self.printOptionDelegate
                                         action:@selector(emailReceiptSwitchToggled:)
                               forControlEvents:UIControlEventValueChanged];
                    }
                    break;
                    
                default:
                    break;
            }
            detailSwitch.userInteractionEnabled = YES;
        } else if ((posFunc == POSFuncPrintGiftReceipt) && [item.displayItem isKindOfClass:[MPOSSaleItem class]]) {
            NSString *reuseIdentifier =
            [OCGCollectionViewCell reuseIdentifierForDetailClass:[UISwitch class]
                                                         options:OCGCollectionViewCellDetailOptionNone];
            cell = [collectionView dequeueReusableCellWithReuseIdentifier:reuseIdentifier
                                                             forIndexPath:indexPath];
            UISwitch *detailSwitch = (UISwitch *) cell.detailView;
            [detailSwitch removeTarget:nil
                                action:@selector(giftReceiptSwitchToggled:)
                      forControlEvents:UIControlEventValueChanged];
            
            MPOSSaleItem *basketItem = (MPOSSaleItem *)item.displayItem;
            detailSwitch.on = basketItem.generateGiftReceiptValue;
            if (self.printOptionDelegate) {
                [detailSwitch addTarget:self.printOptionDelegate
                                 action:@selector(giftReceiptSwitchToggled:)
                       forControlEvents:UIControlEventValueChanged];
            }
            detailSwitch.userInteractionEnabled = YES;
        }
        else if ((posFunc == POSFuncPrintOrEmailReceipt) && [item.displayItem isKindOfClass:[NSArray class]])
        {
            NSString *reuseIdentifier =
            [OCGCollectionViewCell reuseIdentifierForDetailClass:[UISegmentedControl class]
                                                         options:OCGCollectionViewCellDetailOptionNone];
            cell = [collectionView dequeueReusableCellWithReuseIdentifier:reuseIdentifier
                                                             forIndexPath:indexPath];
            
            UISegmentedControl *detailView = (UISegmentedControl *) cell.detailView;
            
            [detailView removeTarget:nil
                              action:@selector(printReceiptSwitchToggled:)
                    forControlEvents:UIControlEventValueChanged];
            [detailView removeTarget:nil
                              action:@selector(emailReceiptSwitchToggled:)
                    forControlEvents:UIControlEventValueChanged];
            
            
            [detailView removeAllSegments];
            [detailView insertSegmentWithTitle:@"Print" atIndex:0 animated:NO];
            [detailView insertSegmentWithTitle:@"Email" atIndex:1 animated:NO];

//            MPOSBasket *basket = (MPOSBasket *) [[item.displayItem firstObject] basket];
            MPOSBasket *basket = BasketController.sharedInstance.basket;

            if (basket.emailReceipt)
            {
                detailView.selectedSegmentIndex = 1;
            }
            else
            {
                detailView.selectedSegmentIndex = 0;
                if (!basket.enablePrint)
                {
                    basket.enablePrint = YES;
                    [BasketController.sharedInstance kick];
                }
            }
            
            [detailView addTarget:self.printOptionDelegate
                             action:@selector(printReceiptSwitchToggled:)
                   forControlEvents:UIControlEventValueChanged];
            [detailView addTarget:self.printOptionDelegate
                             action:@selector(emailReceiptSwitchToggled:)
                   forControlEvents:UIControlEventValueChanged];
            detailView.userInteractionEnabled = YES;
        }
        else {
            // display the POS function
            NSString *reuseIdentifier =
            [OCGCollectionViewCell reuseIdentifierForDetailClass:Nil
                                                         options:OCGCollectionViewCellDetailOptionUseTextLabelBaseline];
            cell = [collectionView dequeueReusableCellWithReuseIdentifier:reuseIdentifier
                                                             forIndexPath:indexPath];
            
        }
        NSString *title = nil;
        UIImage *image = nil;
        [POSFuncController getKeyInfoWithKey:item.softKey
                                       title:&title
                                       image:&image];
        cell.textLabel.text = title;
        cell.imageView.image = image;
        cell.accessoryType = UITableViewCellAccessoryNone;
        cell.minimumImageViewWidth = 44.0f;
        cell.detailTextLabel.text = nil;
        // skin
        cell.tag = kTableCellSkinningTag;
        cell.textLabel.tag = kTableCellTextSkinningTag;
        cell.detailTextLabel.tag = 0;
    } else if ([item.softKey.keyType isEqualToString:@"DISPLAY_MESSAGE"]) {
        NSString *reuseIdentifier =
        [OCGCollectionViewCell reuseIdentifierForDetailClass:Nil
                                                     options:OCGCollectionViewCellDetailOptionUseTextLabelBaseline];
        cell = [collectionView dequeueReusableCellWithReuseIdentifier:reuseIdentifier
                                                         forIndexPath:indexPath];
        cell.textLabel.text = item.softKey.keyDescription;
        cell.detailTextLabel.text = @"";
        cell.minimumImageViewWidth = 0.0f;
        cell.imageView.image = nil;
        cell.accessoryType = UITableViewCellAccessoryNone;
        // skin
        cell.tag = kTableCellSkinningTag;
        cell.textLabel.tag = kTableCellTextSkinningTag;
        cell.detailTextLabel.tag = 0;
    } else if ([item.softKey.keyType isEqualToString:@"EMPTY"]) {
        // display nothing
        NSString *reuseIdentifier =
        [OCGCollectionViewCell reuseIdentifierForDetailClass:[UILabel class]
                                                     options:OCGCollectionViewCellDetailOptionUseTextLabelBaseline];
        cell = [collectionView dequeueReusableCellWithReuseIdentifier:reuseIdentifier
                                                         forIndexPath:indexPath];
        cell.textLabel.text = nil;
        cell.detailTextLabel.text = nil;
        cell.minimumImageViewWidth = 0.0f;
        cell.imageView.image = nil;
        cell.accessoryType = UITableViewCellAccessoryNone;
        // skin
        cell.tag = 0;
        cell.textLabel.tag = 0;
        cell.detailTextLabel.tag = 0;
    } else if ([item.softKey.keyType isEqualToString:@"POS_TENDER"]) {
        // display the POS tender
        NSString *reuseIdentifier =
        [OCGCollectionViewCell reuseIdentifierForDetailClass:[UILabel class]
                                                     options:OCGCollectionViewCellDetailOptionUseTextLabelBaseline];
        cell = [collectionView dequeueReusableCellWithReuseIdentifier:reuseIdentifier
                                                         forIndexPath:indexPath];
        Tender *tender = BasketController.sharedInstance.tenders[item.softKey.tenderType];
        if (tender != nil) {
            NSString *title = item.softKey.keyDescription;
            UIImage *image = [self imageForTenderClass:tender.tenderClass];
            cell.textLabel.text = title;
            cell.imageView.image = image;
            cell.accessoryType = UITableViewCellAccessoryNone;
            cell.minimumImageViewWidth = 44.0f;
            cell.detailTextLabel.text = nil;
            cell.forceTextLabelDisplay = YES;
        }
        // skin
        cell.tag = kTableCellSkinningTag;
        cell.textLabel.tag = kTableCellTextSkinningTag;
        cell.detailTextLabel.tag = 0;
    }
    else
    {
        // display the POS tender
        NSString *reuseIdentifier =
        [OCGCollectionViewCell reuseIdentifierForDetailClass:[UILabel class]
                                                     options:OCGCollectionViewCellDetailOptionUseTextLabelBaseline];
        cell = [collectionView dequeueReusableCellWithReuseIdentifier:reuseIdentifier
                                                         forIndexPath:indexPath];
        
        NSString *title = item.softKey.keyDescription;
        cell.textLabel.text = title;
        
        // skin
        cell.tag = kTableCellSkinningTag;
        cell.textLabel.tag = kTableCellTextSkinningTag;
        cell.detailTextLabel.tag = 0;

    }
	
    if (!cell)
    {
        NSString *reuseIdentifier =
        [OCGCollectionViewCell reuseIdentifierForDetailClass:[UILabel class]
                                                     options:OCGCollectionViewCellDetailOptionUseTextLabelBaseline];
        cell = [collectionView dequeueReusableCellWithReuseIdentifier:reuseIdentifier
                                                         forIndexPath:indexPath];
        
#if DEBUG
        cell.textLabel.text = @"Lost cell";
        cell.detailTextLabel.text = [NSString stringWithFormat:@"indexPath: %@", indexPath.OCGExtensions_description];
        cell.backgroundColor = [UIColor colorWithRed:0.995 green:0.760 blue:0.766 alpha:1.000];
#else
        cell.textLabel.text = nil;
        cell.detailTextLabel.text = nil;
        cell.backgroundColor = [UIColor whiteColor];
#endif
    } else {
        cell.backgroundColor = [UIColor whiteColor];
        BOOL active = YES;
        if ([cell.detailView isKindOfClass:[UISwitch class]] || [cell.detailView isKindOfClass:[UISegmentedControl class]]) {
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
        } else {
            cell.selectionStyle = UITableViewCellSelectionStyleGray;
            if ([item.softKey.keyType isEqualToString:@"POSFUNC"]) {
                active = [[POSFuncController sharedInstance] isMenuOptionActive:item.softKey];
                // don't allow the void pos func for promotions
                if ([item.softKey.keyType isEqualToString:@"POSFUNC"] &&
                    [item.softKey.posFunc isEqualToString:NSStringForPOSFunc(POSFuncRemoveItem)] &&
                    [item.displayItem isKindOfClass:[MPOSBasketRewardSalePromotionAmount class]]) {
                    active = NO;
                }
            }
        }
        if (active) {
            cell.textLabel.alpha = 1.0;
            cell.textLabel.opaque = YES;
            cell.imageView.alpha = 1.0;
            cell.imageView.opaque = YES;
        } else {
            cell.textLabel.opaque = NO;
            cell.textLabel.alpha = 0.2;
            cell.imageView.opaque = NO;
            cell.imageView.alpha = 0.2;
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
        }
    }
    
    [[CRSSkinning currentSkin] applyViewSkin:cell
                             withTagOverride:cell.tag];
    
    // softkey theming overrides everything
    if (item.softKey.foregroundColor.length > 0) {
        UIColor *foregroundColor = [self colorFromRGBString:item.softKey.foregroundColor];
        cell.textLabel.textColor = foregroundColor;
        cell.detailTextLabel.textColor = foregroundColor;
//        NSLog(@"%@", foregroundColor);
    }
    if (item.softKey.backgroundColor.length > 0) {
        UIColor *backgroundColor = [self colorFromRGBString:item.softKey.backgroundColor];
        cell.backgroundColor = backgroundColor;
    }
    
    // hide the text label if we aren't in grid layout
    cell.forceHideTextLabel = ([item dataSourceIsPartOfSummary] &&
                               (BasketLayoutGrid != BasketController.sharedInstance.clientSettings.parsedBasketLayout));

	return cell;
}

#pragma mark - FNKCollectionViewDataSourceDelegate

- (NSArray *)collectionView:(UICollectionView *)collectionView
                     layout:(UICollectionViewLayout*)collectionViewLayout
supplementaryItemsForHeadersAtIndexPath:(NSIndexPath *)indexPath
{
    NSMutableArray *result = [NSMutableArray array];
    
    // find the data source for the index path
    OCGContextCollectionViewDataSource *dataSource = [self dataSourceForIndexPath:indexPath
                                                                      headerItems:&result
                                                                      footerItems:nil];
    
    return result;
}

- (NSArray *)collectionView:(UICollectionView *)collectionView
                     layout:(UICollectionViewLayout*)collectionViewLayout
supplementaryItemsForFootersAtIndexPath:(NSIndexPath *)indexPath
{
    NSMutableArray *result = [NSMutableArray array];
    
    // find the data source for the index path
    OCGContextCollectionViewDataSource *dataSource = [self dataSourceForIndexPath:indexPath
                                                                      headerItems:nil
                                                                      footerItems:&result];
    
    return result;
}

- (FNKLayoutOptions)collectionView:(UICollectionView *)collectionView
                           layout:(UICollectionViewLayout*)collectionViewLayout
         layoutOptionsAtIndexPath:(NSIndexPath *)indexPath
{
    FNKLayoutOptions result = FNKLayoutOptionNone;
    
    // get the display item
    OCGContextCollectionViewDataSource *item = [self dataSourceForIndexPath:indexPath];
    // check if this is an empty key
    if ([item.softKey.keyType isEqualToString:@"EMPTY"]) {
        result = FNKLayoutOptionEmptyCell;
    } else {
        // check if this is a default key
        if ([item isDefaultKey]) {
            result = FNKLayoutOptionDefaultCell;
        }
    }
    
    return result;
}

#pragma mark - Methods

- (void)updataDataSourceWithContext:(SoftKeyContext *)context
                           withData:(id)displayData
{
    // init
    NSMutableArray *childDataSources = [NSMutableArray array];
    
    // build child data sources from the menu
    // split up the root menu
    NSString *trimmmedKeys = [context.rootMenu stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    NSArray *menuKeys = [trimmmedKeys componentsSeparatedByCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    // iterate through the key ids
    for (NSString *menuKeyId in menuKeys) {
        if (menuKeyId.length > 0) {
            SoftKey *menuKey = [self softKeyWithId:menuKeyId
                                         inContext:context];
            OCGContextCollectionViewDataSource *childDataSource =
            [self buildDataSourcesFromSoftKey:menuKey
                              withDisplayData:displayData];
            if (childDataSource) {
                childDataSource.parentDataSource = self;
                [childDataSources addObject:childDataSource];
            }
        }
    }
    
    self.childDataSources = childDataSources;
    
    if ([displayData isKindOfClass:[MPOSBasketItem class]] ||
        [displayData isKindOfClass:[BasketTaxFree class]]) {
        // update the tab's levels
        if (self.childDataSources.count > 0) {
            OCGContextCollectionViewDataSource *tabDataSource = self.childDataSources[0];
            tabDataSource.level = FNKSupplementaryOptionDefault;
        }
    }
    
    // update mappings
    [self updateMapping];
}

- (void)updataDataSourceWithSoftKey:(SoftKey *)softKey
                           withData:(id)displayData
{
    // init
    NSMutableArray *childDataSources = [NSMutableArray array];
    
    if ([softKey.keyType hasSuffix:@"MENU"]) {
        // build child data sources from the menu
        OCGContextCollectionViewDataSource *childDataSource =
        [self buildDataSourcesFromSoftKey:softKey
                          withDisplayData:displayData];
        if (childDataSource) {
            childDataSource.parentDataSource = self;
            [childDataSources addObject:childDataSource];
        }
    } else {
        NSAssert(NO, @"updataDataSourceWithSoftKey:withData: only processes MENU softkeys, not %@ keys", softKey.keyType);
    }
    
    self.childDataSources = childDataSources;
    
    if ([displayData isKindOfClass:[MPOSBasketItem class]]) {
        // update the tab's levels
        if (self.childDataSources.count > 0) {
            OCGContextCollectionViewDataSource *tabDataSource = self.childDataSources[0];
            tabDataSource.level = FNKSupplementaryOptionDefault;
        }
    }
    
    // update mappings
    [self updateMapping];
}

- (void)registerCellReuseIdsWithCollectionView:(UICollectionView *)collectionView
{
    [OCGCollectionViewCell registerForDetailClass:Nil
                                          options:OCGCollectionViewCellDetailOptionUseTextLabelBaseline
                                   collectionView:collectionView];
    [OCGCollectionViewCell registerForDetailClass:[UILabel class]
                                          options:OCGCollectionViewCellDetailOptionUseTextLabelBaseline
                                   collectionView:collectionView];
    [OCGCollectionViewCell registerForDetailClass:[UISwitch class]
                                          options:OCGCollectionViewCellDetailOptionNone
                                   collectionView:collectionView];
    [OCGCollectionViewCell registerForDetailClass:[UISegmentedControl class]
                                          options:OCGCollectionViewCellDetailOptionNone
                                   collectionView:collectionView];
    [OCGCollectionViewCell registerForDetailClass:[OCGTextField class]
                                          options:OCGCollectionViewCellDetailOptionUseTextLabelBaseline
                                   collectionView:collectionView];
}

- (void)registerSupplementaryViewReuseIdsWithCollectionView:(UICollectionView *)collectionView
{
    [collectionView registerClass:[FNKSupplementaryView class]
       forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:viewReuseIdentifier];
    [collectionView registerClass:[FNKSupplementaryView class]
       forSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:viewReuseIdentifier];

}

- (NSString *)titleForSection:(NSInteger)section
{
    NSString *result = nil;
    
    OCGContextCollectionViewDataSource *sectionDataSource = [self dataSourceForSection:section];
    if (sectionDataSource) {
        result = sectionDataSource.sectionTitle;
    }
    
    return result;
}

- (void)updateMapping
{
    // init
    self.sectionCount = self.childDataSources.count;
    self.totalRowCount = 0;
    [self.sectionDataSources removeAllObjects];
    [self.sectionDataSources addObjectsFromArray:self.childDataSources];
    
    // update mappings for each child data source
    self.totalRowCount = [self updateMappingForChildDataSourceWithRunningTotal:0];
    
    // reset cache
    [_dataSourceCache removeAllObjects];
}

- (OCGContextCollectionViewDataSource *)dataSourceForSection:(NSInteger)section
{
    OCGContextCollectionViewDataSource *result = nil;
    
    if (section < self.sectionDataSources.count) {
        result = self.sectionDataSources[section];
    }
    
    return result;
}

- (OCGContextCollectionViewDataSource *)dataSourceForIndexPath:(NSIndexPath *)indexPath
{
    // get the data source
    OCGContextCollectionViewDataSource *result = [self dataSourceForIndexPath:indexPath
                                                                  headerItems:nil
                                                                  footerItems:nil];
    // update its indexpath
    if (result) {
        result.indexPath = indexPath;
    }
    
    // return the data source
    return result;
}

- (BOOL)dataSourceIsPartOfSummary
{
    BOOL result = NO;
    
    // check if the parent data source is the first data source of a basket item
    if ([self.displayItem isKindOfClass:[MPOSBasketItem class]] ||
        [self.displayItem isKindOfClass:[BasketTaxFree class]]) {
        if (([self.parentDataSource.parentDataSource.childDataSources indexOfObject:self.parentDataSource] == 0)) {
            result = YES;
        }
    }
    
    return result;
}

@end
