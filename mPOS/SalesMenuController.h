//
//  SalesMenuViewController.h
//  mPOS
//
//  Created by Antonio Strijdom on 13/01/2014.
//  Copyright (c) 2014 Clarity. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RESTController.h"
#import "OCGMenuPageViewController.h"

/** @file SalesMenuViewController.h */

/** @brief View controller subclass for displaying the sales menu.
 */
@interface SalesMenuController : NSObject

/** @property rootMenu
 *  @brief The menu to display in the view controller.
 */
@property (nonatomic, strong) SoftKeyContext *rootMenu;

/** @property closeItem
 *  @brief The bar button item that closes the sales menu view controller.
 *  @note This item needs to be added to the menu page to allow the user to close the menu.
 */
@property (nonatomic, readonly) UIBarButtonItem *closeItem;

/** @brief Initialiser - creates an instance of the sales menu view controller with the specified root menu.
 *  @param menu The root page of the menu.
 */
- (id) initWithRootMenu:(SoftKeyContext *)menu;

/** @brief Registers either the cell class or xib with the reuse id specified.
 *  @param nibOrNil The nib to register or nil.
 *  @param classOrNil The class to register or nil.
 *  @param reuseId The reuse id to register the cell for.
 *  @note If both xib and class are passed in, the xib takes precedence.
 */
- (void) registerMenuCellFromXib:(UINib *)nibOrNil
                         orClass:(Class)classOrNil
                     withReuseId:(NSString *)reuseId;


- (OCGMenuPageViewController *) menuPageViewControllerForRootMenu;

@end
