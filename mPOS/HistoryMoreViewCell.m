//
//  HistoryMoreViewCell.m
//  mPOS
//
//  Created by Antonio Strijdom on 21/11/2012.
//  Copyright (c) 2012 Clarity. All rights reserved.
//

#import "HistoryMoreViewCell.h"
#import <QuartzCore/QuartzCore.h>

@implementation HistoryMoreViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void) prepareForReuse
{
    [super prepareForReuse];
    // remove the gradient
    CAGradientLayer *gradient = nil;
    for (CALayer *layer in self.moreGradient.layer.sublayers) {
        if ([layer isKindOfClass:[CAGradientLayer class]]) {
            gradient = (CAGradientLayer *)layer;
            break;
        }
    }
    if (gradient != nil) {
        [gradient removeFromSuperlayer];
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
