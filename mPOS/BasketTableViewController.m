//
//  BasketTableViewController.m
//  mPOS
//
//  Created by Antonio Strijdom on 12/02/2014.
//  Copyright (c) 2014 Clarity. All rights reserved.
//

#import "BasketTableViewController.h"
#import "CRSSkinning.h"
#import "BasketController.h"
#import "UICollectionView+OCGExtensions.h"
#import "POSFuncController.h"
#import "DiscountListViewController.h"
#import "FNKCollectionViewLayout.h"
#import "OCGContextCollectionViewDataSource.h"
#import "ContextViewController.h"
#import "OCGReceiptManager.h"
#import "OCGEFTManager.h"
#import "ClientSettings+MPOSExtensions.h"

@interface BasketTableViewController () <DiscountListViewControllerDelegate, OCGPrintOptionSwitchDelegate, OCGTextFieldDelegate> {
    dispatch_group_t _updateGroup;
}
@property (nonatomic, strong) NSMutableArray *basketItems;
@property (nonatomic, strong) OCGContextCollectionViewDataSource *dataSource;
- (BOOL) canShowOperationsForBasketItem:(MPOSBasketItem *)basketItem;
- (void) presentDiscountList;
@end

@implementation BasketTableViewController

#pragma mark - Private

static NSUInteger kRowHeight = 46.0f;
static NSString *basketItemTableViewCellIdentifier = @"BasketItemCollectionViewCell";

- (BOOL) canShowOperationsForBasketItem:(MPOSBasketItem *)basketItem
{
    BOOL result = YES;
    
    SoftKeyContext *softKeyContext = [[ContextViewController sharedInstance] contextLayoutMenuForBasketItem:basketItem];
    if ([softKeyContext.rootMenu length] == 0 ||
        [[softKeyContext.rootMenu componentsSeparatedByString:@" "] count] == 0) {
        result = NO;
    } else {
        if (BasketController.sharedInstance.isPrinting) {
            result = [basketItem isKindOfClass:[MPOSPrintDataLine class]];
        }
    }
    
    return result;
}

- (void) presentDiscountList
{
    // present the discount list
    DiscountListViewController* discountListViewController =
    [[DiscountListViewController alloc] initWithNibName:@"DiscountListView"
                                                 bundle:nil];
    discountListViewController.delegate = self;
    discountListViewController.basketItem = nil;
    discountListViewController.showAddDiscountNavigationButton = NO;
    discountListViewController.showDismissNavigationButton = NO;
    self.splitViewController.navigationItem.backBarButtonItem =
    [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁱󠀹󠁯󠁆󠁉󠁏󠀳󠁎󠁈󠁘󠁉󠁷󠁋󠁁󠁦󠁐󠁗󠁶󠀫󠁒󠁩󠀶󠁺󠁕󠁄󠁚󠁁󠁿*/ @"Basket", nil)
                                     style:UIBarButtonItemStylePlain
                                    target:nil
                                    action:nil];
    
    [self.navigationController pushViewController:discountListViewController
                                         animated:YES];
}

#pragma mark - UIViewController

- (id) init
{
    FNKCollectionViewLayout *layout = [[FNKCollectionViewLayout alloc] init];
    layout.pinLastSectionToBottom = YES;
    self = [super initWithCollectionViewLayout:layout];
    if (self)
    {
        self.allowsSelection = YES;
        self.allowsEditing = YES;
    }
    
    return self;
}

- (void) dealloc
{
    _updateGroup = nil;
}

- (void) viewDidLoad
{
    [super viewDidLoad];
    
    // create data source
    self.dataSource = [[OCGContextCollectionViewDataSource alloc] init];
    [self.dataSource registerCellReuseIdsWithCollectionView:self.collectionView];
    self.collectionView.dataSource = self.dataSource;
    self.collectionView.delaysContentTouches = NO;
    self.dataSource.printOptionDelegate = self;
    self.dataSource.textFieldDelegate = self;
    self.selectedBasketItem = nil;
    
    // apply skin
    self.collectionView.tag = kTableSkinningTag;
    self.collectionView.backgroundColor = [UIColor colorWithRed:0.937 green:0.937 blue:0.957 alpha:1.000];
    [[CRSSkinning currentSkin] applyViewSkin:self];
}

#pragma mark - UICollectionViewDataSource

- (CGFloat) collectionView:(UICollectionView *)collectionView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return kRowHeight;
}

- (BOOL) collectionView:(UICollectionView *)collectionView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    BOOL result = NO;
    
    // get the basket item
    MPOSBasketItem *basketItem = [self.basketItems objectAtIndex:indexPath.row];
    // only allow swipe to delete on sales items
    if ([basketItem isKindOfClass:[MPOSSaleItem class]]) {
        // and if the user has permission
        result = [[BasketController sharedInstance] doesUserHavePermissionTo:POSPermissionCancelItem];
    }
    
    return result;
}

#pragma mark - UICollectionViewDelegate


- (BOOL) collectionView:(UICollectionView *)collectionView shouldIndentWhileEditingRowAtIndexPath:(NSIndexPath *)indexPath
{
    return NO;
}

- (void) collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.allowsSelection) {
        // get the basket item from the current basket
        OCGContextCollectionViewDataSource *displayItem = [((OCGContextCollectionViewDataSource *)self.collectionView.dataSource) dataSourceForIndexPath:indexPath];
        NSAssert(displayItem != nil, @"The reference of the requested basket display item equals nil.");
        if ([displayItem.displayItem isKindOfClass:[MPOSBasketItem class]]) {
            self.selectedBasketItem = displayItem.displayItem;
        }
        if ([displayItem.softKey.keyType isEqualToString:@"DISPLAY_INFO"] ||
            [displayItem.softKey.keyType isEqualToString:@"DISPLAY_MESSAGE"] ||
            [displayItem.softKey.keyType isEqualToString:@"EMPTY"]) {
            MPOSBasketItem *basketItem = (MPOSBasketItem *)displayItem.displayItem;
            if ([basketItem isKindOfClass:[MPOSBasketItem class]])
            {
                // push to the view that shows the operations for that basket item
                if ([self canShowOperationsForBasketItem:basketItem]) {
                    [[POSFuncController sharedInstance] showOperationsForBasketItem:basketItem];
                }
            }
        } else if ([displayItem.softKey.keyType isEqualToString:@"POSFUNC"]) {
            if (([[POSFuncController sharedInstance] isMenuOptionActive:displayItem.softKey]) &&
                (![OCGEFTManager sharedInstance].processing)) {
                [[ContextViewController sharedInstance] didSelectContextMenuItem:displayItem.softKey];
            }
        }
        else if ([displayItem.softKey.keyType isEqualToString:@"POS_TENDER"]) {
            if (![OCGEFTManager sharedInstance].processing) {
                [[ContextViewController sharedInstance] didSelectContextMenuItem:displayItem.softKey];
            }
        }
    }
    [collectionView deselectItemAtIndexPath:indexPath animated:YES];
}

- (BOOL) collectionView:(UICollectionView *)collectionView shouldHighlightItemAtIndexPath:(NSIndexPath *)indexPath
{
    BOOL result = YES;
    
    // get the basket item from the current basket
    OCGContextCollectionViewDataSource *displayItem = [((OCGContextCollectionViewDataSource *)self.collectionView.dataSource) dataSourceForIndexPath:indexPath];
    // don't highlight tax free on ipod
    if (BasketLayoutTable == BasketController.sharedInstance.clientSettings.parsedBasketLayout) {
        if ([displayItem.displayItem isKindOfClass:[BasketTaxFree class]]) {
            result = NO;
        }
    }
    if (BasketController.sharedInstance.isPrinting) {
        result = ([displayItem.softKey.keyType isEqualToString:@"POSFUNC"]);
    }
    if ([OCGEFTManager sharedInstance].processing){
        result = NO;
    }
    
    return result;
}

- (void) collectionView:(UICollectionView *)collectionView didHighlightItemAtIndexPath:(NSIndexPath *)indexPath
{
    // get the basket item from the current basket
    OCGContextCollectionViewDataSource *displayItem = [((OCGContextCollectionViewDataSource *)self.collectionView.dataSource) dataSourceForIndexPath:indexPath];
    // highlight all siblings on ipod
    if (BasketLayoutTable == BasketController.sharedInstance.clientSettings.parsedBasketLayout) {
        if ([displayItem dataSourceIsPartOfSummary]) {
            for (OCGContextCollectionViewDataSource *sibling in displayItem.parentDataSource.childDataSources) {
                UICollectionViewCell *cell = [collectionView cellForItemAtIndexPath:sibling.indexPath];
                cell.highlighted = YES;
            }
        }
    }
}

- (void) collectionView:(UICollectionView *)collectionView didUnhighlightItemAtIndexPath:(NSIndexPath *)indexPath
{
    // get the basket item from the current basket
    OCGContextCollectionViewDataSource *displayItem = [((OCGContextCollectionViewDataSource *)self.collectionView.dataSource) dataSourceForIndexPath:indexPath];
    // highlight all siblings on ipod
    if (BasketLayoutTable == BasketController.sharedInstance.clientSettings.parsedBasketLayout) {
        if ([displayItem dataSourceIsPartOfSummary]) {
            for (OCGContextCollectionViewDataSource *sibling in displayItem.parentDataSource.childDataSources) {
                UICollectionViewCell *cell = [collectionView cellForItemAtIndexPath:sibling.indexPath];
                cell.highlighted = NO;
            }
        }
    }
}

- (UIView *) collectionView:(UICollectionView *)collectionView viewForHeaderInSection:(NSInteger)section
{
    return [[UIView alloc] init];
}

- (CGFloat) collectionView:(UICollectionView *)collectionView heightForHeaderInSection:(NSInteger)section
{
    return 0.001;
}

#pragma mark - DiscountListViewControllerDelegate

- (void) dismissDiscountListViewController:(DiscountListViewController *)discountListViewController
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - OCGPrintOptionSwitchDelegate

- (void) printReceiptSwitchToggled:(UISwitch *)switchDetail
{
    [[OCGReceiptManager configuredManager] printReceipt];
}

- (void) printSummarySwitchToggled:(UISwitch *)switchDetail
{
    [[OCGReceiptManager configuredManager] printSummaryReceipt];
}

- (void) emailReceiptSwitchToggled:(UISwitch *)switchDetail
{
    [[OCGReceiptManager configuredManager] emailReceiptComplete:^{
        MPOSBasket *basket = BasketController.sharedInstance.basket;
        if ([switchDetail isKindOfClass:[UISwitch class]])
        {
            switchDetail.on = basket.emailReceipt;
        }
    }];
}

- (void) giftReceiptSwitchToggled:(UISwitch *)switchDetail
{
    NSIndexPath *indexPath =
    [self.collectionView indexPathForCell:(UICollectionViewCell *)switchDetail.superview.superview];
    if (indexPath) {
        [self collectionView:self.collectionView
    didSelectItemAtIndexPath:indexPath];
    }
}

#pragma mark - Methods

- (void) reloadTableDataAndScroll:(BOOL)scroll
{
    if (_updateGroup == nil) {
        _updateGroup = dispatch_group_create();
    }
    if (dispatch_group_wait(_updateGroup, DISPATCH_TIME_NOW) == 0) {
        dispatch_group_enter(_updateGroup);
        // build basket items
        NSMutableArray *basketItems = [NSMutableArray array];
        // get the active basket
        MPOSBasket *basket = BasketController.sharedInstance.basket;
        // display items
        for (MPOSBasketItem *basketItem in basket.basketItems) {
            if (([basketItem.basketItemId length] > 0) &&
                ![basketItem isKindOfClass:[MPOSBasketTender class]]) {
                [basketItems addObject:basketItem];
            }
        }
        
        // add tenders
        for (MPOSBasketItem *basketItem in basket.basketItems) {
            if (([basketItem.basketItemId length] > 0) &&
                [basketItem isKindOfClass:[MPOSBasketTender class]]) {
                [basketItems addObject:basketItem];
            }
        }
        
        if (basket.taxFree)
        {
            [basketItems addObject:basket.taxFree];
        }
        
        // check if the item count changed
        BOOL hasNewBasketItems = NO;
        if ([self.basketItems count] > 0 && [basketItems count] > [self.basketItems count]) {
            hasNewBasketItems = YES;
        }
        self.basketItems = basketItems;
        // reset selected item
        self.selectedBasketItem = nil;
        // clear the collection view
        self.dataSource = [[OCGContextCollectionViewDataSource alloc] init];
        [self.dataSource registerCellReuseIdsWithCollectionView:self.collectionView];
        self.collectionView.dataSource = self.dataSource;
        self.dataSource.printOptionDelegate = self;
        [self.collectionView reloadData];
        // reload
        SoftKeyContext *currentContextMenuLayout = [[ContextViewController sharedInstance] contextLayoutMenuForCurrentContext];
        if (currentContextMenuLayout) {
                [self.dataSource updataDataSourceWithContext:currentContextMenuLayout
                                                    withData:self.basketItems];
                    // update collection view
                    [self.collectionView reloadData];
                    // scroll to the new rows if there are any
                    if (hasNewBasketItems) {
                        NSIndexPath *indexPath =
                        [self.collectionView indexPathFromIndexPath:[NSIndexPath indexPathForItem:0
                                                                                        inSection:self.collectionView.numberOfSections - 1]
                                                             offset:-1
                                                               wrap:NO];
                        
                        
                        if (indexPath != nil) {
                            UICollectionViewLayoutAttributes *layoutAttributes = [self.collectionView layoutAttributesForItemAtIndexPath:indexPath];

                            [self.collectionView scrollRectToVisible:layoutAttributes.frame
                                                            animated:YES];
                        }
                    }
                    // TFS68611 - scroll to show tenders when checking out
                    if (scroll) {
                        NSIndexPath *tenderIndexPath = nil;
                        for (NSUInteger section = 0; section < self.collectionView.numberOfSections; section++) {
                            for (NSUInteger item = 0; item < [self.collectionView numberOfItemsInSection:section]; item++) {
                                NSIndexPath *itemIndexPath = [NSIndexPath indexPathForItem:item inSection:section];
                                OCGContextCollectionViewDataSource *displayItem = [(OCGContextCollectionViewDataSource *)self.collectionView.dataSource dataSourceForIndexPath:itemIndexPath];
                                if (displayItem) {
                                    if ([displayItem.softKey.keyType isEqualToString:@"POS_TENDER"]) {
                                        tenderIndexPath = itemIndexPath;
                                    } else if ([displayItem.softKey.keyType isEqualToString:@"POSFUNC"] &&
                                               ((POSFuncForString(displayItem.softKey.posFunc) == POSFuncPrintPaperReceipt) ||
                                                (POSFuncForString(displayItem.softKey.posFunc) == POSFuncPrintSummaryReceipt) ||
                                                (POSFuncForString(displayItem.softKey.posFunc) == POSFuncPrintOrEmailReceipt) ||
                                                (POSFuncForString(displayItem.softKey.posFunc) == POSFuncEmailReceipt))) {
                                        tenderIndexPath = itemIndexPath;
                                    }
                                }
                            }
                        }
                        if (tenderIndexPath) {
                            [self.collectionView scrollToItemAtIndexPath:tenderIndexPath
                                                        atScrollPosition:UICollectionViewScrollPositionBottom
                                                                animated:YES];
                        }
                    }
                    dispatch_group_leave(_updateGroup);
        } else {
            dispatch_group_leave(_updateGroup);
        }
    }
}

-(BOOL)OCGContextCollectionViewDataSource:(OCGContextCollectionViewDataSource *)collectionViewDataSource
                      canEditDataLocation:(NSString *)dataLocation
{
    NSLog(@"%@", dataLocation);
    return NO;
}

-(void)OCGContextCollectionViewDataSource:(OCGContextCollectionViewDataSource *)collectionViewDataSource
                       updateDataLocation:(NSString *)dataLocation
                                 withText:(NSString *)text
{
    
}

@end
