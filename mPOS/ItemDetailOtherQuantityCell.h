//
//  ItemDetailOtherQuantityCell.h
//  mPOS
//
//  Created by Antonio Strijdom on 28/05/2013.
//  Copyright (c) 2013 Clarity. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CRSGlossyButton.h"

@interface ItemDetailOtherQuantityCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *storeName;
@property (weak, nonatomic) IBOutlet UILabel *quantity;
@property (weak, nonatomic) IBOutlet UILabel *distance;
@property (weak, nonatomic) IBOutlet UILabel *storeId;
@property (weak, nonatomic) IBOutlet UILabel *quantityDesc;
@property (weak, nonatomic) IBOutlet UILabel *distanceDesc;
@property (weak, nonatomic) IBOutlet CRSGlossyButton *headerBackground;

@end
