//
//  main.m
//  mPOS
//
//  Created by Antonio Strijdom on 24/07/2012.
//  Copyright (c) 2012 Clarity. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"
#import "OCGApplication.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, NSStringFromClass([OCGApplication class]), NSStringFromClass([AppDelegate class]));
    }
}
