//
//  ReceiptListViewController.h
//  mPOS
//
//  Created by John Scott on 05/02/2014.
//  Copyright (c) 2014 Clarity. All rights reserved.
//

#import <UIKit/UIKit.h>

@class BasketDTO;

@interface ReceiptListViewController : UICollectionViewController

@property (strong, nonatomic) BasketDTO *basket;

@end
