//
//  SalesMenuItemCell.m
//  MenuTest
//
//  Created by Antonio Strijdom on 14/03/2013.
//  Copyright (c) 2013 Antonio Strijdom. All rights reserved.
//

#import "SalesMenuItemCell.h"
//#import <QuartzCore/QuartzCore.h>

@interface SalesMenuItemCell () {
    UIColor *originalColor;
}
@property (strong, nonatomic) CRSGlossyButton *backgroundButton;
@property (strong, nonatomic) UILabel *titleLabel;
@end

@implementation SalesMenuItemCell

static CGFloat kLabelInset = 1.0f;

#pragma mark - Properties

- (void) setMenuItem:(SoftKey *)menuItem
{
    [super setMenuItem:menuItem];
    self.titleLabel.hidden = YES;
    self.backgroundButton.hidden = YES;
    if (self.menuItem) {
        if (([self.menuItem.keyType isEqualToString:@"UPC"]) ||
            ([self.menuItem.keyType isEqualToString:@"MENU"])) {
            // setup a menu item cell
            self.titleLabel.hidden = NO;
            self.backgroundButton.hidden = NO;
            self.titleLabel.text = self.menuItem.keyDescription;
        } else {
            // setup an empty cell
            
        }
    }
}

- (void) setFont:(UIFont *)font
{
    [super setFont:font];
    self.titleLabel.font = self.font;
}

- (void) setForegroundColor:(UIColor *)foregroundColor
{
    [super setForegroundColor:foregroundColor];
    self.titleLabel.textColor = self.foregroundColor;
}

- (void) setBackgroundColor:(UIColor *)backgroundColor
{
    [super setBackgroundColor:backgroundColor];
    self.backgroundButton.buttonColor = self.backgroundColor;
}

- (void) setTag:(NSInteger)tag
{
    [super setTag:tag];
    self.contentView.tag = tag;
    self.titleLabel.tag = tag + 2;
    self.backgroundButton.tag = tag + 1;
}

- (void) setHighlighted:(BOOL)highlighted
{
    [super setHighlighted:highlighted];
    self.backgroundButton.highlighted = highlighted;
    if (highlighted) {
//        self.backgroundButton.layer.shadowOffset = CGSizeMake(1.0f, 1.0f);
        if (originalColor == nil) {
            originalColor = self.titleLabel.textColor;
        }
        self.titleLabel.textColor = [UIColor whiteColor];
    } else {
//        self.backgroundButton.layer.shadowOffset = CGSizeMake(5.0f, 5.0f);
        if (originalColor != nil) {
            self.titleLabel.textColor = originalColor;
        }
    }
}
#pragma mark - Methods

- (id) initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // setup the title label
        self.titleLabel = [[UILabel alloc] init];
        self.titleLabel.translatesAutoresizingMaskIntoConstraints = NO;
        self.titleLabel.userInteractionEnabled = NO;
        self.titleLabel.textAlignment = NSTextAlignmentCenter;
        self.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
        self.titleLabel.numberOfLines = 3;
        self.titleLabel.text = @"";
        self.titleLabel.backgroundColor = [UIColor clearColor];
        [self.contentView addSubview:self.titleLabel];
        [self.titleLabel constrain:@"centerY = centerY"
                                to:self.contentView];
        [self.titleLabel constrain:@"left = left + 1.0"
                                to:self.contentView];
        [self.titleLabel constrain:@"right = right - 1.0"
                                to:self.contentView];
        // setup the button background
        self.backgroundButton = [[CRSGlossyButton alloc] init];
        self.backgroundButton.translatesAutoresizingMaskIntoConstraints = NO;
        self.backgroundButton.userInteractionEnabled = NO;
        self.backgroundButton.cornerRadius = 10.0f;
        self.backgroundButton.tag = self.contentView.tag + 1;
        self.backgroundButton.drawRing = YES;
        self.backgroundButton.tintColor = [UIColor lightGrayColor];
//        self.backgroundButton.layer.shadowColor = [UIColor blackColor].CGColor;
//        self.backgroundButton.layer.shadowOffset = CGSizeMake(5.0f, 5.0f);
//        self.backgroundButton.layer.shadowOpacity = 0.8f;
        [self.backgroundButton setTitle:@""
                               forState:UIControlStateNormal];
        [self.contentView addSubview:self.backgroundButton];
        [self.backgroundButton constrain:@"top = top"
                                      to:self.contentView];
        [self.backgroundButton constrain:@"bottom = bottom"
                                      to:self.contentView];
        [self.backgroundButton constrain:@"left = left"
                                      to:self.contentView];
        [self.backgroundButton constrain:@"right = right"
                                      to:self.contentView];
        [self.contentView sendSubviewToBack:self.backgroundButton];
    }
    return self;
}

- (NSString *) reuseIdentifier
{
    return @"SalesMenuItemCell";
}

- (void) prepareForReuse
{
    [super prepareForReuse];
    // reset
    originalColor = nil;
    // default to hidden
    self.titleLabel.hidden = YES;
    self.backgroundButton.hidden = YES;
}

@end
