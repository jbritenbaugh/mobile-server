//
//  BarcodeScannerController.m
//  mPOS
//
//  Created by Antonio Strijdom on 18/02/2013.
//  Copyright (c) 2013 Clarity. All rights reserved.
//

#import "BarcodeScannerController.h"
#import <QuartzCore/QuartzCore.h>
#import <AVFoundation/AVFoundation.h>
#import "CameraInputView.h"
#import "CRSSkinning.h"
#import "ContextViewController.h"
#import "BasketController.h"
#import "FNKTextField.h"
#import "FNKInputView.h"
#import "POSInputView.h"
#import "OCGSettingsController.h"

CGFloat KeyboardHeight()
{
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
    {
        return 216;
    }
    else
    {
        return 264;
    }
}

@interface BarcodeScannerInputView : UIView
@end

@interface BarcodeScannerController () <SMScannerManagerDelegate, UITextFieldDelegate>
@property (nonatomic, strong) NSDate *pauseTime;
@property (nonatomic, readonly) NSNumber *waitInSeconds;
@property (nonatomic, strong) SMScannerManager *manager;
@property (nonatomic, strong) BarcodeScannedBlock scanningBlock;
@property (nonatomic, strong) BarcodeErroredBlock errorBlock;
@property (nonatomic, strong) BarcodeScanTerminatedBlock terminatedBlock;
@property (nonatomic, assign) BOOL pausedForManualEntry;
- (void) handleSettingChangedNotification:(NSNotification *)note;
- (void) resumeScanning;
- (void) stopScanningAnimationsCompleted:(BOOL)finished;
- (void) handleApplicationWillResignActiveNotification:(NSNotification *)notification;
@end

NSString * const kConfigScannerUserDefaultKeyPath = @"config_scanner";
NSString * const kScannerNotificationBarcodeScanned = @"kScannerNotificationBarcodeScanned";
NSString * const kScannerNotificationBarcodeScannedBarcodeKey = @"kScannerNotificationBarcodeScannedBarcodeKey";

@implementation BarcodeScannerController
{
    FNKTextField* _textField;
}

#pragma mark - Private

- (void) handleSettingChangedNotification:(NSNotification *)note
{
    NSString *settingName = note.userInfo[kSettingsControllerSettingDidChangeNotificationSettingNameKey];
    if ([settingName isEqualToString:kConfigScannerUserDefaultKeyPath]) {
        DebugLog(@"Source changed, restarting scanner");
        // if we are scanning
        if (self.scanning) {
            [self stopScanning];
        }
        // kill the current manager
        if (self.manager) {
            [self.manager closeScanner];
        }
        self.manager = nil;
    }
}

- (void) resumeScanning
{
    // if paused for keyboard
    if (self.pausedForManualEntry) {
        // resume scanning
        if (self.manager) {
            // if we are not doing manual entry
            if (self.sourceType != SMScannerSourceTypeManual && self.sourceType != SMScannerSourceTypeInternal) {
                DebugLog(@"Resuming scan");
                [self.manager scanBarcode];
            }
        }
    } else {
        [_textField becomeFirstResponder];
    }
    self.pausedForManualEntry = NO;
}

#pragma mark - Properties

@synthesize sourceType;
- (SMScannerSourceType) sourceType
{
    SMScannerSourceType result = SMScannerSourceTypeManual;
    
    NSNumber *value = [[OCGSettingsController sharedInstance] valueForSettingWithName:kConfigScannerUserDefaultKeyPath];
    if (value) {
        result = [value intValue];
    }
    
    return result;
}

- (void) setSourceType:(SMScannerSourceType)value
{
    [[OCGSettingsController sharedInstance] setValue:@(value)
                                  forSettingWithName:kConfigScannerUserDefaultKeyPath];
}


@synthesize scanning;
- (BOOL) scanning
{
    return (self.scanningBlock != nil);
}

@synthesize waitInSeconds = _waitInSeconds;
- (NSNumber *) waitInSeconds
{
    if (_waitInSeconds == nil) {
        // load from plist
        NSString *result = nil;
        NSBundle *bundle = [NSBundle bundleForClass:[self class]];
        NSURL *settingsURL = [bundle URLForResource:@"Settings"
                                      withExtension:@"plist"];
        if (settingsURL) {
            NSDictionary *settings = [NSDictionary dictionaryWithContentsOfURL:settingsURL];
            result = [settings objectForKey:@"scan_delay_in_seconds"];
        }
        
        if (result != nil) {
            _waitInSeconds = [NSNumber numberWithDouble:result.doubleValue];
        } else {
            _waitInSeconds = @(0);
        }
    }
    
    return _waitInSeconds;
}

#pragma mark - Init

+ (BarcodeScannerController *) sharedInstance
{
    static BarcodeScannerController *_controller = nil;
    
    if (_controller == nil) {
        static dispatch_once_t onceToken;
        dispatch_once(&onceToken, ^{
            _controller = [[BarcodeScannerController alloc] init];
        });
    }
    
    return _controller;
}

- (id) init
{
    DebugLog(@"init");
    self = [super init];
    if (self) {
        // subscribe to notifications sent whenever the source type gets changed
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(handleSettingChangedNotification:)
                                                     name:kSettingsControllerSettingDidChangeNotification
                                                   object:nil];
        // register for UIApplication notifications
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(handleApplicationWillResignActiveNotification:)
                                                     name:UIApplicationWillResignActiveNotification
                                                   object:nil];
        self.scanningBlock = nil;
        self.errorBlock = nil;
        self.terminatedBlock = nil;
        self.manager = nil;
        self.pauseTime = nil;
        self.pausedForManualEntry = NO;
    }
    return self;
}

- (void) dealloc
{
    DebugLog(@"dealloc");
    if (self.manager) {
        [self.manager closeScanner];
        // tell the client
        if (self.terminatedBlock) {
            self.terminatedBlock();
        }
    }
    self.manager = nil;
    self.scanningBlock = nil;
    self.errorBlock = nil;
    self.terminatedBlock = nil;
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(UIResponder *)nextResponder
{
    return UIApplication.sharedApplication.keyWindow;
}

#pragma mark - SMScannerManagerDelegate



- (void) scannerManager:(SMScannerManager *)manager
         barcodeScanned:(SMBarcode *)barcode

{
    [self scanBarcode:barcode];
}

- (void)scanBarcode:(SMBarcode *)barcode
{
    DebugLog(@"barcodeScanned");
    
    double delayInSeconds = 0.1;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        // handle barcode scan; trim eventually transmitted CRLF characters from
        // the data.
        
        NSString *barcodeText = [barcode.data stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"\r\n"]];
        DebugLog(@"Scan received: %@", barcodeText);
        
        // if there is a client listening
        if (self.scanningBlock) {
            // pause scanning
            if (self.sourceType != SMScannerSourceTypeInternal) {
                // unless we are using the camera, as this makes the zbar view
                // behave a little strangely
                DebugLog(@"Pausing scanning");
                [self pauseScanning];
            }
            // send the barcode scan
            DebugLog(@"Sending scan to listener");
            BOOL continueScanning = YES;
            self.scanningBlock(barcodeText, ScannerEntryMethodScanned, barcode.barcodeType, &continueScanning);
            // does the client want to continue?
            if (!continueScanning) {
                // no, stop scanning
                DebugLog(@"Listener is done scanning, stopping");
                [self stopScanning];
            }
        } else {
            // just broadcast the notification
            DebugLog(@"Sending scan notification");
            NSDictionary *userInfo =
            @{ kScannerNotificationBarcodeScannedBarcodeKey: barcode };
            NSNotification *note =
            [NSNotification notificationWithName:kScannerNotificationBarcodeScanned
                                          object:self
                                        userInfo:userInfo];
            [[NSNotificationCenter defaultCenter] postNotification:note];
        }
    });
}

- (void) scannerManager:(SMScannerManager *)manager
scanFailedWithErrorCode:(SMScanError)errorCode
{
    DebugLog(@"scanFailedWithErrorCode");
    // if there is a client listening
    if (self.errorBlock) {
        // build the error text
        NSString *scanError = nil;
        BOOL close = YES;
        switch (errorCode) {
            case SMScanErrorAborted:
                scanError = nil;
                close = NO;
                break;
            case SMScanErrorInitialization:
                close = NO;
//                scanError = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁗󠁪󠁸󠁶󠀷󠁮󠁦󠁮󠁫󠁏󠁃󠁦󠀰󠁺󠁓󠁄󠁈󠁧󠀴󠁈󠁡󠁧󠁪󠁱󠁍󠁴󠁑󠁿*/ @"The barcode scanner has not been initialised. Please check the scanner and try again later.", nil);
                break;
            case SMScanErrorFeatureNotSupported:
                scanError = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁷󠁳󠁹󠀵󠀸󠀰󠁔󠁘󠁙󠁧󠁗󠁮󠁔󠁁󠁵󠁫󠁊󠁃󠁐󠁆󠁉󠁴󠁫󠁶󠁑󠁇󠀴󠁿*/ @"Feature not supported.", nil);
                break;
            case  SMScanErrorInvalidData:
                scanError = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁇󠁸󠁃󠁲󠁦󠀯󠁔󠁵󠁄󠁤󠁱󠁭󠁡󠁄󠁃󠁧󠁨󠁄󠁥󠁓󠀱󠁎󠁗󠁚󠀸󠀰󠁉󠁿*/ @"Invalid data.", nil);
                break;
            case SMScanErrorDeviceDisconnected:
                scanError = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁱󠁰󠁰󠁓󠁡󠁷󠁩󠁶󠁥󠁌󠁄󠁧󠀸󠁡󠁕󠁆󠁸󠀴󠁭󠁤󠁷󠁰󠁡󠁴󠁇󠀯󠁷󠁿*/ @"The external barcode scanner was disconnected or powered off. Please check the scanner.", nil);
                break;
            default:
                scanError = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠀫󠁪󠁤󠁓󠀶󠁲󠁹󠀯󠁊󠁙󠁏󠁸󠁍󠁈󠁣󠁫󠁐󠁚󠁃󠁺󠀸󠁄󠁤󠁌󠁱󠁘󠁍󠁿*/ @"Unknown error", nil);
                break;
        }
        if (scanError != nil) {
            DebugLog(@"Sending error to listener");
            // send it to the client
            self.errorBlock(scanError);
        }
        if (close) {
            // stop scanning
            DebugLog(@"Closing the scanner");
            [self.manager abortScan];
            // close the scanner
            if (self.manager) {
                [self.manager closeScanner];
            }
            self.manager = nil;
        }
    }
}

#pragma mark - UITextFieldDelegate

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSString *barcodeText = [textField.text stringByReplacingCharactersInRange:range
                                                                    withString:string];
    
    if ((barcodeText != nil) &&
        ([[barcodeText stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length] > 0)) {
        textField.FNKInputView_doneButtonTitle = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁃󠀲󠁕󠁲󠁤󠀲󠁯󠁮󠁊󠀸󠁘󠁓󠁌󠁕󠁆󠁨󠁲󠁤󠁅󠁣󠁃󠁯󠁙󠁵󠁂󠀹󠁁󠁿*/ @"Done", nil);
    } else {
        textField.FNKInputView_doneButtonTitle = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁃󠀲󠁕󠁲󠁤󠀲󠁯󠁮󠁊󠀸󠁘󠁓󠁌󠁕󠁆󠁨󠁲󠁤󠁅󠁣󠁃󠁯󠁙󠁵󠁂󠀹󠁁󠁿*/ @"Done", nil);
    }
    
    return YES;
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    NSString *barcodeText = textField.text;
    // TFS 62610 - clear out the manual entry text field
    textField.text = nil;
    _textField.FNKInputView_doneButtonTitle = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁃󠀲󠁕󠁲󠁤󠀲󠁯󠁮󠁊󠀸󠁘󠁓󠁌󠁕󠁆󠁨󠁲󠁤󠁅󠁣󠁃󠁯󠁙󠁵󠁂󠀹󠁁󠁿*/ @"Done", nil);
    
    if ((barcodeText != nil) &&
        ([[barcodeText stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length] > 0)) {
        // send the manual barcode to the controller
        [self sendManualEntry:barcodeText];
    } else {
        [textField resignFirstResponder];
        // are we scanning?
        if (self.scanning) {
            // stop
            [self stopScanning];
        }
    }
    
    
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    if (!textField.FNKInputView_switchingKeyboardType) {
        if (self.scanning && !self.pauseTime) {
            // stop
            [self stopScanning];
            // TFS 68387 - clear out the manual entry text field
            textField.text = nil;
        }
    }
    textField.FNKInputView_switchingKeyboardType = NO;
}

#pragma mark - Methods

- (void) registerForNotifications:(id)registrant
                     withSelector:(SEL)selector
{
    NSNotificationCenter *defaultCenter = [NSNotificationCenter defaultCenter];
    // register for status changes
    [defaultCenter addObserver:registrant
                      selector:selector
                          name:kScannerNotificationBarcodeScanned
                        object:nil];
}

- (void) unregisterForNotifications:(id)deregistrant
{
    [[NSNotificationCenter defaultCenter] removeObserver:deregistrant
                                                    name:kScannerNotificationBarcodeScanned
                                                  object:nil];
}

- (void) enableHardwareButtonScanning:(BOOL)enabled
{
    // initialise the scanner manager if we don't have one
    if (self.manager == nil) {
        DebugLog(@"Initialising scanner manager");
        self.manager = [[SMScannerManager alloc] initWithSource:self.sourceType];
        self.manager.delegate = self;
    }
    if (self.manager) {
        [self.manager enableHardwareButtonScanning:enabled];
    }
}

- (void) beginScanningInViewController:(UIViewController *)vc
                             forReason:(NSString *)scanReason
                        withButtonSize:(CGSize)buttonSize
                          withAddTitle:(NSString *)addTitle
                              mustScan:(BOOL)mustScan
                   alphaNumericAllowed:(BOOL)alphaNumericAllowed
                             withBlock:(BarcodeScannedBlock)scanned
                            terminated:(BarcodeScanTerminatedBlock)ended
                                 error:(BarcodeErroredBlock)error
{
    DebugLog(@"beginScanning");
    self.pausedForManualEntry = NO;
    // keep references to the blocks
    self.scanningBlock = scanned;
    self.errorBlock = error;
    self.terminatedBlock = ended;
    // initialise the scanner manager if we don't have one
    if (self.manager == nil) {
        DebugLog(@"Initialising scanner manager");
        self.manager = [[SMScannerManager alloc] initWithSource:self.sourceType];
        self.manager.delegate = self;
    }
    
    if (_textField == nil)
    {
        _textField = [[FNKTextField alloc] initWithFrame:CGRectMake(0, 0, 50, 30)];
        _textField.nextResponder = self;
        _textField.borderStyle = UITextBorderStyleRoundedRect;
        _textField.inputAccessoryView = [[FNKInputView alloc] initWithFrame:CGRectMake(0, 0, 200, 44) inputViewStyle:UIInputViewStyleKeyboard];
        _textField.delegate = self;
        [_textField.inputAccessoryView addSubview:_textField];
    }
    
    NSMutableArray *keyboardTypes = [NSMutableArray array];
    
    if (self.sourceType == SMScannerSourceTypeInternal) {
        DebugLog(@"Setting up the scanner camera view");
        // setup the camera view
        
        FNKeyboardType *keyboardType = [[FNKeyboardType alloc] init];
        keyboardType.title = @"\uF003";
        keyboardType.titleAttributes = @{
                                         NSFontAttributeName: [UIFont fontWithName:@"OmnicoGroup-Regular" size:18.0],
                                         };
        
        CameraInputView *barcodeInputView = [[CameraInputView alloc] initWithFrame:CGRectMake(0, 0, 200, KeyboardHeight())];
        barcodeInputView.orientation = [[UIApplication sharedApplication] statusBarOrientation];
        keyboardType.inputView = barcodeInputView;
        [keyboardTypes addObject:keyboardType];
    } else if (self.sourceType != SMScannerSourceTypeManual) {
        FNKeyboardType *keyboardType = [[FNKeyboardType alloc] init];
        keyboardType.title = @"\uF004";
        keyboardType.titleAttributes = @{
                                         NSFontAttributeName: [UIFont fontWithName:@"OmnicoGroup-Regular" size:18.0],
                                         };
        keyboardType.inputView = [[BarcodeScannerInputView alloc] initWithFrame:CGRectMake(FLT_MAX, FLT_MAX, FLT_MIN, FLT_MIN)];
        [keyboardTypes addObject:keyboardType];
    }
    
    {
        FNKeyboardType *keyboardType = [[FNKeyboardType alloc] init];
        keyboardType.title = [POSInputView layoutTitle];
        keyboardType.titleAttributes = @{
                                         NSFontAttributeName: [UIFont fontWithName:@"OmnicoGroup-Regular" size:18.0],
                                         };
        keyboardType.inputView = [[POSInputView alloc] init];
        [keyboardTypes addObject:keyboardType];
    }
    
    if (alphaNumericAllowed)
    {
        [keyboardTypes addObject:@(UIKeyboardTypeASCIICapable)];
    }
    
    _textField.FNKInputView_keyboardTypes = keyboardTypes;
    
    // TFS 74644 - Bit of a hack to get around the camera feed not showing
    // the first time scanning is enabled after the app has started.
    // Blame reloadInputViews...
    _textField.inputView = ((FNKeyboardType *)keyboardTypes[0]).inputView;
    
    _textField.placeholder = scanReason;
    
    _textField.FNKInputView_doneButtonTitle = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁃󠀲󠁕󠁲󠁤󠀲󠁯󠁮󠁊󠀸󠁘󠁓󠁌󠁕󠁆󠁨󠁲󠁤󠁅󠁣󠁃󠁯󠁙󠁵󠁂󠀹󠁁󠁿*/ @"Done", nil);
    
    _textField.FNKInputView_switchingKeyboardType = YES;
    
    // if we are not doing manual entry
    if (self.sourceType != SMScannerSourceTypeManual && self.sourceType != SMScannerSourceTypeInternal) {
        // start scanning
        double delayInSeconds = 0.01;
        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
        dispatch_after(popTime, dispatch_get_main_queue(), ^(void) {
            DebugLog(@"Starting scan");
            [self.manager scanBarcode];
        });
    }
    
    [_textField becomeFirstResponder];
}

- (void) beginScanningWithBlock:(BarcodeScannedBlock)scanned
                     terminated:(BarcodeScanTerminatedBlock)ended
                          error:(BarcodeErroredBlock)error
{
    DebugLog(@"beginScanning");
    self.pausedForManualEntry = NO;
    // keep references to the blocks
    self.scanningBlock = scanned;
    self.errorBlock = error;
    self.terminatedBlock = ended;
    // initialise the scanner manager if we don't have one
    if (self.manager == nil) {
        DebugLog(@"Initialising scanner manager");
        self.manager = [[SMScannerManager alloc] initWithSource:self.sourceType];
        self.manager.delegate = self;
    }
        
    NSMutableArray *keyboardTypes = [NSMutableArray array];
    
//    if (self.sourceType == SMScannerSourceTypeInternal) {
//        DebugLog(@"Setting up the scanner camera view");
//        // setup the camera view
//        
//        FNKeyboardType *keyboardType = [[FNKeyboardType alloc] init];
//        keyboardType.title = @"\uF003";
//        keyboardType.titleAttributes = @{
//                                         NSFontAttributeName: [UIFont fontWithName:@"OmnicoGroup-Regular" size:18.0],
//                                         };
//        
//        CameraInputView *barcodeInputView = [[CameraInputView alloc] initWithFrame:CGRectMake(0, 0, 200, KeyboardHeight())];
//        
//        keyboardType.inputView = barcodeInputView;
//        [keyboardTypes addObject:keyboardType];
//    } else if (self.sourceType != SMScannerSourceTypeManual) {
//        FNKeyboardType *keyboardType = [[FNKeyboardType alloc] init];
//        keyboardType.title = @"\uF004";
//        keyboardType.titleAttributes = @{
//                                         NSFontAttributeName: [UIFont fontWithName:@"OmnicoGroup-Regular" size:18.0],
//                                         };
//        keyboardType.inputView = [[BarcodeScannerInputView alloc] initWithFrame:CGRectMake(FLT_MAX, FLT_MAX, FLT_MIN, FLT_MIN)];
//        [keyboardTypes addObject:keyboardType];
//    }
//    
//    {
//        FNKeyboardType *keyboardType = [[FNKeyboardType alloc] init];
//        keyboardType.title = [POSInputView layoutTitle];
//        keyboardType.titleAttributes = @{
//                                         NSFontAttributeName: [UIFont fontWithName:@"OmnicoGroup-Regular" size:18.0],
//                                         };
//        keyboardType.inputView = [[POSInputView alloc] init];
//        _textField.inputView = keyboardType.inputView;
//        [keyboardTypes addObject:keyboardType];
//    }
    // if we are not doing manual entry
    if (self.sourceType != SMScannerSourceTypeManual && self.sourceType != SMScannerSourceTypeInternal) {
        // start scanning
        double delayInSeconds = 0.01;
        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
        dispatch_after(popTime, dispatch_get_main_queue(), ^(void) {
            DebugLog(@"Starting scan");
            [self.manager scanBarcode];
        });
    }
}

- (void) sendManualEntry:(NSString *)barcode
{
    DebugLog(@"sendManualEntry");
    // if there is a client listening
    if (self.scanningBlock) {
        // send the barcode scan
        BOOL continueScanning = YES;
        self.scanningBlock(barcode, ScannerEntryMethodKeyed, SMBarcodeTypeUnknown, &continueScanning);
        // does the client want to continue?
        if (!continueScanning) {
            // no, abort scan
            [self stopScanning];
        } else {
            // disable the pause for manual scanning
            self.pauseTime = nil;
        }
    }
}

- (void) pauseScanning
{
    DebugLog(@"pauseScanning");
    // make a note of the time
    self.pauseTime = [NSDate date];
    // if pausing for manual entry
    if (self.pausedForManualEntry) {
        // stop scanning
        if (self.manager) {
            [self.manager abortScan];
        }
    } else {
        [_textField resignFirstResponder];
    }
    self.pausedForManualEntry = NO;
}

- (void) pauseScanningForManualEntry
{
    self.pausedForManualEntry = YES;
    [self pauseScanning];
}

- (void) resumeScanningWithDelay:(BOOL)delay
{
    DebugLog(@"resumeScanning");
    
    // calulate how much longer to delay
    int64_t finalDelayInNanoSeconds = 0;
    if (delay) {
        if (self.pauseTime) {
            int64_t nanoSecondsSincePause = [[NSDate date] timeIntervalSinceDate:self.pauseTime] * NSEC_PER_SEC;
            int64_t delayInNanoSeconds = self.waitInSeconds.doubleValue * NSEC_PER_SEC;
            // if the configured delay time hasn't passed
            if (delayInNanoSeconds > nanoSecondsSincePause) {
                // wait till it has
                finalDelayInNanoSeconds = delayInNanoSeconds - nanoSecondsSincePause;
            }
        }
    }
    if (finalDelayInNanoSeconds == 0) {
        if (self.scanningBlock != nil) {
            [self resumeScanning];
        }
    } else {
        NSDate *originalPauseTime = [self.pauseTime copy];
        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, finalDelayInNanoSeconds);
        dispatch_after(popTime, dispatch_get_main_queue(), ^(void) {
            if ((self.scanningBlock != nil) &&
                ([self.pauseTime isEqualToDate:originalPauseTime])) {
                [self resumeScanning];
            }
        });
    }
}

- (void) stopScanning
{
    DebugLog(@"stopScanning");
    
    [_textField resignFirstResponder];
//    _inputAccessoryView = nil;
   
    if (self.scanningBlock != nil) {
        // clear the scanning block
        self.scanningBlock = nil;
        // dismiss the manual entry keyboard
    }
    [self stopScanningAnimationsCompleted:YES];
}

- (void) updateCamerViewOrientation:(UIInterfaceOrientation) orientation
{
    if ([_textField.inputView isKindOfClass:[CameraInputView class]]) {
        ((CameraInputView *)_textField.inputView).orientation = orientation;
    }
}

- (BOOL) canScanBarcodeInBatch
{
    BOOL result = YES;
    
    ClientSettings *settings = [[BasketController sharedInstance] clientSettings];
    if (settings != nil) {
        if (settings.scanMode != nil) {
            if ([settings.scanMode isEqualToString:@"SINGLE"]) {
                result = NO;
            }
        }
    }
    
    return result;
}

- (void) stopScanningAnimationsCompleted:(BOOL)finished
{
    // shut down the scanner
    if (self.manager) {
        [self.manager abortScan];
    }
    // tell the client we are done
    if (self.terminatedBlock) {
        self.terminatedBlock();
    }
    // get rid of the blocks
    self.scanningBlock = nil;
    self.errorBlock = nil;
    self.terminatedBlock = nil;

}

#pragma mark - UIApplicationWillResignActiveNotification

-(void)handleApplicationWillResignActiveNotification:(NSNotification*)notification
{
    // are we scanning?
    if (self.scanning)
    {
        // yes, stop scanning
        DebugLog(@"stopScanning");
        
        if (self.scanningBlock != nil)
        {
            [self stopScanning];
        }
    }
}

@end

@implementation BarcodeScannerInputView

-(void)didMoveToWindow
{
    if (self.window)
    {
        BarcodeScannerController.sharedInstance.pausedForManualEntry = YES;
        [BarcodeScannerController.sharedInstance resumeScanningWithDelay:NO];
    }
    else
    {
        [BarcodeScannerController.sharedInstance pauseScanningForManualEntry];
    }
}

@end
