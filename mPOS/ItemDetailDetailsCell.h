//
//  ItemDetailDetailsCell.h
//  mPOS
//
//  Created by Antonio Strijdom on 23/05/2013.
//  Copyright (c) 2013 Clarity. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CRSGlossyButton.h"

@interface ItemDetailDetailsCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *SKULabel;
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;
@property (weak, nonatomic) IBOutlet UILabel *descriptionLabel;
@property (weak, nonatomic) IBOutlet UIView *bodyView;
@property (weak, nonatomic) IBOutlet UILabel *skuDescLabel;
@property (weak, nonatomic) IBOutlet UILabel *priceDescLabel;
@property (weak, nonatomic) IBOutlet CRSGlossyButton *descriptionBackground;

@end
