//
//  ModifierSelection.h
//  mPOS
//
//  Created by Antonio Strijdom on 12/09/2014.
//  Copyright (c) 2014 Omnico. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RESTController.h"
#import "ModifierSelection.h"

@interface GroupSelection : NSObject

@property (nonatomic, strong) ItemModifierGroup *group;
@property (nonatomic, strong) NSArray *selections;

- (ModifierSelection *)selectionForModifier:(ItemModifier *)modifier;

@end
