//
//  FormController.m
//  mPOS
//
//  Created by John Scott on 01/05/2015.
//  Copyright (c) 2015 Clarity. All rights reserved.
//

#import "FormController.h"

#import <objc/runtime.h>

@implementation FormController
{
    UINavigationController *_mainController;
    UINavigationController *_modalController;
    UIAlertController *_alertController;
}

static char SHARED_INSTANCE_IDENTIFER;

+ (void) setSharedInstance:(FormController *) sharedInstance
{
    objc_setAssociatedObject(self, &SHARED_INSTANCE_IDENTIFER, sharedInstance, OBJC_ASSOCIATION_RETAIN);
}

+ (FormController *) sharedInstance
{
    __block id sharedInstance = objc_getAssociatedObject(self, &SHARED_INSTANCE_IDENTIFER);
    if (sharedInstance != nil) {
        return sharedInstance;
    }
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[self alloc] init];
        self.sharedInstance = sharedInstance;
    });
    
    return sharedInstance;
}

-(void)handleModelObject:(id)modelObject contextName:(NSString*)contextName remainingEvents:(NSArray*)remainingEvents
{
    NSArray *contextNameParts = [contextName componentsSeparatedByString:@" "];
    
    DebugLog2(@"{} : {}", NSStringFromClass([modelObject class]), contextName);
    Class typeClass = [RESTController.sharedInstance classForContextName:contextNameParts[0]];
    
    if (typeClass && [modelObject class] != typeClass)
    {
        modelObject = [[typeClass alloc] init];
    }
    
    SoftKeyContext *softKeyContext = [BasketController.sharedInstance getLayoutForContextNamed:contextName];
    
    if (!softKeyContext && modelObject)
    {
        softKeyContext = [self softKeyContextForModelObject:modelObject];
    }
    
    if (!modelObject && softKeyContext)
    {
        
        NSString *contextName = softKeyContext.contextType;
        if ([contextName containsString:@" "])
        {
            contextName = [contextName substringToIndex:[contextName rangeOfString:@" "].location];
        }
        
        Class typeClass = [RESTController.sharedInstance classForContextName:contextName];
        
        modelObject = [[typeClass alloc] init];
    }
    
    if (softKeyContext.presentation.length == 0)
    {
        
    }

}

- (SoftKeyContext *)softKeyContextForModelObject:(id)modelObject
{
    SoftKeyContext *softKeyContext = nil;
    
    Class modelClass = [modelObject class];
    while (!softKeyContext && modelClass)
    {
        NSString *modelContext = [modelClass contextName];
        NSString *modelStatus = nil;//[modelClass statusForModelObject:modelObject];
        
        if (modelContext.length)
        {
            if (modelStatus.length)
            {
                NSString *contextName = [modelContext stringByAppendingFormat:@" %@", modelStatus];
                softKeyContext = [BasketController.sharedInstance getLayoutForContextNamed:contextName];
            }
            
            if (!softKeyContext && !softKeyContext.onCloseFunction && !softKeyContext.onSubmitFunction)
            {
                NSString *contextName = modelContext;
                softKeyContext = [BasketController.sharedInstance getLayoutForContextNamed:contextName];
            }
        }
        
        modelClass = class_getSuperclass(modelClass);
    }
    
    if (!softKeyContext.rootMenu.length && !softKeyContext.onCloseFunction && !softKeyContext.onSubmitFunction)
    {
        // The soft key context is deliberately blank. Dismiss it.
        softKeyContext = nil;
    }
    
    return softKeyContext;
}


@end
