//
//  MoreOptionsTableViewController.m
//  mPOS
//
//  Created by Antonio Strijdom on 21/11/2012.
//  Copyright (c) 2012 Clarity. All rights reserved.
//

#import "MoreOptionsTableViewController.h"
#import "CRSSkinning.h"
#import "BasketController.h"
#import "BasketViewController.h"
#import "RESTController.h"

@implementation MoreOptionsTableViewController
{
    BOOL _needsReloadData;
}

#pragma mark - Properties

@synthesize softKey = _softKey;
- (void) setSoftKey:(SoftKey *)softKey
{
    _softKey = softKey;
    [self setSoftKeyContext:softKey.context menu:softKey.softKeyMenu modelObject:BasketController.sharedInstance.basket];
}

#pragma mark - UIViewController

- (void) viewDidLoad
{
    [super viewDidLoad];
}

- (void) didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITableViewDelegate

-(BOOL)shouldEnableSoftKey:(SoftKey*)softKey modelObject:(id)modelObject
{
    // init
    BOOL result = YES;
    // check to see if this option can be selected
    BOOL active = [self.delegate isMenuOptionActive:softKey];
    if (!active) {
        result = NO;
    }
    
    return result;
}

-(void)didSelectSoftKey:(SoftKey *)softKey modelObject:(id)modelObject
{
    BOOL active = [self.delegate isMenuOptionActive:softKey];
    if (active) {
        [self.delegate moreOptionsController:self
                             didSelectOption:softKey];
    }
}

@end
