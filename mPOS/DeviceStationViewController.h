//
//  DeviceStationViewController.h
//  mPOS
//
//  Created by Antonio Strijdom on 16/10/2013.
//  Copyright (c) 2013 Clarity. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ServerErrorMessage;

@interface DeviceStationViewController : UIViewController

typedef void(^deviceScanCompleteBlockType)(NSString *deviceStationId);

/** @brief Presents an alert for a device station error.
 *  @param errorCode The device station error.
 *  @param deviceStationId The device station relevant to this error.
 *  @param deviceStationName The name of the device station relevant to the error.
 *  @param tillId The till (drawer insert) id relevant to this error.
 *  @param terminal The terminal id relevant to this error.
 *  @param user The user relevant to this error.
 *  @param unavailable The peripherals that are reported as unavailable.
 *  @param dismissedBlock The block to execute when the alert is dismissed.
 */
- (void) presentAlertForDeviceStationError:(ServerErrorMessage*)errorCode
                       withDeviceStationId:(NSString *)deviceStationId
                      andDeviceStationName:(NSString *)deviceStationName
                                 andTillId:(NSString *)tillId
                               andTerminal:(NSString *)terminal
                                   andUser:(NSString *)user
                 andUnavailablePeripherals:(NSString *)unavailable
                                 dismissed:(dispatch_block_t)dismissedBlock;

/** @brief Presents the UI for scanning a device station barcode.
 *  @param complete The block to execute once the barcode is scanned.
 *  @param allowCancel Whether or not cancel is allowed.
 */
- (void) requestDeviceScanComplete:(deviceScanCompleteBlockType)complete
                       allowCancel:(BOOL)allowCancel;

/** @brief Presents the UI for scanning a device station barcode and linking to it.
 *  @param complete The block to execute once the barcode is linked.
 */
- (void) requestDeviceScanForLinkComplete:(deviceScanCompleteBlockType)complete;

/** @brief Presents the UI for scanning a device station barcode to get its status and displays it.
 */
- (void) getDeviceStatus;

@end
