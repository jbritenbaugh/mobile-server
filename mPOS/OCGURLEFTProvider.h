//
//  OCGEFTProvider.h
//  mPOS
//
//  Created by Antonio Strijdom on 15/04/2013.
//  Copyright (c) 2013 Clarity. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "OCGEFTManager.h"
#import "OCGURLSchemeHandler.h"

@class ServerError;
@class OCGEFTManager;
@class Tender;
@class MPOSBasketTender;

/** @file OCGEFTProvider.h */

/** @enum EFTProviderStatus
 *  @brief Status codes indicating provider availability.
 */
typedef NS_ENUM(NSInteger, EFTProviderStatus) {
    kEFTProviderStatusAvailable = 0,
    kEFTProviderStatusStoredPending = 1,
    kEFTProviderStatusNotAvailable = 2
};

/** @brief Base class for all URL based EFT provider interfaces.
 */
@interface OCGURLEFTProvider : NSObject <OCGEFTProviderProtocol, OCGURLProviderProtocol>

/** @property manager
 *  @brief The manager hosting this provider.
 */
@property (nonatomic, readonly) OCGEFTManager *manager;

/** @property status
 *  @brief Flag indicating current status of the provider.
 *  @see EFTProviderStatus
 */
@property (nonatomic, assign) EFTProviderStatus status;

/** @property lastReplyDict
 *  @brief Parsed version of the last reply URL.
 */
@property (nonatomic, strong) NSDictionary *lastReplyDict;

/** @property lastEFTReply
 *  @brief The last reply from the EFT provider.
 */
@property (nonatomic, readonly) NSURL *lastEFTReply;

/** @brief Returns the URL scheme used by this class
 *  @return returnURLScheme the URL scheme used by this class
 */
+ (NSString *) returnURLScheme;

/** @brief Returns the URL of the payment provider.
 */
+ (NSString *) providerURL;

/** @brief Creates a basket tender from the dictionary specified.
 *  @return The new basket tender.
 */
- (BasketTender *) updatePaymentFromDictionary:(NSDictionary *)options;

/** @brief Builds the parameter string from the dictionary passed in.
 *  @param options The options to send to the provider.
 *  @return A parameter string representation of the options dictionary.
 */
- (NSString *) buildOptionStringFromDict:(NSDictionary *)options;

/** @brief Builds a dictionary from the parameter string passed in.
 *  @param url The URL containing the parameter string.
 *  @return An NSDictionary containing the Payware result or nil if the
 *  parameter string was invalid.
 */
- (NSDictionary *) buildDictionaryFromURL:(NSURL *)url;

/** @brief Sends the request specified to the payment provider.
 *  @param params The dictionary containing the parameters to send to the provider.
 *  @return 0 if the request was sent, otherwise the appropriate error.
 *  @note This method will return kPaywareIntegrationErrorCodePaywareNotInstalled if
 *  Payware is not installed or kPaywareIntegrationErrorCodeNotInSled if the sled is
 *  not connected.
 */
- (EFTProviderErrorCode) sendRequestWithParams:(NSDictionary *)params;

/** @brief Determines if the request being made is a retry request.
 *  @param requestDict The request we are sending.
 *  @return YES if the request is a retry, otherwise NO.
 */
- (BOOL) isRetryRequest:(NSDictionary *)requestDict;

/** @brief Determines if the provider response is an error condition.
 *  @param params The parameter dictionary returned from the provider.
 *  @return The error code if there was as error, otherwise 0.
 */
- (EFTProviderErrorCode) checkForErrorInResponse:(NSDictionary *)params;

/** @brief Updates the provider status based on the response returned.
 *  @param response A successful response from the provider.
 *  @return The reason the transaction completed.
 */
- (EFTPaymentCompleteReason) updateStatusFromSuccessfulResponse:(NSDictionary *)response;

/** @brief Updates the provider status based on the response returned.
 *  @param response A failed response from the provider.
 *  @param errorCode The error that was the cause of the failure.
 */
- (void) updateStatusFromFailedResponse:(NSDictionary *)response
                          withErrorCode:(EFTProviderErrorCode)errorCode;

/** @brief This method should be called by the application delegate.
 *  It will decode and process the results coming back from the payment provider.
 *  @param url The URL called on the app delegate.
 *  @param completeReason The reason the payment completed (if it did complete).
 *  @return The error code or kEFTProviderErrorCodeSuccess if the URL could be processed successfully.
 */
- (EFTProviderErrorCode) handlePaymentURL:(NSURL *)url
                    paymentCompleteReason:(EFTPaymentCompleteReason *)completeReason;

/** @brief Logs a payment url to the eft log file.
 *  @param url The URL to log.
 *  @param incoming YES if the URL was received from the provider, NO if it is a send.
 */
- (void) logPaymentURL:(NSURL *)url
              incoming:(BOOL)incoming;

@end
