//
//  FormController.h
//  mPOS
//
//  Created by John Scott on 01/05/2015.
//  Copyright (c) 2015 Clarity. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FormController : NSObject

+ (void) setSharedInstance:(FormController *) sharedInstance;
+ (FormController *) sharedInstance;

-(void)handleModelObject:(id)modelObject contextName:(NSString*)contextName remainingEvents:(NSArray*)remainingEvents;

@end
