//
//  StatusNavigationBar.m
//  mPOS
//
//  Created by John Scott on 15/05/2014.
//  Copyright (c) 2014 Clarity. All rights reserved.
//

#import "StatusNavigationBar.h"

static CGFloat StatusNavigationBarExtraHeight = 20;

@implementation StatusNavigationBar

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.transform = CGAffineTransformTranslate(self.transform, 0, -StatusNavigationBarExtraHeight);
    }
    return self;
}

- (CGSize)sizeThatFits:(CGSize)size
{
    CGSize sizeThatFits = [super sizeThatFits:size];
    sizeThatFits.height += StatusNavigationBarExtraHeight;
    return sizeThatFits;
}

-(void)layoutSubviews
{
    [super layoutSubviews];
    
    UIView *backgroundView = [self.subviews firstObject];
    CGRect frame = backgroundView.frame;
    frame.size.height = self.frame.size.height + StatusNavigationBarExtraHeight;
    
    backgroundView.frame = frame;
}

// This may be needed at a later date.
//
//- (UIView*)hitTest:(CGPoint)point withEvent:(UIEvent *)event
//{
//    UIView *view = nil;
////    if (point.y < CGRectGetMaxY(self.frame) - StatusNavigationBarExtraHeight)
//    {
//           view = [super hitTest:point withEvent:event];
//    }
//    NSLog(@"%f - %f", point.y, CGRectGetMaxY(self.frame));
//
//    return view;
//}

@end
