//
//  OCGContextCollectionViewDataSource.h
//  mPOS
//
//  Created by Antonio Strijdom on 21/07/2014.
//  Copyright (c) 2014 Omnico Group. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FNKCollectionViewLayout.h"

@protocol OCGPrintOptionSwitchDelegate;
@protocol OCGTextFieldDelegate;

/** @file OCGContextCollectionViewDataSource.h */

/** @brief Collection view data source class for displaying contexts
 */
@interface OCGContextCollectionViewDataSource : NSObject <UICollectionViewDataSource, FNKCollectionViewDataSourceDelegate>

/** @property parentDataSource
 *  @brief A weak reference to the parent data source.
 */
@property (weak, nonatomic) OCGContextCollectionViewDataSource *parentDataSource;

/** @property childDataSources;
 *  @brief An array of child data sources.
 */
@property (strong, nonatomic) NSArray *childDataSources;

/** @property sectionCount
 *  @brief The calculated number of sections
 */
@property (assign, nonatomic) NSInteger sectionCount;

/** @property totalRowCount
 *  @brief The number of rows this data source contributes to the collection view
 *  across ALL sections.
 */
@property (assign, nonatomic) NSInteger totalRowCount;

/** @property displayItem
 *  @brief The item the cell displays data of, e.g. basket item, pos function.
 */
@property (weak, nonatomic) id displayItem;

/** @property softKey
 *  @brief A reference to the template softkey used to build this item.
 *  Includes data like the key path the cell displays or the posfunc the
 *  item represents.
 */
@property (strong, nonatomic) SoftKey *softKey;

/** @property printOptionDelegate
 *  @brief Delegate object for handling print option switch toggles.
 */
@property (weak, nonatomic) id<OCGPrintOptionSwitchDelegate> printOptionDelegate;

/** @property printOptionDelegate
 *  @brief Delegate object for handling print option switch toggles.
 */
@property (weak, nonatomic) id<OCGTextFieldDelegate> textFieldDelegate;

/** @property indexPath
 *  @brief Index path this datasource represents.
 */
@property (strong, nonatomic) NSIndexPath *indexPath;

/** @brief Class method that returns a data source built by combining
 *  the context layout and the display data provided.
 *  @param context     The context menu to build the data source from.
 *  @param displayData The data to be displayed.
 *  @note Contexts require very specific display data, this link is not specified
 *  in the context layout itself.
 *  @return The data source for a UICollectionView.
 */
+ (OCGContextCollectionViewDataSource *)dataSourceForContext:(SoftKeyContext *)context
                                                    withData:(id)displayData;

/** @brief Class method that returns a data source built by combining
 *  the menu soft key and the display data provided.
 *  @param softKey     The softkey menu to build the data source from.
 *  @param displayData The data to be displayed.
 *  @return The data source for a UICollectionView.
 */
+ (OCGContextCollectionViewDataSource *)dataSourceForSoftKey:(SoftKey *)softKey
                                                    withData:(id)displayData;

/** @brief Updates the data source with the context and data specified.
 *  @param context     The context menu to build the data source from.
 *  @param displayData The data to be displayed.
 */
- (void)updataDataSourceWithContext:(SoftKeyContext *)context
                           withData:(id)displayData;

/** @brief Updates the data source with the softkey and data specified.
 *  @param softKey     The softkey menu to build the data source from.
 *  @param displayData The data to be displayed.
 */
- (void)updataDataSourceWithSoftKey:(SoftKey *)softKey
                           withData:(id)displayData;

/** @brief Registers the reuse ids for cells used by the data source with the 
 *  specified collection view.
 *  @param collectionView The collection view to register cell reuse ids with.
 */
- (void)registerCellReuseIdsWithCollectionView:(UICollectionView *)collectionView;

/** @brief Registers the reuse ids for supplementary view used by the data source with the
 *  specified collection view.
 *  @param collectionView The collection view to register cell reuse ids with.
 */
- (void)registerSupplementaryViewReuseIdsWithCollectionView:(UICollectionView *)collectionView;

/** @brief Adds a child data source to this data source
 *  @param childDataSource The child data source to add.
 */
//- (void)addChildDataSource:(OCGContextCollectionViewDataSource *)childDataSource;

/** @brief Gets the title displayed in the header for the section specified
 *  @param section The section to get the title of.
 *  @return The title for the section specified.
 *  @note If no title (nil or empty string) is returned, no section header
 *  supplementary view is returned when requested for the section.
 */
- (NSString *)titleForSection:(NSInteger)section;

/** @brief Updates the index path mappings for this datasource.
 */
- (void)updateMapping;

/** @brief Returns the data source responsible for the section specified.
 *  @param section The "flattened" section from the collection view.
 *  @return The data source for the section.
 */
- (OCGContextCollectionViewDataSource *)dataSourceForSection:(NSInteger)section;

/** @brief Returns the data source containing the cell displayed at the index path specified.
 *  @param indexPath The "flattened" index path from the collection view.
 *  @return The data source for the index path.
 */
- (OCGContextCollectionViewDataSource *)dataSourceForIndexPath:(NSIndexPath *)indexPath;

/** @brief Determines if this data source is on the summary tab.
 *  @return YES if the data source is on a summary, otherwise NO.
 */
- (BOOL)dataSourceIsPartOfSummary;

@end

@protocol OCGPrintOptionSwitchDelegate
@required
- (void) printReceiptSwitchToggled:(UISwitch *)switchDetail;
- (void) printSummarySwitchToggled:(UISwitch *)switchDetail;
- (void) emailReceiptSwitchToggled:(UISwitch *)switchDetail;
- (void) giftReceiptSwitchToggled:(UISwitch *)switchDetail;
@end

@protocol OCGTextFieldDelegate <NSObject>

-(BOOL)OCGContextCollectionViewDataSource:(OCGContextCollectionViewDataSource*)collectionViewDataSource
                      canEditDataLocation:(NSString*)dataLocation;

-(void)OCGContextCollectionViewDataSource:(OCGContextCollectionViewDataSource*)collectionViewDataSource
                      updateDataLocation:(NSString*)dataLocation
                                 withText:(NSString*)text;

@end
