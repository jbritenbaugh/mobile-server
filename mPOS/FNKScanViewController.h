//
//  FNKScanViewController.h
//  mPOS
//
//  Created by John Scott on 06/01/2015.
//  Copyright (c) 2015 Clarity. All rights reserved.
//

#import <UIKit/UIKit.h>

@class FNKScanViewController;

@protocol FNKScanViewDelegate <NSObject>

@optional

- (void)FNKScanViewController:(FNKScanViewController *)scanViewController
             didSelectSoftKey:(SoftKey *)softKey;

- (BOOL)FNKScanViewController:(FNKScanViewController *)scanViewController
          shouldSelectSoftKey:(SoftKey *)softKey;

- (void)FNKScanViewController:(FNKScanViewController *)scanViewController
         handleScannedBarcode:(NSString*)barcode;

@end

@interface FNKScanViewController : UICollectionViewController <FNKScanViewDelegate>

@property (nonatomic, strong) SoftKeyContext *softKeyContext;
@property (nonatomic, strong) id displayData;

- (void)startScanningForBarcode;
- (void)dismissViewController;

- (void)viewWillAppear:(BOOL)animated __attribute__((objc_requires_super));
@property (nonatomic, assign) id <FNKScanViewDelegate, UICollectionViewDelegate> delegate;


@end
