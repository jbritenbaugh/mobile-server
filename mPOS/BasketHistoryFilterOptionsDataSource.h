//
//  BasketHistoryFilterOptionsDataSource.h
//  mPOS
//
//  Created by Antonio Strijdom on 22/11/2012.
//  Copyright (c) 2012 Clarity. All rights reserved.
//

#import "CRSEditableTableFieldsDataSource.h"

#define kFieldFromDate  @"fromdate"
#define kFieldToDate    @"todate"
#define kFieldLocation  @"location"
#define kFieldAssociate @"associate"

/** @file BasketHistoryFilterOptionsDataSource.h */

/** @brief The datasource for the basket history filter options table view.
 */
@interface BasketHistoryFilterOptionsDataSource : CRSEditableTableFieldsDataSource

/** @property locations
 *  @brief The location list.
 */
@property (nonatomic, strong) NSArray *locations;

/** @property selectedOptions
 *  @brief A dictionary containing the current filter options.
 */
@property (nonatomic, strong) NSDictionary *selectedOptions;

/** @brief Builds the current selected options dictionary.
 *  @return The current filter selections.
 */
- (NSDictionary *) getOptions;

@end
