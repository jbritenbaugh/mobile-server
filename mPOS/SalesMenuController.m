//
//  SalesMenuViewController.m
//  mPOS
//
//  Created by Antonio Strijdom on 13/01/2014.
//  Copyright (c) 2014 Clarity. All rights reserved.
//

#import "SalesMenuController.h"
#import "BasketController.h"
#import "CRSSkinning.h"
#import "SalesMenuItemCell.h"
#import "SalesMenuFuncCell.h"
#import "OCGMenuPageViewController.h"
#import "BasketViewController.h"

@interface SalesMenuController() <OCGMenuPageViewControllerDataSource, OCGMenuPageViewControllerDelegate>
@property (nonatomic, strong) NSMutableDictionary *cellRegistryDict;
@property (nonatomic, weak) OCGMenuPageViewController *currentPage;
- (OCGMenuPageViewController *) menuPageViewControllerForMenu:(id)menu;
- (void) itemAddedNotification:(NSNotification *)notification;
@end

@implementation SalesMenuController

#pragma mark - Private

static CGFloat kDefaultCellPadding = 4.0f;
static CGFloat kMaxColumnCount = 4.0f;
static CGFloat kMaxRowCount = 6.0f;

- (OCGMenuPageViewController *) menuPageViewControllerForRootMenu
{
    return [self menuPageViewControllerForMenu:self.rootMenu];
}

- (OCGMenuPageViewController *) menuPageViewControllerForMenu:(id)menu
{
    // TODO : cache pages?
    
    // setup a new page collection view
    OCGMenuPageViewController *result =
    [[OCGMenuPageViewController alloc] initWithMenu:menu
                                   withCellReuseIds:self.cellRegistryDict
                                     withDataSource:self
                                        andDelegate:self];
    result.view.tag = kSalesMenuSkinningTag;
    [[CRSSkinning currentSkin] applyViewSkin:result];
    
    return result;
}

- (void) itemAddedNotification:(NSNotification *)notification
{
    // update the total
    self.currentPage.navigationItem.rightBarButtonItem =
    [[UIBarButtonItem alloc] initWithTitle:[[BasketController sharedInstance] basket].amountDue.display
                                     style:UIBarButtonItemStylePlain
                                    target:nil
                                    action:nil];
    NSDictionary *attributes =
    @{NSForegroundColorAttributeName : [[CRSSkinning currentSkin] getColorWithName:@"Navigation Bar Foreground Color"]};
    [self.currentPage.navigationItem.rightBarButtonItem setTitleTextAttributes:attributes
                                                                      forState:UIControlStateNormal];
    self.currentPage.navigationItem.rightBarButtonItem.enabled = NO;
}

#pragma mark - Properties

@synthesize rootMenu = _rootMenu;
- (void) setRootMenu:(SoftKeyContext *)rootMenu
{
    _rootMenu = rootMenu;
}

#pragma mark - Init

- (id) initWithRootMenu:(SoftKeyContext *)menu
{
    self = [super init];
    if (self) {
        self.rootMenu = menu;
        // register cells
        [self registerMenuCellFromXib:nil
                              orClass:[SalesMenuItemCell class]
                          withReuseId:@"SalesMenuItemCell"];
        [self registerMenuCellFromXib:nil
                              orClass:[SalesMenuFuncCell class]
                          withReuseId:@"SalesMenuFuncCell"];
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(itemAddedNotification:)
                                                     name:kBasketControllerBasketItemAddedNotification
                                                   object:nil];
    }
    return self;
}

- (void) dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - OCGMenuPageViewControllerDataSource

- (OCGMenuCell *) menuPageViewController:(OCGMenuPageViewController *)page
                         cellForMenuItem:(SoftKey *)menuItem
                             atIndexPath:(NSIndexPath *)indexPath
{
//    DebugLog(@"Cell for menu item %@ - %@", menuItem.keyId, menuItem.keyType);
    OCGMenuCell *result = nil;
    
    if ([menuItem.keyType isEqualToString:@"POSFUNC"]) {
        result = [page dequeueMenuItemCellWithReuseId:@"SalesMenuFuncCell"
                                         forIndexPath:indexPath];
    } else {
        result = [page dequeueMenuItemCellWithReuseId:@"SalesMenuItemCell"
                                         forIndexPath:indexPath];
    }
    
    if (result) {
        result.menuItem = menuItem;
        if ([menuItem.keyType isEqualToString:@"UPC"]) {
            result.tag = 11000;
        } else if ([menuItem.keyType isEqualToString:@"MENU"]) {
            result.tag = 12000;
        } else if ([menuItem.keyType isEqualToString:@"EMPTY"]) {
            result.tag = 13000;
        } else if ([menuItem.keyType isEqualToString:@"POSFUNC"]) {
            result.tag = 11000;
        }
        [[CRSSkinning currentSkin] applyViewSkin:result.contentView
                                 withTagOverride:result.tag];
    }
    
    return result;
}

- (UIBarButtonItem *) rightNavigationItemForMenuPageViewController:(OCGMenuPageViewController *)page
{    
    // update the total
    UIBarButtonItem *result =
    [[UIBarButtonItem alloc] initWithTitle:[[BasketController sharedInstance] basket].amountDue.display
                                     style:UIBarButtonItemStylePlain
                                    target:nil
                                    action:nil];
    NSDictionary *attributes =
    @{NSForegroundColorAttributeName : [[CRSSkinning currentSkin] getColorWithName:@"Navigation Bar Foreground Color"]};
    [result setTitleTextAttributes:attributes
                          forState:UIControlStateNormal];
    result.enabled = NO;
    return result;
}

- (NSArray *) toolbarItemsForMenuPageViewController:(OCGMenuPageViewController *)page
{
    return nil;
}

#pragma mark - OCGMenuViewControllerDelegate

- (void) menuPageViewController:(OCGMenuPageViewController *)page
                  didSelectItem:(SoftKey *)menuItem
{
    self.currentPage = page;
    if ([menuItem.keyType isEqualToString:@"UPC"]) {
        [BasketController.sharedInstance addItemWithScanId:menuItem.upc
                                                     price:nil
                                                   measure:nil
                                                  quantity:nil
                                               entryMethod:@"KEYED"
                                                    track1:nil
                                                    track2:nil
                                                    track3:nil
                                               barcodeType:nil];
    } else if ([menuItem.keyType isEqualToString:@"MENU"]) {
        OCGMenuPageViewController *pageViewController = [self menuPageViewControllerForMenu:menuItem];
        // make the back button always return to the basket
        UIViewController *rootVC = page.navigationController.viewControllers[0];
        if ([rootVC isKindOfClass:[BasketViewController class]]) {
            page.navigationItem.backBarButtonItem =
            [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁱󠀹󠁯󠁆󠁉󠁏󠀳󠁎󠁈󠁘󠁉󠁷󠁋󠁁󠁦󠁐󠁗󠁶󠀫󠁒󠁩󠀶󠁺󠁕󠁄󠁚󠁁󠁿*/ @"Basket", nil)
                                             style:UIBarButtonItemStylePlain
                                            target:nil
                                            action:nil];
            [page.navigationController setViewControllers:@[rootVC, pageViewController]
                                                 animated:YES];
        } else {
            [page.navigationController pushViewController:pageViewController
                                                 animated:YES];
        }
    }
}

- (CGPoint) cellPaddingForMenuPageViewController:(OCGMenuPageViewController *)page
{
    return CGPointMake(kDefaultCellPadding, kDefaultCellPadding);
}

- (CGFloat) cellHeightForMenuPageViewController:(OCGMenuPageViewController *)page
{
    return (page.collectionView.frame.size.height - 104.0f) / kMaxRowCount;
}

- (CGFloat) cellWidthForMenuItem:(SoftKey *)menuItem onPage:(OCGMenuPageViewController *)page
{
    return page.collectionView.frame.size.width / kMaxColumnCount;
}

#pragma mark - Methods

- (void) registerMenuCellFromXib:(UINib *)nibOrNil
                         orClass:(Class)classOrNil
                     withReuseId:(NSString *)reuseId
{
    if (self.cellRegistryDict == nil) {
        self.cellRegistryDict = [[NSMutableDictionary alloc] init];
    }
    if (nibOrNil != nil) {
        [self.cellRegistryDict setValue:nibOrNil
                                 forKey:reuseId];
    } else if (classOrNil != nil) {
        [self.cellRegistryDict setValue:classOrNil
                                 forKey:reuseId];
    }
}

@end
