//
//  OCGEFTManager.m
//  mPOS
//
//  Created by Antonio Strijdom on 15/04/2013.
//  Copyright (c) 2013 Clarity. All rights reserved.
//

#import "OCGEFTManager.h"
#import <objc/runtime.h>
#import "OCGEFTProviderAdyen.h"
#import "OCGEFTProviderPayware.h"
#import "OCGEFTProviderYespay.h"
#import "OCGEFTProviderFake.h"
#import "OCGEFTProviderFakeIntegrated.h"
#import "RESTController.h"
#import "OCGPeripheralManager.h"
#import "BasketController.h"
#import "OCGSettingsController.h"
#import "UIImage+OCGExtensions.h"
#import "SignatureCaptureViewController.h"
#import "Device+MPOSExtensions.h"
#import "ServerErrorHandler.h"

@interface OCGEFTManager () {
    dispatch_semaphore_t processingLock;
}
@property (nonatomic, readonly) id<OCGEFTProviderProtocol> provider;
- (NSString *) currentPaymentPath;
- (BOOL) saveCurrentPayment;
- (void) resumeCurrentPayment;
- (void) executePaymentStep;
- (void) continueCurrentPayment;
- (void) sendPaymentToServer;
- (void) endPaymentForErrorCode:(EFTProviderErrorCode)errorCode
                      withError:(NSError *)error
                  suppressError:(BOOL)suppressError;
- (void) handlePaymentAddedNotification:(NSNotification *)note;
- (void) handleActiveBasketChange:(NSNotification *)note;
- (void) handleGeneralError:(NSNotification *)note;
@end

@implementation OCGEFTManager

#pragma mark - Private

NSString * const kOCGEFTManagerConfiguredPaymentProviderKey = @"kOCGEFTManagerConfiguredPaymentProviderKey";
NSString * const kEFTNotificationTransactionChanged = @"kEFTNotificationTransactionChanged";
NSString * const kEFTNotificationError = @"kEFTNotificationError";
NSString * const kEFTNotificationErrorErrorKey = @"kEFTNotificationErrorErrorKey";
NSString * const kEFTProviderErrorDomain = @"kEFTProviderErrorDomain";

/** @brief Returns the path of the current payment.
 *  @return The path of the current payment.
 */
- (NSString *) currentPaymentPath
{
    NSURL *result = nil;
    
    result =
    [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory
                                            inDomains:NSUserDomainMask] lastObject];
    result = [result URLByAppendingPathComponent:@"eft.payment"];
    
    return [result path];
}

/** @brief Saves the current payment.
 *  @discussion Should be called whenever currentPayment is updated.
 *  @return YES if the payment is saved, otherwise NO.
 */
- (BOOL) saveCurrentPayment
{
    // save current payment to disk
    DebugLog(@"Saving current payment to %@\n%@", self.currentPaymentPath, self.currentPayment);
    BOOL result =
    [NSKeyedArchiver archiveRootObject:self.currentPayment
                                toFile:self.currentPaymentPath];
    if (!result) {
        ErrorLog(@"Error saving current payment to %@", self.currentPaymentPath);
    }
    
    return result;
}

/** @brief Resumes the payment process.
 *  @discussion This should be called in order to resume a payment that was in progress previously.
 */
- (void) resumeCurrentPayment
{
    if (0 == dispatch_semaphore_wait(processingLock, DISPATCH_TIME_NOW)) {
        DebugLog(@"Resuming EFT payment - current status is: %@", self.currentPayment.localizedStringForCurrentStatus);
        // resume payment from where it was
        switch (self.currentPayment.paymentStatus) {
            case kEFTPaymentStatusInitializing: {
                // still initialising, start a new payment
                [self endCurrentPayment];
                break;
            }
            case kEFTPaymentStatusProcessing:
            case kEFTPaymentStatusSignatureRequired: {
                // resume
                [self executePaymentStep];
                break;
            }
            case kEFTPaymentStatusFinishing: {
                // finishing, execute this step again
                [self executePaymentStep];
                break;
            }
            case kEFTPaymentStatusComplete: {
                // complete, execute this step again
                [self executePaymentStep];
                break;
            }
            default: {
                // ???, end the payment
                ErrorLog(@"Unknown payment state: %i", self.currentPayment.paymentStatus);
                [self endCurrentPayment];
                break;
            }
        }
    } else {
        WarningLog(@"ALREADY PROCESSING!");
    }
}

/** @brief Executes the payment step.
 */
- (void) executePaymentStep
{
    DebugLog(@"Executing payment step: %@", self.currentPayment.localizedStringForCurrentStatus);
    
    // setup the continue block
    EFTProviderCompleteBlock continueOrErrorBlock =
    ^(EFTProviderErrorCode resultCode, NSError *error, BOOL suppressError) {
        if (kEFTProviderErrorCodeSuccess == resultCode) {
            // step passed, move on to next step
            [self continueCurrentPayment];
        } else if (kEFTProviderErrorCodeSignatureRequired == resultCode) {
            // signature required
            self.currentPayment.paymentStatus = kEFTPaymentStatusSignatureRequired;
            if ([self saveCurrentPayment]) {
                [self executePaymentStep];
            }
        } else {
            // step failed, move to error state
            [self endPaymentForErrorCode:resultCode
                               withError:error
                           suppressError:suppressError];
        }
    };
    
    // execute
    switch (self.currentPayment.paymentStatus) {
        case kEFTPaymentStatusInitializing: {
            // initialising - perform the preflight check
            [self.provider preflightCheckForTransaction:YES
                                               complete:continueOrErrorBlock];
            break;
        }
        case kEFTPaymentStatusProcessing: {
            // check if we have a transaction back from the provider
            if (self.currentPayment.transaction == nil) {
                // we don't, perform the transaction
                if (kEFTPaymentTransactionTypeSale == self.currentPayment.transactionType) {
                    [self.provider cardSaleWithTender:self.currentPayment.tender
                                               amount:@(self.currentPayment.tenderAmount.amount.doubleValue)
                                      forBasketWithID:self.currentPayment.basketId
                                    withCustomerEmail:self.currentPayment.customerEmail
                                             complete:continueOrErrorBlock];
                } else if (kEFTPaymentTransactionTypeRefund == self.currentPayment.transactionType) {
                    [self.provider cardRefundWithTender:self.currentPayment.tender
                                             withAmount:@(self.currentPayment.tenderAmount.amount.doubleValue)
                                        forBasketWithID:self.currentPayment.basketId
                                      withCustomerEmail:self.currentPayment.customerEmail
                                               complete:continueOrErrorBlock];
                }
                // for URL providers
                if (self.provider.isURLProvider) {
                    // transaction flow ends here, so release processing lock
                    dispatch_semaphore_signal(processingLock);
                }
            } else {
                // we do, handle the response
                [self.provider resumeTransactionComplete:^(EFTPaymentCompleteReason reason, EFTProviderErrorCode resultCode, NSError *error) {
                    if (kEFTProviderErrorCodeSuccess == resultCode) {
                        self.currentPayment.paymentCompleteReason = reason;
                    } else {
                        self.currentPayment.paymentCompleteReason = kEFTPaymentCompleteReasonError;
                        [[NSNotificationCenter defaultCenter] postNotificationName:kEFTNotificationError
                                                                            object:self
                                                                          userInfo:@{ kEFTNotificationErrorErrorKey : [self.provider getLastServerError] }];
                    }
                    self.currentPayment.resultCode = @(self.currentPayment.paymentCompleteReason);
                    [self continueCurrentPayment];
                }];
            }
            break;
        }
        case kEFTPaymentStatusSignatureRequired: {
            // get the eft signature capture context
            SoftKeyContext *layout = [[BasketController sharedInstance] getLayoutForContextNamed:@"OCGEFTMANAGER SIGNATURE_CAPTURE LAYOUT"];
            if (layout) {
                // start signature capture
                SignatureCaptureViewController *signatureVC = [[SignatureCaptureViewController alloc] init];
                signatureVC.signatureContext = layout;
                signatureVC.modalPresentationStyle = UIModalPresentationFormSheet;
                signatureVC.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
                [ServerErrorHandler topMostControllerComplete:^(UIViewController *viewController) {
                    [viewController presentViewController:signatureVC
                                                 animated:YES
                                               completion:nil];
                }];
            } else {
                NSError *error = [NSError errorWithDomain:kEFTProviderErrorDomain
                                                     code:kEFTProviderErrorCodeApplicationError
                                                 userInfo:nil];
                [self endPaymentForErrorCode:kEFTProviderErrorCodeApplicationError
                                   withError:error
                               suppressError:NO];
            }
            break;
        }
        case kEFTPaymentStatusFinishing: {
            if ([self.provider respondsToSelector:@selector(showRemoveCardAlert)]) {
                if ([self.provider showRemoveCardAlert]) {
                    UIAlertController *alert =
                    [UIAlertController alertControllerWithTitle:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁬󠁪󠁖󠁑󠁷󠁇󠁌󠁢󠁆󠁯󠁊󠁷󠁲󠁎󠁬󠁪󠁈󠁨󠁄󠁉󠁉󠁊󠁅󠁁󠁁󠁳󠁳󠁿*/ @"Remove card", nil)
                                                        message:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁍󠁌󠁉󠁙󠁳󠁢󠁵󠁨󠁘󠀯󠁎󠁘󠀹󠁂󠁇󠁆󠁷󠁐󠁏󠁷󠁓󠁲󠁦󠁉󠁗󠁃󠁍󠁿*/ @"Please remove card", nil)
                                                 preferredStyle:UIAlertControllerStyleAlert];
                    [alert addAction:[UIAlertAction actionWithTitle:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁄󠁴󠁍󠁉󠁣󠀫󠁺󠁔󠁗󠁦󠁹󠁡󠁯󠀷󠁨󠁔󠁩󠀸󠁯󠀫󠁨󠁘󠀹󠁱󠁌󠀶󠀸󠁿*/ @"OK", nil)
                                                              style:UIAlertActionStyleDefault
                                                            handler:^(UIAlertAction *action) {
                                                                [self sendPaymentToServer];
                                                            }]];
                    [ServerErrorHandler topMostControllerComplete:^(UIViewController *viewController) {
                        [viewController presentViewController:alert
                                                     animated:YES
                                                   completion:nil];
                    }];
                } else {
                    [self sendPaymentToServer];
                }
            } else {
                [self sendPaymentToServer];
            }
            break;
        }
        case kEFTPaymentStatusComplete: {
            [self continueCurrentPayment];
            break;
        }
        default: {
            // ???, end the payment
            ErrorLog(@"Unknown payment state: %i", self.currentPayment.paymentStatus);
            [self endCurrentPayment];
            break;
        }
    }
}

/** @brief Moves the current payment on to the next payment step.
 */
- (void) continueCurrentPayment
{
    switch (self.currentPayment.paymentStatus) {
        case kEFTPaymentStatusInitializing: {
            self.currentPayment.paymentStatus = kEFTPaymentStatusProcessing;
            if ([self saveCurrentPayment]) {
                [self executePaymentStep];
            }
            break;
        }
        case kEFTPaymentStatusProcessing: {
            if (kEFTPaymentCompleteReasonNotComplete == self.currentPayment.paymentCompleteReason) {
                [self executePaymentStep];
            } else {
                self.currentPayment.paymentStatus = kEFTPaymentStatusFinishing;
                if ([self saveCurrentPayment]) {
                    [self executePaymentStep];
                }
            }
            break;
        }
        case kEFTPaymentStatusSignatureRequired: {
            self.currentPayment.paymentStatus = kEFTPaymentStatusProcessing;
            if ([self saveCurrentPayment]) {
                [self executePaymentStep];
            }
            break;
        }
        case kEFTPaymentStatusFinishing: {
            // move to complete
            self.currentPayment.paymentStatus = kEFTPaymentStatusComplete;
            if ([self saveCurrentPayment]) {
                [self executePaymentStep];
            }
            break;
        }
        case kEFTPaymentStatusComplete: {
            // end the payment
            if (kEFTPaymentCompleteReasonApproved == self.currentPayment.paymentCompleteReason) {
                [self endCurrentPayment];
            }
            break;
        }
        default: {
            // ???, end the payment
            ErrorLog(@"Unknown payment state: %i", self.currentPayment.paymentStatus);
            [self endCurrentPayment];
            break;
        }
    }
    
    // post a notification
    [[NSNotificationCenter defaultCenter] postNotificationName:kEFTNotificationTransactionChanged
                                                        object:self.currentPayment];
}

/** @brief Sends the current payment to the server.
 */
- (void) sendPaymentToServer
{
    if (![self hasPaymentBeenAdded]) {
        DebugLog(@"Sending payment to server");
        
        // TFS75449 optionally clear out the signature
        Device *eftDevice = [[BasketController sharedInstance] deviceForSettingWithName:kOCGEFTManagerConfiguredPaymentProviderKey];
        NSString *sanitiseSignature = eftDevice.propertyDict[@"SendSignatureToServer"];
        if ([sanitiseSignature.lowercaseString isEqualToString:@"false"]) {
            DebugLog(@"Clearing out signature");
            self.currentPayment.signature = nil;
        }
        
        switch ([self getConfiguredProvider]) {
            case OCGEFTManagerProviderPayware: {
                VerifonePayment *payment = (VerifonePayment *)[self.provider updatePaymentFromTransaction];
                [[BasketController sharedInstance] addVerifonePaymentTender:payment];
                break;
            }
            case OCGEFTManagerProviderYesPay: {
                YespayPayment *payment = (YespayPayment *)[self.provider updatePaymentFromTransaction];
                [[BasketController sharedInstance] addYespayPaymentTender:payment];
                break;
            }
#if DEBUG
            case OCGEFTManagerProviderFake: {
                VerifonePayment *payment = (VerifonePayment *)[self.provider updatePaymentFromTransaction];
                [[BasketController sharedInstance] addVerifonePaymentTender:payment];
                break;
            }
            case OCGEFTManagerProviderFakeIntegrated:
#endif
            default: {
                if ([self.currentPayment.basketId isEqualToString:BasketController.sharedInstance.basket.basketID]) {
                    [[BasketController sharedInstance] addEFTPaymentTender:self.currentPayment
                                                                  complete:nil];
                } else {
                    ErrorLog(@"Could not add payment as basket ids do not match!\n%@", self.currentPayment);
                    UIAlertView *alert =
                    [[UIAlertView alloc] initWithTitle:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁫󠀵󠁉󠁤󠁘󠁖󠁏󠁁󠁆󠀷󠁬󠀯󠁯󠁇󠀲󠀳󠁵󠁢󠀫󠁘󠁦󠀯󠁩󠁁󠁦󠁒󠁑󠁿*/ @"Error", nil)
                                               message:[NSString stringWithFormat:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁔󠁶󠁦󠁷󠁭󠁺󠁐󠀹󠁶󠁱󠁥󠁦󠁒󠀱󠁘󠁳󠁄󠁥󠁤󠀱󠀳󠁏󠁑󠁹󠁎󠁉󠀰󠁿*/ @"Could not add payment to sales basket. Customer may have been charged in error. Payment reference %@", nil), self.currentPayment.uniqueMerchantReference]
                                              delegate:nil
                                     cancelButtonTitle:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁄󠁴󠁍󠁉󠁣󠀫󠁺󠁔󠁗󠁦󠁹󠁡󠁯󠀷󠁨󠁔󠁩󠀸󠁯󠀫󠁨󠁘󠀹󠁱󠁌󠀶󠀸󠁿*/ @"OK", nil)
                                     otherButtonTitles:nil];
                     [alert show];
                     [self endCurrentPayment];
                }
                break;
            }
        }
    } else {
        // payment already added, continue
        [self continueCurrentPayment];
    }
}

/** @brief Posts the error notification and ends the payment process.
 *  @param errorCode     The error code that caused the payment to stop.
 *  @param error         The provider specific error.
 *  @param suppressError Whether to supress sending the error notification or not.
 */
- (void) endPaymentForErrorCode:(EFTProviderErrorCode)errorCode
                      withError:(NSError *)error
                  suppressError:(BOOL)suppressError
{
    ErrorLog(@"Payment failed due to %i\n%@", errorCode, [error localizedDescription]);
    [self endCurrentPayment];
    if (!suppressError) {
        [[NSNotificationCenter defaultCenter] postNotificationName:kEFTNotificationError
                                                            object:self
                                                          userInfo:@{ kEFTNotificationErrorErrorKey : [self.provider getLastServerError] }];
    }
}

- (void) handlePaymentAddedNotification:(NSNotification *)note
{
    if (self.processing && self.currentPayment.transactionType != kEFTPaymentTransactionTypeVoid) {
        // payment processed, continue
        dispatch_semaphore_signal(processingLock);
        [self continueCurrentPayment];
    }
    if (BasketController.sharedInstance.isTendered) {
        // reset tender id
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"OCGEFTManagerCurrentPaymentTenderId"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
}

- (void) handleActiveBasketChange:(NSNotification *)note
{
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        DebugLog(@"Checking for existing EFT payment");
        // check if we don't already have a payment on the go
        if (self.processing) {
            DebugLog(@"Found existing EFT payment");
            // we do, continue the payment process from where we were
            [self resumeCurrentPayment];
        } else {
            DebugLog(@"No EFT payment in progress");
        }
    });
}

- (void) handleGeneralError:(NSNotification *)note
{
    if (self.processing) {
        NSError *error = note.userInfo[kBasketControllerGeneralErrorKey];
        ErrorLog(@"Error processing EFT REST request: %@", error);
        dispatch_semaphore_signal(processingLock);
//        if (kEFTPaymentStatusFinishing == self.currentPayment.paymentStatus) {
//            [self executePaymentStep];
//        }
    }
}

#pragma mark - Properties

@synthesize configured;
- (BOOL) configured
{
    return self.provider != nil;
}

@synthesize provider = _provider;
- (id<OCGEFTProviderProtocol>) provider
{
    if (_provider == nil) {
        // initialise the correct provider, based on the configured
        // payment device
        switch ([self getConfiguredProvider]) {
            case OCGEFTManagerProviderAdyen:
                _provider = [[OCGEFTProviderAdyen alloc] initForManager:self];
                break;
            case OCGEFTManagerProviderPayware:
                _provider = [[OCGEFTProviderPayware alloc] initForManager:self];
                break;
            case OCGEFTManagerProviderYesPay:
                _provider = [[OCGEFTProviderYespay alloc] initForManager:self];
                break;
#if DEBUG
            case OCGEFTManagerProviderFake:
                _provider = [[OCGEFTProviderFake alloc] initForManager:self];
                break;
            case OCGEFTManagerProviderFakeIntegrated:
                _provider = [[OCGEFTProviderFakeIntegrated alloc] initForManager:self];
                break;
#endif
            default:
                break;
        }
    }
    
    return _provider;
}

@synthesize currentPayment = _currentPayment;
- (EFTPayment *) currentPayment
{
    EFTPayment *result = nil;
    
    if (_currentPayment == nil) {
        // load the payment from disk
        DebugLog(@"Checking for current payment...");
        _currentPayment = [NSKeyedUnarchiver unarchiveObjectWithFile:self.currentPaymentPath];
        if (_currentPayment) {
            DebugLog(@"Found stored payment at %@", self.currentPaymentPath);
        } else {
            DebugLog(@"Could not load stored payment at %@", self.currentPaymentPath);
        }
    }
    
    result = _currentPayment;
    
    return result;
}

@synthesize processing;
- (BOOL) processing
{
    return self.currentPayment != nil;
}

@synthesize supportsVoids;
- (BOOL) supportsVoids
{
    return [self.provider voidsAllowed];
}

#pragma mark - Init

static char SHARED_INSTANCE_IDENTIFER;

+ (void) setSharedInstance:(OCGEFTManager *) sharedInstance
{
    objc_setAssociatedObject(self, &SHARED_INSTANCE_IDENTIFER, sharedInstance, OBJC_ASSOCIATION_RETAIN);
}

+ (OCGEFTManager *) sharedInstance
{
    __block id sharedInstance = objc_getAssociatedObject(self, &SHARED_INSTANCE_IDENTIFER);
    if (sharedInstance != nil) {
        return sharedInstance;
    }
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[self alloc] init];
        self.sharedInstance = sharedInstance;
    });
    
    return sharedInstance;
}

- (instancetype) init
{
    self = [super init];
    if (self) {
        processingLock = dispatch_semaphore_create(1);
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(handlePaymentAddedNotification:)
                                                     name:kBasketControllerPaymentChangeNotification
                                                   object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(handleActiveBasketChange:)
                                                     name:kBasketControllerBasketChangeNotification
                                                   object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(handleGeneralError:)
                                                     name:kBasketControllerGeneralErrorNotification
                                                   object:nil];
    }
    
    return self;
}

- (void) dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - OCGURLProviderProtocol

- (BOOL) application:(UIApplication *)application
             openURL:(NSURL *)url
   sourceApplication:(NSString *)sourceApplication
          annotation:(id)annotation
{
    BOOL result = NO;
    
    if (self.provider.isURLProvider) {
        id<OCGURLProviderProtocol> URLprovider = (id<OCGURLProviderProtocol>)self.provider;
        if ([URLprovider respondsToSelector:@selector(isProviderURL:)]) {
            result = [URLprovider isProviderURL:url];
            if (result) {
                if (self.processing) {
                    // save the URL
                    self.currentPayment.transaction = [url absoluteString];
                    if (![self saveCurrentPayment]) {
                        ErrorLog(@"Could not save payment URL:\n%@", url);
                        [self endCurrentPayment];
                    }
                }
            }
        }
    }
    
    return result;
}

#pragma mark - Methods

- (NSArray *) supportedPaymentProviders
{
    return @[@(OCGEFTManagerProviderAdyen),
             @(OCGEFTManagerProviderPayware),
             @(OCGEFTManagerProviderYesPay),
#if DEBUG
             @(OCGEFTManagerProviderFake),
             @(OCGEFTManagerProviderFakeIntegrated),
#endif
             ];
}

- (OCGEFTManagerProvider) getConfiguredProvider
{
    OCGEFTManagerProvider result = OCGEFTManagerProviderNone;
    
    if ([[OCGSettingsController sharedInstance] enabledCheckForSettingWithName:kOCGEFTManagerConfiguredPaymentProviderKey]) {
        NSNumber *providerNumber = [[OCGSettingsController sharedInstance] valueForSettingWithName:kOCGEFTManagerConfiguredPaymentProviderKey];
        if (providerNumber) {
            result = providerNumber.integerValue;
        }
    }
    
    return result;
}

- (void) setConfiguredProvider:(OCGEFTManagerProvider)provider
{
    [[OCGSettingsController sharedInstance] setValue:@(provider)
                                  forSettingWithName:kOCGEFTManagerConfiguredPaymentProviderKey];
    [self resetProvider];
}

- (NSString *) nameForProvider:(OCGEFTManagerProvider)provider
{
    NSString *result = nil;
    
    switch (provider) {
        case OCGEFTManagerProviderNone:
            result = @"None";
            break;
        case OCGEFTManagerProviderPayware:
            result = @"PAYWare";
            break;
        case OCGEFTManagerProviderYesPay:
            result = @"YESPay";
            break;
        case OCGEFTManagerProviderAdyen:
            result = @"Adyen";
            break;
#if DEBUG
        case OCGEFTManagerProviderFake:
            result = @"Fake";
            break;
        case OCGEFTManagerProviderFakeIntegrated:
            result = @"Fake (integrated)";
            break;
#endif
        default:
            result = @"Unknown";
            break;
    }
    
    return result;
}

- (OCGEFTManagerProvider) providerForName:(NSString *)name
{
    OCGEFTManagerProvider result = OCGEFTManagerProviderNone;
    
    if ([name isEqualToString:@"PAYWare"]) {
        result = OCGEFTManagerProviderPayware;
    } else if ([name isEqualToString:@"YESPay"]) {
        result = OCGEFTManagerProviderYesPay;
    } else if ([name isEqualToString:@"Adyen"]) {
        result = OCGEFTManagerProviderAdyen;
#if DEBUG
    } else if ([name isEqualToString:@"Fake"]) {
        result = OCGEFTManagerProviderFake;
    } else if ([name isEqualToString:@"Fake (integrated)"]) {
        result = OCGEFTManagerProviderFakeIntegrated;
#endif
    }
    
    return result;
}

- (NSString *) localizedNameForProvider:(OCGEFTManagerProvider)provider
{
    NSString *result = nil;
    
    switch (provider) {
        case OCGEFTManagerProviderNone:
            result = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠀰󠁐󠁍󠁚󠁇󠁡󠁇󠁡󠁋󠁨󠁔󠁯󠀲󠁶󠁱󠁍󠀲󠁯󠀳󠁙󠁍󠁷󠁪󠀫󠁍󠁪󠁑󠁿*/ @"None", nil);
            break;
        case OCGEFTManagerProviderPayware:
            result = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁅󠁂󠁚󠀷󠁰󠀱󠁤󠁂󠀰󠁌󠁳󠀴󠁑󠁄󠁮󠀲󠁆󠁃󠁎󠀷󠁅󠁯󠀫󠀹󠁩󠀯󠁳󠁿*/ @"PAYWare", nil);
            break;
        case OCGEFTManagerProviderYesPay:
            result = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁐󠁭󠁥󠁢󠁆󠁙󠁃󠀵󠁥󠁌󠁁󠁭󠁁󠁃󠁶󠁉󠁔󠀷󠁍󠀲󠁒󠁷󠁶󠁲󠁄󠁪󠁯󠁿*/ @"YESPay", nil);
            break;
        case OCGEFTManagerProviderAdyen:
            result = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁳󠀰󠁏󠁬󠁌󠁘󠁡󠁫󠁇󠁗󠁉󠁳󠁤󠁅󠁦󠁤󠁨󠁙󠁇󠁂󠀰󠁓󠀱󠁌󠁇󠁃󠁣󠁿*/ @"Adyen", nil);
            break;
#if DEBUG
        case OCGEFTManagerProviderFake:
            result = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠀹󠁉󠀯󠁤󠀰󠁖󠁰󠁱󠁙󠀴󠁢󠁣󠁦󠁏󠁅󠀶󠁉󠀶󠁣󠁎󠁭󠁈󠁘󠁵󠁘󠁑󠁯󠁿*/ @"Fake", nil);
            break;
        case OCGEFTManagerProviderFakeIntegrated:
            result = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁨󠁖󠁇󠁩󠁵󠁚󠁧󠁫󠁦󠁖󠁓󠁖󠀲󠁹󠁎󠁆󠁏󠁕󠁇󠀰󠁬󠁵󠁆󠁌󠁆󠀳󠁷󠁿*/ @"Fake (integrated)", nil);
            break;
#endif
        default:
            result = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁣󠀲󠁐󠁏󠀸󠁄󠁫󠁙󠁶󠁺󠁲󠁷󠁧󠁣󠁊󠁭󠀲󠁍󠁊󠁩󠁧󠁑󠁣󠀴󠁔󠀴󠁁󠁿*/ @"Unknown", nil);
            break;
    }
    
    return result;
}

- (void) resetProvider
{
    // make sure we get the new provider
    _provider = nil;
    // perform the preflight check
    [self.provider preflightCheckForTransaction:NO
                                       complete:^(EFTProviderErrorCode resultCode, NSError *error, BOOL suppressError) {
        if (kEFTProviderErrorCodeSuccess != resultCode) {
            UIAlertView *alert =
            [[UIAlertView alloc] initWithTitle:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁫󠀵󠁉󠁤󠁘󠁖󠁏󠁁󠁆󠀷󠁬󠀯󠁯󠁇󠀲󠀳󠁵󠁢󠀫󠁘󠁦󠀯󠁩󠁁󠁦󠁒󠁑󠁿*/ @"Error", nil)
                                       message:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁘󠁥󠀸󠁢󠁕󠁬󠁒󠁤󠁐󠀳󠁫󠁏󠁲󠁃󠁰󠁎󠀷󠁧󠁓󠁆󠁥󠁃󠁄󠁪󠁋󠁏󠁣󠁿*/ @"The configured EFT provider cannot be verified as working. Please check your configuration.\nEFT payments will not be available.", nil)
                                      delegate:nil
                             cancelButtonTitle:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁄󠁴󠁍󠁉󠁣󠀫󠁺󠁔󠁗󠁦󠁹󠁡󠁯󠀷󠁨󠁔󠁩󠀸󠁯󠀫󠁨󠁘󠀹󠁱󠁌󠀶󠀸󠁿*/ @"OK", nil)
                             otherButtonTitles:nil];
            [alert show];
        }
    }];
}

- (BOOL) deviceSelectionAllowed
{
    BOOL result = NO;
    
    Device *EFTDevice = [[BasketController sharedInstance] deviceForSettingWithName:kOCGEFTManagerConfiguredPaymentProviderKey];
    if (EFTDevice) {
        result = ((DeviceSelectionModeNone != EFTDevice.selectionMode) || EFTDevice.scanToIdentify);
    }
    
    return result;
}

- (BOOL) providerRequiresAdditionalSetupWithDescription:(NSString **)setupDescription
                                               andValue:(NSString **)setupValue
{
    BOOL result = NO;
    
    if ([self deviceSelectionAllowed]) {
        result = [self.provider respondsToSelector:@selector(additionalSetupComplete:)];
        if (setupDescription) {
            *setupDescription = @"Additional Setup";
            if ([self.provider respondsToSelector:@selector(additionalSetupDescription)]) {
                *setupDescription = [self.provider additionalSetupDescription];
            }
        }
        if (setupValue) {
            *setupValue = nil;
            if ([self.provider respondsToSelector:@selector(additionalSetupValue)]) {
                *setupValue = [self.provider additionalSetupValue];
            }
        }
    }
    
    return result;
}

- (void) additionalProviderSetupComplete:(dispatch_block_t)complete
{
    [self.provider additionalSetupComplete:complete];
}

- (NSString *) additionalProviderStatus
{
    NSString *result = nil;
    
    if (self.provider) {
        if ([self.provider respondsToSelector:@selector(localizedProviderStatus)]) {
            result = [self.provider localizedProviderStatus];
        }
    }
    
    return result;
}

- (void) EFTPaymentForTransactionType:(EFTPaymentTransactionType)transactionType
                           withAmount:(NSNumber *)amount
                           inCurrency:(NSString *)currency
                            forTender:(Tender *)tender
                            forBasket:(BasketAssociateDTO *)basket
{
    DebugLog(@"Checking for existing EFT payment");
    // check if we don't already have a payment on the go
    if (!self.processing) {
        // we don't, start a new transaction
        if (0 == dispatch_semaphore_wait(processingLock, DISPATCH_TIME_NOW)) {
            DebugLog(@"Starting new EFT transaction for %i - %@", transactionType, amount);
            _currentPayment = [[OCGEFTManagerCurrentPayment alloc] init];
            _currentPayment.paymentStatus = kEFTPaymentStatusInitializing;
            _currentPayment.paymentCompleteReason = kEFTPaymentCompleteReasonNotComplete;
            _currentPayment.transactionType = transactionType;
            _currentPayment.customerEmail = basket.customer.email;
            _currentPayment.basketId = basket.basketID;
            _currentPayment.basket = basket;
            _currentPayment.tender = tender;
            _currentPayment.tenderAmount = [[BasketPricingDTO alloc] init];
            _currentPayment.tenderAmount.amount = [NSString stringWithFormat:@"%f", amount.doubleValue];
            _currentPayment.currencyCode = (currency == nil) ? tender.currencyCode : currency;
            if ([self saveCurrentPayment]) {
                [self executePaymentStep];
            }
            // post a notification
            [[NSNotificationCenter defaultCenter] postNotificationName:kEFTNotificationTransactionChanged
                                                                object:self.currentPayment];
        } else {
            WarningLog(@"ALREADY PROCESSING!");
        }
    } else {
        WarningLog(@"Found existing EFT payment");
        // we do, continue the payment process from where we were
        [self resumeCurrentPayment];
    }
}

- (void) endCurrentPayment
{
    WarningLog(@"Deleting current payment at %@", self.currentPaymentPath);
    
    // delete the file
    NSError *error = nil;
    BOOL result =
    [[NSFileManager defaultManager] removeItemAtPath:self.currentPaymentPath
                                               error:&error];
    if (!result) {
        ErrorLog(@"Error deleting current payment at %@:\n%@", self.currentPaymentPath, error);
    }
    _currentPayment = nil;
    
    // post a notification
    [[NSNotificationCenter defaultCenter] postNotificationName:kEFTNotificationTransactionChanged
                                                        object:self.currentPayment];
    
    dispatch_semaphore_signal(processingLock);
}

- (void) cancelTx
{
    if (self.processing) {
        if (kEFTPaymentStatusFinishing == self.currentPayment.paymentStatus) {
            UIAlertController *alert =
            [UIAlertController alertControllerWithTitle:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁢󠁳󠁅󠁧󠁱󠁫󠀶󠁡󠀴󠁗󠀸󠁊󠀸󠁨󠁲󠁈󠁰󠁓󠁥󠁋󠁑󠁂󠁯󠁖󠀹󠁣󠁅󠁿*/ @"Transaction has been processed", nil)
                                                message:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁊󠁐󠁨󠀴󠁕󠁒󠁭󠁗󠀴󠁉󠁈󠁌󠀱󠁲󠀳󠁕󠁪󠁂󠁵󠀵󠁬󠀴󠁌󠀰󠁗󠀷󠁁󠁿*/ @"Selecting Cancel will cancel adding the payment to this sale, but the customer may still be charged.\nSelecting Retry will attempt to add the payment to the basket again.", nil)
                                         preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *cancelAction =
            [UIAlertAction actionWithTitle:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁓󠁬󠁹󠀱󠀫󠁘󠁳󠁎󠁏󠁩󠁇󠁰󠀵󠁄󠁄󠁚󠁉󠁪󠁥󠁯󠁪󠀷󠁌󠁭󠁢󠁺󠁳󠁿*/ @"Retry", nil)
                                     style:UIAlertActionStyleCancel
                                   handler:^(UIAlertAction *action) {
                                       [self executePaymentStep];
                                   }];
            [alert addAction:cancelAction];
            UIAlertAction *proceedAction =
            [UIAlertAction actionWithTitle:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁩󠁢󠁺󠁬󠁏󠁶󠁣󠁧󠁫󠁍󠁋󠁧󠁫󠀱󠁶󠁐󠁐󠀹󠁘󠀴󠁘󠁡󠁕󠁭󠁹󠁷󠀰󠁿*/ @"Cancel", nil)
                                     style:UIAlertActionStyleDestructive
                                   handler:^(UIAlertAction *action) {
                                       NSString *message = [NSString stringWithFormat:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁔󠁶󠁦󠁷󠁭󠁺󠁐󠀹󠁶󠁱󠁥󠁦󠁒󠀱󠁘󠁳󠁄󠁥󠁤󠀱󠀳󠁏󠁑󠁹󠁎󠁉󠀰󠁿*/ @"Could not add payment to sales basket. Customer may have been charged in error. Payment reference %@", nil), self.currentPayment.uniqueMerchantReference];
                                       [self endCurrentPayment];
                                       UIAlertView *alert =
                                       [[UIAlertView alloc] initWithTitle:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁫󠀵󠁉󠁤󠁘󠁖󠁏󠁁󠁆󠀷󠁬󠀯󠁯󠁇󠀲󠀳󠁵󠁢󠀫󠁘󠁦󠀯󠁩󠁁󠁦󠁒󠁑󠁿*/ @"Error", nil)
                                                                  message:message
                                                                 delegate:nil
                                                        cancelButtonTitle:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁄󠁴󠁍󠁉󠁣󠀫󠁺󠁔󠁗󠁦󠁹󠁡󠁯󠀷󠁨󠁔󠁩󠀸󠁯󠀫󠁨󠁘󠀹󠁱󠁌󠀶󠀸󠁿*/ @"OK", nil)
                                                        otherButtonTitles:nil];
                                       [alert show];
                                   }];
            [alert addAction:proceedAction];
            [ServerErrorHandler topMostControllerComplete:^(UIViewController *viewController) {
                [viewController presentViewController:alert
                                             animated:YES
                                           completion:nil];
            }];
        }
        if ([self.provider respondsToSelector:@selector(cancelTx)]) {
            [self.provider cancelTx];
        }
    }
}

- (void) voidTransaction:(MPOSEFTPayment *)transaction
                complete:(EFTProviderCompleteBlock)complete
{
    DebugLog(@"Checking for existing EFT payment");
    if (!self.processing) {
        DebugLog(@"Check if the provider supports voids");
        if ([self.provider voidsAllowed]) {
            // no transaction on the go and voids supported, start the transaction
            DebugLog(@"Starting new VOID transaction for %@", transaction.authorization.referenceNumber);
            _currentPayment = [[OCGEFTManagerCurrentPayment alloc] init];
            _currentPayment.paymentStatus = kEFTPaymentStatusProcessing;
            _currentPayment.paymentCompleteReason = kEFTPaymentCompleteReasonNotComplete;
            [self.provider voidTransactionWithReference:transaction.authorization.referenceNumber
                                               complete:^(EFTProviderErrorCode resultCode, NSError *error, BOOL suppressError) {
                                                   if (kEFTProviderErrorCodeSuccess == resultCode) {
                                                       _currentPayment.paymentStatus = kEFTPaymentStatusFinishing;
                                                       _currentPayment.paymentCompleteReason = kEFTPaymentCompleteReasonVoided;
                                                       _currentPayment.resultCode = @(kEFTPaymentCompleteReasonVoided);
                                                       _currentPayment.transactionType = kEFTPaymentTransactionTypeVoid;
                                                       _currentPayment.basketId = transaction.basket.basketID;
                                                       _currentPayment.basket = transaction.basket;
                                                       _currentPayment.tenderType = transaction.tenderType;
                                                       _currentPayment.tender = BasketController.sharedInstance.tenders[transaction.tenderType];
                                                       _currentPayment.tenderAmount = transaction.tenderAmount;
                                                       _currentPayment.currencyCode = _currentPayment.tender.currencyCode;
                                                       _currentPayment.referenceNumber = transaction.authorization.referenceNumber;
                                                       [[BasketController sharedInstance] addEFTPaymentTender:self.currentPayment
                                                                                                     complete:^{
                                                                                                         [self endCurrentPayment];
                                                                                                         complete(resultCode, error, NO);
                                                                                                     }];
                                                   } else {
                                                       [self endCurrentPayment];
                                                       complete(resultCode, error, NO);
                                                   }
                                               }];
        } else {
            DebugLog(@"No void support, ending transaction");
            complete(kEFTProviderErrorCodeApplicationError, nil, NO);
        }
    } else {
        complete(kEFTProviderErrorCodeBusy, nil, NO);
    }
}

- (void) retryStoredTransaction
{
    // call the retry method if the provider implements it
    if ([self.provider respondsToSelector:@selector(retryStoredTransaction)]) {
        [self.provider retryStoredTransaction];
    }
}

- (void) confirmSignature:(UIImage *)signature
{
    self.currentPayment.signature =
    [UIImagePNGRepresentation(signature) base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
    self.currentPayment.signatureConfirmed = YES;
    [ServerErrorHandler topMostControllerComplete:^(UIViewController *viewController) {
        [viewController dismissViewControllerAnimated:YES
                                           completion:^{
                                               if (self.processing) {
                                                   if ([self.provider respondsToSelector:@selector(confirmSignature:complete:)]) {
                                                       [self.provider confirmSignature:signature
                                                                              complete:^(EFTProviderErrorCode resultCode, NSError *error, BOOL suppressError) {
                                                                                  if (kEFTProviderErrorCodeSuccess == resultCode) {
                                                                                      [self continueCurrentPayment];
                                                                                  } else {
                                                                                      // step failed, move to error state
                                                                                      [self endPaymentForErrorCode:resultCode
                                                                                                         withError:error
                                                                                                     suppressError:suppressError];
                                                                                  }
                                                                              }];
                                                   }
                                               }
                                           }];
    }];
}

- (void) unConfirmSignature:(UIImage *)signature
{
    self.currentPayment.signature =
    [UIImagePNGRepresentation(signature) base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
    self.currentPayment.signatureConfirmed = NO;
    [ServerErrorHandler topMostControllerComplete:^(UIViewController *viewController) {
        [viewController dismissViewControllerAnimated:YES
                                           completion:^{
                                               if (self.processing) {
                                                   if ([self.provider respondsToSelector:@selector(unconfirmSignature:complete:)]) {
                                                       [self.provider unconfirmSignature:signature
                                                                                complete:^(EFTProviderErrorCode resultCode, NSError *error, BOOL suppressError) {
                                                                                    if (kEFTProviderErrorCodeSuccess == resultCode) {
                                                                                        [self continueCurrentPayment];
                                                                                    } else {
                                                                                        // step failed, move to error state
                                                                                        [self endPaymentForErrorCode:resultCode
                                                                                                           withError:error
                                                                                                       suppressError:suppressError];
                                                                                    }
                                                                                }];
                                                   }
                                               }
                                           }];
    }];
}

- (BOOL) hasPaymentBeenAdded
{
    BOOL result = NO;
    
    DebugLog(@"Checking for added EFT payment...");
    // check if the payment was added
    for (BasketItem *item in BasketController.sharedInstance.basket.basketItems) {
        if ([item isKindOfClass:[BasketTender class]]) {
            BasketTender *tender = (BasketTender *)item;
#if DEBUG
            if (OCGEFTManagerProviderFakeIntegrated == [self getConfiguredProvider]) {
                result = YES;
                break;
            } else {
#endif
                if (tender.authorization) {
                    if ([tender.authorization.referenceNumber isEqualToString:self.currentPayment.uniqueMerchantReference]) {
                        // payment was added, continue
                        DebugLog(@"Found EFT payment: %@", tender.authorization.referenceNumber);
                        result = YES;
                        break;
                    }
                }
#if DEBUG
            }
#endif
        }
    }
    
    return result;
}

@end
