//
//  DiscountListViewController,h
//  mPOS
//
//  Created by Meik Schuetz on 20/08/2012.
//  Copyright (c) 2012 Clarity. All rights reserved.
//

#import <UIKit/UIKit.h>

/** @file DiscountListViewController.h */

@class MPOSBasket;
@class MPOSBasketItem;
@class DiscountListViewController;

/** @brief Protocol that the owner delegate for this view controller needs to implement.
 */
@protocol DiscountListViewControllerDelegate <NSObject>
@required

/** @brief Called whenever the user chosed to dismiss the view without recalling a basket.
 *  @param viewController A reference to the view controller to be dismissed.
 */
- (void) dismissDiscountListViewController:(DiscountListViewController *)discountListViewController;

@end

/** @brief Class that implements all methods and properties for the view controller
 *  that lists the discounts of a specified basket or basket item.
 */
@interface DiscountListViewController : UITableViewController

/** @brief A reference to the basket item that is being edited.
 */
@property (weak, nonatomic) MPOSBasketItem *basketItem;

/** @brief YES, if a Dismisss button should be shown in the navigation bar.
 */
@property (atomic) BOOL showDismissNavigationButton;

/** @brief YES, if a Add Discount button should be shown in the navigation bar.
 */
@property (atomic) BOOL showAddDiscountNavigationButton;

/** @brief A reference to the delegete that receives notifications from this class instance.
 */
@property (weak, nonatomic) id<DiscountListViewControllerDelegate> delegate;

@end
