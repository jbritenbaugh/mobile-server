//
//  StatusViewController.m
//  mPOS
//
//  Created by Antonio Strijdom on 10/03/2014.
//  Copyright (c) 2014 Clarity. All rights reserved.
//

#import "StatusViewController.h"
#import "UIImage+OCGExtensions.h"
#import "UIImage+CRSUniversal.h"
#import "CRSSkinning.h"
#import "AppDelegate.h"
#import "BasketController.h"
#import "CRSLocationController.h"
#import "LocationDataSource.h"
#import "TestModeController.h"
#import "TillSetupController.h"

@interface StatusViewController ()
@property (nonatomic, readonly) CGFloat singleBarHeight;
@property (nonatomic, readonly) CGFloat doubleBarHeight;
@property (nonatomic, readonly) CGFloat controlHeight;
@property (nonatomic, strong) LocationDataSource *locationDataSource;
@property (nonatomic, strong) UIImageView *userIcon;
@property (nonatomic, strong) NSLayoutConstraint *userIconBottomConstraint;
@property (nonatomic, strong) UILabel *userNameLabel;
@property (nonatomic, strong) NSLayoutConstraint *userNameWidthConstraint;
@property (nonatomic, strong) UIImageView *storeIcon;
@property (nonatomic, strong) UILabel *storeNameLabel;
@property (nonatomic, strong) NSLayoutConstraint *storeNameWidthConstraint;
@property (nonatomic, strong) UILabel *tillNameLabel;
@property (nonatomic, strong) NSLayoutConstraint *tillNameWidthConstraint;
@property (nonatomic, strong) UIImageView *basketIcon;
@property (nonatomic, strong) UILabel *basketNameLabel;
@property (nonatomic, strong) NSLayoutConstraint *basketNameWidthConstraint;
@property (nonatomic, strong) UIImageView *trainingModeView;
@property (nonatomic, strong) UILabel *trainingModeLabel;
@property (nonatomic, strong) UIImageView *offlineModeView;
- (void) handleSettingsViewDismissNotification:(NSNotification *)note;
- (void) handleUserLogonNotification:(NSNotification *)note;
- (void) handleUserLoggedOffNotification:(NSNotification *)note;
- (void) handleServerModeChangeNotification:(NSNotification *)note;
- (void) handleTrainingModeChangeNotification:(NSNotification *)note;
- (void) handleBasketChangeNotification:(NSNotification *)note;
- (void) updateUI;
- (void) updateStoreName;
@end

@implementation StatusViewController

#pragma mark - Private

- (void) handleSettingsViewDismissNotification:(NSNotification *)note
{
    // reload store name
    [self updateStoreName];
    [self updateUI];
}

- (void) handleUserLogonNotification:(NSNotification *)note
{
    [self updateUI];
}

- (void) handleUserLoggedOffNotification:(NSNotification *)note
{
    [self updateUI];
}

- (void) handleServerModeChangeNotification:(NSNotification *)note
{
    // reload store name
    [self updateStoreName];
    [self updateUI];
}

- (void) handleTrainingModeChangeNotification:(NSNotification *)note
{
    [self updateUI];
}

- (void) handleBasketChangeNotification:(NSNotification *)note
{
    [self updateUI];
}

- (void) updateUI
{
    // display the username
    if ([BasketController sharedInstance].needsAuthentication) {
        self.userNameLabel.text = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁋󠁐󠁢󠁵󠀯󠁤󠁬󠁐󠁣󠁇󠁊󠁷󠁏󠁳󠁮󠁖󠁷󠁙󠁭󠀸󠁇󠁭󠁓󠁩󠁊󠁐󠁕󠁿*/ @"Please log on", nil);
    } else {
        self.userNameLabel.text = BasketController.sharedInstance.credentials.displayName;
        if (self.userNameLabel.text.length == 0) {
            self.userNameLabel.text = BasketController.sharedInstance.credentials.username;
        }
//        self.userNameLabel.text = @"Ford Prefect";
    }
    CGSize userNameSize = [[CRSSkinning currentSkin] sizeForString:self.userNameLabel.text
                                                           withTag:self.userNameLabel.tag];
    self.userNameWidthConstraint.constant = userNameSize.width;
    [self.userNameLabel setNeedsUpdateConstraints];
    NSString *tillNumber = [TillSetupController getTillNumber];
    NSMutableAttributedString *attributedText = [[NSMutableAttributedString alloc] init];
    NSDictionary *attributes = @{
                                 NSFontAttributeName : [UIFont fontWithName:@"OmnicoGroup" size:self.tillNameLabel.font.pointSize],
//                                 NSForegroundColorAttributeName : [UIColor blackColor],
                                 };
    if (tillNumber != nil)
    {
        [attributedText appendAttributedString:[[NSAttributedString alloc]  initWithString:@" "]];
        [attributedText appendAttributedString:[[NSAttributedString alloc]  initWithString:@"\uF000" attributes:attributes]];
        [attributedText appendAttributedString:[[NSAttributedString alloc]  initWithString:@" "]];
        [attributedText appendAttributedString:[[NSAttributedString alloc]  initWithString:tillNumber]];
    }
    
#if DEBUG
    {
        [attributedText appendAttributedString:[[NSAttributedString alloc]  initWithString:@" "]];
        [attributedText appendAttributedString:[[NSAttributedString alloc]  initWithString:@"\uF00B" attributes:attributes]];
        [attributedText appendAttributedString:[[NSAttributedString alloc]  initWithString:@" "]];
        NSURL *serverURL = OCGDebuggerServerAddress();
        if (serverURL.host.length > 0)
        {
            NSMutableString *serverAddress = [NSMutableString string];
            [serverAddress appendString:serverURL.host];
            if (serverURL.port.integerValue != 80)
            {
                [serverAddress appendFormat:@":%@", serverURL.port];
            }
            if (![serverURL.path isEqualToString:@"/"])
            {
                [serverAddress appendString:serverURL.path];
            }
            [attributedText appendAttributedString:[[NSAttributedString alloc]  initWithString:serverAddress]];
        }
    }
#endif
    
    self.tillNameLabel.attributedText = attributedText;
    
    self.tillNameWidthConstraint.constant =
    [[CRSSkinning currentSkin] sizeForString:self.tillNameLabel.text
                                     withTag:self.tillNameLabel.tag].width;
    [self.tillNameLabel setNeedsUpdateConstraints];
    // display the training mode indicator
    self.trainingModeView.hidden = !BasketController.sharedInstance.isInTrainingMode;
    self.trainingModeLabel.hidden = self.trainingModeView.hidden;
    if (self.trainingModeView.hidden) {
        self.preferredContentSize = CGSizeMake(0, self.singleBarHeight);
        self.userIconBottomConstraint.constant = 0.0f;
    } else {
        self.preferredContentSize = CGSizeMake(0, self.doubleBarHeight);
        self.userIconBottomConstraint.constant = (self.controlHeight + 7.5f) * -1.0f;
    }
    // display the offline/online indicator
    self.offlineModeView.hidden = [TestModeController serverMode] != ServerModeOffline;
    // display basket info
    if ([BasketController sharedInstance].basket.basketID.length) {
        self.basketIcon.hidden = NO;
        self.basketNameLabel.hidden = NO;
        self.basketNameLabel.text = [BasketController sharedInstance].basket.transactionNumber;
        self.basketNameWidthConstraint.constant =
        [[CRSSkinning currentSkin] sizeForString:self.basketNameLabel.text
                                         withTag:self.basketNameLabel.tag].width;
        [self.basketNameLabel setNeedsUpdateConstraints];
    } else {
        self.basketIcon.hidden = YES;
        self.basketNameLabel.hidden = YES;
    }
}

- (void) updateStoreName
{
    // display the location
    NSString *key = [CRSLocationController getLocationKey];
    self.storeNameLabel.text = [NSString stringWithFormat:@"Location %@", key];
    self.storeNameWidthConstraint.constant =
    [[CRSSkinning currentSkin] sizeForString:self.storeNameLabel.text
                                     withTag:self.storeNameLabel.tag].width;
    [self.storeNameLabel setNeedsUpdateConstraints];
    
    __weak typeof(self) welf = self;
    // load the actual location
    [self.locationDataSource getLocationSummaryForLocationKey:key
                                               serviceBaseURL:[TestModeController getServerAddress]
                                                        block:^(CRSLocationSummary* data, NSError *error) {
                                                            if (data.name) {
                                                                welf.storeNameLabel.text = data.name;
                                                            }
                                                            welf.storeNameWidthConstraint.constant =
                                                            [[CRSSkinning currentSkin] sizeForString:welf.storeNameLabel.text
                                                                                             withTag:self.storeNameLabel.tag].width;
                                                            [welf.storeNameLabel setNeedsUpdateConstraints];
                                                        }];
}

#pragma mark - Properties

- (CGFloat) singleBarHeight
{
    CGFloat result = 0.0f;
    
    if (UIInterfaceOrientationIsPortrait([UIApplication sharedApplication].statusBarOrientation)) {
        result = MIN([UIApplication sharedApplication].statusBarFrame.size.height, 20.0f);
    } else {
        result = MIN([UIApplication sharedApplication].statusBarFrame.size.width, 20.0f);
    }
    
    if (UIUserInterfaceIdiomPhone == UI_USER_INTERFACE_IDIOM()) {
        result *= 1.5f;
    } else {
        result *= 2.0f;
    }
    
    return result;
}

- (CGFloat) doubleBarHeight
{
    return self.singleBarHeight * 1.5f;
}

- (CGFloat) controlHeight
{
    if (UIUserInterfaceIdiomPhone == UI_USER_INTERFACE_IDIOM()) {
        return self.singleBarHeight * 0.25;
    } else {
        return self.singleBarHeight * 0.50;
    }
}

#pragma mark - UIViewController

- (void) viewDidLoad
{
    [super viewDidLoad];
    
    // create the location data source
    self.locationDataSource = [[LocationDataSource alloc] init];
    
    // build the UI
    // user icon
    UIImage *image = [UIImage OCGExtensions_imageNamed:@"MODIFY_SALESPERSON"];
    self.userIcon = [[UIImageView alloc] initWithImage:image];
    self.userIcon.translatesAutoresizingMaskIntoConstraints = NO;
    [self.view addSubview:self.userIcon];
    [self.userIcon constrain:@"left = left + 5.0" to:self.view];
    [self.userIcon constrain:[NSString stringWithFormat:@"height = %f", self.controlHeight]
                          to:nil];
    self.userIconBottomConstraint = [self.userIcon constrain:@"bottom = bottom" to:self.view];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.userIcon
                                                          attribute:NSLayoutAttributeWidth
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:self.userIcon
                                                          attribute:NSLayoutAttributeHeight
                                                         multiplier:1.0f
                                                           constant:0.0f]];
    self.userIcon.contentMode = UIViewContentModeScaleAspectFit;
    // user name
    self.userNameLabel = [[UILabel alloc] init];
    self.userNameLabel.translatesAutoresizingMaskIntoConstraints = NO;
    [self.view addSubview:self.userNameLabel];
    [self.userNameLabel constrain:@"left = right + 5.0" to:self.userIcon];
    [self.userNameLabel constrain:@"height = height" to:self.userIcon];
    self.userNameWidthConstraint = [self.userNameLabel constrain:@"width = 0.0" to:nil];
    [self.userNameLabel constrain:@"bottom = bottom" to:self.userIcon];
    self.userNameLabel.lineBreakMode = NSLineBreakByClipping;
    self.userNameLabel.text = nil;
    // store icon
    image = [UIImage OCGExtensions_imageNamed:@"Store"];
    self.storeIcon = [[UIImageView alloc] initWithImage:image];
    self.storeIcon.translatesAutoresizingMaskIntoConstraints = NO;
    [self.view addSubview:self.storeIcon];
    [self.storeIcon constrain:@"left = right + 5.0" to:self.userNameLabel];
    [self.storeIcon constrain:@"height = height" to:self.userIcon];
    [self.storeIcon constrain:@"bottom = bottom" to:self.userIcon];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.storeIcon
                                                          attribute:NSLayoutAttributeWidth
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:self.storeIcon
                                                          attribute:NSLayoutAttributeHeight
                                                         multiplier:1.0f
                                                           constant:0.0f]];
    self.storeIcon.contentMode = UIViewContentModeScaleAspectFit;
    // store name
    self.storeNameLabel = [[UILabel alloc] init];
    self.storeNameLabel.translatesAutoresizingMaskIntoConstraints = NO;
    [self.view addSubview:self.storeNameLabel];
    [self.storeNameLabel constrain:@"left = right + 5.0" to:self.storeIcon];
    [self.storeNameLabel constrain:@"height = height" to:self.userIcon];
    self.storeNameWidthConstraint = [self.storeNameLabel constrain:@"width = 0.0" to:nil];
    [self.storeNameLabel constrain:@"bottom = bottom" to:self.userIcon];
    self.storeNameLabel.lineBreakMode = NSLineBreakByClipping;
    self.storeNameLabel.text = nil;
    // till name
    self.tillNameLabel = [[UILabel alloc] init];
    self.tillNameLabel.translatesAutoresizingMaskIntoConstraints = NO;
    [self.view addSubview:self.tillNameLabel];
    [self.tillNameLabel constrain:@"left = right" to:self.storeNameLabel];
    [self.tillNameLabel constrain:@"height = height" to:self.userIcon];
    self.tillNameWidthConstraint = [self.tillNameLabel constrain:@"width = 0.0" to:nil];
    [self.tillNameLabel constrain:@"bottom = bottom" to:self.userIcon];
    self.tillNameLabel.lineBreakMode = NSLineBreakByClipping;
    // basket icon
    image = [UIImage OCGExtensions_imageNamed:@"Basket"];
    self.basketIcon = [[UIImageView alloc] initWithImage:image];
    self.basketIcon.translatesAutoresizingMaskIntoConstraints = NO;
    [self.view addSubview:self.basketIcon];
    [self.basketIcon constrain:@"left = right + 5.0" to:self.tillNameLabel];
    [self.basketIcon constrain:@"height = height" to:self.userIcon];
    [self.basketIcon constrain:@"bottom = bottom" to:self.userIcon];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.basketIcon
                                                          attribute:NSLayoutAttributeWidth
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:self.basketIcon
                                                          attribute:NSLayoutAttributeHeight
                                                         multiplier:1.0f
                                                           constant:0.0f]];
    self.basketIcon.contentMode = UIViewContentModeScaleAspectFit;
    // basket name
    self.basketNameLabel = [[UILabel alloc] init];
    self.basketNameLabel.translatesAutoresizingMaskIntoConstraints = NO;
    [self.view addSubview:self.basketNameLabel];
    [self.basketNameLabel constrain:@"left = right + 5.0" to:self.basketIcon];
    [self.basketNameLabel constrain:@"height = height" to:self.userIcon];
    self.basketNameWidthConstraint = [self.basketNameLabel constrain:@"width = 0.0" to:nil];
    [self.basketNameLabel constrain:@"bottom = bottom" to:self.userIcon];
    self.basketNameLabel.lineBreakMode = NSLineBreakByClipping;
    self.basketNameLabel.text = nil;
    // setup the training mode icon
    image = [UIImage OCGExtensions_imageNamed:@"TOGGLE_TRAINING_MODE"];
    self.trainingModeView = [[UIImageView alloc] initWithImage:image];
    self.trainingModeView.translatesAutoresizingMaskIntoConstraints = NO;
    [self.view addSubview:self.trainingModeView];
    [self.trainingModeView constrain:@"right = right - 5.0" to:self.view];
    [self.trainingModeView constrain:@"top = top" to:self.userIcon];
    [self.trainingModeView constrain:@"bottom = bottom" to:self.view];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.trainingModeView
                                                          attribute:NSLayoutAttributeWidth
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:self.trainingModeView
                                                          attribute:NSLayoutAttributeHeight
                                                         multiplier:1.0f
                                                           constant:0.0f]];
    self.trainingModeView.contentMode = UIViewContentModeScaleAspectFit;
    // setup training mode label
    self.trainingModeLabel = [[UILabel alloc] init];
    self.trainingModeLabel.translatesAutoresizingMaskIntoConstraints = NO;
    [self.view addSubview:self.trainingModeLabel];
    [self.trainingModeLabel constrain:@"left = left" to:self.view];
    [self.trainingModeLabel constrain:[NSString stringWithFormat:@"height = %f", self.controlHeight + 5.0f]
                                                              to:nil];
    [self.trainingModeLabel constrain:@"right = right" to:self.view];
    [self.trainingModeLabel constrain:@"bottom = bottom" to:self.view];
    self.trainingModeLabel.textAlignment = NSTextAlignmentCenter;
    // setup the offline icon
    image = [UIImage OCGExtensions_imageNamed:@"Cloud"];
    self.offlineModeView = [[UIImageView alloc] initWithImage:image];
    self.offlineModeView.translatesAutoresizingMaskIntoConstraints = NO;
    [self.view addSubview:self.offlineModeView];
    [self.offlineModeView constrain:@"right = right - 5.0" to:self.trainingModeView];
    [self.offlineModeView constrain:@"top = top" to:self.userIcon];
    [self.offlineModeView constrain:@"bottom = bottom" to:self.view];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.offlineModeView
                                                          attribute:NSLayoutAttributeWidth
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:self.offlineModeView
                                                          attribute:NSLayoutAttributeHeight
                                                         multiplier:1.0f
                                                           constant:0.0f]];
    self.offlineModeView.contentMode = UIViewContentModeScaleAspectFit;
    
    // skin
    self.view.tag = kStatusViewTag;
    self.userIcon.tag = kStatusViewTag;
    self.userNameLabel.tag = kStatusViewTag;
    self.storeIcon.tag = kStatusViewTag;
    self.storeNameLabel.tag = kStatusViewTag;
    self.tillNameLabel.tag = kStatusViewTag;
    self.basketIcon.tag = kStatusViewTag;
    self.basketNameLabel.tag = kStatusViewTag;
    self.trainingModeView.tag = kStatusViewTag;
    self.trainingModeLabel.tag = kStatusViewTrainingTag;
    self.offlineModeView.tag = kStatusViewTag;
    if (UIUserInterfaceIdiomPad == UI_USER_INTERFACE_IDIOM()) {
        self.userNameLabel.tag = kStatusViewiPadTag;
        self.storeNameLabel.tag = kStatusViewiPadTag;
        self.tillNameLabel.tag = kStatusViewiPadTag;
        self.basketNameLabel.tag = kStatusViewiPadTag;
    }
    
    // get the store name
    [self updateStoreName];
    
	// listen for settings view dismisses
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(handleSettingsViewDismissNotification:)
                                                 name:kAppDelegateSettingsDismissedNotification
                                               object:nil];
    // listen for logon/off notifications
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(handleUserLogonNotification:)
                                                 name:kBasketControllerUserLoggedOnNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(handleUserLoggedOffNotification:)
                                                 name:kBasketControllerHasLoggedOutNotification
                                               object:nil];
    // listen for online/offline notifications
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(handleServerModeChangeNotification:)
                                                 name:kTestModeControllerServerModeDidChangeNotification
                                               object:nil];
    // listen for training mode toggle notifications
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(handleTrainingModeChangeNotification:)
                                                 name:kBasketControllerTrainingModeToggledNotification
                                               object:nil];
    // listen for basket change notifications
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(handleBasketChangeNotification:)
                                                 name:kBasketControllerBasketChangeNotification
                                               object:nil];
}

- (void) viewWillAppear:(BOOL)animated
{
    self.trainingModeLabel.text = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁨󠁐󠁥󠁚󠁣󠁣󠁉󠁃󠁧󠁌󠁳󠁋󠁬󠁁󠀶󠀲󠁏󠁃󠁘󠁘󠁉󠁖󠁎󠀴󠁘󠀯󠁣󠁿*/ @"TRAINING MODE", nil);

    [[CRSSkinning currentSkin] applyViewSkin:self];
    self.offlineModeView.image = [self.offlineModeView.image imageWithEnabled:NO];
    [self updateUI];
//    self.view.backgroundColor = [UIColor redColor];
    [super viewWillAppear:animated];
}

- (void) dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end
