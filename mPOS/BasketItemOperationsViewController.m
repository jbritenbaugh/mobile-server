//
//  BasketItemOperationsViewController.m
//  mPOS
//
//  Created by Meik Schuetz on 15/08/2012.
//  Copyright (c) 2012 Clarity. All rights reserved.
//
#import <QuartzCore/QuartzCore.h>
#import "BadgeTableViewCell.h"
#import "BasketItemOperationsViewController.h"
#import "BasketItemQuantityViewController.h"
#import "BasketDiscountViewController.h"
#import "DiscountListViewController.h"
#import "BasketItemTableViewCell.h"
#import "BasketController.h"
#import "Model.h"
#import "CRSSkinning.h"
#import "UINavigationItem+CRSCustomNavItemTitle.h"
#import "NSArray+OCGIndexPath.h"
#import "UIAlertView+MKBlockAdditions.h"
#import "BasketViewController.h"
#import "CustomerViewController.h"
#import "CustomerController.h"
#import "POSFunc.h"

#define kBasketItemContext                          @"kBasketItemContext"
#define kBasketItemOperationChangeQuantity          NSStringForPOSFunc(POSFuncModifyQuantity)
#define kBasketItemOperationPriceOverride           NSStringForPOSFunc(POSFuncModifyPrice)
#define kBasketItemOperationAddDiscount             NSStringForPOSFunc(POSFuncAddDiscount)
#define kBasketItemOperationRemoveDiscount          NSStringForPOSFunc(POSFuncRemoveDiscount)
#define kBasketItemOperationRemoveBasketItem        NSStringForPOSFunc(POSFuncRemoveItem)
#define kBasketItemOperationLookupCustomerDetails   NSStringForPOSFunc(POSFuncLookupCustomer)
#define kBasketItemOperationModifyAssociate         NSStringForPOSFunc(POSFuncModifySalesperson)

@interface BasketItemOperationsViewController () <CustomerViewControllerDelegate, UITextFieldDelegate, UIAlertViewDelegate>
@property (strong, nonatomic) NSArray *layout;
@property (strong, nonatomic) NSDictionary *captions;

@property (strong, nonatomic) BadgeTableViewCell *basketItemRemoveDiscountTableViewCell;
@property (strong, nonatomic) BadgeTableViewCell *basketRemoveDiscountTableViewCell;

/** @brief YES, if the POSFuncItemDetails is set.
 */
@property (atomic, assign) BOOL showItemDetails;

/** @brief Determines if an option is valid for the current
 *  item state.
 *  @example If the item has a discount assigned, change quantity is not valid.
 *  @param indexPath The indexpath of the item that we are querying.
 *  @param reason The reason this option is not valid will be placed in this string.
 *  @return YES if the option is valid, otherwise NO.
 */
- (BOOL) isOptionAtIndexPathValid:(NSIndexPath *)indexPath
                          because:(NSString **)reason;

/** @brief Calculates the number of discounts on the basket item.
 *  @return The number of discounts.
 *  @note This number specifically excludes price overrides.
 *  @see MPOS-391
 */
- (NSInteger) discountCount;

@end

@implementation BasketItemOperationsViewController

#pragma mark - Private

- (BOOL) isOptionAtIndexPathValid:(NSIndexPath *)indexPath
                          because:(NSString **)reason
{
    BOOL result = YES;
    
    // MPOS-438, 316 - if there's a sale discount, don't allow the user to do anything
    
    BOOL validBasketRewards = NO;
    
    for (id reward in self.basketItem.basket.rewards)
    {
        if (![reward isKindOfClass:[MPOSBasketRewardPromotionAmount class]])
        {
            validBasketRewards = YES;
            break;
        }
    }
    
    /*
    if (validBasketRewards) {
            result = NO;
            if (reason != nil) {
                *reason = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁳󠁚󠁴󠁶󠁥󠁌󠁷󠁢󠁓󠁏󠀱󠁗󠀷󠁰󠁶󠁚󠁶󠁺󠁤󠁔󠁈󠁺󠁺󠁄󠁢󠁸󠀸󠁿*/ @"Cannot change item with sale discount applied", nil);
            }
    }
    */
     
    if (result) {
        // get the operation for the indexpath specified
        id operationIdentifier = [self.layout objectForIndexPath:indexPath];
        
        if ([operationIdentifier isEqual:kBasketItemOperationRemoveDiscount]) {
            // check the discount quantity
            if ([self discountCount] == 0) {
                result = NO;
                if (reason != nil) {
#warning Reenable this when removing discounts works
                    //*reason = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁐󠁰󠀳󠁴󠁨󠁑󠀵󠁖󠁰󠁱󠁳󠀹󠁣󠀴󠁶󠁷󠁨󠁣󠁘󠁮󠁆󠁏󠀱󠁰󠁏󠁨󠁁󠁿*/ @"No discounts to remove", nil);
                }
            }
        }
    }
    
    return result;
}

- (NSInteger) discountCount
{
    NSInteger result = 0;
    
    if (self.basketItem != nil) {
        if (self.basketItem.rewards != nil) {
            for (MPOSBasketReward *reward in self.basketItem.rewards) {
                // specifically exclude price adjustments
                if (![reward isKindOfClass:[MPOSBasketRewardLinePriceAdjust class]]) {
                    result++;
                }
            }
        }
    }
    
    return result;
}

#pragma mark Object Lifecycle
@synthesize basketItem = _basketItem;

/** @brief The designated initializer.
 *  @param nibNameOrNil The name of the nib file to associate with the view controller.
 *  @param nubBundleOrNil The bundle in which to search for the nib file.
 *  @return A newly initialized object of this class.
 */
- (id) initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    if (self = [super initWithNibName: nibNameOrNil bundle: nibBundleOrNil]) {
        
        self.captions = @{
        kBasketItemContext : NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠀵󠁎󠁄󠁤󠁘󠁘󠁐󠁶󠁔󠁚󠁚󠁅󠀳󠁲󠁊󠁘󠀳󠁹󠁉󠀫󠀯󠀱󠁘󠁥󠁘󠁱󠁅󠁿*/ @"--context--", nil),
        kBasketItemOperationChangeQuantity : NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁶󠁙󠁍󠀳󠀳󠁄󠀲󠀶󠁥󠁏󠁂󠁐󠁪󠀸󠀸󠁔󠁔󠁈󠁤󠁴󠁬󠀸󠁬󠁌󠁊󠁉󠁍󠁿*/ @"Change quantity", nil),
        
        kBasketItemOperationPriceOverride :          NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠀶󠁭󠁅󠁨󠁫󠀳󠁋󠁃󠁚󠁌󠀸󠀱󠁨󠁫󠀫󠁵󠁷󠁷󠁕󠁐󠁄󠀳󠁕󠁹󠁧󠁬󠁙󠁿*/ @"Adjust price", nil),
        kBasketItemOperationAddDiscount    :         NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁎󠀲󠁴󠁪󠁆󠁙󠁈󠁉󠀷󠁁󠁒󠁖󠁄󠁳󠀴󠁄󠁹󠁒󠁍󠁓󠁷󠁧󠁆󠁱󠁣󠁐󠁫󠁿*/ @"Add discount", nil),
#warning Reenable this when removing discounts works
        //kBasketItemOperationRemoveDiscount :         NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁁󠀹󠁪󠁫󠁮󠁫󠀳󠁮󠁐󠁋󠀫󠀷󠁥󠁋󠁢󠁴󠁗󠁬󠁪󠁄󠁪󠁁󠁳󠀹󠁌󠀳󠁑󠁿*/ @"Remove discount", nil),
        kBasketItemOperationRemoveDiscount :         NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠀷󠁩󠁋󠁑󠁁󠁗󠁢󠁺󠁳󠁒󠁗󠁥󠁚󠁕󠁲󠀶󠁖󠀯󠁲󠀵󠁗󠁌󠁊󠁥󠁉󠀷󠁑󠁿*/ @"Discounts", nil),
        kBasketItemOperationRemoveBasketItem   :     NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁂󠁩󠁩󠀹󠁚󠀰󠁱󠁌󠁨󠀱󠁩󠁵󠁅󠀫󠀯󠁗󠀰󠁋󠁸󠁆󠁂󠁖󠁦󠁤󠁕󠁔󠁯󠁿*/ @"Remove item", nil),
        kBasketItemOperationLookupCustomerDetails   :     NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁮󠀱󠀫󠁘󠁘󠁚󠁎󠀫󠁳󠁁󠁕󠁌󠁋󠁏󠁨󠁢󠀷󠁍󠁈󠁰󠁵󠁒󠀹󠁷󠀵󠁤󠁁󠁿*/ @"Customer details", nil),
        kBasketItemOperationModifyAssociate : NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁕󠁓󠁳󠁱󠁙󠁆󠀳󠁣󠀴󠁗󠁫󠁥󠁮󠁉󠁵󠁈󠁩󠁁󠁥󠁓󠀹󠁕󠀷󠁃󠀸󠁊󠀰󠁿*/ @"Assign sales person", nil)
        };
    }
    return self;
}

/** @brief Called after the controller’s view is loaded into memory.
 */
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // set the view's title
    
    self.title = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁨󠁒󠁩󠀸󠁊󠁥󠁡󠁦󠀳󠀯󠁉󠁒󠁊󠁸󠁡󠁈󠁄󠁈󠀳󠁳󠁑󠀰󠁊󠁫󠁣󠁴󠁅󠁿*/ @"Operations", nil);
    
    // apply skin
    self.tableView.tag = kTableSkinningTag;
    [[CRSSkinning currentSkin] applyViewSkin: self];
}

-(void)setLayout:(NSArray *)layout
{
    _layout = layout;
    if (layout != nil)
    {
        [self.tableView reloadData];
    }
}

/** @brief Notifies the view controller that its view was added to a view hierarchy.
 *  @param animated If YES, the view was added to the window using an animation.
 */
- (void)viewWillAppear:(BOOL)animated
{
    // call the super class
    
    [super viewWillAppear: animated];
    
    SoftKeyContextType softKeyContextType = SoftKeyContextTypeUnknown;
    if ([self.basketItem.entity.name isEqualToString:@"ReturnItem"])
    {
        softKeyContextType = SoftKeyContextTypeReturnItem;
    }
    else if ([self.basketItem.entity.name isEqualToString:@"BasketCustomerCard"])
    {
        softKeyContextType = SoftKeyContextTypeCustomerCardItem;
    }
    else if ([self.basketItem.entity.name isEqualToString:@"SaleItem"])
    {
        softKeyContextType = SoftKeyContextTypeSaleItem;
    }
    
    NSMutableArray *rowIdentifiers = [NSMutableArray array];
    [rowIdentifiers addObject:kBasketItemContext];
    MPOSSoftKeyContext *softKeyContext = [[BasketController sharedInstance] getLayoutForContextOfType:softKeyContextType];
    if (softKeyContext.rootMenu != nil)
    {
        NSArray *keyIds = [softKeyContext.rootMenu componentsSeparatedByString:@" "];
        NSMutableArray *softKeys = [NSMutableArray array];
        for (NSString *keyId in keyIds)
        {
            BOOL isSoftKeyUndefined = YES;
            MPOSSoftKey *softKeyForKeyId = nil;
            for (MPOSSoftKey *softKey in softKeyContext.softKeys)
            {
                if ([softKey.keyId isEqualToString:keyId])
                {
                    if (POSFuncForString(softKey.posFunc) == POSFuncItemDetails)
                    {
                        self.showItemDetails = YES;
                    }
                    else
                    {
                        [rowIdentifiers addObject:softKey.posFunc];
                    }
                    break;
                }
            }
        }
    }
    else
    {
        for (MPOSSoftKey *softKey in softKeyContext.softKeys)
        {
            if (POSFuncForString(softKey.posFunc) == POSFuncItemDetails)
            {
                self.showItemDetails = YES;
            }
            else
            {
                [rowIdentifiers addObject:softKey.posFunc];
            }
        }
    }

    self.layout = @[rowIdentifiers];
    DebugLog(@"layout: %@", self.layout);
    
    
    // show the navigation bar
    
    [self.navigationController setNavigationBarHidden: NO
                                             animated: YES];
    
    // subscribe to discount chnage notifications
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(handleBasketControllerDiscountChangeNotification:)
                                                 name:kBasketControllerDiscountChangeNotification
                                               object:nil];
    
    // subscribe to basket chnage notifications
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(handleBasketControllerBasketItemChangeNotification:)
                                                 name:kBasketControllerBasketItemChangeNotification
                                               object:nil];

    [self updateView];
}

/** @brief Notifies the view controller that its view was removed from a view hierarchy.
 *  @param animated If YES, the disappearance of the view was animated.
 */
-(void)viewDidDisappear: (BOOL)animated
{
    // unsubscribe from CoreData changes
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

/** @brief Called when the controller’s view is released from memory.
 */
- (void)viewDidUnload
{
    [super viewDidUnload];
    
    self.tableView.dataSource = nil;
    self.tableView.delegate = nil;
    
    self.basketItem = nil;
    self.layout = nil;
    self.captions = nil;
    
    self.basketItemRemoveDiscountTableViewCell = nil;
    self.basketRemoveDiscountTableViewCell = nil;
}

#pragma mark Private Methods

/** @brief Creates and initializes a new instance of a badge table view cell.
 *  @param caption The caption that should be displayed in the table cell.
 *  @return A reference to the created badge table view cell.
 */
- (UITableViewCell *) setupBadgeTableViewCellWithCaption:(NSString *)caption
                                                 allowed:(BOOL)allowed
{
    UITableViewCell *cell = nil;
    if (allowed) {
        cell = [[BadgeTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                         reuseIdentifier:nil];
    } else {
        cell = [[BadgeTableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle
                                         reuseIdentifier:nil];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleBlue;
    cell.accessoryType = UITableViewCellAccessoryNone;
    cell.editingAccessoryType = UITableViewCellAccessoryNone;
    cell.textLabel.text = caption;
    
    return cell;    
}

/** @brief Called to update the dynamic table view cells
 */
-(void)updateView
{
    // make sure that we reload the row that shows the basket item's data.
    DebugLog(@"updateView - %@", (self.basketItem.managedObjectContext == nil ? @"Item is deleted" : @"Item still exists"));
    if ((self.basketItem != nil) && (self.basketItem.managedObjectContext != nil)) {
        NSArray *reloadIndexPaths = [self.layout indexPathsForObjects:@[ kBasketItemContext, kBasketItemOperationChangeQuantity,kBasketItemOperationPriceOverride, kBasketItemOperationRemoveDiscount, kBasketItemOperationModifyAssociate]];
        
        if ((reloadIndexPaths != nil) && ([reloadIndexPaths count] > 0)) {
            [self.tableView reloadRowsAtIndexPaths: reloadIndexPaths
                                  withRowAnimation: UITableViewRowAnimationNone];
        }
    } else {
        UIAlertView *alert =
        [UIAlertView alertViewWithTitle:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁫󠀵󠁉󠁤󠁘󠁖󠁏󠁁󠁆󠀷󠁬󠀯󠁯󠁇󠀲󠀳󠁵󠁢󠀫󠁘󠁦󠀯󠁩󠁁󠁦󠁒󠁑󠁿*/ @"Error", nil)
                                message:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠀳󠁌󠀷󠁷󠁓󠁇󠁁󠁯󠁧󠁈󠁍󠁓󠁢󠁵󠁯󠁺󠁹󠁲󠀰󠁥󠁃󠁃󠁁󠁰󠁎󠁳󠁉󠁿*/ @"This item has been deleted.", nil)
                      cancelButtonTitle:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁄󠁴󠁍󠁉󠁣󠀫󠁺󠁔󠁗󠁦󠁹󠁡󠁯󠀷󠁨󠁔󠁩󠀸󠁯󠀫󠁨󠁘󠀹󠁱󠁌󠀶󠀸󠁿*/ @"OK", nil)
                      otherButtonTitles:nil
                              onDismiss:^(int buttonIndex) {
                                  
                              }
                               onCancel:^{
                                   [self.navigationController popToRootViewControllerAnimated:YES];
                               }];
        [alert show];
    }
}

#pragma mark - Table view data source

/** @brief Asks the data source to return the number of sections in the table view.
 *  @param An object representing the table view requesting this information.
 *  @return The number of sections in tableView. The default value is 1.
 */
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [self.layout count];
}

/** @brief Tells the data source to return the number of rows in a given section of a table view.
 *  @tableView The table-view object requesting this information.
 *  @section An index number identifying a section in tableView.
 *  @return The number of rows in section.
 */
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [[self.layout objectAtIndex:section] count];
}

/** @brief Asks the data source for a cell to insert in a particular location of the table view.
 *  @tableView A table-view object requesting the cell.
 *  @indexPath An index path locating a row in tableView.
 *  @return An object inheriting from UITableViewCellthat the table view can use for the specified row.
 */
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *resultCell = nil;
    
    NSString *disallowedReason = nil;
    BOOL allowed = [self isOptionAtIndexPathValid:indexPath
                                          because:&disallowedReason];
    id operationIdentifier = [self.layout objectForIndexPath:indexPath];
    if ([operationIdentifier isEqual: kBasketItemContext]) {

        // create a cell that displays the basket items data in the table view.
        
        static NSString *basketItemTableViewCellIdentifier = @"BasketItemTableViewCell";
        BasketItemTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:basketItemTableViewCellIdentifier];
        if (cell == nil) {
            [tableView registerClass:[BasketItemTableViewCell class] forCellReuseIdentifier:basketItemTableViewCellIdentifier];
            cell = [tableView dequeueReusableCellWithIdentifier: basketItemTableViewCellIdentifier];
        }

        cell.backgroundColor = [UIColor colorWithRed: 0.90f green: 0.90f blue: 0.90f alpha: 1.0f];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        if (self.showItemDetails)
        {
            cell.accessoryType = UITableViewCellAccessoryDetailDisclosureButton;
        }
        [cell updateDisplayForBasketItem:self.basketItem tableStyle:tableView.style];
        resultCell = cell;
    }
    else if ([operationIdentifier isEqual: kBasketItemOperationRemoveDiscount]) {
        if (self.basketItemRemoveDiscountTableViewCell == nil) {
            self.basketItemRemoveDiscountTableViewCell =
            (BadgeTableViewCell *)[self setupBadgeTableViewCellWithCaption:[self.captions objectForKey:kBasketItemOperationRemoveDiscount ]
                                                                   allowed:allowed];
        }
        
        self.basketItemRemoveDiscountTableViewCell.badgeLabel.text = [NSString stringWithFormat: @"%i", [self discountCount]];
        resultCell = self.basketItemRemoveDiscountTableViewCell;
        if (allowed) {
            resultCell.textLabel.alpha = 1.0f;
            resultCell.textLabel.opaque = YES;
            resultCell.detailTextLabel.text = nil;
        } else {
            resultCell.textLabel.opaque = NO;
            resultCell.textLabel.alpha = 0.2f;
            resultCell.detailTextLabel.text = disallowedReason;
            resultCell.detailTextLabel.alpha = 1.0f;
            resultCell.detailTextLabel.opaque = YES;
        }
    }
    else if ([operationIdentifier isEqual: kBasketItemOperationLookupCustomerDetails]) {
        NSString *cellCaption = [self.captions objectForKey:operationIdentifier];
        UITableViewCell *cell = nil;
        if (allowed) {
            cell = [[UITableViewCell alloc] initWithStyle: UITableViewCellStyleDefault reuseIdentifier: nil];
        } else {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle
                                          reuseIdentifier:nil];
        }
        cell.selectionStyle = allowed ? UITableViewCellSelectionStyleBlue : UITableViewCellSelectionStyleNone;
        cell.editingAccessoryType = UITableViewCellAccessoryNone;
        if ([operationIdentifier isEqual:kBasketItemOperationRemoveBasketItem])
        {
            cell.accessoryType = UITableViewCellAccessoryNone;
        }
        else
        {
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        }
        cell.textLabel.text = cellCaption;
        if (allowed) {
            cell.textLabel.alpha = 1.0f;
            cell.textLabel.opaque = YES;
        } else {
            cell.textLabel.opaque = NO;
            cell.textLabel.alpha = 0.2f;
            cell.detailTextLabel.text = disallowedReason;
            cell.detailTextLabel.font = [UIFont systemFontOfSize:10.0f];
            cell.detailTextLabel.textColor = [UIColor blackColor];
            cell.detailTextLabel.alpha = 1.0f;
            cell.detailTextLabel.opaque = YES;
        }
        resultCell = cell;
    }
    else if ([operationIdentifier isEqual: kBasketItemOperationModifyAssociate]) {
        NSString *cellCaption = [self.captions objectForKey:operationIdentifier];
        UITableViewCell *cell = nil;
        if (allowed) {
            cell = [[UITableViewCell alloc] initWithStyle: UITableViewCellStyleValue1 reuseIdentifier: nil];
        } else {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle
                                          reuseIdentifier:nil];
        }
        
        cell.selectionStyle = allowed ? UITableViewCellSelectionStyleBlue : UITableViewCellSelectionStyleNone;
        cell.editingAccessoryType = UITableViewCellAccessoryNone;
        if ([operationIdentifier isEqual:kBasketItemOperationRemoveBasketItem] ||
            [operationIdentifier isEqual:kBasketItemOperationModifyAssociate])
        {
            cell.accessoryType = UITableViewCellAccessoryNone;
        }
        else
        {
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        }
        if (allowed) {
            cell.textLabel.alpha = 1.0f;
            cell.textLabel.opaque = YES;
            NSString *value = self.basketItem.salesPerson.operatorId;
            if ([self.basketItem.salesPerson.displayName length] > 0)
            {
                value = self.basketItem.salesPerson.displayName;
            }
            if ([value length] > 0)
            {
                cell.textLabel.text = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁴󠁷󠁖󠁑󠁎󠁅󠁊󠀱󠁚󠀱󠁨󠁧󠁵󠀫󠁥󠁱󠁡󠁯󠁯󠁗󠁎󠁃󠁄󠀫󠁎󠁁󠁙󠁿*/ @"Sales person", nil);
                cell.detailTextLabel.text = value;
            }
            else
            {
                cell.textLabel.text = cellCaption;
                cell.detailTextLabel.text = @"";
            }

        } else {
            cell.textLabel.opaque = NO;
            cell.textLabel.alpha = 0.2f;
            cell.detailTextLabel.text = disallowedReason;
            cell.detailTextLabel.font = [UIFont systemFontOfSize:10.0f];
            cell.detailTextLabel.textColor = [UIColor blackColor];
            cell.detailTextLabel.alpha = 1.0f;
            cell.detailTextLabel.opaque = YES;
        }
        resultCell = cell;

    }
    else {
        NSString *cellCaption = [self.captions objectForKey:operationIdentifier];
        UITableViewCell *cell = nil;
        if (allowed) {
            cell = [[UITableViewCell alloc] initWithStyle: UITableViewCellStyleDefault reuseIdentifier: nil];
        } else {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle
                                          reuseIdentifier:nil];
        }
        cell.selectionStyle = allowed ? UITableViewCellSelectionStyleBlue : UITableViewCellSelectionStyleNone;
        cell.editingAccessoryType = UITableViewCellAccessoryNone;
        if ([operationIdentifier isEqual:kBasketItemOperationRemoveBasketItem] ||
            [operationIdentifier isEqual:kBasketItemOperationModifyAssociate])
        {
            cell.accessoryType = UITableViewCellAccessoryNone;
        }
        else
        {
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        }
        cell.textLabel.text = cellCaption;
        if (allowed) {
            cell.textLabel.alpha = 1.0f;
            cell.textLabel.opaque = YES;
        } else {
            cell.textLabel.opaque = NO;
            cell.textLabel.alpha = 0.2f;
            cell.detailTextLabel.text = disallowedReason;
            cell.detailTextLabel.font = [UIFont systemFontOfSize:10.0f];
            cell.detailTextLabel.textColor = [UIColor blackColor];
            cell.detailTextLabel.alpha = 1.0f;
            cell.detailTextLabel.opaque = YES;
        }
        resultCell = cell;
    }
    
    if ([operationIdentifier isEqual: kBasketItemContext]) {
        resultCell.tag = kBasketItemHeaderCellSkinningTag;
    } else {
        resultCell.tag = kTableCellSkinningTag;
        resultCell.textLabel.tag = kTableCellTextSkinningTag;
        resultCell.detailTextLabel.tag = kTableCellValueSkinningTag;
    }
    
    return resultCell;
}

- (void) tableView:(UITableView *)tableView
   willDisplayCell:(UITableViewCell *)cell
 forRowAtIndexPath:(NSIndexPath *)indexPath
{
    [[CRSSkinning currentSkin] applyCellSkin:cell
                                    forStyle:tableView.style];
}

#pragma mark - Table view delegate

- (NSIndexPath *) tableView:(UITableView *)tableView
   willSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    BOOL allowed = [self isOptionAtIndexPathValid:indexPath
                                          because:nil];
    if (allowed) {
        return indexPath;
    } else {
        return nil;
    }
}

/** @brief Tells the delegate that the specified row is now selected.
 *  @param tableView A table-view object informing the delegate about the new row selection.
 *  @param indexPath An index path locating the new selected row in tableView.
 */
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *disallowedReason = nil;
    BOOL allowed = [self isOptionAtIndexPathValid:indexPath
                                          because:&disallowedReason];
    if (allowed) {
        id operationIdentifier = [self.layout objectForIndexPath:indexPath];
        if ([operationIdentifier isEqual: kBasketItemOperationChangeQuantity]) {
            if ([[BasketController sharedInstance] doesUserHavePermissionTo:POSPermissionModifyItemQuantity]) {
                BasketItemQuantityViewController *changeItemQuantityViewController = [[BasketItemQuantityViewController alloc] initWithNibName: @"BasketItemQuantityView" bundle: nil];
                            
                changeItemQuantityViewController.basketItem = self.basketItem;
                [self.navigationController pushViewController: changeItemQuantityViewController animated: YES];
            } else {
                [tableView deselectRowAtIndexPath:indexPath
                                         animated:NO];
                [BasketViewController presentPermissionAlert:POSPermissionModifyItemQuantity
                                                   withBlock:nil];
            }
        }
        else if ([operationIdentifier isEqual: kBasketItemOperationPriceOverride]) {
            if ([[BasketController sharedInstance] doesUserHavePermissionTo:POSPermissionModifyItemPrice]) {
                BasketDiscountViewController *discountViewController = [[BasketDiscountViewController alloc] initWithNibName: @"BasketDiscountView" bundle: nil];
                
                discountViewController.discountType = DiscountTypeBasketItemPriceOverride;
                discountViewController.basketItem = self.basketItem;
                [self.navigationController pushViewController: discountViewController animated: YES];
            } else {
                [tableView deselectRowAtIndexPath:indexPath
                                         animated:NO];
                [BasketViewController presentPermissionAlert:POSPermissionModifyItemPrice
                                                   withBlock:nil];
            }
        }
        else if ([operationIdentifier isEqual: kBasketItemOperationAddDiscount]) {
            if ([[BasketController sharedInstance] doesUserHavePermissionTo:POSPermissionModifyItemDiscount]) {
                BasketDiscountViewController *discountViewController = [[BasketDiscountViewController alloc] initWithNibName: @"BasketDiscountView" bundle: nil];
                
                discountViewController.discountType = DiscountTypeBasketItemDiscount;
                discountViewController.basketItem = self.basketItem;
                [self.navigationController pushViewController: discountViewController animated: YES];
            } else {
                [tableView deselectRowAtIndexPath:indexPath
                                         animated:NO];
                [BasketViewController presentPermissionAlert:POSPermissionModifyItemDiscount
                                                   withBlock:nil];
            }
        }
        else if ([operationIdentifier isEqual: kBasketItemOperationRemoveDiscount]) {
            if ([[BasketController sharedInstance] doesUserHavePermissionTo:POSPermissionModifyItemDiscount]) {
                // check if we've got any discount to remove
                
                if ([self discountCount] <= 0) {
                    [tableView deselectRowAtIndexPath: indexPath animated: YES];
                    return;
                }
                
                DiscountListViewController *removeItemDiscountViewController = [[DiscountListViewController alloc] initWithNibName: @"DiscountListView" bundle: nil];
                
                removeItemDiscountViewController.basket = nil;
                removeItemDiscountViewController.basketItem = self.basketItem;
                [self.navigationController pushViewController: removeItemDiscountViewController animated: YES];
            } else {
                [tableView deselectRowAtIndexPath:indexPath
                                         animated:NO];
                [BasketViewController presentPermissionAlert:POSPermissionModifyItemDiscount
                                                   withBlock:nil];
            }
        }
        else if ([operationIdentifier isEqual: kBasketItemOperationRemoveBasketItem]) {
            if ([[BasketController sharedInstance] doesUserHavePermissionTo:POSPermissionCancelItem]) {
                // queue the operation to be sent to the backend
            
                [[BasketController sharedInstance] removeBasketItem: self.basketItem];
            
                // pop back to the root view controller
            
                [self.navigationController popViewControllerAnimated: YES];
            } else {
                [tableView deselectRowAtIndexPath:indexPath
                                         animated:NO];
                [BasketViewController presentPermissionAlert:POSPermissionCancelItem
                                                   withBlock:nil];
            }
            
        }
        else if ([operationIdentifier isEqual: kBasketItemOperationLookupCustomerDetails])
        {
            MPOSBasketCustomerCard *customerCard = (MPOSBasketCustomerCard *) self.basketItem;
            
            CustomerViewController* customerSearchViewController = [[CustomerViewController alloc] init];
            customerSearchViewController.delegate = self;
            
            [self.navigationController pushViewController:customerSearchViewController animated:YES];
            
            [CustomerController.sharedInstance getCustomerDetailByCardNo:customerCard.customerCardNumber cardType:nil];
        }
        else if ([operationIdentifier isEqual:kBasketItemOperationModifyAssociate]) {
            if ([[BasketController sharedInstance] doesUserHavePermissionTo:POSPermissionModifySalesperson]) {
                UIAlertView *alert =
                [[UIAlertView alloc] initWithTitle:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁴󠁷󠁖󠁑󠁎󠁅󠁊󠀱󠁚󠀱󠁨󠁧󠁵󠀫󠁥󠁱󠁡󠁯󠁯󠁗󠁎󠁃󠁄󠀫󠁎󠁁󠁙󠁿*/ @"Sales person", nil)
                                           message:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠀵󠁶󠁂󠀴󠀴󠁸󠁇󠁴󠁴󠁗󠁙󠁐󠁒󠁆󠁳󠁐󠁥󠁎󠁑󠁡󠁇󠁊󠁷󠀫󠁸󠁆󠁯󠁿*/ @"Please enter the ID of the sales associate you with to assign", nil)
                                          delegate:self
                                 cancelButtonTitle:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁩󠁢󠁺󠁬󠁏󠁶󠁣󠁧󠁫󠁍󠁋󠁧󠁫󠀱󠁶󠁐󠁐󠀹󠁘󠀴󠁘󠁡󠁕󠁭󠁹󠁷󠀰󠁿*/ @"Cancel", nil)
                                 otherButtonTitles:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠀹󠁂󠀹󠀹󠀹󠁲󠁺󠁷󠁈󠁴󠁉󠀴󠀸󠁉󠁐󠁪󠁈󠁋󠀫󠁥󠁄󠁷󠁥󠁅󠁈󠁚󠁅󠁿*/ @"Assign", nil), nil];
                alert.alertViewStyle = UIAlertViewStylePlainTextInput;
                UITextField *alertTextField = [alert textFieldAtIndex:0];
                alertTextField.delegate = self;
                alertTextField.placeholder = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁁󠀶󠀸󠀲󠁙󠀵󠁅󠁭󠀱󠁑󠀵󠀫󠁃󠁨󠁒󠀲󠁓󠁰󠁦󠁯󠁸󠁡󠁙󠁚󠁍󠁩󠀸󠁿*/ @"Associate ID", nil);
                alertTextField.text = nil;
                // TFS59176 - User ids are numeric only
                alertTextField.keyboardType = UIKeyboardTypeNumberPad;
                alertTextField.keyboardAppearance = UIKeyboardAppearanceDefault;
                [alert show];
                [tableView deselectRowAtIndexPath:indexPath
                                         animated:NO];
            } else {
                [tableView deselectRowAtIndexPath:indexPath
                                         animated:NO];
                [BasketViewController presentPermissionAlert:POSPermissionModifySalesperson
                                                   withBlock:nil];
            }
            
        }
    }
}

- (void) tableView:(UITableView *)tableView accessoryButtonTappedForRowWithIndexPath:(NSIndexPath *)indexPath
{
    id operationIdentifier = [self.layout objectForIndexPath:indexPath];
    if ([operationIdentifier isEqual:kBasketItemContext]) {
        if ([[BasketController sharedInstance] doesUserHavePermissionTo:POSPermissionItemDetails]) {
            // load item details
            [[BasketController sharedInstance] findProductForSKU:self.basketItem.itemID
                                                       forReason:ProductSearchReasonItemDetails];
        } else {
            [BasketViewController presentPermissionAlert:POSPermissionItemDetails
                                               withBlock:nil];
        }
    }
}

#pragma mark CoreData notifications

/** @brief Handle discount changes
 *  @brief notification Reference to the notification object.
 */
- (void) handleBasketControllerDiscountChangeNotification:(NSNotification *)notification
{
    [self updateView];
}

/** @brief Handle basket item changes
 *  @brief notification Reference to the notification object.
 */
- (void) handleBasketControllerBasketItemChangeNotification:(NSNotification *)notification
{
    [self updateView];
}

#pragma mark - CustomerViewControllerDelegate

- (void) dismissCustomerSearchViewController:(CustomerViewController *)customerController
{
    [customerController.navigationController popViewControllerAnimated:YES];
}

#pragma mark - UITextFieldDelegate

- (BOOL) textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    BOOL result = YES;
    
    NSString *text = [textField.text stringByReplacingCharactersInRange:range withString:string];
    
    if ([text length] > 10)
    {
        result = NO;
    }
    
//    NSString *associateRegex = [BasketController sharedInstance].clientSettings.salesAssociateIDMask;
//    
//    
//    if ([associateRegex length] > 0) {
//        NSRange range = [string rangeOfString:associateRegex
//                                      options:NSRegularExpressionSearch];
//        result = (range.length > 0 && range.location != NSNotFound);
//    }
    
    return result;
}

#pragma mark - UIAlertViewDelegate

- (BOOL) alertViewShouldEnableFirstOtherButton:(UIAlertView *)alertView
{
    BOOL result = NO;
    
    UITextField *alertTextField = [alertView textFieldAtIndex:0];
    if (alertTextField.text.length > 0) {
        result = YES;
    }
    
    return result;
}

- (void) alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 1) {
        // send request
        
        UITextField *alertTextField = [alertView textFieldAtIndex:0];
        NSString *operatorId = alertTextField.text;
        [BasketController.sharedInstance modifyBasketItem:self.basketItem
                                              salesPerson:operatorId];
    }
}

@end
