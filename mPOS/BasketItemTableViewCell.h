//
//  BasketItemTableViewCell.h
//  mPOS
//
//  Created by Meik Schuetz on 15/08/2012.
//  Copyright (c) 2012 Clarity. All rights reserved.
//

#import <UIKit/UIKit.h>

@class MPOSBasketItem;
@class MPOSItem;

/** @file BasketItemTableViewCell.h */

@interface BasketItemTableViewCell : UITableViewCell

/** @brief Method to update the cell for a basket item.
 *  @param basketItem The basket item to display.
 */
-(void)updateDisplayForBasketItem:(MPOSBasketItem*)basketItem
                       tableStyle:(UITableViewStyle)tableStyle;

/** @brief Method to update the cell for a basket item.
 *  @param basketItem The basket item to display.
 *  @param showSelection Whether the selection background is shown.
 */
-(void)updateDisplayForBasketItem:(MPOSBasketItem*)basketItem
          showSelectionBackground:(BOOL)showSelection
                       tableStyle:(UITableViewStyle)tableStyle;

/** @brief Method to update the cell for an item
 */
-(void)updateDisplayForItem:(MPOSItem*)item
                 tableStyle:(UITableViewStyle)tableStyle;

/** @brief Method to update the cell with the rewards total
 */
-(void)updateWithRewardTotal:(NSString*)rewardTotal
                  tableStyle:(UITableViewStyle)tableStyle;

@end
