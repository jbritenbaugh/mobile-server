//
//  BarcodeInput.h
//  mPOS
//
//  Created by John Scott on 10/06/2015.
//  Copyright (c) 2015 Clarity. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "BarcodeScannerController.h"

@protocol BarcodeInput <NSObject>
@optional

- (void)insertBarcode:(NSString *)barcode entryMethod:(ScannerEntryMethod)entryMethod barcodeType:(SMBarcodeType)barcodeType;

@end

@interface UIResponder (BarcodeInput) <BarcodeInput>

@end