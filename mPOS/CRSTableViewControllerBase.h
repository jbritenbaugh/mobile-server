//
//  CRSTableViewControllerBase.h
//  mPOS
//
//  Created by Antonio Strijdom on 23/11/2012.
//  Copyright (c) 2012 Clarity. All rights reserved.
//

#import <UIKit/UIKit.h>

/** @file CRSTableViewControllerBase.h */

/** @brief Base class for table view controllers.
 */
@interface CRSTableViewControllerBase : UITableViewController

/** @brief Called to show/hide the overlay
 *  @param show YES, if the overlay should be shown, NO otherwise.
 *  @param animated YES, if the overlay should be shown/hidden using a visual transition.
 */
- (void) showModalProgressOverlay:(BOOL)show
                         animated:(BOOL)animated;

@end
