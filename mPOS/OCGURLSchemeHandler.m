//
//  OCGURLSchemeHandler.m
//  mPOS
//
//  Created by Antonio Strijdom on 12/08/2013.
//  Copyright (c) 2013 Clarity. All rights reserved.
//

#import "OCGURLSchemeHandler.h"

@implementation OCGURLSchemeHandler

#pragma mark - URL handling helper methods

- (NSDictionary *) dictionaryForQuery:(NSString *)query
{
    NSMutableDictionary *result = [NSMutableDictionary dictionary];
    
    // split into key/value pairs
    NSArray *queryKeyValues = [query componentsSeparatedByString:@"&"];
    
    for (NSString *keyValue in queryKeyValues) {
        // find the '=' seperator
        NSRange equalRange = [keyValue rangeOfString:@"="];
        if (equalRange.location != NSNotFound) {
            // split the pair
            NSString *key = [keyValue substringToIndex:equalRange.location];
            NSString *value = [keyValue substringFromIndex:equalRange.location + 1];
            // undo percent escapes
            value = [value stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
            // update the dictionary
            [result setValue:value
                      forKey:key];
        }
    }
    
    return result;
}

- (NSString *) queryForDictionary:(NSDictionary *)dictionary
{
    NSMutableString *result = [NSMutableString string];
    
    // enumerate through the keys
    for (NSString *key in dictionary) {
        // add the seperator
        if ([result length] > 0) {
            [result appendString:@"&"];
        }
        // get the value for this key
        NSString *value = [dictionary[key] description];
        // percent escape
        value = [value stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        // append
        [result appendFormat:@"%@=%@", key, value];
    }
    
    return result;
}

- (NSString *) stringForHexString:(NSString *)hexString
{
	char *s = malloc((int) [hexString length] / 2);
    unsigned x;
    int i;
	for (i = 0; i<((int) [hexString length] / 2); i++) {
		[[NSScanner scannerWithString:[hexString substringWithRange:NSMakeRange(i*2, 2)]] scanHexInt:&x];
		s[i] = (char)x;
	}
    NSString *result = [[NSString alloc] initWithBytes:s
                                                length:((int)[hexString length] / 2)
                                              encoding:NSUTF8StringEncoding];
    free(s);
    
    return result;
}

- (NSString *) hexStringForString:(NSString *)string
{
	NSMutableString *result = [NSMutableString string];
    
	short index = 0;
	for (; index < [string length]; index++) {
		[result appendFormat:@"%x", [string characterAtIndex:index]];
	}
    
	return result;
}

#pragma mark - Methods

- (NSString *) returnURLScheme
{
    return @"";
}

- (BOOL) application:(UIApplication *)application
             openURL:(NSURL *)url
   sourceApplication:(NSString *)sourceApplication
          annotation:(id)annotation
{
    BOOL result = YES;
    
    // First check we're responding to the correct scheme for this class.
    if (![url.scheme isEqualToString:[self returnURLScheme]]) {
        result = NO;
    } else {
        result = YES;
    }
    
    return result;
}

@end
