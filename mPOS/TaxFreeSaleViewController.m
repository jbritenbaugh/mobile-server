//
//  TaxFreeSaleViewController.m
//  mPOS
//
//  Created by John Scott on 14/05/2014.
//  Copyright (c) 2014 Clarity. All rights reserved.
//

#import "TaxFreeSaleViewController.h"

#import "NSArray+OCGIndexPath.h"
#import "UITableView+OCGExtensions.h"

#import "OCGTableViewCell.h"
#import "OCGTextField.h"
#import "OCGFirstResponderAccessoryView.h"
#import "OCGSelectViewController.h"

#import "OCGSelectViewController.h"
#import "BasketController.h"
#import "CRSSkinning.h"
#import "OCGKeyboardTypeButtonItem.h"
#import "OCGInputAccessoryView.h"

#define DEFAULT_IF_ONE_VALUE (1)


typedef NS_ENUM(NSInteger, _TaxFreeSaleDetail)
{
    _TaxFreeSaleDetailDocumentId,
    _TaxFreeSaleDetailDocumentType,
};


@interface TaxFreeSaleViewController () <UITextFieldDelegate, OCGFirstResponderAccessoryViewDelegate>

@end

@implementation TaxFreeSaleViewController
{
    BasketTaxFree *_basketTaxFree;
    NSArray *_visibleDetails;
    OCGFirstResponderAccessoryView *_firstResponderAccessoryView;
}

static NSString *CellIdentifier = @"Cell";

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:UITableViewStyleGrouped];
    if (self) {
        _basketTaxFree = [[BasketTaxFree alloc] init];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    [self.tableView registerClass:[OCGTableViewCell class] forCellReuseIdentifier:CellIdentifier];

    _firstResponderAccessoryView = [[OCGFirstResponderAccessoryView alloc] init];
    
    _firstResponderAccessoryView.delegate = self;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if (DEFAULT_IF_ONE_VALUE && [[BasketController.sharedInstance customerIdentificationTypes] count] == 1)
    {
        CustomerIdentificationType *customerIdentificationType = [[BasketController.sharedInstance customerIdentificationTypes] firstObject];
        _basketTaxFree.documentType = customerIdentificationType.type;
    }
    [[CRSSkinning currentSkin] applyViewSkin:self];
    [self updateNavigationItem];
    [self.tableView reloadData];
}

-(void)viewDidAppear:(BOOL)animated
{
    if (DEFAULT_IF_ONE_VALUE && [[BasketController.sharedInstance customerIdentificationTypes] count] == 1)
    {
        NSIndexPath *indexPath = [_visibleDetails firstIndexPathForObject:@(_TaxFreeSaleDetailDocumentId)];
        OCGTableViewCell *cell = (OCGTableViewCell *) [self.tableView cellForRowAtIndexPath:indexPath];
        [cell.detailView becomeFirstResponder];
    }
}

-(void)updateNavigationItem
{
    if (_basketTaxFree.documentType)
    {
        _visibleDetails = @[
                            @[
                                @(_TaxFreeSaleDetailDocumentType),
                                @(_TaxFreeSaleDetailDocumentId),
                                ],
                            ];
    }
    else if ([[BasketController.sharedInstance customerIdentificationTypes] count] > 0)
    {
        _visibleDetails = @[
                            @[
                                @(_TaxFreeSaleDetailDocumentType),
                                ],
                            ];
    }
    else
    {
        _visibleDetails = @[
                            @[
                                @(_TaxFreeSaleDetailDocumentId),
                                ],
                            ];
    }
    
    if ([self.navigationController.viewControllers[0] isEqual:self])
    {
        UIBarButtonItem *cancelButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel
                                                                                      target:self
                                                                                      action:@selector(dismissViewController)];
        
        self.navigationItem.leftBarButtonItem = cancelButton;
    }
    
    UIBarButtonItem *submitButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁈󠁳󠁲󠁲󠁌󠀴󠀱󠁍󠁘󠁗󠁐󠁔󠁘󠁏󠁕󠀱󠁚󠁣󠁮󠁏󠁴󠀲󠁖󠁹󠁣󠁄󠁉󠁿*/ @"Submit", nil)
                                                                     style:UIBarButtonItemStyleDone
                                                                    target:self
                                                                    action:@selector(submitButtonTapped)];
    
    submitButton.enabled = [self canSubmitItem];
    
    self.navigationItem.rightBarButtonItem = submitButton;
}

-(void)dismissViewController
{
    [self dismissViewControllerAnimated:YES completion:NULL];
}

-(BOOL)canSubmitItem
{
    BOOL canSubmitItem = YES;
    
    if ([[BasketController.sharedInstance customerIdentificationTypes] count] > 0 && [_basketTaxFree.documentType length] == 0)
    {
        canSubmitItem = NO;
    }

    return canSubmitItem;
}

-(void)submitButtonTapped
{
    ocg_async_background_overlay(^{
        BasketDTO *basket = BasketController.sharedInstance.basket;
        basket.taxFree = _basketTaxFree;
        
        NSError *error = nil;
        ServerError *serverError = nil;
        basket = [BasketController.sharedInstance adjustSale:basket
                                                       error:&error
                                                 serverError:&serverError];
        
        ocg_sync_main(^{
            if (basket)
            {
                BasketController.sharedInstance.basket = basket;
                [BasketController.sharedInstance kick];
            }
            
            if (error != nil || serverError != nil)
            {
                // there was an error
                ServerError *mposerror = serverError;
                [BasketController.sharedInstance kick];
                // send the notification
                [[NSNotificationCenter defaultCenter] postNotificationName:kBasketControllerGeneralErrorNotification
                                                                    object:self
                                                                  userInfo:@{kBasketControllerGeneralErrorKey:mposerror}];
            }
            else
            {
                [self dismissViewController];
            }
        });
    });
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [_visibleDetails count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [_visibleDetails[section] count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    return cell;
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(OCGTableViewCell*)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    _TaxFreeSaleDetail detail = [_visibleDetails integerForIndexPath:indexPath];
    cell.tag = kEditableTableCellSkinningTag;
    cell.textLabel.tag = kEditableCellKeySkinningTag;
    cell.detailTextField.tag = kEditableCellValueSkinningTag;
    cell.accessoryType = UITableViewCellAccessoryNone;
    cell.detailTextField.clearButtonMode = UITextFieldViewModeAlways;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.detailTextField.delegate = self;
    cell.detailTextField.inputAccessoryView = _firstResponderAccessoryView;
    switch (detail) {
        case _TaxFreeSaleDetailDocumentId:
        {
            cell.textLabel.text = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁩󠁱󠁸󠁇󠁑󠁦󠁣󠁔󠁕󠁂󠀳󠀳󠁐󠁙󠁂󠁙󠁂󠁖󠀵󠀶󠀯󠁄󠁎󠁱󠁢󠁷󠁕󠁿*/ @"Identfier", @"The document identfier for tax free status");
            cell.detailTextField.text = _basketTaxFree.documentId;
            OCGInputAccessoryView *inputAccessoryView = [[OCGInputAccessoryView alloc] init];
            OCGKeyboardTypeButtonItem *keyboardTypeButtonItem =
            [[OCGKeyboardTypeButtonItem alloc] initWithKeyboardTypes:@[kOCGKeyboardTypeNumberPad, kOCGKeyboardTypeASCIICapable]
                                                               style:UIBarButtonItemStylePlain
                                                              target:cell.detailTextField];
            
            inputAccessoryView.leftButtons = @[keyboardTypeButtonItem];
            
            UIBarButtonItem *doneButton =
            [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone
                                                          target:cell.detailTextField
                                                          action:@selector(resignFirstResponder)];
            
            inputAccessoryView.rightButtons = @[doneButton];
            
            cell.detailTextField.inputAccessoryView = inputAccessoryView;
        }
            break;
        case _TaxFreeSaleDetailDocumentType:
            cell.textLabel.text = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁋󠁘󠀷󠁬󠁃󠁍󠁷󠁌󠁘󠁐󠁗󠁘󠁫󠁯󠁏󠁕󠁐󠁑󠁲󠁈󠀵󠀶󠁙󠁫󠀲󠁗󠁑󠁿*/ @"Type", @"The document type for tax free status");
            cell.detailTextField.text = _basketTaxFree.documentType;
            if (!DEFAULT_IF_ONE_VALUE || [[BasketController.sharedInstance customerIdentificationTypes] count] > 1)
            {
                cell.selectionStyle = UITableViewCellSelectionStyleBlue;
                cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            }
            else
            {
                cell.detailTextField.clearButtonMode = UITextFieldViewModeNever;
            }
           break;
            
        default:
            break;
    }
    
    [[CRSSkinning currentSkin] applyCellSkin:cell
                                    forStyle:tableView.style];
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    _TaxFreeSaleDetail detail = [_visibleDetails integerForIndexPath:indexPath];
    if (detail == _TaxFreeSaleDetailDocumentType && (!DEFAULT_IF_ONE_VALUE || [[BasketController.sharedInstance customerIdentificationTypes] count] > 1))
    {
        // present the reason list
        OCGSelectViewController *selectViewController =
        [[OCGSelectViewController alloc] init];
        selectViewController.title = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁚󠁍󠁕󠁕󠁸󠁣󠁮󠁎󠀲󠁐󠀰󠁲󠁁󠁎󠁳󠁹󠁁󠁖󠁧󠁊󠁃󠀷󠁕󠁒󠁣󠁌󠁑󠁿*/ @"Document type", nil);
        selectViewController.options = @[[[BasketController sharedInstance] customerIdentificationTypes]];
        selectViewController.titleForOption = ^(OCGSelectViewController *selectViewController, NSIndexPath *indexPath)
        {
            CustomerIdentificationType *type =
            [selectViewController.options objectForIndexPath:indexPath];
            return type.description;
        };
        selectViewController.didSelectOption = ^(OCGSelectViewController *selectViewController, NSIndexPath *indexPath)
        {
            CustomerIdentificationType *type =
            [selectViewController.options objectForIndexPath:indexPath];
            _basketTaxFree.documentType = type.type;
            [self.navigationController popViewControllerAnimated:YES];
            NSArray *indexPaths = [_visibleDetails indexPathsForObjects:@[@(_TaxFreeSaleDetailDocumentType), @(_TaxFreeSaleDetailDocumentId)]];
            [self.tableView reloadRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationNone];
            [self updateNavigationItem];
        };
        [self.navigationController pushViewController:selectViewController
                                             animated:YES];
    }
    else if (detail == _TaxFreeSaleDetailDocumentId)
    {
        OCGTableViewCell *cell = (OCGTableViewCell *) [self.tableView cellForRowAtIndexPath:indexPath];
        [cell.detailView becomeFirstResponder];
    }
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    NSIndexPath *indexPath = [self.tableView indexPathForCell:textField.OCGTableViewCell_tableViewCell];
    _TaxFreeSaleDetail detail = [_visibleDetails integerForIndexPath:indexPath];
    
    BOOL textFieldShouldBeginEditing = YES;
    switch (detail) {
        case _TaxFreeSaleDetailDocumentType:
            textFieldShouldBeginEditing = NO;
            break;
            
        default:
            textFieldShouldBeginEditing = YES;
            break;
    }
    return textFieldShouldBeginEditing;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    NSIndexPath *indexPath = [self.tableView indexPathForCell:textField.OCGTableViewCell_tableViewCell];
    _firstResponderAccessoryView.currentPosition = indexPath;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    _firstResponderAccessoryView.currentPosition = nil;
}

- (BOOL)textFieldShouldClear:(UITextField *)textField
{
    NSIndexPath *indexPath = [self.tableView indexPathForCell:textField.OCGTableViewCell_tableViewCell];
    
    _TaxFreeSaleDetail detail = [_visibleDetails integerForIndexPath:indexPath];
    switch (detail) {
        case _TaxFreeSaleDetailDocumentType:
            _basketTaxFree.documentType = nil;
            [self updateNavigationItem];
            [self.tableView reloadData];
            break;
        
        case _TaxFreeSaleDetailDocumentId:
            _basketTaxFree.documentId = nil;
            
        default:
            break;
    }
    
    OCGTableViewCell *cell = (OCGTableViewCell *) [self.tableView cellForRowAtIndexPath:indexPath];
    cell.detailTextField.text = nil;
    
    return NO;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if ([self canSubmitItem]) {
        [self submitButtonTapped];
    } else {
        [_firstResponderAccessoryView moveFirstResponderByOffset:1];
    }
    return NO;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSIndexPath *indexPath = [self.tableView indexPathForCell:textField.OCGTableViewCell_tableViewCell];
    NSString *text = [textField.text stringByReplacingCharactersInRange:range withString:string];
    
    _TaxFreeSaleDetail detail = [_visibleDetails integerForIndexPath:indexPath];
    switch (detail) {
         case _TaxFreeSaleDetailDocumentId:
            _basketTaxFree.documentId = text;
            [self updateNavigationItem];
            
        default:
            break;
    }

    return YES;
}
//    NSIndexPath *indexPath = [self.tableView indexPathForCell:textField.OCGTableViewCell_tableViewCell];
//    NSString *text = [textField.text stringByReplacingCharactersInRange:range withString:string];
//    
//    
//    
//    ReceiptItemDetail itemDetail = [@[_visibleDetails] integerForIndexPath:indexPath];
//    
//    if (itemDetail == ReceiptItemDetailOriginalItemDescription && [text length] > 255)
//    {
//        return NO;
//    }
//    
//    if (itemDetail == ReceiptItemDetailPrice)
//    {
//        _itemDetails[@(itemDetail)] = [_priceValidator numberFromString:text];
//        textField.text = [_priceValidator stringFromNumber:_itemDetails[@(itemDetail)]];
//        [self updateNavigationItem];
//        
//        return NO;
//    }
//    else
//    {
//        _itemDetails[@(itemDetail)] = text;
//        
//        [self updateNavigationItem];
//        
//        return YES;
//    }
//}

#pragma mark - OCGFirstResponderAccessoryViewDelegate

-(UIResponder*)OCGFirstResponderAccessoryView:(OCGFirstResponderAccessoryView *)firstResponderAccessoryView firstResponderViewForPosition:(id)position
{
    [self.tableView scrollToRowAtIndexPath:position atScrollPosition:UITableViewScrollPositionMiddle animated:NO];
    OCGTableViewCell *cell = (OCGTableViewCell*) [self.tableView cellForRowAtIndexPath:position];
    if ([cell respondsToSelector:@selector(detailTextField)])
    {
        return cell.detailTextField;
    }
    else
    {
        return nil;
    }
}

- (id)OCGFirstResponderAccessoryView:(OCGFirstResponderAccessoryView*)firstResponderAccessoryView positionFromPosition:(id)position offset:(NSInteger)offset
{
    return [self.tableView indexPathFromIndexPath:position offset:offset wrap:YES];
}

@end
