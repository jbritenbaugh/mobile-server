//
//  GiftReceiptViewController.m
//  mPOS
//
//  Created by Antonio Strijdom on 19/02/2014.
//  Copyright (c) 2014 Clarity. All rights reserved.
//

#import "GiftReceiptViewController.h"
#import "BasketController.h"
#import "POSFuncController.h"

@interface GiftReceiptViewController ()
@property (nonatomic, strong) NSMutableSet *originalGiftReceiptItems;
- (void) updateUI;
- (IBAction) cancelButtonTapped:(id)sender;
- (IBAction) saveButtonTapped:(id)sender;
@end

@implementation GiftReceiptViewController

#pragma mark - Private

- (void) updateUI
{
    self.navigationItem.rightBarButtonItem.enabled = ![self.selectedBasketItems isEqual:self.originalGiftReceiptItems];
}

#pragma mark - UIViewController

- (void) viewWillAppear:(BOOL)animated
{
    self.title = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠀵󠁓󠁹󠁱󠁎󠁨󠁣󠁘󠁷󠁬󠁁󠁑󠁉󠁨󠁤󠁕󠁌󠀲󠁓󠁵󠁕󠀷󠁊󠁣󠁁󠁇󠁍󠁿*/ @"Gift Receipt", nil);
    
	self.navigationItem.leftBarButtonItem =
    [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel
                                                  target:self
                                                  action:@selector(cancelButtonTapped:)];
    
    self.navigationItem.rightBarButtonItem =
    [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemSave
                                                  target:self
                                                  action:@selector(saveButtonTapped:)];

    [super viewWillAppear:animated];
    
    self.selectedBasketItems = [NSMutableSet set];
    self.originalGiftReceiptItems = [NSMutableSet set];
    for (int i = 0; i < self.validBasketItems.count; i++) {
        MPOSSaleItem *basketItem = self.validBasketItems[i];
        if (basketItem.generateGiftReceiptValue) {
            [self.originalGiftReceiptItems addObject:basketItem];
            NSIndexPath *path = [NSIndexPath indexPathForRow:i inSection:0];
            [self tableView:self.tableView didSelectRowAtIndexPath:path];
            [self.tableView selectRowAtIndexPath:path
                                        animated:NO
                                  scrollPosition:UITableViewScrollPositionNone];
        }
    }
    
    [self updateUI];
}

#pragma mark - UITableViewDelegate

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [super tableView:tableView didSelectRowAtIndexPath:indexPath];
    [self updateUI];
}

- (void) tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [super tableView:tableView didDeselectRowAtIndexPath:indexPath];
    [self updateUI];
}

#pragma mark Actions

- (IBAction) cancelButtonTapped:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:NULL];
}

- (IBAction) saveButtonTapped:(id)sender
{ 
    if ([[POSFuncController sharedInstance] markBasketItemsForGiftReceipt:[self selectedBasketItemIdArray]]) {
        [self dismissViewControllerAnimated:YES
                                 completion:nil];
    }
}

@end
