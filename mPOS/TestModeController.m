//
//  TestModeController.m
//  mPOS
//
//  Created by Antonio Strijdom on 25/07/2012.
//  Copyright (c) 2012 Clarity. All rights reserved.
//

#import "TestModeController.h"
#import "CRSSkinning.h"
#import "OCGSettingsController.h"

@interface TestModeController ()
@end

/** @brief Provides test mode functionality.
 *  Allows access to the test mode setting and sends 
 *  notications when the test mode is toggled.
 */

NSString * const kTestModeControllerServerModeDidChangeNotification = @"kTestModeControllerServerModeDidChangeNotification";
NSString * const kTestModeControllerLiveServerConfigKey = @"live_server";
NSString * const kTestModeControllerOfflineServerConfigKey = @"offline_server";
NSString * const kTestModeControllerServerModeConfigKey = @"server_mode";

@implementation TestModeController

#pragma mark - Methods

+ (NSString *) getServerAddress
{
    NSString *serverAddress = nil;
    switch ([self serverMode])
    {
        case ServerModeOnline:
            serverAddress = [self getOnlineServerAddress];
            break;
            
        case ServerModeOffline:
            serverAddress = [self getOfflineServerAddress];
            break;
            
        default:
            break;
    }
    return serverAddress;
}

+ (NSString *) getOnlineServerAddress
{
    NSString *server = [[OCGSettingsController sharedInstance] valueForSettingWithName:kTestModeControllerLiveServerConfigKey];
    
    return server;
}

+ (NSString *) getOfflineServerAddress
{
    NSString *server = [[OCGSettingsController sharedInstance] valueForSettingWithName:kTestModeControllerOfflineServerConfigKey];
    
    return server;
}

+ (ServerMode) serverMode
{
    NSNumber *serverModeNumber = [[OCGSettingsController sharedInstance] valueForSettingWithName:kTestModeControllerServerModeConfigKey];
    ServerMode  serverMode = serverModeNumber.integerValue;
    
    return serverMode;
}

+ (void) setServerMode:(ServerMode)serverMode
{
    NSNumber *oldServerMode = [[OCGSettingsController sharedInstance] valueForSettingWithName:kTestModeControllerServerModeConfigKey];
    if (oldServerMode.integerValue != serverMode)
    {
        [[OCGSettingsController sharedInstance] setValue:@(serverMode)
                                      forSettingWithName:kTestModeControllerServerModeConfigKey];

        [[NSNotificationCenter defaultCenter] postNotificationName:kTestModeControllerServerModeDidChangeNotification object:nil];
    }
}


@end
