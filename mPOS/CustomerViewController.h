//
//  CustomerViewController.h
//  mPOS
//
//  Created by John Scott on 14/05/2013.
//  Copyright (c) 2013 Clarity. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol CustomerViewControllerDelegate;
@class MPOSCustomer;

@interface CustomerViewController : UITableViewController

/** @brief A reference to the owner delegate which will receive notifications from this view controller.
 */
@property (strong, nonatomic) id<CustomerViewControllerDelegate> delegate;
/** @property customer
 *  @brief The customer we are displaying.
 */
@property (strong, nonatomic) MPOSCustomer *customer;
/** @property presentedFromSearch
 *  @brief Whether the view was presented from a search or not.
 *  @discussion If the view was presented from a search, it does not display a dismiss button. If it was not, it will display the dismiss button and it will ask the delegate when tapped and the delegate is responsible for dismissing this view.
 *  @see CustomerViewControllerDelegate
 */
@property (assign, nonatomic) BOOL presentedFromSearch;

@end

/** @brief Protocol that the owner delegate for this view controller needs to implement.
 */
@protocol CustomerViewControllerDelegate <NSObject>
@required

/** @brief Called whenever the user chosed to dismiss the view without recalling a basket.
 *  @param viewController A reference to the view controller to be dismissed.
 */
- (void) dismissCustomerSearchViewController:(CustomerViewController *)customerController;
@optional
/** @brief Called when the customer is assigned to a basket.
 */
- (void) customerAssignedController:(CustomerViewController *)customerController
                             adding:(BOOL)adding;

@end
