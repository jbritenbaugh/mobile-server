//
//  OCGModifierViewController.h
//  mPOS
//
//  Created by Antonio Strijdom on 11/09/2014.
//  Copyright (c) 2014 Omnico. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RESTController.h"
#import "GroupSelection.h"

@protocol OCGModifierViewControllerDelegate;

/** @brief Collection view for presenting modifier options
 */
@interface OCGModifierViewController : UICollectionViewController

/** @property delegate
 *  @brief The callback delegate for the modifier view controller.
 */
@property (nonatomic, assign) id<OCGModifierViewControllerDelegate> delegate;

/** @property optional
 *  @brief The user is making optional modifier selections - allow cancelling
 */
@property (nonatomic, assign) BOOL optional;

/** @property itemDesc
 *  @brief The name of the item we are displaying modifiers for.
 */
@property (nonatomic, strong) NSString *itemDesc;

/** @property choices
 *  @brief The modifier choices presented to the user.
 *  @note Setting this property will reset selections.
 *  @see selections
 */
@property (nonatomic, strong) NSArray *choices;

/** @property selections
 *  @brief Keeps track of the selections the user has made.
 *  @see choices
 */
@property (nonatomic, readonly) NSArray *selections;

@end

/** @brief Delegate protocol for OCGModifierViewController
 */
@protocol OCGModifierViewControllerDelegate
@required
/** @brief Called when the user has made all the required selections.
 *  @param vc         The view controller calling this method.
 *  @param selections The modifiers the user selected.
 */
- (void)modifierViewController:(OCGModifierViewController *)vc
             didMakeSelections:(NSArray *)selections;
/** @brief Called when the user cancels modifier selection.
 *  @param vc The view controller calling this method.
 */
- (void)modifierViewControllerDidCancel:(OCGModifierViewController *)vc;
/** @brief Called when modifier selection cannot continue.
 *  @param vc The view controller calling this method.
 */
- (void)modifierViewControllerDidError:(OCGModifierViewController *)vc;
@end