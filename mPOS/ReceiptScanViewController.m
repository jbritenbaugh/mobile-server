//
//  ReturnReceiptScanViewController.m
//  mPOS
//
//  Created by John Scott on 04/02/2014.
//  Copyright (c) 2014 Clarity. All rights reserved.
//

#import "ReceiptScanViewController.h"

#import "UICollectionView+OCGExtensions.h"
#import "FNKCollectionViewLayout.h"
#import "OCGContextCollectionViewDataSource.h"

#import "OCGCollectionViewCell.h"

#import "BarcodeScannerController.h"
#import "ReceiptSearchViewController.h"
#import "ServerErrorHandler.h"
#import "ReceiptListViewController.h"
#import "RESTController.h"
#import "UIAlertView+MKBlockAdditions.h"
#import "ReceiptItemViewController.h"
#import "BasketController.h"
#import "POSFuncController.h"
#import "POSFunc.h"
#import "CRSSkinning.h"
#import "SoftKeyContextType.h"

@interface ReceiptScanViewController ()

@end

@implementation ReceiptScanViewController

- (void)viewWillAppear:(BOOL)animated
{
    self.title = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁲󠁭󠁵󠁶󠁘󠁹󠀰󠁴󠁨󠀫󠁇󠁄󠁩󠁱󠀶󠁴󠁃󠁔󠀳󠁋󠁄󠁹󠁑󠁗󠁔󠁣󠀸󠁿*/ @"Return", nil);
    
    // reload the layout
    self.softKeyContext =
    [BasketController.sharedInstance getLayoutForContextNamed:NSStringForSoftKeyContextType(SoftKeyContextTypeMenuReturnsLayout)];
    
    self.delegate = self;
    
    [super viewWillAppear:animated];
}

#pragma mark - FNKCollectionViewDelegate

-(void)FNKScanViewController:(FNKScanViewController *)collectionViewController
            didSelectSoftKey:(SoftKey *)softKey
{
    if ([softKey.keyType isEqualToString:@"POSFUNC"]) {
        if ([softKey.posFunc isEqualToString:@"CONTEXT_MENU_ITEM_SCAN"]) {
            [self startScanningForBarcode];
        } else if ([softKey.posFunc isEqualToString:@"RETURN_MANUAL_ENTRY"]) {
            [self manualReceiptButtonTapped];
        } else if ([softKey.posFunc isEqualToString:@"RETURN_INTERNET"]) {
            [self internetReceiptReturn];
        } else if ([softKey.posFunc isEqualToString:@"RETURN_NO_RECEIPT"]) {
            [self noReceiptReturn];
        }
    }
}

#pragma mark - Methods

-(void)manualReceiptButtonTapped
{
    __weak UIViewController *presentingViewController = self.presentingViewController;
    [self dismissViewControllerAnimated:YES completion:^{
        BarcodeScannerController *controller = [BarcodeScannerController sharedInstance];
        // are we scanning?
        if (controller.scanning)
        {
            [controller stopScanning];
        }
        ReceiptSearchViewController *viewController = [[ReceiptSearchViewController alloc] init];
        
        UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:viewController];
        navigationController.modalPresentationStyle = UIModalPresentationFormSheet;
        [presentingViewController presentViewController:navigationController animated:YES completion:NULL];
    }];
}

-(void)internetReceiptReturn
{
    if ([[BasketController sharedInstance] doesUserHavePermissionTo:POSPermissionAddReturnItemInternet])
    {
        __weak UIViewController *presentingViewController = self.presentingViewController;
        [self dismissViewControllerAnimated:YES completion:^{
            ReceiptItemViewController *viewController = [[ReceiptItemViewController alloc] init];
            viewController.type = ReceiptItemViewControllerTypeInternetReturn;
            
            UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:viewController];
            navigationController.modalPresentationStyle = UIModalPresentationFormSheet;
            [presentingViewController presentViewController:navigationController animated:YES completion:NULL];
        }];
    }
    else
    {
        [POSFuncController presentPermissionAlert:POSPermissionAddReturnItemInternet
                                        withBlock:nil];
    }
}

-(void)noReceiptReturn
{
    if ([[BasketController sharedInstance] doesUserHavePermissionTo:POSPermissionAddReturnItemNoreceipt])
    {
        __weak UIViewController *presentingViewController = self.presentingViewController;
        [self dismissViewControllerAnimated:YES completion:^{
            ReceiptItemViewController *viewController = [[ReceiptItemViewController alloc] init];
            viewController.type = ReceiptItemViewControllerTypeNoReceiptReturn;
            
            UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:viewController];
            navigationController.modalPresentationStyle = UIModalPresentationFormSheet;
            [presentingViewController presentViewController:navigationController animated:YES completion:NULL];
        }];
    }
    else
    {
        [POSFuncController presentPermissionAlert:POSPermissionAddReturnItemNoreceipt
                                        withBlock:nil];
    }
}

-(void)unlinkedReturn:(ServerErrorBarcodeItemNotReturnedMessage*)message
{
    __weak UIViewController *presentingViewController = self.presentingViewController;
    [self dismissViewControllerAnimated:YES completion:^{
        BarcodeScannerController *controller = [BarcodeScannerController sharedInstance];
        // are we scanning?
        if (controller.scanning)
        {
            [controller stopScanning];
        }
        ReceiptItemViewController *viewController = [[ReceiptItemViewController alloc] init];
        viewController.type = ReceiptItemViewControllerTypeUnlinkedReturn;
        NSMutableDictionary *receiptDetails = [NSMutableDictionary dictionary];
        //                                [receiptDetails setValue:message.originalItemPrice forKey:@(ReceiptItemDetailOriginalItemPrice)];
        [receiptDetails setObject:message.originalTransactionDate forKey:@(ReceiptItemDetailOriginalTransactionDate)];
        [receiptDetails setObject:message.originalTransactionId forKey:@(ReceiptItemDetailOriginalTransactionId)];
        [receiptDetails setObject:message.originalTransactionStoreId forKey:@(ReceiptItemDetailOriginalTransactionStoreId)];
        [receiptDetails setObject:message.originalTransactionTill forKey:@(ReceiptItemDetailOriginalTransactionTillNumber)];
        viewController.initialItemDetails = receiptDetails;
        
        
        UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:viewController];
        navigationController.modalPresentationStyle = UIModalPresentationFormSheet;
        [presentingViewController presentViewController:navigationController animated:YES completion:NULL];
    }];
}

-(void)FNKScanViewController:(FNKScanViewController *)collectionViewController
        handleScannedBarcode:(NSString *)receiptBarcode
{
    ocg_async_background_overlay(^{
        [RESTController.sharedInstance lookupSaleWithBarcode:receiptBarcode
                                            originalBasketId:nil
                                             originalStoreId:nil
                                          originalTillNumber:nil
                                     originalTransactionDate:nil
                                                    complete:^(BasketAssociateDTO *basket, NSError *error, ServerError *serverError)
         {
             dispatch_async(dispatch_get_main_queue(), ^{
                 if (error != nil || serverError != nil)
                 {
                     ServerErrorBarcodeItemNotReturnedMessage *message = [serverError.messages lastObject];
                     if ([message isKindOfClass:[ServerErrorBarcodeItemNotReturnedMessage class]] && [[BasketController sharedInstance] doesUserHavePermissionTo:POSPermissionAddReturnItemUnlinked])
                     {
                         if ([[BasketController sharedInstance] doesUserHavePermissionTo:POSPermissionAddReturnItemLinked])
                         {
                             NSString *title = nil;
                             if ([message.code integerValue] == MShopperBarcodeItemCouldNotBeReturnedExceptionCode)
                             {
                                 title = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠀰󠁳󠁹󠁕󠁲󠀲󠁮󠁪󠁱󠁹󠁔󠁔󠀰󠁋󠁲󠀯󠁖󠁮󠁫󠁌󠁵󠁔󠀵󠁷󠁡󠁑󠀰󠁿*/ @"Receipt not found", nil);
                             }
                             else
                             {
                                 title = [[ServerErrorHandler sharedInstance] buildServerErrorMessage:serverError
                                                                                       transportError:error
                                                                                          withContext:nil];
                             }
                             
                             UIAlertView *alert =
                             [[UIAlertView alloc] initWithTitle:title
                                                        message:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁖󠁵󠁨󠁈󠁥󠀰󠀳󠁌󠀱󠁨󠁨󠁋󠀶󠁡󠁘󠁬󠀸󠀫󠁃󠀹󠀷󠁨󠀷󠁬󠁶󠁊󠁫󠁿*/ @"Continue as an unlinked return?", nil)
                                                       delegate:[UIAlertView class]
                                              cancelButtonTitle:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁩󠁢󠁺󠁬󠁏󠁶󠁣󠁧󠁫󠁍󠁋󠁧󠁫󠀱󠁶󠁐󠁐󠀹󠁘󠀴󠁘󠁡󠁕󠁭󠁹󠁷󠀰󠁿*/ @"Cancel", nil)
                                              otherButtonTitles:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁄󠁴󠁍󠁉󠁣󠀫󠁺󠁔󠁗󠁦󠁹󠁡󠁯󠀷󠁨󠁔󠁩󠀸󠁯󠀫󠁨󠁘󠀹󠁱󠁌󠀶󠀸󠁿*/ @"OK", nil), nil];
                             
                             alert.dismissBlock = ^(int buttonIndex) {
                                 if (buttonIndex == 0)
                                 {
                                     [self unlinkedReturn:message];
                                 }
                             };
                             [alert show];
                         }
                         else
                         {
                             [self unlinkedReturn:message];
                         }
                     }
                     else
                     {
                         ocg_define_weak_self();
                         [[ServerErrorHandler sharedInstance] handleServerError:serverError
                                                                 transportError:error
                                                                    withContext:nil
                                                                   dismissBlock:^{
                                                                       [ocg_get_weak_self() startScanningForBarcode];
                                                                   }
                                                                    repeatBlock:nil];
                     }
                 }
                 else
                 {
                     __weak UIViewController *presentingViewController = self.presentingViewController;
                     [self dismissViewControllerAnimated:YES completion:^{
                         ReceiptListViewController *viewController = [[ReceiptListViewController alloc] init];
                         viewController.basket = basket;
                         
                         UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:viewController];
                         navigationController.modalPresentationStyle = UIModalPresentationFormSheet;
                         [presentingViewController presentViewController:navigationController animated:YES completion:NULL];
                     }];
                 }
             });
         }];
    });
}

@end
