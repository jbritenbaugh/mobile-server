//
//  SettingsTableViewController.m
//  mPOS
//
//  Created by Antonio Strijdom on 25/07/2012.
//  Copyright (c) 2012 Clarity. All rights reserved.
//

#import "SettingsTableViewController.h"
#import "CRSSkinning.h"
#import "ScannerSettingTableViewController.h"
#import "PrinterController.h"
#import "PrinterSettingTableViewController.h"
#import "CRSLocationController.h"
#import "LocationDataSource.h"
#import "CRSLocationSummary.h"
#import "TillSetupController.h"
#import "BasketController.h"
#import "NSArray+OCGIndexPath.h"
#import "TestModeController.h"
#import "UIAlertView+MKBlockAdditions.h"
#import "AppDelegate.h"
#import "OCGSelectViewController.h"
#import "OCGProgressOverlay.h"
#import "BarcodeScannerController.h"
#import "OCGPeripheralManager.h"
#import "BasketController.h"
#import "OCGEFTManager.h"
#import "OCGCollectionViewCell.h"
#import "OCGReceiptManager.h"
#import "VersionDetails.h"
#import "NSLocale+OCGAdditions.h"
#import "FNKCollectionViewLayout.h"
#import "FNKSupplementaryView.h"
#import "OCGSettingsController.h"
#import "OCGKeyboardTypeButtonItem.h"

#define kSettingEntryScanner @"kSettingEntryScanner"
#define kSettingEntryPrinter @"kSettingEntryPrinter"
#define kSettingEntryOnlineLocation @"kSettingEntryOnlineLocation"
#define kSettingEntryOnlineTillNumber @"kSettingEntryOnlineTillNumber"
#define kSettingEntryOnlineServerAddress @"kSettingEntryOnlineServerAddress"
#define kSettingEntryOnlineServerEnabled @"kSettingEntryOnlineServerEnabled"
#define kSettingEntryOnlineServer @"kSettingEntryOnlineServer"
#define kSettingEntryOfflineLocation @"kSettingEntryOfflineLocation"
#define kSettingEntryOfflineTillNumber @"kSettingEntryOfflineTillNumber"
#define kSettingEntryOfflineServerAddress @"kSettingEntryOfflineServerAddress"
#define kSettingEntryOfflineServerEnabled @"kSettingEntryOfflineServerEnabled"
#define kSettingEntryOfflineServer @"kSettingEntryOfflineServer"
#define kSettingEntryPeripherals @"kSettingEntryPeripherals"
#define kSettingEntryOperator @"kSettingEntryOperator"
#define kSettingEntryDisplayName @"kSettingEntryDisplayName"
#define kSettingEntryConfigurationUpdate @"kSettingEntryConfigurationUpdate"
#define kSettingEntryPayment @"kSettingEntryPayment"
#define kSettingEntryPaymentAdditional @"kSettingEntryPaymentAdditional"
#define kSettingEntryLanguage @"kSettingEntryLanguage"


#ifdef __IPHONE_7_0
#define VALUE_COLOR (([UIDevice currentDevice].systemVersion.floatValue >= 7.0)?nil:[UIColor colorWithRed:0.22 green:0.33 blue:0.53 alpha:1])
#else
#define VALUE_COLOR ([UIColor colorWithRed:0.22 green:0.33 blue:0.53 alpha:1])
#endif
#define VALUE_COLOR_NIL ([UIColor grayColor])

NSString *cellReuseIdentifier = @"Cell";


@interface SettingsTableViewController () <ScannerSettingTableViewControllerDelegate, PrinterSettingTableViewControllerDelegate, UIAlertViewDelegate, FNKCollectionViewDataSourceDelegate, UITextFieldDelegate>
@property (strong, nonatomic) CRSLocationSummary *currentOnlineLocation;
@property (strong, nonatomic) CRSLocationSummary *currentOfflineLocation;
@property (strong, nonatomic) LocationDataSource *locationDataSource;
- (void) adminTapped:(id)sender;
@property (strong, nonatomic) NSArray* sectionLayout;
@property (strong, nonatomic) NSArray* layout;
@property (strong, nonatomic) UIBarButtonItem *dismiss;
@property (assign, nonatomic) BOOL adminEnabled;
- (NSString *) settingForLayoutEntry:(NSString *)entryIdentifier;
- (BOOL) isSettingLocallyConfigured:(NSString *)entryIdentifier;
- (void) buildLayout;
- (NSArray *) trimDisabledSettings:(NSArray *)layout;
- (void) handleConfigurationUpdatedNotification:(NSNotification *)note;
- (NSString *) normalisedURLString:(NSString*)text;
- (void) showOnlineServerAddressDialogWithTitle:(NSString*)title
                                        message:(NSString*)message;
- (void) showOfflineServerAddressDialogWithTitle:(NSString *)title
                                         message:(NSString *)message;
@end

/** @brief A table view that displays settings to the user.
 *  Allows the user to change the settings.
 */

@implementation SettingsTableViewController
{
    NSString *_labelReuseIdentifier;
    NSString *_switchReuseIdentifier;
    UIAlertView *_alertView;
    BOOL _resetAdmin;
}

#pragma mark - Private

- (NSString *) settingForLayoutEntry:(NSString *)entryIdentifier
{
    NSString *result = nil;
    
    if ([entryIdentifier isEqualToString:kSettingEntryScanner]) {
        result = kOCGPeripheralManagerConfiguredScannerDeviceKey;
    } else if ([entryIdentifier isEqualToString:kSettingEntryPrinter]) {
        result = kPrinterControllerPrinterConfigKey;
    } else if ([entryIdentifier isEqualToString:kSettingEntryOnlineLocation]) {
        result = kCRSLocationControllerStoreConfigKey;
    } else if ([entryIdentifier isEqualToString:kSettingEntryOnlineTillNumber]) {
        result = kTillSetupOnlineTillNumberConfigKey;
    } else if ([entryIdentifier isEqualToString:kSettingEntryOnlineServerAddress]) {
        result = kTestModeControllerLiveServerConfigKey;
    } else if ([entryIdentifier isEqualToString:kSettingEntryOnlineServerEnabled]) {
        result = kTestModeControllerServerModeConfigKey;
    } else if ([entryIdentifier isEqualToString:kSettingEntryOfflineLocation]) {
        result = kCRSLocationControllerOfflineStoreConfigKey;
    } else if ([entryIdentifier isEqualToString:kSettingEntryOfflineTillNumber]) {
        result = kTillSetupOfflineTillNumberConfigKey;
    } else if ([entryIdentifier isEqualToString:kSettingEntryOfflineServerAddress]) {
        result = kTestModeControllerOfflineServerConfigKey;
    } else if ([entryIdentifier isEqualToString:kSettingEntryOfflineServerEnabled]) {
        result = kTestModeControllerServerModeConfigKey;
    }  else if ([entryIdentifier isEqualToString:kSettingEntryPayment]) {
        result = kOCGEFTManagerConfiguredPaymentProviderKey;
    }
    
    return result;
}

- (BOOL) isSettingLocallyConfigured:(NSString *)entryIdentifier
{
    BOOL result = NO;
    
    NSString *settingName = [self settingForLayoutEntry:entryIdentifier];
    result = [[OCGSettingsController sharedInstance] configurableCheckForSettingWithName:settingName];
    
    return result;
}

- (void) buildLayout
{
    if ([[BasketController sharedInstance] needsAuthentication])
    {
        self.layout = @[
                        @[kSettingEntryOnlineServerAddress, kSettingEntryOnlineLocation, kSettingEntryOnlineTillNumber, kSettingEntryOnlineServerEnabled],
                        @[kSettingEntryOfflineServerAddress, kSettingEntryOfflineLocation, kSettingEntryOfflineTillNumber, kSettingEntryOfflineServerEnabled],
                        @[kSettingEntryScanner, kSettingEntryPrinter, kSettingEntryPayment, kSettingEntryPaymentAdditional],
                        @[
                            kSettingEntryConfigurationUpdate,
#if DEBUG
                            kSettingEntryLanguage,
#endif
                            ],
                        ];
    }
    else
    {
        if ([TestModeController serverMode] == ServerModeOnline)
        {
            self.layout = @[
                            @[kSettingEntryOnlineServerAddress, kSettingEntryOnlineLocation, kSettingEntryOnlineTillNumber, kSettingEntryOnlineServerEnabled, kSettingEntryOperator, kSettingEntryDisplayName],
                            @[kSettingEntryOfflineServerAddress, kSettingEntryOfflineLocation, kSettingEntryOfflineTillNumber, kSettingEntryOfflineServerEnabled],
                            @[kSettingEntryScanner, kSettingEntryPrinter, kSettingEntryPayment, kSettingEntryPaymentAdditional],
#if DEBUG
                            @[kSettingEntryConfigurationUpdate, kSettingEntryLanguage],
#endif
                            ];
        }
        else
        {
            self.layout = @[
                            @[kSettingEntryOnlineServerAddress, kSettingEntryOnlineLocation, kSettingEntryOnlineTillNumber, kSettingEntryOnlineServerEnabled],
                            @[kSettingEntryOfflineServerAddress, kSettingEntryOfflineLocation, kSettingEntryOfflineTillNumber, kSettingEntryOfflineServerEnabled, kSettingEntryOperator, kSettingEntryDisplayName],
                            @[kSettingEntryScanner, kSettingEntryPrinter, kSettingEntryPayment, kSettingEntryPaymentAdditional],
#if DEBUG
                            @[kSettingEntryLanguage],
#endif
                            ];
        }
    }
    
    // remove settings that are not enabled
    self.layout = [self trimDisabledSettings:self.layout];
}

- (NSArray *) trimDisabledSettings:(NSArray *)layout
{
    NSMutableArray *mutableLayout = [layout mutableCopy];
    
    for (NSUInteger index = 0; index < layout.count; index++) {
        id layoutEntry = layout[index];
        if ([layoutEntry isKindOfClass:[NSArray class]]) {
            NSArray *trimmedLayout = [self trimDisabledSettings:layoutEntry];
            [mutableLayout replaceObjectAtIndex:index
                                     withObject:trimmedLayout];
        } else if ([layoutEntry isEqualToString:kSettingEntryPaymentAdditional]) {
            if (![[OCGEFTManager sharedInstance] providerRequiresAdditionalSetupWithDescription:nil andValue:nil]) {
                [mutableLayout removeObject:layoutEntry];
            }
        } else {
            NSString *settingName = [self settingForLayoutEntry:layoutEntry];
            if (![[OCGSettingsController sharedInstance] enabledCheckForSettingWithName:settingName]) {
                [mutableLayout removeObject:layoutEntry];
            }
        }
    }
    
    return mutableLayout;
}

- (void) handleConfigurationUpdatedNotification:(NSNotification *)note
{
    // add/remove settings that are/are not enabled
    [self buildLayout];
    // reload
    [self.collectionView reloadData];
}

#define MatchRangeAtIndex(match, index, string) ([match rangeAtIndex:index].location != NSNotFound ? [text substringWithRange:[match rangeAtIndex:index]] : nil)

- (NSString *) normalisedURLString:(NSString*)text
{
    NSRegularExpression *regex = nil;
    
    if (regex == nil)
    {
        NSError *error = nil;
        regex = [NSRegularExpression regularExpressionWithPattern:@"^([^:]+://)?(.+?)(:\\d+)?$"
                                                          options:NSRegularExpressionCaseInsensitive
                                                            error:&error];
    }
    
    NSTextCheckingResult* match = [regex firstMatchInString:text options:0 range:NSMakeRange(0, [text length])];
    
    NSString *url = text;
    if (match)
    {
        NSString *scheme = MatchRangeAtIndex(match, 1, text);
        NSString *host = MatchRangeAtIndex(match, 2, text);
        NSString *port = MatchRangeAtIndex(match, 3, text);
        
        if ([scheme length] == 0)
        {
            scheme = @"http://";
        }
        
        if ([port length] == 0)
        {
            port = @":8080";
        }
        
        url = [NSString stringWithFormat:@"%@%@%@", scheme, host, port];
        
    }
    return url;
}

- (void) showOnlineServerAddressDialogWithTitle:(NSString*)title
                                        message:(NSString*)message
{
    NSString *actionButton = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁣󠁃󠁃󠀱󠁯󠁁󠁘󠀳󠁧󠁰󠁫󠁷󠁍󠁭󠁍󠁘󠁬󠁤󠁪󠁘󠁄󠁈󠀱󠁦󠁗󠁬󠁙󠁿*/ @"Set", nil);
    
    NSString *serverAddress = [TestModeController getOnlineServerAddress];
    
    
    _alertView = [[UIAlertView alloc] initWithTitle:title
                                            message:message
                                           delegate:[UIAlertView class]
                                  cancelButtonTitle:([serverAddress length] > 0 ?NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁩󠁢󠁺󠁬󠁏󠁶󠁣󠁧󠁫󠁍󠁋󠁧󠁫󠀱󠁶󠁐󠁐󠀹󠁘󠀴󠁘󠁡󠁕󠁭󠁹󠁷󠀰󠁿*/ @"Cancel", nil):nil)
                                  otherButtonTitles:actionButton, nil];
    
    _alertView.alertViewStyle = UIAlertViewStylePlainTextInput;
    
    __block UITextField *textField = [_alertView textFieldAtIndex:0];
    textField.keyboardType = UIKeyboardTypeURL;
    if ([serverAddress length] > 0) {
        textField.placeholder = serverAddress;
    } else {
        textField.placeholder = @"http://example.com:8080";
    }
    textField.delegate = self;
    
    __weak SettingsTableViewController *weakSelf = self;
    _alertView.dismissBlock = ^(int buttonIndex) {
        weakSelf.currentOnlineLocation = nil;
        [CRSLocationController setOnlineLocationKey:nil];
        [TillSetupController resetOnlineTillNumber];
        if ([textField.text length] > 0) {
            [[OCGSettingsController sharedInstance] setValue:[weakSelf normalisedURLString:textField.text]
                                          forSettingWithName:kTestModeControllerLiveServerConfigKey];
        } else {
            [[OCGSettingsController sharedInstance] removeSettingWithName:kTestModeControllerLiveServerConfigKey];
        }
                
        // create the location data source
        weakSelf.locationDataSource = [[LocationDataSource alloc] init];
        // initialise local cache
        weakSelf.currentOnlineLocation = nil;
        
        NSArray *indexPaths = [weakSelf.layout indexPathsForObjects:@[kSettingEntryOnlineServerAddress, kSettingEntryOnlineLocation, kSettingEntryOnlineTillNumber, kSettingEntryOnlineServerEnabled, kSettingEntryOfflineServerEnabled]];
        
        [weakSelf.collectionView reloadItemsAtIndexPaths:indexPaths];
    };
    
    
    [_alertView show];
}

- (void) showOfflineServerAddressDialogWithTitle:(NSString *)title
                                         message:(NSString *)message
{
    NSString *actionButton = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁣󠁃󠁃󠀱󠁯󠁁󠁘󠀳󠁧󠁰󠁫󠁷󠁍󠁭󠁍󠁘󠁬󠁤󠁪󠁘󠁄󠁈󠀱󠁦󠁗󠁬󠁙󠁿*/ @"Set", nil);
    
    NSString *serverAddress = [TestModeController getOfflineServerAddress];
    
    
    _alertView = [[UIAlertView alloc] initWithTitle:title message:message
                                           delegate:[UIAlertView class]
                                  cancelButtonTitle:([serverAddress length] > 0 ?NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁩󠁢󠁺󠁬󠁏󠁶󠁣󠁧󠁫󠁍󠁋󠁧󠁫󠀱󠁶󠁐󠁐󠀹󠁘󠀴󠁘󠁡󠁕󠁭󠁹󠁷󠀰󠁿*/ @"Cancel", nil):nil)
                                  otherButtonTitles:actionButton, nil];
    
    _alertView.alertViewStyle = UIAlertViewStylePlainTextInput;
    
    __block UITextField *textField = [_alertView textFieldAtIndex:0];
    textField.keyboardType = UIKeyboardTypeURL;
    if ([serverAddress length] > 0) {
        textField.placeholder = serverAddress;
    } else {
        textField.placeholder = @"http://example.com:8080";
    }
    textField.delegate = self;
    
    __weak SettingsTableViewController *weakSelf = self;
    _alertView.dismissBlock = ^(int buttonIndex) {
        weakSelf.currentOfflineLocation = nil;
        [CRSLocationController setOfflineLocationKey:nil];
        [TillSetupController resetOfflineTillNumber];
        if ([textField.text length] > 0) {
            [[OCGSettingsController sharedInstance] setValue:[weakSelf normalisedURLString:textField.text]
                                          forSettingWithName:kTestModeControllerOfflineServerConfigKey];
        } else {
            [[OCGSettingsController sharedInstance] removeSettingWithName:kTestModeControllerOfflineServerConfigKey];
        }
        
        NSArray *indexPaths = [weakSelf.layout indexPathsForObjects:@[kSettingEntryOfflineServerAddress, kSettingEntryOfflineLocation, kSettingEntryOfflineTillNumber, kSettingEntryOnlineServerEnabled]];
        
        [weakSelf.collectionView reloadItemsAtIndexPaths:indexPaths];
    };
    
    
    [_alertView show];
}

#pragma mark - UITableViewController

- (id) init
{
    FNKCollectionViewLayout *layout = [[FNKCollectionViewLayout alloc] init];
    
    self = [super initWithCollectionViewLayout:layout];
    if (self)
    {
    }
    
    return self;
}

- (void) viewDidLoad
{
    [super viewDidLoad];
    
    // create the location data source
    self.locationDataSource = [[LocationDataSource alloc] init];
    // initialise local cache
    self.currentOnlineLocation = nil;
    self.currentOfflineLocation = nil;
    
    _labelReuseIdentifier =
    [OCGCollectionViewCell registerForDetailClass:[UILabel class]
                                          options:OCGCollectionViewCellDetailOptionUseTextLabelBaseline
                                   collectionView:self.collectionView];
    
    _switchReuseIdentifier =
    [OCGCollectionViewCell registerForDetailClass:[UISwitch class]
                                          options:OCGCollectionViewCellDetailOptionNone
                                   collectionView:self.collectionView];
    
    self.collectionView.tag = kTableSkinningTag;
    self.collectionView.delaysContentTouches = NO;
    self.collectionView.backgroundColor = [UIColor colorWithRed:0.937 green:0.937 blue:0.957 alpha:1.000];
}

- (void) viewWillAppear:(BOOL)animated
{
    // localise
    self.title = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁏󠁐󠀵󠁖󠁨󠁭󠁭󠁑󠁁󠁉󠁚󠁁󠁃󠁭󠁚󠁓󠁅󠀵󠁘󠀴󠀱󠁂󠀫󠁤󠀱󠁃󠁯󠁿*/ @"Settings", nil);

    // add the dismiss button
    
    NSString *dismissTitle = nil;
    
    if (BasketController.sharedInstance.credentials)
    {
        dismissTitle = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁡󠁩󠁡󠁍󠁰󠀸󠁷󠁣󠁹󠁩󠁕󠀷󠁪󠁶󠀱󠀸󠁐󠁖󠀵󠁥󠀳󠁫󠁕󠁚󠁄󠁶󠁙󠁿*/ @"Dismiss", nil);
    }
    else
    {
        dismissTitle = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁦󠀯󠁢󠁘󠁈󠁡󠁰󠁥󠁧󠁯󠁥󠁓󠁨󠁁󠁯󠁧󠁧󠀯󠁣󠁧󠁍󠁵󠁑󠁈󠁓󠁥󠁫󠁿*/ @"Sign in", nil);
    }
    
    
    self.dismiss = [[UIBarButtonItem alloc] initWithTitle: dismissTitle
                                                    style: UIBarButtonItemStylePlain
                                                   target: self
                                                   action: @selector(dissmissTapped:)];
    

    
    self.dismiss.tag = kSecondaryButtonSkinningTag;
    
    // add the admin button
    UIBarButtonItem *admin = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁢󠁈󠁫󠁡󠀯󠁳󠀯󠁣󠁺󠁦󠁸󠁥󠁬󠁴󠁂󠁴󠀯󠁆󠁈󠁉󠁡󠁡󠁘󠁯󠀳󠁯󠁍󠁿*/ @"Admin", nil)
                                                              style:UIBarButtonItemStylePlain
                                                             target:self
                                                             action:@selector(adminTapped:)];
    admin.tag = kSecondaryButtonSkinningTag;
    
    // TFS67626 - Only reset adminEnabled when the view is being presented initially
    if (_resetAdmin) {
        self.adminEnabled = BasketController.sharedInstance.credentials == nil || BasketController.sharedInstance.clientSettings.adminPinCode == nil || [[TillSetupController getOnlineTillNumber] length] == 0;
        if (![[BasketController sharedInstance] needsAuthentication])
        {
            self.adminEnabled = [[BasketController sharedInstance] doesUserHavePermissionTo:POSPermissionModifySettings];
        }
        
        if (!self.adminEnabled)
        {
            self.navigationItem.rightBarButtonItem = admin;
        }
    }
    
    _resetAdmin = NO;
    
    [[CRSSkinning currentSkin] applyViewSkin:self];
    
    self.sectionLayout = @[
    kSettingEntryOnlineServer,
    kSettingEntryOfflineServer,
    kSettingEntryPeripherals,
    kSettingEntryConfigurationUpdate,
    ];
    
    [self buildLayout];
    
    [self.collectionView reloadData];
    
    // check if the location is configured
    NSString *serverAddress = [TestModeController getOnlineServerAddress];
    if ([serverAddress length] == 0) {
        // show the initial setup alert
        [self showOnlineServerAddressDialogWithTitle:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁕󠀫󠁃󠁓󠁆󠁗󠀵󠀸󠁡󠀷󠁁󠁂󠁉󠁓󠁃󠁙󠁢󠁡󠁑󠀰󠀶󠁯󠀱󠁐󠁯󠁰󠁁󠁿*/ @"Welcome", nil)
                                       message:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁚󠁬󠁏󠁦󠁉󠁁󠁐󠁦󠁺󠁱󠁸󠁕󠁭󠁭󠁓󠁩󠁡󠁫󠁐󠁔󠁄󠁑󠁏󠁷󠁸󠀯󠀴󠁿*/ @"You need to configure your server address, location and till number.", nil)];
    }

    [super viewWillAppear:animated];
    
    if ([self.navigationController.viewControllers indexOfObject:self] == 0)
    {
        self.navigationItem.leftBarButtonItem = self.dismiss;
    }
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(handleConfigurationUpdatedNotification:)
                                                 name:kBasketControllerConfigurationDidChangeNotification
                                               object:nil];
    
    [self checkView];
}

- (void) viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (BOOL) shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView;
{
    // return the number of settings the user can view/modify
    // one section per setting
    NSInteger result = [self.layout count];
    return result;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    // only one row per setting
    return [(NSArray*)self.layout[section] count];
}

- (NSArray *)collectionView:(UICollectionView *)collectionView
                      layout:(UICollectionViewLayout*)collectionViewLayout
   supplementaryItemsForHeadersAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *title = nil;
    
    if (indexPath.item == 0)
    {
        NSString *entryIdentifier = [self.sectionLayout objectAtIndex:indexPath.section];
        
        if ([entryIdentifier isEqualToString:kSettingEntryOnlineServer])
        {
            title = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁆󠁱󠁐󠀯󠁕󠁸󠁹󠁸󠀶󠁈󠁥󠁇󠁡󠁯󠁭󠁧󠁚󠁶󠁏󠁴󠁴󠁣󠁪󠁦󠁷󠁗󠁅󠁿*/ @"Primary Server", nil);
        }
        else if ([entryIdentifier isEqualToString:kSettingEntryOfflineServer])
        {
            title = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁖󠁯󠁔󠁺󠁏󠁪󠁨󠁉󠁍󠁢󠁰󠁯󠀹󠁡󠀴󠀵󠁩󠁒󠁎󠁱󠁲󠁌󠁘󠁅󠁶󠁦󠁷󠁿*/ @"Secondary Server", nil);
        }
        else if ([entryIdentifier isEqualToString:kSettingEntryPeripherals])
        {
            title = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁆󠁕󠁰󠁨󠀴󠁑󠁤󠁲󠀵󠁰󠁲󠁸󠁰󠁌󠁳󠁢󠀯󠁍󠁷󠁱󠀹󠁷󠁯󠁗󠁑󠁎󠁅󠁿*/ @"Peripherals", nil);
        }
        else if ([entryIdentifier isEqualToString:kSettingEntryConfigurationUpdate])
        {
            title = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁭󠁋󠁴󠁖󠁱󠀷󠁊󠁥󠁷󠁩󠁩󠁥󠁂󠀶󠁹󠁓󠁒󠁯󠁚󠁪󠁬󠁃󠁄󠁌󠀱󠁤󠁧󠁿*/ @"Configuration", nil);
        }
        title = [title uppercaseString];
    }

    if (title)
    {
        FNKSupplementaryItem *result = [[FNKSupplementaryItem alloc] init];
        result.title = title;
        
        return @[result];
    }
    else
    {
        return nil;
    }
}

- (NSArray *)collectionView:(UICollectionView *)collectionView
                      layout:(UICollectionViewLayout*)collectionViewLayout
   supplementaryItemsForFootersAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *title = nil;

    if (indexPath.item == [collectionView numberOfItemsInSection:indexPath.section] - 1)
    {
        
        NSString *entryIdentifier = [self.sectionLayout objectAtIndex:indexPath.section];
        
        title = @" ";
        
        
        if ([entryIdentifier isEqualToString:kSettingEntryConfigurationUpdate]) {
            title = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠀰󠀷󠁩󠁋󠁳󠁨󠁊󠁶󠀷󠁧󠁮󠁡󠁄󠁨󠁌󠁩󠁪󠁋󠁥󠀫󠁕󠁰󠀱󠀹󠁥󠁡󠀰󠁿*/ @"Get the latest menus, tenders, discount and reason codes from the server.", nil);
        }
        
        if (indexPath.section == [self.collectionView numberOfSections] - 1)
        {
            if ([title length] > 0)
            {
                title = [title stringByAppendingString:@"\n\n"];
            }
            
            
            NSString *bundleVersion = NSBundle.mainBundle.infoDictionary[@"CFBundleVersion"];
            
            NSMutableString *version_details = [NSMutableString string];
            
#if !DEBUG
            [version_details appendFormat:@"version %@, ", bundleVersion];
#endif
            
            [version_details appendFormat:@"commit %@", [VERSION_DETAILS_GIT_HASH substringToIndex:7]];
#if DEBUG
            if (VERSION_DETAILS_GIT_HAS_CHANGES)
            {
                [version_details appendString:@" with changes"];
            }
            
            NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
            [dateFormatter setDateStyle: NSDateFormatterShortStyle];
            [dateFormatter setTimeStyle: NSDateFormatterShortStyle];
            
            NSURL *serverURL = OCGDebuggerServerAddress();
            if (serverURL.host.length > 0)
            {
                [version_details appendFormat:@"\nlogging at %@", serverURL];
            }
#endif
            
            title = [title stringByAppendingString:version_details];
        }
    }
    
    if (title)
    {
        FNKSupplementaryItem *result = [[FNKSupplementaryItem alloc] init];
        result.title = title;
        
        return @[result];
    }
    else
    {
        return nil;
    }
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *entryIdentifier = [self.layout objectForIndexPath:indexPath];
    
    
    NSString *reuseIdentifier = _labelReuseIdentifier;
    if ([entryIdentifier isEqualToString:kSettingEntryOnlineServerEnabled] ||
        [entryIdentifier isEqualToString:kSettingEntryOfflineServerEnabled])
    {
        reuseIdentifier = _switchReuseIdentifier;
    }
    
    __block OCGCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:reuseIdentifier forIndexPath:indexPath];
    
    
    if ([cell.detailView respondsToSelector:@selector(text)])
    {
        [cell.detailView performSelector:@selector(text) withObject:@""];
    }
    
    if ([entryIdentifier isEqualToString:kSettingEntryScanner])
    {
        cell.textLabel.text = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁁󠁗󠁙󠁤󠁊󠁄󠀸󠁂󠁥󠁐󠁑󠁆󠁔󠁍󠁍󠁋󠀱󠁪󠀵󠁪󠁰󠀯󠁷󠁑󠁅󠁱󠁧󠁿*/ @"Scanner", nil);
        SMScannerSourceType source = [BarcodeScannerController sharedInstance].sourceType;
        switch (source) {
            case SMScannerSourceTypeExternal:
                cell.detailTextLabel.text = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁥󠀯󠁨󠁔󠁹󠀰󠁇󠀳󠀰󠁘󠁈󠁁󠁋󠁏󠁹󠁢󠁯󠀴󠀴󠁇󠁈󠁯󠁇󠁓󠁲󠁣󠁑󠁿*/ @"External", nil);
                break;
            case SMScannerSourceTypeInternal:
                cell.detailTextLabel.text = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁡󠁚󠁣󠁯󠁲󠁙󠁫󠁊󠁏󠁵󠁨󠁫󠀵󠁘󠁨󠀴󠀸󠁆󠁬󠁍󠁭󠁰󠁧󠀳󠁂󠀷󠁫󠁿*/ @"Camera", nil);
                break;
            case SMScannerSourceTypeManual:
                cell.detailTextLabel.text = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁤󠁲󠁨󠀵󠁯󠁹󠁁󠁺󠁸󠁋󠁺󠁡󠁥󠁎󠁦󠁕󠁓󠁰󠁱󠀷󠁐󠁴󠁎󠁑󠁶󠁥󠁷󠁿*/ @"Manual", nil);
                break;
            default:
                cell.detailTextLabel.text = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠀰󠁐󠁍󠁚󠁇󠁡󠁇󠁡󠁋󠁨󠁔󠁯󠀲󠁶󠁱󠁍󠀲󠁯󠀳󠁙󠁍󠁷󠁪󠀫󠁍󠁪󠁑󠁿*/ @"None", nil);
                cell.detailTextLabel.textColor = VALUE_COLOR_NIL;
                break;
        }
        
        if (self.adminEnabled)
        {
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            cell.selectionStyle = UITableViewCellSelectionStyleBlue;
        }
        else
        {
            cell.accessoryType = UITableViewCellAccessoryNone;
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
        }
    }
    else if ([entryIdentifier isEqualToString:kSettingEntryPrinter])
    {
        cell.textLabel.text = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁚󠁸󠁉󠁭󠁗󠁡󠁏󠁷󠁨󠁃󠁣󠁖󠀱󠀵󠁙󠀯󠁐󠁏󠁒󠁋󠁮󠁔󠁨󠀲󠁌󠁂󠁑󠁿*/ @"Printer", nil);
        // if we are using local client printing
        if ([OCGReceiptManager configuredReceiptManagerType] == ReceiptManagerTypeClient) {
            NSString *printerName = [PrinterController getConfiguredPrinterName];
            if (printerName)
            {
                cell.detailTextLabel.text = printerName;
            }
            else
            {
                cell.detailTextLabel.text = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠀰󠁐󠁍󠁚󠁇󠁡󠁇󠁡󠁋󠁨󠁔󠁯󠀲󠁶󠁱󠁍󠀲󠁯󠀳󠁙󠁍󠁷󠁪󠀫󠁍󠁪󠁑󠁿*/ @"None", nil);
                cell.detailTextLabel.textColor = VALUE_COLOR_NIL;
            }
            
            if (self.adminEnabled)
            {
                cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
                cell.selectionStyle = UITableViewCellSelectionStyleBlue;
                cell.accessoryView = nil;
            }
            else
            {
                cell.accessoryType = UITableViewCellAccessoryNone;
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
                /*
                UILabel *lockLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 100, 40)];
                lockLabel.text = @"🔒";
                lockLabel.baselineAdjustment =  UIBaselineAdjustmentAlignCenters;
                [lockLabel sizeToFit];
                lockLabel.backgroundColor = [UIColor clearColor];
                cell.accessoryView = lockLabel;
                */
            }
        } else {
            // if we are not printing locally
            cell.detailTextLabel.text = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁣󠁏󠁮󠁅󠁳󠁦󠀳󠁌󠁑󠀶󠁊󠁎󠁈󠁦󠁓󠁪󠁬󠁶󠁉󠁤󠁸󠁹󠁓󠁺󠁆󠀫󠁳󠁿*/ @"Remote", nil);
            cell.accessoryType = UITableViewCellAccessoryNone;
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
        }
    } else if ([entryIdentifier isEqualToString:kSettingEntryPayment]) {
        cell.textLabel.text = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁰󠁸󠁹󠁓󠁯󠁺󠁧󠀸󠀷󠀵󠁺󠁆󠁊󠁆󠁕󠁲󠁊󠁢󠁉󠁕󠀱󠁆󠁣󠁈󠀯󠀫󠁳󠁿*/ @"EFT Payment Provider", nil);
        OCGEFTManagerProvider provider = [[OCGEFTManager sharedInstance] getConfiguredProvider];
        NSString *providerName = [[OCGEFTManager sharedInstance] localizedNameForProvider:provider];
        if (providerName) {
            cell.detailTextLabel.text = providerName;
        }
        if (self.adminEnabled)
        {
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            cell.selectionStyle = UITableViewCellSelectionStyleBlue;
            cell.accessoryView = nil;
        }
        else
        {
            cell.accessoryType = UITableViewCellAccessoryNone;
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
        }
    } else if ([entryIdentifier isEqualToString:kSettingEntryPaymentAdditional]) {
        NSString *name = nil;
        NSString *value = nil;
        if ([[OCGEFTManager sharedInstance] providerRequiresAdditionalSetupWithDescription:&name
                                                                                  andValue:&value]) {
            cell.textLabel.text = name;
            cell.detailTextLabel.text = value;
            if (self.adminEnabled)
            {
                cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
                cell.selectionStyle = UITableViewCellSelectionStyleBlue;
                cell.accessoryView = nil;
            }
            else
            {
                cell.accessoryType = UITableViewCellAccessoryNone;
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
            }
        }
    }
    else if ([entryIdentifier isEqualToString:kSettingEntryOnlineLocation])
    {
        cell.textLabel.text = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁱󠁉󠁰󠁄󠀲󠁪󠁥󠁒󠁆󠀯󠁫󠀰󠁭󠁗󠀯󠁳󠁌󠁤󠁊󠁹󠁄󠁣󠁒󠁖󠁕󠁐󠁷󠁿*/ @"Location", nil);
        NSString *key = [CRSLocationController getOnlineLocationKey];
        if (key) {
            if (self.currentOnlineLocation == nil) {
                cell.detailTextLabel.text = [NSString stringWithFormat:@"Location %@", key];
                UIActivityIndicatorView *spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
                [spinner startAnimating];
                cell.accessoryView = spinner;
                
                __weak typeof(self) welf = self;
                // load the actual location
                [self.locationDataSource getLocationSummaryForLocationKey:key
                                                           serviceBaseURL:[TestModeController getOnlineServerAddress] block:^(CRSLocationSummary* data, NSError *error) {
                    if (data && !error) {
                        welf.currentOnlineLocation = data;
                        // reload the cell
                        [welf.collectionView reloadItemsAtIndexPaths:@[ indexPath ]];
                     }
                }];
            } else {
                cell.detailTextLabel.text = self.currentOnlineLocation.name;
                cell.accessoryView = nil;
            }
        } else {
            cell.detailTextLabel.text = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠀰󠁐󠁍󠁚󠁇󠁡󠁇󠁡󠁋󠁨󠁔󠁯󠀲󠁶󠁱󠁍󠀲󠁯󠀳󠁙󠁍󠁷󠁪󠀫󠁍󠁪󠁑󠁿*/ @"None", nil);
            cell.detailTextLabel.textColor = VALUE_COLOR_NIL;
        }
        
        NSString *serverAddress = [TestModeController getOnlineServerAddress];
        
        if ([[BasketController sharedInstance] needsAuthentication] && self.adminEnabled && serverAddress != nil)
        {
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            cell.selectionStyle = UITableViewCellSelectionStyleBlue;
        }
        else
        {
            cell.accessoryType = UITableViewCellAccessoryNone;
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        
        cell.detailRequirement = OCGCollectionViewCellDetailRequirementRequired;
    }
    else if ([entryIdentifier isEqualToString:kSettingEntryOfflineLocation])
    {
        cell.textLabel.text = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁱󠁉󠁰󠁄󠀲󠁪󠁥󠁒󠁆󠀯󠁫󠀰󠁭󠁗󠀯󠁳󠁌󠁤󠁊󠁹󠁄󠁣󠁒󠁖󠁕󠁐󠁷󠁿*/ @"Location", nil);
        NSString *key = [CRSLocationController getOfflineLocationKey];
        if (key) {
            if (self.currentOfflineLocation == nil) {
                cell.detailTextLabel.text = [NSString stringWithFormat:@"Location %@", key];
                UIActivityIndicatorView *spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
                [spinner startAnimating];
                cell.accessoryView = spinner;
                
                __weak typeof(self) welf = self;
                // load the actual location
                [self.locationDataSource getLocationSummaryForLocationKey:key
                                                           serviceBaseURL:[TestModeController getOfflineServerAddress] block:^(CRSLocationSummary* data, NSError *error) {
                                                               if (data && !error) {
                                                                   welf.currentOfflineLocation = data;
                                                                   // reload the cell
                                                                   [welf.collectionView reloadItemsAtIndexPaths:@[ indexPath ]];
                                                               }
                                                           }];
            } else {
                cell.detailTextLabel.text = self.currentOfflineLocation.name;
                cell.accessoryView = nil;
            }
        } else {
            cell.detailTextLabel.text = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠀰󠁐󠁍󠁚󠁇󠁡󠁇󠁡󠁋󠁨󠁔󠁯󠀲󠁶󠁱󠁍󠀲󠁯󠀳󠁙󠁍󠁷󠁪󠀫󠁍󠁪󠁑󠁿*/ @"None", nil);
            cell.detailTextLabel.textColor = VALUE_COLOR_NIL;
        }
        
        NSString *serverAddress = [TestModeController getOfflineServerAddress];
        
        if ([[BasketController sharedInstance] needsAuthentication] && self.adminEnabled && serverAddress != nil)
        {
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            cell.selectionStyle = UITableViewCellSelectionStyleBlue;
        }
        else
        {
            cell.accessoryType = UITableViewCellAccessoryNone;
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
        }
    }
    else if ([entryIdentifier isEqualToString:kSettingEntryOnlineTillNumber])
    {
        cell.textLabel.text = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁃󠀱󠁨󠁡󠁆󠁲󠁗󠁷󠁴󠁉󠀲󠁢󠁋󠀫󠁰󠀷󠁰󠀵󠁮󠁦󠀳󠁷󠁘󠁭󠀹󠁭󠁁󠁿*/ @"Till number", nil);
        NSString *tillNumber = [TillSetupController getOnlineTillNumber];
        if (tillNumber)
        {
            cell.detailTextLabel.text = tillNumber;
        }
        else
        {
            cell.detailTextLabel.text = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠀰󠁐󠁍󠁚󠁇󠁡󠁇󠁡󠁋󠁨󠁔󠁯󠀲󠁶󠁱󠁍󠀲󠁯󠀳󠁙󠁍󠁷󠁪󠀫󠁍󠁪󠁑󠁿*/ @"None", nil);
            cell.detailTextLabel.textColor = VALUE_COLOR_NIL;
        }

        NSString *locationKey = [CRSLocationController getOnlineLocationKey];

        if ([[BasketController sharedInstance] needsAuthentication] && self.adminEnabled && locationKey != nil)
        {
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            cell.selectionStyle = UITableViewCellSelectionStyleBlue;
        }
        else
        {
            cell.accessoryType = UITableViewCellAccessoryNone;
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
        }

        cell.detailRequirement = OCGCollectionViewCellDetailRequirementRequired;
    }
    else if ([entryIdentifier isEqualToString:kSettingEntryOfflineTillNumber])
    {
        cell.textLabel.text = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁃󠀱󠁨󠁡󠁆󠁲󠁗󠁷󠁴󠁉󠀲󠁢󠁋󠀫󠁰󠀷󠁰󠀵󠁮󠁦󠀳󠁷󠁘󠁭󠀹󠁭󠁁󠁿*/ @"Till number", nil);
        NSString *tillNumber = [TillSetupController getOfflineTillNumber];
        if (tillNumber)
        {
            cell.detailTextLabel.text = tillNumber;
        }
        else
        {
            cell.detailTextLabel.text = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠀰󠁐󠁍󠁚󠁇󠁡󠁇󠁡󠁋󠁨󠁔󠁯󠀲󠁶󠁱󠁍󠀲󠁯󠀳󠁙󠁍󠁷󠁪󠀫󠁍󠁪󠁑󠁿*/ @"None", nil);
            cell.detailTextLabel.textColor = VALUE_COLOR_NIL;
        }
        
        NSString *locationKey = [CRSLocationController getOfflineLocationKey];
        
        if ([[BasketController sharedInstance] needsAuthentication] && self.adminEnabled && locationKey != nil)
        {
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            cell.selectionStyle = UITableViewCellSelectionStyleBlue;
        }
        else
        {
            cell.accessoryType = UITableViewCellAccessoryNone;
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
        }
    }
    else if ([entryIdentifier isEqualToString:kSettingEntryConfigurationUpdate])
    {
        cell.textLabel.text = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁡󠁡󠁈󠁔󠁇󠁩󠀲󠁪󠁶󠁔󠁙󠁭󠁲󠁥󠁴󠀱󠁉󠁂󠁂󠁒󠁏󠁉󠁦󠁗󠁷󠁲󠁣󠁿*/ @"Refresh configuration", nil);
        cell.detailTextLabel.text = nil;
        cell.accessoryType = UITableViewCellAccessoryNone;
        cell.selectionStyle = UITableViewCellSelectionStyleBlue;
    }
    else if ([entryIdentifier isEqualToString:kSettingEntryOnlineServerAddress])
    {
        cell.textLabel.text = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠀰󠀵󠁧󠁶󠁲󠀴󠀹󠁣󠁹󠀰󠁦󠀴󠁦󠀴󠁶󠁧󠀹󠁍󠁙󠀷󠁳󠁨󠀸󠁹󠁔󠁗󠁍󠁿*/ @"Address", nil);
        NSString *serverAddress = [TestModeController getOnlineServerAddress];

        if (serverAddress)
        {
            cell.detailTextLabel.text = serverAddress;
        }
        else
        {
            cell.detailTextLabel.text = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠀰󠁐󠁍󠁚󠁇󠁡󠁇󠁡󠁋󠁨󠁔󠁯󠀲󠁶󠁱󠁍󠀲󠁯󠀳󠁙󠁍󠁷󠁪󠀫󠁍󠁪󠁑󠁿*/ @"None", nil);
            cell.detailTextLabel.textColor = VALUE_COLOR_NIL;
        }

        cell.accessoryType = UITableViewCellAccessoryNone;
        
        if ([[BasketController sharedInstance] needsAuthentication] && self.adminEnabled)
        {
            cell.selectionStyle = UITableViewCellSelectionStyleBlue;
        }
        else
        {
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
        }

        cell.detailRequirement = OCGCollectionViewCellDetailRequirementRequired;
    }
    else if ([entryIdentifier isEqualToString:kSettingEntryOfflineServerAddress])
    {
        cell.textLabel.text = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠀰󠀵󠁧󠁶󠁲󠀴󠀹󠁣󠁹󠀰󠁦󠀴󠁦󠀴󠁶󠁧󠀹󠁍󠁙󠀷󠁳󠁨󠀸󠁹󠁔󠁗󠁍󠁿*/ @"Address", nil);
        NSString *serverAddress = [TestModeController getOfflineServerAddress];
        
        if (serverAddress)
        {
            cell.detailTextLabel.text = serverAddress;
        }
        else
        {
            cell.detailTextLabel.text = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠀰󠁐󠁍󠁚󠁇󠁡󠁇󠁡󠁋󠁨󠁔󠁯󠀲󠁶󠁱󠁍󠀲󠁯󠀳󠁙󠁍󠁷󠁪󠀫󠁍󠁪󠁑󠁿*/ @"None", nil);
            cell.detailTextLabel.textColor = VALUE_COLOR_NIL;
        }
        
        cell.accessoryType = UITableViewCellAccessoryNone;
        
        if ([[BasketController sharedInstance] needsAuthentication] && self.adminEnabled)
        {
            cell.selectionStyle = UITableViewCellSelectionStyleBlue;
        }
        else
        {
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
        }
    }
    else if ([entryIdentifier isEqualToString:kSettingEntryOperator])
    {
        cell.textLabel.text = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁨󠁣󠁖󠁹󠁈󠀵󠁭󠁊󠀰󠁁󠁗󠁭󠁶󠁬󠁔󠁶󠀷󠁲󠀷󠁈󠁱󠁣󠀸󠁭󠁦󠁚󠁁󠁿*/ @"Operator", nil);
        cell.detailTextLabel.text = BasketController.sharedInstance.credentials.username;
         
        cell.accessoryType = UITableViewCellAccessoryNone;
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    else if ([entryIdentifier isEqualToString:kSettingEntryDisplayName])
    {
        cell.textLabel.text = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁍󠀹󠁘󠁴󠁭󠁰󠁇󠁎󠀫󠁂󠁗󠁖󠁢󠁸󠁭󠁙󠁮󠁕󠀴󠁢󠀲󠁗󠀸󠁱󠀫󠁵󠀸󠁿*/ @"Operator Name", nil);
        cell.detailTextLabel.text = BasketController.sharedInstance.credentials.displayName;
        
        cell.accessoryType = UITableViewCellAccessoryNone;
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    } else if ([entryIdentifier isEqualToString:kSettingEntryOnlineServerEnabled] ||
               [entryIdentifier isEqualToString:kSettingEntryOfflineServerEnabled]) {
        
        cell.accessoryType = UITableViewCellAccessoryNone;
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        cell.textLabel.text = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠀯󠁳󠁢󠁮󠁸󠁖󠁹󠁋󠁎󠁊󠁶󠀹󠀳󠀱󠁪󠁊󠁪󠁖󠁳󠁙󠁲󠀸󠁆󠁈󠁪󠁑󠁯󠁿*/ @"Enabled", nil);
        
        UISwitch *detailSwitch = (UISwitch*) cell.detailView;
        
        // Ensure an previous addTargets are removed.
        [detailSwitch removeTarget:self
                         action:@selector(switchDetailChanged:)
               forControlEvents:UIControlEventValueChanged];

        [detailSwitch addTarget:self
                         action:@selector(switchDetailChanged:)
               forControlEvents:UIControlEventValueChanged];
        
        [self updateSwitchDetail:detailSwitch entryIdentifier:entryIdentifier];
    }
    else if ([entryIdentifier isEqualToString:kSettingEntryLanguage])
    {
        cell.textLabel.text = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁡󠁗󠁁󠀹󠁹󠁆󠁖󠀱󠁄󠁋󠁴󠁦󠁂󠁭󠁰󠁦󠀶󠁰󠁉󠁌󠁥󠁲󠁓󠁹󠁬󠁂󠁯󠁿*/ @"Language", nil);

        NSLocale *runtimeLocale = [NSLocale OCGAdditions_runtimeLocale];
        
        cell.selectionStyle = UITableViewCellSelectionStyleBlue;
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        
        if (runtimeLocale)
        {
            cell.detailTextLabel.text = [runtimeLocale displayNameForKey:NSLocaleIdentifier value:[[NSLocale currentLocale] localeIdentifier]];
        }
        else
        {
            cell.detailTextLabel.text = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠀰󠁐󠁍󠁚󠁇󠁡󠁇󠁡󠁋󠁨󠁔󠁯󠀲󠁶󠁱󠁍󠀲󠁯󠀳󠁙󠁍󠁷󠁪󠀫󠁍󠁪󠁑󠁿*/ @"None", nil);
            cell.detailTextLabel.textColor = VALUE_COLOR_NIL;
        }
    }
    else
    {
        NSLog(@"WTF!?");
    }
    
    // turn off selection and any accessory for centrally managed settings
    if (![self isSettingLocallyConfigured:entryIdentifier]) {
        cell.accessoryType = UITableViewCellAccessoryNone;
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    
    cell.tag = kTableCellSkinningTag;
    cell.textLabel.tag = kTableCellKeySkinningTag;
    cell.detailView.tag = kTableCellValueSkinningTag;
    cell.backgroundColor = [UIColor whiteColor];

    [[CRSSkinning currentSkin] applyViewSkin:cell
                             withTagOverride:cell.tag];
    
    return cell;
}

#pragma mark - UITableViewDelegate

- (BOOL) collectionView:(UICollectionView *)collectionView shouldSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    BOOL result = YES;
    
    NSString *entryIdentifier = [self.layout objectForIndexPath:indexPath];
    
    result = [self isSettingLocallyConfigured:entryIdentifier];
    
    if ([entryIdentifier isEqualToString:kSettingEntryPrinter]) {
        if ([OCGReceiptManager configuredReceiptManagerType] == ReceiptManagerTypeServer) {
            result = NO;
        }
    }
    
    return result;
}

- (void) collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *entryIdentifier = [self.layout objectForIndexPath:indexPath];
    
    if ([entryIdentifier isEqualToString:kSettingEntryScanner] && self.adminEnabled)
    {
        // navigate to the scanner setup table
        ScannerSettingTableViewController *scannerSettings =
        [[ScannerSettingTableViewController alloc] initWithStyle:UITableViewStyleGrouped];
        scannerSettings.delegate = self;
        [self.navigationController pushViewController:scannerSettings
                                             animated:YES];
    }
    else if ([entryIdentifier isEqualToString:kSettingEntryPrinter] && self.adminEnabled)
    {
        // navigate to the printer setup table
        PrinterSettingTableViewController *printerSettings =
        [[PrinterSettingTableViewController alloc] initWithStyle:UITableViewStyleGrouped];
        printerSettings.delegate = self;
        [self.navigationController pushViewController:printerSettings
                                             animated:YES];
    }
    else if ([entryIdentifier isEqualToString:kSettingEntryPayment] && self.adminEnabled)
    {
        // navigate to the payment provider setup table
        OCGSelectViewController *selectViewController = [[OCGSelectViewController alloc] init];
        
        selectViewController.options = @[[OCGEFTManager sharedInstance].supportedPaymentProviders];
        
        selectViewController.titleForOption = ^(OCGSelectViewController *selectViewController, NSIndexPath *indexPath)
        {
            NSArray *providers = [OCGEFTManager sharedInstance].supportedPaymentProviders;
            NSNumber *provider = providers[indexPath.row];
            return [[OCGEFTManager sharedInstance] localizedNameForProvider:provider.integerValue];
        };
        
        selectViewController.didSelectOption = ^(OCGSelectViewController *selectViewController, NSIndexPath *indexPath)
        {
            [self.navigationController popViewControllerAnimated:YES];
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                // save the new provider
                NSArray *providers = [OCGEFTManager sharedInstance].supportedPaymentProviders;
                NSNumber *provider = providers[indexPath.row];
                [[OCGEFTManager sharedInstance] setConfiguredProvider:provider.integerValue];
                
                // rebuild the layout in case it has changed
                [self buildLayout];
                [self.collectionView reloadData];
            });
        };
        
        selectViewController.title = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁰󠁸󠁹󠁓󠁯󠁺󠁧󠀸󠀷󠀵󠁺󠁆󠁊󠁆󠁕󠁲󠁊󠁢󠁉󠁕󠀱󠁆󠁣󠁈󠀯󠀫󠁳󠁿*/ @"EFT Payment Provider", nil);
        [self.navigationController pushViewController:selectViewController
                                             animated:YES];
    }
    else if ([entryIdentifier isEqualToString:kSettingEntryPaymentAdditional] && self.adminEnabled) {
        [[OCGEFTManager sharedInstance] additionalProviderSetupComplete:^{
            [self.collectionView reloadItemsAtIndexPaths:[self.layout indexPathsForObject:entryIdentifier]];
        }];
    }
    else if ([entryIdentifier isEqualToString:kSettingEntryOnlineLocation])
    {
        NSString *serverAddress = [TestModeController getOnlineServerAddress];
        
        if ([[BasketController sharedInstance] needsAuthentication] && self.adminEnabled && [serverAddress length] > 0)
        {
            OCGSelectViewController *selectViewController = [[OCGSelectViewController alloc] init];
                        
            selectViewController.title = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁱󠁉󠁰󠁄󠀲󠁪󠁥󠁒󠁆󠀯󠁫󠀰󠁭󠁗󠀯󠁳󠁌󠁤󠁊󠁹󠁄󠁣󠁒󠁖󠁕󠁐󠁷󠁿*/ @"Location", nil);
            
            selectViewController.defaultMessage = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁹󠁔󠁨󠁍󠁂󠁫󠀫󠁪󠀯󠁣󠁕󠁦󠁖󠁋󠁧󠁶󠁙󠁰󠁣󠁹󠁯󠀳󠁚󠁪󠁨󠁈󠁕󠁿*/ @"Searching for locations", nil);
            
            selectViewController.titleForOption = ^(OCGSelectViewController *selectViewController, NSIndexPath *indexPath)
            {
                CRSLocationSummary *location = [selectViewController.options objectForIndexPath:indexPath];
                return location.name;
            };
            
            selectViewController.didSelectOption = ^(OCGSelectViewController *selectViewController, NSIndexPath *indexPath)
            {
                CRSLocationSummary *location = [selectViewController.options objectForIndexPath:indexPath];
                self.currentOnlineLocation = location;
                [CRSLocationController setOnlineLocationKey:location.key];
                [TillSetupController resetOnlineTillNumber];
                [self.navigationController popViewControllerAnimated:YES];
            };
            [self.navigationController pushViewController:selectViewController
                                                 animated:YES];
            
            [self.locationDataSource getLocationListForServiceBaseURL:serverAddress
                                                                block:^(NSMutableArray *locations, NSError *error) {
                                                                    if ([locations count] > 0)
                                                                    {
                                                                        selectViewController.options = @[locations];
                                                                    }
                                                                    else
                                                                    {
                                                                        selectViewController.defaultMessage = [error localizedDescription];
                                                                    }
                                                                }];
        }
    }
    else if ([entryIdentifier isEqualToString:kSettingEntryOfflineLocation])
    {
        NSString *serverAddress = [TestModeController getOfflineServerAddress];
        
        if ([[BasketController sharedInstance] needsAuthentication] && self.adminEnabled && [serverAddress length] > 0)
        {
            OCGSelectViewController *selectViewController = [[OCGSelectViewController alloc] init];
            
            selectViewController.title = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁱󠁉󠁰󠁄󠀲󠁪󠁥󠁒󠁆󠀯󠁫󠀰󠁭󠁗󠀯󠁳󠁌󠁤󠁊󠁹󠁄󠁣󠁒󠁖󠁕󠁐󠁷󠁿*/ @"Location", nil);
                        
            selectViewController.defaultMessage = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁹󠁔󠁨󠁍󠁂󠁫󠀫󠁪󠀯󠁣󠁕󠁦󠁖󠁋󠁧󠁶󠁙󠁰󠁣󠁹󠁯󠀳󠁚󠁪󠁨󠁈󠁕󠁿*/ @"Searching for locations", nil);
            
            selectViewController.titleForOption = ^(OCGSelectViewController *selectViewController, NSIndexPath *indexPath)
            {
                CRSLocationSummary *location = [selectViewController.options objectForIndexPath:indexPath];
                return location.name;
            };
            
            selectViewController.didSelectOption = ^(OCGSelectViewController *selectViewController, NSIndexPath *indexPath)
            {
                CRSLocationSummary *location = [selectViewController.options objectForIndexPath:indexPath];
                self.currentOfflineLocation = location;
                [CRSLocationController setOfflineLocationKey:location.key];
                [TillSetupController resetOfflineTillNumber];
                [self.navigationController popViewControllerAnimated:YES];
            };
            [self.navigationController pushViewController:selectViewController
                                                 animated:YES];
            
            [self.locationDataSource getLocationListForServiceBaseURL:serverAddress
                                                                block:^(NSMutableArray *locations, NSError *error) {
                                                                    if ([locations count] > 0)
                                                                    {
                                                                        selectViewController.options = @[locations];
                                                                    }
                                                                    else
                                                                    {
                                                                        selectViewController.defaultMessage = [error localizedDescription];
                                                                    }
                                                                }];
        }
    }
    else if ([entryIdentifier isEqualToString:kSettingEntryOnlineTillNumber])
    {
        if ([[BasketController sharedInstance] needsAuthentication] && self.adminEnabled && [CRSLocationController getOnlineLocationKey] > 0)
        {
            if (self.currentOnlineLocation.tillNumbers != nil)
            {
                OCGSelectViewController *selectViewController = [[OCGSelectViewController alloc] init];
                
                selectViewController.options = @[self.currentOnlineLocation.tillNumbers];
                
                selectViewController.titleForOption = ^(OCGSelectViewController *selectViewController, NSIndexPath *indexPath)
                {
                    Till *till = [selectViewController.options objectForIndexPath:indexPath];
                    return till.till;
                };
                
                selectViewController.didSelectOption = ^(OCGSelectViewController *selectViewController, NSIndexPath *indexPath)
                {
                    // update the cell
                    NSArray *indexPaths = [self.layout indexPathsForObjects:@[kSettingEntryOnlineLocation, kSettingEntryOnlineTillNumber]];
                    
                    Till *till = [selectViewController.options objectForIndexPath:indexPath];
                    [OCGProgressOverlay show];
                    [TillSetupController setDeviceProfileTillNumber:till.till
                                           locationKey:[CRSLocationController getOnlineLocationKey]
                                        serviceBaseURL:[TestModeController getOnlineServerAddress]
                                              complete:^(NSError *error, ServerError *serverError) {
                                                  [OCGProgressOverlay hide];
                                                  if (error || serverError) {
                                                      UIAlertView *alert =
                                                      [[UIAlertView alloc] initWithTitle:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁌󠁶󠁏󠁥󠁫󠀵󠀹󠁯󠀱󠁈󠁋󠁒󠁚󠀯󠁶󠁺󠁊󠁲󠁒󠀯󠁣󠁨󠀹󠀫󠁲󠁺󠁷󠁿*/ @"Till Number", nil)
                                                                                 message:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁐󠀶󠁈󠁯󠀷󠀸󠁒󠁩󠁐󠁎󠁬󠁥󠁍󠀲󠁬󠀰󠁑󠁚󠁨󠀰󠀹󠁂󠁹󠁺󠁍󠁴󠁉󠁿*/ @"Error saving till number.", nil)
                                                                                delegate:nil
                                                                       cancelButtonTitle:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁄󠁴󠁍󠁉󠁣󠀫󠁺󠁔󠁗󠁦󠁹󠁡󠁯󠀷󠁨󠁔󠁩󠀸󠁯󠀫󠁨󠁘󠀹󠁱󠁌󠀶󠀸󠁿*/ @"OK", nil)
                                                                       otherButtonTitles:nil];
                                                      [alert show];
                                                  } else {
                                                      [TillSetupController setOnlineTillNumber:till.till];
                                                      [self.collectionView reloadItemsAtIndexPaths:indexPaths];
                                                      
                                                      [self checkView];
                                                      // dismiss the controller
                                                      [self.navigationController popViewControllerAnimated:YES];
                                                      // reload configuration
                                                      [[BasketController sharedInstance] refreshConfiguration:YES];
                                                  }
                                              }];
                };
                
                selectViewController.title = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁃󠀱󠁨󠁡󠁆󠁲󠁗󠁷󠁴󠁉󠀲󠁢󠁋󠀫󠁰󠀷󠁰󠀵󠁮󠁦󠀳󠁷󠁘󠁭󠀹󠁭󠁁󠁿*/ @"Till number", nil);
                [self.navigationController pushViewController:selectViewController
                                                     animated:YES];
            }
            else
            {
                [self.collectionView deselectItemAtIndexPath:indexPath animated:YES];
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁃󠀱󠁨󠁡󠁆󠁲󠁗󠁷󠁴󠁉󠀲󠁢󠁋󠀫󠁰󠀷󠁰󠀵󠁮󠁦󠀳󠁷󠁘󠁭󠀹󠁭󠁁󠁿*/ @"Till number", nil)
                                                                    message:@""
                                                                   delegate:[UIAlertView class]
                                                          cancelButtonTitle:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁩󠁢󠁺󠁬󠁏󠁶󠁣󠁧󠁫󠁍󠁋󠁧󠁫󠀱󠁶󠁐󠁐󠀹󠁘󠀴󠁘󠁡󠁕󠁭󠁹󠁷󠀰󠁿*/ @"Cancel", nil)
                                                          otherButtonTitles:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁣󠁃󠁃󠀱󠁯󠁁󠁘󠀳󠁧󠁰󠁫󠁷󠁍󠁭󠁍󠁘󠁬󠁤󠁪󠁘󠁄󠁈󠀱󠁦󠁗󠁬󠁙󠁿*/ @"Set", nil), nil];
                
                alertView.alertViewStyle = UIAlertViewStylePlainTextInput;
                
                __block UITextField *textField = [alertView textFieldAtIndex:0];
                textField.keyboardType = UIKeyboardTypeURL;
                textField.placeholder = [TillSetupController getOnlineTillNumber];
                /*
                if ([serverAddress length] > 0)
                {
                    textField.placeholder = serverAddress;
                }
                else
                {
                    textField.placeholder = @"http://example.com:8080";
                }
                */
                alertView.dismissBlock = ^(int buttonIndex)
                {
                    NSString *tillNumber = textField.text;
                    [OCGProgressOverlay show];
                    [TillSetupController setDeviceProfileTillNumber:tillNumber
                                           locationKey:[CRSLocationController getOnlineLocationKey]
                                        serviceBaseURL:[TestModeController getOnlineServerAddress]
                                              complete:^(NSError *error, ServerError *serverError) {
                                                  [OCGProgressOverlay hide];
                                                  if (error || serverError) {
                                                      UIAlertView *alert =
                                                      [[UIAlertView alloc] initWithTitle:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁌󠁶󠁏󠁥󠁫󠀵󠀹󠁯󠀱󠁈󠁋󠁒󠁚󠀯󠁶󠁺󠁊󠁲󠁒󠀯󠁣󠁨󠀹󠀫󠁲󠁺󠁷󠁿*/ @"Till Number", nil)
                                                                                 message:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁐󠀶󠁈󠁯󠀷󠀸󠁒󠁩󠁐󠁎󠁬󠁥󠁍󠀲󠁬󠀰󠁑󠁚󠁨󠀰󠀹󠁂󠁹󠁺󠁍󠁴󠁉󠁿*/ @"Error saving till number.", nil)
                                                                                delegate:nil
                                                                       cancelButtonTitle:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁄󠁴󠁍󠁉󠁣󠀫󠁺󠁔󠁗󠁦󠁹󠁡󠁯󠀷󠁨󠁔󠁩󠀸󠁯󠀫󠁨󠁘󠀹󠁱󠁌󠀶󠀸󠁿*/ @"OK", nil)
                                                                       otherButtonTitles:nil];
                                                      [alert show];
                                                  } else {
                                                      [TillSetupController setOnlineTillNumber:tillNumber];
                                                      NSArray *indexPaths = [self.layout indexPathsForObjects:@[kSettingEntryOnlineLocation, kSettingEntryOnlineTillNumber, kSettingEntryOnlineServerEnabled, kSettingEntryOfflineServerEnabled]];

                                                      [self.collectionView reloadItemsAtIndexPaths:indexPaths];
                                                      
                                                      [self checkView];

                                                  }
                                              }];                };
                
                
                [alertView show];
            }
        }
    }
    else if ([entryIdentifier isEqualToString:kSettingEntryOfflineTillNumber])
    {
        if ([[BasketController sharedInstance] needsAuthentication] && self.adminEnabled && [CRSLocationController getOfflineLocationKey] > 0)
        {
            if (self.currentOfflineLocation.tillNumbers != nil)
            {
                OCGSelectViewController *selectViewController = [[OCGSelectViewController alloc] init];
                
                selectViewController.options = @[self.currentOfflineLocation.tillNumbers];
                
                selectViewController.titleForOption = ^(OCGSelectViewController *selectViewController, NSIndexPath *indexPath)
                {
                    Till *till = [selectViewController.options objectForIndexPath:indexPath];
                    return till.till;
                };
                
                selectViewController.didSelectOption = ^(OCGSelectViewController *selectViewController, NSIndexPath *indexPath)
                {
                    // update the cell
                    NSArray *indexPaths = [self.layout indexPathsForObjects:@[kSettingEntryOfflineLocation, kSettingEntryOfflineTillNumber, kSettingEntryOfflineServerEnabled, kSettingEntryOnlineServerEnabled]];
                    
                    Till *till = [selectViewController.options objectForIndexPath:indexPath];
                    [OCGProgressOverlay show];
                    [TillSetupController setDeviceProfileTillNumber:till.till
                                           locationKey:[CRSLocationController getOfflineLocationKey]
                                        serviceBaseURL:[TestModeController getOfflineServerAddress]
                                              complete:^(NSError *error, ServerError *serverError) {
                                                  [OCGProgressOverlay hide];
                                                  if (error || serverError) {
                                                      UIAlertView *alert =
                                                      [[UIAlertView alloc] initWithTitle:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁌󠁶󠁏󠁥󠁫󠀵󠀹󠁯󠀱󠁈󠁋󠁒󠁚󠀯󠁶󠁺󠁊󠁲󠁒󠀯󠁣󠁨󠀹󠀫󠁲󠁺󠁷󠁿*/ @"Till Number", nil)
                                                                                 message:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁐󠀶󠁈󠁯󠀷󠀸󠁒󠁩󠁐󠁎󠁬󠁥󠁍󠀲󠁬󠀰󠁑󠁚󠁨󠀰󠀹󠁂󠁹󠁺󠁍󠁴󠁉󠁿*/ @"Error saving till number.", nil)
                                                                                delegate:nil
                                                                       cancelButtonTitle:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁄󠁴󠁍󠁉󠁣󠀫󠁺󠁔󠁗󠁦󠁹󠁡󠁯󠀷󠁨󠁔󠁩󠀸󠁯󠀫󠁨󠁘󠀹󠁱󠁌󠀶󠀸󠁿*/ @"OK", nil)
                                                                       otherButtonTitles:nil];
                                                      [alert show];
                                                  } else {
                                                      [TillSetupController setOfflineTillNumber:till.till];
                                                      [self.collectionView reloadItemsAtIndexPaths:indexPaths];
                                                      
                                                      [self checkView];
                                                      // dismiss the controller
                                                      [self.navigationController popViewControllerAnimated:YES];
//                                                      // reload configuration (why? this just reloads the online config)
//                                                      [[BasketController sharedInstance] refreshConfiguration:YES];
                                                  }
                                              }];
                };
                
                selectViewController.title = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁃󠀱󠁨󠁡󠁆󠁲󠁗󠁷󠁴󠁉󠀲󠁢󠁋󠀫󠁰󠀷󠁰󠀵󠁮󠁦󠀳󠁷󠁘󠁭󠀹󠁭󠁁󠁿*/ @"Till number", nil);
                [self.navigationController pushViewController:selectViewController
                                                     animated:YES];
            }
            else
            {
                [self.collectionView deselectItemAtIndexPath:indexPath animated:YES];
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁃󠀱󠁨󠁡󠁆󠁲󠁗󠁷󠁴󠁉󠀲󠁢󠁋󠀫󠁰󠀷󠁰󠀵󠁮󠁦󠀳󠁷󠁘󠁭󠀹󠁭󠁁󠁿*/ @"Till number", nil) message:@""  delegate:[UIAlertView class] cancelButtonTitle:@"Cancel" otherButtonTitles:@"Set", nil];
                
                alertView.alertViewStyle = UIAlertViewStylePlainTextInput;
                
                __block UITextField *textField = [alertView textFieldAtIndex:0];
                textField.keyboardType = UIKeyboardTypeURL;
                textField.placeholder = [TillSetupController getOfflineTillNumber];
                /*
                 if ([serverAddress length] > 0)
                 {
                 textField.placeholder = serverAddress;
                 }
                 else
                 {
                 textField.placeholder = @"http://example.com:8080";
                 }
                 */
                alertView.dismissBlock = ^(int buttonIndex)
                {
                    NSString *tillNumber = textField.text;
                    [OCGProgressOverlay show];
                    [TillSetupController setDeviceProfileTillNumber:tillNumber
                                           locationKey:[CRSLocationController getOfflineLocationKey]
                                        serviceBaseURL:[TestModeController getOfflineServerAddress]
                                              complete:^(NSError *error, ServerError *serverError) {
                                                  [OCGProgressOverlay hide];
                                                  if (error || serverError) {
                                                      UIAlertView *alert =
                                                      [[UIAlertView alloc] initWithTitle:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁌󠁶󠁏󠁥󠁫󠀵󠀹󠁯󠀱󠁈󠁋󠁒󠁚󠀯󠁶󠁺󠁊󠁲󠁒󠀯󠁣󠁨󠀹󠀫󠁲󠁺󠁷󠁿*/ @"Till Number", nil)
                                                                                 message:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁐󠀶󠁈󠁯󠀷󠀸󠁒󠁩󠁐󠁎󠁬󠁥󠁍󠀲󠁬󠀰󠁑󠁚󠁨󠀰󠀹󠁂󠁹󠁺󠁍󠁴󠁉󠁿*/ @"Error saving till number.", nil)
                                                                                delegate:nil
                                                                       cancelButtonTitle:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁄󠁴󠁍󠁉󠁣󠀫󠁺󠁔󠁗󠁦󠁹󠁡󠁯󠀷󠁨󠁔󠁩󠀸󠁯󠀫󠁨󠁘󠀹󠁱󠁌󠀶󠀸󠁿*/ @"OK", nil)
                                                                       otherButtonTitles:nil];
                                                      [alert show];
                                                  } else {
                                                      [TillSetupController setOfflineTillNumber:tillNumber];
                                                      NSArray *indexPaths = [self.layout indexPathsForObjects:@[kSettingEntryOfflineLocation, kSettingEntryOfflineTillNumber, kSettingEntryOfflineServerEnabled, kSettingEntryOnlineServerEnabled]];
                                                      
                                                      [self.collectionView reloadItemsAtIndexPaths:indexPaths];
                                                      
                                                      [self checkView];
                                                      
                                                  }
                                              }];                };
                
                
                [alertView show];
            }
        }
    }
    else if ([entryIdentifier isEqualToString:kSettingEntryOnlineServerAddress])
    {
        [self.collectionView deselectItemAtIndexPath:indexPath animated:YES];
        if ([[BasketController sharedInstance] needsAuthentication] && self.adminEnabled)
        {
            [self showOnlineServerAddressDialogWithTitle:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁦󠁪󠀳󠁶󠁌󠁩󠁳󠁩󠁲󠀫󠁡󠀳󠀴󠀶󠀱󠁊󠁋󠀱󠁖󠁢󠀳󠁙󠁎󠁓󠁵󠁳󠁫󠁿*/ @"Server address", nil)
                                                 message:nil];
        }
    }
    else if ([entryIdentifier isEqualToString:kSettingEntryOfflineServerAddress])
    {
        [self.collectionView deselectItemAtIndexPath:indexPath animated:YES];
        if ([[BasketController sharedInstance] needsAuthentication] && self.adminEnabled)
        {
            [self showOfflineServerAddressDialogWithTitle:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁦󠁪󠀳󠁶󠁌󠁩󠁳󠁩󠁲󠀫󠁡󠀳󠀴󠀶󠀱󠁊󠁋󠀱󠁖󠁢󠀳󠁙󠁎󠁓󠁵󠁳󠁫󠁿*/ @"Server address", nil)
                                                  message:nil];
        }
    }
    else if ([entryIdentifier isEqualToString:kSettingEntryConfigurationUpdate]) {
        [collectionView deselectItemAtIndexPath:indexPath animated:YES];
        [[BasketController sharedInstance] forceServerConfigurationCacheRefresh];
    } else if ([entryIdentifier isEqualToString:kSettingEntryOnlineServerEnabled] ||
               [entryIdentifier isEqualToString:kSettingEntryOfflineServerEnabled]) {
        [collectionView deselectItemAtIndexPath:indexPath
                                       animated:NO];
    }
    else if ([entryIdentifier isEqualToString:kSettingEntryLanguage])
    {
        OCGSelectViewController *selectViewController = [[OCGSelectViewController alloc] init];
        
        NSArray *fileNames = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:[[NSBundle mainBundle] resourcePath] error:NULL];
        NSMutableArray *builtInLanguages = [NSMutableArray array];
        for (NSString *fileName in fileNames)
        {
            if ([[fileName pathExtension] isEqual:@"lproj"])
            {
                [builtInLanguages addObject:[fileName stringByDeletingPathExtension]];
            }
        }
     
        NSLocale *locale = [NSLocale currentLocale];

        [builtInLanguages sortUsingComparator:^(id obj1, id obj2){
            if ([[[NSLocale currentLocale] localeIdentifier] isEqual:obj1])
            {
                return NSOrderedAscending;
            }
            if ([[[NSLocale currentLocale] localeIdentifier] isEqual:obj2])
            {
                return NSOrderedDescending;
            }
            
            NSString *dsp1 = [locale displayNameForKey:NSLocaleIdentifier value:obj1];
            NSString *dsp2 = [locale displayNameForKey:NSLocaleIdentifier value:obj2];
            return [[locale displayNameForKey:NSLocaleIdentifier value:obj1] compare:[locale displayNameForKey:NSLocaleIdentifier value:obj2]];
        }];
        
        selectViewController.options = @[builtInLanguages];
        
        selectViewController.titleForOption = ^(OCGSelectViewController *selectViewController, NSIndexPath *indexPath)
        {
            NSString *languageIdentifier = [selectViewController.options objectForIndexPath:indexPath];
            NSString *displayName = [locale displayNameForKey:NSLocaleIdentifier value:languageIdentifier];
            return displayName ?: languageIdentifier;
        };
        
        selectViewController.didSelectOption = ^(OCGSelectViewController *selectViewController, NSIndexPath *indexPath)
        {
            NSString *runtimeLocaleIdentifier = [selectViewController.options objectForIndexPath:indexPath];
            NSLocale *runtimeLocale = [[NSLocale alloc] initWithLocaleIdentifier:runtimeLocaleIdentifier];
            [NSLocale OCGAdditions_setRuntimeLocale:runtimeLocale];
            [selectViewController.navigationController popViewControllerAnimated:YES];
        };
        
        selectViewController.title = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁡󠁗󠁁󠀹󠁹󠁆󠁖󠀱󠁄󠁋󠁴󠁦󠁂󠁭󠁰󠁦󠀶󠁰󠁉󠁌󠁥󠁲󠁓󠁹󠁬󠁂󠁯󠁿*/ @"Language", nil);
        [self.navigationController pushViewController:selectViewController
                                             animated:YES];

    }
}

#pragma mark - ScannerSettingTableViewControllerDelegate

- (void) scannerSettingTable:(ScannerSettingTableViewController *)vc
            didChangeScanner:(SMScannerSourceType)newType
{
    // update the current scanner source
    [BarcodeScannerController sharedInstance].sourceType = newType;
    // update the cell
    NSArray *indexPaths = [self.layout indexPathsForObjects:@[kSettingEntryScanner]];
    
    [self.collectionView reloadItemsAtIndexPaths:indexPaths];
    
    if (newType != SMScannerSourceTypeExternal) {
        // dismiss the controller
        [self.navigationController popViewControllerAnimated:YES];
    }
}

- (void) scannerSettingTable:(ScannerSettingTableViewController *)vc
      didChangeScannerDevice:(OCGPeripheralManagerDevice)newType
{
    // update the current scanner device
    [[OCGPeripheralManager sharedInstance] setConfiguredDevice:newType];
    // dismiss the controller
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - PrinterSettingTableViewControllerDelegate

- (void) printerSettingTable:(PrinterSettingTableViewController *)tableVC
            didSelectPrinter:(PRINTER_TYPE)newPrinter
{
    // update the printer cell
    NSArray *indexPaths = [self.layout indexPathsForObjects:@[kSettingEntryPrinter]];
    
    [self.collectionView reloadItemsAtIndexPaths:indexPaths];
}

- (void) printerSettingTable:(PrinterSettingTableViewController *)tableVC
            didSetConnection:(NSString *)connection
{
    // update the printer cell
    NSArray *indexPaths = [self.layout indexPathsForObjects:@[kSettingEntryPrinter]];
    
    [self.collectionView reloadItemsAtIndexPaths:indexPaths];
}

#pragma mark - UITextFieldDelegate

- (BOOL) textFieldShouldReturn:(UITextField *)textField
{
    if (_alertView) {
        [_alertView dismissWithClickedButtonIndex:1
                                         animated:YES];
    }
    [textField resignFirstResponder];
    return NO;
}

#pragma mark - Actions

/** @brief Called whenever the user wants to dismiss the currenly shown view controller
 *  @param sender Reference to the object that triggered the event.
 */
- (void) dissmissTapped:(id)sender
{
    if (self.delegate != nil) {
        if ([self.delegate respondsToSelector:@selector(dismissSettingsTableViewController:)]) {
            [self.delegate dismissSettingsTableViewController: self];
        }
    }
}

/** @brief User tapped the admin button. 
 */
- (void) adminTapped:(id)sender
{
    if (self.adminEnabled)
    {
        self.adminEnabled = NO;
    }
    else if (BasketController.sharedInstance.clientSettings.adminPinCode == nil)
    {
        self.adminEnabled = YES;
    }
    else
    {
        // show PIN alert
        UIAlertView *pinAlert =
        [[UIAlertView alloc] initWithTitle:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁱󠁰󠁮󠁒󠁁󠁵󠀰󠁂󠁰󠁨󠁱󠁬󠁤󠁯󠁵󠀸󠁑󠁷󠀱󠁮󠁫󠀴󠀷󠁗󠀴󠁡󠁧󠁿*/ @"PIN required", nil)
                                   message:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁴󠁁󠁧󠁆󠁮󠁨󠁄󠁡󠁆󠁸󠁈󠀰󠀵󠀷󠁴󠀰󠁇󠁖󠁯󠁚󠀸󠁌󠀹󠁷󠁤󠁒󠀸󠁿*/ @"Please enter the admin PIN", nil)
                                  delegate:[UIAlertView class]
                         cancelButtonTitle:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁩󠁢󠁺󠁬󠁏󠁶󠁣󠁧󠁫󠁍󠁋󠁧󠁫󠀱󠁶󠁐󠁐󠀹󠁘󠀴󠁘󠁡󠁕󠁭󠁹󠁷󠀰󠁿*/ @"Cancel", nil)
                         otherButtonTitles:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁄󠁴󠁍󠁉󠁣󠀫󠁺󠁔󠁗󠁦󠁹󠁡󠁯󠀷󠁨󠁔󠁩󠀸󠁯󠀫󠁨󠁘󠀹󠁱󠁌󠀶󠀸󠁿*/ @"OK", nil), nil];
        pinAlert.alertViewStyle = UIAlertViewStyleSecureTextInput;
        
        __block UITextField *pinField = [pinAlert textFieldAtIndex:0];
        pinField.delegate = self;
#if DEBUG
        if (BasketController.sharedInstance.clientSettings.adminPinCode != nil)
        {
            pinField.text = BasketController.sharedInstance.clientSettings.adminPinCode;
        }
#endif
         
        [OCGKeyboardTypeButtonItem setKeyboardType:kOCGKeyboardTypeNumberPad forTarget:pinField];
        
        pinAlert.dismissBlock = ^(int buttonIndex)
        {
            if ([pinField.text isEqualToString:BasketController.sharedInstance.clientSettings.adminPinCode])
            {
                self.adminEnabled = YES;
                self.navigationItem.rightBarButtonItem = nil;
                [self.collectionView reloadData];
            } else {
                [[[UIAlertView alloc] initWithTitle:nil
                                            message:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁂󠁙󠀰󠁊󠀷󠁵󠁡󠁏󠀯󠁑󠁉󠀫󠀷󠁁󠁶󠁦󠁖󠁧󠁚󠁓󠁄󠀶󠁆󠀲󠁌󠁺󠁕󠁿*/ @"Incorrect PIN entered", nil)
                                           delegate:nil
                                  cancelButtonTitle:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁄󠁴󠁍󠁉󠁣󠀫󠁺󠁔󠁗󠁦󠁹󠁡󠁯󠀷󠁨󠁔󠁩󠀸󠁯󠀫󠁨󠁘󠀹󠁱󠁌󠀶󠀸󠁿*/ @"OK", nil)
                                  otherButtonTitles:nil] show];
            }
            _alertView = nil;
        };
        _alertView = pinAlert;
        [pinAlert show];
    }
}

#pragma mark - UISwitch change event

- (void) switchDetailChanged:(UISwitch*)switchDetail
{
    NSIndexPath *indexPath = [self.collectionView indexPathForCell:switchDetail.OCGCollectionViewCell_collectionViewCell];
    NSString *entryIdentifier = [self.layout objectForIndexPath:indexPath];
    if ([entryIdentifier isEqualToString:kSettingEntryOnlineServerEnabled]) {
        // toggled the online server toggle
        if (switchDetail.on) {
            // toggle online mode on
            [TestModeController setServerMode:ServerModeOnline];
        } else {
            // toggle online mode off
            [TestModeController setServerMode:ServerModeOffline];
        }
        indexPath = [self.layout indexPathsForObject:kSettingEntryOfflineServerEnabled][0];
        OCGCollectionViewCell *otherCell = (OCGCollectionViewCell *)[self.collectionView cellForItemAtIndexPath:indexPath];
        UISwitch *otherSwitchDetail = (UISwitch*) otherCell.detailView;
        [self updateSwitchDetail:otherSwitchDetail entryIdentifier:kSettingEntryOfflineServerEnabled];
    } else if ([entryIdentifier isEqualToString:kSettingEntryOfflineServerEnabled]) {
        // toggled the offline server toggle
        if (switchDetail.on) {
            // toggle offline mode on
            [TestModeController setServerMode:ServerModeOffline];
        } else {
            // toggle offline mode off
            [TestModeController setServerMode:ServerModeOnline];
        }
        indexPath = [self.layout indexPathsForObject:kSettingEntryOnlineServerEnabled][0];
        OCGCollectionViewCell *otherCell = (OCGCollectionViewCell *)[self.collectionView cellForItemAtIndexPath:indexPath];
        UISwitch *otherSwitchDetail = (UISwitch*) otherCell.detailView;
        [self updateSwitchDetail:otherSwitchDetail entryIdentifier:kSettingEntryOnlineServerEnabled];
    }
    [self updateSwitchDetail:switchDetail entryIdentifier:entryIdentifier];
    [self checkView];
}

-(void)updateSwitchDetail:(UISwitch*)detailSwitch entryIdentifier:(NSString*)entryIdentifier
{
    NSAssert(detailSwitch, @"");
    if ([entryIdentifier isEqualToString:kSettingEntryOnlineServerEnabled]) {
        detailSwitch.on = ([TestModeController serverMode] == ServerModeOnline);
    } else if ([entryIdentifier isEqualToString:kSettingEntryOfflineServerEnabled]) {
        detailSwitch.on = ([TestModeController serverMode] == ServerModeOffline);
    }
    // TFS 57926 - let the user toggle the enable switch for offline or online if
    // there is a valid address/location id/till id for the relevant option
        
    if ([entryIdentifier isEqualToString:kSettingEntryOnlineServerEnabled]) {
        detailSwitch.enabled = [[BasketController sharedInstance] needsAuthentication]
        && [TillSetupController getOnlineTillNumber] != nil
        && ([TillSetupController getOfflineTillNumber] != nil || [TestModeController serverMode] == ServerModeOffline);
    } else if ([entryIdentifier isEqualToString:kSettingEntryOfflineServerEnabled]) {
        detailSwitch.enabled = [[BasketController sharedInstance] needsAuthentication]
        && [TillSetupController getOfflineTillNumber] != nil
        && ([TillSetupController getOnlineTillNumber] != nil || [TestModeController serverMode] == ServerModeOnline);
    }
}

-(BOOL)collectionView:(UICollectionView *)collectionView canPerformAction:(SEL)action forItemAtIndexPath:(NSIndexPath *)indexPath withSender:(id)sender
{
    
    return YES;
}

#pragma mark - Methods

-(void)checkView
{
    if ((([TestModeController serverMode] == ServerModeOnline) && ([TillSetupController getOnlineTillNumber] != nil)) ||
        (([TestModeController serverMode] == ServerModeOffline) && ([TillSetupController getOfflineTillNumber] != nil)))
    {
        self.navigationItem.leftBarButtonItem = self.dismiss;
    }
    else
    {
        self.navigationItem.leftBarButtonItem = nil;
    }
}

-(void)resetAdmin
{
    _resetAdmin = YES;
}

@end
