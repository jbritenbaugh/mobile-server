//
//  BasketHistoryFilterOptionsTableViewController.m
//  mPOS
//
//  Created by Antonio Strijdom on 22/11/2012.
//  Copyright (c) 2012 Clarity. All rights reserved.
//

#import "BasketHistoryFilterOptionsTableViewController.h"
#import "CRSEditableTableField.h"

@interface BasketHistoryFilterOptionsTableViewController ()

@end

@implementation BasketHistoryFilterOptionsTableViewController

#pragma mark - Properties

@synthesize locations = _locations;
- (void) setLocations:(NSArray *)locations
{
    _locations = locations;
    if (self.dataSource != nil) {
        self.dataSource = [self initializeDataSource];
    }
}

#pragma mark - UIViewController

- (void) viewDidLoad
{
    [super viewDidLoad];
    self.tableView.dataSource = self.dataSource;
    self.dataSource.navigationController = self.navigationController;
}

- (void) viewWillAppear:(BOOL)animated
{
    
    self.title = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁰󠁑󠁤󠁉󠁴󠀳󠀴󠁚󠁑󠁗󠁢󠁸󠀵󠁄󠁂󠁘󠁰󠁙󠁉󠁪󠁶󠀷󠁌󠁨󠁳󠀰󠁅󠁿*/ @"Filter", nil);
    
    // add the buttons
    UIBarButtonItem *cancel =
    [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel
                                                  target:self
                                                  action:@selector(clearTapped:)];
    self.navigationItem.leftBarButtonItem = cancel;
    UIBarButtonItem *apply =
    [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁇󠁱󠁰󠁄󠀯󠁨󠀴󠁑󠁎󠀫󠁦󠁄󠁖󠁵󠀱󠁧󠁍󠁚󠁅󠀲󠁂󠁇󠁔󠀴󠁵󠁹󠁧󠁿*/ @"Apply", nil)
                                     style:UIBarButtonItemStyleDone
                                    target:self
                                    action:@selector(filterTapped:)];
    self.navigationItem.rightBarButtonItem = apply;

    [super viewWillAppear:animated];
    [self.tableView reloadData];
    self.editMode = YES;
}

#pragma mark - CRSEditableTableViewController

- (void) didExitEditingMode
{
    // update the dictionary
    self.selectedOptions = [(BasketHistoryFilterOptionsDataSource *)self.dataSource getOptions];
    
    [super didExitEditingMode];
}

- (CRSEditableTableFieldsDataSource *) initializeDataSource
{
    BasketHistoryFilterOptionsDataSource *result =
    [[BasketHistoryFilterOptionsDataSource alloc] init];
    result.selectedOptions = self.selectedOptions;
    result.locations = self.locations;
    
    return result;
}

#pragma mark - Actions

- (IBAction) filterTapped:(id)sender
{
    self.editMode = NO;
    [self.delegate basketHistoryFilterOptionsVC:self
                                 didApplyFilter:self.selectedOptions];
}

- (IBAction) clearTapped:(id)sender
{
    self.editMode = NO;
    [self.delegate basketHistoryFilterOptionsVC:self
                                 didApplyFilter:nil];
}

@end
