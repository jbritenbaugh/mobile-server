//
//  OCGModifierViewController.m
//  mPOS
//
//  Created by Antonio Strijdom on 11/09/2014.
//  Copyright (c) 2014 Omnico. All rights reserved.
//

#import "OCGModifierViewController.h"
#import "OCGCollectionViewCell.h"
#import "OCGModifierCell.h"
#import "UIAlertView+OCGBlockAdditions.h"
#import "CRSSkinning.h"

@interface OCGModifierSupplementaryView : UICollectionReusableView
@property (nonatomic, strong) UILabel *label;
@end

@implementation OCGModifierSupplementaryView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
        self.label = [[UILabel alloc] init];
        self.label.translatesAutoresizingMaskIntoConstraints = NO;
        self.label.textAlignment = NSTextAlignmentLeft;
        self.label.textColor = [UIColor colorWithRed:0.49
                                               green:0.49
                                                blue:0.50
                                               alpha:1];
        self.label.backgroundColor = [UIColor clearColor];
        [self addSubview:self.label];
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"|-[label]-|"
                                                                     options:0
                                                                     metrics:nil
                                                                       views:@{@"label": self.label}]];
        [self addConstraint:[NSLayoutConstraint constraintWithItem:self.label
                                                         attribute:NSLayoutAttributeCenterY
                                                         relatedBy:NSLayoutRelationEqual
                                                            toItem:self
                                                         attribute:NSLayoutAttributeCenterY
                                                        multiplier:1.0f
                                                          constant:0.0f]];
    }
    
    return self;
}

- (void)prepareForReuse
{
    [super prepareForReuse];
    self.label.text = nil;
}

@end

@interface OCGModifierViewController () <UITextFieldDelegate> {
    UIAlertView *_freeTextAlert;
    CGFloat _columnCount;
    NSUInteger _sectionCount;
}
@property (nonatomic, assign) NSInteger currentChoice;
- (BOOL)selectNextChoice;
- (void)moveToNextChoice;
- (void)addSelection:(ModifierSelection *)selection
             inGroup:(ItemModifierGroup *)group;
- (void)selectModifier:(ItemModifier *)selection
               inGroup:(ItemModifierGroup *)group;
- (void)cancelSelections:(id)sender;
@end

@implementation OCGModifierViewController

#pragma mark - Private

static const CGFloat kROW_HEIGHT = 44.0f;
static const CGFloat kSEPERATOR_HEIGHT = 1.0f;
static const CGFloat kPADDING_X = 5.0f;
static const CGFloat kPADDING_Y = 10.0f;
static const CGFloat kSEPERATOR_INDENT = 60.0f;

static NSString *kHeaderReuseId = @"kHeaderReuseId";
static NSString *kModifierReuseId = @"kModifierReuseId";

- (BOOL)selectNextChoice
{
    BOOL result = NO;
    
    // select the next modifier menu
    for (NSInteger nextChoice = self.currentChoice + 1; nextChoice < self.choices.count; nextChoice++) {
        // skip empty modifier groups
        ItemModifierGroup *nextGroup = self.choices[nextChoice];
        if (nextGroup.itemModifiers.count > 0) {
            self.currentChoice = nextChoice;
            result = YES;
            break;
        } else {
            ErrorLog(@"No modifiers to select for group %@", nextGroup.name);
            // TFS 71401 - error if there are no choices
            if (self.delegate) {
                [self.delegate modifierViewControllerDidError:self];
            }
        }
    }
    
    return result;
}

- (void)moveToNextChoice
{
    // check if the last selection has a group associated with it
    BOOL error = NO;
    if (self.currentChoice != -1) {
        GroupSelection *lastSelections = [self.selections lastObject];
        for (ModifierSelection *selection in lastSelections.selections) {
            if (selection.itemModifierGroupId) {
                // find the group
                ItemModifierGroup *selectionGroup =
                [[BasketController sharedInstance] modifierGroupWithId:selection.itemModifierGroupId];
                if (selectionGroup) {
                    // insert it into the choices list
                    NSMutableArray *newChoices = [self.choices mutableCopy];
                    NSInteger insertIndex = self.currentChoice + 1;
                    if (insertIndex < self.choices.count) {
                        [newChoices insertObject:selectionGroup
                                         atIndex:insertIndex];
                    } else {
                        [newChoices addObject:selectionGroup];
                    }
                    // don't use the setter as it will reset the current selections
                    _choices = newChoices;
                } else {
                    error = YES;
                }
            }
        }
    }
    if (error) {
        // TFS 71401 - error if the modifier group doesn't exist
        if (self.delegate) {
            [self.delegate modifierViewControllerDidError:self];
        }
    } else {
        // check if we are out of selections
        if (self.currentChoice == self.choices.count - 1) {
            // inform the delegate of the selections
            if (self.delegate) {
                [self.delegate modifierViewController:self
                                    didMakeSelections:self.selections];
            }
        } else {
            // move to the next choice
            if (![self selectNextChoice]) {
                self.currentChoice = -1;
                [self.collectionView reloadData];
            } else {
                // update the UI
                self.navigationItem.rightBarButtonItem = nil;
                [self.collectionView reloadSections:[NSIndexSet indexSetWithIndex:_sectionCount - 1]];
            }
        }
    }
}

- (void)addSelection:(ModifierSelection *)selection
             inGroup:(ItemModifierGroup *)group
{
    // get the selection group
    GroupSelection *selectionGroup = nil;
    // if we have already made a selection for this group
    if (self.selections.count > self.currentChoice) {
        // use the existing group
        selectionGroup = self.selections[self.currentChoice];
    } else {
        // create a new selection group
        selectionGroup = [[GroupSelection alloc] init];
        selectionGroup.group = group;
        // add to the selections array
        NSMutableArray *selections = [self.selections mutableCopy];
        [selections addObject:selectionGroup];
        _selections = selections;
    }
    // add the selection
    BOOL added = NO;
    if (selectionGroup.selections == nil) {
        selectionGroup.selections = @[selection];
        added = YES;
    } else {
        // check if this selection already exists
        ModifierSelection *existingSelection = [selectionGroup selectionForModifier:selection];
        if (existingSelection != nil) {
            // already exists, just increment the quantity
            existingSelection.quantity = existingSelection.quantity + 1;
        } else {
            // doesn't exist, just use the new selection
            selectionGroup.selections = [selectionGroup.selections arrayByAddingObject:selection];
        }
    }
    // update the view
    [self.collectionView performBatchUpdates:^{
        if (added) {
            [self.collectionView insertSections:[NSIndexSet indexSetWithIndex:self.selections.count - 1]];
        } else {
            [self.collectionView reloadSections:[NSIndexSet indexSetWithIndex:[self.selections indexOfObject:selectionGroup]]];
        }
        // automatically move to the next selection, unless this is a multiple mandatory group
        self.navigationItem.rightBarButtonItem = nil;
        if (![group.type isEqualToString:@"MULTIPLE"]) {
            [self moveToNextChoice];
        } else {
            // show the done button for moving to the next choice
            self.navigationItem.rightBarButtonItem =
            [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone
                                                          target:self
                                                          action:@selector(doneWithSelections:)];
        }
    }
                                  completion:^(BOOL finished) {
                                      
    }];
}

- (void)selectModifier:(ItemModifier *)selection
               inGroup:(ItemModifierGroup *)group
{
    ModifierSelection *newSelection = [ModifierSelection selectionFromItemModifier:selection];
    // check if we need to specify text
    if ([newSelection.type isEqualToString:@"FREETEXT"]) {
        // show the text entry prompt
        _freeTextAlert =
        [[UIAlertView alloc] initWithTitle:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁺󠁦󠁤󠁐󠁂󠁲󠁂󠁴󠁯󠁎󠀲󠁗󠁫󠁄󠀫󠁹󠁢󠁋󠁌󠁄󠀶󠀶󠁃󠁔󠁊󠁮󠁕󠁿*/ @"Free Text", nil)
                                   message:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁣󠁶󠁥󠁵󠁦󠁙󠀶󠀶󠁣󠁣󠁑󠁷󠀰󠁧󠁦󠁭󠁂󠁱󠁶󠁬󠀫󠁃󠁢󠁭󠁂󠁅󠀴󠁿*/ @"Please enter a description for this modifier", nil)
                                  delegate:[UIAlertView class]
                         cancelButtonTitle:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁩󠁢󠁺󠁬󠁏󠁶󠁣󠁧󠁫󠁍󠁋󠁧󠁫󠀱󠁶󠁐󠁐󠀹󠁘󠀴󠁘󠁡󠁕󠁭󠁹󠁷󠀰󠁿*/ @"Cancel", nil)
                         otherButtonTitles:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁄󠁴󠁍󠁉󠁣󠀫󠁺󠁔󠁗󠁦󠁹󠁡󠁯󠀷󠁨󠁔󠁩󠀸󠁯󠀫󠁨󠁘󠀹󠁱󠁌󠀶󠀸󠁿*/ @"OK", nil), nil];
        _freeTextAlert.alertViewStyle = UIAlertViewStylePlainTextInput;
        UITextField *freeTextField = [_freeTextAlert textFieldAtIndex:0];
        freeTextField.delegate = self;
        freeTextField.keyboardType = UIKeyboardTypeASCIICapable;
        __weak OCGModifierViewController *weakSelf = self;
        _freeTextAlert.dismissBlock = ^(NSInteger buttonIndex) {
            if (buttonIndex == 0) {
                newSelection.freeText = freeTextField.text;
                [weakSelf addSelection:newSelection
                               inGroup:group];
            }
        };
        [_freeTextAlert show];
    } else {
        [self addSelection:newSelection
                   inGroup:group];
    }
}

#pragma mark Actions

- (void)cancelSelections:(id)sender
{
    // inform the delegate of the cancel
    if (self.delegate) {
        [self.delegate modifierViewControllerDidCancel:self];
    }
}

- (void)doneWithSelections:(id)sender
{
    if (self.selections.count > self.currentChoice) {
        [self.collectionView performBatchUpdates:^{
            [self moveToNextChoice];
        }
                                      completion:nil];
    } else {
        [[[UIAlertView alloc] initWithTitle:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁋󠁂󠁎󠁵󠁎󠀯󠁈󠁑󠀶󠁢󠁍󠁥󠀴󠁈󠁔󠁡󠁫󠁹󠁹󠁎󠁏󠁫󠀹󠁘󠁚󠁦󠁣󠁿*/ @"Modifiers", nil)
                                    message:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁃󠁁󠁵󠁡󠁃󠀸󠁎󠀷󠁢󠁒󠁒󠁣󠁬󠁤󠁪󠁔󠁡󠁬󠀰󠀵󠁄󠁃󠁹󠁧󠁪󠁔󠁣󠁿*/ @"Please make at least one selection", nil)
                                   delegate:nil
                          cancelButtonTitle:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁄󠁴󠁍󠁉󠁣󠀫󠁺󠁔󠁗󠁦󠁹󠁡󠁯󠀷󠁨󠁔󠁩󠀸󠁯󠀫󠁨󠁘󠀹󠁱󠁌󠀶󠀸󠁿*/ @"OK", nil)
                          otherButtonTitles:nil] show];
    }
}

#pragma mark - Properties

@synthesize choices = _choices;
- (void)setChoices:(NSArray *)choices
{
    if (choices != _choices) {
        _choices = choices;
        _selections = [NSArray array];
        self.currentChoice = -1;
        [self selectNextChoice];
    }
}

@synthesize selections = _selections;
@synthesize currentChoice = _currentChoice;

#pragma mark - init

- (instancetype)init
{
    UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
    layout.scrollDirection = UICollectionViewScrollDirectionVertical;
    layout.headerReferenceSize = CGSizeZero;
    layout.footerReferenceSize = CGSizeZero;
    self = [super initWithCollectionViewLayout:layout];
    if (self) {
        
    }
    return self;
}

#pragma mark - UIViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = self.itemDesc;
    self.collectionView.backgroundColor = [UIColor colorWithRed:0.937
                                                          green:0.937
                                                           blue:0.957
                                                          alpha:1.000];
    [OCGCollectionViewCell registerForDetailClass:[UILabel class]
                                          options:OCGCollectionViewCellDetailOptionUseTextLabelBaseline
                                   collectionView:self.collectionView];
    [self.collectionView registerClass:[OCGModifierCell class]
            forCellWithReuseIdentifier:kModifierReuseId];
    [self.collectionView registerClass:[OCGModifierSupplementaryView class]
            forSupplementaryViewOfKind:UICollectionElementKindSectionHeader
                   withReuseIdentifier:kHeaderReuseId];
    self.collectionView.delaysContentTouches = NO;
    
    _columnCount = 2.0f;
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        _columnCount = 3.0f;
    }
    
    // show the cancel button for optional modifiers
    self.navigationItem.leftBarButtonItem = nil;
    if (self.optional) {
        self.navigationItem.leftBarButtonItem =
        [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel
                                                      target:self
                                                      action:@selector(cancelSelections:)];
    }
    self.navigationItem.rightBarButtonItem = nil;
    
    self.collectionView.tag = kTableSkinningTag;
    [[CRSSkinning currentSkin] applyViewSkin:self];
    
    [self.collectionView reloadData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    NSInteger result = 0;
    
    // calculate the number of selections
    if (self.selections != nil) {
        for (GroupSelection *selection in self.selections) {
            result++;
        }
    }
    // add the current selection
    if (self.currentChoice != -1) {
        result++;
    }
    
    // make a note of the section count
    _sectionCount = result;
    
    return result;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView
     numberOfItemsInSection:(NSInteger)section
{
    NSInteger result = 0;
    
    if (section != _sectionCount - 1) {
        // if this section already has a selection, just show it
        GroupSelection *selection = self.selections[section];
        result = selection.selections.count;
    } else {
        // otherwise show all options
        ItemModifierGroup *choice = self.choices[self.currentChoice];
        result = choice.itemModifiers.count;
    }
    
    return result;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView
                  cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *reuseIdentifier = nil;
    // if this section already has a selection
    if (indexPath.section != _sectionCount - 1) {
        reuseIdentifier = [OCGCollectionViewCell reuseIdentifierForDetailClass:[UILabel class]
                                                                       options:OCGCollectionViewCellDetailOptionUseTextLabelBaseline];
    } else {
        reuseIdentifier = kModifierReuseId;
    }
    UICollectionViewCell *result = [collectionView dequeueReusableCellWithReuseIdentifier:reuseIdentifier
                                                                             forIndexPath:indexPath];
    if (result) {
        // if this section already has a selection
        if (indexPath.section != _sectionCount - 1) {
            // just show it
            OCGCollectionViewCell *selectionCell = (OCGCollectionViewCell *)result;
            GroupSelection *selection = self.selections[indexPath.section];
            ModifierSelection *modifier = selection.selections[indexPath.item];
            if (modifier.freeText.length > 0) {
                selectionCell.textLabel.text = modifier.freeText;
            } else {
                if (modifier.quantity == 1) {
                    selectionCell.textLabel.text = modifier.name;
                } else {
                    selectionCell.textLabel.text =
                    [NSString stringWithFormat:@"%lu x %@", (unsigned long)modifier.quantity, modifier.name];
                }
            }
            selectionCell.detailTextLabel.text = modifier.retailPrice.display;
            selectionCell.detailTextLabel.textAlignment = NSTextAlignmentRight;
            selectionCell.tag = kTableCellSkinningTag;
            selectionCell.textLabel.tag = kTableCellTextSkinningTag;
            selectionCell.detailTextLabel.tag = kTableCellValueSkinningTag;
            // if this isn't the last selection
            if (indexPath.item < selection.selections.count - 1) {
                // add a seperator
                CGRect seperatorRect = CGRectMake(kSEPERATOR_INDENT, kROW_HEIGHT - kSEPERATOR_HEIGHT, collectionView.frame.size.width - kSEPERATOR_INDENT, kSEPERATOR_HEIGHT);
                UIView *seperatorView = [[UIView alloc] initWithFrame:seperatorRect];
                seperatorView.translatesAutoresizingMaskIntoConstraints = NO;
                seperatorView.backgroundColor = [UIColor colorWithRed:0.89 green:0.89 blue:0.90 alpha:1.000];
                [selectionCell addSubview:seperatorView];
                [selectionCell bringSubviewToFront:seperatorView];
            }
        } else {
            OCGModifierCell *modifierCell = (OCGModifierCell *)result;
            // get the modifier group for this section
            ItemModifierGroup *choice = self.choices[self.currentChoice];
            // get the modifier
            ItemModifier *modifier = choice.itemModifiers[indexPath.item];
            modifierCell.titleLabel.text = modifier.name;
            modifierCell.backgroundColor = [UIColor whiteColor];
            modifierCell.priceLabel.text = modifier.retailPrice.display;
            modifierCell.opaque = YES;
            modifierCell.alpha = 1.0f;
        }
        result.backgroundColor = [UIColor whiteColor];
    }
    
    [[CRSSkinning currentSkin] applyViewSkin:result
                             withTagOverride:result.tag];
    
    return result;
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView
           viewForSupplementaryElementOfKind:(NSString *)kind
                                 atIndexPath:(NSIndexPath *)indexPath
{
    OCGModifierSupplementaryView *result = nil;
    
    if ([kind isEqualToString:UICollectionElementKindSectionHeader]) {
        result = [collectionView dequeueReusableSupplementaryViewOfKind:kind
                                                    withReuseIdentifier:kHeaderReuseId
                                                           forIndexPath:indexPath];
        if ((indexPath.section != _sectionCount - 1) &&
            (indexPath.section == 0)) {
            result.label.text = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁭󠁶󠁆󠁵󠀫󠁳󠁦󠁒󠁊󠀵󠀯󠀰󠁂󠁺󠁄󠁚󠁡󠀵󠁪󠁂󠁄󠁄󠀶󠁔󠁆󠁲󠀸󠁿*/ @"Selections", nil);
        } else {
            ItemModifierGroup *group = self.choices[self.currentChoice];
            result.label.text = group.name;
        }
    }
    
    return result;
}

#pragma mark - UICollectionViewDelegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    [collectionView deselectItemAtIndexPath:indexPath
                                   animated:YES];
    // if this section hasn't already got a selection
    if (indexPath.section == _sectionCount - 1) {
        // make a note of the selection
        ItemModifierGroup *group = self.choices[self.currentChoice];
        ItemModifier *selectedModifier = group.itemModifiers[indexPath.item];
        [self selectModifier:selectedModifier
                     inGroup:group];
    }
}

#pragma mark - UICollectionViewDelegateFlowLayout

- (CGSize)collectionView:(UICollectionView *)collectionView
                  layout:(UICollectionViewLayout*)collectionViewLayout
  sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CGSize result = CGSizeZero;
    
    // if this section already has a selection
    if (indexPath.section != _sectionCount - 1) {
        result = CGSizeMake(self.collectionView.frame.size.width,
                            kROW_HEIGHT);
    } else {
        result = CGSizeMake((self.collectionView.frame.size.width / _columnCount) - (kPADDING_X * 2),
                            kROW_HEIGHT - kPADDING_Y);
    }
    
    return result;
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView
                        layout:(UICollectionViewLayout*)collectionViewLayout
        insetForSectionAtIndex:(NSInteger)section
{
    UIEdgeInsets result = UIEdgeInsetsZero;
    
    // if this section already has a selection
    if (section != _sectionCount - 1) {
        result = UIEdgeInsetsZero;
    } else {
        result = UIEdgeInsetsMake(0.0f, kPADDING_X, kPADDING_Y, kPADDING_X);
    }
    
    return result;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView
                   layout:(UICollectionViewLayout*)collectionViewLayout
minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
    CGFloat result = 0.0f;
    
    // if this section already has a selection
    if (section != _sectionCount - 1) {
        result = 0.0f;
    } else {
        result = kPADDING_Y;
    }
    
    return result;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView
                   layout:(UICollectionViewLayout*)collectionViewLayout
minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
    CGFloat result = 0.0f;
    
    // if this section already has a selection
    if (section != _sectionCount - 1) {
        result = 0.0f;
    } else {
        result = kPADDING_X;
    }
    
    return result;
}

- (CGSize)collectionView:(UICollectionView *)collectionView
                  layout:(UICollectionViewLayout*)collectionViewLayout
referenceSizeForHeaderInSection:(NSInteger)section
{
    CGSize result = CGSizeZero;
    
    // if this section already has a selection
    if (section != _sectionCount - 1) {
        if (section == 0) {
            result = CGSizeMake(collectionView.frame.size.width,
                                kROW_HEIGHT);
        } else {
            result = CGSizeZero;
        }
    } else {
        result = CGSizeMake(collectionView.frame.size.width,
                            kROW_HEIGHT);
    }
    
    return result;
}

- (CGSize)collectionView:(UICollectionView *)collectionView
                  layout:(UICollectionViewLayout*)collectionViewLayout
referenceSizeForFooterInSection:(NSInteger)section
{
    return CGSizeZero;
}

#pragma mark - UITextFieldDelegate

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    BOOL result = YES;
    
    NSString *newText = [textField.text stringByReplacingCharactersInRange:range
                                                                withString:string];
    if (newText.length >= 40) {
        result = NO;
    }
    
    return result;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    if (_freeTextAlert) {
        [_freeTextAlert dismissWithClickedButtonIndex:1
                                             animated:YES];
    }
    return NO;
}

@end
