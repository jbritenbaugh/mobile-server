//
//  ContextController.m
//  mPOS
//
//  Created by Antonio Strijdom on 15/01/2014.
//  Copyright (c) 2014 Clarity. All rights reserved.
//

#import "ContextViewController.h"
#import <objc/runtime.h>
#import "CRSSkinning.h"
#import "AppDelegate.h"
#import "BasketController.h"
#import "ContextMenuViewController.h"
#import "BasketViewController.h"
#import "SalesMenuController.h"
#import "TestModeController.h"
#import "POSFunc.h"
#import "MoreOptionsTableViewController.h"
#import "POSFuncController.h"
#import "LoginViewController.h"
#import "ExternalAuthViewController.h"
#import "TillSetupController.h"
#import "CRSLocationController.h"
#import "OCGMenuPageViewController.h"
#import "BasketItemDetailViewController.h"
#import "SearchViewController.h"
#import "OCGReceiptManager.h"
#import "DeviceStationViewController.h"
#import "DeviceStationStatusViewController.h"
#import "BasketEditViewController.h"
#import "BasketItemAddViewController.h"
#import "BasketItemDetailViewController.h"
#import "UIAlertView+OCGBlockAdditions.h"
#import "OCGEFTManager.h"
#import "StatusViewController.h"
#import "OCGPeripheralManager.h"
#import "OCGTenderAmountIntegratedTextFieldToolbar.h"
#import "pcre.h"
#import "OCGModifierViewController.h"
#import "UIImage+OCGExtensions.h"
#import "PrinterController.h"
#import "OCGInputAccessoryView.h"
#import "OCGKeyboardTypeButtonItem.h"
#import "OperatorOverrideViewController.h"
#import "ClientSettings+MPOSExtensions.h"
#import "NSArray+OCGExtensions.h"
#import "OCGSelectViewController.h"

typedef NS_ENUM(NSInteger, SoftKeySelectionSource) {
    SoftKeySelectionSourceDefault,
    SoftKeySelectionSourceBar,
    SoftKeySelectionSourceSwipe,
    SoftKeySelectionSourceMenuChanged
};

@interface ContextViewController () <UINavigationControllerDelegate, LoginViewControllerDelegate, UITextFieldDelegate, OCGModifierViewControllerDelegate> {
    BOOL _loginConfigChanged;
    BOOL _settingsPresented;
    BOOL _loginPresented;
    BOOL _modifierSelectionPresented;
    BarcodeUseType _barcodeUseType;
}
@property (nonatomic, assign) enum MPOSContext currentContext;
@property (nonatomic, assign) enum MPOSUIState currentUIState;
@property (nonatomic, strong) ServerError *addItemError;
@property (nonatomic, readonly) ContextMenuViewController *menuVC;
@property (nonatomic, readonly) UIViewController *currentViewController;
@property (nonatomic, readonly) UIViewController *loginViewController;
@property (nonatomic, strong) UINavigationController *loginNavigationController;
@property (nonatomic, strong) OCGSplitViewController *loginSplitViewController;
@property (nonatomic, strong) BasketViewController *basketViewController;
@property (nonatomic, strong) BasketItemAddViewController *addItemVC;
@property (nonatomic, strong) BasketItemDetailViewController *operationsViewController;
@property (nonatomic, assign) UINavigationControllerOperation transitionOperation;
@property (nonatomic, strong) SearchViewController *searchViewController;
@property (nonatomic, strong) UISwipeGestureRecognizer *leftSwipeGesture;
@property (nonatomic, strong) UISwipeGestureRecognizer *rightSwipeGesture;
@property (nonatomic, strong) Payment *payment;
@property (nonatomic, strong) OCGTenderAmountIntegratedTextFieldToolbar *tenderAmountToolbar;
@property (nonatomic, strong) FormViewController *presentedFormViewController;
@end

@implementation ContextViewController

#pragma mark - Private

static CGFloat kTransitionAnimationDuration = 0.2f;
static double kScannerButtonEnableDelayInSeconds = 0.1;

#pragma mark Notifications

- (void) handleBasketControllerActiveTaxFreeChangeNotification:(NSNotification *)note
{
    [FormViewController handleModelObject:note.object contextName:nil presentingViewController:self];
}

- (void) handleBasketControllerNotification:(NSNotification *)note
{
    // check for specific notifications
    if ([note.name isEqualToString:kBasketControllerConfigurationDidChangeNotification]) {
        // configuration change
        [self handleConfigurationChangeNotification:note];
    } else if ([note.name isEqualToString:kBasketControllerDeviceNotFoundNotification]) {
        // device registration not found
        [self handleDeviceNotFoundNotification:note];
    } else if ([note.name isEqualToString:kBasketControllerUserLoggedOnNotification]) {
        // user logged on
        [self handleUserWasLoggedOnNotification:note];
    } else if ([note.name isEqualToString:kBasketControllerHasLoggedOutNotification]) {
        // user logged off
        [self handleUserWasLoggedOutNotification:note];
    } else if ([note.name isEqualToString:kBasketControllerBasketAddItemNotification]) {
        // item changed
        [self handleBasketItemChangedNotification:note];
    } else if ([note.name isEqualToString:kBasketControllerControlErrorNotification]) {
        // control error
        [self handleControlErrorNotification:note];
    } else if ([note.name isEqualToString:kBasketControllerBasketAddItemErrorNotification]) {
        // add item error
        [self handleBasketAddItemErrorNotification:note];
    } else if ([note.name isEqualToString:kBasketControllerBasketChangeNotification] ||
               [note.name isEqualToString:kBasketControllerBasketItemChangeNotification]) {
        // check for mandadory modifiers
        [self mandatoryModifierCheck];
        // TFS71309 - is auto printins is enabled check for print data lines and print them
        if (BasketController.sharedInstance.autoPrintingEnabled) {
            [self handlePrintDataLines];
        }
    } else if ([note.name isEqualToString:kBasketControllerBasketCheckedOutErrorNotification]) {
        // check out failed
        // get the active basket
        BasketController *basketController = [BasketController sharedInstance];
        MPOSBasket *basket = basketController.basket;
        // if we were totalling in a foreign currency
        if (((basket.foreignSubtotal != nil) && (![basket.foreignSubtotal.currencyCode isEqual:self.payment.tender.currencyCode])) ||
            ((basket.foreignSubtotal == nil) && (![basket.currencyCode isEqual:self.payment.tender.currencyCode]))) {
            [self.tenderAmountToolbar.textField resignFirstResponder];
            _payment = nil;
        }
    }
    
    // if we are processing a payment
    if (self.payment.tender != nil) {
        [self assemblePayment];
        // if we are adding a foreign currency
        if (self.payment.tender.isForeignCurrency) {
            // update/show the foreign currency total
            if (self.navigationItem.rightBarButtonItem != nil) {
                // get the active basket
                BasketController *basketController = [BasketController sharedInstance];
                MPOSBasket *basket = basketController.basket;
                // update the label
                [self.navigationItem.rightBarButtonItem setTitle:basket.foreignSubtotal.foreignAmount.display];
            }
        } else {
            // hide the label
            if (self.navigationItem.rightBarButtonItem != nil) {
                // update the label
                [self.navigationItem.rightBarButtonItem setTitle:nil];
            }
        }
    }
    
    // update the app context
    [self updateCurrentContext];
    
    // TFS70298 - if we checked out / uncheckout out, make sure the basket is visible
    if ([note.name isEqualToString:kBasketControllerBasketCheckedOutChangeNotification]) {
        [self ensureBasketViewControllerIsPresented];
    }
    
    // override errors
    // check if the user can override this error message
    if ([note.name isEqualToString:kBasketControllerOverrideErrorNotification]) {
        ServerError *serverError = note.userInfo[kBasketControllerGeneralErrorKey];
        if (serverError) {
            // present the override view
            OperatorOverrideViewController *overrideVC = [[OperatorOverrideViewController alloc] init];
            overrideVC.serverError = serverError;
            UINavigationController *overrideNav = [[UINavigationController alloc] initWithRootViewController:overrideVC];
            overrideNav.modalPresentationStyle = UIModalPresentationOverFullScreen;
            overrideNav.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
            [self presentViewController:overrideNav
                               animated:YES
                             completion:nil];
        }
    }
    
    // if customer card added, force a context update
    if ([note.name isEqualToString:kBasketControllerCustomerCardAddedNotification]) {
        [self updateUIContextChanged:YES];
    }
}

- (void) handleConfigurationChangeNotification:(NSNotification *)note
{
    // show the logon
    if ([[BasketController sharedInstance] needsAuthentication]) {
        if (_loginPresented) {
            [self.loginNavigationController setViewControllers:@[self.loginViewController]
                                                      animated:!_settingsPresented];
        }
    }
    // update the UI
    [self updateUIContextChanged:YES];
    // if we are showing the sales menu view controller
    if ([self presentingViewControllerOfClass:[OCGMenuPageViewController class]]) {
        // get the view controller
        OCGMenuPageViewController *menuViewController = (OCGMenuPageViewController *)
        [self getViewControllerForClass:[OCGMenuPageViewController class]];
        if (menuViewController != nil) {
            // get the context
            SoftKeyContext *context = nil;
            if (self.menuVC.selectedItem.data.length == 0) {
                context = [[BasketController sharedInstance] getLayoutForContextNamed:NSStringForSoftKeyContextType(SoftKeyContextTypeSales)];
            } else {
                context = [[BasketController sharedInstance] getLayoutForContextNamed:self.menuVC.selectedItem.data];
            }
            // update the controller
            SalesMenuController *salesMenuViewController = [[SalesMenuController alloc] initWithRootMenu:context];
            menuViewController = [salesMenuViewController menuPageViewControllerForRootMenu];
            [self presentContextViewController:menuViewController
                                      animated:NO
                        alreadyOnNavController:NO];
        }
        // set the selection color
        self.menuVC.selectedMenuColor = menuViewController.view.backgroundColor;
    }
    // reset the EFT provider
    [[OCGEFTManager sharedInstance] resetProvider];

}

- (void) handleUserWasLoggedOnNotification:(NSNotification *)note
{
    // Quietly set the current context so that the UI will be refreshed as if from scratch.
    _currentContext = MPOSContextUnknown;
    // get the active basket
    [[BasketController sharedInstance] getActiveBasket];
}

- (void) handleUserWasLoggedOutNotification:(NSNotification *)note
{
    [[POSFuncController sharedInstance] logoutCurrentAssociate];
}

- (void) handleDeviceNotFoundNotification:(NSNotification *)note
{
    [self resetSharedRequirements];
}

- (void) handleSettingsPresentedNotification:(NSNotification *)note
{
    _settingsPresented = YES;
}

- (void) handleSettingsDismissedNotification:(NSNotification *)note
{
    _settingsPresented = NO;
    if (_loginConfigChanged && _loginPresented) {
        // dismiss the current login view
        [self dismissModalLoginViewController:self];
    }
    _loginConfigChanged = NO;
}

- (void) handleBasketItemChangedNotification:(NSNotification *)note
{
    if (!BasketController.sharedInstance.isAddingBasketItem) {
        if ((self.addItemError == nil) &&
            // MPOS-307 Disable multiple scans when returning
            (_barcodeUseType != BarcodeUseTypeReturn) &&
            ![BasketController.sharedInstance needsAuthentication]) {
            // resume scanning if there is no error
            [[BarcodeScannerController sharedInstance] resumeScanningWithDelay:YES];
        } else {
            [[BarcodeScannerController sharedInstance] stopScanning];
        }
    }
    // clear the item error
    self.addItemError = nil;
}

- (void) handleBarcodeScannedNotification:(NSNotification *)note
{
    SMBarcode *barcode = note.userInfo[kScannerNotificationBarcodeScannedBarcodeKey];
    [self handleBarcodeScan:barcode.data
                entryMethod:ScannerEntryMethodScanned
                barcodeType:barcode.barcodeType];
}

- (void) handleControlErrorNotification:(NSNotification *)note
{
    // get the error
    ServerError *error = note.userInfo[kBasketControllerGeneralErrorKey];
    BasketControllerControlRetryBlockType retryBlock = note.userInfo[kBasketControllerControlRetryBlockKey];
    NSNumber *canCancelValue = note.userInfo[kBasketControllerControlCancelBlockKey];
    BOOL canCancel = YES;
    if (canCancelValue) {
        canCancel = canCancelValue.boolValue;
    }
    
    NSAssert(error != nil, @"The reference to the error object has not been specified.");
    NSAssert([error isKindOfClass:[ServerError class]], @"The specified error object is inherited from an unexpected BasketController.sharedInstance class.");
    
    // get the device station error
    ServerErrorMessage *deviceStationError = [error deviceStationErrorMessage];
    
    // handle the station error
    if (deviceStationError != nil) {
        DeviceStationViewController *deviceVC = [[DeviceStationViewController alloc] init];
        // if we haven't claimed a station
        if (deviceStationError.codeValue == MShopperDeviceStationNotClaimedExceptionCode) {
            // present the scan screen
            [deviceVC requestDeviceScanComplete:^(NSString *deviceStationId) {
                [deviceVC dismissViewControllerAnimated:YES
                                             completion:^{
                                                 // send the finish again
                                                 if (retryBlock != nil) {
                                                     retryBlock(deviceStationId, deviceStationError.code.unsignedIntegerValue);
                                                 }
                                             }];
            }
                                    allowCancel:canCancel];
            UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:deviceVC];
            navController.modalPresentationStyle = UIModalPresentationFormSheet;
            navController.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
            [self.splitViewController presentViewController:navController
                                                   animated:YES
                                                 completion:nil];
        } else if (deviceStationError.codeValue == MShopperDeviceStationDrawerNotAssignedExceptionCode) {
            // get the current device station id
            NSString *deviceStationId = [[BasketController sharedInstance] basket].deviceStationId;
            // if there isn't one, prompt again
            if ((deviceStationId == nil) || ([deviceStationId isEqualToString:@""])) {
                // present the scan screen
                [deviceVC requestDeviceScanComplete:^(NSString *deviceStationId) {
                    // present the associate alert
                    __block DeviceStationStatusViewController *statusVC = [[DeviceStationStatusViewController alloc] initWithStyle:UITableViewStyleGrouped];
                    [statusVC associateStationWithId:deviceStationId
                                            complete:^(DeviceStation *deviceStation) {
                                                [deviceVC dismissViewControllerAnimated:YES
                                                                             completion:^{
                                                if (deviceStation != nil) {
                                                    // send the finish again
                                                    if (retryBlock != nil) {
                                                        retryBlock(deviceStationId, deviceStationError.code.unsignedIntegerValue);
                                                    }
                                                }
                                                statusVC = nil;
                                                                             }];
                                            }];
                }
                                        allowCancel:canCancel];
                UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:deviceVC];
                navController.modalPresentationStyle = UIModalPresentationFormSheet;
                navController.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
                [self.splitViewController presentViewController:navController
                                                       animated:YES
                                                     completion:nil];
            } else {
                // present the associate alert
                __block DeviceStationStatusViewController *statusVC = [[DeviceStationStatusViewController alloc] initWithStyle:UITableViewStyleGrouped];
                [statusVC associateStationWithId:deviceStationId
                                        complete:^(DeviceStation *deviceStation) {
                                            if (deviceStation != nil) {
                                                // send the finish again
                                                if (retryBlock != nil) {
                                                    retryBlock(deviceStationId, deviceStationError.code.unsignedIntegerValue);
                                                }
                                            }
                                            statusVC = nil;
                                            // update the app context
                                            [self updateCurrentContext];
                                        }];
            }
        } else {
            NSString *errorDeviceStationId = nil;
            NSString *errorDeviceStationName = nil;
            NSString *errorTerminal = nil;
            NSString *errorUser = nil;
            NSString *errorPeripheral = nil;
            if ([deviceStationError isKindOfClass:[ServerErrorDeviceStationMessage class]]) {
                errorDeviceStationId = [(ServerErrorDeviceStationMessage *)deviceStationError deviceStationId];
                errorDeviceStationName = [(ServerErrorDeviceStationMessage *)deviceStationError deviceStationName];
                errorTerminal = [(ServerErrorDeviceStationMessage *)deviceStationError terminal];
                errorUser = [(ServerErrorDeviceStationMessage *)deviceStationError user];
                errorPeripheral = [(ServerErrorDeviceStationMessage *)deviceStationError peripherals];
            }
            ServerErrorMessage *message = [[ServerErrorMessage alloc] init];
            message.code = deviceStationError.code;
            message.value = deviceStationError.value;
            message.basketItemId = deviceStationError.basketItemId;
            [deviceVC presentAlertForDeviceStationError:message
                                    withDeviceStationId:errorDeviceStationId
                                   andDeviceStationName:errorDeviceStationName
                                              andTillId:nil
                                            andTerminal:errorTerminal
                                                andUser:errorUser
                              andUnavailablePeripherals:errorPeripheral
                                              dismissed:^{
                                                  // we're done
                                                  // if we can't cancel
                                                  if (!canCancel && retryBlock) {
                                                      // retry
                                                      retryBlock(nil, deviceStationError.code.unsignedIntegerValue);
                                                  } else {
                                                      // the app context will be updated by the basket update.
                                                      [self updateUIContextChanged:YES];
                                                      [[BasketController sharedInstance] getActiveBasket];
                                                  }
                                                  
                                              }];
        }
    } else {
        [self.basketViewController handleGenericBasketControllerErrorNotification:note];
    }
}

- (void) handleBasketAddItemErrorNotification:(NSNotification *)notification
{
    // get the error
    ServerError *error = notification.userInfo[kBasketControllerBasketItemErrorKey];
    
    NSAssert(error != nil, @"The reference to the error object has not been specified.");
    NSAssert([error isKindOfClass:[ServerError class]], @"The specified error object is inherited from an unexpected BasketController.sharedInstance class.");
    
    // check for return error messages
    BOOL returning = NO;
    for (ServerErrorMessage *message in error.messages) {
        if ((message.codeValue == MShopperBarcodeItemCouldNotBeReturnedExceptionCode) ||
            (message.codeValue == MShopperBarcodeNotRecognizedExceptionCode)) {
            returning = YES;
            break;
        }
    }
    
    if (!returning) {
        self.addItemError = error;
    }
}

- (void) handleEFTNotification:(NSNotification *)note
{
    [self updateUIContextChanged:YES];
}

- (void) handleEFTErrorNotification:(NSNotification *)note
{
    ServerError *error = note.userInfo[kEFTNotificationErrorErrorKey];
    if (error) {
        [self EFTTransactionFailedWithError:error];
        [self updateUIContextChanged:YES];
    }
}

#pragma mark EFT

- (void) EFTTransactionSucceeded
{
    DebugLog(@"ContextViewController::paywareTransactionSucceeded");
}

- (void) EFTTransactionFailedWithError:(ServerError *)serverError
{
    DebugLog(@"ContextViewController::paywareTransactionFailedWithError: %@", serverError);
    
    NSString *errorMessage = @"";
    if ([serverError.messages count] > 0) {
        for (ServerErrorMessage *serverErrorMessage in serverError.messages) {
            errorMessage = [errorMessage stringByAppendingString: [NSString stringWithFormat: @"%@\n", serverErrorMessage.value]];
        }
        DebugLog(@"  error message assembled using serverError array");
    }
    
    UIAlertView *alert =
    [[UIAlertView alloc] initWithTitle:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁺󠁎󠁖󠁰󠁲󠁶󠁡󠁎󠁂󠁸󠁶󠁶󠀴󠁳󠀵󠁱󠀶󠁃󠀷󠁷󠀱󠁉󠁂󠁥󠁌󠁅󠁙󠁿*/ @"Card payment error", nil)
                               message:errorMessage
                              delegate:nil
                     cancelButtonTitle:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁄󠁴󠁍󠁉󠁣󠀫󠁺󠁔󠁗󠁦󠁹󠁡󠁯󠀷󠁨󠁔󠁩󠀸󠁯󠀫󠁨󠁘󠀹󠁱󠁌󠀶󠀸󠁿*/ @"OK", nil)
                     otherButtonTitles:nil];
    [alert show];
}

#pragma mark Barcode

- (BOOL) handleBarcodeScan:(NSString *)barcode
               entryMethod:(ScannerEntryMethod)entryMethod
               barcodeType:(SMBarcodeType) barcodeType
{
    DebugLog(@"BARCODE - Processing barcode %@", barcode);
    
    NSString *entryMethodString = nil;
    switch (entryMethod) {
        case ScannerEntryMethodKeyed: entryMethodString = @"KEYED"; break;
        case ScannerEntryMethodScanned: entryMethodString = @"SCANNED"; break;
        case ScannerEntryMethodSwiped: entryMethodString = @"SWIPED"; break;
        case ScannerEntryMethodUnknown:
        default:
            break;
    }
    
    DebugLog(@"BARCODE - Entry Method - %@", entryMethodString);
    
    NSString *barcodeTypeString = nil;
    switch (barcodeType)
    {
        case SMBarcodeTypeUnknown: barcodeTypeString = @"UNKNOWN"; break;
 		case SMBarcodeTypeEAN2: barcodeTypeString=@"EAN2"; break;
		case SMBarcodeTypeEAN5: barcodeTypeString=@"EAN5"; break;
		case SMBarcodeTypeEAN8: barcodeTypeString=@"EAN8"; break;
		case SMBarcodeTypeEAN13 : barcodeTypeString=@"EAN13"; break;
		case SMBarcodeTypeUPCE : barcodeTypeString=@"UPCE"; break;
		case SMBarcodeTypeUPCA : barcodeTypeString=@"UPCA"; break;
		case SMBarcodeTypeI2OF5 : barcodeTypeString=@"I2OF5"; break;
		case SMBarcodeTypeCode11: barcodeTypeString=@"Code11"; break;
		case SMBarcodeTypeCode39 : barcodeTypeString=@"Code39"; break;
		case SMBarcodeTypeCode93 : barcodeTypeString=@"Code93"; break;
		case SMBarcodeTypeCode128 : barcodeTypeString=@"Code128"; break;
		case SMBarcodeTypeGS1 : barcodeTypeString=@"GS1"; break;
		case SMBarcodeTypeQRCode: barcodeTypeString=@"QRCode"; break;
		case SMBarcodeTypeISBN10: barcodeTypeString=@"ISBN10"; break;
		case SMBarcodeTypeISBN13: barcodeTypeString=@"ISBN13"; break;
		case SMBarcodeTypeComposite: barcodeTypeString=@"Composite"; break;
		case SMBarcodeTypePDF417: barcodeTypeString=@"PDF417"; break;
		case SMBarcodeTypeKeyed:
            entryMethodString = @"KEYED";
            barcodeTypeString=@""; break;
        default:
#if DEBUG
            if (entryMethod == ScannerEntryMethodScanned)
                barcodeTypeString=@"Code128";
#endif
            break;
    }
    
    DebugLog(@"BARCODE - Type - %@", barcodeTypeString);
    
    NSString *trimmedBarcode = [barcode stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    // TFS 57365 - Strip out nulls and everything after the first null
    trimmedBarcode =
    [trimmedBarcode stringByReplacingOccurrencesOfString:@"\\x0.*"
                                              withString:@""
                                                 options:NSRegularExpressionSearch
                                                   range:NSMakeRange(0, [trimmedBarcode length])];
    
    DebugLog(@"BARCODE - Trimmed - %@", trimmedBarcode);
    
    BOOL hasHandledBarcodeScan = NO;
    // if we can handle scans
    if ([BasketController sharedInstance].canScanToBasket) {
        // update the current basket
        switch (_barcodeUseType) {
            case BarcodeUseTypeItem: {
                hasHandledBarcodeScan = [BasketController.sharedInstance addItemWithScanId:trimmedBarcode
                                                                                     price:nil
                                                                                   measure:nil
                                                                                  quantity:nil
                                                                               entryMethod:entryMethodString
                                                                                    track1:nil
                                                                                    track2:nil
                                                                                    track3:nil
                                                                               barcodeType:barcodeTypeString];
                break;
            }
            case BarcodeUseTypeReturn: {
                [[BasketController sharedInstance] findProductForSKU:trimmedBarcode
                                                           forReason:ProductSearchReasonReturnItem];
                hasHandledBarcodeScan = YES;
                break;
            }
            case BarcodeUseTypeLookup: {
                [[BasketController sharedInstance] findProductForSKU:trimmedBarcode
                                                           forReason:ProductSearchReasonItemDetails];
                hasHandledBarcodeScan = YES;
                break;
            }
            default:
                NSAssert(NO, @"Unknown barcodeUseType: %d", _barcodeUseType);
                break;
        }
    }
    return hasHandledBarcodeScan;
}

#pragma mark Tendering support

- (BOOL) testMatchInRegex:(NSString *)re
                   string:(NSString *)subject
{
    const char *errstr;
    int erroffset;
    
    // compile the regex
    pcre *regex = pcre_compile([re cStringUsingEncoding:NSUTF8StringEncoding] , 0, &errstr, &erroffset, NULL);
    // test for a match
    int rc = pcre_exec(regex,
                       NULL,
                       [subject cStringUsingEncoding:NSUTF8StringEncoding],
                       (int)strlen([subject cStringUsingEncoding:NSUTF8StringEncoding]),
                       0,
                       PCRE_PARTIAL,
                       NULL,
                       0);
    
    DebugLog(@"%@: %@\n", subject,
             rc == 0 || rc == PCRE_ERROR_PARTIAL ?
             @"match or partial" : @"no match");
    
    return rc == 0 || rc == PCRE_ERROR_PARTIAL;
}

- (BOOL) alphaNumericAllowedForTender:(Tender *)tender
{
    BOOL result = NO;
    
    // get the tender regex
    NSString *regex = tender.validationRegex; //@"[A-Za-z0-9]+";
    NSString *test = @"A";
    if (regex.length > 0) {
        result = [self testMatchInRegex:regex
                                 string:test];
    }
    
    return result;
}

- (void) tenderWithType:(NSString *)tenderType
{
    // security check
    if ([[BasketController sharedInstance] doesUserHavePermissionTo:POSPermissionAddTender]) {
        // stop scanning if we are
        BarcodeScannerController *scanner = [BarcodeScannerController sharedInstance];
        if (scanner.scanning) {
            [scanner stopScanning];
        }
        // get the tender for this tender type
        Tender *tender = BasketController.sharedInstance.tenders[tenderType];
        if (tender == nil) {
            // no tender found
            UIAlertView *alert =
            [[UIAlertView alloc] initWithTitle:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁭󠁋󠁡󠀲󠀸󠁲󠁪󠁔󠁅󠁌󠁵󠁫󠁲󠁉󠁋󠁵󠁁󠁬󠁖󠁘󠁷󠁢󠁬󠁰󠁅󠁍󠁁󠁿*/ @"Tender not found", nil)
                                       message:[NSString stringWithFormat:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁎󠁥󠀳󠁭󠁣󠁶󠁷󠁸󠁰󠁩󠀲󠁸󠁃󠁷󠁂󠀫󠁕󠁭󠁌󠁊󠁪󠀱󠀶󠁦󠁋󠀹󠁑󠁿*/ @"No tender is defined for the tender type %@", nil), tenderType]
                                      delegate:nil
                             cancelButtonTitle:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁄󠁴󠁍󠁉󠁣󠀫󠁺󠁔󠁗󠁦󠁹󠁡󠁯󠀷󠁨󠁔󠁩󠀸󠁯󠀫󠁨󠁘󠀹󠁱󠁌󠀶󠀸󠁿*/ @"OK", nil)
                             otherButtonTitles:nil];
            [alert show];
        } else if (([tender.maximumAmount floatValue] < [BasketController.sharedInstance.basketTotal floatValue]) &&
                   (!tender.promptForAmount)) {
            // basket total exceeds tender max limit
            UIAlertView *alert =
            [[UIAlertView alloc] initWithTitle:tender.tenderName
                                       message:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁊󠁯󠁥󠁍󠁶󠁸󠀱󠁇󠁳󠀯󠁦󠁚󠁍󠁭󠁘󠁃󠀯󠁷󠁧󠁚󠁖󠀳󠀳󠁍󠁸󠀲󠁍󠁿*/ @"Basket amount has exceeded maximum allowed for this tender.", nil)
                                      delegate:self
                             cancelButtonTitle:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁄󠁴󠁍󠁉󠁣󠀫󠁺󠁔󠁗󠁦󠁹󠁡󠁯󠀷󠁨󠁔󠁩󠀸󠁯󠀫󠁨󠁘󠀹󠁱󠁌󠀶󠀸󠁿*/ @"OK", nil)
                             otherButtonTitles:nil];
            [alert show];
        } else {
            // all ok, setup the payment
            self.payment.tender = tender;
            // clear the current values
            self.payment.amount = nil;
            
            [self assemblePayment];
        }
    } else {
        [POSFuncController presentPermissionAlert:POSPermissionAddTender
                                        withBlock:nil];
    }
}

- (void) assemblePayment
{
    BarcodeScannerController *scanner = [BarcodeScannerController sharedInstance];
    // get the active basket
    BasketController *basketController = [BasketController sharedInstance];
    MPOSBasket *basket = basketController.basket;
    
    // if we are tendering in a foreign currency
    DebugLog(@"currencyCode: %@ -> %@", basket.currencyCode, self.payment.tender.currencyCode);
    // and we don't have the total in this currency
    // TFS66978 - always update the foreign currency subtotal
    if ((self.payment.tender.currencyCode) && (
        ((basket.foreignSubtotal != nil) && (![basket.foreignSubtotal.currencyCode isEqual:self.payment.tender.currencyCode])) ||
        ((basket.foreignSubtotal == nil) && (![basket.currencyCode isEqual:self.payment.tender.currencyCode])))) {
        // get the total in this currency
        basket.currencyCode = self.payment.tender.currencyCode;
        [BasketController.sharedInstance checkoutBasket];
    } else if (self.payment.tender.promptForAmount && self.payment.amount == nil) {
        // user needs to specify a total
        [self.tenderAmountToolbar showTenderAmountKeyboardForPayment:self.payment
                                              withAmountEnteredBlock:^(NSDecimalNumber *amountTendered) {
                                                  self.payment.amount = amountTendered;
                                                  [self assemblePayment];
                                              }
                                                  andTerminatedBlock:^{
                                                      _payment = nil;
                                                  }];
    } else if (self.payment.tender.promptForReference && self.payment.reference == nil) {
        // hide the keyboard if visible
        
        // present the reference alert
        UIAlertView *alert =
        [[UIAlertView alloc] initWithTitle:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁕󠀸󠀴󠁵󠀶󠁖󠁺󠁈󠁭󠁖󠁴󠁁󠁪󠁱󠁺󠁑󠀱󠁭󠁄󠀲󠁧󠀸󠁈󠁦󠁐󠁓󠁫󠁿*/ @"Reference", nil)
                                   message:[NSString stringWithFormat:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁑󠁺󠁆󠁷󠀷󠁢󠁂󠁷󠁯󠁢󠁈󠀲󠁩󠀸󠁙󠁵󠁎󠁰󠁷󠀹󠁊󠁭󠀹󠁦󠁩󠀲󠁷󠁿*/ @"Please enter a reference for %@", nil), self.payment.tender.tenderName]
                                  delegate:[UIAlertView class]
                         cancelButtonTitle:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁩󠁢󠁺󠁬󠁏󠁶󠁣󠁧󠁫󠁍󠁋󠁧󠁫󠀱󠁶󠁐󠁐󠀹󠁘󠀴󠁘󠁡󠁕󠁭󠁹󠁷󠀰󠁿*/ @"Cancel", nil)
                         otherButtonTitles:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁄󠁴󠁍󠁉󠁣󠀫󠁺󠁔󠁗󠁦󠁹󠁡󠁯󠀷󠁨󠁔󠁩󠀸󠁯󠀫󠁨󠁘󠀹󠁱󠁌󠀶󠀸󠁿*/ @"OK", nil), nil];
        alert.alertViewStyle = UIAlertViewStylePlainTextInput;
        UITextField *textField = [alert textFieldAtIndex:0];
        textField.placeholder = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁕󠀸󠀴󠁵󠀶󠁖󠁺󠁈󠁭󠁖󠁴󠁁󠁪󠁱󠁺󠁑󠀱󠁭󠁄󠀲󠁧󠀸󠁈󠁦󠁐󠁓󠁫󠁿*/ @"Reference", nil);
        textField.text = nil;
        textField.keyboardType = UIKeyboardTypeAlphabet;
        textField.autocapitalizationType = UITextAutocapitalizationTypeNone;
        textField.autocorrectionType = UITextAutocorrectionTypeNo;
        textField.spellCheckingType = UITextSpellCheckingTypeNo;
        
        alert.dismissBlock = ^(int buttonIndex) {
            if (buttonIndex == 0) {
                // get the text entered
                if (![textField.text isEqualToString:@""]) {
                    // add the reference
                    self.payment.reference = textField.text;
                    [self assemblePayment];
                }
            }
        };
        
        alert.cancelBlock = ^ {
            _payment = nil;
        };
        
        alert.shouldEnableFirstOtherButtonBlock = ^BOOL {
            BOOL result = YES;
            
            // get the text entered
            if ([textField.text isEqualToString:@""]) {
                result = NO;
            }
            
            return result;
        };
        [alert show];
    } else if (self.payment.tender.promptForAuthCode && self.payment.authCode == nil) {
        // hide the keyboard if visible
        if (scanner.scanning) {
            [scanner stopScanning];
        }
        // present the auth code alert
        UIAlertView *alert =
        [[UIAlertView alloc] initWithTitle:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁏󠁗󠁹󠁬󠁫󠀫󠁎󠁒󠁅󠁑󠁳󠁖󠁶󠀱󠁺󠀶󠁺󠁬󠀹󠁤󠁬󠁢󠁐󠁙󠁈󠁵󠁑󠁿*/ @"Auth Code", nil)
                                   message:[NSString stringWithFormat:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁌󠁵󠀶󠀰󠁚󠁗󠁶󠀱󠁔󠁔󠁯󠁫󠁇󠀲󠀸󠀱󠁋󠁮󠁂󠁍󠁏󠁗󠁎󠀲󠁁󠀶󠁷󠁿*/ @"Please enter an auth code for %@", nil), self.payment.tender.tenderName]
                                  delegate:[UIAlertView class]
                         cancelButtonTitle:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁩󠁢󠁺󠁬󠁏󠁶󠁣󠁧󠁫󠁍󠁋󠁧󠁫󠀱󠁶󠁐󠁐󠀹󠁘󠀴󠁘󠁡󠁕󠁭󠁹󠁷󠀰󠁿*/ @"Cancel", nil)
                         otherButtonTitles:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁄󠁴󠁍󠁉󠁣󠀫󠁺󠁔󠁗󠁦󠁹󠁡󠁯󠀷󠁨󠁔󠁩󠀸󠁯󠀫󠁨󠁘󠀹󠁱󠁌󠀶󠀸󠁿*/ @"OK", nil), nil];
        alert.alertViewStyle = UIAlertViewStylePlainTextInput;
        UITextField *textField = [alert textFieldAtIndex:0];
        textField.placeholder = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁏󠁗󠁹󠁬󠁫󠀫󠁎󠁒󠁅󠁑󠁳󠁖󠁶󠀱󠁺󠀶󠁺󠁬󠀹󠁤󠁬󠁢󠁐󠁙󠁈󠁵󠁑󠁿*/ @"Auth Code", nil);
        textField.text = nil;
        textField.keyboardType = UIKeyboardTypeASCIICapable;
        textField.delegate = self;
        textField.autocapitalizationType = UITextAutocapitalizationTypeNone;
        textField.autocorrectionType = UITextAutocorrectionTypeNo;
        textField.spellCheckingType = UITextSpellCheckingTypeNo;
        
        OCGInputAccessoryView *inputAccessoryView = [[OCGInputAccessoryView alloc] init];
        OCGKeyboardTypeButtonItem *keyboardTypeButtonItem =
        [[OCGKeyboardTypeButtonItem alloc] initWithKeyboardTypes:kOCGKeyboardTypesDefaultUPC
                                                           style:UIBarButtonItemStylePlain
                                                          target:textField];
        
        inputAccessoryView.leftButtons = @[keyboardTypeButtonItem];
        textField.inputAccessoryView = inputAccessoryView;
        
        alert.dismissBlock = ^(int buttonIndex) {
            if (buttonIndex == 0) {
                // get the text entered
                if (![textField.text length] == 0) {
                    // add the reference
                    self.payment.authCode = textField.text;
                    [self assemblePayment];
                }
            }
        };
        
        alert.cancelBlock = ^ {
            _payment = nil;
        };
        
        alert.shouldEnableFirstOtherButtonBlock = ^BOOL {
            BOOL result = YES;
            
            // get the text entered
            if ([textField.text isEqualToString:@""]) {
                result = NO;
            }
            
            return result;
        };
        [alert show];
        
    } else if (self.payment.tender.promptForCardNumber &&
               self.payment.cardNumber == nil) {
        // hide the keyboard if visible
        // get the configured entry method
        enum PaymentCardCaptureMethod capture = [[BasketController sharedInstance] paymentCardCaptureMethodForTender:self.payment.tender];
        // proceed based on capture method
        switch (capture) {
            case PaymentCardCaptureMethodScan: {
                NSString *reason = nil;
                if (NSLocalizedString(self.payment.tender.tenderName, nil) != nil) {
                    reason =
                    [NSString stringWithFormat:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁘󠁑󠁸󠁑󠁃󠁰󠁈󠁇󠁏󠁊󠁹󠁢󠀵󠁵󠁏󠁥󠁑󠁙󠁇󠀱󠁧󠁉󠁗󠁳󠁅󠁦󠁣󠁿*/ @"Enter a %@ %@", nil),
                     NSLocalizedString(self.payment.tender.tenderName, nil), NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁘󠁉󠁣󠁲󠁰󠁰󠁍󠁸󠁇󠁑󠀰󠁕󠀶󠁏󠁔󠁏󠁦󠁳󠁡󠀸󠁆󠁳󠀶󠁲󠁡󠀰󠀰󠁿*/ @"Barcode", nil)];
                } else {
                    reason = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁵󠁏󠁨󠁄󠁴󠁂󠁄󠀰󠁁󠁃󠁃󠁯󠁖󠁑󠁖󠁭󠁴󠀴󠁱󠁆󠁈󠁘󠁔󠁫󠁚󠁩󠁧󠁿*/ @"Enter a Barcode or SKU", nil);
                }
                if (!scanner.scanning) {
                    [scanner beginScanningInViewController:self.view.window.rootViewController
                                                 forReason:reason
                                            withButtonSize:CGSizeZero
                                              withAddTitle:nil
                                                  mustScan:NO
                                       alphaNumericAllowed:[self alphaNumericAllowedForTender:self.payment.tender]
                                                 withBlock:^(NSString *barcode, ScannerEntryMethod entryMethod, SMBarcodeType barcodeType, BOOL *continueScanning) {
                                                     NSString *trimmedBarcode =
                                                     [barcode stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                                                     // make sure that we've got a scanned barcode
                                                     if ([trimmedBarcode length] > 0) {
                                                         // add the gift card
                                                         self.payment.cardNumber = trimmedBarcode;
                                                         [self assemblePayment];
                                                     }
                                                     *continueScanning = NO;
                                                 }
                                                terminated:^{
                                                    if (!self.payment.cardNumber)
                                                    {
                                                        _payment = nil;
                                                    }
                                                }
                                                     error:^(NSString *errorDesc) {
                                                         
                                                     }];
                } else {
                    [scanner stopScanning];
                }
                break;
            }
            case PaymentCardCaptureMethodSwipe: {
                // listen for notifications
                //                [[OCGPeripheralManager sharedInstance] registerForNotifications:self
                //                                                                   withSelector:@selector(handlePeripheralNotification:)];
                //
                //                // start the card reader
                //                [[OCGPeripheralManager sharedInstance].currentScannerDevice startMSRSwipeWithContext:kPAYMENT_REFRENCE_SWIPE];
                
                break;
            }
            default:
                break;
        }
    }
    else
    {
        if (scanner.scanning) {
            [scanner stopScanning];
        }
        
        // add the payment
        [[BasketController sharedInstance] paymentWithTender:self.payment.tender
                                                      amount:self.payment.amount
                                                   reference:self.payment.reference
                                                    authCode:self.payment.authCode
                                                  cardNumber:self.payment.cardNumber];
        _payment = nil;
    }
}

- (Payment *) payment
{
    if (_payment == nil) {
        _payment = [[Payment alloc] init];
    }
    return _payment;
}

- (void) handlePrintDataLines
{
    // check if there is a printer configured
    if ([PrinterController getConfiguredPrinterName] != nil) {
        // get the current basket
        BasketController *basketController = [BasketController sharedInstance];
        MPOSBasket *basket = basketController.basket;
        // if we are printing
        if (basketController.isPrinting) {
            // find an unprinted printing line
            MPOSPrintDataLine *lineToPrint = nil;
            for (MPOSBasketItem *line in basket.basketItems) {
                if ([line isKindOfClass:[MPOSPrintDataLine class]]) {
                    if (!((MPOSPrintDataLine *)line).printed) {
                        lineToPrint = (MPOSPrintDataLine *)line;
                        break;
                    }
                }
            }
            // if we found a printing line
            if (lineToPrint) {
                // check if the line hasn't errored
                if (lineToPrint.printResult.length == 0) {
                    // mark as auto printing
                    lineToPrint.printResult = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠀰󠁖󠁩󠁤󠁧󠁤󠁢󠀯󠁧󠁹󠁡󠁱󠁺󠁓󠁋󠁃󠁍󠁥󠁅󠁬󠁊󠀲󠀶󠁐󠁶󠁥󠁧󠁿*/ @"Printing...", nil);
                    // print the line
                    [basketController printPrintDataLine:lineToPrint
                                                 skipped:NO
                                               newStatus:@"PRINTED"];
                    // at this point, the job will be submitted to the server
                    // and a new basket containing more printdatalines (or not)
                    // will be processed
                }
            }
        }
    }
}

#pragma mark Methods

- (void) updateCurrentContext
{
    // default to no sale
    enum MPOSContext newContext = MPOSContextUnknown;
    
    BasketController *basketController = [BasketController sharedInstance];
        // do we have a basket?
        if ((basketController.basket != nil) &&
            (basketController.basket.basketID.length != 0)) {
            newContext = MPOSContextSale;
            // is the basket checked out?
            if (basketController.isPrinting) {
                newContext = MPOSContextSalePrinting;
            } else if (basketController.isCheckedOut) {
                newContext = MPOSContextCheckedOut;
            } else if (basketController.isTendered || basketController.isFinished) {
                newContext = MPOSContextSaleComplete;
            } else if (basketController.basket.basketItems.count == 0) {
                newContext = MPOSContextEmptySale;
            }
        } else {
            newContext = MPOSContextNoActiveSale;
        }
    
    // update
    self.currentContext = newContext;
}

- (enum MPOSUIState) updateUIStateForCurrentViewController
{
    enum MPOSUIState result = MPOSUIStateUnknown;
    
    // check if in MPOSUIStateBasket state
    if ([self.currentViewController isKindOfClass:[BasketViewController class]]) {
        result = MPOSUIStateBasket;
    } else if ([self.currentViewController isKindOfClass:[OCGMenuPageViewController class]]) {
        // MPOSUIStateMenu state
        result = MPOSUIStateMenu;
    } else if ([self.currentViewController isKindOfClass:[MoreOptionsTableViewController class]]) {
        // MPOSUIStateMore
        result = MPOSUIStateMore;
    } else if ([self.currentViewController isKindOfClass:[BasketItemDetailViewController class]]) {
        // MPOSUIStateSaleItemDetail
        result = MPOSUIStateSaleItemDetail;
    } else if ([self.currentViewController isKindOfClass:[SearchViewController class]]) {
        // MPOSUIStateSearch
        result = MPOSUIStateSearch;
    } else if ([self.currentViewController isKindOfClass:[BasketEditViewController class]]) {
        // MPOSUIStateEditBasket
        result = MPOSUIStateEditBasket;
    }

    if ([BarcodeScannerController sharedInstance].scanning) {
        result = MPOSUIStateScanning;
    }
    
    // check if this ui state caused the context to switch
    BOOL updateUI = [self uiStateHasNewContextMenu:result];
    // update
    self.currentUIState = result;
    // update the UI
    [self updateUIContextChanged:updateUI];
    
    return result;
}

- (BOOL) uiStateHasNewContextMenu:(enum MPOSUIState)uiState
{
    BOOL result = NO;
    
    // get the current
    SoftKeyContext *currentContext = [self contextMenuForContext:self.currentContext
                                                        andState:self.currentUIState];
    // and new context
    SoftKeyContext *context = [self contextMenuForContext:self.currentContext
                                                 andState:uiState];
    // check if we are changing context
    result = (![context.contextType isEqualToString:currentContext.contextType]);
    
    return result;
}

- (UIViewController *) getViewControllerForClass:(Class)vcClass;
{
    // init
    UIViewController *result = nil;
    
    // check if view controller is on the navigation stack
    for (UIViewController *vc in self.contextNavigationController.viewControllers) {
        if ([vc isKindOfClass:vcClass]) {
            result = vc;
            break;
        }
    }
    
    return result;
}

- (SoftKeyContextType) contextMenuForBasketItem:(MPOSBasketItem *)item
{
    SoftKeyContextType result = SoftKeyContextTypeUnknown;
    
    NSString *entity = NSStringFromClass([item class]);
    if ([entity isEqualToString:@"ReturnItem"]) {
        result = SoftKeyContextTypeMenuItemReturn;
    } else if ([entity isEqualToString:@"BasketCustomerCard"]) {
        result = SoftKeyContextTypeMenuItemCustomerCard;
    } else if ([entity isEqualToString:@"SaleItem"]) {
        result = SoftKeyContextTypeMenuItemSale;
    }
    
    return result;
}

- (SoftKeyContext *) contextMenuForContext:(enum MPOSContext)context
                                  andState:(enum MPOSUIState)state
{
    SoftKeyContext *result = nil;
    
    
    NSString *basketStatus = @"NO_SALE";
    MPOSBasket *basket = BasketController.sharedInstance.basket;
    if ((basket != nil) && (basket.basketID.length > 0)) {
        basketStatus = basket.status;
    }
    
    id object = nil;
    NSString *uiState = nil;
    
    if (state == MPOSUIStateSaleItemDetail) {
        if ([self.currentViewController isKindOfClass:[BasketItemDetailViewController class]]) {
            // get the current item
            BasketItemDetailViewController *detailsVC = (BasketItemDetailViewController *)self.currentViewController;
            // get the item detail menu based on the item type
            object = detailsVC.basketItem;
            
        }
    }
    else if (state == MPOSUIStateEditBasket) {
        object = basket;
        uiState = @"EDIT";
    } else if ([OCGEFTManager sharedInstance].processing) {
        object = basket;
        uiState = @"EFT";
        // add EFT transaction status
        switch ([OCGEFTManager sharedInstance].currentPayment.paymentStatus) {
            case kEFTPaymentStatusInitializing:
                uiState = [uiState stringByAppendingString:@"/INITIALIZING"];
                break;
            case kEFTPaymentStatusProcessing:
                uiState = [uiState stringByAppendingString:@"/PROCESSING"];
                break;
            case kEFTPaymentStatusSignatureRequired:
                uiState = [uiState stringByAppendingString:@"/SIGNATUREREQUIRED"];
                break;
            case kEFTPaymentStatusFinishing:
                uiState = [uiState stringByAppendingString:@"/FINISHING"];
                break;
            case kEFTPaymentStatusComplete: {
                uiState = [uiState stringByAppendingString:@"/COMPLETE"];
                // add the completion result
                switch ([OCGEFTManager sharedInstance].currentPayment.paymentCompleteReason) {
                    case kEFTPaymentCompleteReasonApproved:
                        uiState = [uiState stringByAppendingString:@"/APPROVED"];
                        break;
                    case kEFTPaymentCompleteReasonDeclined:
                        uiState = [uiState stringByAppendingString:@"/DECLINED"];
                        break;
                    case kEFTPaymentCompleteReasonCancelled:
                        uiState = [uiState stringByAppendingString:@"/CANCELLED"];
                        break;
                    case kEFTPaymentCompleteReasonError:
                        uiState = [uiState stringByAppendingString:@"/ERROR"];
                        break;
                    default:
                        break;
                }
                break;
            }
        }
    } else if (basket)
    {
        object = basket;
    }
    else
    {
        object = [[BasketAssociateDTO alloc] init];
    }
    
    if (object)
    {
        result = [BasketController.sharedInstance getLayoutForObject:object
                                                        basketStatus:basketStatus
                                                             element:@"BAR"
                                                               state:uiState];
        if ((result == nil) && (state != MPOSUIStateSaleItemDetail))
        {
            result = [BasketController.sharedInstance getLayoutForObject:[BasketAssociateDTO new]
                                                            basketStatus:@"NO_SALE"
                                                                 element:@"BAR"
                                                                   state:uiState];
        }
    }

    if (result == nil)
    {
        // try and get the menu for the basket status
        NSString *contextName = @"NO_SALE";
        MPOSBasket *basket = BasketController.sharedInstance.basket;
        if ((basket != nil) && (basket.basketID.length > 0)) {
            contextName = basket.status;
        }
        // append any UI state
        if (state == MPOSUIStateSaleItemDetail) {
            if ([self.currentViewController isKindOfClass:[BasketItemDetailViewController class]]) {
                // get the current item
                BasketItemDetailViewController *detailsVC = (BasketItemDetailViewController *)self.currentViewController;
                // get the item detail menu based on the item type
                contextName =
                [NSString stringWithFormat:@"ITEM_%@", [NSStringFromClass(detailsVC.basketItem.class) uppercaseString]];
            } else {
                contextName = nil;
            }
        } else if (state == MPOSUIStateEditBasket) {
            contextName =
            [NSString stringWithFormat:@"MENU_%@_EDIT", [contextName uppercaseString]];
        } else {
            contextName =
            [NSString stringWithFormat:@"MENU_%@", [contextName uppercaseString]];
        }
        // append EFT state
        if ([OCGEFTManager sharedInstance].processing) {
            NSString *EFTContextName = [contextName stringByAppendingString:@"_EFT"];
            // add EFT transaction status
            switch ([OCGEFTManager sharedInstance].currentPayment.paymentStatus) {
                case kEFTPaymentStatusInitializing:
                    EFTContextName = [EFTContextName stringByAppendingString:@"_INITIALIZING"];
                    break;
                case kEFTPaymentStatusProcessing:
                    EFTContextName = [EFTContextName stringByAppendingString:@"_PROCESSING"];
                    break;
                case kEFTPaymentStatusSignatureRequired:
                    EFTContextName = [EFTContextName stringByAppendingString:@"_SIGNATUREREQUIRED"];
                    break;
                case kEFTPaymentStatusFinishing:
                    EFTContextName = [EFTContextName stringByAppendingString:@"_FINISHING"];
                    break;
                case kEFTPaymentStatusComplete: {
                    EFTContextName = [EFTContextName stringByAppendingString:@"_COMPLETE"];
                    // add the completion result
                    NSString *transactionCompleteContextName = EFTContextName;
                    switch ([OCGEFTManager sharedInstance].currentPayment.paymentCompleteReason) {
                        case kEFTPaymentCompleteReasonApproved:
                            transactionCompleteContextName = [transactionCompleteContextName stringByAppendingString:@"_APPROVED"];
                            break;
                        case kEFTPaymentCompleteReasonDeclined:
                            transactionCompleteContextName = [transactionCompleteContextName stringByAppendingString:@"_DECLINED"];
                            break;
                        case kEFTPaymentCompleteReasonCancelled:
                            transactionCompleteContextName = [transactionCompleteContextName stringByAppendingString:@"_CANCELLED"];
                            break;
                        case kEFTPaymentCompleteReasonError:
                            transactionCompleteContextName = [transactionCompleteContextName stringByAppendingString:@"_ERROR"];
                            break;
                        default:
                            break;
                    }
                    // get the layout
                    result = [BasketController.sharedInstance getLayoutForContextNamed:transactionCompleteContextName];
                    break;
                }
            }
            // get the layout
            if ((result == nil) && (EFTContextName != nil)) {
                result = [BasketController.sharedInstance getLayoutForContextNamed:EFTContextName];
            }
            // fall back to EFT layout
            if ((result == nil) && (contextName != nil)) {
                result = [BasketController.sharedInstance getLayoutForContextNamed:[contextName stringByAppendingString:@"_EFT"]];
            }
        }
        
        // get the layout
        if ((result == nil) && (contextName != nil)) {
            result = [BasketController.sharedInstance getLayoutForContextNamed:contextName];
        }
        
        // fall back
        if ((result == nil) && (contextName != nil)) {
            WarningLog(@"LAYOUT DOES NOT EXIST (%@) USING FALLBACK", contextName);
            // determine the current menu from sales context and the UI state
            switch (context) {
                case MPOSContextNoActiveSale:
                    contextName = NSStringForSoftKeyContextType(SoftKeyContextTypeMenuNoSale);
                    break;
                case MPOSContextEmptySale:
                    contextName = NSStringForSoftKeyContextType(SoftKeyContextTypeMenuEmptySale);
                    break;
                case MPOSContextSale:
                    contextName = NSStringForSoftKeyContextType(SoftKeyContextTypeMenuInSale);
                    if (state == MPOSUIStateSaleItemDetail) {
                        if ([self.currentViewController isKindOfClass:[BasketItemDetailViewController class]]) {
                            // get the current item
                            BasketItemDetailViewController *detailsVC = (BasketItemDetailViewController *)self.currentViewController;
                            // get the item detail menu based on the item type
                            contextName = NSStringForSoftKeyContextType([self contextMenuForBasketItem:detailsVC.basketItem]);
                        } else {
                            contextName = NSStringForSoftKeyContextType(SoftKeyContextTypeUnknown);
                        }
                    } else if (state == MPOSUIStateEditBasket) {
                        contextName = NSStringForSoftKeyContextType(SoftKeyContextTypeMenuEditSale);
                    }
                    break;
                case MPOSContextCheckedOut:
                    contextName = NSStringForSoftKeyContextType(SoftKeyContextTypeMenuCheckedOut);
                    break;
                case MPOSContextSaleComplete:
                    contextName = NSStringForSoftKeyContextType(SoftKeyContextTypeMenuTenderedSale);
                    break;
                default:
                    break;
            }
            result = [BasketController.sharedInstance getLayoutForContextNamed:contextName];
        }
    }
    
    return result;
}

- (BOOL) presentingViewControllerOfClass:(Class)viewControllerClass
{
    BOOL result = NO;
    
    result = [[self.contextNavigationController topViewController] isKindOfClass:viewControllerClass];

    return result;
}

- (void) presentContextViewControllers:(NSArray *)viewControllers
                              animated:(BOOL)animated
{
    [self.contextNavigationController setViewControllers:viewControllers
                                                animated:animated];
}

- (void) presentContextViewController:(UIViewController *)viewController
                             animated:(BOOL)animated
               alreadyOnNavController:(BOOL)alreadyOnNavController
{
    if (!alreadyOnNavController) {
        // handle the item detail view slightly differently
        if ([viewController isKindOfClass:[BasketItemDetailViewController class]]) {
            [self.contextNavigationController pushViewController:viewController
                                                        animated:animated];
        } else {
            [self presentContextViewControllers:@[viewController]
                                       animated:animated];
        }
    } else {
        if (viewController != nil) {
            [self.contextNavigationController popToViewController:viewController
                                                         animated:animated];
        } else {
            [self.contextNavigationController popToRootViewControllerAnimated:animated];
        }
    }
}

- (void) dismissContextViewControllerAnimated:(BOOL)animated
{
    [self.contextNavigationController popViewControllerAnimated:animated];
}

- (SoftKeyContext *) currentContextSwipeGestureForDirection:(UISwipeGestureRecognizerDirection)direction
{
    SoftKeyContext *result = nil;
    
    NSString *basketStatus = @"NO_SALE";
    MPOSBasket *basket = BasketController.sharedInstance.basket;
    if ((basket != nil) && (basket.basketID.length > 0)) {
        basketStatus = basket.status;
    }
    
    NSString *element = nil;

    if (direction == UISwipeGestureRecognizerDirectionLeft) {
        element = @"LEFT_GESTURE";
    } else if (direction == UISwipeGestureRecognizerDirectionRight) {
        element = @"RIGHT_GESTURE";
    }

    if (element)
    {
        result = [BasketController.sharedInstance getLayoutForObject:basket ?: [[BasketAssociateDTO alloc] init]
                                                        basketStatus:basketStatus
                                                             element:element
                                                               state:nil];
    }
    
    if (result == nil)
    {
        // try and get the gesture for the basket status
        NSString *contextName = @"";
        MPOSBasket *basket = BasketController.sharedInstance.basket;
        if ((basket != nil) && (basket.basketID.length > 0)) {
            contextName = basket.status;
        }
        if (direction == UISwipeGestureRecognizerDirectionLeft) {
            contextName = [NSString stringWithFormat:@"MENU_%@_LEFT_GESTURE", [contextName uppercaseString]];
        } else if (direction == UISwipeGestureRecognizerDirectionRight) {
            contextName = [NSString stringWithFormat:@"MENU_%@_RIGHT_GESTURE", [contextName uppercaseString]];
        }
        result = [BasketController.sharedInstance getLayoutForContextNamed:contextName];
        
        // fall back
        if (result == nil) {
            WarningLog(@"LAYOUT DOES NOT EXIST (%@) USING FALLBACK", contextName);
            // determine the current layout from sales context
            if (direction == UISwipeGestureRecognizerDirectionLeft) {
                switch (self.currentContext) {
                    case MPOSContextNoActiveSale:
                        contextName = NSStringForSoftKeyContextType(SoftKeyContextTypeMenuNoSaleLeftGesture);
                        break;
                    case MPOSContextEmptySale:
                        contextName = NSStringForSoftKeyContextType(SoftKeyContextTypeMenuEmptySaleLeftGesture);
                        break;
                    case MPOSContextSale:
                        contextName = NSStringForSoftKeyContextType(SoftKeyContextTypeMenuInSaleLeftGesture);
                        break;
                    case MPOSContextCheckedOut:
                        contextName = NSStringForSoftKeyContextType(SoftKeyContextTypeMenuCheckedOutLeftGesture);
                        break;
                    case MPOSContextSaleComplete:
                        contextName = NSStringForSoftKeyContextType(SoftKeyContextTypeMenuTenderedSaleLeftGesture);
                        break;
                    default:
                        break;
                }
            } else if (direction == UISwipeGestureRecognizerDirectionRight) {
                switch (self.currentContext) {
                    case MPOSContextNoActiveSale:
                        contextName = NSStringForSoftKeyContextType(SoftKeyContextTypeMenuNoSaleRightGesture);
                        break;
                    case MPOSContextEmptySale:
                        contextName = NSStringForSoftKeyContextType(SoftKeyContextTypeMenuEmptySaleRightGesture);
                        break;
                    case MPOSContextSale:
                        contextName = NSStringForSoftKeyContextType(SoftKeyContextTypeMenuInSaleRightGesture);
                        break;
                    case MPOSContextCheckedOut:
                        contextName = NSStringForSoftKeyContextType(SoftKeyContextTypeMenuCheckedOutRightGesture);
                        break;
                    case MPOSContextSaleComplete:
                        contextName = NSStringForSoftKeyContextType(SoftKeyContextTypeMenuTenderedSaleRightGesture);
                        break;
                    default:
                        break;
                }
            }
            // get the soft key menu for this context
            result = [BasketController.sharedInstance getLayoutForContextNamed:contextName];
        }
    }
    
    return result;
}

- (void) performFunctionForSwipe:(UISwipeGestureRecognizer *)swipe
{
    // determine the pos func for this swipe direction
    // TFS69181 - don't allow gestures on item details
    if ((self.currentUIState == MPOSUIStateBasket) && (![BarcodeScannerController sharedInstance].scanning)) {
        UISwipeGestureRecognizerDirection direction = swipe.direction;
        SoftKeyContext *swipeContext = [self currentContextSwipeGestureForDirection:direction];
        __block SoftKey *swipeKey = nil;
        [BasketController.sharedInstance enumerateSoftKeysForKeyMenu:swipeContext.rootMenu
                                                      softKeyContext:swipeContext
                                                          usingBlock:^(SoftKey *softKey, BOOL *stop)
        {
            swipeKey = softKey;
            *stop = YES;
        }];

        if (swipeKey) {
            [self didSelectContextMenuItem:swipeKey
                                    source:SoftKeySelectionSourceSwipe];
        }
    }
}

-(void)ensureBasketViewControllerIsPresented
{
    if (![self presentingViewControllerOfClass:[BasketViewController class]]) {
        // get the view controller
        UIViewController *basketViewController =
        [self getViewControllerForClass:[BasketViewController class]];
        if (basketViewController == nil) {
            // not on the navigation controller, just set it
            [self presentContextViewController:self.basketViewController
                                      animated:NO
                        alreadyOnNavController:NO];
        } else {
            // already on the nav controller, pop to it
            [self presentContextViewController:self.basketViewController
                                      animated:YES
                        alreadyOnNavController:YES];
        }
    }
}

- (void) didSelectContextMenuItem:(SoftKey *)selectedMenuItem
                           source:(SoftKeySelectionSource)source
{
    if ([selectedMenuItem.keyType isEqualToString:@"UPC"]) {
        [[BasketController sharedInstance] addItemWithScanId:selectedMenuItem.upc
                                                       price:nil
                                                     measure:nil
                                                    quantity:nil
                                                 entryMethod:@"KEYED"
                                                      track1:nil
                                                      track2:nil
                                                      track3:nil
                                                 barcodeType:nil];
    } else if ([selectedMenuItem.keyType isEqualToString:@"MENU"]) {
        // show the more options menu controller
        // get the view controller
        MoreOptionsTableViewController *moreOptionsViewController = moreOptionsViewController =
        [[POSFuncController sharedInstance] buildMoreOptionsVCForKey:selectedMenuItem];
            // not on the navigation controller, just set it
            [self presentContextViewController:moreOptionsViewController
                                      animated:NO
                        alreadyOnNavController:NO];
        // set the selection color
        self.menuVC.selectedMenuColor = moreOptionsViewController.view.backgroundColor;
    } else if ([selectedMenuItem.keyType isEqualToString:@"POSFUNC"]) {
        POSFunc posFunc = POSFuncForString(selectedMenuItem.posFunc);
        switch (posFunc) {
            case POSFuncContextMenuItemBasket: {
                // if we are in a sale
                if ((self.currentContext == MPOSContextSale) ||
                    (self.currentContext == MPOSContextSalePrinting) ||
                    (self.currentContext == MPOSContextEmptySale) ||
                    (self.currentContext == MPOSContextNoActiveSale) ||
                    (self.currentContext == MPOSContextCheckedOut) ||
                    (self.currentContext == MPOSContextSaleComplete)) {
                    // show the basket view controller
                    if (source != SoftKeySelectionSourceSwipe) {
                        [self ensureBasketViewControllerIsPresented];
                    }
                    // set the selection color
                    self.menuVC.selectedMenuColor = self.basketViewController.view.backgroundColor;
                }
                break;
            }
                
            case POSFuncContextMenuItemMenu: {                
                // show the sales menu view controller
                // check we aren't on this view already
                if (![self presentingViewControllerOfClass:[OCGMenuPageViewController class]]) {
                    // get the view controller
                    OCGMenuPageViewController *menuViewController = (OCGMenuPageViewController *)
                    [self getViewControllerForClass:[OCGMenuPageViewController class]];
                    if (menuViewController == nil) {
                        // get the context
                        SoftKeyContext *context = nil;
                        if (selectedMenuItem.data.length == 0) {
                            context = [[BasketController sharedInstance] getLayoutForContextNamed:NSStringForSoftKeyContextType(SoftKeyContextTypeSales)];
                        } else {
                            context = [[BasketController sharedInstance] getLayoutForContextNamed:selectedMenuItem.data];
                        }
                        // create the controller
                        SalesMenuController *salesMenuViewController = [[SalesMenuController alloc] initWithRootMenu:context];
                        menuViewController = [salesMenuViewController menuPageViewControllerForRootMenu];
                        
                    }
                    // show it
                    if (source == SoftKeySelectionSourceBar) {
                        self.currentViewController.navigationItem.backBarButtonItem =
                        [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁱󠀹󠁯󠁆󠁉󠁏󠀳󠁎󠁈󠁘󠁉󠁷󠁋󠁁󠁦󠁐󠁗󠁶󠀫󠁒󠁩󠀶󠁺󠁕󠁄󠁚󠁁󠁿*/ @"Basket", nil)
                                                         style:UIBarButtonItemStylePlain
                                                        target:nil
                                                        action:nil];
                        [self.contextNavigationController pushViewController:menuViewController
                                                                    animated:NO];
                    } else {
                        [self presentContextViewController:menuViewController
                                                  animated:NO
                                    alreadyOnNavController:NO];
                    }
                    // set the selection color
                    self.menuVC.selectedMenuColor = menuViewController.view.backgroundColor;
                }
                break;
            }
                
            case POSFuncContextMenuItemSearch: {
                // show the search view controller
                // check we aren't on this view already
                if (![self presentingViewControllerOfClass:[SearchViewController class]]) {
                    // show it
                    [self presentContextViewController:self.searchViewController
                                              animated:NO
                                alreadyOnNavController:NO];
                    // set the selection color
                    self.menuVC.selectedMenuColor = self.searchViewController.view.backgroundColor;
                }
                break;
            }
                
            case POSFuncContextMenuItemScan: {
                // show the scanning view controller
                [self showBarcodeCaptureWithBarcodeUseType:BarcodeUseTypeItem];
                break;
            }
                
            case POSFuncAddDiscount: {
                if (self.currentUIState == MPOSUIStateEditBasket) {
                    BasketEditViewController *basketEditViewController = (BasketEditViewController *)self.currentViewController;
                    [[POSFuncController sharedInstance] presentBasketItemDiscount:basketEditViewController.selectedBasketItemIdArray];
                } else if (self.currentUIState == MPOSUIStateSaleItemDetail) {
                    BasketItemDetailViewController *basketItemDetailViewController = (BasketItemDetailViewController*) self.currentViewController;
                    [[POSFuncController sharedInstance] presentBasketItemDiscount:@[basketItemDetailViewController.basketItem.basketItemId]];
                } else {
                    if ([self.currentViewController respondsToSelector:@selector(selectedBasketItem)]) {
                        MPOSBasketItem *selectedItem = [self.currentViewController performSelector:@selector(selectedBasketItem)];
                        if (selectedItem) {
                            [[POSFuncController sharedInstance] presentBasketItemDiscount:@[selectedItem.basketItemId]];
                        }
                    }
                }
                break;
            }
                
            case POSFuncAddReturnItem:
                [[POSFuncController sharedInstance] scanReturnToBasket];
                break;
                
            case POSFuncAssignEmailAddress:
                [[POSFuncController sharedInstance] captureEmail];
                break;
                
            case POSFuncCancelSale:
                [[POSFuncController sharedInstance] cancelActiveBasket];
                break;
                
            case POSFuncCheckoutSale:
                [[POSFuncController sharedInstance] checkoutBasket];
                break;
                
            case POSFuncDeviceStationStatus:
                [[POSFuncController sharedInstance] deviceStationStatus];
                break;
                
            case POSFuncItemDetails:
                [[POSFuncController sharedInstance] startItemLookup];
                break;
                
            case POSFuncItemSearch:
                [[POSFuncController sharedInstance] presentItemSearch:selectedMenuItem];
                break;
                
            case POSFuncLogoutOperator:
                [[POSFuncController sharedInstance] logoutCurrentAssociate];
                break;
                
            case POSFuncLookupCustomer:
                [[POSFuncController sharedInstance] presentCustomerSearch];
                break;
                
            case POSFuncModifyQuantity: {
                if ([self.currentViewController isKindOfClass:[BasketItemDetailViewController class]]) {
                    BasketItemDetailViewController *basketItemDetailViewController = (BasketItemDetailViewController*) self.currentViewController;
                    [[POSFuncController sharedInstance] changeBasketItemQuantity:basketItemDetailViewController.basketItem];
                } else {
                    if ([self.currentViewController respondsToSelector:@selector(selectedBasketItem)]) {
                        MPOSBasketItem *selectedItem = [self.currentViewController performSelector:@selector(selectedBasketItem)];
                        if (selectedItem) {
                            [[POSFuncController sharedInstance] changeBasketItemQuantity:selectedItem];
                        }
                    }
                }
                break;
            }
                
            case POSFuncModifySaleDiscount:
                [[POSFuncController sharedInstance] presentBasketDiscounts];
                break;
                
            case POSFuncModifySalesperson: {
                if (self.currentUIState == MPOSUIStateEditBasket) {
                    BasketEditViewController *basketEditViewController = (BasketEditViewController *)self.currentViewController;
                    [[POSFuncController sharedInstance] assignSalesAssociateToBasketItems:basketEditViewController.selectedBasketItemIdArray];
                } else if (self.currentUIState == MPOSUIStateSaleItemDetail) {
                    BasketItemDetailViewController *basketItemDetailViewController = (BasketItemDetailViewController*) self.currentViewController;
                    [[POSFuncController sharedInstance] assignSalesAssociateToBasketItems:@[basketItemDetailViewController.basketItem.basketItemId]];
                } else {
                    if ([self.currentViewController respondsToSelector:@selector(selectedBasketItem)]) {
                        MPOSBasketItem *selectedItem = [self.currentViewController performSelector:@selector(selectedBasketItem)];
                        if (selectedItem) {
                            [[POSFuncController sharedInstance] assignSalesAssociateToBasketItems:@[selectedItem.basketItemId]];
                        }
                    }
                }
                break;
            }
                
            case POSFuncModifySettings:
                [[POSFuncController sharedInstance] presentSettingsView];
                break;

            case POSFuncOptionalModifiers: {
                if ([self.currentViewController isKindOfClass:[BasketItemDetailViewController class]]) {
                    BasketItemDetailViewController *basketItemDetailViewController = (BasketItemDetailViewController*) self.currentViewController;
                    [self presentOptionalModifiersForItem:basketItemDetailViewController.basketItem];
                } else {
                    if ([self.currentViewController respondsToSelector:@selector(selectedBasketItem)]) {
                        MPOSBasketItem *selectedItem = [self.currentViewController performSelector:@selector(selectedBasketItem)];
                        if (selectedItem) {
                            [self presentOptionalModifiersForItem:selectedItem];
                        }
                    }
                }
                break;
            }
                
            case POSFuncOpenCashDrawer:
                [[POSFuncController sharedInstance] openCashDrawer];
                break;
                
            case POSFuncModifyPrice: {
                if ([self.currentViewController isKindOfClass:[BasketItemDetailViewController class]]) {
                    BasketItemDetailViewController *basketItemDetailViewController = (BasketItemDetailViewController*) self.currentViewController;
                    [[POSFuncController sharedInstance] presentBasketItemPriceOverride:basketItemDetailViewController.basketItem];
                } else {
                    if ([self.currentViewController respondsToSelector:@selector(selectedBasketItem)]) {
                        MPOSBasketItem *selectedItem = [self.currentViewController performSelector:@selector(selectedBasketItem)];
                        if (selectedItem) {
                            [[POSFuncController sharedInstance] presentBasketItemPriceOverride:selectedItem];
                        }
                    }
                }
                break;
            }
                
            case POSFuncPrintGiftReceipt:
                if (self.currentUIState == MPOSUIStateEditBasket) {
                    BasketEditViewController *basketEditViewController = (BasketEditViewController *)self.currentViewController;
                    if (!basketEditViewController.errored) {
                        BOOL result = [[POSFuncController sharedInstance] markBasketItemsForGiftReceipt:basketEditViewController.selectedBasketItemIdArray];
                        if (result)
                        {
                            [self presentContextViewController:nil
                                                      animated:YES
                                        alreadyOnNavController:YES];
                        }
                    }
                } else if (self.currentUIState == MPOSUIStateSaleItemDetail) {
                    BasketItemDetailViewController *basketItemDetailViewController = (BasketItemDetailViewController*) self.currentViewController;
                    [[POSFuncController sharedInstance] printGiftReceiptForItem:basketItemDetailViewController.basketItem];
                } else if (self.currentUIState == MPOSUIStateMore) {
                    [[POSFuncController sharedInstance] printGiftReceiptForItem:nil];
                } else if (BasketLayoutGrid == BasketController.sharedInstance.clientSettings.parsedBasketLayout) {
                    MPOSBasketItem *selectedItem = nil;
                    if ([self.currentViewController respondsToSelector:@selector(selectedBasketItem)]) {
                        selectedItem = [self.currentViewController performSelector:@selector(selectedBasketItem)];
                    }
                    [[POSFuncController sharedInstance] printGiftReceiptForItem:selectedItem];
                }
                break;
                
            case POSFuncPrintLastTransaction:
                [[POSFuncController sharedInstance] reprintLastTx];
                break;
                
            case POSFuncRemoveDiscount: {
                if ([self.currentViewController isKindOfClass:[BasketItemDetailViewController class]]) {
                    BasketItemDetailViewController *basketItemDetailViewController = (BasketItemDetailViewController*) self.currentViewController;
                    [[POSFuncController sharedInstance] removeDiscountFromBasketItem:basketItemDetailViewController.basketItem];
                } else {
                    if ([self.currentViewController respondsToSelector:@selector(selectedBasketItem)]) {
                        MPOSBasketItem *selectedItem = [self.currentViewController performSelector:@selector(selectedBasketItem)];
                        if (selectedItem) {
                            [[POSFuncController sharedInstance] removeDiscountFromBasketItem:selectedItem];
                        }
                    }
                }
                break;
            }
                
            case POSFuncRemoveItem: {
                if (self.currentUIState == MPOSUIStateEditBasket) {
                    BasketEditViewController *basketEditViewController = (BasketEditViewController *)self.currentViewController;
                    [[POSFuncController sharedInstance] removeBasketItems:basketEditViewController.selectedBasketItemIdArray];
                } else if (self.currentUIState == MPOSUIStateSaleItemDetail) {
                    BasketItemDetailViewController *basketItemDetailViewController = (BasketItemDetailViewController*) self.currentViewController;
                    BOOL deleted =
                    [[POSFuncController sharedInstance] removeBasketItems:@[basketItemDetailViewController.basketItem.basketItemId]];
                    if (deleted) {
                        // pop back to the root view controller
                        [self dismissContextViewControllerAnimated:YES];
                    }
                } else {
                    if ([self.currentViewController respondsToSelector:@selector(selectedBasketItem)]) {
                        MPOSBasketItem *selectedItem = [self.currentViewController performSelector:@selector(selectedBasketItem)];
                        if (selectedItem) {
                            [[POSFuncController sharedInstance] removeBasketItems:@[selectedItem.basketItemId]];
                        }
                    }
                }
                break;
            }
                
            case POSFuncReprintReceipt:
                [[POSFuncController sharedInstance] presentReprint];
                break;
                
            case POSFuncResumeSale:
                [[POSFuncController sharedInstance] recallActiveBasket];
                break;
                
            case POSFuncReloadOptions:
                [[BasketController sharedInstance] forceServerConfigurationCacheRefresh];
                break;
                
            case POSFuncSwipeData:
                [[POSFuncController sharedInstance] startSwipingData];
                break;
                
            case POSFuncSuspendSale:
                [[POSFuncController sharedInstance] suspendActiveBasket];
                break;
                
            case POSFuncToggleTrainingMode:
                [[POSFuncController sharedInstance] toggleTrainingMode];
                break;
                
            case POSFuncTaxFreeSale:
                [[POSFuncController sharedInstance] presentTaxFreeSaleView];
                break;
                
            case POSFuncFinishSale:
                [[POSFuncController sharedInstance] finishBasket];
                break;
                
            case POSFuncUncheckoutSale:
                [[BasketController sharedInstance] cancelBasketCheckout];
                break;
                
            case POSFuncPrintPaperReceipt:
                break;
            
            case POSFuncPrintOrEmailReceipt:
                break;
                
            case POSFuncEmailReceipt:
                break;
                
            case POSFuncPrintSummaryReceipt:
                break;
                
            case POSFuncPrintPrintline:
            case POSFuncSkipPrintline: {
                MPOSPrintDataLine *selectedItem = nil;
                if (self.currentUIState == MPOSUIStateSaleItemDetail) {
                    BasketItemDetailViewController *basketItemDetailViewController = (BasketItemDetailViewController*) self.currentViewController;
                    selectedItem = (MPOSPrintDataLine *)basketItemDetailViewController.basketItem;
                    [self dismissContextViewControllerAnimated:YES];
                } else {
                    if ([self.currentViewController respondsToSelector:@selector(selectedBasketItem)]) {
                        selectedItem = (MPOSPrintDataLine *)[self.currentViewController performSelector:@selector(selectedBasketItem)];
                    }
                }
                if (selectedItem) {
                    [[BasketController sharedInstance] printPrintDataLine:selectedItem
                                                                  skipped:(posFunc == POSFuncSkipPrintline)
                                                                newStatus:selectedMenuItem.data];
                }
                break;
            }
                
            case POSFuncEftCancelTx: {
                [[OCGEFTManager sharedInstance] cancelTx];
                break;
            }
                
            case POSFuncEftFinishTx: {
                [[OCGEFTManager sharedInstance] endCurrentPayment];
                [self updateUIContextChanged:YES];
                break;
            }
                
            case POSFuncEftRetryStoredTx: {
                [[OCGEFTManager sharedInstance] retryStoredTransaction];
                [[OCGEFTManager sharedInstance] endCurrentPayment];
                [self updateUIContextChanged:YES];
                break;
            }
                
            case POSFuncEditBasket: {
                [self editButtonTapped:self];
                break;
            }
                
            case POSFuncAssignAffiliation: {
                // if a specific affiliation was specified
                if (selectedMenuItem.data.length > 0) {
                    NSDictionary *affiliationsByType = [BasketController.sharedInstance.affiliations OCGExtensions_dictionaryWithKeyPath:@"affiliationType"];
                    Affiliation *affiliation = affiliationsByType[selectedMenuItem.data];
                    [[POSFuncController sharedInstance] assignAffiliation:affiliation
                                                               completion:nil];
                } else {
                    // let the user pick one
                    OCGSelectViewController *selectViewController = [[OCGSelectViewController alloc] init];
                    selectViewController.title = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁖󠁲󠁥󠁴󠁍󠁅󠁤󠁉󠁺󠀸󠁴󠁲󠀰󠁲󠁮󠁮󠁁󠀵󠁌󠁑󠁊󠁮󠀫󠁵󠀷󠁒󠀴󠁿*/ @"Affiliations", nil);
                    selectViewController.options = @[[BasketController sharedInstance].affiliations];
                    selectViewController.titleForOption = ^(OCGSelectViewController *selectViewController, NSIndexPath *indexPath) {
                        if (selectViewController.navigationItem.leftBarButtonItem == nil) {
                            selectViewController.navigationItem.leftBarButtonItem =
                            [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁩󠁢󠁺󠁬󠁏󠁶󠁣󠁧󠁫󠁍󠁋󠁧󠁫󠀱󠁶󠁐󠁐󠀹󠁘󠀴󠁘󠁡󠁕󠁭󠁹󠁷󠀰󠁿*/ @"Cancel", nil)
                                                             style:UIBarButtonItemStylePlain
                                                            target:selectViewController
                                                            action:@selector(cancelSelection)];
                        }
                        Affiliation *affiliation = [BasketController sharedInstance].affiliations[indexPath.row];
                        return affiliation.affiliationDescription;
                    };
                    selectViewController.didSelectOption = ^(OCGSelectViewController *selectViewController, NSIndexPath *indexPath) {
                        [self dismissViewControllerAnimated:YES
                                                 completion:^{
                                                     if (indexPath) {
                                                         Affiliation *affiliation = [BasketController sharedInstance].affiliations[indexPath.row];
                                                         [[POSFuncController sharedInstance] assignAffiliation:affiliation
                                                                                                    completion:nil];
                                                     }
                                                 }];
                    };
                    UINavigationController *navController =
                    [[UINavigationController alloc] initWithRootViewController:selectViewController];
                    navController.modalPresentationStyle = UIModalPresentationFormSheet;
                    [self presentViewController:navController
                                       animated:YES
                                     completion:nil];
                }
                break;
            }
            
            default:
                [POSFuncController presentUnavailableWithBlock:NULL];
                break;
        }
    } else if ([selectedMenuItem.keyType isEqualToString:@"POS_TENDER"]) {
        [self tenderWithType:selectedMenuItem.tenderType];
    }
    
    
    if (selectedMenuItem.softKeyEvents.count > 0)
    {
        for (id softKeyEvent in selectedMenuItem.softKeyEvents)
        {
            if ([softKeyEvent isKindOfClass:UIContextEventDTO.class])
            {
                UIContextEventDTO *softKeyContextEvent = softKeyEvent;
                
                [FormViewController handleModelObject:nil contextName:softKeyContextEvent.context presentingViewController:self];
            }
            else if ([softKeyEvent isKindOfClass:POSFunctionSoftKeyEventDTO.class])
            {
                POSFunctionSoftKeyEventDTO* POSFunctionSoftKeyEvent = softKeyEvent;
                
                SoftKey *softKey = [[SoftKey alloc] init];
                softKey.context = selectedMenuItem.context;
                softKey.keyType = @"POSFUNC";
                softKey.posFunc = POSFunctionSoftKeyEvent.function;
                
                [self didSelectContextMenuItem:softKey source:source];
            }
            else
            {
                NSLog(@"Unknown soft key event: %@", softKeyEvent);
            }
        }
    }
}

- (IBAction) editButtonTapped:(id)sender
{
    // stop scanning
    BarcodeScannerController *scanner = [BarcodeScannerController sharedInstance];
    if (scanner.scanning) {
        [scanner stopScanning];
    }
    self.currentViewController.navigationItem.backBarButtonItem =
    [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁱󠀹󠁯󠁆󠁉󠁏󠀳󠁎󠁈󠁘󠁉󠁷󠁋󠁁󠁦󠁐󠁗󠁶󠀫󠁒󠁩󠀶󠁺󠁕󠁄󠁚󠁁󠁿*/ @"Basket", nil)
                                     style:UIBarButtonItemStylePlain
                                    target:nil
                                    action:nil];
    BasketEditViewController *editVC = [[BasketEditViewController alloc] initWithStyle:UITableViewStylePlain];
    [self.contextNavigationController pushViewController:editVC
                                         animated:YES];
}

#pragma mark - Properties

@synthesize currentViewController;
- (UIViewController *)currentViewController
{
    return self.contextNavigationController.topViewController;
}

- (UIViewController *) loginViewController
{
    UIViewController *loginViewController = nil;
    // determine if we need to show the login view for local login
    // or a web view for external login
    if (([BasketController sharedInstance].externalAuthEnabled)) {
        // use external auth
        ExternalAuthViewController *externalAuthController =
        [[ExternalAuthViewController alloc] init];
        externalAuthController.delegate = self;
        loginViewController = externalAuthController;
    } else {
        // show the login view
        LoginViewController *localLoginViewController = [[LoginViewController alloc] init];
        localLoginViewController.delegate = self;
        loginViewController = localLoginViewController;
    }
    
    return loginViewController;
}

@synthesize addItemError = _addItemError;
- (void) setAddItemError:(ServerError *)addItemError
{
    if (addItemError != nil) {
        _addItemError = addItemError;
        if (self.addItemVC == nil) {
            // show it
            self.addItemVC = [[BasketItemAddViewController alloc] initWithStyle:UITableViewStyleGrouped];
            __weak ContextViewController *contextVC = self;
            self.addItemVC.cancelBlock = ^() {
                BasketController.sharedInstance.isAddingBasketItem = NO;
            };
            if ([self.addItemVC handleAddBasketItemError:_addItemError]) {
                UINavigationController *addItemNav = [[UINavigationController alloc] initWithRootViewController:self.addItemVC];
                [self presentViewController:addItemNav
                                   animated:YES
                                 completion:nil];
            } else {
                BasketController.sharedInstance.isAddingBasketItem = NO;
                self.addItemVC = nil;
            }
        } else {
            [self.addItemVC handleAddBasketItemError:_addItemError];
        }
    } else {
        if (_addItemError != nil) {
            _addItemError = nil;
            // dismiss the add item vc if it it showing
            if (self.addItemVC != nil) {
                [self.addItemVC dismissViewControllerAnimated:YES
                                                   completion:^{
                                                       self.addItemVC = nil;
                                                       dispatch_async(dispatch_get_main_queue(), ^{
                                                           [self updateUIContextChanged:YES];
                                                       });
                                                   }];
            }
        }
    }
}

@synthesize menuVC = _menuVC;
- (ContextMenuViewController *) menuVC
{
    if (_menuVC == nil) {
        _menuVC = [[ContextMenuViewController alloc] init];
        ContextViewController *contextController = self;
        _menuVC.menuItemSelectedBlock = ^(SoftKey *selectedMenuItem, BOOL switchedMenus) {
            [contextController didSelectContextMenuItem:selectedMenuItem
                                                 source:switchedMenus?SoftKeySelectionSourceMenuChanged:SoftKeySelectionSourceBar];
        };
    }
    
    return _menuVC;
}

@synthesize contextNavigationController = _contextNavigationController;
- (void) setContextNavigationController:(UINavigationController *)contextNavigationController
{
    if (_contextNavigationController == nil) {
        _contextNavigationController = contextNavigationController;
        _contextNavigationController.delegate = self;
        _contextNavigationController.preferredContentSize = CGSizeMake(self.view.frame.size.width - 320.0f, 0.0f);
    }
}

@synthesize currentContext = _currentContext;
- (void) setCurrentContext:(enum MPOSContext)currentContext
{
    if (_currentContext != currentContext) {
        // update context
        _currentContext = currentContext;
        // update UI
        [self updateUIContextChanged:_currentContext != MPOSContextUnknown];
    } else {
        // update UI
        [self updateUIContextChanged:NO];
    }
}

#pragma mark - UIViewController

- (void) viewDidLoad
{
    [super viewDidLoad];
    
//    self.mainViewWrapper.backgroundColor = [UIColor redColor];
    
    // initialise
    self.currentContext = MPOSContextUnknown;
    self.currentUIState = MPOSUIStateUnknown;
    self.addItemError = nil;
    _loginConfigChanged = NO;
    _settingsPresented = NO;
    _loginPresented = NO;
    _modifierSelectionPresented = NO;
    self.loginSplitViewController = nil;
    self.loginNavigationController = nil;
    _barcodeUseType = BarcodeUseTypeItem;
    self.tenderAmountToolbar = [[OCGTenderAmountIntegratedTextFieldToolbar alloc] init];
    // setup notification listeners
    NSNotificationCenter *noteCenter = [NSNotificationCenter defaultCenter];
    // listen for taxfree baskets
    [noteCenter addObserver:self
                   selector:@selector(handleBasketControllerActiveTaxFreeChangeNotification:)
                       name:kBasketControllerTaxFreeChangeNotification
                     object:nil];
    // listen for all basket controller notifications
    [noteCenter addObserver:self
                   selector:@selector(handleBasketControllerNotification:)
                       name:nil
                     object:[BasketController sharedInstance]];
    // listen specifically for overridable errors from other objects
    [noteCenter addObserver:self
                   selector:@selector(handleBasketControllerNotification:)
                       name:kBasketControllerOverrideErrorNotification
                     object:nil];
    // listen to settings view notifications
    [noteCenter addObserver:self
                   selector:@selector(handleSettingsPresentedNotification:)
                       name:kAppDelegateSettingsPresentedNotification
                     object:nil];
    [noteCenter addObserver:self
                   selector:@selector(handleSettingsDismissedNotification:)
                       name:kAppDelegateSettingsDismissedNotification
                     object:nil];
    // listen for eft notifications
    [noteCenter addObserver:self
                   selector:@selector(handleEFTNotification:)
                       name:kEFTNotificationTransactionChanged
                     object:nil];
    [noteCenter addObserver:self
                   selector:@selector(handleEFTErrorNotification:)
                       name:kEFTNotificationError
                     object:nil];
    // present the context menu
    [self setSideViewControllers:@[self.menuVC]];
    // create the basket view controller
    self.basketViewController = [[BasketViewController alloc] init];
    // force the basket view to load
    self.basketViewController.view.tag = self.basketViewController.view.tag;
    // create the basket item view controller
    self.operationsViewController = [[BasketItemDetailViewController alloc] init];
    // preload the search view controller as it is quite slow to load
    self.searchViewController = [[SearchViewController alloc] init];
    // register swipe gestures
    self.leftSwipeGesture =
    [[UISwipeGestureRecognizer alloc] initWithTarget:self
                                              action:@selector(performFunctionForSwipe:)];
    self.leftSwipeGesture.direction = UISwipeGestureRecognizerDirectionLeft;
    self.rightSwipeGesture =
    [[UISwipeGestureRecognizer alloc] initWithTarget:self
                                              action:@selector(performFunctionForSwipe:)];
    self.rightSwipeGesture.direction = UISwipeGestureRecognizerDirectionRight;
    [self.view addGestureRecognizer:self.leftSwipeGesture];
    [self.view addGestureRecognizer:self.rightSwipeGesture];
}

- (void) viewWillAppear:(BOOL)animated
{
    // reload the menu
    [self.menuVC reloadMenu:NO];
    // register for scan notifications
    [[BarcodeScannerController sharedInstance] registerForNotifications:self
                                                           withSelector:@selector(handleBarcodeScannedNotification:)];
    [super viewWillAppear:animated];
    // check shared requirements
    [self checkSharedRequirements];
}

- (void) viewDidAppear:(BOOL)animated
{
    // get the active basket
    if (![[BasketController sharedInstance] needsAuthentication]) {
        if ([BasketController sharedInstance].basket == nil) {
            [[BasketController sharedInstance] getActiveBasket];
        }
    }
    // check for errors
    for (MPOSBasketItem *basketItem in BasketController.sharedInstance.basket.basketItems) {
        if (basketItem.basketItemCreatedError != nil) {
            self.addItemError = basketItem.basketItemCreatedError;
            break;
        }
    }
    
    [super viewDidAppear:animated];
}

- (void) viewWillLayoutSubviews
{
    [super viewWillLayoutSubviews];
}

- (void) viewWillTransitionToSize:(CGSize)size withTransitionCoordinator:(id<UIViewControllerTransitionCoordinator>)coordinator
{
    [coordinator animateAlongsideTransition:^(id<UIViewControllerTransitionCoordinatorContext> context) {
        [[BarcodeScannerController sharedInstance] updateCamerViewOrientation:[UIApplication sharedApplication].statusBarOrientation];
    }
                                 completion:^(id<UIViewControllerTransitionCoordinatorContext> context) {
                                     [[BarcodeScannerController sharedInstance] updateCamerViewOrientation:[UIApplication sharedApplication].statusBarOrientation];
                                     [self.menuVC reloadMenu:YES];
                                     self.contextNavigationController.preferredContentSize = CGSizeMake(self.view.frame.size.width - 320.0f, 0.0f);
                                     if ([self.contextNavigationController.topViewController isKindOfClass:[OCGMenuPageViewController class]]) {
                                         OCGMenuPageViewController *menuPage = (OCGMenuPageViewController *)self.contextNavigationController.topViewController;
                                         CGRect frame = menuPage.collectionView.frame;
                                         menuPage.collectionView.frame = frame;
                                         [menuPage calculatePageMetricsAndMenu];
                                         [menuPage.collectionView reloadData];
                                     }
                                 }];
    [super viewWillTransitionToSize:size
          withTransitionCoordinator:coordinator];
}

- (void) dealloc
{
    // stop listening for notifications
    [[BarcodeScannerController sharedInstance] unregisterForNotifications:self];
    [[BarcodeScannerController sharedInstance] enableHardwareButtonScanning:NO];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - UINavigationControllerDelegate

- (void) navigationController:(UINavigationController *)navigationController
        didShowViewController:(UIViewController *)viewController
                     animated:(BOOL)animated
{
    if ([viewController isKindOfClass:[UIViewController class]]) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self updateUIStateForCurrentViewController];
        });
    }
}

#pragma mark - LoginViewControllerDelegate

- (void) dismissModalLoginViewController:(id)sender
{
    [self dismissViewControllerAnimated:YES
                             completion:^{
        _loginPresented = NO;
        self.loginSplitViewController = nil;
        self.loginNavigationController = nil;
        [self checkSharedRequirements];
    }];
    [UIView animateWithDuration:0.5
                     animations:^{self.splitViewController.view.alpha  = 1.0;}];
}

#pragma mark - UITextFieldDelegate

- (BOOL) textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range
 replacementString:(NSString *)string
{
    NSString *acceptedCharacters = @"1234567890";
    NSMutableCharacterSet *acceptedCharacterSet = [NSMutableCharacterSet characterSetWithCharactersInString:acceptedCharacters];
    [acceptedCharacterSet formUnionWithCharacterSet:[NSCharacterSet lowercaseLetterCharacterSet]];
    [acceptedCharacterSet formUnionWithCharacterSet:[NSCharacterSet uppercaseLetterCharacterSet]];
    
    BOOL result = ([string rangeOfCharacterFromSet:[acceptedCharacterSet invertedSet]
                                           options:NSLiteralSearch].location == NSNotFound);
    
    NSString *text = [textField.text stringByReplacingCharactersInRange:range withString:string];
    
    if ([text length] > 40)
    {
        result = NO;
    }
    
    return result;
}

#pragma mark - OCGModifierViewControllerDelegate

- (void) modifierViewController:(OCGModifierViewController *)vc
              didMakeSelections:(NSArray *)selections
{
    NSMutableArray *itemsToAdd = [NSMutableArray arrayWithCapacity:selections.count];
    NSMutableArray *itemDescriptions = [NSMutableArray arrayWithCapacity:selections.count];
    for (GroupSelection *selection in selections) {
        for (ModifierSelection *modifier in selection.selections) {
            if (modifier.sku.length > 0) {
                for (NSUInteger quantity = 0; quantity < modifier.quantity; quantity++) {
                    [itemsToAdd addObject:modifier.sku];
                    if (modifier.freeText.length > 0) {
                        [itemDescriptions addObject:modifier.freeText];
                    } else {
                        [itemDescriptions addObject:@""];
                    }
                }
            } else {
                ErrorLog(@"Modifier %@ does not have an SKU", modifier.name);
            }
        }
    }
    DebugLog(@"User selected the following modifiers: %@", itemsToAdd);
    [self dismissViewControllerAnimated:YES
                             completion:^{
                                 if (itemsToAdd.count > 0) {
                                     // add items to the basket
                                     [[BasketController sharedInstance] addItemsWithScanIds:itemsToAdd
                                                                            andDescriptions:itemDescriptions];
                                 }
                                 // let the view controller be presented again
                                 _modifierSelectionPresented = NO;
                                 // if no items will be added
                                 if (itemsToAdd.count == 0) {
                                     // manually kick the mandatory modifier check
                                     dispatch_async(dispatch_get_main_queue(), ^{
                                         [self mandatoryModifierCheck];
                                     });
                                 }
                             }];
}

- (void)modifierViewControllerDidCancel:(OCGModifierViewController *)vc
{
    DebugLog(@"User cancelled modifier selection");
    [self dismissViewControllerAnimated:YES
                             completion:^{
                                 // let the view controller be presented again
                                 _modifierSelectionPresented = NO;
                                 // check for mandatory modifiers
                                 [self mandatoryModifierCheck];
                             }];
}

- (void)modifierViewControllerDidError:(OCGModifierViewController *)vc
{
    dispatch_async(dispatch_get_main_queue(), ^{
        if (_modifierSelectionPresented) {
            [self dismissViewControllerAnimated:YES
                                     completion:^{
                                         _modifierSelectionPresented = NO;
                                         UIAlertView *alert =
                                         [[UIAlertView alloc] initWithTitle:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁩󠁮󠁔󠀰󠁕󠁲󠁩󠀱󠁩󠁘󠀳󠁥󠁘󠁖󠀫󠀲󠁬󠁔󠁑󠁰󠀵󠀶󠁁󠁄󠁤󠁂󠁑󠁿*/ @"Invalid modifier configuration", nil)
                                                                    message:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁅󠁉󠁤󠁔󠀷󠁆󠀸󠁺󠁙󠁅󠁤󠀶󠁔󠁸󠁵󠁭󠁧󠁧󠁳󠁩󠁭󠁣󠁪󠁓󠁺󠁭󠀸󠁿*/ @"Please check your modifier configuration. This transaction cannot continue and will be cancelled.", nil)
                                                                   delegate:[UIAlertView class]
                                                          cancelButtonTitle:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁄󠁴󠁍󠁉󠁣󠀫󠁺󠁔󠁗󠁦󠁹󠁡󠁯󠀷󠁨󠁔󠁩󠀸󠁯󠀫󠁨󠁘󠀹󠁱󠁌󠀶󠀸󠁿*/ @"OK", nil)
                                                          otherButtonTitles:nil];
                                         alert.cancelBlock = ^() {
                                             // cancel the basket
                                             [[BasketController sharedInstance] clearBasket];
                                         };
                                         [alert show];
                                     }];
        } else {
            UIAlertView *alert =
            [[UIAlertView alloc] initWithTitle:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁩󠁮󠁔󠀰󠁕󠁲󠁩󠀱󠁩󠁘󠀳󠁥󠁘󠁖󠀫󠀲󠁬󠁔󠁑󠁰󠀵󠀶󠁁󠁄󠁤󠁂󠁑󠁿*/ @"Invalid modifier configuration", nil)
                                       message:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁅󠁉󠁤󠁔󠀷󠁆󠀸󠁺󠁙󠁅󠁤󠀶󠁔󠁸󠁵󠁭󠁧󠁧󠁳󠁩󠁭󠁣󠁪󠁓󠁺󠁭󠀸󠁿*/ @"Please check your modifier configuration. This transaction cannot continue and will be cancelled.", nil)
                                      delegate:[UIAlertView class]
                             cancelButtonTitle:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁄󠁴󠁍󠁉󠁣󠀫󠁺󠁔󠁗󠁦󠁹󠁡󠁯󠀷󠁨󠁔󠁩󠀸󠁯󠀫󠁨󠁘󠀹󠁱󠁌󠀶󠀸󠁿*/ @"OK", nil)
                             otherButtonTitles:nil];
            alert.cancelBlock = ^() {
                // cancel the basket
                [[BasketController sharedInstance] clearBasket];
            };
            [alert show];
        }
    });
}

#pragma mark - Methods

+ (ContextViewController *) sharedInstance
{
    ContextViewController *result = nil;
    
    // get the app delegate
    AppDelegate *delegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    // get the root view controller
    if ([UIDevice currentDevice].systemVersion.floatValue >= 7.0) {
        result = (ContextViewController *)((OCGSplitViewController *)delegate.window.rootViewController).mainViewController;
    } else {
        result = (ContextViewController *)(OCGSplitViewController *)delegate.window.rootViewController;
    }
    
    return result;
}

- (void) updateUIContextChanged:(BOOL)contextChanged
{
    // reset menu state
    self.menuVC.enabled = YES;
    // set the selection color
    self.menuVC.selectedMenuColor = self.currentViewController.view.backgroundColor;
    // show or hide the navigation bar
    [self.contextNavigationController setNavigationBarHidden:NO];
    // if the mPOS context has changed
    if (contextChanged) {
        // enable/disable the scanner buttons
        if (BasketController.sharedInstance.canScanToBasket) {
            // enable the barcode scanner buttons
            dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(kScannerButtonEnableDelayInSeconds * NSEC_PER_SEC));
            dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
                [[BarcodeScannerController sharedInstance] enableHardwareButtonScanning:YES];
            });
        } else {
            // disable the barcode scanner buttons
            [[BarcodeScannerController sharedInstance] enableHardwareButtonScanning:NO];
        }
        // get the new menu layout
        SoftKeyContext *menu = [self contextMenuForContext:self.currentContext
                                                  andState:self.currentUIState];
        // if we just switched to a no sale state - make sure we show the basket
        ContextMenuItemSelectedBlockType block = self.menuVC.menuItemSelectedBlock;
        if (self.currentContext == MPOSContextNoActiveSale) {
            ContextViewController *contextController = self;
            self.menuVC.menuItemSelectedBlock = ^(SoftKey *selectedMenuItem, BOOL switchedMenus) {
                [contextController didSelectContextMenuItem:selectedMenuItem
                                                     source:SoftKeySelectionSourceDefault];
            };
        }
        // set the current menu
        [self.menuVC setMenu:menu
           andSelectFirstTab:(self.currentUIState != MPOSUIStateMenu)];
        // restore the block
        self.menuVC.menuItemSelectedBlock = block;
        // setting the menu here no longer causes a didSelectContextMenuItem
    } else if (self.currentContext == MPOSContextCheckedOut) {
        [self.menuVC reloadMenu:NO];
    }
    if ((self.currentUIState == MPOSUIStateBasket) ||
        (self.currentUIState == MPOSUIStateMenu) ||
        (self.currentUIState == MPOSUIStateSearch) ||
        (self.currentUIState == MPOSUIStateMore) ||
        (self.currentUIState == MPOSUIStateScanning)) {
        MPOSBasket *basket = BasketController.sharedInstance.basket;
        if (basket != nil) {
            if (self.currentContext == MPOSContextNoActiveSale) {
                if (self.currentUIState != MPOSUIStateSearch) {
                    self.currentViewController.navigationItem.leftBarButtonItem = nil;
                }
            }
            // display the total
            if (self.currentUIState == MPOSUIStateBasket)
            {
                NSString *totalAmount = basket.amountDue.display;
                if (totalAmount.length > 0 && self.currentContext == MPOSContextSale) {
                    self.currentViewController.navigationItem.title = totalAmount;
                } else {
                    self.currentViewController.navigationItem.title = self.currentViewController.title;
                }
            }
            
            if (self.currentContext != MPOSContextSaleComplete) {
                if (self.currentContext == MPOSContextCheckedOut) {
                    self.currentViewController.navigationItem.leftBarButtonItem = nil;
                    [self.currentViewController.navigationItem setHidesBackButton:YES];
                    if ([self.currentViewController isKindOfClass:[BasketViewController class]]) {
                        BasketViewController *checkoutViewController = ((BasketViewController *)self.currentViewController);
//                        checkoutViewController.enabled = YES;
                        // add the foreign currency item
                        UIBarButtonItem *foreignCurrencyItem =
                        [[UIBarButtonItem alloc] initWithTitle:@""
                                                         style:UIBarButtonItemStylePlain
                                                        target:nil
                                                        action:nil];
                        // set the font and color
                        NSDictionary *attributes =
                        @{NSFontAttributeName : [[CRSSkinning currentSkin] getFontWithName:@"Foreign Currency Font"],
                          NSForegroundColorAttributeName : [[CRSSkinning currentSkin] getColorWithName:@"Navigation Bar Foreground Color"]};
                        [foreignCurrencyItem setTitleTextAttributes:attributes
                                                           forState:UIControlStateNormal];
                        // make sure it isn't tappable
                        foreignCurrencyItem.enabled = NO;
                        // update the title
                        // if we are tendering
                        if (self.payment.tender != nil) {
                            // if we are adding a foreign currency
                            if (self.payment.tender.isForeignCurrency) {
                                // update/show the foreign currency total
                                [foreignCurrencyItem setTitle:basket.foreignSubtotal.foreignAmount.display];
                            } else {
                                // hide the label
                                [foreignCurrencyItem setTitle:nil];
                            }
                        }
                        self.currentViewController.navigationItem.rightBarButtonItem = foreignCurrencyItem;
                    }
                } else if (self.currentContext == MPOSContextSalePrinting) {
                    self.currentViewController.navigationItem.leftBarButtonItem = nil;
                }
            } else {
                // disable the view controllers
                self.menuVC.enabled = NO;
                if ([self.currentViewController isKindOfClass:[BasketViewController class]]) {
//                    ((BasketViewController *)self.currentViewController).enabled = NO;
//                    [(BasketViewController *)self.currentViewController hidePrompt];
                }

                [self.currentViewController.navigationItem setHidesBackButton:YES];
                // enabled the context menu
                self.menuVC.enabled = YES;
                // check if the user has selected a receipt option
                if (([OCGReceiptManager configuredReceiptManagerType] != ReceiptManagerTypeServer) ||
                    basket.emailReceipt || basket.enablePrint  || basket.printSummaryReceipt) {
                } else {
                    self.currentViewController.navigationItem.rightBarButtonItem = nil;
                    // show the receipt options prompt
                    if ([self.currentViewController isKindOfClass:[BasketViewController class]]) {
//                        [(BasketViewController *)self.currentViewController showReceiptOptionsPrompt];
                    }
                }
            }
        }
    }
}

- (void) didSelectContextMenuItem:(SoftKey *)selectedMenuItem
{    
    [self didSelectContextMenuItem:selectedMenuItem
                            source:SoftKeySelectionSourceDefault];
}

- (BOOL) resetSharedRequirements
{
    [CRSLocationController setLocationKey:nil];
    [TillSetupController resetTillNumber];
    [[BasketController sharedInstance] setNeedsAuthentication];
    return [self checkSharedRequirements];
}

- (BOOL) checkSharedRequirements
{
    if ([[BasketController sharedInstance] needsAuthentication]) {
        dispatch_async(dispatch_get_main_queue(), ^{
            NSString *locationkey = [CRSLocationController getLocationKey];
            NSString *tillNumber = [TillSetupController getTillNumber];
            
            BOOL needsSettings = (locationkey == nil || tillNumber == nil);
            
            [self presentLoginViewAnimated:!needsSettings];
            
            if (needsSettings) {
                // get the app delegate
                AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
                [appDelegate presentSettingsViewAnimated:NO
                                          viewController:self.loginSplitViewController?self.loginSplitViewController:self];
            }
        });
        
        return NO;
    }
    
    // requirements fullfilled
    return YES;
}

- (void) presentLoginViewAnimated:(BOOL)animated
{
    __weak ContextViewController *weakSelf = (ContextViewController *) self;
    if (self.presentedViewController && !self.presentedViewController.isBeingDismissed && !_loginPresented)
    {
        [self.presentedViewController dismissViewControllerAnimated:YES // If set to NO the new presented view controller vanishes
                                                         completion:^{
                                                             [weakSelf presentLoginViewAnimated:animated];
                                                         }];
        return;
    }
    
    [UIAlertView OCGAdditions_cancelAnimated:NO];
    
    if (!_loginPresented && self.view.window) {
        _loginPresented = YES;
        // TFS70303 - Make sure the scanner is disabled when logging on
        BarcodeScannerController *scanner = [BarcodeScannerController sharedInstance];
        if (scanner.scanning) {
            [scanner stopScanning];
        }
        self.loginNavigationController = [[UINavigationController alloc] init];
        self.loginNavigationController.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
        [self.loginNavigationController setViewControllers:@[self.loginViewController]
                                                  animated:NO];
        self.loginSplitViewController = [[OCGSplitViewController alloc] init];
        self.loginSplitViewController.modalPresentationStyle = UIModalPresentationFormSheet;
        self.loginSplitViewController.displayMode = OCGSplitViewSideDisplayModeTop;
        self.loginSplitViewController.mainViewController = self.loginNavigationController;
        StatusViewController *statusVC = [[StatusViewController alloc] init];
        statusVC.view.tag = statusVC.view.tag;
        [self.loginSplitViewController setSideViewControllers:@[statusVC]];
        
        [self presentViewController:self.loginSplitViewController
                           animated:animated
                         completion:NULL];
        if (animated)
        {
            [UIView animateWithDuration:0.5
                             animations:^{self.splitViewController.view.alpha  = 0;}];
        }
        else
        {
            self.splitViewController.view.alpha  = 0;
        }

    }
}

- (void) showBarcodeCaptureWithBarcodeUseType:(BarcodeUseType)barcodeUseType
{
    // make a note of the use type
    _barcodeUseType = barcodeUseType;
    
    BarcodeScannerController *controller = [BarcodeScannerController sharedInstance];
    // are we scanning?
    if (!controller.scanning) {
        // no, start scanning, if we can
        if (BasketController.sharedInstance.canScanToBasket) {
            [controller beginScanningInViewController:self.view.window.rootViewController
                                            forReason:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁘󠁉󠁣󠁲󠁰󠁰󠁍󠁸󠁇󠁑󠀰󠁕󠀶󠁏󠁔󠁏󠁦󠁳󠁡󠀸󠁆󠁳󠀶󠁲󠁡󠀰󠀰󠁿*/ @"Barcode", nil)
                                       withButtonSize:[self.menuVC keySizeForCurrentPage]
                                         withAddTitle:nil
                                             mustScan:NO
                                  alphaNumericAllowed:YES
                                            withBlock:^(NSString *barcode, ScannerEntryMethod entryMethod, SMBarcodeType barcodeType, BOOL *continueScanning) {
                                                // debug assertions
                                                NSAssert(BasketController.sharedInstance != nil,
                                                         @"The data source has not been initialized.");
                                                NSAssert(BasketController.sharedInstance.basket != nil,
                                                         @"The active basket has not yet been initialized.");
                                                NSAssert(barcode != nil,
                                                         @"The reference to the scanned barcode has not been initialized.");
                                                NSAssert([[barcode stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length] > 0,
                                                         @"The returned barcode does not contain any characters.");
                                                
                                                if (BasketController.sharedInstance.canScanToBasket) {
                                                    // show the basket
                                                    [self.menuVC selectFirstTab];
                                                    // pause scanning while the barcode is processing
                                                    [[BarcodeScannerController sharedInstance] pauseScanning];
                                                    
                                                    // make sure that we've got a scanned barcode
                                                    if ([[barcode stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length] <= 0) {
                                                        // something probably went wrong, stop scanning
                                                        *continueScanning = NO;
                                                    } else {
                                                        *continueScanning = [controller canScanBarcodeInBatch];
                                                        
                                                        BOOL hasHandledBarcodeScan =
                                                        [self handleBarcodeScan:barcode
                                                                    entryMethod:entryMethod
                                                                    barcodeType:barcodeType];
                                                        if (hasHandledBarcodeScan) {
                                                            if (_barcodeUseType == BarcodeUseTypeReturn) {
                                                                *continueScanning = NO;
                                                            } else if (_barcodeUseType == BarcodeUseTypeLookup) {
                                                                *continueScanning = NO;
                                                            }
                                                            
                                                            // make sure the user is logged on
                                                            if ([[BasketController sharedInstance] needsAuthentication]) {
                                                                // not logged on, stop scanning
                                                                *continueScanning = NO;
                                                            }
                                                        }
                                                        else
                                                        {
                                                            [[BarcodeScannerController sharedInstance] resumeScanningWithDelay:YES];
                                                        }
                                                        
                                                    }
                                                } else {
                                                    *continueScanning = YES;
                                                    ErrorLog(@"An item was scanned while the data source prohibits scanning items.");
                                                }
                                            }
                                           terminated:^{
                                               [self updateUIStateForCurrentViewController];
                                           }
                                                error:^(NSString *errorDesc) {
                                                    if (errorDesc) {
                                                        // display an alert
                                                        UIAlertView *alert =
                                                        [[UIAlertView alloc] initWithTitle:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁯󠀫󠁋󠁆󠁢󠁚󠁱󠁩󠁈󠁕󠀲󠀸󠁫󠁲󠁧󠁦󠀲󠁴󠁘󠁣󠁈󠁎󠀶󠀲󠁯󠀶󠁳󠁿*/ @"Scanning Error", nil)
                                                                                   message:errorDesc
                                                                                  delegate:nil
                                                                         cancelButtonTitle:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁄󠁴󠁍󠁉󠁣󠀫󠁺󠁔󠁗󠁦󠁹󠁡󠁯󠀷󠁨󠁔󠁩󠀸󠁯󠀫󠁨󠁘󠀹󠁱󠁌󠀶󠀸󠁿*/ @"OK", nil)
                                                                         otherButtonTitles:nil];
                                                        [alert show];
                                                    }
                                                }];
        }
    } else {
        // yes, stop scanning if not in manual scanning
        if (controller.sourceType != SMScannerSourceTypeManual) {
            [controller stopScanning];
        }
    }
}

- (void) showDetailsForItem:(MPOSBasketItem *)basketItem
{
    // only show details on iPod/iPhone
    if (BasketLayoutTable == BasketController.sharedInstance.clientSettings.parsedBasketLayout
        && (basketItem != nil)
        && (![BarcodeScannerController sharedInstance].scanning)) {
        
        SoftKeyContext *softKeyContext = [self contextLayoutMenuForBasketItem:basketItem];

        if ([[softKeyContext.rootMenu componentsSeparatedByString:@" "] count] > 1)
        {
            // show the details view
            self.operationsViewController.basketItem = basketItem;
            
            self.currentViewController.navigationItem.backBarButtonItem =
            [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁱󠀹󠁯󠁆󠁉󠁏󠀳󠁎󠁈󠁘󠁉󠁷󠁋󠁁󠁦󠁐󠁗󠁶󠀫󠁒󠁩󠀶󠁺󠁕󠁄󠁚󠁁󠁿*/ @"Basket", nil)
                                             style:UIBarButtonItemStylePlain
                                            target:nil
                                            action:nil];
            
            [self presentContextViewController:self.operationsViewController
                                      animated:YES
                        alreadyOnNavController:NO];
        }
    }
}

- (SoftKeyContext *) contextLayoutMenuForCurrentContext
{
    SoftKeyContext *result = nil;
    
    NSString *basketStatus = @"NO_SALE";
    MPOSBasket *basket = BasketController.sharedInstance.basket;
    if ((basket != nil) && (basket.basketID.length > 0)) {
        basketStatus = basket.status;
    }
    NSString *element = @"LAYOUT";
    
    result = [BasketController.sharedInstance getLayoutForObject:basket ?: [[BasketAssociateDTO alloc] init]
                                                    basketStatus:basketStatus
                                                         element:element
                                                           state:nil];

    
    
    if (result == nil)
    {
        // try and get the layout for the basket status
        NSString *contextName = @"NO_SALE";
        MPOSBasket *basket = BasketController.sharedInstance.basket;
        if ((basket != nil) && (basket.basketID.length > 0)) {
            contextName = basket.status;
        }
        contextName = [NSString stringWithFormat:@"MENU_%@_LAYOUT", [contextName uppercaseString]];
        result = [BasketController.sharedInstance getLayoutForContextNamed:contextName];
        
        // fall back
        if (result == nil) {
            WarningLog(@"LAYOUT DOES NOT EXIST (%@) USING FALLBACK", contextName);
            // determine the current layout from sales context
            switch (self.currentContext) {
                case MPOSContextNoActiveSale:
                    contextName = NSStringForSoftKeyContextType(SoftKeyContextTypeMenuNoSaleLayout);
                    break;
                case MPOSContextEmptySale:
                    contextName = NSStringForSoftKeyContextType(SoftKeyContextTypeMenuEmptySaleLayout);
                    break;
                case MPOSContextSale:
                    contextName = NSStringForSoftKeyContextType(SoftKeyContextTypeMenuInSaleLayout);
                    break;
                case MPOSContextCheckedOut:
                    contextName = NSStringForSoftKeyContextType(SoftKeyContextTypeMenuCheckedOutLayout);
                    break;
                case MPOSContextSaleComplete:
                    contextName = NSStringForSoftKeyContextType(SoftKeyContextTypeMenuTenderedSaleLayout);
                    break;
                default:
                    break;
            }
            // get the soft key menu for this context
            result = [[BasketController sharedInstance] getLayoutForContextNamed:contextName];
            if ((result == nil) && (self.currentContext == MPOSContextEmptySale)) {
                result =
                [[BasketController sharedInstance] getLayoutForContextNamed:NSStringForSoftKeyContextType(SoftKeyContextTypeMenuInSaleLayout)];
            }
        }
    }
    
    return result;
}

- (SoftKeyContext *) contextLayoutMenuForBasketItem:(MPOSBasketItem *)item
{
    SoftKeyContext *result = nil;
    
    NSString *element = @"LAYOUT";
    
    if ([item respondsToSelector:@selector(uiState)] && item.uiState.length)
    {
        element = [element stringByAppendingPathComponent:item.uiState];
    }
    
    result = [BasketController.sharedInstance getLayoutForObject:item
                                                    basketStatus:item.basket.status
                                                         element:element
                                                           state:nil];
    
    if (result == nil)
    {
        // try and get the layout for the basket status
        NSString *contextName = NSStringFromClass(item.class);
        
        if (item.uiState.length)
        {
            // TFS71312 - mPOS wasn't falling back to the non-uistate layout context name properly
            NSString *contextUIStateName = [NSString stringWithFormat:@"MENU_ITEM_%@_%@_LAYOUT", [contextName uppercaseString], item.uiState];
            result = [BasketController.sharedInstance getLayoutForContextNamed:contextUIStateName];
        }
        
        if (result == nil)
        {
            contextName = [NSString stringWithFormat:@"MENU_ITEM_%@_LAYOUT", [contextName uppercaseString]];
            result = [BasketController.sharedInstance getLayoutForContextNamed:contextName];
        }
        
        // fall back
        if (result == nil) {
            WarningLog(@"LAYOUT DOES NOT EXIST (%@) USING FALLBACK", contextName);
            if ([item isKindOfClass:[RESTObject class]]) {
                NSString *entity = NSStringFromClass([item class]);
                if ([entity isEqualToString:@"ReturnItem"]) {
                    contextName = NSStringForSoftKeyContextType(SoftKeyContextTypeMenuItemReturnLayout);
                } else if ([entity isEqualToString:@"BasketCustomerCard"]) {
                    contextName = NSStringForSoftKeyContextType(SoftKeyContextTypeMenuItemCustomerCardLayout);
                } else if ([entity isEqualToString:@"SaleItem"]) {
                    contextName = NSStringForSoftKeyContextType(SoftKeyContextTypeMenuItemSaleLayout);
                } else if ([entity hasPrefix:@"BasketRewardSale"]) {
                    if ([entity isEqualToString:@"BasketRewardSalePromotionAmount"]) {
                        contextName = NSStringForSoftKeyContextType(SoftKeyContextTypeMenuItemPromotionLayout);
                    } else {
                        contextName = NSStringForSoftKeyContextType(SoftKeyContextTypeMenuItemRewardLayout);
                    }
                } else if ([entity isEqual:@"BasketTender"]) {
                    contextName = NSStringForSoftKeyContextType(SoftKeyContextTypeMenuItemTenderLayout);
                } else if ([entity isEqual:@"CustomerEmail"]) {
                    contextName = NSStringForSoftKeyContextType(SoftKeyContextTypeMenuItemCustomeremailLayout);
                }
                
                // get the soft key menu for this context
                result = [[BasketController sharedInstance] getLayoutForContextNamed:contextName];
            } else if ([item isKindOfClass:[BasketTaxFree class]]) {
                result = [[BasketController sharedInstance] getLayoutForContextNamed:NSStringForSoftKeyContextType(SoftKeyContextTypeMenuItemTaxfreeLayout)];
            }
        }
    }
    
    return result;
}

- (void) mandatoryModifierCheck
{
    // if we aren't already showing modifier selection
    if (!_modifierSelectionPresented) {
        // loop through basket item
        MPOSBasket *basket = [[BasketController sharedInstance] basket];
        for (MPOSBasketItem *basketItem in basket.basketItems) {
            // if this item has a mandatory modifier group
            if (basketItem.mandatoryModifierGroups.count > 0) {
                // stop scanning
                if ([[BarcodeScannerController sharedInstance] scanning]) {
                    [[BarcodeScannerController sharedInstance] stopScanning];
                }
                // display the view
                _modifierSelectionPresented = YES;
                OCGModifierViewController *modifiersVC = [[OCGModifierViewController alloc] init];
                modifiersVC.optional = NO;
                modifiersVC.itemDesc = basketItem.desc;
                modifiersVC.delegate = self;
                // get the actual modifier groups from config
                NSMutableArray *choices = [NSMutableArray arrayWithCapacity:basketItem.mandatoryModifierGroups.count];
                BOOL error = NO;
                for (MPOSModifierGroup *group in basketItem.mandatoryModifierGroups) {
                    ItemModifierGroup *configGroup = [[BasketController sharedInstance] modifierGroupWithId:group.itemModifierGroupId];
                    if (configGroup) {
                        if (configGroup.itemModifiers.count == 0) {
                            error = YES;
                        } else {
                            // turns out type is defined by the basket item modifier
                            configGroup.type = group.type;
                            [choices addObject:configGroup];
                        }
                    } else {
                        error = YES;
                    }
                }
                if (!error) {
                    modifiersVC.choices = choices;
                    UINavigationController *modifiersNavController = [[UINavigationController alloc] initWithRootViewController:modifiersVC];
                    modifiersNavController.modalPresentationStyle = UIModalPresentationFormSheet;
                    [self presentViewController:modifiersNavController
                                       animated:YES
                                     completion:nil];
                } else {
                    // TFS 71401 - error if the modifier group doesn't exist
                    _modifierSelectionPresented = NO;
                    [self modifierViewControllerDidError:modifiersVC];
                }
                break;
            }
        }
    }
}

- (void) presentOptionalModifiersForItem:(MPOSBasketItem *)basketItem
{
    // if we aren't already showing modifier selection
    if (!_modifierSelectionPresented) {
        // if this item has an optional modifier group
        if (basketItem.optionalModifierGroups.count > 0) {
            // stop scanning
            if ([[BarcodeScannerController sharedInstance] scanning]) {
                [[BarcodeScannerController sharedInstance] stopScanning];
            }
            // display the view
            OCGModifierViewController *modifiersVC = [[OCGModifierViewController alloc] init];
            modifiersVC.optional = YES;
            modifiersVC.itemDesc = basketItem.desc;
            modifiersVC.delegate = self;
            // get the actual modifier groups from config
            NSMutableArray *choices = [NSMutableArray arrayWithCapacity:basketItem.optionalModifierGroups.count];
            for (MPOSModifierGroup *group in basketItem.optionalModifierGroups) {
                ItemModifierGroup *configGroup = [[BasketController sharedInstance] modifierGroupWithId:group.itemModifierGroupId];
                if (configGroup) {
                    // turns out type is defined by the basket item modifier
                    configGroup.type = group.type;
                    [choices addObject:configGroup];
                }
            }
            modifiersVC.choices = choices;
            if (choices.count > 0) {
                _modifierSelectionPresented = YES;
                UINavigationController *modifiersNavController = [[UINavigationController alloc] initWithRootViewController:modifiersVC];
                modifiersNavController.modalPresentationStyle = UIModalPresentationFormSheet;
                [self presentViewController:modifiersNavController
                                   animated:YES
                                 completion:nil];
            } else {
                [[[UIAlertView alloc] initWithTitle:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁄󠁍󠁺󠁧󠁩󠁮󠁂󠁖󠁭󠁵󠁹󠁰󠁔󠁳󠁯󠁸󠁪󠁓󠁭󠁦󠁰󠁸󠁤󠁪󠀶󠁎󠁁󠁿*/ @"Optional Modifiers", nil)
                                            message:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁤󠀳󠁰󠁥󠁂󠁢󠁑󠁌󠀴󠁅󠁦󠁮󠁰󠁋󠀸󠀹󠁳󠁭󠁸󠁲󠁅󠁕󠁈󠁶󠁘󠁧󠁫󠁿*/ @"No optional modifiers available", nil)
                                           delegate:nil
                                  cancelButtonTitle:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁄󠁴󠁍󠁉󠁣󠀫󠁺󠁔󠁗󠁦󠁹󠁡󠁯󠀷󠁨󠁔󠁩󠀸󠁯󠀫󠁨󠁘󠀹󠁱󠁌󠀶󠀸󠁿*/ @"OK", nil)
                                  otherButtonTitles:nil] show];
            }
        } else {
            [[[UIAlertView alloc] initWithTitle:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁄󠁍󠁺󠁧󠁩󠁮󠁂󠁖󠁭󠁵󠁹󠁰󠁔󠁳󠁯󠁸󠁪󠁓󠁭󠁦󠁰󠁸󠁤󠁪󠀶󠁎󠁁󠁿*/ @"Optional Modifiers", nil)
                                        message:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁤󠀳󠁰󠁥󠁂󠁢󠁑󠁌󠀴󠁅󠁦󠁮󠁰󠁋󠀸󠀹󠁳󠁭󠁸󠁲󠁅󠁕󠁈󠁶󠁘󠁧󠁫󠁿*/ @"No optional modifiers available", nil)
                                       delegate:nil
                              cancelButtonTitle:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁄󠁴󠁍󠁉󠁣󠀫󠁺󠁔󠁗󠁦󠁹󠁡󠁯󠀷󠁨󠁔󠁩󠀸󠁯󠀫󠁨󠁘󠀹󠁱󠁌󠀶󠀸󠁿*/ @"OK", nil)
                              otherButtonTitles:nil] show];
        }
    }
}

@end
