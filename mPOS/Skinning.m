
#import "Skinning.h"

#import "OCGDateField.h"

NSDictionary* SkinningDictionary()
{
    ClientSettings *clientSettings = [[BasketController sharedInstance] clientSettings];
    
    NSArray *primaryColor = [clientSettings.DRAFT_primaryColor componentsSeparatedByString:@","];
    NSArray *secondaryColor = [clientSettings.DRAFT_secondaryColor componentsSeparatedByString:@","];
    NSString *fontFamily = clientSettings.DRAFT_fontFamily;
    
    NSString *keyboardAppearanceString = [clientSettings.DRAFT_keyboardAppearance lowercaseString];
    
    UIKeyboardAppearance keyboardAppearance = UIKeyboardAppearanceDefault;
    if (UIDevice.currentDevice.systemVersion.floatValue >= 7.0)
    {
        if ([keyboardAppearanceString isEqualToString:@"dark"])
        {
            keyboardAppearance = UIKeyboardAppearanceDark;
        }
        
        CGFloat Y = 0.2126 * [primaryColor[0] floatValue] + 0.7152 * [primaryColor[1] floatValue] + 0.0722 * [primaryColor[2] floatValue];
        if (Y < 128)
        {
            [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent animated:NO];
        }
        else
        {
            [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault animated:NO];
        }
        
    }
    
    /*
         [[UITextField appearance] setKeyboardAppearance:keyboardAppearance]; sets the real value too late
     so the keyboard appears the first time with UIKeyboardAppearanceDefault. The below notification gets posted
     early enough for us to correctly set the appearance.
     */
    [[NSNotificationCenter defaultCenter] addObserverForName:UITextFieldTextDidBeginEditingNotification
                                                      object:nil
                                                       queue:nil
                                                  usingBlock:^(NSNotification *note) {
                                                      [note.object setKeyboardAppearance:keyboardAppearance];
                                                  }];
    
    [[NSNotificationCenter defaultCenter] addObserverForName:OCGDateFieldDateDidBeginEditingNotification
                                                      object:nil
                                                       queue:nil
                                                  usingBlock:^(NSNotification *note) {
                                                      [note.object setKeyboardAppearance:keyboardAppearance];
                                                  }];
    NSDictionary *skinDictionary = @{
                                     @"Colors" : @{
                                             @"Alternate Basket Item Cell Background Color" : @[
                                                     @(247),
                                                     @(247),
                                                     @(247),
                                                     @(255)
                                                     ],
                                             @"Alternate Basket Item Cell Detail Foreground Color" : @[
                                                     @(0),
                                                     @(0),
                                                     @(0),
                                                     @(255)
                                                     ],
                                             @"Alternate Basket Item Cell Foreground Color" : @[
                                                     @(0),
                                                     @(0),
                                                     @(0),
                                                     @(255)
                                                     ],
                                             @"Basket Item Cell Background Color" : @[
                                                     @(0),
                                                     @(0),
                                                     @(0),
                                                     @(0)
                                                     ],
                                             @"Basket Item Cell Detail Foreground Color" : @[
                                                     @(0),
                                                     @(0),
                                                     @(0),
                                                     @(255)
                                                     ],
                                             @"Basket Item Cell Foreground Color" : @[
                                                     @(0),
                                                     @(0),
                                                     @(0),
                                                     @(255)
                                                     ],
                                             @"Basket Item Header Cell Background Color" : @[
                                                     @(229),
                                                     @(229),
                                                     @(229),
                                                     @(255)
                                                     ],
                                             @"Cell Key Foreground Color" : @[
                                                     @(0),
                                                     @(0),
                                                     @(0),
                                                     @(255)
                                                     ],
                                             @"Cell Text Foreground Color" : @[
                                                     @(0),
                                                     @(0),
                                                     @(0),
                                                     @(255)
                                                     ],
                                             @"Cell Value Foreground Color" : @[
                                                     @(56.1),
                                                     @(84.16),
                                                     @(135.15),
                                                     @(255)
                                                     ],
                                             @"Editable Cell Key Foreground Color" : @[
                                                     @(56.1),
                                                     @(84.16),
                                                     @(135.15),
                                                     @(255)
                                                     ],
                                             @"Editable Cell Value Foreground Color" : @[
                                                     @(0),
                                                     @(0),
                                                     @(0),
                                                     @(255)
                                                     ],
                                             @"Editable Table Background Color" : @[
                                                     @(0),
                                                     @(0),
                                                     @(0),
                                                     @(0)
                                                     ],
                                             @"Editable Table Cell Background Color" : @[
                                                     @(0),
                                                     @(0),
                                                     @(0),
                                                     @(0)
                                                     ],
                                             @"Editable Table Foreground Color" : @[
                                                     @(0),
                                                     @(0),
                                                     @(0),
                                                     @(0)
                                                     ],
                                             @"Editable Table Shadow Color" : @[
                                                     @(0),
                                                     @(0),
                                                     @(0),
                                                     @(0)
                                                     ],
                                             @"Keyboard Accessory Background Color" : @[
                                                     @(247),
                                                     @(247),
                                                     @(247),
                                                     @(255)
                                                     ],
                                             @"Keyboard Accessory Button Background Color" : @[
                                                     @(0),
                                                     @(0),
                                                     @(0),
                                                     @(255)
                                                     ],
                                             @"Keyboard Accessory Button Disabled Background Color" : @[
                                                     @(247),
                                                     @(247),
                                                     @(247),
                                                     @(255)
                                                     ],
                                             @"Keyboard Accessory Button Foreground Color" : @[
                                                     @(255),
                                                     @(255),
                                                     @(255),
                                                     @(255)
                                                     ],
                                             @"Keyboard Accessory Foreground Color" : @[
                                                     @(0),
                                                     @(0),
                                                     @(0),
                                                     @(255)
                                                     ],
                                             @"Keyboard Accessory Text Field Background Color" : @[
                                                     @(255),
                                                     @(255),
                                                     @(255),
                                                     @(255)
                                                     ],
                                             @"Menu Background Color" : @[
                                                     @(56),
                                                     @(59),
                                                     @(65),
                                                     @(255)
                                                     ],
                                             @"Menu Button Foreground Disabled Color" : @[
                                                     @(255),
                                                     @(255),
                                                     @(255),
                                                     @(76)
                                                     ],
                                             @"Menu Button Foreground Enabled Color" : @[
                                                     @(255),
                                                     @(255),
                                                     @(255),
                                                     @(255)
                                                     ],
                                             @"Menu Change View Background Color" : @[
                                                     @(0),
                                                     @(0),
                                                     @(0),
                                                     @(0)
                                                     ],
                                             @"Menu Change View Foreground Color" : @[
                                                     @(255),
                                                     @(255),
                                                     @(255),
                                                     @(255)
                                                     ],
                                             @"Menu Comment View Background Color" : @[
                                                     @(0),
                                                     @(0),
                                                     @(0),
                                                     @(0)
                                                     ],
                                             @"Menu Comment View Foreground Color" : @[
                                                     @(255),
                                                     @(255),
                                                     @(255),
                                                     @(255)
                                                     ],
                                             @"Menu Foreground Color" : @[
                                                     @(255),
                                                     @(255),
                                                     @(255),
                                                     @(255)
                                                     ],
                                             @"Menu Primary Button Background Disabled Color" : @[
                                                     @(113),
                                                     @(140),
                                                     @(106),
                                                     @(16)
                                                     ],
                                             @"Menu Primary Button Background Enabled Color" : @[
                                                     @(113),
                                                     @(140),
                                                     @(106),
                                                     @(76)
                                                     ],
                                             @"Menu Secondary Button Background Color" : @[
                                                     @(0),
                                                     @(0),
                                                     @(0),
                                                     @(76)
                                                     ],
                                             @"Navigation Bar Background Color" : primaryColor,
                                             @"Navigation Bar Foreground Color" : secondaryColor,
                                             @"Navigation Bar Shadow Color" : @[
                                                     @(0),
                                                     @(0),
                                                     @(0),
                                                     @(0)
                                                     ],
                                             @"No Sale Menu Button Background Color" : @[
                                                     @(255),
                                                     @(255),
                                                     @(255),
                                                     @(255)
                                                     ],
                                             @"No Sale Menu Button Foreground Color" : @[
                                                     @(0),
                                                     @(0),
                                                     @(0),
                                                     @(255)
                                                     ],
                                             @"Primary Button Background Color" : @[
                                                     @(0),
                                                     @(204),
                                                     @(0),
                                                     @(255)
                                                     ],
                                             @"Primary Button Foreground Color" : @[
                                                     @(0),
                                                     @(0),
                                                     @(0),
                                                     @(255)
                                                     ],
                                             @"Print Options View Background Color" : @[
                                                     @(0),
                                                     @(0),
                                                     @(0),
                                                     @(255)
                                                     ],
                                             @"Print Options View Foreground Color" : @[
                                                     @(255),
                                                     @(255),
                                                     @(255),
                                                     @(255)
                                                     ],
                                             @"Print Options View Indicator Ring Color" : @[
                                                     @(247),
                                                     @(247),
                                                     @(247),
                                                     @(255)
                                                     ],
                                             @"Sales Menu Background Color" : @[
                                                     @(255),
                                                     @(255),
                                                     @(255),
                                                     @(255)
                                                     ],
                                             @"Scanning Button Background Color" : @[
                                                     @(0),
                                                     @(0),
                                                     @(0),
                                                     @(255)
                                                     ],
                                             @"Search Bar Background Color" : @[
                                                     @(247),
                                                     @(247),
                                                     @(247),
                                                     @(255)
                                                     ],
                                             @"Search Bar Foreground Color" : @[
                                                     @(0),
                                                     @(0),
                                                     @(0),
                                                     @(255)
                                                     ],
                                             @"Search View Background Color" : @[
                                                     @(247),
                                                     @(247),
                                                     @(247),
                                                     @(255)
                                                     ],
                                             @"Search View Foreground Color" : @[
                                                     @(0),
                                                     @(0),
                                                     @(0),
                                                     @(255)
                                                     ],
                                             @"Secondary Button Background Color" : @[
                                                     @(205),
                                                     @(205),
                                                     @(205),
                                                     @(255)
                                                     ],
                                             @"Secondary Button Foreground Color" : @[
                                                     @(0),
                                                     @(0),
                                                     @(0),
                                                     @(255)
                                                     ],
                                             @"Selected Tender Menu Item Color" : @[
                                                     @(0),
                                                     @(0),
                                                     @(0),
                                                     @(255)
                                                     ],
                                             @"SlightlyTransparentBlack" : @[
                                                     @(0),
                                                     @(0),
                                                     @(0),
                                                     @(200)
                                                     ],
                                             @"Status View Background Color" : primaryColor,
                                             @"Status View Foreground Color" : secondaryColor,
                                             @"Status View Training Background Color" : @[
                                                     @(0),
                                                     @(0),
                                                     @(0),
                                                     @(0)
                                                     ],
                                             @"Sub Total Foreground Color" : @[
                                                     @(84),
                                                     @(84),
                                                     @(84),
                                                     @(255)
                                                     ],
                                             @"Sub Total Label Foreground Color" : @[
                                                     @(135),
                                                     @(135),
                                                     @(135),
                                                     @(255)
                                                     ],
                                             @"Sub Total Shadow Color" : @[
                                                     @(252),
                                                     @(252),
                                                     @(252),
                                                     @(255)
                                                     ],
                                             @"Table Background Color" : @[
                                                     @(0),
                                                     @(0),
                                                     @(0),
                                                     @(0)
                                                     ],
                                             @"Table Cell Background Color" : @[
                                                     @(0),
                                                     @(0),
                                                     @(0),
                                                     @(0)
                                                     ],
                                             @"Table Foreground Color" : @[
                                                     @(0),
                                                     @(0),
                                                     @(0),
                                                     @(0)
                                                     ],
                                             @"Table Shadow Color" : @[
                                                     @(0),
                                                     @(0),
                                                     @(0),
                                                     @(0)
                                                     ],
                                             @"Table Tint Color" : primaryColor,
                                             @"Tender Bar Background Color" : @[
                                                     @(155),
                                                     @(155),
                                                     @(155),
                                                     @(255)
                                                     ],
                                             @"Tender Bar Foreground Color" : @[
                                                     @(255),
                                                     @(255),
                                                     @(255),
                                                     @(255)
                                                     ],
                                             @"Tender Bar Tender Background Color" : @[
                                                     @(0),
                                                     @(0),
                                                     @(0),
                                                     @(255)
                                                     ],
                                             },
                                     @"Fonts" : @{
                                             @"Alternate Basket Item Cell Detail Font" : @[
                                                     fontFamily,
                                                     @(11)
                                                     ],
                                             @"Alternate Basket Item Cell Font" : @[
                                                     fontFamily,
                                                     @(14)
                                                     ],
                                             @"Barcode Scanner Font" : @[
                                                     fontFamily,
                                                     @(11)
                                                     ],
                                             @"Basket Item Cell Detail Font" : @[
                                                     fontFamily,
                                                     @(11)
                                                     ],
                                             @"Basket Item Cell Font" : @[
                                                     fontFamily,
                                                     @(14)
                                                     ],
                                             @"Cell Key Font" : @[
                                                     @"HelveticaNeue",
                                                     @(14)
                                                     ],
                                             @"Cell Text Font" : @[
                                                     fontFamily,
                                                     @(14)
                                                     ],
                                             @"Cell Value Font" : @[
                                                     fontFamily,
                                                     @(14)
                                                     ],
                                             @"Change Font" : @[
                                                     fontFamily,
                                                     @(17)
                                                     ],
                                             @"Editable Cell Key Font" : @[
                                                     @"HelveticaNeue-Bold",
                                                     @(12)
                                                     ],
                                             @"Editable Cell Value Font" : @[
                                                     fontFamily,
                                                     @(15)
                                                     ],
                                             @"Foreign Currency Font" : @[
                                                     fontFamily,
                                                     @(17)
                                                     ],
                                             @"Keyboard Accessory Font" : @[
                                                     fontFamily,
                                                     @(14)
                                                     ],
                                             @"Menu Button Font" : @[
                                                     fontFamily,
                                                     @(10)
                                                     ],
                                             @"Menu Change View Font" : @[
                                                     fontFamily,
                                                     @(30)
                                                     ],
                                             @"Menu Comment View Font" : @[
                                                     fontFamily,
                                                     @(17)
                                                     ],
                                             @"Menu Primary Button Font" : @[
                                                     fontFamily,
                                                     @(17)
                                                     ],
                                             @"Menu Secondary Button Font" : @[
                                                     fontFamily,
                                                     @(17)
                                                     ],
                                             @"Primary Button Font" : @[
                                                     fontFamily,
                                                     @(14)
                                                     ],
                                             @"Print Options View Font" : @[
                                                     fontFamily,
                                                     @(10)
                                                     ],
                                             @"Secondary Button Font" : @[
                                                     fontFamily,
                                                     @(14)
                                                     ],
                                             @"Status View Font" : @[
                                                     fontFamily,
                                                     @(8)
                                                     ],
                                             @"Status View Training Font" : @[
                                                     fontFamily,
                                                     @(10)
                                                     ],
                                             @"Sub Total Font" : @[
                                                     @"HelveticaNeue-Bold",
                                                     @(42)
                                                     ],
                                             @"Sub Total Label Font" : @[
                                                     @"HelveticaNeue-Bold",
                                                     @(14)
                                                     ],
                                             @"Tender Bar Font" : @[
                                                     fontFamily,
                                                     @(10)
                                                     ],
                                             @"Status View Font (iPad)" : @[
                                                     fontFamily,
                                                     @(14)
                                                     ],
                                             },
                                     @(kGlobalTag) : @{
                                             @"BarItemTint" : @"Secondary Button Background Color",
                                             @"Name" : @"Global appearance settings",
                                             @"NavBarTint" : @"Navigation Bar Background Color",
                                             @"NavBarTitleColor" : @"Navigation Bar Foreground Color",
                                             @"NavBarTitleShadowColor" : @"Navigation Bar Shadow Color",
                                             @"SwitchOnTint" : @"Application Tint Color",
                                             @"Tint" : @"Application Tint Color",
                                             @"ToolBarTint" : @"Navigation Bar Background Color",
                                             @"ToolBarTitleColor" : @"Navigation Bar Foreground Color",
                                             },
                                     @(kMenuSkinningTag) : @{
                                             @"Color" : @"Menu Background Color",
                                             @"Name" : @"Menu background color",
                                             @"TableBackgroundColor" : @"Menu Background Color",
                                             @"ToolBarTint" : @"Menu Background Color",
                                             @"Used for" : @[
                                                     @"Menu background",
                                                     @"Spinner background",
                                                     @"Gift receipt header background"
                                                     ],
                                             },
                                     @(kTableCellValueSkinningTag) : @{
                                             @"Font" : @"Cell Value Font",
                                             @"FontColor" : @"Cell Value Foreground Color",
                                             @"Name" : @"Cell value",
                                             @"Used for" : @[
                                                     @"The detailTextLabel in cells"
                                                     ],
                                             },
                                     @(kEditableCellKeySkinningTag) : @{
                                             @"Color" : @"Editable Table Cell Background Color",
                                             @"Font" : @"Editable Cell Key Font",
                                             @"FontColor" : @"Editable Cell Key Foreground Color",
                                             @"Name" : @"Editable cell key",
                                             @"Used for" : @[
                                                     @"Login",
                                                     @"Customer entry",
                                                     @"Transaction history filter"
                                                     ],
                                             },
                                     @(kEditableCellValueSkinningTag) : @{
                                             @"Color" : @"Editable Table Cell Background Color",
                                             @"Font" : @"Editable Cell Value Font",
                                             @"FontColor" : @"Editable Cell Value Foreground Color",
                                             @"Name" : @"Editable cell value",
                                             @"Used for" : @[
                                                     @"Login",
                                                     @"Customer entry",
                                                     @"Transaction history filter"
                                                     ],
                                             },
                                     @(kEditableTableSkinningTag) : @{
                                             @"LabelFontColor" : @"Editable Table Foreground Color",
                                             @"LabelShadowColor" : @"Editable Table Shadow Color",
                                             @"Name" : @"Editable table",
                                             @"TableBackgroundColor" : @"Editable Table Background Color",
                                             @"Used for" : @[
                                                     @"Login",
                                                     @"Customer entry",
                                                     @"Transaction history filter"
                                                     ],
                                             },
                                     @(kEditableTableCellSkinningTag) : @{
                                             @"Name" : @"Editable cell",
                                             @"TableCellBackgroundColor" : @"Editable Table Cell Background Color",
                                             @"Used for" : @[
                                                     @"Login",
                                                     @"Customer entry",
                                                     @"Transaction history filter"
                                                     ],
                                             },
                                     @(kTableCellSkinningTag) : @{
                                             @"Name" : @"Cell",
                                             @"TableCellBackgroundColor" : @"Table Cell Background Color",
                                             @"Used for" : @[
                                                     @"Used where there is no associated value [in UITableViewCellStyleDefault] @[e.g. the POS function list]"
                                                     ],
                                             },
                                     @(kTableCellTextSkinningTag) : @{
                                             @"Font" : @"Cell Text Font",
                                             @"FontColor" : @"Cell Text Foreground Color",
                                             @"Name" : @"Cell Text",
                                             @"Used for" : @[
                                                     @"Used where there is no associated value [in UITableViewCellStyleDefault] @[e.g. the POS function list]"
                                                     ],
                                             },
                                     @(kBasketItemCellSkinningTag) : @{
                                             @"Font" : @"Basket Item Cell Font",
                                             @"FontColor" : @"Basket Item Cell Foreground Color",
                                             @"Name" : @"Basket item cell",
                                             @"TableCellBackgroundColor" : @"Basket Item Cell Background Color",
                                             @"Used for" : @[
                                                     @"The quantity, name and value on basket lines"
                                                     ],
                                             },
                                     @(kBasketItemCellDetailSkinningTag) : @{
                                             @"Font" : @"Basket Item Cell Detail Font",
                                             @"FontColor" : @"Basket Item Cell Detail Foreground Color",
                                             @"Name" : @"Basket item cell detail",
                                             @"Used for" : @[
                                                     @"The item description on basket items"
                                                     ],
                                             },
                                     @(kBasketItemAltCellSkinningTag) : @{
                                             @"Font" : @"Alternate Basket Item Cell Font",
                                             @"FontColor" : @"Alternate Basket Item Cell Foreground Color",
                                             @"Name" : @"Basket item cell @[alternate]",
                                             @"TableCellBackgroundColor" : @"Alternate Basket Item Cell Background Color",
                                             @"Used for" : @[
                                                     @"Discount, return and other negative amount basket items"
                                                     ],
                                             },
                                     @(kBasketItemAltCellDetailSkinningTag) : @{
                                             @"Font" : @"Alternate Basket Item Cell Detail Font",
                                             @"FontColor" : @"Alternate Basket Item Cell Detail Foreground Color",
                                             @"Name" : @"Alternate Basket item cell detail @[alternate]",
                                             @"Used for" : @[
                                                     @"Discount, return and other negative amount basket items"
                                                     ],
                                             },
                                     @(kTableSkinningTag) : @{
                                             @"LabelFontColor" : @"Table Foreground Color",
                                             @"LabelShadowColor" : @"Table Shadow Color",
                                             @"Name" : @"Table",
                                             @"TableBackgroundColor" : @"Table Background Color",
                                             @"Tint" : @"Table Tint Color",
                                             @"Used for" : @[
                                                     @"Basket",
                                                     @"Recall list"
                                                     ],
                                             },
                                     @(kScanButtonSkinningTag) : @{
                                             @"ButtonColor" : @"Scanning Button Background Color",
                                             @"Font" : @"Menu Button Font",
                                             @"FontColor" : @"Menu Foreground Color",
                                             @"ImageTintColor" : @"Menu Foreground Color",
                                             @"Name" : @"Scan menu button",
                                             @"Used for" : @[
                                                     @"The scanning button"
                                                     ],
                                             },
                                     @(kSalesMenuSkinningTag) : @{
                                             @"Color" : @"Sales Menu Background Color",
                                             @"Name" : @"Sales menu",
                                             @"Used for" : @[
                                                     @"Sales menu"
                                                     ],
                                             },
                                     @(kKeyboardAccessoryTag) : @{
                                             @"Color" : @"Keyboard Accessory Background Color",
                                             @"Font" : @"Keyboard Accessory Font",
                                             @"FontColor" : @"Keyboard Accessory Foreground Color",
                                             @"Name" : @"Keyboard Accessory",
                                             @"Used for" : @[
                                                     @"Keyboards with accessory views"
                                                     ],
                                             },
                                     @(kKeyboardAccessoryButtonTag) : @{
                                             @"ButtonColor" : @"Keyboard Accessory Button Background Color",
                                             @"DisabledColor" : @"Keyboard Accessory Button Disabled Background Color",
                                             @"Font" : @"Keyboard Accessory Font",
                                             @"FontColor" : @"Keyboard Accessory Button Foreground Color",
                                             @"ImageTintColor" : @"Keyboard Accessory Button Foreground Color",
                                             @"Name" : @"Keyboard Accessory Button",
                                             @"Used for" : @[
                                                     @"Keyboards with accessory views"
                                                     ],
                                             },
                                     @(kKeyboardAccessoryTextFieldTag) : @{
                                             @"Color" : @"Keyboard Accessory Text Field Background Color",
                                             @"Font" : @"Keyboard Accessory Font",
                                             @"FontColor" : @"Keyboard Accessory Foreground Color",
                                             @"Name" : @"Keyboard Accessory Text Field",
                                             @"Used for" : @[
                                                     @"Keyboards with accessory views"
                                                     ],
                                             },
                                     @(kSearchSkinningTag) : @{
                                             @"Color" : @"Search View Background Color",
                                             @"Name" : @"Search View",
                                             @"SegmentTint" : @"Search View Foreground Color",
                                             @"Used for" : @[
                                                     @"Product and customer search"
                                                     ],
                                             },
                                     @(kSearchBarSkinningTag) : @{
                                             @"ButtonColor" : @"Search Bar Foreground Color",
                                             @"Color" : @"Search Bar Background Color",
                                             @"Name" : @"Search Bar",
                                             @"SearchBarTint" : @"Search Bar Foreground Color",
                                             @"Used for" : @[
                                                     @"Product and customer search bars"
                                                     ],
                                             },
                                     @(kNoSaleMenuItemSkinningTag) : @{
                                             @"ButtonColor" : @"No Sale Menu Button Background Color",
                                             @"Font" : @"Menu Button Font",
                                             @"FontColor" : @"No Sale Menu Button Foreground Color",
                                             @"ImageTintColor" : @"No Sale Menu Button Foreground Color",
                                             @"Name" : @"No sale quick function button",
                                             @"Used for" : @[
                                                     @"Function buttons on the no sale view"
                                                     ],
                                             },
                                     @(kStatusViewTag) : @{
                                             @"Color" : @"Status View Background Color",
                                             @"Font" : @"Status View Font",
                                             @"FontColor" : @"Status View Foreground Color",
                                             @"ImageTintColor" : @"Status View Foreground Color",
                                             @"Name" : @"Status view",
                                             @"Used for" : @[
                                                     @"The status view under the status bar"
                                                     ],
                                             },
                                     @(kStatusViewiPadTag) : @{
                                             @"Color" : @"Status View Background Color",
                                             @"Font" : @"Status View Font (iPad)",
                                             @"FontColor" : @"Status View Foreground Color",
                                             @"ImageTintColor" : @"Status View Foreground Color",
                                             @"Name" : @"Status view (iPad)",
                                             @"Used for" : @[
                                                     @"The status view under the status bar on iPad"
                                                     ],
                                             },
                                     @(kStatusViewTrainingTag) : @{
                                             @"Color" : @"Status View Training Background Color",
                                             @"Font" : @"Status View Training Font",
                                             @"FontColor" : @"Status View Foreground Color",
                                             @"ImageTintColor" : @"Status View Foreground Color",
                                             @"Name" : @"Status view training label",
                                             @"Used for" : @[
                                                     @"The status view training mode label"
                                                     ],
                                             },
                                     @(kBasketMenuButtonSkinningTag) : @{
                                             @"Font" : @"Menu Button Font",
                                             @"FontColor" : @"Menu Foreground Color",
                                             @"ImageTintColor" : @"Menu Foreground Color",
                                             @"Name" : @"Menu function button",
                                             @"TableCellBackgroundColor" : @"Menu Background Color",
                                             @"Used for" : @[
                                                     @"Used for the POS function buttons"
                                                     ],
                                             },
                                     @(kPrimaryButtonSkinningTag) : @{
                                             @"ButtonColor" : @"Primary Button Background Color",
                                             @"Font" : @"Primary Button Font",
                                             @"FontColor" : @"Primary Button Foreground Color",
                                             @"Name" : @"Primary button",
                                             @"Used for" : @[
                                                     @"Primary button used in add discount view @",
                                                     @"Signon button on login screen"
                                                     ],
                                             },
                                     @(kPrimaryButtonInvertedSkinningTag) : @{
                                             @"FontColor" : @"Navigation Bar Background Color",
                                             @"Font" : @"Primary Button Font",
                                             @"ButtonColor" : @"Navigation Bar Foreground Color",
                                             @"Name" : @"Primary button",
                                             @"Used for" : @[
                                                     @"Primary button used in add discount view @",
                                                     @"Signon button on login screen"
                                                     ],
                                             },
                                     @(kSecondaryButtonSkinningTag) : @{
                                             @"ButtonColor" : @"Secondary Button Background Color",
                                             @"Font" : @"Secondary Button Font",
                                             @"FontColor" : @"Secondary Button Foreground Color",
                                             @"Name" : @"Secondary button",
                                             @"Used for" : @[
                                                     @"All normal bar button items",
                                                     @"Cancel button on Add discount view",
                                                     @"Get more sales in the sales history view"
                                                     ],
                                             },
                                     @(kTableCellKeySkinningTag) : @{
                                             @"Font" : @"Cell Key Font",
                                             @"FontColor" : @"Cell Key Foreground Color",
                                             @"Name" : @"Cell key",
                                             @"Used for" : @[
                                                     @"The textLabel in cells"
                                                     ],
                                             },
                                     };
    return skinDictionary;
}
