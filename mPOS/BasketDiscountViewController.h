//
//  BasketDiscountViewController.h
//  mPOS
//
//  Created by Antonio Strijdom on 02/12/2013.
//  Copyright (c) 2013 Clarity. All rights reserved.
//

#import <UIKit/UIKit.h>

/** @file BasketDiscountViewController.h */

/** @brief This view controller is being reused for adding 
 *  discounts to baskets, basket items, as well as for price 
 *  override. The client called needs to specify the operation 
 *  mode for this view controller by using the following
 *  constants.
 */
NS_ENUM(NSInteger, DiscountTypes) {
    /** @brief Mode that allows adding a discount to the specified basket item.
     */
    DiscountTypeBasketItemDiscount = 0,
    /** @brief Mode that allows adding a discount to the active basket.
     */
    DiscountTypeBasketDiscount = 1,
    /** @brief Mode that allows defining a new price for specified basket item.
     */
    DiscountTypeBasketItemPriceOverride = 2
};

/** @brief View controller that implements all logic to allow the 
 *  user to add a discount to the selected basket item or the 
 *  whole sales basket.
 */
@interface BasketDiscountViewController : UITableViewController

/** @property basketItems
 *  @brief When applying a discount to multiple items, this property should be set to an array of the relevant basketItemIds.
 */
@property (strong, nonatomic) NSArray *basketItems;

/** @property discountType
 *  @brief The discount type for which the view should be initialized.
 */
@property (assign, nonatomic) enum DiscountTypes discountType;

@end
