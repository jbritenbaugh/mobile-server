//
//  ItemDetailImageCollectionCell.h
//  mPOS
//
//  Created by Antonio Strijdom on 22/05/2013.
//  Copyright (c) 2013 Clarity. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ItemDetailImageCollectionCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (weak, nonatomic) IBOutlet UIImageView *shadowImageView;

@end
