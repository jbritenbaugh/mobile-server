//
//  BasketEditViewController.h
//  mPOS
//
//  Created by Antonio Strijdom on 19/02/2014.
//  Copyright (c) 2014 Clarity. All rights reserved.
//

#import "BasketMultiSelectViewController.h"

@interface BasketEditViewController : BasketMultiSelectViewController

/** @property errored
 *  @brief Flag indicating if the edit view has unread errors.
 */
@property (nonatomic, readonly) BOOL errored;

@end
