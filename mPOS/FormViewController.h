//
//  SoftKeyContextViewController.h
//  mPOS
//
//  Created by John Scott on 28/03/2015.
//  Copyright (c) 2015 Clarity. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FormViewController : UIViewController

+(void)handleModelObject:(id)modelObject contextName:(NSString*)contextName presentingViewController:(UIViewController*)presentingViewController;

-(void)setSoftKeyContext:(SoftKeyContext*)softKeyContext menu:(NSString*)menu modelObject:(id)modelObject;

-(BOOL)shouldEnableSoftKey:(SoftKey*)softKey modelObject:(id)modelObject;

-(BOOL)shouldHighlightSoftKey:(SoftKey*)softKey modelObject:(id)modelObject;

-(void)didSelectSoftKey:(SoftKey*)softKey modelObject:(id)modelObject;

@end
