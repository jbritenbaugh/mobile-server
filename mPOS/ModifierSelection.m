//
//  ModifierSelection.m
//  mPOS
//
//  Created by Antonio Strijdom on 17/09/2014.
//  Copyright (c) 2014 Clarity. All rights reserved.
//

#import "ModifierSelection.h"

@implementation ModifierSelection

+ (ModifierSelection *)selectionFromItemModifier:(ItemModifier *)modifier
{
    ModifierSelection *result = [[ModifierSelection alloc] init];
    
    result.itemModifierId = modifier.itemModifierId;
    result.itemModifierGroupId = modifier.itemModifierGroupId;
    result.name = modifier.name;
    result.retailPrice = [[BasketPricingDTO alloc] init];
    result.retailPrice.amount = [modifier.retailPrice.amount copy];
    result.retailPrice.display = [modifier.retailPrice.display copy];
    result.sku = modifier.sku;
    result.type = modifier.type;
    result.quantity = 1;
    
    return result;
}

@end
