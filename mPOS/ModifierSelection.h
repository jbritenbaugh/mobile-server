//
//  ModifierSelection.h
//  mPOS
//
//  Created by Antonio Strijdom on 17/09/2014.
//  Copyright (c) 2014 Clarity. All rights reserved.
//

#import "RESTController.h"

@interface ModifierSelection : ItemModifier

@property (nonatomic, assign) NSUInteger quantity;
@property (nonatomic, strong) NSString *freeText;

+ (ModifierSelection *)selectionFromItemModifier:(ItemModifier *)modifier;

@end
