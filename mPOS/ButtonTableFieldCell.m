//
//  ButtonTableFieldCell.m
//  mPOS
//
//  Created by Meik Schuetz on 16/08/2012.
//  Copyright (c) 2012 Clarity. All rights reserved.
//

#import "ButtonTableFieldCell.h"
#import "CRSGlossyButton.h"
#import "CRSSkinning.h"

#define kSkinningTagButtonTableField        100
#define kSkinningTagButtonTableFieldButton  kSkinningTagButtonTableField + 1

@interface ButtonTableFieldCell ( )

/** @brief The array of buttons that are displayed in the table view cell.
 */
@property (strong, nonatomic) NSArray *buttons;

@end

@implementation ButtonTableFieldCell

/** @brief Designated constructor.
 *  @param buttonTitles The array of button titles to be shown in this cell.
 *  @return A newly initialized object of this class.
 */
- (id) initWithButtonTitles:(NSArray *)buttonTitles;
{
    if (self = [super init]) {
        
        // backe sure the cell gets a transparent background

        UIView *backView = [[UIView alloc] initWithFrame:CGRectZero];
        backView.backgroundColor = [UIColor clearColor];
        self.backgroundView = backView;
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.tag = kSkinningTagButtonTableField;
        
        // create a collection of buttons with the specified titles
        
        self.buttons = [self createTableCellButtonsWithTitles: buttonTitles];
        
        // apply skin
        
        [[CRSSkinning currentSkin] applyCellSkin:self];
    }
    return self;
}

#pragma mark Public Methods

/** @brief Returns the reference to the button at the specified index.
 *  @param index The index of the requested button.
 *  @return A reference to the requested button.
 */
- (UIButton *)buttonAtIndex:(int)index
{
    if ((index < 0) || (index >= [self.buttons count]))
        return nil;
    
    return (UIButton *)[self.buttons objectAtIndex:index];
}

#pragma mark Private Methods

/** @brief Creates a single button instance to be shown in the table cell.
 *  @param buttonTitle The title to be shown in the button.
 *  @return A reference to the initialized button.
 */
- (CRSGlossyButton *)createTableCellButtonWithTitle:(NSString *)buttonTitle
{
    CRSGlossyButton *tableCellButton = [[CRSGlossyButton alloc] init];
    [tableCellButton setTitle: buttonTitle forState: UIControlStateNormal];
    tableCellButton.tag = kSkinningTagButtonTableFieldButton;

    [tableCellButton addTarget: self
                        action: @selector(tableCellButtonTapped:)
              forControlEvents: UIControlEventTouchUpInside];
    
    return tableCellButton;
}

/** @brief Creates an array of button instances and adds them to the cell's content view.
 *  @param buttonTitles The array of button titles for the set of buttons to create.
 *  @return A reference to the array of buttons.
 */
- (NSArray *) createTableCellButtonsWithTitles:(NSArray *)buttonTitles
{
    NSMutableArray *tableCellButtons = [[NSMutableArray alloc] init];
    for (NSString *buttonTitle in buttonTitles) {
        UIButton *tableCellButton = [self createTableCellButtonWithTitle: buttonTitle];
        [tableCellButtons addObject: tableCellButton];
        [self.contentView addSubview: tableCellButton];
    }
    
    return [NSArray arrayWithArray: tableCellButtons];
}

#pragma mark Overridden Methods

/** @brief Layouts the cell's controls as specified.
 */
- (void)layoutSubviews
{
    [super layoutSubviews];
    
    CGRect rcContent = CGRectMake(self.contentView.bounds.origin.x,
                                  self.contentView.bounds.origin.y,
                                  self.contentView.bounds.size.width,
                                  self.contentView.bounds.size.height);
    
    CGFloat widthButtonMargins = ([self.buttons count] - 1) * 5.0;
    CGFloat widthButtons = (rcContent.size.width - widthButtonMargins) / [self.buttons count];
    
    int buttonIndex = 0;
    for (UIButton *tableCellButton in self.buttons) {
        CGRect rcButton = CGRectMake(buttonIndex++ * (widthButtons + widthButtonMargins),
                                     rcContent.origin.y,
                                     widthButtons,
                                     rcContent.size.height);
        tableCellButton.frame = rcButton;
    }
}

#pragma mark Action Methods

- (void) tableCellButtonTapped:(id)sender
{
    if ((self.delegate != nil)
        && ([self.delegate respondsToSelector: @selector(tappedButtonWithIndex:)])) {
        [self.delegate tappedButtonWithIndex: [self.buttons indexOfObject: sender]];
    }
}

@end
