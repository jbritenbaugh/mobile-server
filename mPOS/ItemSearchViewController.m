//
//  ItemSearchViewController.m
//  mPOS
//
//  Created by John Scott on 20/11/2013.
//  Copyright (c) 2013 Clarity. All rights reserved.
//

#import "ItemSearchViewController.h"
#import "CRSSkinning.h"
#import "RESTController.h"
#import "CRSLocationController.h"
#import "BasketController.h"
#import "ItemDetailDetailsCell.h"
#import "BasketItemTableViewCell.h"
#import "OCGFirstResponderAccessoryView.h"

@interface ItemSearchViewController () <UISearchBarDelegate, OCGFirstResponderAccessoryViewDelegate>
@property (strong, nonatomic) OCGFirstResponderAccessoryView *accessoryView;
@property (strong, nonatomic) UISearchBar *departmentSearchBar;
@property (strong, nonatomic) UISearchBar *itemDescriptionSearchBar;
@property (assign, nonatomic) NSInteger pageNumber;
@property (assign, nonatomic) NSInteger maxPageNumber;
@property (strong, nonatomic) NSMutableArray *items;

@end

@implementation ItemSearchViewController

static NSString *CellIdentifier = @"Cell";

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
        _items = [NSMutableArray array];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self.tableView registerClass:[BasketItemTableViewCell class]
           forCellReuseIdentifier:CellIdentifier];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    self.departmentSearchBar = [[UISearchBar alloc] init];
    self.departmentSearchBar.tag = kSearchBarSkinningTag;
    [self setupSearchBar:self.departmentSearchBar];
    self.departmentSearchBar.frame = CGRectMake(0.0, 0.0, self.tableView.frame.size.width, 44.0f);
    
    self.itemDescriptionSearchBar = [[UISearchBar alloc] init];
    self.itemDescriptionSearchBar.tag = kSearchBarSkinningTag;
    [self setupSearchBar:self.itemDescriptionSearchBar];
    self.itemDescriptionSearchBar.frame = CGRectMake(0.0, 44, self.tableView.frame.size.width, 44.0f);
    
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.tableView.frame.size.width, 88)];
    [headerView addSubview:self.departmentSearchBar];
    [headerView addSubview:self.itemDescriptionSearchBar];
    headerView.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    self.tableView.tableHeaderView = headerView;
    
    // create the keyboard accessory
    self.accessoryView = [[OCGFirstResponderAccessoryView alloc] init];
    self.accessoryView.delegate = self;
    self.accessoryView.frame = CGRectMake(0.0, 0.0, self.tableView.frame.size.width, 44.0f);
    self.departmentSearchBar.inputAccessoryView = self.accessoryView;
    self.itemDescriptionSearchBar.inputAccessoryView = self.accessoryView;
    
    // skin
    self.tableView.tag = kTableSkinningTag;
}

-(void)viewWillAppear:(BOOL)animated
{
    self.departmentSearchBar.placeholder = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁚󠁦󠁋󠁎󠁹󠁢󠁒󠁋󠀫󠁅󠀯󠀸󠁲󠀴󠁗󠁕󠁡󠁦󠁚󠁧󠁥󠁭󠁱󠁨󠀴󠁬󠀴󠁿*/ @"Department", nil);
    self.itemDescriptionSearchBar.placeholder = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁑󠁙󠀶󠀱󠁰󠁷󠁖󠁌󠁁󠁏󠁹󠁦󠁷󠁲󠁣󠀳󠁋󠀰󠀴󠁲󠁅󠀲󠁑󠁷󠁫󠁖󠀴󠁿*/ @"Item Description", nil);
    
    // add the dismiss button
    UIBarButtonItem *dismissButton =
    [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁡󠁩󠁡󠁍󠁰󠀸󠁷󠁣󠁹󠁩󠁕󠀷󠁪󠁶󠀱󠀸󠁐󠁖󠀵󠁥󠀳󠁫󠁕󠁚󠁄󠁶󠁙󠁿*/ @"Dismiss", nil)
                                     style:UIBarButtonItemStylePlain
                                    target:self
                                    action:@selector(dismissViewController:)];
    self.navigationItem.leftBarButtonItem = dismissButton;
    
    [[CRSSkinning currentSkin] applyViewSkin:self];
    self.tableView.contentOffset = CGPointZero;
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(handleBasketAddItemAddedNotification:)
                                                 name:kBasketControllerBasketItemAddedNotification
                                               object:nil];
    
    [super viewWillAppear:animated];
}

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
    self.departmentSearchBar.text = nil;
    self.itemDescriptionSearchBar.text = nil;
    self.pageNumber = 1;
    self.maxPageNumber = 1<<30;
    [self.items removeAllObjects];
    [self.tableView reloadData];

}

-(void)setupSearchBar:(UISearchBar*)searchBar
{
    searchBar.autocapitalizationType = UITextAutocapitalizationTypeNone;
    searchBar.autocorrectionType = UITextAutocorrectionTypeNo;
    searchBar.spellCheckingType = UITextSpellCheckingTypeNo;
    searchBar.keyboardType = UIKeyboardTypeAlphabet;
    searchBar.delegate = self;
    searchBar.autoresizingMask = UIViewAutoresizingFlexibleWidth;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return [self.items count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(BasketItemTableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    id object = self.items[indexPath.row];
    if ([object isKindOfClass:[Item class]]) {
        Item *item = (Item *)object;
        if (item.displayPrice == nil)
        {
            item.displayPrice = item.price;
        }
        [cell updateDisplayForItem:(MPOSItem *)item tableStyle:tableView.style];
        cell.textLabel.text = nil;
        cell.selectionStyle = UITableViewCellSelectionStyleBlue;
    } else if ([object isKindOfClass:[NSString class]]) {
        cell.textLabel.tag = kTableCellTextSkinningTag;
        cell.textLabel.text = object;
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    [[CRSSkinning currentSkin] applyCellSkin:cell
                                    forStyle:tableView.style];
}

- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 53.f;
}


#pragma mark - Table view delegate

- (NSIndexPath *) tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    id object = self.items[indexPath.row];
    if ([object isKindOfClass:[Item class]]) {
        return indexPath;
    } else {
        return nil;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    id object = self.items[indexPath.row];
    if ([object isKindOfClass:[Item class]]) {
        Item *item = (Item *)object;
        [BasketController.sharedInstance addItemWithScanId:item.itemRefId
                                                     price:nil
                                                   measure:nil
                                                  quantity:nil
                                               entryMethod:@"KEYED"
                                                    track1:nil
                                                    track2:nil
                                                    track3:nil
                                               barcodeType:nil];
    }
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
}

#pragma mark - Actions

- (void)dismissViewController:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:NULL];
}

#pragma mark - UISearchBarDelegate

- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar
{
    // setup the keyboard
    UITextField *searchBarTextField = nil;
    for (UIView *subview in searchBar.subviews)
    {
        for (UIView *subSubview in subview.subviews)
        {
            if ([subSubview conformsToProtocol:@protocol(UITextInputTraits)])
            {
                searchBarTextField = (UITextField *)subSubview;
                break;
            }
        }
    }
    searchBarTextField.enablesReturnKeyAutomatically = NO;
    return YES;
}

- (void) searchBarTextDidBeginEditing:(UISearchBar *)searchBar
{
    if (searchBar == self.departmentSearchBar) {
        self.accessoryView.currentPosition = [NSIndexPath indexPathForRow:0
                                                                inSection:0];
    } else if (searchBar == self.itemDescriptionSearchBar) {
        self.accessoryView.currentPosition = [NSIndexPath indexPathForRow:1
                                                                inSection:0];
    }
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    [searchBar resignFirstResponder];
    self.pageNumber = 1;
    self.maxPageNumber = 1<<30;
    [self.items removeAllObjects];
    [self.tableView reloadData];
    [self updateItemsFromServer];
}

- (void) searchBarTextDidEndEditing:(UISearchBar *)searchBar
{
    self.accessoryView.currentPosition = nil;
}

#pragma mark - OCGFirstResponderAccessoryViewDelegate

- (UIResponder *) OCGFirstResponderAccessoryView:(OCGFirstResponderAccessoryView *)firstResponderAccessoryView
                   firstResponderViewForPosition:(id)position
{
    UIView *result = nil;
    
    if (position != nil) {
        NSIndexPath *path = (NSIndexPath *)position;
        if (path.row == 0) {
            result = self.departmentSearchBar;
        } else {
            result = self.itemDescriptionSearchBar;
        }
    }
    
    return result;
}

- (id) OCGFirstResponderAccessoryView:(OCGFirstResponderAccessoryView *)firstResponderAccessoryView
                 positionFromPosition:(id)position
                               offset:(NSInteger)offset
{
    NSIndexPath *result = nil;
    
    // determine which search bar to select
    if (position != nil) {
        // select the other search bar
        NSIndexPath *path = (NSIndexPath *)position;
        if (path.row == 0) {
            // select the item search
            result = [NSIndexPath indexPathForRow:1
                                        inSection:0];
        } else {
            // select the dept search
            result = [NSIndexPath indexPathForRow:0
                                        inSection:0];
        }
    }
    
    return result;
}

-(void)updateItemsFromServer
{
    if (self.pageNumber >=  self.maxPageNumber)
    {
        return;
    }
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^(void) {
        DebugLog(@"department: %@ itemDescription: %@ pageNumber: %d", self.departmentSearchBar.text, self.itemDescriptionSearchBar.text, self.pageNumber);
        
        NSInteger pageNumber = self.pageNumber;
        [RESTController.sharedInstance searchItemsWithDepartment:self.departmentSearchBar.text
                                                 itemDescription:self.itemDescriptionSearchBar.text
                                                      pageNumber:pageNumber
                                                         storeID:[CRSLocationController getLocationKey]
                                                        complete:^(NSArray *restItems, NSError *error, ServerError *serverError)
         {
             dispatch_async(dispatch_get_main_queue(), ^(void) {
                 // check for errors
                 if (restItems != nil) {
                     if ([restItems count] > 0)
                     {
                         [self.items addObjectsFromArray:restItems];
                         [self.tableView reloadData];
                         if (self.pageNumber > 1)
                         {
                             [self.tableView flashScrollIndicators];
                         }
                     }
                     else
                     {
                         self.pageNumber =  self.maxPageNumber;
                         if (self.items.count == 0) {
                             // TFS 59784 - Show an dummy "no items found" row
                             [self.items addObject:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁮󠁳󠁂󠁒󠁢󠁖󠁶󠁅󠁫󠁆󠁑󠀳󠁑󠁹󠁱󠁌󠁚󠀶󠁣󠁹󠁎󠁷󠁡󠀵󠁁󠀷󠁫󠁿*/ @"No products found", nil)];
                             [self.tableView reloadData];
                         }
                     }
                 } else {
                     // there was an error
                     [BasketController.sharedInstance kick];
                     // send the notification
                     [[NSNotificationCenter defaultCenter] postNotificationName:kBasketControllerGeneralErrorNotification
                                                                         object:self
                                                                       userInfo:@{kBasketControllerGeneralErrorKey:serverError}];
                 }
             });
             
         }];
    });
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    float endScrolling = scrollView.contentOffset.y + scrollView.frame.size.height;
    if (endScrolling >= scrollView.contentSize.height)
    {
        self.pageNumber++;
        [self updateItemsFromServer];
    }
}

- (void) handleBasketAddItemAddedNotification:(NSNotification *)notification
{
    [self dismissViewController:nil];
}

@end
//