//
//  CRSViewControllerBase.m
//  mPOS
//
//  Created by Meik Schuetz on 17/08/2012.
//  Copyright (c) 2012 Clarity. All rights reserved.
//

#import "CRSViewControllerBase.h"
#import "AppDelegate.h"
#import "ModalErrorViewController.h"
#import "LoginViewController.h"
#import "BasketController.h"
#import "CRSLocationController.h"
#import "CRSSkinning.h"
#import "OCGProgressOverlay.h"
#import "RESTClient.h"
#import "TillSetupController.h"
#import "TestModeController.h"
#import "ExternalAuthViewController.h"

@interface CRSViewControllerBase () {
    BOOL _loginConfigChanged;
    BOOL _settingsPresented;
    BOOL _loginPresented;
    UINavigationController *_loginNavigationController;
}
- (void) handleConfigurationChange:(NSNotification *)note;
- (void) handleSettingsPresented:(NSNotification *)note;
- (void) handleSettingsDismissed:(NSNotification *)note;
@end

@implementation CRSViewControllerBase
@synthesize modalErrorViewController = _modalErrorViewController;

#pragma mark - Private

- (void) handleConfigurationChange:(NSNotification *)note
{
    if ([[BasketController sharedInstance] needsAuthentication]) {
        if (_loginPresented)
        {
            UIViewController *loginViewController = [self setupLoginViewController];
            [_loginNavigationController setViewControllers:@[loginViewController] animated:!_settingsPresented];
        }
    }
}

- (void) handleSettingsPresented:(NSNotification *)note
{
    _settingsPresented = YES;
}

- (void) handleSettingsDismissed:(NSNotification *)note
{
    _settingsPresented = NO;
    if (_loginConfigChanged && _loginPresented) {
        // dismiss the current login view
        [self dismissModalLoginViewController:self];
    }
    _loginConfigChanged = NO;
}

#pragma mark Object Lifecycle

/** @brief Called after the controller’s view is loaded into memory.
 */
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // update the modal overlay view
    
    // subscribe to configuration changes
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(handleConfigurationChange:)
                                                 name:kBasketControllerConfigurationDidChangeNotification
                                               object:nil];
    // subscribe to settings view notifications
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(handleSettingsPresented:)
                                                 name:kAppDelegateSettingsPresentedNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(handleSettingsDismissed:)
                                                 name:kAppDelegateSettingsDismissedNotification
                                               object:nil];
    
    _loginConfigChanged = NO;
    _settingsPresented = NO;
    _loginPresented = NO;
    _loginNavigationController = nil;
    
    // skinning
    
    [[CRSSkinning currentSkin] applyViewSkin: self];
}

- (void) viewWillAppear:(BOOL)animated
{
    // check the shared requirements
    [super viewWillAppear:animated];
    [self checkSharedRequirements:animated];
}

/** @brief Called when the controller’s view is released from memory.
 */
-(void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:kBasketControllerActivityChangeNotification
                                                  object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:kBasketControllerConfigurationDidChangeNotification
                                                  object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:kAppDelegateSettingsPresentedNotification
                                                  object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:kAppDelegateSettingsDismissedNotification
                                                  object:nil];
}

#pragma mark Properties

/** @brief Returns the reference to the modal error view controller.
 *  @discussion The modal error view controller is responsible for displaying an
 *  error message and provide an action button to guide the user with the next
 *  step.
 *  @return A reference to the modal view controller.
 */
- (ModalErrorViewController *) modalErrorViewController
{
    if (_modalErrorViewController == nil)
        _modalErrorViewController = [self createModalErrorView];
    
    return _modalErrorViewController;    
}

/** @brief Returns the reference to the application delegate.
 *  @return A reference to the application delegate.
 */
- (AppDelegate *) applicationDelegate
{
    return (AppDelegate *)[[UIApplication sharedApplication] delegate];
}

#pragma mark Modal Error Management

/** @brief Creates and initializes the modal error view controller.
 *  @discussion The modal error view controller is responsible for displaying an 
 *  error message and provide an action button to guide the user with the next 
 *  step.
 */
- (ModalErrorViewController *) createModalErrorView
{
    ModalErrorViewController *viewController =
    [[ModalErrorViewController alloc] initWithNibName: @"ModalErrorView"
                                               bundle: nil];
    
    viewController.view.frame = self.view.frame;
    [self.view addSubview: viewController.view];
    return viewController;
}

/** @brief Turns the modal error view visible while displaying the
 *  specified error message.
 *  @param errorMessage The error message that should be displayed.
 *  @param actionButtonCaption The caption for the button that guides the user with the next step.
 *  @param actionButtonBlock The block statements to execute when action button is tapped.
 */
- (void) showModalErrorViewWithMessage: (NSString *)errorMessage
                   actionButtonCaption: (NSString *)actionButtonCaption
                     actionButtonBlock: (void (^)())actionButtonBlock
{
    [self.modalErrorViewController setMessageText: errorMessage];
    [self.modalErrorViewController setActionCaption: actionButtonCaption];
    [self.modalErrorViewController setActionBlock: actionButtonBlock];
    [self.modalErrorViewController.view setHidden: NO];
}

/** @brief Hides the modal error view.
 */
- (void) hideModalErrorView
{
    [self.modalErrorViewController.view setHidden: YES];
}

-(BOOL)resetSharedRequirements
{
    [CRSLocationController setLocationKey:nil];
    [TillSetupController resetTillNumber];
    [[BasketController sharedInstance] setNeedsAuthentication];
    return [self checkSharedRequirements:YES];
}

/** @brief Check the common requirements for all views.
 *  @return YES, if all shared requirements are fullfilled, NO otherwise.
 */
- (BOOL) checkSharedRequirements:(BOOL)animated
{
    if ([[BasketController sharedInstance] needsAuthentication])
    {
        dispatch_async(dispatch_get_main_queue(), ^(void) {
            
            NSString *locationkey = [CRSLocationController getLocationKey];
            NSString *tillNumber = [TillSetupController getTillNumber];

            BOOL needsSettings = (locationkey == nil || tillNumber == nil);
            
            [self presentLoginViewAnimated:animated && !needsSettings];
            
            if (needsSettings)
            {
                [self.applicationDelegate presentSettingsViewAnimated:NO viewController:_loginNavigationController?_loginNavigationController:self];
            }
        });
        
        return NO;
    }
    
    // requirements fullfilled
    
    [self hideModalErrorView];
    return YES;
}

- (void) presentLoginViewAnimated:(BOOL)animated
{
    if (self.navigationController != nil) {
        // we can more accurately determine if the login view controller is presented
        // by checking the nav controller for one
        _loginPresented =
        ([self.navigationController.topViewController isKindOfClass:[LoginViewController class]] ||
         [self.navigationController.topViewController isKindOfClass:[ExternalAuthViewController class]]);
    }
    if (!_loginPresented) {
        _loginPresented = YES;
        // determine if we need to show the login view for local login
        // or a web view for external login
        
        UIViewController *loginViewController = [self setupLoginViewController];
        _loginNavigationController = [[UINavigationController alloc] initWithRootViewController:loginViewController];
        
        [self presentViewController:_loginNavigationController
                           animated:animated
                         completion:nil];
    }
}

-(UIViewController*)setupLoginViewController
{
    UIViewController *loginViewController = nil;
    if (([TestModeController serverMode] == ServerModeOnline) &&
        ([BasketController sharedInstance].externalAuthEnabled)) {
        // use external auth
        ExternalAuthViewController *externalAuthController = [[ExternalAuthViewController alloc] init];
        externalAuthController.delegate = self;
        loginViewController = externalAuthController;
    } else {
        // show the login view
        LoginViewController *localLoginViewController = [[LoginViewController alloc] initWithNibName:@"LoginView"
                                                                                         bundle:nil];
        localLoginViewController.delegate = self;
        loginViewController = localLoginViewController;

    }
    return loginViewController;
}

#pragma mark LoginViewControllerDelegate

/** @brief Called whenever the modal login view controller should be dismissed.
 *  @param sender The LoginViewController that send the message.
 */
- (void) dismissModalLoginViewController:(id)sender
{
    [sender dismissViewControllerAnimated: YES completion: ^{
        _loginPresented = NO;
        _loginNavigationController = nil;
        // TFS61030 - Because of the way the iPad presents view controllers
        // the basket view controller's viewWillAppear is getting called too soon.
        // This would be ok, except that we do the getActiveBasket there.
        // So now we do an active basket check once we have credentials.
        if ([self checkSharedRequirements:YES]) {
            [[BasketController sharedInstance] getActiveBasket];
        }
    }];
}

@end
