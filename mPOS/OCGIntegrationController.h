//
//  IntegrationController.h
//  mPOS
//
//  Created by Antonio Strijdom on 12/08/2013.
//  Copyright (c) 2013 Clarity. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "OCGURLSchemeHandler.h"

/** @file IntegrationController.h */

/** @brief Class for handling launch URLs. */
@interface OCGIntegrationController : OCGURLSchemeHandler

/** @property wasLauchedFromURL
 *  @brief Whether or not the application was launched from a URL.
 */
@property (nonatomic, assign) BOOL wasLauchedFromURL;

/** @brief Returns the singleton instance of the URL integration controller.
 */
+ (OCGIntegrationController *) sharedInstance;

@end
