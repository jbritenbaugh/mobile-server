//
//  OCGSegmentedToggleTableViewCell.h
//  mPOS
//
//  Created by Antonio Strijdom on 15/08/2013.
//  Copyright (c) 2013 Clarity. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol OCGSegmentedToggleTableViewCellDelegate;

@interface OCGSegmentedToggleTableViewCell : UITableViewCell

@property (nonatomic, assign) id<OCGSegmentedToggleTableViewCellDelegate> delegate;
@property (nonatomic, assign) NSInteger selection;

- (void) setOptions:(NSArray *)options;

@end

@protocol OCGSegmentedToggleTableViewCellDelegate <NSObject>
@optional
- (void) segmentTableViewCell:(OCGSegmentedToggleTableViewCell *)cell
                 didSwitchTo:(NSInteger)segmentIndex;
@end
