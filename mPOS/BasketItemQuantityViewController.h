//
//  BasketItemQuantityViewController.h
//  mPOS
//
//  Created by Meik Schuetz on 16/08/2012.
//  Copyright (c) 2012 Clarity. All rights reserved.
//

#import <UIKit/UIKit.h>

/** @file BasketItemQuantityViewController.h */

@class MPOSBasketItem;

/** @brief View controller that implements all logic to allow the user to change
 *  the quantity of a specified basket item.
 */
@interface BasketItemQuantityViewController : UITableViewController

/** @brief A reference to the basket item that is being edited.
 */
@property (weak, nonatomic) MPOSBasketItem *basketItem;

@end
