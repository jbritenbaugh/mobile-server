//
//  ItemDiscountTableViewCell.m
//  mPOS
//
//  Created by Meik Schuetz on 20/08/2012.
//  Copyright (c) 2012 Clarity. All rights reserved.
//

#import "ItemDiscountTableViewCell.h"
#import "CRSSkinning.h"

#define CELL_LEFT_MARGIN        10
#define CELL_TOP_MARGIN         10
#define CELL_RIGHT_MARGIN       10
#define CELL_BOTTOM_MARGIN      10
#define CELL_HORZ_FIELD_MARGIN  5

@implementation ItemDiscountTableViewCell

/** @brief Prepares the receiver for service after it has been loaded from an
 *  Interface Builder archive, or nib file.
 */
- (void) awakeFromNib
{
    // make sure we call the super class
    
    [super awakeFromNib];
    
    // apply the skinning theme
    
    [[CRSSkinning currentSkin] applyCellSkin: self];
}

#pragma mark Properties

/** @brief Sets the discount object.
 *  @discussion Adds additional code that updates the cell's UI elements with
 *  the values from the new discount.
 *  @param discount The new discount to be displayed in this cell instance.
 */
- (void) setDiscount:(id)discount
{
    _discount = discount;
    
    // initialize the cell's subviews
    if ([_discount isKindOfClass:[MPOSBasketRewardSale class]]) {
        MPOSBasketRewardSale *saleDiscount = (MPOSBasketRewardSale *)_discount;
        self.discountDescription.text = saleDiscount.rewardText;
        self.discountValue.text = saleDiscount.rewardAmount.display;
    } else if ([_discount isKindOfClass:[MPOSBasketRewardLine class]]) {
        MPOSBasketRewardLine *lineDiscount = (MPOSBasketRewardLine *)_discount;
        self.discountDescription.text = lineDiscount.rewardText;
        if (self.discountDescription.text.length == 0) {
            if ([lineDiscount isKindOfClass:[MPOSBasketRewardLinePriceAdjust class]]) {
                self.discountDescription.text = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁯󠁅󠁰󠀶󠁂󠁶󠁥󠁵󠁡󠁵󠁺󠁒󠁓󠁵󠁯󠁯󠁦󠁉󠁄󠁨󠁥󠁪󠁃󠁺󠁢󠁶󠁕󠁿*/ @"Price adjust", nil);
            }
        }
        self.discountValue.text = lineDiscount.rewardAmount.display;
    }
    
    [self setNeedsDisplay];
}

#pragma mark Overridden Methods

/** @brief Layouts the cell's controls as specified.
 */
- (void) layoutSubviews
{
    [super layoutSubviews];
    
    /*
    self.discountDescription.backgroundColor = [UIColor lightGrayColor];
    self.discountValue.backgroundColor = [UIColor lightGrayColor];
     */
    
    // get the rectangle of the usable cell area.
    
    CGRect rcCell = CGRectMake(self.contentView.bounds.origin.x + CELL_LEFT_MARGIN,
                               self.contentView.bounds.origin.y + CELL_TOP_MARGIN,
                               self.contentView.bounds.size.width - (CELL_LEFT_MARGIN + CELL_RIGHT_MARGIN),
                               self.contentView.bounds.size.height - (CELL_TOP_MARGIN + CELL_BOTTOM_MARGIN));
    
    // layout the area for the discount description
    
    CGRect rcDiscountDescription = CGRectMake(rcCell.origin.x,
                                              rcCell.origin.y,
                                              rcCell.size.width * 0.75,
                                              rcCell.size.height);
    self.discountDescription.frame = rcDiscountDescription;
    
    // layout the area for the item amount
    
    CGRect rcDiscountValue = CGRectMake(rcDiscountDescription.origin.x + rcDiscountDescription.size.width + CELL_HORZ_FIELD_MARGIN,
                                        rcCell.origin.y,
                                        rcCell.size.width * 0.25,
                                        rcCell.size.height);
    self.discountValue.frame = rcDiscountValue;
}


@end
