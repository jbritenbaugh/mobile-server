//
//  ItemDetailViewController.h
//  mPOS
//
//  Created by Antonio Strijdom on 22/05/2013.
//  Copyright (c) 2013 Clarity. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ItemDetailViewController : UITableViewController <UICollectionViewDataSource, UICollectionViewDelegate>

@property (nonatomic, strong) MPOSItem *item;

@end
