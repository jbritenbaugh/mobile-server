//
//  OCGEFTManager.h
//  mPOS
//
//  Created by Antonio Strijdom on 15/04/2013.
//  Copyright (c) 2013 Clarity. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "OCGEFTManagerCurrentPayment.h"
#import "OCGURLSchemeHandler.h"

/** @file OCGEFTManager.h */

@class ServerError;

#pragma mark - Constants

/** @brief Configured payment provider.
 */
extern NSString * const kOCGEFTManagerConfiguredPaymentProviderKey;

/** @brief Transaction state changed notification.
 */
extern NSString * const kEFTNotificationTransactionChanged;

/** @brief EFT error notification.
 */
extern NSString * const kEFTNotificationError;
/** @brief The key in the userinfo dictionary with the error that was thrown.
 */
extern NSString * const kEFTNotificationErrorErrorKey;

/** @brief The error domain for provider errors. 
 */
extern NSString * const kEFTProviderErrorDomain;

#pragma mark - Types

/** @enum OCGEFTManagerProvider
 *  @brief Enumeration of the providers supported by the OCGEFTManager.
 *  @see OCGEFTManager
 */
typedef NS_ENUM(NSInteger, OCGEFTManagerProvider)
{
    /** @brief No external device detected. */
    OCGEFTManagerProviderNone = 0,
    /** @brief The Payware sled is connected. */
    OCGEFTManagerProviderPayware = 1,
    /** @brief A Datecs device is connected. */
    OCGEFTManagerProviderYesPay = 2,
    /** @brief The Adyen provider is in use. */
    OCGEFTManagerProviderAdyen = 3,
#if DEBUG
    /** @brief A fake provider for testing. */
    OCGEFTManagerProviderFake = 4,
    /** @brief A fake provider for testing direct integration. */
    OCGEFTManagerProviderFakeIntegrated = 5
#endif
};

/** @enum EFTProviderErrorCode
 *  @brief Error codes that are returned from the Payware software. 
 */
typedef NS_ENUM(NSInteger, EFTProviderErrorCode) {
    /** @brief The EFT software is not installed. */
    kEFTProviderErrorCodeNotInstalled = -4,
    /** @brief The external payment device is not attached or not working. */
    kEFTProviderErrorCodeNotInSled = -3,
    /** @brief The EFT provider is busy processing a payment. */
    kEFTProviderErrorCodeBusy = -2,
    /** @brief A response was received, but it wasn't from the configured EFT provider. */
    kEFTProviderErrorCodeInvalidSource = -1,
    /** @brief Success (For a transaction, this means the transaction
     *  is completed successfully).
     */
    kEFTProviderErrorCodeSuccess = 0,
    /** @brief Application error.
     */
    kEFTProviderErrorCodeApplicationError = 1,
    /** @brief Approved with communication failure during Save Request.
     *  (Need to call again with retryStoredTran set to 1)
     */
    kEFTProviderErrorCodeSaveCommsFailure = 2,
    /** @brief Communication failure during Authorization Request.
     *  Transaction needs to be cancelled.
     */
    kEFTProviderErrorCodeAuthCommsFailure = 3,
    /** @brief Communication failure during the Validation Request.
     *  Transaction needs to be retried.
     */
    kEFTProviderErrorCodeValidationCommsFailure = 4,
    /** @brief Communication failure when calling retry.
     */
    kEFTProviderErrorCodeRetryCommsFailure = 5,
    /** @brief Signature required.
     *  @note This error will kick off the signature capture process for providers that support it.
     */
    kEFTProviderErrorCodeSignatureRequired = 6
};

#pragma mark - Blocks

@protocol OCGEFTProviderProtocol;

/** @brief Completion block for EFT provider calls.
 *  @param resultCode The result of the request, the error code or kEFTProviderErrorCodeSuccess if the callout succeeded.
 *  @param error Any errors that may have been raised or nil.
 *  @param suppressError Whether or not to display the error.
 *  @see EFTProviderErrorCode
 */
typedef void(^EFTProviderCompleteBlock)(EFTProviderErrorCode resultCode, NSError *error, BOOL suppressError);

/** @brief Completion block for EFT provider transaction calls.
 *  @param reason The reason the transaction completed.
 *  @param resultCode The result of the request, the error code or kEFTProviderErrorCodeSuccess if the callout succeeded.
 *  @param error Any errors that may have been raised or nil.
 *  @see EFTProviderErrorCode
 */
typedef void(^EFTProviderTransactionCompleteBlock)(EFTPaymentCompleteReason reason, EFTProviderErrorCode resultCode, NSError *error);

#pragma mark - Protocols

@class OCGEFTManager;

/** @protocol OCGEFTProviderProtocol
 *  @brief Defines a common interface for all EFT provider classes.
 */
@protocol OCGEFTProviderProtocol <NSObject>
@required
/** @brief Initialiser.
 *  @param manager The manager for this provider.
 */
- (instancetype) initForManager:(OCGEFTManager *)manager;

/** @brief Checks if the configured provider is a URL provider
 *  @return YES if the provider is a URL provider, otherwise NO.
 */
- (BOOL) isURLProvider;

/** @brief Gets the last error returned from the provider.
 *  or nil if the last response wasn't an error.
 */
- (ServerError *) getLastServerError;

/** @brief Determines if the EFT transaction can be processed.
 *  @param forTx Whether or not this check is for a transaction or not.
 *  @param complete The block to execute once the checks are complete.
 *  @return YES if the transaction can proceed, otherwise no.
 */
- (void) preflightCheckForTransaction:(BOOL)forTx
                             complete:(EFTProviderCompleteBlock)complete;

/** @brief Performs a card sale for a specific amount.
 *  @param tender The tender to add the payment with.
 *  @param amount The amount to take a payment for.
 *  @param basketId The unique ID of the basket being paid for.
 *  @param customerEmail The customer's email address.
 *  @param complete The block to execute when the payment is complete.
 */
- (void) cardSaleWithTender:(Tender *)tender
                     amount:(NSNumber *)amount
            forBasketWithID:(NSString *)basketId
          withCustomerEmail:(NSString *)customerEmail
                   complete:(EFTProviderCompleteBlock)complete;

/** @brief Performs a card refund for a specific amount.
 *  @param tender The tender to add the payment with.
 *  @param amount The amount to refund.
 *  @param basketId The unique ID of the basket containing the refund.
 *  @param customerEmail The customer's email address
 *  @param complete The block to execute when the payment is complete.
 */
- (void) cardRefundWithTender:(Tender *)tender
                   withAmount:(NSNumber *)amount
              forBasketWithID:(NSString *)basketId
            withCustomerEmail:(NSString *)customerEmail
                     complete:(EFTProviderCompleteBlock)complete;

/** @brief Whether or not the provider supports voiding.
 *  @return YES if the provider supports voiding, otherwise NO.
 */
- (BOOL) voidsAllowed;

/** @brief Attempts to process a void for the transaction with the reference specified.
 *  @param originalTxReference The unique merchant reference for the transaction.
 *  @param complete The block to execute when the void is complete.
 */
- (void) voidTransactionWithReference:(NSString *)originalTxReference
                             complete:(EFTProviderCompleteBlock)complete;

/** @brief Resumes an in progress transaction.
 *  @param complete The block to execute when the payment is complete.
 */
- (void) resumeTransactionComplete:(EFTProviderTransactionCompleteBlock)complete;

/** @brief Serialises the transaction into a raw string.
 *  @return The string or nil.
 */
- (NSString *) RawStringForTransaction;

@optional

/** @brief Optional description for the additional setup step.
 *  @return The optional description.
 */
- (NSString *) additionalSetupDescription;

/** @brief Optional description for the additional setup value.
 *  @return The optional value;
 */
- (NSString *) additionalSetupValue;

/** @brief Begins the additional setup step.
 *  @param complete The block to execute when setup is complete.
 */
- (void) additionalSetupComplete:(dispatch_block_t)complete;

/** @brief Sends the retry stored transaction message to the payment provider.
 *  @return The error code or kEFTProviderErrorCodeSuccess if the callout succeeded.
 */
- (EFTProviderErrorCode) retryStoredTransaction;

/** @brief Cancels the payment transaction in progress.
 *  @note This method may not necessarily cancel the payment, but it will always attempt to request the cancellation.
 */
- (void) cancelTx;

/** @brief Whether or not to show the "remove card" alert.
 *  @return YES to show the alert, no to skip it.
 */
- (BOOL) showRemoveCardAlert;

/** @brief Legacy method for building the basket tender from the provider transaction.
 *  @return The basket tender for the transaction.
 *  @note The transaction property must be set before calling this method.
 */
- (BasketTender *) updatePaymentFromTransaction;

/** @brief Any additional status information the provider wants to display.
 *  @return Additional provider status or nil.
 *  @note The additional information will not be displayed until a
 *  kEFTNotificationTransactionChanged notification is posted.
 */
- (NSString *) localizedProviderStatus;

/** @brief Informs the provider that the signatures matched.
 *  @param signature The signature.
 *  @param complete The block to execute after the signature is processed.
 */
- (void) confirmSignature:(UIImage *)signature
                 complete:(EFTProviderCompleteBlock)complete;

/** @brief Informs the provider that the signatures did not match.
 *  @param signature The signature.
 *  @param complete The block to execute after the signature is processed.
 */
- (void) unconfirmSignature:(UIImage *)signature
                   complete:(EFTProviderCompleteBlock)complete;

@end

#pragma mark - Classes

/** @brief Class for managing communication to external EFT payment providers.
 */
@interface OCGEFTManager : NSObject <OCGURLProviderProtocol>

/** @property configured
 *  @brief Indicates whether or not the EFT settings are present and correct.
 */
@property (nonatomic, readonly) BOOL configured;

/** @property currentPayment
 *  @brief The current EFT payment in progress.
 *  @discussion If no payment is in progress, this will be nil.
 */
@property (nonatomic, readonly) OCGEFTManagerCurrentPayment *currentPayment;

/** @property processing
 *  @brief YES if there is currently a transaction in progress, otherwise NO.
 */
@property (nonatomic, readonly) BOOL processing;

/** @property supportsVoids;
 *  @brief YES if voids are supported, otherwise NO.
 */
@property (nonatomic, readonly) BOOL supportsVoids;

/** @brief Gets the shared EFT manager.
 *  @return The default shared EFT manager.
 */
+ (OCGEFTManager *) sharedInstance;

/** @brief Returns a list of supported payment providers.
 *  @return A list of supported payment providers as an NSArray of NSNumbers.
 */
- (NSArray *) supportedPaymentProviders;

/** @brief Gets the currently configured EFT provider.
 *  @note This does not return the actual provider instance, just the value of the setting.
 *  @return The configured provider or OCGEFTManagerProviderNone if none is configured.
 *  @see OCGEFTManagerProvider
 */
- (OCGEFTManagerProvider) getConfiguredProvider;

/** @brief Sets the currently configured EFT provider.
 *  @note This will reset the current provider instance by calling resetProvider.
 *  @param provider The new provider.
 *  @see OCGEFTManagerProvider
 */
- (void) setConfiguredProvider:(OCGEFTManagerProvider)provider;

/** @brief Gets the internal name of the specified provider.
 *  @param device The provider to get the name of.
 *  @return The name of the provider.
 */
- (NSString *) nameForProvider:(OCGEFTManagerProvider)provider;

/** @brief Gets the enum value of the specified provider.
 *  @param name The name of the provider.
 *  @return The enum value of the provider.
 */
- (OCGEFTManagerProvider) providerForName:(NSString *)name;

/** @brief Gets the human readable localized name of the specified provider for display.
 *  @param device The provider to get the name of.
 *  @return The name of the provider.
 */
- (NSString *) localizedNameForProvider:(OCGEFTManagerProvider)provider;

/** @brief Resets the current provider, by retreiving the new one from the application settings.
 */
- (void) resetProvider;

/** @brief Whether the EFT device is configured to allow device selection.
 *  @return YES if selection or scanning (or both) is allowed, otherwise NO.
 */
- (BOOL) deviceSelectionAllowed;

/** @brief Gets information about whether addition setup is required for the current provider.
 *  @param setupDescription If provided, returns the name of the setup step required.
 *  @param setupValue If provided, returns the value of additional setup.
 *  @return YES if additional setup is required, otherwise NO.
 */
- (BOOL) providerRequiresAdditionalSetupWithDescription:(NSString **)setupDescription
                                               andValue:(NSString **)setupValue;

/** @brief Begins the additional setup step.
 *  @param complete The block to execute when setup is complete.
 */
- (void) additionalProviderSetupComplete:(dispatch_block_t)complete;

/** @brief Any status information the provider has to display on screen.
 *  @return The additional status information or nil.
 */
- (NSString *) additionalProviderStatus;

/** @brief Performs an EFT transaction of the specified type.
 *  @param transactionType The type of transaction to execute.
 *  @param amount The amount of the EFT transaction.
 *  @param currency The currency of the EFT transaction (or nil to use the tender currency).
 *  @param tender The tender type that will be used for the payment.
 *  @param basket The basket the payment should be added to.
 */
- (void) EFTPaymentForTransactionType:(EFTPaymentTransactionType)transactionType
                           withAmount:(NSNumber *)amount
                           inCurrency:(NSString *)currency
                            forTender:(Tender *)tender
                            forBasket:(BasketAssociateDTO *)basket;

/** @brief Immediately ends any EFT transactions in progress.
 */
- (void) endCurrentPayment;

/** @brief Cancels the payment transaction in progress.
 *  @note This method may not necessarily cancel the payment, but it will always attempt to request the cancellation.
 */
- (void) cancelTx;

/** @brief Attempts to process a void for the transaction with the reference specified.
 *  @param transaction The transaction to void.
 *  @param complete The block to execute when the void is complete.
 */
- (void) voidTransaction:(MPOSEFTPayment *)transaction
                complete:(EFTProviderCompleteBlock)complete;

/** @brief Legacy method for dealing with 'stuck' transactions in Payware.
 */
- (void) retryStoredTransaction;

/** @brief The signature was reported as confirmed as matching the source sample.
 *  @param signature  The signature captured.
 */
- (void) confirmSignature:(UIImage *)signature;

/** @brief The signature was reported as NOT matching the source sample.
 *  @param signature  The signature captured.
 */
- (void) unConfirmSignature:(UIImage *)signature;

/** @brief Checks to see if the current payment being processed has been added 
 *  to the current sales basket.
 *  @return YES if the payment has been added, otherwise NO.
 */
- (BOOL) hasPaymentBeenAdded;

@end
