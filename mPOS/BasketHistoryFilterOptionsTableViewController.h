//
//  BasketHistoryFilterOptionsTableViewController.h
//  mPOS
//
//  Created by Antonio Strijdom on 22/11/2012.
//  Copyright (c) 2012 Clarity. All rights reserved.
//

#import "CRSEditableTableViewController.h"
#import "BasketHistoryFilterOptionsDataSource.h"
#import "CRSGlossyButton.h"

/** @file CRSEditableTableViewController.h */

@protocol BasketHistoryFilterOptionsTableViewControllerDelegate;

/** @brief The table view controller, delegate and datasource for the filter options editable table view.
 */
@interface BasketHistoryFilterOptionsTableViewController : CRSEditableTableViewController

/** @property delegate
 */
@property (nonatomic, weak) id<BasketHistoryFilterOptionsTableViewControllerDelegate> delegate;

/** @property selectedOptions
 *  @brief A dictionary containing the current filter options.
 */
@property (nonatomic, strong) NSDictionary *selectedOptions;

/** @property locations
 *  @brief The location list.
 */
@property (nonatomic, strong) NSArray *locations;

/** @brief User tapped the filter button.
 */
- (IBAction) filterTapped:(id)sender;

/** @brief User tapped the clear button.
 */
- (IBAction) clearTapped:(id)sender;

@end

/** @brief Delegate protocol for the options table view.
 */
@protocol BasketHistoryFilterOptionsTableViewControllerDelegate <NSObject>
@required
/** @brief Informs the delegate of the users selected filter options.
 *  @param vc The view controller.
 *  @param options Dictionary containing the selected filter options or nil if the filter was cleared.
 */
- (void) basketHistoryFilterOptionsVC:(BasketHistoryFilterOptionsTableViewController *)vc
                       didApplyFilter:(NSDictionary *)options;
@end
