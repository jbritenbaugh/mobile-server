//
//  UIImage+OCGExtensions.h
//  mPOS
//
//  Created by John Scott on 03/12/2014.
//  Copyright (c) 2014 Clarity. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (OCGExtensions)

+ (UIImage*)OCGExtensions_imageNamed:(NSString*)name;

+ (UIImage*)OCGExtensions_imageNamed:(NSString*)name tint:(UIColor*)tint;

@end
