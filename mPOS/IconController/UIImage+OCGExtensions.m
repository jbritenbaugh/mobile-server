//
//  UIImage+OCGExtensions.m
//  mPOS
//
//  Created by John Scott on 03/12/2014.
//  Copyright (c) 2014 Clarity. All rights reserved.
//

#import "UIImage+OCGExtensions.h"

#import "RXMLElement.h"
#import "NSRegularExpression+OCGExtensions.h"

@implementation UIImage (OCGExtensions)

+(void)load
{
    __weak __block id observer =
    [NSNotificationCenter.defaultCenter addObserverForName:UIApplicationDidFinishLaunchingNotification
                                                    object:nil
                                                     queue:NSOperationQueue.mainQueue
                                                usingBlock:^(NSNotification *note) {
                                                    [self OCGExtensions_exportIcons];
                                                    [NSNotificationCenter.defaultCenter removeObserver:observer];
                                                }];
}

+(NSDictionary *)iconPaths:(NSArray*)iconFiles
{
    NSMutableDictionary *iconPaths = [NSMutableDictionary dictionary];
    
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:@"(F1|M|L|C|Z)|(?:([\\d\\.\\-]+),([\\d\\.\\-]+))"
                                                                           options:0
                                                                             error:NULL];
    
    for (NSString *iconsFile in iconFiles)
    {
        RXMLElement *root = [RXMLElement elementFromURL:[NSBundle.mainBundle URLForResource:iconsFile withExtension:@"xaml"]];
        
        CGAffineTransform transform = CGAffineTransformIdentity;
        transform = CGAffineTransformScale(transform, 1, -1);
        transform = CGAffineTransformTranslate(transform, 0, -1);
        CGAffineTransform *transformPointer = &transform;
        
        [root iterate:@"Style.Setter"
           usingBlock:^(RXMLElement *childElement) {
               NSString *property = [childElement attribute:@"Property"];
               NSString *value = [childElement attribute:@"Value"];
               
               
               if ([property isEqualToString:@"Width"])
               {
                   *transformPointer = CGAffineTransformScale(*transformPointer, 1. / [value doubleValue], 1);
               }
               else if ([property isEqualToString:@"Height"])
               {
                   *transformPointer = CGAffineTransformScale(*transformPointer, 1, 1. / [value doubleValue]);
               }
           }];
        
        [root iterate:@"Geometry"
           usingBlock:^(RXMLElement *childElement) {
               NSString *key = [childElement attribute:@"Key"];
               NSString *data = childElement.text;
               
               CGMutablePathRef path = CGPathCreateMutable();
               
               __block NSString *command = nil;
               __block CGPoint *points = calloc(3, sizeof(CGPoint));
               __block NSUInteger pointCount = 0;
               
               [regex enumerateMatchesInString:data
                                       options:0
                                         range:NSMakeRange(0, data.length)
                                    usingBlock:^(NSTextCheckingResult *result, NSMatchingFlags flags, BOOL *stop)
                {
                    NSRange commandRange = [result rangeAtIndex:1];
                    NSRange xRange = [result rangeAtIndex:2];
                    NSRange yRange = [result rangeAtIndex:3];
                    
                    if (commandRange.location != NSNotFound)
                    {
                        command = [data substringWithRange:commandRange];
                        pointCount = 0;
                    }
                    else if (xRange.location != NSNotFound && yRange.location != NSNotFound && command)
                    {
                        points[pointCount].x = [[data substringWithRange:xRange] doubleValue];
                        points[pointCount].y = [[data substringWithRange:yRange] doubleValue];
                        pointCount++;
                    }

                    
                    if ([command isEqual:@"M"])
                    {
                        if (pointCount >= 1)
                        {
                            CGPathMoveToPoint(path, transformPointer, points[0].x, points[0].y);
                            pointCount = 0;
                        }
                    }
                    else if ([command isEqual:@"L"])
                    {
                        if (pointCount >= 1)
                        {
                            CGPathAddLineToPoint(path, transformPointer, points[0].x, points[0].y);
                            pointCount = 0;
                        }
                    }
                    else if ([command isEqual:@"C"])
                    {
                        if (pointCount >= 3)
                        {
                            CGPathAddCurveToPoint(path, transformPointer, points[0].x, points[0].y, points[1].x, points[1].y, points[2].x, points[2].y);
                            pointCount = 0;
                        }
                    }
                    else if ([command isEqual:@"Z"])
                    {
                        CGPathCloseSubpath(path);
                        pointCount = 0;
                    }
                    else
                    {
                        command = nil;
                        pointCount = 0;
                    }
                }];
               
               free(points);
               
               iconPaths[key] = CFBridgingRelease(path);
           }];
    }
    return iconPaths;
}

+ (UIImage*)OCGExtensions_imageNamed:(NSString*)name color:(UIColor*)color size:(CGSize)size
{
    if (name == nil)
    {
        return nil;
    }
    
    static NSDictionary *iconPaths = nil;
    
    if (iconPaths == nil) {
        ocg_once((^{
            iconPaths = [self iconPaths:@[@"Glyphish", @"mPOS", @"Icons"]];
        }));
        
    }
    
    if (!iconPaths[name])
    {
        NSDictionary *oldNames = @{
                                   // menu_icons/
                                   
                                   @"ADD_DISCOUNT" : @"BlankLabel",
                                   @"ADD_RETURN_ITEM" : @"AnticlockwiseArrow",
                                   @"ASSIGN_AFFILIATION" : @"PersonWithBasket",
                                   @"ASSIGN_EMAIL_ADDRESS" : @"Envelope",
                                   @"CANCEL_SALE" : @"TrashCan",
                                   @"CHECKOUT_SALE" : @"Cart",
                                   @"CONTEXT_MENU_ITEM_BASKET" : @"Basket",
                                   @"CONTEXT_MENU_ITEM_MENU" : @"Menu",
                                   @"CONTEXT_MENU_ITEM_SCAN" : @"Barcode",
                                   @"CONTEXT_MENU_ITEM_SEARCH" : @"MagnifyingGlass",
                                   @"DEVICE_STATION_STATUS" : @"EKG",
                                   @"EDIT_BASKET" : @"Pencil",
                                   @"FINISH_SALE" : @"SkipForward",
                                   @"ITEM_DETAILS" : @"MagnifyingGlass",
                                   @"ITEM_SEARCH" : @"MagnifyingGlass",
                                   @"LOGOUT_OPERATOR" : @"LeftOutArrow",
                                   @"LOOKUP_CUSTOMER" : @"People",
                                   @"MENU" : @"Select",
                                   @"MODIFY_PRICE" : @"BlankLabel",
                                   @"MODIFY_QUANTITY" : @"Plus",
                                   @"MODIFY_SALESPERSON" : @"Person",
                                   @"MODIFY_SALE_DISCOUNT" : @"BlankLabel",
                                   @"MODIFY_SETTINGS" : @"Cog",
                                   @"OPEN_CASH_DRAWER" : @"Eject",
                                   @"OPTIONAL_MODIFIERS" : @"Plus",
                                   @"PRINT_GIFT_RECEIPT" : @"Gift",
                                   @"PRINT_LAST_TRANSACTION" : @"Printer",
                                   @"PRINT_PRINTLINE" : @"Printer",
                                   @"REMOVE_DISCOUNT" : @"BlankLabel",
                                   @"REMOVE_ITEM" : @"TrashCan",
                                   @"REPRINT_RECEIPT" : @"Printer",
                                   @"RESUME_SALE" : @"ClockwiseDoubleArrow",
                                   @"RELOAD_OPTIONS" : @"Cog",
                                   @"SKIP_PRINTLINE" : @"SkipForward",
                                   @"SUSPEND_SALE" : @"Pause",
                                   @"SWIPE_DATA" : @"Card",
                                   @"TAX_FREE_SALE" : @"Airplane",
                                   @"TOGGLE_TRAINING_MODE" : @"GraduationHat",
                                   @"UNCHECKOUT_SALE" : @"Cart",
                                   @"EFT_CANCEL_TX" : @"Cross",
                                   @"EFT_FINISH_TX" : @"TickInCircle",
                                   @"EFT_CONFIRM_SIGNATURE" : @"Tick",
                                   @"EFT_UNCONFIRM_SIGNATURE" : @"Cross",
                                   @"EFT_RETRY_STORED_TX" : @"ClockwiseArrow",
                                   
                                   // tender_icons
                                   
                                   @"tender_class_ca" : @"Money",
                                   @"tender_class_cr" : @"CreditCard",
                                   @"tender_class_mc" : @"Coupon",
                                   @"tender_class_sc" : @"Coupon",
                                   @"tender_class_un" : @"QuestionMarkInCircle",
                                   @"tender_class_vo" : @"Receipt",
                                   
                                   
                                   };
        
        
        if (oldNames[name])
        {
            name = oldNames[name];
        }
        else
        {
            return [self imageNamed:[@"menu_icons/" stringByAppendingString:name]];
        }
    }
    
    
    
    CGPathRef path = (__bridge CGPathRef)(iconPaths[name]);
    
    if (path)
    {
        CGContextRef context = CGBitmapContextCreate(NULL,
                                                     floor(size.width),
                                                     floor(size.height),
                                                     8,
                                                     4*floor(size.width),
                                                     CGColorSpaceCreateDeviceRGB(),
                                                     (CGBitmapInfo) kCGImageAlphaPremultipliedLast);
        
        CGContextSetFillColorWithColor(context, [color CGColor]);
        CGContextConcatCTM(context, CGAffineTransformMakeScale(size.width, size.height));
        CGContextAddPath(context, path);
        CGContextDrawPath(context, kCGPathFill);
        
        CGImageRef CGImage = CGBitmapContextCreateImage(context);
        
        UIImage *image = [UIImage imageWithCGImage:CGImage scale:2. orientation:UIImageOrientationUp];
        CFRelease(context);
        CFRelease(CGImage);
        return image;
        
    }
    return nil;
}

+ (UIImage*)OCGExtensions_imageNamed:(NSString*)name
{
    UIColor *color = [UIColor colorWithRed:0.27 green:0.27 blue:0.27 alpha:1.0];
    return [self OCGExtensions_imageNamed:name
                                    color:color
                                     size:CGSizeMake(64, 64)];
}

+ (UIImage*)OCGExtensions_imageNamed:(NSString*)name tint:(UIColor*)tint
{
    return [self OCGExtensions_imageNamed:name
                                    color:tint
                                     size:CGSizeMake(64, 64)];
  
}

+(void)OCGExtensions_exportIcons
{
    NSFileManager *defaultManager = [NSFileManager defaultManager];
    
    NSString *userName = NSUserName();
    
    #if TARGET_IPHONE_SIMULATOR
    if (userName.length == 0)
    {
        NSArray *bundlePathComponents = [NSBundle.mainBundle.bundlePath pathComponents];
        
        if (bundlePathComponents.count >= 3
            && [bundlePathComponents[0] isEqualToString:@"/"]
            && [bundlePathComponents[1] isEqualToString:@"Users"])
        {
            userName = bundlePathComponents[2];
        }
    }
    #endif
    
    if (userName.length > 0)
    {
        NSString *rootExportPath = [NSString stringWithFormat:@"/Users/%@/Desktop/mPOS Icons", userName];
        
        for (NSString *iconFile in @[@"Glyphish", @"mPOS", @"Icons"])
        {
            NSString *exportPath = [rootExportPath stringByAppendingPathComponent:iconFile];
            if (![defaultManager fileExistsAtPath:exportPath])
            {
                [defaultManager createDirectoryAtPath:exportPath
                          withIntermediateDirectories:YES
                                           attributes:nil
                                                error:NULL];
                
                NSDictionary *iconPaths = [self iconPaths:@[iconFile]];
                for (NSString *iconName in iconPaths.allKeys)
                {
                    UIImage *image = [self OCGExtensions_imageNamed:iconName];
                    NSData *data = UIImagePNGRepresentation (image);
                    [data writeToFile:[[exportPath stringByAppendingPathComponent:iconName] stringByAppendingPathExtension:@"png"] atomically:NO];
                }
            }
        }
    }
}

@end
