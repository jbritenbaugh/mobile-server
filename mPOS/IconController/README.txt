XAML icons
^^^^^^^^^^

The only canonical icons in this set of three files is Icons.xaml. All others should be retired when the opportunity arises.

The files are:

Glyphish
^^^^^^^^

This file contains icons we use from the Glyphish icons set transposed into XAML.

Icons
^^^^^

Is file is direct copy from $/FREEDOM/ClarityLive/NextPointRelease/Client/Sandstorm/Skins/BaseOmnicoGreen/Icons.xaml

mPOS
^^^^

These are custom icons derived from icons in Icons by the mobile team to match a specific need.


 