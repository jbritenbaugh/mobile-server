//
//  LocationDataSource.h
//  mPOS
//
//  Created by Antonio Strijdom on 10/08/2012.
//  Copyright (c) 2012 Clarity. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CRSLocationSummary.h"

/** @file LocationDataSource.h */

static NSString *kLocationDataSourceErrorDomain = @"kLocationDataSourceErrorDomain";
static const NSInteger kLocationDataSourceErrorServerErrorCode = 1000;
static NSString *kLocationDataSourceErrorServerErrorKey = @"kLocationDataSourceErrorServerErrorKey";

/** @brief The datasource for the location setting table view. */
@interface LocationDataSource : NSObject

- (void) getLocationListWithBlock:(void (^)(id data, NSError *error))complete;


/** @brief Gets the summary for the currently configured location.
 *  @param locationKey The key of the location
 *  @param serviceBaseURL The RESTClient's serviceBaseURL
 *  @param complete The block to execute when the location is retreived.
 *  @note The location should be returned as a CRSLocationSummary object.
 *  @note If no store is currently configured, this method will simply return nil.
 */
- (void) getLocationSummaryForLocationKey:(NSString*)key
                           serviceBaseURL:(NSString*)serviceBaseURL
                                    block:(void (^)(CRSLocationSummary *result, NSError *error))complete;

/** @brief Gets the current location list for displaying in the table.
 *  @param serviceBaseURL The RESTClient's serviceBaseURL
 *  @param complete The block to execute when the location list is loaded.
 *  @note The location list should be an NSArray of CRSLocationSummary objects.
 */
- (void) getLocationListForServiceBaseURL:(NSString*)serviceBaseURL
                                    block:(void (^)(NSMutableArray *locations, NSError *error))complete;


- (void) getCurrentLocationSummaryWithBlock:(void (^)(id data, NSError *error))complete;

@end
