//
//  OCGEFTDeviceSelectionRDMDatasource.m
//  mPOS
//
//  Created by Antonio Strijdom on 16/03/2015.
//  Copyright (c) 2015 Clarity. All rights reserved.
//

#import "OCGEFTDeviceSelectionRDMDatasource.h"
#import "Device+MPOSExtensions.h"
#import "OCGSettingsController.h"
#import "BasketController.h"
#import "OCGEFTManager.h"

@implementation OCGEFTDeviceSelectionRDMDatasource

#pragma mark - Init

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.deviceType = @"PED";
    }
    return self;
}

#pragma mark - DeviceSelectionViewControllerDataSource

- (NSString *) deviceSelectionControllerUsageText:(DeviceSelectionViewController *)deviceSelectionViewController
{
    return NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁴󠁯󠁗󠀳󠁪󠁙󠁊󠁎󠁃󠀰󠁴󠁹󠁌󠁸󠁌󠁃󠀸󠁉󠁲󠀰󠁢󠁋󠁶󠀹󠁶󠁩󠁣󠁿*/ @"EFT payments", nil);
}

- (Device *) deviceSelectionViewControllerDeviceClass:(DeviceSelectionViewController *)deviceSelectionViewController
{
    // get the EFT "device"
    Device *EFTDevice = [[BasketController sharedInstance] deviceForSettingWithName:kOCGEFTManagerConfiguredPaymentProviderKey];
    return EFTDevice;
}

@end
