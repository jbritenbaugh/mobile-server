//
//  OCGApplication.m
//  mPOS
//
//  Created by John Scott on 20/05/2014.
//  Copyright (c) 2014 Clarity. All rights reserved.
//

#import "OCGApplication.h"

@implementation OCGApplication
{
    NSTimeInterval _activityTimestamp;
    BOOL _inactivityPeriodTrackingEnabled;
}

-(id)init
{
    self = [super init];
    if (self)
    {
        _activityTimestamp = NSProcessInfo.processInfo.systemUptime;
    }
    return self;
}

- (void)sendEvent:(UIEvent *)event {
    [super sendEvent:event];
    
    switch (event.type)
    {
        case UIEventTypeTouches:
        case UIEventTypeMotion:
            _activityTimestamp = event.timestamp;
            break;

        default:
            break;
    }
}

-(NSTimeInterval)inactivityPeriod
{
    return _inactivityPeriodTrackingEnabled ? NSProcessInfo.processInfo.systemUptime - _activityTimestamp : 0;
}

- (void)setInactivityPeriodTrackingEnabled:(BOOL)inactivityPeriodTrackingEnabled
{
    _inactivityPeriodTrackingEnabled = inactivityPeriodTrackingEnabled;
    if (inactivityPeriodTrackingEnabled)
    {
        _activityTimestamp  = NSProcessInfo.processInfo.systemUptime;
    }
}

- (BOOL)inactivityPeriodTrackingEnabled
{
    return _inactivityPeriodTrackingEnabled;
}

@end
