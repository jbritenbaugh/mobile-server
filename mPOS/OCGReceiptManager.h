//
//  OCGReceiptManager.h
//  mPOS
//
//  Created by Antonio Strijdom on 09/10/2013.
//  Copyright (c) 2013 Clarity. All rights reserved.
//

#import <Foundation/Foundation.h>

/** @file OCGReceiptManager.h */

/** @enum ReceiptManagerType 
 *  @brief Enum for receipt manager types.
 */
NS_ENUM(NSInteger, ReceiptManagerType) {
    ReceiptManagerTypeClient = 0,
    ReceiptManagerTypeServer = 1
};

/** @brief OCGReceiptManager encapsulates all receipt functionality.
 */
@interface OCGReceiptManager : NSObject

/** @property printing
 *  @brief Whether or not the receipt manager is currently printing a receipt.
 */
@property (nonatomic, readonly) BOOL printing;

/** @brief Returns the configured receipt manager.
 *  @return The configured receipt manager. Defaults to ReceiptManagerTypeClient.
 */
+ (enum ReceiptManagerType) configuredReceiptManagerType;

/** @brief Returns the configured receipt manager.
 */
+ (OCGReceiptManager *) configuredManager;

/** @brief Called to email the receipt.
 *  @param complete The block to execute after the email address has been captured.
 */
- (void) emailReceiptComplete:(dispatch_block_t)complete;

/** @brief Called to print the receipt.
 */
- (void) printReceipt;

/** @brief Called to print the summary receipt.
 */
- (void) printSummaryReceipt;

-(BOOL)printRTIPrintData:(NSString*)printData error:(NSError**)error;

-(NSAttributedString*)attributedStringFromRTIPrintData:(NSString*)printData;

-(void)testRTIReceipt;

@end
