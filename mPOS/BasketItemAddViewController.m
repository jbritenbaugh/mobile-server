//
//  BasketItemAddViewController.m
//  mPOS
//
//  Created by John Scott on 03/02/2014.
//  Copyright (c) 2014 Clarity. All rights reserved.
//

#import "BasketItemAddViewController.h"
#import "CRSSkinning.h"
#import "OCGTableViewCell.h"
#import "OCGTextField.h"
#import "BasketController.h"
#import "POSFuncController.h"
#import "ServerErrorHandler.h"
#import "OCGSelectViewController.h"
#import "OCGNumberTextFieldValidator.h"
#import "OCGKeyboardTypeButtonItem.h"

@interface BasketItemAddViewController () <UIAlertViewDelegate, UITextFieldDelegate>
@property (nonatomic, strong) UIBarButtonItem *doneButton;
@property (nonatomic, strong) MPOSBasketItem *processingBasketItem;
@property (nonatomic, strong) ServerErrorMessage *processingErrorMessage;
@property (nonatomic, strong) NSMutableArray *processedErrors;
@property (nonatomic, strong) NSMutableArray *processedErrorValues;
@property (nonatomic, strong) OCGNumberTextFieldValidator *numberValidator;
@property (nonatomic, strong) OCGNumberTextFieldValidator *priceValidator;
- (void) setValue:(NSString *)value forError:(ServerErrorMessage *)error;
- (void) updateValidator;
- (void) updateCellForProcessingErrorMessage:(OCGTableViewCell *)cell;
- (void) updateDoneButtonStatus;
- (void) cancelAddBasketItem;
- (void) finishProcessingBasketItemCancelled:(BOOL)cancelled;
- (void) recheckForAddBasketItemErrors;
- (void) chooseDumpCode:(DumpCode *)dumpCode;
- (IBAction) cancelTapped:(id)sender;
- (IBAction) doneTapped:(id)sender;
@end

@implementation BasketItemAddViewController

#pragma mark - Private

static NSString *kCellIdentifier = @"Cell";

- (void) setValue:(NSString *)value forError:(ServerErrorMessage *)error
{
    NSInteger existing = NSIntegerMin;
    // check if we have this error already (we are reprocessing)
    for (NSInteger i = 0; i < self.processedErrors.count; i++) {
        if (((NSNumber *)self.processedErrors[i]).integerValue == error.codeValue) {
            existing = i;
            break;
        }
    }
    if (existing != NSIntegerMin) {
        [self.processedErrors removeObjectAtIndex:existing];
        [self.processedErrorValues removeObjectAtIndex:existing];
    }
    // add the error to the array
    [self.processedErrors addObject:error.code];
    [self.processedErrorValues addObject:value];
}

- (void) updateValidator
{
    // reset to defaults
    self.numberValidator.currencyCode = BasketController.sharedInstance.currencyCode;
    self.numberValidator.maxValue = INT_MAX;
    self.numberValidator.minValue = 0;
    
    if (self.processingErrorMessage.codeValue == MShopperUnitOfMeasureRequiredExceptionCode) {
        ServerErrorUnitOfMeasureMessage *unitOfMeasureMessage = (ServerErrorUnitOfMeasureMessage *)self.processingErrorMessage;
        self.numberValidator.scale = [unitOfMeasureMessage.scale integerValue];
        self.numberValidator.maxValue = [unitOfMeasureMessage.maxValue doubleValue];
    } else if (self.processingErrorMessage.codeValue == MShopperItemQtyRequiredExceptionCode) {
        self.numberValidator.scale = 0;
        NSString *maxQty = BasketController.sharedInstance.clientSettings.maxItemQuantity;
        if (maxQty != nil) {
            self.numberValidator.maxValue = [maxQty integerValue];
        } else {
            self.numberValidator.maxValue = 999;
        }
    }
}

- (void) updateCellForProcessingErrorMessage:(OCGTableViewCell *)cell
{
    // update the cell with the current error message
    if (self.processingErrorMessage != nil) {
        switch (self.processingErrorMessage.codeValue) {
            case MShopperUnitOfMeasureRequiredExceptionCode: {
                // unit of measure required error
                cell.textLabel.text = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠀱󠀳󠁸󠀫󠀸󠁚󠁕󠁚󠀶󠁂󠁸󠁈󠁔󠀯󠁅󠁙󠁣󠁡󠁱󠁰󠁪󠁳󠁸󠁃󠁫󠁯󠁅󠁿*/ @"Measure", nil);
                [OCGKeyboardTypeButtonItem setKeyboardType:kOCGKeyboardTypeNumberPad forTarget:cell.detailTextField];
                cell.detailTextField.placeholder = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠀱󠀳󠁸󠀫󠀸󠁚󠁕󠁚󠀶󠁂󠁸󠁈󠁔󠀯󠁅󠁙󠁣󠁡󠁱󠁰󠁪󠁳󠁸󠁃󠁫󠁯󠁅󠁿*/ @"Measure", nil);
                // add the units
                ServerErrorUnitOfMeasureMessage *unitOfMeasureMessage = (ServerErrorUnitOfMeasureMessage *)self.processingErrorMessage;
                UILabel *displayUnitsLabel = [[UILabel alloc] initWithFrame:CGRectZero];
                displayUnitsLabel.text = unitOfMeasureMessage.displayUnits;
                CGSize displayUnitsLabelSize = [displayUnitsLabel.text sizeWithAttributes:@{ NSFontAttributeName : displayUnitsLabel.font }];
                displayUnitsLabel.frame = CGRectMake(0,
                                                     0,
                                                     displayUnitsLabelSize.width,
                                                     displayUnitsLabelSize.height);
                cell.detailTextField.rightView = displayUnitsLabel;
                cell.detailTextField.rightViewMode = UITextFieldViewModeAlways;
                // set the default
                cell.detailTextField.text = [self.numberValidator stringFromString:@""];
                // select it
                [cell.detailTextField becomeFirstResponder];
                break;
            }
            case MShopperPriceRequiredExceptionCode: {
                // price required error
                cell.textLabel.text = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠀵󠁫󠁸󠁢󠁐󠁱󠀫󠁣󠁏󠁣󠁒󠁚󠁇󠁳󠁐󠁹󠁉󠀸󠀯󠁈󠁥󠁏󠁤󠁺󠁔󠁉󠀰󠁿*/ @"Price", nil);
                [OCGKeyboardTypeButtonItem setKeyboardType:kOCGKeyboardTypeNumberPad forTarget:cell.detailTextField];
                cell.detailTextField.placeholder = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠀵󠁫󠁸󠁢󠁐󠁱󠀫󠁣󠁏󠁣󠁒󠁚󠁇󠁳󠁐󠁹󠁉󠀸󠀯󠁈󠁥󠁏󠁤󠁺󠁔󠁉󠀰󠁿*/ @"Price", nil);
                cell.detailTextField.text = [self.numberValidator stringFromString:@""];
                // select it
                [cell.detailTextField becomeFirstResponder];
                break;
            }
            case MShopperItemQtyRequiredExceptionCode: {
                // quantity required error
                cell.textLabel.text = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁑󠁄󠁏󠁴󠁩󠁔󠁈󠀫󠁌󠁵󠁔󠁆󠁧󠁤󠀸󠁭󠀸󠁱󠀷󠁢󠁓󠁣󠁎󠁡󠁯󠁇󠁙󠁿*/ @"Quantity", nil);
                [OCGKeyboardTypeButtonItem setKeyboardType:kOCGKeyboardTypeNumberPad forTarget:cell.detailTextField];
                cell.detailTextField.placeholder = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁑󠁄󠁏󠁴󠁩󠁔󠁈󠀫󠁌󠁵󠁔󠁆󠁧󠁤󠀸󠁭󠀸󠁱󠀷󠁢󠁓󠁣󠁎󠁡󠁯󠁇󠁙󠁿*/ @"Quantity", nil);
                cell.detailTextField.text = [self.numberValidator stringFromString:@""];
                // select it
                [cell.detailTextField becomeFirstResponder];
                break;
            }
            case MShopperItemSerialNumberRequiredExceptionCode: {
                // serial number required error
                cell.textLabel.text = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠀳󠀰󠀫󠁢󠀲󠀷󠀴󠁊󠁭󠁧󠁪󠀯󠁊󠁓󠁈󠁌󠁐󠁹󠁇󠁪󠁤󠁌󠁚󠁳󠁩󠀰󠁅󠁿*/ @"Serial number", nil);
                cell.detailTextField.keyboardType = UIKeyboardTypeASCIICapable;
                cell.detailTextField.placeholder = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠀳󠀰󠀫󠁢󠀲󠀷󠀴󠁊󠁭󠁧󠁪󠀯󠁊󠁓󠁈󠁌󠁐󠁹󠁇󠁪󠁤󠁌󠁚󠁳󠁩󠀰󠁅󠁿*/ @"Serial number", nil);
                // select it
                [cell.detailTextField becomeFirstResponder];
                break;
            }
            case MShopperItemDescriptionRequiredExceptionCode: {
                // description required error
                cell.textLabel.text = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁶󠁍󠁡󠀰󠁓󠁧󠀹󠁩󠁊󠁏󠀶󠁓󠁸󠁒󠁄󠁉󠀯󠁄󠁯󠀴󠁏󠁏󠁎󠁹󠁹󠁐󠁍󠁿*/ @"Description", nil);
                cell.detailTextField.keyboardType = UIKeyboardTypeASCIICapable;
                cell.detailTextField.placeholder = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁶󠁍󠁡󠀰󠁓󠁧󠀹󠁩󠁊󠁏󠀶󠁓󠁸󠁒󠁄󠁉󠀯󠁄󠁯󠀴󠁏󠁏󠁎󠁹󠁹󠁐󠁍󠁿*/ @"Description", nil);
                // select it
                [cell.detailTextField becomeFirstResponder];
                break;
            }
            case MShopperItemNotFoundExceptionCode: {
                // item not found error
                cell.textLabel.text = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁚󠀳󠁖󠀶󠁙󠁶󠀱󠁂󠁎󠁍󠀸󠁎󠁢󠀴󠁋󠁳󠁚󠁮󠁏󠁦󠁆󠁥󠁶󠁖󠀲󠁉󠁉󠁿*/ @"Item not found", nil);
                cell.detailTextField.userInteractionEnabled = NO;
                cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
                break;
            }
            default:
                break;
        }
    }
    
    [self updateDoneButtonStatus];
    
    // do a little colour fade
    UIColor *original = cell.backgroundColor;
    cell.backgroundColor = [UIColor redColor];
    [UIView animateWithDuration:0.5f
                     animations:^{
                         cell.backgroundColor = original;
                     }];
}

- (void) updateDoneButtonStatus
{
    // get the done button
    self.navigationItem.rightBarButtonItem = self.doneButton;
    // set the title
    [self.doneButton setTitle:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁃󠀲󠁕󠁲󠁤󠀲󠁯󠁮󠁊󠀸󠁘󠁓󠁌󠁕󠁆󠁨󠁲󠁤󠁅󠁣󠁃󠁯󠁙󠁵󠁂󠀹󠁁󠁿*/ @"Done", nil)];
    // get the processing cell
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:[self.tableView numberOfRowsInSection:0] - 1
                                                inSection:0];
    OCGTableViewCell *cell = (OCGTableViewCell *)[self.tableView cellForRowAtIndexPath:indexPath];
    // get the text field
    UITextField *textField = cell.detailTextField;
    
    // set validator and defaults
    self.doneButton.enabled = YES;
    OCGNumberTextFieldValidator *validator = [[OCGNumberTextFieldValidator alloc] init];
    validator.scale = 2;
    validator.maxValue = INT_MAX;
    validator.minValue = 1 / pow(10, validator.scale);
    
    if (self.processingErrorMessage != nil) {
        switch (self.processingErrorMessage.codeValue) {
            case MShopperUnitOfMeasureRequiredExceptionCode: {
                ServerErrorUnitOfMeasureMessage *unitOfMeasureMessage = (ServerErrorUnitOfMeasureMessage *)self.processingErrorMessage;
                validator.scale = [unitOfMeasureMessage.scale integerValue];
                validator.maxValue = [unitOfMeasureMessage.maxValue doubleValue];
                validator.minValue = [unitOfMeasureMessage.minValue doubleValue];
                self.doneButton.enabled = [validator isStringValid:textField.text];
                break;
            }
            case MShopperPriceRequiredExceptionCode: {
                // TFS59537 Allow zero to be entered for price required prompts
                validator.minValue = 0;
                validator.currencyCode = BasketController.sharedInstance.basket.currencyCode;
                self.doneButton.enabled = [validator isStringValid:textField.text];
                break;
            }
            case MShopperItemQtyRequiredExceptionCode: {
                // quantity required error
                validator.scale = 0;
                validator.minValue = 1;
                NSString *maxQty = BasketController.sharedInstance.clientSettings.maxItemQuantity;
                if (maxQty != nil) {
                    validator.maxValue = [maxQty integerValue];
                } else {
                    validator.maxValue = 999;
                }
                self.doneButton.enabled = [validator isStringValid:textField.text];
                break;
            }
            case MShopperItemSerialNumberRequiredExceptionCode: {
                self.doneButton.enabled = ((textField.text.length > 0) && (textField.text.length <= 60));
                break;
            }
            case MShopperItemDescriptionRequiredExceptionCode: {
                // description required error
                self.doneButton.enabled = [textField.text length] >= 1 && [textField.text length] <= 60;
                break;
            }
            case MShopperItemNotFoundExceptionCode: {
                self.doneButton.enabled = NO;
                self.navigationItem.rightBarButtonItem = nil;
                break;
            }
            default:
                break;
        }
    }
}

- (void) cancelAddBasketItem
{
    // must have tapped cancel. Clear out the basket item.
    [self finishProcessingBasketItemCancelled:YES];
}

- (void) finishProcessingBasketItemCancelled:(BOOL)cancelled
{
    [BasketController.sharedInstance kick];

    if (!cancelled) {
        [[BasketController sharedInstance] addItem:self.processingBasketItem completion:NULL];
    } else if (self.cancelBlock != nil) {
        self.cancelBlock();
    }
    self.processingBasketItem = nil;
    self.processingErrorMessage = nil;
}

- (void) recheckForAddBasketItemErrors
{
    [self.processingBasketItem.basketItemCreatedError removeMessage:self.processingErrorMessage];
    
    if (0 == [self.processingBasketItem.basketItemCreatedError.messages count]) {
        // No more error to handle
        self.processingBasketItem.basketItemCreatedError = nil;
    }
    
    [self finishProcessingBasketItemCancelled:NO];
}

- (void) chooseDumpCode:(DumpCode *)dumpCode
{
    if (self.processingBasketItem.originalItemScanId == nil) {
        self.processingBasketItem.originalItemScanId = self.processingBasketItem.itemScanId;
    }
    self.processingBasketItem.itemScanId = dumpCode.itemSellingCode;
    [self setValue:dumpCode.desc forError:self.processingErrorMessage];
    
    // dump code items need a price...
    ServerErrorMessage *message = [[ServerErrorMessage alloc] init];
    message.codeValue = MShopperPriceRequiredExceptionCode;
    [self.processingBasketItem.basketItemCreatedError addMessage:message];
    // and a quantity...
    if (BasketController.sharedInstance.clientSettings.promptQtyForDumpCode) {
        ServerErrorMessage *message = [[ServerErrorMessage alloc] init];
        message.codeValue = MShopperItemQtyRequiredExceptionCode;
        [self.processingBasketItem.basketItemCreatedError addMessage:message];
    }
    // and a description
    if (BasketController.sharedInstance.clientSettings.promptForDumpCodeDescription) {
        ServerErrorMessage *message = [[ServerErrorMessage alloc] init];
        message.codeValue = MShopperItemDescriptionRequiredExceptionCode;
        [self.processingBasketItem.basketItemCreatedError addMessage:message];
    }
    
    [self recheckForAddBasketItemErrors];
}

#pragma mark - UIViewController

- (void) viewDidLoad
{
    [super viewDidLoad];
    self.tableView.tag = kTableSkinningTag;
    
    self.processedErrors = [NSMutableArray array];
    self.processedErrorValues = [NSMutableArray array];
    
    // number validator
    self.numberValidator = [[OCGNumberTextFieldValidator alloc] init];
    
    self.priceValidator = [[OCGNumberTextFieldValidator alloc] init];
    self.priceValidator.maxValue = INT_MAX;
    self.priceValidator.minValue = 0;
    self.priceValidator.currencyCode = BasketController.sharedInstance.currencyCode;
    
    // register cell types
    [self.tableView registerClass:[OCGTableViewCell class]
           forCellReuseIdentifier:kCellIdentifier];
}

- (void) viewWillAppear:(BOOL)animated
{
    self.title = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁄󠁷󠁃󠁮󠁙󠁹󠀳󠁁󠁎󠁫󠁗󠁗󠁕󠁫󠁧󠁒󠀷󠁭󠁴󠀹󠁦󠁍󠁲󠁇󠁔󠀸󠁉󠁿*/ @"Add Item", nil);

    // add the navbar buttons
    self.navigationItem.leftBarButtonItem =
    [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁩󠁢󠁺󠁬󠁏󠁶󠁣󠁧󠁫󠁍󠁋󠁧󠁫󠀱󠁶󠁐󠁐󠀹󠁘󠀴󠁘󠁡󠁕󠁭󠁹󠁷󠀰󠁿*/ @"Cancel", nil)
                                     style:UIBarButtonItemStylePlain
                                    target:self
                                    action:@selector(cancelTapped:)];
    self.doneButton =
    [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁃󠀲󠁕󠁲󠁤󠀲󠁯󠁮󠁊󠀸󠁘󠁓󠁌󠁕󠁆󠁨󠁲󠁤󠁅󠁣󠁃󠁯󠁙󠁵󠁂󠀹󠁁󠁿*/ @"Done", nil)
                                     style:UIBarButtonItemStyleDone
                                    target:self
                                    action:@selector(doneTapped:)];
    [[CRSSkinning currentSkin] applyViewSkin:self];
    [self updateDoneButtonStatus];
    [super viewWillAppear:animated];
}

#pragma mark - UITableViewDataSource

- (NSInteger) numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSInteger result = 0;
    
    if (self.processedErrors != nil) {
        result += self.processedErrors.count;
    }
    if (self.processingErrorMessage != nil) {
        result++;
    }
    
    return result;
}

- (UITableViewCell *) tableView:(UITableView *)tableView
          cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kCellIdentifier
                                                            forIndexPath:indexPath];
    
    return cell;
}

- (void) tableView:(UITableView *)tableView willDisplayCell:(OCGTableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    cell.accessoryType = UITableViewCellAccessoryNone;
    cell.textLabel.text = nil;
    cell.detailTextField.userInteractionEnabled = YES;
    cell.detailTextField.text = nil;
    cell.detailTextField.rightView = nil;
    cell.detailTextField.rightViewMode = UITextFieldViewModeNever;
    cell.detailTextField.autocapitalizationType = UITextAutocapitalizationTypeNone;
    cell.detailTextField.autocorrectionType = UITextAutocorrectionTypeNo;
    cell.detailTextField.spellCheckingType = UITextSpellCheckingTypeNo;
    
    // if this is the last row
    if ((indexPath.row == [tableView numberOfRowsInSection:0] - 1) &&
        (self.processingErrorMessage != nil)) {
        // show the processing error
        cell.detailTextField.delegate = self;
        cell.detailTextField.clearButtonMode = UITextFieldViewModeAlways;
        cell.selectionStyle = UITableViewCellSelectionStyleDefault;
        [self updateCellForProcessingErrorMessage:cell];
    } else {
        // show the processed error
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.detailTextField.text = self.processedErrorValues[indexPath.row];
        cell.detailTextField.userInteractionEnabled = NO;
        cell.detailTextField.delegate = nil;
        cell.detailTextField.clearButtonMode = UITextFieldViewModeNever;
        NSNumber *processedErrorCode = self.processedErrors[indexPath.row];
        switch (processedErrorCode.integerValue) {
            case MShopperUnitOfMeasureRequiredExceptionCode: {
                // unit of measure required error
                cell.textLabel.text = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠀱󠀳󠁸󠀫󠀸󠁚󠁕󠁚󠀶󠁂󠁸󠁈󠁔󠀯󠁅󠁙󠁣󠁡󠁱󠁰󠁪󠁳󠁸󠁃󠁫󠁯󠁅󠁿*/ @"Measure", nil);
                // add the units
                ServerErrorUnitOfMeasureMessage *unitOfMeasureMessage = (ServerErrorUnitOfMeasureMessage *)self.processingErrorMessage;
                UILabel *displayUnitsLabel = [[UILabel alloc] initWithFrame:CGRectZero];
                displayUnitsLabel.text = unitOfMeasureMessage.displayUnits;
                CGSize displayUnitsLabelSize = [displayUnitsLabel.text sizeWithAttributes:@{ NSFontAttributeName : displayUnitsLabel.font }];
                displayUnitsLabel.frame = CGRectMake(0,
                                                     0,
                                                     displayUnitsLabelSize.width,
                                                     displayUnitsLabelSize.height);
                cell.detailTextField.rightView = displayUnitsLabel;
                cell.detailTextField.rightViewMode = UITextFieldViewModeAlways;
                break;
            }
            case MShopperPriceRequiredExceptionCode: {
                // price required error
                cell.textLabel.text = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠀵󠁫󠁸󠁢󠁐󠁱󠀫󠁣󠁏󠁣󠁒󠁚󠁇󠁳󠁐󠁹󠁉󠀸󠀯󠁈󠁥󠁏󠁤󠁺󠁔󠁉󠀰󠁿*/ @"Price", nil);
                break;
            }
            case MShopperItemQtyRequiredExceptionCode: {
                // quantity required error
                cell.textLabel.text = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁑󠁄󠁏󠁴󠁩󠁔󠁈󠀫󠁌󠁵󠁔󠁆󠁧󠁤󠀸󠁭󠀸󠁱󠀷󠁢󠁓󠁣󠁎󠁡󠁯󠁇󠁙󠁿*/ @"Quantity", nil);
                break;
            }
            case MShopperItemSerialNumberRequiredExceptionCode: {
                // serial number required error
                cell.textLabel.text = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠀳󠀰󠀫󠁢󠀲󠀷󠀴󠁊󠁭󠁧󠁪󠀯󠁊󠁓󠁈󠁌󠁐󠁹󠁇󠁪󠁤󠁌󠁚󠁳󠁩󠀰󠁅󠁿*/ @"Serial number", nil);
                break;
            }
            case MShopperItemDescriptionRequiredExceptionCode: {
                // description required error
                cell.textLabel.text = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁶󠁍󠁡󠀰󠁓󠁧󠀹󠁩󠁊󠁏󠀶󠁓󠁸󠁒󠁄󠁉󠀯󠁄󠁯󠀴󠁏󠁏󠁎󠁹󠁹󠁐󠁍󠁿*/ @"Description", nil);
                break;
            }
            case MShopperItemNotFoundExceptionCode: {
                // item not found error
                cell.textLabel.text = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁕󠀯󠁹󠁬󠁈󠀹󠁮󠁹󠁑󠁐󠁨󠁃󠀷󠁋󠁤󠁩󠀱󠁃󠁨󠁯󠁥󠁅󠁄󠁕󠁄󠁔󠁳󠁿*/ @"Dump code", nil);
                break;
            }
            default:
                break;
        }
    }
    // skin
    cell.tag = kTableCellSkinningTag;
    cell.textLabel.tag = kTableCellKeySkinningTag;
    cell.detailTextField.tag = kTableCellValueSkinningTag;
    [[CRSSkinning currentSkin] applyCellSkin:cell
                                    forStyle:tableView.style];
}

- (NSString *) tableView:(UITableView *)tableView titleForFooterInSection:(NSInteger)section
{
    NSString *result = nil;
    
    if (self.processingErrorMessage != nil) {
        switch (self.processingErrorMessage.codeValue) {
            case MShopperUnitOfMeasureRequiredExceptionCode: {
                result = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠀸󠁗󠁈󠁉󠁚󠁚󠁕󠀫󠁈󠀯󠁊󠁡󠁡󠁕󠁅󠁵󠁚󠁭󠀯󠁶󠁘󠁄󠀸󠁄󠁙󠁇󠁳󠁿*/ @"Please specify a measure for this item", nil);
                break;
            }
            case MShopperPriceRequiredExceptionCode: {
                result = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁏󠁭󠁡󠁅󠁴󠀰󠀷󠁒󠁲󠁁󠁺󠁈󠁰󠁄󠁚󠀷󠁮󠁫󠁺󠁺󠀶󠀸󠀰󠁋󠁴󠁤󠁣󠁿*/ @"Please specify a price for this item", nil);
                break;
            }
            case MShopperItemQtyRequiredExceptionCode: {
                result = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁊󠁇󠁎󠁋󠁶󠁤󠁌󠁓󠁅󠁸󠁮󠁢󠁲󠁭󠀹󠁚󠁈󠁏󠁣󠁚󠀫󠀲󠁐󠁧󠁸󠁬󠁑󠁿*/ @"Please specify a quantity for this item", nil);
                break;
            }
            case MShopperItemSerialNumberRequiredExceptionCode: {
                result = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁧󠁫󠀰󠁃󠁣󠁪󠀲󠁙󠁨󠁹󠁁󠁗󠁦󠀴󠁥󠁁󠁋󠁫󠁹󠁅󠁤󠁗󠁐󠁪󠁗󠁦󠁉󠁿*/ @"Please specify a serial number for this item", nil);
                break;
            }
            case MShopperItemDescriptionRequiredExceptionCode: {
                result = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁨󠀳󠀰󠀰󠁬󠀵󠀰󠁩󠁗󠁏󠁶󠁈󠁖󠁑󠁓󠁦󠁊󠀲󠁦󠁲󠁫󠁵󠁅󠁙󠁘󠁍󠁑󠁿*/ @"Please specify a description for this item", nil);
                break;
            }
            case MShopperItemNotFoundExceptionCode: {
                result = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁺󠁸󠁇󠁗󠁙󠁱󠁋󠁋󠀷󠁯󠁷󠀴󠁰󠁔󠁋󠁷󠁴󠁈󠀰󠁱󠁊󠁈󠁮󠁦󠀱󠁊󠁳󠁿*/ @"Please select a dump code for this item", nil);
                break;
            }
        }
    }
    
    return result;
}

#pragma mark - UITableViewDelegate

- (NSIndexPath *) tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSIndexPath *result = nil;
    
    if ((indexPath.row == [tableView numberOfRowsInSection:0] - 1) &&
        (self.processingErrorMessage != nil)) {
        result = indexPath;
    }
    
    return result;
}

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // if this is the processing row
    if ((indexPath.row == [tableView numberOfRowsInSection:0] - 1) &&
        (self.processingErrorMessage != nil)) {
        if (self.processingErrorMessage.codeValue != MShopperItemNotFoundExceptionCode) {
            OCGTableViewCell *cell = (OCGTableViewCell *)[tableView cellForRowAtIndexPath:indexPath];
            
        } else {
            // navigate to the dump code selection view
            OCGSelectViewController *selectViewController = [[OCGSelectViewController alloc] init];
            selectViewController.options = @[BasketController.sharedInstance.dumpCodes];
            selectViewController.titleForOption = ^(OCGSelectViewController *selectViewController, NSIndexPath *indexPath) {
                DumpCode *dumpCode = selectViewController.options[indexPath.section][indexPath.row];
                return dumpCode.desc;
            };
            selectViewController.didSelectOption = ^(OCGSelectViewController *selectViewController, NSIndexPath *indexPath) {
                if (indexPath) {
                    DumpCode *dumpCode = selectViewController.options[indexPath.section][indexPath.row];
                    [self chooseDumpCode:dumpCode];
                } else {
                    [self cancelAddBasketItem];
                }
                [selectViewController.navigationController popViewControllerAnimated:YES];
            };
            selectViewController.title = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁚󠀳󠁖󠀶󠁙󠁶󠀱󠁂󠁎󠁍󠀸󠁎󠁢󠀴󠁋󠁳󠁚󠁮󠁏󠁦󠁆󠁥󠁶󠁖󠀲󠁉󠁉󠁿*/ @"Item not found", nil);
            [self.navigationController pushViewController:selectViewController
                                                 animated:YES];
        }
    }
    [tableView deselectRowAtIndexPath:indexPath
                             animated:NO];
}

#pragma mark - UITextFieldDelegate

- (BOOL) textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    BOOL result = YES;
    
    if (self.processingErrorMessage.codeValue == MShopperItemDescriptionRequiredExceptionCode) {
        NSString *text = [textField.text stringByReplacingCharactersInRange:range withString:string];
        
        /*
         TFS59026 The POS has a limit of 20 (?) characters for this field.
         */
        if ([text length] > 20) {
            result = NO;
        } else {
            NSMutableCharacterSet *acceptedCharacterSet = [NSMutableCharacterSet alphanumericCharacterSet];
            [acceptedCharacterSet addCharactersInString:@" "];
            result = ([string rangeOfCharacterFromSet:[acceptedCharacterSet invertedSet]
                                              options:NSCaseInsensitiveSearch].location == NSNotFound);
        }
    } else if (self.processingErrorMessage.codeValue == MShopperItemSerialNumberRequiredExceptionCode) {
        NSString *text = [textField.text stringByReplacingCharactersInRange:range withString:string];
        
        if ([text length] > 60) {
            result = NO;
        } else {
            NSMutableCharacterSet *acceptedCharacterSet = [NSMutableCharacterSet alphanumericCharacterSet];
            result = ([string rangeOfCharacterFromSet:[acceptedCharacterSet invertedSet]
                                              options:NSCaseInsensitiveSearch].location == NSNotFound);
        }
    } else {
        // validate entry
        result = [self.numberValidator textField:textField
                   shouldChangeCharactersInRange:range
                               replacementString:string];
    }
    
    [self updateDoneButtonStatus];
    
    return result;
}

- (BOOL) textFieldShouldReturn:(UITextField *)textField
{
    // do done on return
    [textField resignFirstResponder];
    [self doneTapped:self];
    return NO;
}

#pragma mark - Actions

- (IBAction) cancelTapped:(id)sender
{
    [self cancelAddBasketItem];
    if (self.cancelBlock != nil) {
        self.cancelBlock();
    }
}

- (IBAction) doneTapped:(id)sender
{
    // if we are processing an error
    if (self.processingBasketItem != nil) {
        // get the processing cell
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:[self.tableView numberOfRowsInSection:0] - 1
                                                    inSection:0];
        OCGTableViewCell *cell = (OCGTableViewCell *)[self.tableView cellForRowAtIndexPath:indexPath];
        // get the text field
        UITextField *textView = cell.detailTextField;
        // process user entry
        switch (self.processingErrorMessage.codeValue) {
            case MShopperPriceRequiredExceptionCode: {
                NSNumber *number = [self.priceValidator numberFromString:textView.text];
                self.processingBasketItem.netAmount = [number description];
                break;
            }
            case MShopperUnitOfMeasureRequiredExceptionCode: {
                self.processingBasketItem.measure = textView.text;
                break;
            }
            case MShopperItemQtyRequiredExceptionCode: {
                self.processingBasketItem.quantity = textView.text;
                break;
            }
            case MShopperItemSerialNumberRequiredExceptionCode: {
                if (textView.text.length) {
                    self.processingBasketItem.itemSerialNumber = textView.text;
                } else {
                    self.processingBasketItem.itemSerialNumber = nil;
                }
                break;
            }
            case MShopperItemDescriptionRequiredExceptionCode: {
                self.processingBasketItem.originalItemDescription = textView.text;
                break;
            }
            default:
                NSAssert(NO, @"Unknown required parameters: %@", self.processingErrorMessage);
                break;
        }
        
        // add this value to the processed arrays
        [self setValue:textView.text forError:self.processingErrorMessage];
        
        [self recheckForAddBasketItemErrors];
    }
}

#pragma mark - Methods

- (BOOL) handleAddBasketItemError:(ServerError*)error
{
    BOOL result = YES;
    
    if (self.processingBasketItem == nil) {
        // get a reference to the basket item
        self.processingBasketItem = error.createdBasketItem;
        
        // MPOS-228 check if this is a 'price required' error
        self.processingErrorMessage = nil;
        BOOL isBasketDead = NO;
        BOOL isLoggedOut = NO;
        BOOL isDeviceNotFound = NO;
        
        // get the error message (just the first one)
        for (ServerErrorMessage *message in error.messages) {
            if (message.codeValue == MShopperItemNotFoundExceptionCode) {
                if ([BasketController.sharedInstance.dumpCodes count] > 0) {
                    self.processingErrorMessage = message;
                    // TFS71004 - if the item not found error was thrown for a dump code item
                    // show an alert and cancel basket add
                    if ((message.codeValue == MShopperItemNotFoundExceptionCode) &&
                        ([BasketController.sharedInstance isDumpCodeItem:self.processingBasketItem.itemScanId])) {
                        UIAlertView *dumpCodeAlert =
                        [[UIAlertView alloc] initWithTitle:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁦󠁹󠀴󠀷󠁔󠁲󠁚󠁗󠁚󠁪󠀵󠀱󠁅󠁑󠁫󠀵󠁹󠁡󠀷󠁩󠁇󠀳󠁆󠁖󠁃󠁍󠁁󠁿*/ @"Invalid Dump Code", nil)
                                                   message:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁷󠁪󠁰󠁴󠁯󠁪󠁺󠁎󠁴󠀶󠁱󠁺󠁮󠀵󠁃󠀷󠀳󠀱󠁩󠁎󠀵󠁎󠀫󠁇󠁈󠁺󠁉󠁿*/ @"The dump code item specified does not exist.", nil)
                                                  delegate:[UIAlertView class]
                                         cancelButtonTitle:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁄󠁴󠁍󠁉󠁣󠀫󠁺󠁔󠁗󠁦󠁹󠁡󠁯󠀷󠁨󠁔󠁩󠀸󠁯󠀫󠁨󠁘󠀹󠁱󠁌󠀶󠀸󠁿*/ @"OK", nil)
                                         otherButtonTitles:nil];
                        [dumpCodeAlert show];
                    }
                    break;
                }
            } else if (message.codeValue == MShopperInvalidBasketStatusExceptionCode) {
                isBasketDead = YES;
            } else if (message.codeValue == MShopperSaleBasketNotFoundExceptionCode) {
                isBasketDead = YES;
            } else if (message.codeValue == MShopperOperatorNotSignedOnExceptionCode) {
                isLoggedOut = YES;
            } else if (message.codeValue == MShopperDeviceNotFoundExceptionCode) {
                isDeviceNotFound = YES;
            } else if ((message.codeValue == MShopperPriceRequiredExceptionCode) ||
                       (message.codeValue == MShopperItemNotFoundExceptionCode)) {
                self.processingErrorMessage = message;
                if (![[BasketController sharedInstance] doesUserHavePermissionTo:POSPermissionAddItemWithPrice]) {
                    [POSFuncController presentPermissionAlert:POSPermissionAddItemWithPrice
                                                    withBlock:^{
                                                        [self cancelAddBasketItem];
                                                    }];
                }
            } else if ((message.codeValue == MShopperItemAgeVerificationRequiredExceptionCode) &&
                       ([message isKindOfClass:[ServerErrorAgeRestrictionMessage class]])) {
                self.processingErrorMessage = message;
                // age restriction error, show the age verification alert
                ServerErrorAgeRestrictionMessage *ageMessage = (ServerErrorAgeRestrictionMessage *)self.processingErrorMessage;
                NSString *messageText = [NSString stringWithFormat:@"%@ %@ %@",
                                         NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁯󠁊󠁙󠁊󠁭󠁇󠁓󠀲󠁖󠁮󠁆󠁅󠀴󠁎󠀸󠀳󠀯󠁴󠁙󠁱󠀹󠁂󠁧󠁏󠁏󠁃󠁑󠁿*/ @"Please verify that the purchaser is at least", nil),
                                         ageMessage.minimumAge,
                                         NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠀶󠁆󠁖󠀲󠁒󠁁󠁪󠁢󠁋󠁓󠁴󠁐󠀰󠁴󠁥󠁤󠁦󠁥󠁴󠁎󠁙󠁕󠁸󠁣󠁨󠁋󠁧󠁿*/ @"years of age", nil)];
                UIAlertView *alert =
                [[UIAlertView alloc] initWithTitle:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠀰󠁰󠁶󠁯󠁳󠁮󠁊󠁨󠁙󠁡󠁸󠁳󠁬󠁘󠁯󠀰󠀸󠀶󠁱󠁚󠁈󠁅󠁪󠁷󠁏󠁐󠁫󠁿*/ @"Age Restricted Item", nil)
                                           message:messageText
                                          delegate:[UIAlertView class]
                                 cancelButtonTitle:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁩󠁢󠁺󠁬󠁏󠁶󠁣󠁧󠁫󠁍󠁋󠁧󠁫󠀱󠁶󠁐󠁐󠀹󠁘󠀴󠁘󠁡󠁕󠁭󠁹󠁷󠀰󠁿*/ @"Cancel", nil)
                                 otherButtonTitles:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁄󠁓󠁍󠁊󠁤󠁨󠀲󠁆󠀯󠁴󠀷󠁚󠁫󠁂󠁺󠁁󠁄󠀫󠁊󠁘󠀲󠁰󠁄󠁈󠁹󠁃󠁙󠁿*/ @"Age Verified", nil), nil];
                alert.dismissBlock = ^(int buttonIndex) {
                    if (buttonIndex == 0) {
                        // just set the age restriction to the minimum age
                        self.processingBasketItem.ageRestriction = ageMessage.minimumAge;
                        [self recheckForAddBasketItemErrors];
                    }
                };
                alert.cancelBlock = ^() {
                    [self cancelTapped:nil];
                };
                dispatch_async(dispatch_get_main_queue(), ^{
                    [alert show];
                });
            } else {
                self.processingErrorMessage = message;
            }
        }
        
        BasketController.sharedInstance.isReturnItem = [self.processingBasketItem isKindOfClass:[MPOSReturnItem class]];
        
        // if we aren't handling this error
        if (((BasketController.sharedInstance.dumpCodes.count == 0) &&
             (self.processingErrorMessage.codeValue == MShopperItemNotFoundExceptionCode)) ||
            ((self.processingErrorMessage.codeValue != MShopperItemNotFoundExceptionCode) &&
            (self.processingErrorMessage.codeValue != MShopperUnitOfMeasureRequiredExceptionCode) &&
            (self.processingErrorMessage.codeValue != MShopperPriceRequiredExceptionCode) &&
            (self.processingErrorMessage.codeValue != MShopperItemQtyRequiredExceptionCode) &&
            (self.processingErrorMessage.codeValue != MShopperItemSerialNumberRequiredExceptionCode) &&
            (self.processingErrorMessage.codeValue != MShopperItemDescriptionRequiredExceptionCode) &&
            (self.processingErrorMessage.codeValue != MShopperItemAgeVerificationRequiredExceptionCode))) {
            result = NO;
            // show the default error alert view
            ServerErrorHandler *serverErrorHandler = [ServerErrorHandler sharedInstance];
            [serverErrorHandler handleServerError:error
                                   transportError:error.transportError
                                      withContext:error.context
                                     dismissBlock: ^{
                                         if (isDeviceNotFound) {
                                             // jjrscott self.requiredParameters = RequiredParameterBarcode;
                                             [[BasketController sharedInstance] deviceNotFound];
                                         } else if (isLoggedOut) {
                                             // jjrscott self.requiredParameters = RequiredParameterBarcode;
                                             [[BasketController sharedInstance] hasLoggedOut];
                                         }
                                    
                                         // delete the add item line
                                         [self cancelAddBasketItem];
                                      
                                         if (isBasketDead) {
                                             [[BasketController sharedInstance] resetBasketToServer];
                                         }
                                     }
                                      repeatBlock:nil];
        } else {
            // check to see if the last processed error actually failed
            if (self.processedErrors.count) {
                NSNumber *lastErrorCode = [self.processedErrors lastObject];
                if (lastErrorCode.integerValue == self.processingErrorMessage.codeValue) {
                    [self.processedErrors removeLastObject];
                    [self.processedErrorValues removeLastObject];
                }
            }
            
            // update the number validator
            [self updateValidator];
            
            // we have an error, insert a new row into the table
            [self.tableView reloadData];
        }
    }
    
    return result;
}

@end