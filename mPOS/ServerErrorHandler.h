//
//  MPOSEditableTableFieldsDataSource.h
//  mPOS
//
//  Created by Antonio Strijdom on 24/10/2012.
//  Copyright (c) 2012 Clarity. All rights reserved.
//

/** @file MPOS */

@class ServiceClient;
@class ServerError;
@class ServerError;
@class ServerErrorMessage;

/** @brief MPOS specific data source class that is used for a editable table view.
 *  @discussion The inherited class should override the method createTableFields: to initialize
 *  the list of fields the editable table view should show.
 */
@interface ServerErrorHandler : NSObject

+ (void) setSharedInstance:(ServerErrorHandler *) sharedInstance;
+ (ServerErrorHandler *) sharedInstance;

/** @brief Gets the presented view controller (long story).
 *  @return The top most view controller
 */
+ (void) topMostControllerComplete:(void (^)(UIViewController *viewController))complete;

/** @brief Translates the specified error message that has been received from the web service.
 *  @return The localized error message has should be displayed in the user interface.
 */
- (NSString *) translateServerErrorMessage:(ServerErrorMessage *)message withContext:(NSString *)context;

/** @brief Builds a human readable error message from a server error.
 *  @param context The context of the original call (or nil)
 *  @param serverError The error from the server
 *  @param transportError Any transport errors returned during a service communication.
 *  @return The error string.
 */
- (NSString *) buildServerErrorMessage:(ServerError *)serverError
                        transportError:(NSError *)error
                           withContext:(NSString *)context;

/**
 *  @brief Handles the errors that took origin by a service communication.
 *  @param serverError The marshalled HTTP body error message received from the web service.
 *  @param dismissBlock The block that is executed when the user dismisses the alert.
 *  @param repeatBlock The block that is executed when the user chooses to retry the operation.
 */
//- (void) handleServerError:(ServerError *)mposError
//              dismissBlock:(void (^)())dismissedBlock
//               repeatBlock:(void (^)())repeatBlock;

/**
 *  @brief Handles the errors that took origin by a service communication.
 *  @param serverError The marshalled HTTP body error message received from the web service.
 *  @param transportError Any transport errors returned during a service communication.
 *  @param context The context of the original call (or nil)
 *  @param dismissBlock The block that is executed when the user dismisses the alert.
 *  @param repeatBlock The block that is executed when the user chooses to retry the operation.
 */

- (void) handleServerError:(ServerError *)serverError
            transportError:(NSError*)transportError
               withContext:(NSString *)context
              dismissBlock:(void (^)())dismissedBlock
               repeatBlock:(void (^)())repeatBlock;

@end
