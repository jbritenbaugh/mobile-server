//
//  FlightDetailsItemViewController.h
//  mPOS
//
//  Created by John Scott on 05/02/2014.
//  Copyright (c) 2015 Clarity. All rights reserved.
//

typedef NS_ENUM(NSInteger, FNKItemType)
{
    FNKUknownItemType = -1,
    FNKEditableStringItemType,
    
};

typedef NS_ENUM(NSInteger, FlightDetailsItemDetail)
{
    FlightDetailsItemDetailAirlineCode,
    FlightDetailsItemDetailBoardingPassBarcode,
    FlightDetailsItemDetailDestinationAirportCode,
    FlightDetailsItemDetailFlightNumber,
    FlightDetailsItemDetailManualEntry,
    FlightDetailsItemDetailNationality,
    FlightDetailsItemDetailNotFlying,
    FlightDetailsItemDetailOriginAirportCode
};

typedef NS_ENUM(NSInteger, FlightDetailsItemViewControllerType)
{
    FlightDetailsItemViewControllerTypeBoardingPassBarcode,
    FlightDetailsItemViewControllerTypeManualEntry,
    FlightDetailsItemViewControllerTypeNotFlying,
};

#import <UIKit/UIKit.h>

@interface FlightDetailsItemViewController : UICollectionViewController

@property (assign, nonatomic) FlightDetailsItemViewControllerType type;
@property (strong, nonatomic) NSDictionary *initialItemDetails;

@property (copy) void (^sucessfulReturn)();

@end
