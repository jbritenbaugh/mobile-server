//
//  OCGEFTProviderAdyen.h
//  mPOS
//
//  Created by Antonio Strijdom on 21/01/2015.
//  Copyright (c) 2015 Clarity. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AdyenToolkit/AdyenToolkit.h>
#import "OCGEFTManager.h"

extern NSString * const kOCGEFTProviderAdyenDeviceConfigKey;

@interface OCGEFTProviderAdyen : NSObject <OCGEFTProviderProtocol>

/** @brief Utility method for getting the key paths for the properties of ADYTransactionData.
 *  @return The keys for ADYTransactionData.
 *  @note This should be kept in sync with ADYTransactionData.
 *  @note Only keys that are JSONSerialisable are returned. Additional keys can be added manually.
 */
+ (NSArray *) transactionKeys;

@end
