//
//  OCGEFTAdyenDeviceSelectionRDMDatasource.h
//  mPOS
//
//  Created by Antonio Strijdom on 16/03/2015.
//  Copyright (c) 2015 Clarity. All rights reserved.
//

#import "OCGEFTDeviceSelectionRDMDatasource.h"
#import <AdyenToolkit/AdyenToolkit.h>

@interface OCGEFTAdyenDeviceSelectionRDMDatasource : OCGEFTDeviceSelectionRDMDatasource

- (void) deviceForBarcode:(NSString *)barcode
                 complete:(void (^)(ADYDevice *adyenDevice))complete;

@end
