//
//  OCGEFTProviderFakeIntegrated.m
//  mPOS
//
//  Created by Antonio Strijdom on 04/02/2015.
//  Copyright (c) 2015 Clarity. All rights reserved.
//

#import "OCGEFTProviderFakeIntegrated.h"
#import "OCGEFTProviderAdyen.h"
#import "FakeAdyenViewController.h"
#import <objc/runtime.h>
#import "ServerErrorHandler.h"

@interface OCGEFTProviderFakeIntegrated ()
@property (nonatomic, strong) NSError *lastError;
@property (nonatomic, strong) OCGEFTManager *manager;
@property (nonatomic, strong) FakeAdyenTransitioningDelegate *transitioningDelegate;
- (void) presentFakeAdyenControllerWithStep:(FakeAdyenViewControllerStep)step
                                 andOKBlock:(dispatch_block_t)okBlock
                             andCancelBlock:(dispatch_block_t)cancelBlock
                              andErrorBlock:(dispatch_block_t)errorBlock;
- (NSString *) localizedDescriptionForErrorCode:(ADYErrorCode)errorCode;
- (ADYTransactionData *) transactionWithTransactionType:(ADYTransactionType)transactionType
                                                 tender:(Tender *)tender
                                                 amount:(NSNumber *)amount
                                        forBasketWithID:(NSString *)basketId
                                      withCustomerEmail:(NSString *)customerEmail;
@end

@implementation OCGEFTProviderFakeIntegrated

#pragma mark - Private

- (void) presentFakeAdyenControllerWithStep:(FakeAdyenViewControllerStep)step
                                 andOKBlock:(dispatch_block_t)okBlock
                             andCancelBlock:(dispatch_block_t)cancelBlock
                              andErrorBlock:(dispatch_block_t)errorBlock
{
    FakeAdyenViewController *fakeAdyenViewController = [[FakeAdyenViewController alloc] init];
    fakeAdyenViewController.step = step;
    fakeAdyenViewController.modalPresentationStyle = UIModalPresentationCustom;
    fakeAdyenViewController.transitioningDelegate = self.transitioningDelegate;
    fakeAdyenViewController.okBlock = okBlock;
    fakeAdyenViewController.cancelBlock = cancelBlock;
    fakeAdyenViewController.errorBlock = errorBlock;
    [ServerErrorHandler topMostControllerComplete:^(UIViewController *viewController) {
        [viewController presentViewController:fakeAdyenViewController
                                     animated:YES
                                   completion:nil];
    }];
}

- (NSString *) localizedDescriptionForErrorCode:(ADYErrorCode)errorCode
{
    NSString *result = nil;
    
    switch (errorCode) {
        case ADYErrorCodeUnknownError:
            result = @"Unknown error";
            break;
        case ADYErrorCodeNoDeviceConnected:
            result = @"No device connected";
            break;
        case ADYErrorCodeUnauthorizedForDevice:
            result = @"Unauthorised device";
            break;
        case ADYErrorCodeDeviceError:
            result = @"Device error";
            break;
        case ADYErrorCodeInvalidCredentialsForPaymentDevice:
            result = @"Invalid credentials for device";
            break;
        case ADYErrorCodeAmountTooLow:
            result = @"Amount too low";
            break;
        case ADYErrorCodeTransactionInitializationError:
            result = @"Transaction initialisation error";
            break;
        case ADYErrorCodeTransactionRequiresLocationServices:
            result = @"Location services are required, but not available";
            break;
        case ADYErrorCodeInvalidCredentialsForPSP:
            result = @"Invalid merchant credentials";
            break;
        case ADYErrorCodePaymentDeviceConnectionError:
            result = @"Device connection error";
            break;
        case ADYErrorCodeNetworkError:
            result = @"Network error";
            break;
        case ADYErrorCodeNoInternetConnection:
            result = @"No internet connection available";
            break;
        case ADYErrorCodeInternalServerError:
            result = @"Internal server error";
            break;
        case ADYErrorCodeAppRegistrationFailed:
            result = @"Registration failed";
            break;
        case ADYErrorCodeInvalidApp:
            result = @"Registration required";
            break;
        case ADYErrorCodeUserPermissionError:
            result = @"Invalid permissions to perform operation";
        case ADYErrorCodeInvalidMerchant:
            result = @"Invalid merchant account";
            break;
        case ADYErrorCodeAlreadyLoggedIn:
            result = @"Already logged in";
            break;
        case ADYErrorCodeNotLoggedIn:
            result = @"Not logged in";
            break;
        case ADYErrorCodeAppRegistrationTokenCheckTimeout:
            result = @"Registration token check timed out";
            break;
        case ADYErrorCodeNotSupported:
            result = @"Transaction not supported";
            break;
        case ADYErrorCodeDeviceEMVError:
            result = @"Device EMV error";
            break;
        case ADYErrorCodeDeviceSAPIError:
            result = @"Device SAPI error";
            break;
        case ADYErrorCodeDevicePrintReceiptFailed:
            result = @"Receipt printing failed";
            break;
        case ADYErrorCodePSPValidateError:
            result = @"PSP Validation error";
            break;
        default:
            result = @"Undefined error";
            break;
    }
    
    return result;
}

- (ADYTransactionData *) transactionWithTransactionType:(ADYTransactionType)transactionType
                                                 tender:(Tender *)tender
                                                 amount:(NSNumber *)amount
                                        forBasketWithID:(NSString *)basketId
                                      withCustomerEmail:(NSString *)customerEmail
{
    MPOSBasket *basket = [[BasketController sharedInstance] basket];
    ADYTransactionData *transaction = [[ADYTransactionData alloc] init];
    transaction.mid = @"1000";
    transaction.tid = basket.tillNumber;
    transaction.posEntryMode = @"CHIP";
    transaction.txRef = basket.basketID;
    transaction.tenderReference = @"535I001110016037000";
    transaction.shopperReference = nil;
    transaction.shopperEmail = customerEmail;
    transaction.transactionType = transactionType;
    transaction.amountValue = amount;
    transaction.amountCurrency = tender.currencyCode;
    transaction.adjustedAmountValue = amount;
    transaction.adjustedAmountCurrency = tender.currencyCode;
    transaction.gratuityAmountValue = @(0.00f);
    transaction.gratuityAmountCurrency = tender.currencyCode;
    transaction.additionalAmountValue = @(0.00f);
    transaction.additionalAmountCurrency = tender.currencyCode;
    transaction.authAmountValue = amount;
    transaction.authAmountCurrency = tender.currencyCode;
    transaction.aidCode = @"A000000004306001";
    transaction.applicationPreferredName = @"mPOS";
    transaction.merchantReference = self.manager.currentPayment.uniqueMerchantReference;
    self.manager.currentPayment.transaction = @"OK";
    transaction.taxPercentage = @(0.00f);
    transaction.cardType = @"VISA";
    transaction.cardNumber = @"5301********9004";
    transaction.cardSeq = @"";
    transaction.cardHolderName = @"BMS TEST CARD";
    transaction.cardExpiryYear = @"08";
    transaction.cardExpiryMonth = @"03";
    transaction.cardEffectiveYear = @"02";
    transaction.cardEffectiveMonth = @"04";
    transaction.paymentVerificationData = nil;
    transaction.issuerCountry = @"UK";
    transaction.cardBin = @"679999";
    transaction.applicationLabel = @"VISA";
    transaction.cardHolderVerificationMethodResults = @"420900";
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.dateFormat = @"dd-MM-YYYY";
    transaction.txDate = [formatter stringFromDate:[NSDate date]];
    formatter.dateFormat = @"HH:mm:ss";
    transaction.txTime = [formatter stringFromDate:[NSDate date]];
    transaction.cardScheme = ADYCardSchemeVisaVisa;
    transaction.finalState = ADYFinalTenderStateApproved;
    transaction.finalStateString = @"APPROVED";
    transaction.processingState = ADYProcessingStateProcessed;
    transaction.pspAuthCode = @"123456";
    transaction.pspReference = @"7914255488899015";
    transaction.dateSubmitted = [NSDate date];
    transaction.terminalId = @"VX680-321187985";
    transaction.signatureData = nil;
    transaction.location = nil;
    transaction.logoReceipt = nil;
    transaction.deviceData = nil;
    
    return transaction;
}

#pragma mark - OCGEFTProviderProtocol

- (instancetype) initForManager:(OCGEFTManager *)manager
{
    self = [super init];
    if (self) {
        self.manager = manager;
        self.transitioningDelegate = [[FakeAdyenTransitioningDelegate alloc] init];
    }
    
    return self;
}

- (BOOL) isURLProvider
{
    return NO;
}

- (ServerError *) getLastServerError
{
    ServerError *error = nil;
    
    // if we have an error
    if (self.lastError) {
        // and it is an Adyen error
        if ([self.lastError.domain isEqualToString:ADYErrorDomain]) {
            // get error keys
            NSInteger errorCode = self.lastError.code;
            NSString *errorText = [self localizedDescriptionForErrorCode:errorCode];
            
            // build the error
            ServerErrorMessage *message = [[ServerErrorMessage alloc] init];
            message.code = [NSNumber numberWithInteger:errorCode];
            message.value = errorText;
            error = [[ServerError alloc] init];
            error.messages = [NSArray arrayWithObject:message];
        }
    }
    
    // return
    return error;
}

- (void) preflightCheckForTransaction:(BOOL)forTx
                             complete:(EFTProviderCompleteBlock)complete
{
    [self presentFakeAdyenControllerWithStep:FakeAdyenViewControllerStepLogin
                                  andOKBlock:^{
                                      complete(kEFTProviderErrorCodeSuccess, nil, NO);
                                  }
                              andCancelBlock:^{
                                  NSError *error = [NSError errorWithDomain:ADYErrorDomain
                                                                       code:ADYErrorCodeNotLoggedIn
                                                                   userInfo:nil];
                                  self.lastError = error;
                                  complete(kEFTProviderErrorCodeApplicationError, nil, NO);
                              }
                               andErrorBlock:^{
                                   NSError *error = [NSError errorWithDomain:ADYErrorDomain
                                                                        code:ADYErrorCodeAlreadyLoggedIn
                                                                    userInfo:nil];
                                   self.lastError = error;
                                   complete(kEFTProviderErrorCodeApplicationError, error, NO);
                               }];
}

- (void) cardSaleWithTender:(Tender *)tender
                     amount:(NSNumber *)amount
            forBasketWithID:(NSString *)basketId
          withCustomerEmail:(NSString *)customerEmail
                   complete:(EFTProviderCompleteBlock)complete
{
    // build a fake Adyen response
    ADYTransactionData *transaction = [self transactionWithTransactionType:ADYTransactionTypeGoodsServices
                                                                    tender:tender
                                                                    amount:amount
                                                           forBasketWithID:basketId
                                                         withCustomerEmail:customerEmail];
    self.manager.currentPayment.transaction = transaction;
    // update the payment
    EFTPayment *payment = self.manager.currentPayment;
    payment.encryptedAccountNumber = transaction.cardNumber;
    payment.authorizationCode = transaction.pspAuthCode;
    payment.referenceNumber = self.manager.currentPayment.uniqueMerchantReference;
    payment.authorizingTermId = transaction.tid;
    payment.cardTypeCode = transaction.cardType;
    payment.eftProvider = @"Adyen";
    payment.providerTenderId = transaction.tenderReference;
    payment.providerCentralTenderId = transaction.pspReference;
    payment.rawData = [self RawStringForTransaction];
    payment.tenderType = payment.tender.tenderType;
    payment.receiptCustomer = @"Omnico POS TEST                         \n	www.omnicogroup.com                     \n	CARDHOLDER COPY                         \n	Time                            14:17:41\n	Date                          03-03-2015\n	prefName                           ms en\n	aid                     A000000004306001\n	mid                            *\n	tid                      VX680-321187985\n	posEntryMode                         ICC\n	card type                           MSEN\n	authCode                          123456\n	Card                        9990\n	PANSeq                                53\n	TxRef                535I001109859461149\n	txType                    GOODS_SERVICES\n	Reference      0001010510024201503030030\n	Total                           1.00 GBP\n	                                        \n	                                        \n	                                        \n	                                        \n	                                        \n	APPROVED                                \n	                                        \n	                                        \n	Please retain for your records          \n	";
    payment.receiptMerchant = @"Omnico POS TEST                         \n	www.omnicogroup.com                     \n	MERCHANT COPY                           \n	Time                            14:17:41\n	Date                          03-03-2015\n	prefName                           ms en\n	aid                     A000000004306001\n	mid                            *\n	tid                      VX680-321187985\n	posEntryMode                         ICC\n	card type                           MSEN\n	authCode                          123456\n	Card                        9990\n	PANSeq                                53\n	TxRef                535I001109859461149\n	txType                    GOODS_SERVICES\n	Reference      0001010510024201503030030\n	Total                           1.00 GBP\n	                                        \n	                                        \n	                                        \n	                                        \n	                                        \n	APPROVED                                \n	                                        \n	                                        \n	Please retain for your records          \n	";
    [self presentFakeAdyenControllerWithStep:FakeAdyenViewControllerStepSignatureRequired
                                  andOKBlock:^{
                                      // capture signature
                                      complete(kEFTProviderErrorCodeSignatureRequired, nil, NO);
                                  }
                              andCancelBlock:^{
                                  complete(kEFTProviderErrorCodeSuccess, nil, NO);
                              }
                               andErrorBlock:^{
                                   // throw a random error
                                   NSError *error = [NSError errorWithDomain:ADYErrorDomain
                                                                        code:ADYErrorCodeUnknownError
                                                                    userInfo:nil];
                                   self.lastError = error;
                                   complete(kEFTProviderErrorCodeApplicationError, error, NO);
                               }];
}

- (void) cardRefundWithTender:(Tender *)tender
                   withAmount:(NSNumber *)amount
              forBasketWithID:(NSString *)basketId
            withCustomerEmail:(NSString *)customerEmail
                     complete:(EFTProviderCompleteBlock)complete
{
    // build a fake Adyen response
    ADYTransactionData *transaction = [self transactionWithTransactionType:ADYTransactionTypeRefund
                                                                    tender:tender
                                                                    amount:amount
                                                           forBasketWithID:basketId
                                                         withCustomerEmail:customerEmail];
    self.manager.currentPayment.transaction = transaction;
    // update the payment
    EFTPayment *payment = self.manager.currentPayment;
    payment.encryptedAccountNumber = transaction.cardNumber;
    payment.authorizationCode = transaction.pspAuthCode;
    payment.referenceNumber = self.manager.currentPayment.uniqueMerchantReference;
    payment.authorizingTermId = transaction.tid;
    payment.cardTypeCode = transaction.cardType;
    payment.eftProvider = @"Adyen";
    payment.providerTenderId = transaction.tenderReference;
    payment.providerCentralTenderId = transaction.pspReference;
    payment.rawData = [self RawStringForTransaction];
    payment.tenderType = payment.tender.tenderType;
    payment.receiptCustomer = @"Omnico POS TEST                         \n	www.omnicogroup.com                     \n	CARDHOLDER COPY                         \n	Time                            14:17:41\n	Date                          03-03-2015\n	prefName                           ms en\n	aid                     A000000004306001\n	mid                            *\n	tid                      VX680-321187985\n	posEntryMode                         ICC\n	card type                           MSEN\n	authCode                          123456\n	Card                        9990\n	PANSeq                                53\n	TxRef                535I001109859461149\n	txType                    GOODS_SERVICES\n	Reference      0001010510024201503030030\n	Total                           1.00 GBP\n	                                        \n	                                        \n	                                        \n	                                        \n	                                        \n	APPROVED                                \n	                                        \n	                                        \n	Please retain for your records          \n	";
    payment.receiptMerchant = @"Omnico POS TEST                         \n	www.omnicogroup.com                     \n	MERCHANT COPY                           \n	Time                            14:17:41\n	Date                          03-03-2015\n	prefName                           ms en\n	aid                     A000000004306001\n	mid                            *\n	tid                      VX680-321187985\n	posEntryMode                         ICC\n	card type                           MSEN\n	authCode                          123456\n	Card                        9990\n	PANSeq                                53\n	TxRef                535I001109859461149\n	txType                    GOODS_SERVICES\n	Reference      0001010510024201503030030\n	Total                           1.00 GBP\n	                                        \n	                                        \n	                                        \n	                                        \n	                                        \n	APPROVED                                \n	                                        \n	                                        \n	Please retain for your records          \n	";
    complete(kEFTProviderErrorCodeSuccess, nil, NO);
}

- (void) resumeTransactionComplete:(EFTProviderTransactionCompleteBlock)complete
{
    // signature check
    if (self.manager.currentPayment.signature.length == 0) {
        [self presentFakeAdyenControllerWithStep:FakeAdyenViewControllerStepTransactionResult
                                      andOKBlock:^{
                                          // transaction approved
                                          complete(kEFTPaymentCompleteReasonApproved, kEFTProviderErrorCodeSuccess, nil);
                                      }
                                  andCancelBlock:^{
                                      // transaction declined
                                      complete(kEFTPaymentCompleteReasonDeclined, kEFTProviderErrorCodeSuccess, nil);
                                  }
                                   andErrorBlock:^{
                                       NSError *error = [NSError errorWithDomain:ADYErrorDomain
                                                                            code:ADYErrorCodeTransactionInitializationError
                                                                        userInfo:nil];
                                       self.lastError = error;
                                       complete(kEFTPaymentCompleteReasonError, kEFTProviderErrorCodeSuccess, nil);
                                   }];
    } else {
        if (self.manager.currentPayment.signatureConfirmed) {
            // transaction approved
            complete(kEFTPaymentCompleteReasonApproved, kEFTProviderErrorCodeSuccess, nil);
        } else {
            // declined (signature check failed)
            complete(kEFTPaymentCompleteReasonDeclined, kEFTProviderErrorCodeSuccess, nil);
        }
    }
}

- (NSString *) RawStringForTransaction
{
    NSString *result = nil;
    
    ADYTransactionData *transaction = self.manager.currentPayment.transaction;
    if (transaction) {
        // first, iterate through each property of the transaction and build a dictionary
        NSMutableDictionary *transactionDictionary =
        [[transaction dictionaryWithValuesForKeys:[OCGEFTProviderAdyen transactionKeys]] mutableCopy];
        [transactionDictionary setObject:@(transaction.transactionType) forKey:@"transactionType"];
        [transactionDictionary setObject:@(transaction.cardScheme) forKey:@"cardScheme"];
        [transactionDictionary setObject:@(transaction.cardNeedsToBeRemovedAfterTransaction) forKey:@"cardNeedsToBeRemovedAfterTransaction"];
        [transactionDictionary setObject:@(transaction.finalState) forKey:@"finalState"];
        [transactionDictionary setObject:@(transaction.processingState) forKey:@"processingState"];
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        formatter.dateFormat = @"yyyyMMdd";
        [transactionDictionary setObject:[formatter stringFromDate:transaction.dateSubmitted] forKey:@"dateSubmitted"];
        [transactionDictionary setObject:@(transaction.failureReasonCode) forKey:@"failureReasonCode"];
        [transactionDictionary setObject:@(transaction.needsReferralAuthorisation) forKey:@"needsReferralAuthorisation"];
        
        /*
         @property (nonatomic, strong) CLLocation* location;
         @property (nonatomic, strong) NSError *rawFailureReason;
         */
        
        // serialise the dictionary
        NSError *error = nil;
        NSData *JSONData =
        [NSJSONSerialization dataWithJSONObject:transactionDictionary
#if DEBUG
                                        options:NSJSONWritingPrettyPrinted
#else
                                        options:0
#endif
                                          error:&error];
        if (JSONData) {
            result = [[NSString alloc] initWithData:JSONData
                                           encoding:NSUTF8StringEncoding];
        } else {
            ErrorLog(@"Could not generate JSON from transaction: %@", [error localizedDescription]);
        }
    }
    
    return result;
}

- (BOOL) voidsAllowed
{
    return YES;
}

- (void) voidTransactionWithReference:(NSString *)originalTxReference
                             complete:(EFTProviderCompleteBlock)complete
{
    complete(kEFTProviderErrorCodeSuccess, nil, NO);
}

- (void) confirmSignature:(UIImage *)signature
                 complete:(EFTProviderCompleteBlock)complete
{
    complete(kEFTProviderErrorCodeSuccess, nil, NO);
}

- (void) unconfirmSignature:(UIImage *)signature
                   complete:(EFTProviderCompleteBlock)complete
{
    complete(kEFTProviderErrorCodeSuccess, nil, NO);
}

@end
