//
//  BarcodeInputView.m
//  mPOS
//
//  Created by John Scott on 13/10/2014.
//  Copyright (c) 2014 Clarity. All rights reserved.
//

#import "CameraInputView.h"

#import <AVFoundation/AVFoundation.h>

@interface CameraInputView () <AVCaptureMetadataOutputObjectsDelegate>

@end

@implementation CameraInputView
{
    
    /* Here’s a quick rundown of the instance variables (via 'iOS 7 By Tutorials'):
     
     1. _captureSession – AVCaptureSession is the core media handling class in AVFoundation. It talks to the hardware to retrieve, process, and output video. A capture session wires together inputs and outputs, and controls the format and resolution of the output frames.
     
     2. _videoDevice – AVCaptureDevice encapsulates the physical camera on a device. Modern iPhones have both front and rear cameras, while other devices may only have a single camera.
     
     3. _videoInput – To add an AVCaptureDevice to a session, wrap it in an AVCaptureDeviceInput. A capture session can have multiple inputs and multiple outputs.
     
     4. _previewLayer – AVCaptureVideoPreviewLayer provides a mechanism for displaying the current frames flowing through a capture session; it allows you to display the camera output in your UI.
     5. _running – This holds the state of the session; either the session is running or it’s not.
     6. _metadataOutput - AVCaptureMetadataOutput provides a callback to the application when metadata is detected in a video frame. AV Foundation supports two types of metadata: machine readable codes and face detection.
     7. _backgroundQueue - Used for showing alert using a separate thread.
     */
    AVCaptureSession *_captureSession;
    AVCaptureDevice *_videoDevice;
    AVCaptureDeviceInput *_videoInput;
    AVCaptureVideoPreviewLayer *_previewLayer;
    BOOL _running;
    AVCaptureMetadataOutput *_metadataOutput;
    NSString *_lastCode;
}

static CameraInputView *camera = nil;

@synthesize orientation = _orientation;
- (void) setOrientation:(UIInterfaceOrientation)orientation
{
    _orientation = orientation;
    AVCaptureConnection *previewLayerConnection = _previewLayer.connection;
    if ([previewLayerConnection isVideoOrientationSupported]) {
        [previewLayerConnection setVideoOrientation:(AVCaptureVideoOrientation)orientation];
    }
}

-(instancetype)initWithFrame:(CGRect)frame
{
    if (!camera)
    {
        camera = [super initWithFrame:frame];
        
        if (camera)
        {
            [self setupCaptureSession];
            _previewLayer.frame = self.bounds;
            [self.layer addSublayer:_previewLayer];
            
            // listen for going into the background and stop the session
            [NSNotificationCenter.defaultCenter addObserver:camera
                                                   selector:@selector(applicationWillEnterForeground:)
                                                       name:UIApplicationWillEnterForegroundNotification
                                                     object:nil];
            
            [NSNotificationCenter.defaultCenter addObserver:camera
                                                     selector:@selector(applicationDidEnterBackground:)
                                                         name:UIApplicationDidEnterBackgroundNotification
                                                       object:nil];
            
            [NSNotificationCenter.defaultCenter addObserver:camera
                                                   selector:@selector(applicationDidEnterBackground:)
                                                       name:UIApplicationDidReceiveMemoryWarningNotification
                                                     object:nil];
            
            
        }
        return camera;
    }
    return camera;
}

-(void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(void)didMoveToSuperview
{
    if (self.superview)
    {
        [self startRunning];
    }
}

-(void)layoutSubviews
{
    [super layoutSubviews];
    _previewLayer.frame = self.bounds;
}

#pragma mark - AV capture methods

- (void)setupCaptureSession {
    // 1
    if (_captureSession) return;
    // 2
    _videoDevice = [AVCaptureDevice
                    defaultDeviceWithMediaType:AVMediaTypeVideo];
    if (!_videoDevice) {
        NSLog(@"No video camera on this device!");
        return;
    }
    // 3
    _captureSession = [[AVCaptureSession alloc] init];
    _captureSession.sessionPreset = AVCaptureSessionPresetPhoto;

    // 4
    _videoInput = [[AVCaptureDeviceInput alloc]
                   initWithDevice:_videoDevice error:nil];
    // 5
    if ([_captureSession canAddInput:_videoInput]) {
        [_captureSession addInput:_videoInput];
    }
    // 6
    _previewLayer = [[AVCaptureVideoPreviewLayer alloc]
                     initWithSession:_captureSession];
    _previewLayer.videoGravity =
    AVLayerVideoGravityResizeAspectFill;
    
    
    // capture and process the metadata
    _metadataOutput = [[AVCaptureMetadataOutput alloc] init];
    dispatch_queue_t metadataQueue =
    dispatch_queue_create("com.1337labz.featurebuild.metadata", 0);
    [_metadataOutput setMetadataObjectsDelegate:self
                                          queue:metadataQueue];
    if ([_captureSession canAddOutput:_metadataOutput]) {
        [_captureSession addOutput:_metadataOutput];
    }
}

- (void)startRunning {
    self.orientation = [[UIApplication sharedApplication] statusBarOrientation];
    if (_running) return;
    [_captureSession startRunning];
    _metadataOutput.metadataObjectTypes =
    _metadataOutput.availableMetadataObjectTypes;
    _running = YES;
}
- (void)stopRunning {
    if (!_running) return;
    [_captureSession stopRunning];
    _running = NO;
}

//  handle going foreground/background
- (void)applicationWillEnterForeground:(NSNotification*)note
{
    [self startRunning];
}
- (void)applicationDidEnterBackground:(NSNotification*)note
{
    [self stopRunning];
}

-(void)applicationDidReceiveMemoryWarningNotification:(NSNotification*)note
{
    if (!self.superview)
    {
        camera = nil;
    }
}

#pragma mark - Delegate functions

- (void)captureOutput:(AVCaptureOutput *)captureOutput
didOutputMetadataObjects:(NSArray *)metadataObjects
fromConnection:(AVCaptureConnection *)connectionbounds
{
    for (AVMetadataMachineReadableCodeObject *code in metadataObjects)
    {
        CGRect metaRect = [_previewLayer rectForMetadataOutputRectOfInterest:code.bounds];

        if ([code isKindOfClass:[AVMetadataMachineReadableCodeObject class]] && CGRectContainsRect(_previewLayer.bounds, metaRect))
        {
            if (![_lastCode isEqualToString:code.stringValue])
            {
                _lastCode = code.stringValue;

                {
                    id responder = [self nextResponder];
                    
                    while (responder)
                    {
                        if ([responder respondsToSelector:@selector(insertBarcode:entryMethod:barcodeType:)])
                        {
                            SMBarcodeType type = SMBarcodeTypeUnknown;
                            
                            if (0) {}
                            else if ([code.type isEqual:AVMetadataObjectTypeUPCECode]) { type = SMBarcodeTypeUPCE; }
                            else if ([code.type isEqual:AVMetadataObjectTypeCode39Code]) { type = SMBarcodeTypeCode39; }
//                            else if ([code.type isEqual:AVMetadataObjectTypeCode39Mod43Code]) { type = SMBarcodeTypeCode39Mod43; }
                            else if ([code.type isEqual:AVMetadataObjectTypeEAN13Code]) { type = SMBarcodeTypeEAN13; }
                            else if ([code.type isEqual:AVMetadataObjectTypeEAN8Code]) { type = SMBarcodeTypeEAN8; }
                            else if ([code.type isEqual:AVMetadataObjectTypeCode93Code]) { type = SMBarcodeTypeCode93; }
                            else if ([code.type isEqual:AVMetadataObjectTypeCode128Code]) { type = SMBarcodeTypeCode128; }
//                            else if ([code.type isEqual:AVMetadataObjectTypePDF417Code]) { type = SMBarcodeTypePDF417Code; }
                            else if ([code.type isEqual:AVMetadataObjectTypeQRCode]) { type = SMBarcodeTypeQRCode; }
//                            else if ([code.type isEqual:AVMetadataObjectTypeAztecCode]) { type = SMBarcodeTypeAztecCode; }
#ifdef __IPHONE_8_0
                            else if ([code.type isEqual:AVMetadataObjectTypeInterleaved2of5Code]) { type = SMBarcodeTypeI2OF5; }
#endif
//                            else if ([code.type isEqual:AVMetadataObjectTypeITF14Code]) { type = SMBarcodeTypeITF14; }
//                            else if ([code.type isEqual:AVMetadataObjectTypeDataMatrixCode]) { type = SMBarcodeTypeDataMatrixCode; }

                            ocg_sync_main(^{
                                [responder insertBarcode:code.stringValue entryMethod:ScannerEntryMethodScanned barcodeType:type];
                            });
                            return;
                        }
                        
                        responder = [responder nextResponder];
                    }
                }
                
                
                {
                    id responder = [self nextResponder];
                    
                    while (responder)
                    {
                        if ([responder isKindOfClass:[UITextField class]])
                        {
                            UITextField *textField = responder;
                            ocg_sync_main(^{
                                [textField insertText:code.stringValue];
                                if ([textField.delegate respondsToSelector:@selector(textFieldShouldReturn:)])
                                {
                                    [textField.delegate textFieldShouldReturn:textField];
                                }
                            });
                            return;
                        }
                        
                        responder = [responder nextResponder];
                    }
                }
                
                
                {
                    id responder = [self nextResponder];
                    
                    while (responder)
                    {
                        if ([responder conformsToProtocol:@protocol(UITextInput)])
                        {
                            ocg_sync_main(^{
                                [[responder inputDelegate] textWillChange:responder];
                                
                                
                                [responder setText:code.stringValue];
                                [responder insertText:@"\r"];
                                [[responder inputDelegate] textDidChange:responder];
                            });
                            return;
                        }
                        
                        responder = [responder nextResponder];
                    }
                }
                
                
            }
        }
    }
    if (metadataObjects.count == 0)
    {
        _lastCode = nil;
    }
}

@end
