//
//  OCGSwitchTableViewCell.m
//  mPOS
//
//  Created by Antonio Strijdom on 14/08/2013.
//  Copyright (c) 2013 Clarity. All rights reserved.
//

#import "OCGSwitchTableViewCell.h"

@interface OCGSwitchTableViewCell ()
@property (nonatomic, strong) UISwitch *toggleSwitch;
- (IBAction) toggleValueDidChange:(id)sender;
@end

@implementation OCGSwitchTableViewCell

#pragma mark - Private

- (IBAction) toggleValueDidChange:(id)sender
{
    if (self.delegate != nil) {
        if ([self.delegate respondsToSelector:@selector(switchTableViewCell:didToggleTo:)]) {
            [self.delegate switchTableViewCell:self
                                   didToggleTo:self.toggleSwitch.on];
        }
    }
}

#pragma mark - Properties

@synthesize on;
- (BOOL) on
{
    BOOL result = NO;
    
    if (self.toggleSwitch != nil) {
        result = self.toggleSwitch.on;
    }
    
    return result;
}
- (void) setOn:(BOOL)value
{
    if (self.toggleSwitch != nil) {
        [self.toggleSwitch setOn:value
                        animated:YES];
    }
}

#pragma mark - Init

- (id) initWithStyle:(UITableViewCellStyle)style
     reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style
                reuseIdentifier:reuseIdentifier];
    if (self) {
        self.accessoryType = UITableViewCellAccessoryNone;
        self.selectionStyle = UITableViewCellEditingStyleNone;
        self.toggleSwitch = [[UISwitch alloc] init];
        self.toggleSwitch.translatesAutoresizingMaskIntoConstraints = NO;
        [self.contentView addSubview:self.toggleSwitch];
        [self.contentView addConstraint:
         [NSLayoutConstraint constraintWithItem:self.toggleSwitch
                                      attribute:NSLayoutAttributeCenterY
                                      relatedBy:NSLayoutRelationEqual
                                         toItem:self.contentView
                                      attribute:NSLayoutAttributeCenterY
                                     multiplier:1.0
                                       constant:0.0]];
        CGFloat padding = -10.0f;
#ifdef __IPHONE_7_0
        if ([UIDevice currentDevice].systemVersion.floatValue >= 7.0) {
            padding = 20.0f;
        }
#endif
        [self.contentView addConstraint:
         [NSLayoutConstraint constraintWithItem:self.toggleSwitch
                                      attribute:NSLayoutAttributeRight
                                      relatedBy:NSLayoutRelationEqual
                                         toItem:self.contentView
                                      attribute:NSLayoutAttributeRight
                                     multiplier:1.0
                                       constant:padding]];
        // 79
        [self.contentView addConstraint:
         [NSLayoutConstraint constraintWithItem:self.toggleSwitch
                                      attribute:NSLayoutAttributeWidth
                                      relatedBy:NSLayoutRelationEqual
                                         toItem:nil
                                      attribute:NSLayoutAttributeNotAnAttribute
                                     multiplier:1.0
                                       constant:79]];
        // 27
        [self.contentView addConstraint:
         [NSLayoutConstraint constraintWithItem:self.toggleSwitch
                                      attribute:NSLayoutAttributeHeight
                                      relatedBy:NSLayoutRelationEqual
                                         toItem:nil
                                      attribute:NSLayoutAttributeNotAnAttribute
                                     multiplier:1.0
                                       constant:27]];
        [self.toggleSwitch addTarget:self
                              action:@selector(toggleValueDidChange:)
                    forControlEvents:UIControlEventValueChanged];
    }
    return self;
}

- (void) layoutSubviews
{
    [super layoutSubviews];
    [self.contentView bringSubviewToFront:self.toggleSwitch];
    self.toggleSwitch.enabled = !self.readonly;
    if (self.labelXPosition != nil) {
        CGRect labelRect = self.textLabel.frame;
        labelRect.origin.x = self.labelXPosition.doubleValue;
        self.textLabel.frame = labelRect;
    }
}

@end
