//
//  ItemDetailViewController.m
//  mPOS
//
//  Created by Antonio Strijdom on 22/05/2013.
//  Copyright (c) 2013 Clarity. All rights reserved.
//

#import "ItemDetailViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "CRSSkinning.h"
#import "ItemDetailImageCollectionCell.h"
#import "ItemDetailImageCell.h"
#import "ItemDetailDetailsCell.h"
#import "ItemDetailQuantityCell.h"
#import "ItemDetailPropertyCell.h"
#import "BasketController.h"
#import "ItemDetailOtherQuantityViewController.h"

@interface ItemDetailViewController ()
@property (nonatomic, strong) NSMutableDictionary *imageCache;
@property (nonatomic, strong) UIButton *imageView;
@property (nonatomic, strong) NSArray *images;
@property (nonatomic, strong) NSArray *properties;
- (void) handleAvailabilityNotification:(NSNotification *)note;
- (void) displayImage:(UIImage *)image
         zoomFromRect:(CGRect)rect;
- (void) hideImage:(BOOL)animated;
- (void) imageTapped:(id)sender;
@end

@implementation ItemDetailViewController

#pragma mark - Private

NSString *kItemDetailImageCollectionCell = @"kItemDetailImageCollectionCell";
NSString *kImageCellIdentifier = @"kImageCellIdentifier";
NSString *kItemDetailDetailsCell = @"kItemDetailDetailsCell";
NSString *kItemDetailQuantityCell = @"kItemDetailQuantityCell";
NSString *kItemPropertyHeaderCell = @"kItemPropertyHeaderCell";
NSString *kItemPropertyCell = @"kItemPropertyCell";
const NSInteger kSectionIndexImages = 0;
const NSInteger kSectionIndexDetails = 1;
const NSInteger kSectionIndexQuantity = 2;
const NSInteger kSectionIndexProperties = 3;

- (void) handleAvailabilityNotification:(NSNotification *)note
{
    NSOrderedSet *availability = note.userInfo[kBasketControllerItemAvailabilityResultsKey];
    if (availability) {
        ItemDetailOtherQuantityViewController *vc = [[ItemDetailOtherQuantityViewController alloc] initWithStyle:UITableViewStyleGrouped];
        vc.availability = [availability array];
        [self.navigationController pushViewController:vc
                                             animated:YES];
    }
}

- (void) displayImage:(UIImage *)image
         zoomFromRect:(CGRect)rect
{
    self.imageView.frame = rect;    
    self.imageView.alpha = 0.0f;
    self.imageView.hidden = NO;
    [self.view bringSubviewToFront:self.imageView];
    
//    // keep aspect ratio
//    CGSize finalSize = CGSizeZero;
//    // if the image is taller than it is wide
//    if (image.size.width > image.size.height) {
//        CGFloat aspect = image.size.width / image.size.height;
//        finalSize = CGSizeMake(self.view.frame.size.width * aspect,
//                               self.view.frame.size.height);
//    } else {
//        CGFloat aspect = image.size.height / image.size.width;
//        finalSize = CGSizeMake(self.view.frame.size.width,
//                               self.view.frame.size.height * aspect);
//    }
    CGSize finalSize = image.size;
    
    // resize the image to fit
    UIGraphicsBeginImageContextWithOptions(self.navigationController.view.frame.size, NO, 0.0);
    [image drawInRect:CGRectMake((self.navigationController.view.frame.size.width / 2) - (finalSize.width / 2),
                                 (self.navigationController.view.frame.size.height / 2) - (finalSize.height / 2),
                                 finalSize.width,
                                 finalSize.height)];
    UIImage *buttonImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    [self.imageView setImage:buttonImage
                    forState:UIControlStateNormal];
    [self.imageView setImage:buttonImage
                    forState:UIControlStateHighlighted];
    
    [UIView animateWithDuration:0.4
                     animations:^{
                         self.imageView.frame = self.navigationController.view.frame;
                         self.imageView.alpha = 1.0f;
                     }];
}

- (void) hideImage:(BOOL)animated
{
    if (animated) {
        [UIView animateWithDuration:0.2
                         animations:^{
                             self.imageView.alpha = 0.0f;
                         }
                         completion:^(BOOL finished) {
                             self.imageView.hidden = YES;
                         }];
    } else {
        self.imageView.alpha = 0.0f;
        self.imageView.hidden = YES;
    }
}

- (void) imageTapped:(id)sender
{
    [self hideImage:YES];
}

#pragma mark - Properties

@synthesize item = _item;
- (void) setItem:(MPOSItem *)item
{
    _item = item;
    if (item) {
        self.images = item.additionalImages;
        self.properties = [self.item.properties sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2) {
            NSString *key1 = [(MPOSItemProperties *)obj1 key];
            NSString *key2 = [(MPOSItemProperties *)obj2 key];
            return [key1 compare:key2
                         options:NSCaseInsensitiveSearch];
        }];
    } else {
        self.images = nil;
        self.properties = nil;
    }
}

#pragma mark - UITableViewController

- (id) initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {

    }
    return self;
}

- (void) viewDidLoad
{
    [super viewDidLoad];
	// register cell nibs
    UINib *nib = [UINib nibWithNibName:@"ItemDetailImageCollectionCell"
                                bundle:nil];
    [self.tableView registerNib:nib
         forCellReuseIdentifier:kItemDetailImageCollectionCell];
    nib = [UINib nibWithNibName:@"ItemDetailDetailsCell"
                         bundle:nil];
    [self.tableView registerNib:nib
         forCellReuseIdentifier:kItemDetailDetailsCell];
    nib = [UINib nibWithNibName:@"ItemDetailQuantityCell"
                         bundle:nil];
    [self.tableView registerNib:nib
         forCellReuseIdentifier:kItemDetailQuantityCell];
    nib = [UINib nibWithNibName:@"ItemDetailPropertyHeaderCell"
                         bundle:nil];
    [self.tableView registerNib:nib
         forCellReuseIdentifier:kItemPropertyHeaderCell];
    nib = [UINib nibWithNibName:@"ItemDetailPropertyCell"
                         bundle:nil];
    [self.tableView registerNib:nib
         forCellReuseIdentifier:kItemPropertyCell];
    
    // create the image view
    self.imageView = [UIButton buttonWithType:UIButtonTypeCustom];
    self.imageView.backgroundColor = [UIColor whiteColor];
    self.imageView.opaque = NO;
    self.imageView.hidden = YES;
    [self.imageView addTarget:self
                       action:@selector(imageTapped:)
             forControlEvents:UIControlEventTouchUpInside];
    [self.navigationController.view addSubview:self.imageView];
    
    // create the image cache
    self.imageCache = [NSMutableDictionary dictionary];
    
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    // apply skin
    self.tableView.tag = kTableSkinningTag;
}

- (void) viewWillAppear:(BOOL)animated
{
    // localise
    self.title = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁈󠁣󠁤󠁗󠁪󠁵󠀲󠁢󠁵󠀰󠁃󠁘󠁩󠁲󠁎󠁁󠁘󠁦󠁹󠁫󠁑󠁹󠁂󠁓󠁴󠁏󠁙󠁿*/ @"Item Details", nil);

    [[CRSSkinning currentSkin] applyViewSkin: self];
    
    [super viewWillAppear:animated];
    
    [self hideImage:NO];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(handleAvailabilityNotification:)
                                                 name:kBasketControllerItemAvailabilityGetCompleteNotification
                                               object:nil];
}

- (void) viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [self hideImage:NO];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:kBasketControllerItemAvailabilityGetCompleteNotification
                                                  object:nil];
}

- (void) didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITableViewDataSource

- (NSInteger) numberOfSectionsInTableView:(UITableView *)tableView
{
    return 4;
}

- (NSInteger) tableView:(UITableView *)tableView
  numberOfRowsInSection:(NSInteger)section
{
    NSInteger result = 0;
    
    if (self.item) {
        switch (section) {
            case kSectionIndexImages:
                result = 1;
                break;
            case kSectionIndexDetails:
                result = 1;
                break;
            case kSectionIndexQuantity:
                result = 1;
                break;
            case kSectionIndexProperties:
                result = self.item.properties.count;
                break;
            default:
                break;
        }
    }
    
    return result;
}

- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat result = 44.0f;
    
    switch (indexPath.section) {
        case kSectionIndexImages:
            result = 100.0f;
            break;
        case kSectionIndexDetails:
            result = 93.0f;
            break;
        case kSectionIndexQuantity:
            result = 44.0f;
            break;
        case kSectionIndexProperties: {
            // calculate the basic row height
            result = 44.0f;
            if (indexPath.row == self.item.properties.count - 1) {
                result += 10.0f;
            } else if (indexPath.row == 0) {
                result = 75.0f;
            }
            // take off the detail label's height
            result -= 21.0f;
            // get the text for this row
            MPOSItemProperties *property = [self.properties objectAtIndex:indexPath.row];
            if (property) {
                // calculate the new text height
                NSStringDrawingContext *context = [[NSStringDrawingContext alloc] init];
                context.minimumScaleFactor = 1.0f;
                CGRect textBounds =
                [property.value boundingRectWithSize:CGSizeMake(self.tableView.frame.size.width - 20.0f, CGFLOAT_MAX)
                                             options:NSStringDrawingUsesLineFragmentOrigin
                                          attributes:@{ NSFontAttributeName : [UIFont systemFontOfSize:17.0f] }
                                             context:context];
                CGSize textSize = textBounds.size;
                // add the height to the row height
                result += textSize.height;
            }
            break;
        }
        default:
            break;
    }
    
    return result;
}

- (UITableViewCell *) tableView:(UITableView *)tableView
          cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *result = nil;
    
    switch (indexPath.section) {
        case kSectionIndexImages: {
            ItemDetailImageCollectionCell *imageCell =
            [tableView dequeueReusableCellWithIdentifier:kItemDetailImageCollectionCell];
            if (imageCell) {
                imageCell.collectionView.delegate = self;
                imageCell.collectionView.dataSource = self;
                // register cell nibs
                UINib *nib = [UINib nibWithNibName:@"ItemDetailImageCell"
                                            bundle:nil];
                [imageCell.collectionView registerNib:nib
                           forCellWithReuseIdentifier:kImageCellIdentifier];
                result = imageCell;
            }
            break;
        }
        case kSectionIndexDetails: {
            ItemDetailDetailsCell *detailCell =
            [tableView dequeueReusableCellWithIdentifier:kItemDetailDetailsCell];
            if (detailCell) {
                detailCell.SKULabel.text = self.item.itemRefId;
                detailCell.priceLabel.text = self.item.displayPrice;
                detailCell.descriptionLabel.text = self.item.shortDesc;
                detailCell.skuDescLabel.text = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁖󠀹󠁏󠁋󠀵󠁱󠁥󠀫󠁡󠁢󠁕󠁨󠀰󠁓󠁫󠁇󠀰󠁃󠁬󠁊󠁁󠀫󠁴󠁺󠁶󠁄󠁷󠁿*/ @"SKU", nil);
                detailCell.priceDescLabel.text = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠀵󠁫󠁸󠁢󠁐󠁱󠀫󠁣󠁏󠁣󠁒󠁚󠁇󠁳󠁐󠁹󠁉󠀸󠀯󠁈󠁥󠁏󠁤󠁺󠁔󠁉󠀰󠁿*/ @"Price", nil);
                // skin
                detailCell.tag = kTableCellSkinningTag;
                detailCell.SKULabel.tag = kTableCellValueSkinningTag;
                detailCell.priceLabel.tag = kTableCellValueSkinningTag;
                detailCell.descriptionLabel.tag = kTableCellTextSkinningTag;
                detailCell.skuDescLabel.tag = kTableCellKeySkinningTag;
                detailCell.priceDescLabel.tag = kTableCellKeySkinningTag;
                detailCell.descriptionBackground.tag = kSecondaryButtonSkinningTag;
                result = detailCell;
            }
            break;
        }
        case kSectionIndexQuantity: {
            ItemDetailQuantityCell *quantityCell =
            [tableView dequeueReusableCellWithIdentifier:kItemDetailQuantityCell];
            if (quantityCell) {
                quantityCell.quantityDescLabel.text = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁑󠁄󠁏󠁴󠁩󠁔󠁈󠀫󠁌󠁵󠁔󠁆󠁧󠁤󠀸󠁭󠀸󠁱󠀷󠁢󠁓󠁣󠁎󠁡󠁯󠁇󠁙󠁿*/ @"Quantity", nil);
                quantityCell.quantityLabel.text = self.item.quantity;
                // skin
                quantityCell.tag = kTableCellSkinningTag;
                quantityCell.quantityDescLabel.tag = kTableCellTextSkinningTag;
                quantityCell.quantityLabel.tag = kTableCellKeySkinningTag;
                quantityCell.bodyView.tag = kPrimaryButtonSkinningTag;
                result = quantityCell;
            }
            break;
        }
        case kSectionIndexProperties: {
            ItemDetailPropertyCell *propertyCell = nil;
            if (indexPath.row == 0) {
                propertyCell = [tableView dequeueReusableCellWithIdentifier:kItemPropertyHeaderCell];
            } else {
                propertyCell = [tableView dequeueReusableCellWithIdentifier:kItemPropertyCell];
            }
            if (propertyCell) {
                MPOSItemProperties *property = [self.properties objectAtIndex:indexPath.row];
                if (property) {
                    propertyCell.headerLabel.text = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁬󠁕󠁨󠁉󠁨󠁃󠁰󠁑󠁥󠁪󠁄󠁐󠁖󠁊󠁨󠁔󠁫󠁲󠁄󠁙󠁯󠁖󠁋󠀴󠁵󠁳󠁣󠁿*/ @"Properties", nil);
                    propertyCell.propertyName.text = property.key;
                    propertyCell.propertyValue.text = property.value;
                    if (indexPath.row == self.item.properties.count - 1) {
                        propertyCell.bottomSpaceConstraint.constant = 10.0f;
                        propertyCell.seperator.hidden = YES;
                    } else {
                        propertyCell.bottomSpaceConstraint.constant = 0.0f;
                        propertyCell.seperator.hidden = NO;
                    }
                    // skin
                    propertyCell.tag = kTableCellSkinningTag;
                    propertyCell.headerBackground.tag = kSecondaryButtonSkinningTag;
                    propertyCell.headerLabel.tag = kTableCellTextSkinningTag;
                    propertyCell.propertyName.tag = kTableCellKeySkinningTag;
                    propertyCell.propertyValue.tag = kTableCellValueSkinningTag;
                    result = propertyCell;
                }
            }
            break;
        }
        default:
            break;
    }
    
    return result;
}

- (void) tableView:(UITableView *)tableView
   willDisplayCell:(UITableViewCell *)cell
 forRowAtIndexPath:(NSIndexPath *)indexPath
{
    [[CRSSkinning currentSkin] applyCellSkin:cell
                                    forStyle:tableView.style];
}

#pragma mark - UITableViewDelegate

- (NSIndexPath *) tableView:(UITableView *)tableView
   willSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == kSectionIndexQuantity) {
        return indexPath;
    } else {
        return nil;
    }
}

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == kSectionIndexQuantity) {
        // load availability
        [[BasketController sharedInstance] getAvailabilityForItem:self.item];
    }
}

#pragma mark - UICollectionViewDataSource

- (NSInteger) numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (NSInteger) collectionView:(UICollectionView *)collectionView
      numberOfItemsInSection:(NSInteger)section
{
    NSInteger result = 0;
    
    if (self.item) {
        result = self.item.additionalImages.count;
    }
    
    return result;
}

- (UICollectionViewCell *) collectionView:(UICollectionView *)collectionView
                   cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    ItemDetailImageCell *result = nil;
    
    switch (indexPath.section) {
        case 0: {
            result =
            [collectionView dequeueReusableCellWithReuseIdentifier:kImageCellIdentifier
                                                      forIndexPath:indexPath];
            
            MPOSItemImages *image = [self.images objectAtIndex:indexPath.row];
            NSString *imageURLString = image.image;
            // check the cache
            UIImage *cachedImage = self.imageCache[imageURLString];
            if (cachedImage) {
                result.imageView.image = cachedImage;
            } else {
                [result.activityIndicator startAnimating];
                ocg_async_background_overlay(^{
                    // download the image
                    NSURL *url = [NSURL URLWithString:imageURLString];
                    NSURLRequest *request = [NSURLRequest requestWithURL:url];
                    NSURLResponse *response = nil;
                    NSError *error = nil;
                    NSData *imageData = [NSURLConnection sendSynchronousRequest:request
                                                              returningResponse:&response
                                                                          error:&error];
                    if (imageData != nil) {
                        UIImage *image = [UIImage imageWithData:imageData];
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [self.imageCache setObject:image
                                                forKey:imageURLString];
                            result.imageView.image = image;
                            [result.activityIndicator stopAnimating];
                        });
                    }
                });
            }
            break;
        }
        default:
            break;
    }
    
    return result;
}

#pragma mark - UICollectionViewDelegate

- (void) collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    // display the image full screen
    UICollectionViewLayoutAttributes *attributes =
    [collectionView layoutAttributesForItemAtIndexPath:indexPath];
    CGRect cellRect = attributes.frame;
    cellRect = [self.view convertRect:cellRect
                             fromView:collectionView];
    // get the image
    MPOSItemImages *image = [self.images objectAtIndex:indexPath.row];
    NSString *imageURLString = image.image;
    UIImage *cachedImage = self.imageCache[imageURLString];
    if (cachedImage) {
        [self displayImage:cachedImage
              zoomFromRect:cellRect];
    }
    [collectionView deselectItemAtIndexPath:indexPath
                                   animated:NO];
}

@end
