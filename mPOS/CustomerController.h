//
//  CustomerController.h
//  mPOS
//
//  Created by Meik Schuetz on 21/09/2012.
//  Copyright (c) 2012 Clarity. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "RESTController.h"

@interface CustomerController : NSObject

+ (void) setSharedInstance:(CustomerController *) sharedInstance;
+ (CustomerController *) sharedInstance;

/** @brief Clears the current search.
 */
- (void) clearCustomerSearch;

/** @brief Retrieves a customer record by using email as key.
 *  @param customerEmail The customer email to be used as search key.
 */
- (void) getCustomerDetailByEmail:(NSString *)customerEmail;

/** @brief Retrieves a customer record by using email as key.
 *  @param customerId The customer id to be used as search key.
 */
- (void) getCustomerDetailById:(NSString *)customerId;

/** @brief Retrieves a customer record by using email as key.
 */

- (void) getCustomerDetailByCardNo:(NSString *)cardNo
                          cardType:(NSString *)cardType;

/** @brief Retreives the cards and notes associated with this customer.
 *  @param customer The customer to retreive cards for.
 *  @param cards Whether to fetch cards or not.
 *  @param notes Whether to fetch notes or not.
 */
- (void) getCustomerDetails:(MPOSCustomer *)customer
                      cards:(BOOL)cards
                      notes:(BOOL)notes;

/** @brief Updates the customer record on the server.
 */
- (void) synchronizeChangesForCustomer:(MPOSCustomer *)customer;

/** @brief Adds a new customer.
 *  @return The new customer.
 *  @note This will also clear the current search.
 */
- (MPOSCustomer *) addCustomer;

/** @brief Assigns the customer card number provided to the basket.
 *  @param cardNumber The card number to assign.
 */
- (void) assignCustomerCardToBasket:(NSString *)cardNumber;

- (NSOrderedSet *) currentCustomers;

@property (nonatomic, strong) MPOSCustomerSearch *customerSearch;

-(void)filloutAllValuesForCustomer:(MPOSCustomer*)customer;

@end
