//
//  OCGEFTManagerCurrentPayment.h
//  mPOS
//
//  Created by Antonio Strijdom on 25/02/2015.
//  Copyright (c) 2015 Clarity. All rights reserved.
//

#import "RESTController.h"

/** @enum EFTPaymentTransactionType
 *  @brief Enumaration of EFT transaction types.
 */
typedef NS_ENUM(NSInteger, EFTPaymentTransactionType) {
    /** @brief Sale
     */
    kEFTPaymentTransactionTypeSale = 0,
    /** @brief Refund
     */
    kEFTPaymentTransactionTypeRefund = 1,
    /** @brief Void
     */
    kEFTPaymentTransactionTypeVoid = 2
};

/** @enum EFTPaymentStatus
 *  @brief Enum representing the known states of an EFT Payment
 */
typedef NS_ENUM(NSInteger, EFTPaymentStatus) {
    /** @brief The payment is initialising.
     *  @discussion The payment API is being initialised, preflight checks are being performed.
     */
    kEFTPaymentStatusInitializing = 0,
    /** @brief The payment is processing on the payment device.
     */
    kEFTPaymentStatusProcessing = 1,
    /** @brief The customer's signature is required.
     *  @discussion The signature capture process will be kicked off.
     */
    kEFTPaymentStatusSignatureRequired = 2,
    /** @brief The payment has been processed on the payment device.
     *  @discussion The payment will now be sent to the server and
     *  (depending on the outcome) a payment will be added to the basket.
     */
    kEFTPaymentStatusFinishing = 4,
    /** @brief The payment is complete.
     */
    kEFTPaymentStatusComplete = 5
};

/** @enum EFTPaymentCompleteReason
 *  @brief Reasons the EFT payment completed.
 */
typedef NS_ENUM(NSInteger, EFTPaymentCompleteReason) {
    /** @brief The transaction is not complete.
     */
    kEFTPaymentCompleteReasonNotComplete = -1,
    /** @brief Transaction was approved.
     */
    kEFTPaymentCompleteReasonApproved = 0,
    /** @brief Transaction was declined.
     */
    kEFTPaymentCompleteReasonDeclined = 1,
    /** @brief Transaction was cancelled.
     */
    kEFTPaymentCompleteReasonCancelled = 2,
    /** @brief Transaction errored.
     */
    kEFTPaymentCompleteReasonError = 3,
    /** @brief Transaction voided.
     */
    kEFTPaymentCompleteReasonVoided = 4
};

/** @brief Extends EFTPayment to include data pertaining to the status
 *  of the current ongoing transaction.
 */
@interface OCGEFTManagerCurrentPayment : EFTPayment

/** @property transactionType
 *  @brief The transaction type of this payment (e.g. Sale, Refund, Void, etc).
 */
@property (nonatomic, assign) EFTPaymentTransactionType transactionType;

/** @property customerEmail
 *  @brief The optional customer email address (for electronic receipts).
 */
@property (nonatomic, strong) NSString *customerEmail;

/** @property transaction
 *  @brief A reference to the provider specific transaction.
 */
@property (nonatomic, strong) id transaction;

/** @property signatureConfirmed
 *  @brief Whether the customer's signature was confirmed as matching or not.
 */
@property (nonatomic, assign) BOOL signatureConfirmed;

/** @property The unique merchant reference for this transaction.
 */
@property (nonatomic, readonly) NSString *uniqueMerchantReference;

/** @property originalMerchantReference
 *  @brief When voiding, the unique merchant reference of the original transaction.
 */
@property (nonatomic, strong) NSString *originalMerchantReference;

/** @property originalBasketItemId
 *  @brief When voiding, the basket item id of the original transaction
 */
@property (nonatomic, strong) NSString *originalBasketItemId;

/** @property paymentStatus
 *  @brief The current status of the payment.
 *  @see EFTPaymentStatus
 */
@property (nonatomic, assign) EFTPaymentStatus paymentStatus;

/** @property paymentCompleteReason
 *  @brief The final completion reason (e.g. Approved, declined, etc).
 *  @see EFTPaymentCompleteReason
 */
@property (nonatomic, assign) EFTPaymentCompleteReason paymentCompleteReason;

/** @brief Returns a localised human readable string for the current payment status.
 *  @return A localised human readable string for the current payment status.
 */
- (NSString *) localizedStringForCurrentStatus;

@end