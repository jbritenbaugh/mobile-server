//
//  AlternateContextMenuCell.m
//  mPOS
//
//  Created by Antonio Strijdom on 05/02/2014.
//  Copyright (c) 2014 Clarity. All rights reserved.
//

#import "AlternateContextMenuCell.h"

@interface AlternateContextMenuCell ()
@property (nonatomic, strong) UIButton *button;
@property (nonatomic, strong) UILabel *buttonCaption;
@end

@implementation AlternateContextMenuCell

- (id) initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // setup the label
        self.buttonCaption = [[UILabel alloc] init];
        self.buttonCaption.backgroundColor = [UIColor clearColor];
        self.buttonCaption.translatesAutoresizingMaskIntoConstraints = NO;
        self.buttonCaption.userInteractionEnabled = NO;
        self.buttonCaption.tag = kNoSaleMenuItemSkinningTag;
        self.buttonCaption.textAlignment = NSTextAlignmentCenter;
        self.buttonCaption.numberOfLines = 2;
        self.buttonCaption.lineBreakMode = NSLineBreakByWordWrapping;
        [self.contentView addSubview:self.buttonCaption];
        [self.buttonCaption constrain:@"left = left"
                                   to:self.contentView];
        [self.buttonCaption constrain:@"bottom = bottom"
                                   to:self.contentView];
        [self.buttonCaption constrain:@"width = width"
                                   to:self.contentView];
        [self.buttonCaption constrain:@"height = 25.0"
                                   to:nil];
        // setup the button background
        self.button = [UIButton buttonWithType:UIButtonTypeCustom];
        self.button.translatesAutoresizingMaskIntoConstraints = NO;
        self.button.userInteractionEnabled = NO;
        self.button.tag = kNoSaleMenuItemSkinningTag;
        self.button.layer.cornerRadius = 5.0f;
        [self.contentView addSubview:self.button];
        [self.button constrain:@"top = top"
                            to:self.contentView];
//        [self.button constrain:@"left = left + 7.5"
//                            to:self.contentView];
        [self.button constrain:@"bottom = top - 2.0"
                            to:self.buttonCaption];
//        [self.button constrain:@"width = height - 5.0" to:self];
        [self.contentView addConstraint:
         [NSLayoutConstraint constraintWithItem:self.button
                                      attribute:NSLayoutAttributeHeight
                                      relatedBy:NSLayoutRelationEqual
                                         toItem:self.button
                                      attribute:NSLayoutAttributeWidth
                                     multiplier:1.0
                                       constant:0.0]];
        [self.button constrain:@"centerX = centerX" to:self.contentView];
//        [self.button constrain:@"right = right - 7.5"
//                            to:self.contentView];
    }
    return self;
}

@end
