//
//  SignatureCaptureViewController.m
//  mPOS
//
//  Created by Antonio Strijdom on 13/02/2015.
//  Copyright (c) 2015 Clarity. All rights reserved.
//

#import "SignatureCaptureViewController.h"
#import "CRSGlossyButton.h"
#import "CRSSkinning.h"
#import "T1Autograph.h"
#import "POSFunc.h"
#import "POSFuncController.h"
#import "UIImage+OCGExtensions.h"

typedef NS_ENUM(NSUInteger, SignatureCaptureStep) {
    kSignatureCaptureStepStart = 0,
    kSignatureCaptureStepSign = 1,
    kSignatureCaptureStepConfirm = 2
};

@interface SignatureCaptureViewController () <T1AutographDelegate> {
    BOOL _setup;
}
@property (nonatomic, readonly) SoftKey *yesKey;
@property (nonatomic, readonly) SoftKey *noKey;
@property (nonatomic, assign) SignatureCaptureStep currentStep;
@property (nonatomic, strong) UIView *contentView;
@property (nonatomic, retain) UIImage *signatureImage;
@property (nonatomic, strong) UILabel *instructionLabel;
@property (nonatomic, strong) UIView *signatureView;
@property (nonatomic, strong) UIView *buttonView;
@property (nonatomic, strong) T1Autograph *autograph;
@property (nonatomic, strong) CRSGlossyButton *yesButton;
@property (nonatomic, strong) CRSGlossyButton *noButton;
- (void) updateConstraints;
- (void) updateInstructions;
- (void) nextStep;
- (void) updateUIForCurrentStep;
- (void) executeSoftkeyPOSFunc:(SoftKey *)softkey;
- (IBAction) yesButtonTapped:(id)sender;
- (IBAction) noButtonTapped:(id)sender;
@end

@implementation SignatureCaptureViewController

#pragma mark - Private

- (void) updateConstraints
{
    DebugLog(@"updateConstraints");
    
    if (!_setup) return;
    
    UIUserInterfaceIdiom idiom = (self.traitCollection != nil ? self.traitCollection.userInterfaceIdiom : UI_USER_INTERFACE_IDIOM());
    
    if (UIUserInterfaceIdiomUnspecified == idiom) {
        idiom = UI_USER_INTERFACE_IDIOM();
    }
    
    
    if (UIUserInterfaceIdiomPhone == idiom) {
        DebugLog(@"updateConstraints - phone");
        
        self.contentView.transform = CGAffineTransformMakeRotation(-90.0f / 180.0f * M_PI);
        
        [self.contentView constrain:@"width = height" to:self.view];
        [self.contentView constrain:@"height = width" to:self.view];
    }
    else
    {
        [self.contentView constrain:@"width = width" to:self.view];
        [self.contentView constrain:@"height = height" to:self.view];
    }


    {
        DebugLog(@"updateConstraints - other");
        
        // buttons
        [self.contentView constrain:@"centerX = centerX" to:self.view];
        [self.contentView constrain:@"centerY = centerY" to:self.view];
        
        [self.buttonView addSubview:self.instructionLabel];
        // build horizontal constraints
        // signature
        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[signatureView]-0-|"
                                                                          options:0
                                                                          metrics:nil
                                                                            views:@{ @"signatureView" : self.signatureView }]];
        // buttons
        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[buttonView]-0-|"
                                                                          options:0
                                                                          metrics:nil
                                                                            views:@{ @"buttonView" : self.buttonView }]];
        // build vertical constraints
        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[signatureView]-0-[buttonView(>=44)]-0-|"
                                                                          options:0
                                                                          metrics:nil
                                                                            views:@{ @"topGuide" : self.topLayoutGuide,
                                                                                     @"signatureView" : self.signatureView,
                                                                                     @"buttonView" : self.buttonView }]];
        // button constraints
        // horizontal
        [self.buttonView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[no(>=130)]-0-[instructionLabel]-0-[yes(==no)]-0-|"
                                                                                options:0
                                                                                metrics:nil
                                                                                  views:@{ @"yes" : self.yesButton,
                                                                                           @"instructionLabel" : self.instructionLabel,
                                                                                           @"no" : self.noButton }]];
        // vertical
        [self.buttonView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[no]-0-|"
                                                                                options:0
                                                                                metrics:nil
                                                                                  views:@{ @"no" : self.noButton }]];
        [self.buttonView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[instructionLabel]-0-|"
                                                                                options:0
                                                                                metrics:nil
                                                                                  views:@{ @"instructionLabel" : self.instructionLabel }]];
        [self.buttonView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[yes]-0-|"
                                                                                options:0
                                                                                metrics:nil
                                                                                  views:@{ @"yes" : self.yesButton }]];
    }
    
    _setup = NO;
}
- (void) updateInstructions
{
    [self.yesButton setNeedsDisplay];
    [self.noButton setNeedsDisplay];
}

- (void) nextStep
{
    switch (self.currentStep) {
        case kSignatureCaptureStepStart:
            self.currentStep = kSignatureCaptureStepSign;
            break;
        case kSignatureCaptureStepSign:
            self.currentStep = kSignatureCaptureStepConfirm;
            break;
        default:
            break;
    }
    [UIView animateWithDuration:0.3
                     animations:^{
                         [self updateUIForCurrentStep];
                     }];
}

- (void) updateUIForCurrentStep
{
    switch (self.currentStep) {
        case kSignatureCaptureStepStart: {
            self.instructionLabel.text = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁶󠁮󠁱󠁭󠁊󠁎󠁺󠁇󠁸󠁓󠁧󠁈󠁲󠁚󠀯󠁷󠀳󠀳󠀷󠁊󠁨󠁇󠁄󠁌󠁣󠁧󠀸󠁿*/ @"Tap the ready button and hand the device to the customer for them to provide their signature", nil);
            [self.yesButton setTitle:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁋󠁶󠁇󠁪󠁖󠀲󠁬󠀯󠁐󠁩󠁋󠁋󠁄󠁓󠁏󠁶󠁈󠁯󠁥󠁵󠁚󠀴󠁔󠁈󠁉󠁍󠀸󠁿*/ @"Ready", nil)
                            forState:UIControlStateNormal];
            [self.noButton setTitle:nil
                           forState:UIControlStateNormal];
            self.noButton.userInteractionEnabled = NO;
            self.noButton.hidden = YES;
            self.signatureView.userInteractionEnabled = NO;
            self.signatureView.hidden = YES;
            break;
        }
        case kSignatureCaptureStepSign: {
            [UIView animateWithDuration:0 animations:NULL completion:^(BOOL finished) {
                if (finished)
                {
                    self.autograph = [T1Autograph autographWithView:self.signatureView
                                                           delegate:self];
                    self.autograph.licenseCode = @"db58e0ae655f8624d7752d662e11b7f06a6f9d71";
                    self.autograph.strokeWidth = 3.0f;
                }
            }];
            
            self.instructionLabel.text = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁶󠁚󠁦󠀰󠁮󠀲󠁭󠁚󠁒󠁶󠁚󠁐󠁁󠁎󠁙󠀳󠁖󠁦󠀫󠁹󠁊󠀶󠁷󠁎󠁦󠁉󠁑󠁿*/ @"Please sign the screen and hand the device back to the sales associate. Tap the button marked clear to clear the signature.", nil);
            [self.yesButton setTitle:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁃󠀲󠁕󠁲󠁤󠀲󠁯󠁮󠁊󠀸󠁘󠁓󠁌󠁕󠁆󠁨󠁲󠁤󠁅󠁣󠁃󠁯󠁙󠁵󠁂󠀹󠁁󠁿*/ @"Done", nil)
                            forState:UIControlStateNormal];
            [self.noButton setTitle:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁩󠁭󠀯󠁱󠁃󠁍󠁹󠁑󠁒󠀸󠁙󠁘󠀲󠁣󠀸󠁨󠀸󠁕󠁌󠁕󠁵󠁔󠁸󠀵󠁑󠁍󠁳󠁿*/ @"Clear", nil)
                           forState:UIControlStateNormal];
            self.noButton.userInteractionEnabled = YES;
            self.noButton.hidden = NO;
            self.signatureView.userInteractionEnabled = YES;
            self.signatureView.hidden = NO;
            break;
        }
        case kSignatureCaptureStepConfirm: {
            self.instructionLabel.text = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁣󠀳󠀱󠀫󠁗󠁪󠁪󠁄󠁴󠁤󠁂󠁩󠀳󠁲󠀱󠀴󠁅󠁘󠁰󠁬󠁣󠁎󠁕󠁯󠁊󠁮󠁙󠁿*/ @"Do the signatures match?", nil);
            [self.yesButton setTitle:self.yesKey.keyDescription
                            forState:UIControlStateNormal];
            [self.yesButton setImage:[UIImage OCGExtensions_imageNamed:self.yesKey.posFunc]
                            forState:UIControlStateNormal];
            [self.noButton setTitle:self.noKey.keyDescription
                           forState:UIControlStateNormal];
            [self.noButton setImage:[UIImage OCGExtensions_imageNamed:self.noKey.posFunc]
                           forState:UIControlStateNormal];
            self.noButton.userInteractionEnabled = YES;
            self.noButton.hidden = NO;
            self.signatureView.userInteractionEnabled = NO;
            self.signatureView.hidden = NO;
            break;
        }
        default:
            break;
    }
    [self updateInstructions];
}

- (void) executeSoftkeyPOSFunc:(SoftKey *)softkey
{
    switch (POSFuncForString(softkey.posFunc)) {
        case POSFuncEftConfirmSignature:
            [[POSFuncController sharedInstance] eftConfirmSignature:self.signatureImage];
            break;
        case POSFuncEftUnconfirmSignature:
            [[POSFuncController sharedInstance] eftUnConfirmSignature:self.signatureImage];
            break;
        default:
            break;
    }
}

#pragma mark Actions

- (IBAction) yesButtonTapped:(id)sender
{
    switch (self.currentStep) {
        case kSignatureCaptureStepStart: {
            [self nextStep];
            break;
        }
        case kSignatureCaptureStepSign: {
            [self.autograph done:sender];
            break;
        }
        case kSignatureCaptureStepConfirm: {
            if (self.yesKey) {
                [self executeSoftkeyPOSFunc:self.yesKey];
            }
            break;
        }
        default:
            break;
    }
}

- (IBAction) noButtonTapped:(id)sender
{
    switch (self.currentStep) {
        case kSignatureCaptureStepSign:
            [self.autograph reset:sender];
            break;
        case kSignatureCaptureStepConfirm: {
            if (self.noKey) {
                [self executeSoftkeyPOSFunc:self.noKey];
            }
            break;
        }
        default:
            break;
    }
}

#pragma mark - Properties

@synthesize signatureContext = _signatureContext;
- (void) setSignatureContext:(SoftKeyContext *)signatureContext
{
    _signatureContext = signatureContext;
    _yesKey = nil;
    _noKey = nil;
}

@synthesize yesKey = _yesKey;
- (SoftKey *) yesKey
{
    if (nil == _yesKey) {
        if (self.signatureContext) {
            NSArray *keys = [self.signatureContext.rootMenu componentsSeparatedByString:@" "];
            if (keys.count > 0) {
                NSString *keyId = keys[0];
                if (keyId.length > 0) {
                    _yesKey =  [BasketController.sharedInstance softKeyForKeyId:keyId softKeyContext:self.signatureContext];
                }
            }
        }
    }
    
    return _yesKey;
}

@synthesize noKey = _noKey;
- (SoftKey *) noKey
{
    if (nil == _noKey) {
        if (self.signatureContext) {
            NSArray *keys = [self.signatureContext.rootMenu componentsSeparatedByString:@" "];
            if (keys.count > 1) {
                NSString *keyId = keys[1];
                if (keyId.length > 0) {
                    _noKey = [BasketController.sharedInstance softKeyForKeyId:keyId softKeyContext:self.signatureContext];
                }
            }
        }
    }
    
    return _noKey;
}

#pragma mark - UIViewController

- (void) viewDidLoad
{
    [super viewDidLoad];
    
    self.contentView = [[UIView alloc] init];
    self.contentView.translatesAutoresizingMaskIntoConstraints = NO;
    [self.view addSubview:self.contentView];

    self.view.backgroundColor = [UIColor whiteColor];
    
    // build instruction label
    self.instructionLabel = [[UILabel alloc] init];
    self.instructionLabel.autoresizingMask = UIViewAutoresizingNone;
    self.instructionLabel.translatesAutoresizingMaskIntoConstraints = NO;
    self.instructionLabel.tag = kBasketMenuButtonSkinningTag;
    self.instructionLabel.numberOfLines = 2;
    self.instructionLabel.minimumScaleFactor = 0.5;
    self.instructionLabel.font = [UIFont preferredFontForTextStyle:UIFontTextStyleCaption1];
    self.instructionLabel.backgroundColor = [UIColor clearColor];
    self.instructionLabel.textAlignment = NSTextAlignmentCenter;
    // build signature capture view
    self.signatureView = [[UIView alloc] initWithFrame:CGRectZero];
    self.signatureView.translatesAutoresizingMaskIntoConstraints = NO;
    [self.contentView addSubview:self.signatureView];
    // build buttons
    // container
    self.buttonView = [[UIView alloc] init];
    self.buttonView.translatesAutoresizingMaskIntoConstraints = NO;
    self.buttonView.clipsToBounds = YES;
    self.buttonView.tag = kMenuSkinningTag;
    [self.contentView addSubview:self.buttonView];
    // yes
    self.yesButton = [[CRSGlossyButton alloc] init];
    self.yesButton.translatesAutoresizingMaskIntoConstraints = NO;
    self.yesButton.cornerRadius = 0.0f;
    self.yesButton.tag = kPrimaryButtonSkinningTag;
    [self.yesButton addTarget:self
                       action:@selector(yesButtonTapped:)
             forControlEvents:UIControlEventTouchUpInside];
    [self.buttonView addSubview:self.yesButton];
    // no
    self.noButton = [[CRSGlossyButton alloc] init];
    self.noButton.translatesAutoresizingMaskIntoConstraints = NO;
    self.noButton.cornerRadius = 0.0f;
    self.noButton.tag = kSecondaryButtonSkinningTag;
    [self.noButton addTarget:self
                      action:@selector(noButtonTapped:)
            forControlEvents:UIControlEventTouchUpInside];
    [self.buttonView addSubview:self.noButton];
    
    _setup = YES;
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void) viewDidAppear:(BOOL)animated
{
    [self updateConstraints];
    [self updateUIForCurrentStep];
    [self setStatusBarHiddenIfNeeded:YES];
    [[CRSSkinning currentSkin] applyViewSkin:self.instructionLabel
                             withTagOverride:self.instructionLabel.tag];
    [[CRSSkinning currentSkin] applyViewSkin:self.buttonView
                             withTagOverride:self.buttonView.tag];
    [super viewDidAppear:animated];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self setStatusBarHiddenIfNeeded:NO];
}

- (void)setStatusBarHiddenIfNeeded:(BOOL)hidden
{
    UIUserInterfaceIdiom idiom = (self.traitCollection != nil ? self.traitCollection.userInterfaceIdiom : UI_USER_INTERFACE_IDIOM());
    
    if (UIUserInterfaceIdiomUnspecified == idiom)
    {
        idiom = UI_USER_INTERFACE_IDIOM();
    }
    
    
    if (UIUserInterfaceIdiomPhone == idiom) {
        [UIApplication.sharedApplication setStatusBarHidden:hidden
                                              withAnimation:UIStatusBarAnimationNone];
    }

}

#pragma mark - T1AutographDelegate

- (void)       autograph:(T1Autograph *)autograph
didCompleteWithSignature:(T1Signature *)signature
{
    // signature was successful
    self.signatureImage = [signature imageView].image;
    [self nextStep];
}

- (void) autographDidCompleteWithNoData
{
    // user pressed the done button without signing
    [[[UIAlertView alloc] initWithTitle:@"Error"
                                message:@"Please provide a signature before tapping done"
                               delegate:nil
                      cancelButtonTitle:@"OK"
                      otherButtonTitles:nil] show];
}

@end
