//
//  OCGEFTProviderFakeIntegrated.h
//  mPOS
//
//  Created by Antonio Strijdom on 04/02/2015.
//  Copyright (c) 2015 Clarity. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "OCGEFTManager.h"

@interface OCGEFTProviderFakeIntegrated : NSObject <OCGEFTProviderProtocol>

@end
