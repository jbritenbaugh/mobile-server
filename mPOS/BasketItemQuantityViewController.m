//
//  BasketItemQuantityViewController.m
//  mPOS
//
//  Created by Meik Schuetz on 16/08/2012.
//  Copyright (c) 2012 Clarity. All rights reserved.
//

#import "BasketItemQuantityViewController.h"
#import "CRSEditableTableField.h"
#import "CRSEditableTableFieldCell.h"
#import "RESTController.h"
#import "BasketController.h"
#import "CRSSkinning.h"
#import "ContextViewController.h"

#define kButtonIndexConfirm                 1
#define kButtonIndexCancel                  0

@interface BasketItemQuantityViewController () <CRSEditableTableFieldCellDelegate>
@property (atomic) BOOL updateBasketItem;
@property (strong, nonatomic) CRSEditableTableField *quantityField;
@property (strong, nonatomic) CRSEditableTableFieldCell *quantityFieldCell;
- (BOOL) validateQuantityField;
- (void) updateMaxItemQuantity;
- (IBAction) cancelTapped:(id)sender;
- (IBAction) changeTapped:(id)sender;
@end

@implementation BasketItemQuantityViewController

#pragma mark - Private

- (void) updateMaxItemQuantity
{
    // MPOS-367 Get the maximum quantity
    NSString *maxQty = BasketController.sharedInstance.clientSettings.maxItemQuantity;
    if (maxQty != nil) {
        self.quantityField.maximumValue = maxQty.integerValue;
    } else {
        self.quantityField.maximumValue = 999;
    }
}

/** @brief Called to validate the user input
 *  @param showAlert If YES an alert is presented for the validation error.
 *  @return YES, if the user input is valid, NO otherwise.
 */
- (BOOL) validateQuantityField
{
    int quantity = [self.quantityField.value intValue];
    NSString *validationMessage = nil;
    
    if ((self.quantityField.hasMinimumValue) && (self.quantityField.minimumValue > quantity)) {
        
        validationMessage = [NSString stringWithFormat: NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁱󠁈󠀷󠀷󠁶󠀴󠀰󠀹󠁗󠁗󠁔󠀯󠁺󠀴󠀯󠀹󠀷󠁫󠁨󠀷󠁐󠁥󠁋󠁂󠁤󠁃󠁅󠁿*/ @"Please enter a minimum quantity of %@.", nil),
                             [CRSEditableTableField formattedStringFromFloat: self.quantityField.minimumValue decimalDigits: 0]];
        
    }
    else if ((self.quantityField.hasMaximumValue) && (self.quantityField.maximumValue < quantity)) {
        
        validationMessage = [NSString stringWithFormat: NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁲󠀴󠁁󠁴󠁰󠁔󠀳󠁐󠁴󠀵󠁳󠁚󠁳󠁕󠁆󠁈󠁂󠁌󠀫󠁥󠀶󠁧󠁃󠀲󠀰󠀯󠁅󠁿*/ @"Please enter a maximum quantity of %@.", nil),
                             [CRSEditableTableField formattedStringFromFloat: self.quantityField.minimumValue decimalDigits: 0]];
        
    }
    
    if (validationMessage != nil) {
        [[[UIAlertView alloc] initWithTitle: nil
                                    message: validationMessage
                                   delegate: nil
                          cancelButtonTitle: NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁡󠁩󠁡󠁍󠁰󠀸󠁷󠁣󠁹󠁩󠁕󠀷󠁪󠁶󠀱󠀸󠁐󠁖󠀵󠁥󠀳󠁫󠁕󠁚󠁄󠁶󠁙󠁿*/ @"Dismiss", nil)
                          otherButtonTitles: nil] show];
        
        return NO;
    }
    
    return YES;
}

#pragma mark - UIViewController

/** @brief Called after the controller’s view is loaded into memory.
 */
- (void) viewDidLoad
{
    [super viewDidLoad];
    
    // apply skin
    self.tableView.tag = kEditableTableSkinningTag;
}

- (void) viewWillAppear:(BOOL)animated
{
    // set the view's title
    
    self.title = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁑󠁄󠁏󠁴󠁩󠁔󠁈󠀫󠁌󠁵󠁔󠁆󠁧󠁤󠀸󠁭󠀸󠁱󠀷󠁢󠁓󠁣󠁎󠁡󠁯󠁇󠁙󠁿*/ @"Quantity", nil);
    
    // initialize the quantity field and cell objects.
    
    self.quantityField = [CRSEditableTableField editableTableFieldWithInteger: 0
                                                                      caption: NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁆󠁱󠁁󠁭󠁌󠁕󠁧󠁩󠁪󠁥󠁁󠁙󠀰󠁡󠁺󠁯󠁣󠁍󠁱󠁍󠀱󠁋󠀶󠁱󠁎󠁣󠁧󠁿*/ @"New quantity", nil)];
    self.quantityField.minimumValue = 1;
    
    // create the cell that allows the user to edit the quantity
    
    self.quantityFieldCell = [CRSEditableTableFieldCell cellViewForField: self.quantityField
                                                               tableView: self.tableView];
    self.quantityFieldCell.delegate = self;
    self.quantityFieldCell.tag = kEditableTableCellSkinningTag;
    self.quantityFieldCell.textLabel.tag = kEditableCellKeySkinningTag;
    self.quantityFieldCell.label.tag = kEditableCellKeySkinningTag;
    self.quantityFieldCell.label.textAlignment = NSTextAlignmentLeft;
    self.quantityFieldCell.textField.tag = kEditableCellValueSkinningTag;
    
    self.quantityFieldCell.textField.clearButtonMode = UITextFieldViewModeNever;
    
    // set the navigation buttons
    // cancel
    self.navigationItem.leftBarButtonItem =
    [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁩󠁢󠁺󠁬󠁏󠁶󠁣󠁧󠁫󠁍󠁋󠁧󠁫󠀱󠁶󠁐󠁐󠀹󠁘󠀴󠁘󠁡󠁕󠁭󠁹󠁷󠀰󠁿*/ @"Cancel", nil)
                                     style:UIBarButtonItemStylePlain
                                    target:self
                                    action:@selector(cancelTapped:)];
    self.navigationItem.leftBarButtonItem.tag = kSecondaryButtonSkinningTag;
    // change
    self.navigationItem.rightBarButtonItem =
    [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁘󠁙󠁙󠀫󠁥󠁚󠁗󠁸󠁗󠁏󠁯󠁰󠁘󠀱󠁫󠀶󠁑󠁉󠁨󠀷󠁉󠁂󠁴󠁘󠀰󠁷󠀴󠁿*/ @"Change", nil)
                                     style:UIBarButtonItemStyleDone
                                    target:self
                                    action:@selector(changeTapped:)];
    self.navigationItem.rightBarButtonItem.tag = kPrimaryButtonSkinningTag;


    [[CRSSkinning currentSkin] applyViewSkin:self];
    [[CRSSkinning currentSkin] applyCellSkin:self.quantityFieldCell
                                    forStyle:self.tableView.style];
    
    [super viewWillAppear:animated];
    self.quantityFieldCell.textField.placeholder = self.basketItem.quantity;
    self.quantityFieldCell.textField.text = nil;
    
    [self updateMaxItemQuantity];
    
    NSNotificationCenter *defaultCenter = [NSNotificationCenter defaultCenter];
    
    [defaultCenter addObserver: self
                      selector: @selector(updateMaxItemQuantity)
                          name: kBasketControllerConfigurationDidChangeNotification
                        object: NULL];    
}

/** @brief Notifies the view controller that its view was added to a view hierarchy.
 *  @param animated If YES, the view was added to the window using an animation.
 */
- (void) viewDidAppear:(BOOL)animated
{
    [super viewDidAppear: animated];
    
    // initialize attributes
    
    self.updateBasketItem = NO;
    
    // make sure that we enter directly into edit mode.
    
    [self.quantityFieldCell setEditMode: YES];
    [self.quantityFieldCell becomeFirstResponder];
    [self.tableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]
                                animated:YES
                          scrollPosition:UITableViewScrollPositionNone];
}

/** @brief Notifies the view controller that its view was removed from a view hierarchy.
 *  @param animated If YES, the disappearance of the view was animated.
 */
- (void) viewDidDisappear:(BOOL)animated
{
    // call super
    
    [super viewDidDisappear: animated];
    
    // update the basket item's quantity.
    
    if (self.updateBasketItem) {
            
        [[BasketController sharedInstance] changeQuantityOfItem: self.basketItem
                                                     toQuantity: [self.quantityField.value intValue]];
        
    }
    
    NSNotificationCenter *defaultCenter = [NSNotificationCenter defaultCenter];
    [defaultCenter removeObserver:self];
}

/** @brief Called when the controller’s view is released from memory.
 */
- (void) viewDidUnload
{
    [super viewDidUnload];

    self.tableView.delegate = nil;
    self.tableView.dataSource = nil;
}

#pragma mark - UITableViewDataSource

- (NSInteger) numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

- (UITableViewCell *) tableView:(UITableView *)tableView
          cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return self.quantityFieldCell;
}

- (void) tableView:(UITableView *)tableView
   willDisplayCell:(UITableViewCell *)cell
 forRowAtIndexPath:(NSIndexPath *)indexPath
{
    [[CRSSkinning currentSkin] applyCellSkin:cell
                                    forStyle:tableView.style];
    [self.quantityFieldCell setEditMode: YES];
    [self.quantityFieldCell becomeFirstResponder];
}

#pragma mark - CRSEditableTableFieldCellDelegate

- (void) assignNextResponderInTableView:(UITableView *)tableView
                                forCell:(CRSEditableTableFieldCell *)fieldCell
{
    [fieldCell.textField resignFirstResponder];
    [self changeTapped:nil];
}

#pragma mark - Actions

- (IBAction) cancelTapped:(id)sender
{
    [[ContextViewController sharedInstance] dismissViewControllerAnimated:YES
                                                               completion:nil];
}

- (IBAction) changeTapped:(id)sender
{
    if ([self validateQuantityField]) {
        self.updateBasketItem = YES;
        [[ContextViewController sharedInstance] dismissViewControllerAnimated:YES
                                                                   completion:nil];
    } else {
        [self.quantityFieldCell setEditMode:YES];
        [self.quantityFieldCell becomeFirstResponder];
    }
}

@end
