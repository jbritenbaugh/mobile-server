//
//  HistoryTableViewCell.h
//  mPOS
//
//  Created by Antonio Strijdom on 21/11/2012.
//  Copyright (c) 2012 Clarity. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HistoryTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *basketReferenceLabel;
@property (weak, nonatomic) IBOutlet UILabel *basketLocationLabel;
@property (weak, nonatomic) IBOutlet UILabel *basketAmountLabel;
@property (weak, nonatomic) IBOutlet UILabel *basketTimeLabel;

@end
