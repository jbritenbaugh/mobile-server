//
//  AppDelegate.h
//  mPOS
//
//  Created by Antonio Strijdom on 24/07/2012.
//  Copyright (c) 2012 Clarity. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SettingsTableViewController.h"

/** @brief Posted when the settings view is presented */
extern NSString * const kAppDelegateSettingsPresentedNotification;
/** @brief Posted when the settings view is dismissed */
extern NSString * const kAppDelegateSettingsDismissedNotification;

@interface AppDelegate : UIResponder <UIApplicationDelegate, SettingsTableViewControllerDelegate>

@property (strong, nonatomic) UIWindow *window;

/** @brief Called to present the settings view modally.
 */
- (void) presentSettingsViewAnimated:(BOOL)animated viewController:(UIViewController*)viewController;

@end
