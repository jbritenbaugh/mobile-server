//
//  EFTDeviceSelectionViewController.m
//  mPOS
//
//  Created by Antonio Strijdom on 28/01/2015.
//  Copyright (c) 2015 Clarity. All rights reserved.
//

#import "DeviceSelectionViewController.h"
#import "Device+MPOSExtensions.h"
#import "UIImage+OCGExtensions.h"
#import "POSFunc.h"
#import "OCGTableViewCell.h"
#import "BarcodeScannerController.h"
#import "CRSSkinning.h"

@interface EFTDeviceTableViewCell : UITableViewCell
@property (nonatomic, strong) UILabel *deviceNameLabel;
@property (nonatomic, strong) UILabel *deviceStatusLabel;
@end

@implementation EFTDeviceTableViewCell

CGFloat const kStatusLabelWidth = 120.0f;

- (instancetype) initWithStyle:(UITableViewCellStyle)style
               reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // name
        self.deviceNameLabel = [[UILabel alloc] init];
        self.deviceNameLabel.translatesAutoresizingMaskIntoConstraints = NO;
        self.deviceNameLabel.numberOfLines = 1;
        self.deviceNameLabel.minimumScaleFactor = 0.5;
        self.deviceNameLabel.font = [UIFont preferredFontForTextStyle:UIFontTextStyleBody];
        self.deviceNameLabel.textColor = [UIColor darkTextColor];
        self.deviceNameLabel.backgroundColor = [UIColor clearColor];
        self.deviceNameLabel.textAlignment = NSTextAlignmentLeft;
        [self.contentView addSubview:self.deviceNameLabel];
        [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:self.deviceNameLabel
                                                                     attribute:NSLayoutAttributeCenterY
                                                                     relatedBy:NSLayoutRelationEqual
                                                                        toItem:self.contentView
                                                                     attribute:NSLayoutAttributeCenterY
                                                                    multiplier:1.0f
                                                                      constant:0.0f]];
        [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:self.deviceNameLabel
                                                                     attribute:NSLayoutAttributeHeight
                                                                     relatedBy:NSLayoutRelationEqual
                                                                        toItem:self.contentView
                                                                     attribute:NSLayoutAttributeHeight
                                                                    multiplier:1.0f
                                                                      constant:0.0f]];
        [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:self.deviceNameLabel
                                                                     attribute:NSLayoutAttributeHeight
                                                                     relatedBy:NSLayoutRelationGreaterThanOrEqual
                                                                        toItem:nil
                                                                     attribute:NSLayoutAttributeNotAnAttribute
                                                                    multiplier:1.0f
                                                                      constant:44.0f]];
        [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:self.deviceNameLabel
                                                                     attribute:NSLayoutAttributeLeft
                                                                     relatedBy:NSLayoutRelationEqual
                                                                        toItem:self.contentView
                                                                     attribute:NSLayoutAttributeLeft
                                                                    multiplier:1.0f
                                                                      constant:5.0f]];
        [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:self.deviceNameLabel
                                                                     attribute:NSLayoutAttributeWidth
                                                                     relatedBy:NSLayoutRelationEqual
                                                                        toItem:self.contentView
                                                                     attribute:NSLayoutAttributeWidth
                                                                    multiplier:1.0f
                                                                      constant:kStatusLabelWidth * -1.0f]];
        // status
        self.deviceStatusLabel = [[UILabel alloc] init];
        self.deviceStatusLabel.translatesAutoresizingMaskIntoConstraints = NO;
        self.deviceStatusLabel.numberOfLines = 1;
        self.deviceStatusLabel.minimumScaleFactor = 0.0f;
        self.deviceStatusLabel.font = [UIFont preferredFontForTextStyle:UIFontTextStyleFootnote];
        self.deviceStatusLabel.textColor = [UIColor lightGrayColor];
        self.deviceStatusLabel.backgroundColor = [UIColor clearColor];
        self.deviceStatusLabel.textAlignment = NSTextAlignmentCenter;
        [self.contentView addSubview:self.deviceStatusLabel];
        [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:self.deviceStatusLabel
                                                                     attribute:NSLayoutAttributeCenterY
                                                                     relatedBy:NSLayoutRelationEqual
                                                                        toItem:self.deviceNameLabel
                                                                     attribute:NSLayoutAttributeCenterY
                                                                    multiplier:1.0f
                                                                      constant:0.0f]];
        [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:self.deviceStatusLabel
                                                                     attribute:NSLayoutAttributeHeight
                                                                     relatedBy:NSLayoutRelationEqual
                                                                        toItem:self.deviceNameLabel
                                                                     attribute:NSLayoutAttributeHeight
                                                                    multiplier:1.0f
                                                                      constant:0.0f]];
        [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:self.deviceStatusLabel
                                                                     attribute:NSLayoutAttributeRight
                                                                     relatedBy:NSLayoutRelationEqual
                                                                        toItem:self.contentView
                                                                     attribute:NSLayoutAttributeRight
                                                                    multiplier:1.0f
                                                                      constant:5.0f]];
        [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:self.deviceStatusLabel
                                                                     attribute:NSLayoutAttributeWidth
                                                                     relatedBy:NSLayoutRelationEqual
                                                                        toItem:nil
                                                                     attribute:NSLayoutAttributeNotAnAttribute
                                                                    multiplier:1.0f
                                                                      constant:kStatusLabelWidth]];
    }
    return self;
}

- (void) prepareForReuse
{
    self.detailTextLabel.text = nil;
    self.deviceStatusLabel.text = nil;
    [super prepareForReuse];
}

@end

@interface DeviceSelectionViewController () {
    NSUInteger _sectionCount;
    NSUInteger _scanSectionIndex;
    NSUInteger _availableSectionIndex;
    NSUInteger _unavailableSectionIndex;
    NSString *_deviceBarcode;
}
@property (nonatomic, readonly) NSString *usageText;
@property (nonatomic, readonly) Device *deviceClass;
@property (nonatomic, strong) NSArray *deviceList;
- (BOOL) deviceInitialized:(id)device;
- (NSString *)deviceStatus:(id)device;
- (NSString *)deviceName:(id)device;
- (BOOL) deviceSelected:(id)device;
- (id) deviceForIndexPath:(NSIndexPath *)indexPath;
- (void) startScanningForBarcode;
- (IBAction) refreshTapped:(id)sender;
- (IBAction) cancelTapped:(id)sender;
@end

@implementation DeviceSelectionViewController

#pragma mark - Private

static NSString * const kScanCellReuseId = @"ScanCellReuseId";
static NSString * const kDeviceCellReuseId = @"EFTDeviceCellReuseId";

- (BOOL) deviceInitialized:(id)device
{
    BOOL result = NO;
    
    if (device) {
        if (self.dataSource) {
            if ([self.dataSource respondsToSelector:@selector(deviceSelectionController:getStatus:ofDevice:)]) {
                NSString *status = nil;
                result = [self.dataSource deviceSelectionController:self
                                                          getStatus:&status
                                                           ofDevice:device];
            }
        }
    }
    
    return result;
}

- (NSString *)deviceStatus:(id)device
{
    NSString *result = @"?";
    
    if (device) {
        if (self.dataSource) {
            if ([self.dataSource respondsToSelector:@selector(deviceSelectionController:getStatus:ofDevice:)]) {
                [self.dataSource deviceSelectionController:self
                                                 getStatus:&result
                                                  ofDevice:device];
            }
        }
    }
    
    return result;
}

- (NSString *)deviceName:(id)device
{
    NSString *result = @"?";
    
    if (device) {
        if (self.dataSource) {
            if ([self.dataSource respondsToSelector:@selector(deviceSelectionController:nameOfDevice:)]) {
                result = [self.dataSource deviceSelectionController:self
                                                       nameOfDevice:device];
            }
        }
    }
    
    return result;
}

- (BOOL) deviceSelected:(id)device
{
    BOOL result = NO;
    
    if (device) {
        if (self.deviceCurrentlySelectedBlock) {
            result = self.deviceCurrentlySelectedBlock(device);
        }
    }
    
    return result;
}

- (id) deviceForIndexPath:(NSIndexPath *)indexPath
{
    id result = nil;
    NSInteger index = -1;
    
    for (id device in self.deviceList) {
        if ((indexPath.section == _availableSectionIndex) && ([self deviceInitialized:device])) index++;
        if ((indexPath.section == _unavailableSectionIndex) && (![self deviceInitialized:device])) index++;
        if (indexPath.row == index) {
            result = device;
            break;
        }
    }
    
    return result;
}

- (void) startScanningForBarcode
{
    BarcodeScannerController *controller = [BarcodeScannerController sharedInstance];
    // are we scanning?
    if (!controller.scanning) {
        // no, start scanning
        [controller beginScanningInViewController:self
                                        forReason:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁫󠁨󠁪󠁁󠁇󠁶󠁊󠁔󠁍󠀸󠁯󠁨󠁷󠁘󠀲󠁏󠁘󠁺󠁡󠁮󠁌󠁡󠀫󠁗󠁫󠁯󠀴󠁿*/ @"Device", nil)
                                   withButtonSize:CGSizeZero
                                     withAddTitle:nil
                                         mustScan:NO
                              alphaNumericAllowed:YES
                                        withBlock:^(NSString *barcode, ScannerEntryMethod entryMethod, SMBarcodeType barcodeType, BOOL *continueScanning) {
                                            // debug assertions
                                            NSAssert(barcode != nil,
                                                     @"The reference to the scanned barcode has not been initialized.");
                                            NSAssert([[barcode stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length] > 0,
                                                     @"The returned barcode does not contain any characters.");
                                            // we only want to scan the one barcode
                                            *continueScanning = NO;
                                            // grab the barcode we scanned
                                            _deviceBarcode =
                                            [barcode stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                                        }
                                       terminated:^{
                                           if (_deviceBarcode != nil) {
                                               // process the barcode
                                               if (self.delegate) {
                                                   [self.delegate deviceSelectionViewController:self
                                                                                  didScanDevice:_deviceBarcode];
                                               }
                                               _deviceBarcode = nil;
                                           }
                                       }
                                            error:^(NSString *errorDesc) {
                                                if (errorDesc) {
                                                    // display an alert
                                                    UIAlertView *alert =
                                                    [[UIAlertView alloc] initWithTitle:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁯󠀫󠁋󠁆󠁢󠁚󠁱󠁩󠁈󠁕󠀲󠀸󠁫󠁲󠁧󠁦󠀲󠁴󠁘󠁣󠁈󠁎󠀶󠀲󠁯󠀶󠁳󠁿*/ @"Scanning Error", nil)
                                                                               message:errorDesc
                                                                              delegate:nil
                                                                     cancelButtonTitle:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁄󠁴󠁍󠁉󠁣󠀫󠁺󠁔󠁗󠁦󠁹󠁡󠁯󠀷󠁨󠁔󠁩󠀸󠁯󠀫󠁨󠁘󠀹󠁱󠁌󠀶󠀸󠁿*/ @"OK", nil)
                                                                     otherButtonTitles:nil];
                                                    [alert show];
                                                }
                                            }];
    }
}

- (IBAction) refreshTapped:(id)sender
{
    if (self.dataSource) {
        [self.dataSource deviceSelectionController:self
                            refreshDevicesComplete:^(NSArray *devices) {
                                self.deviceList = devices;
                                [self.refreshControl endRefreshing];
                                [self.tableView reloadData];
                            }
                                    currentDevices:self.deviceList];
    }
}

- (IBAction) cancelTapped:(id)sender
{
    if ([BarcodeScannerController sharedInstance].scanning) {
        [[BarcodeScannerController sharedInstance] stopScanning];
    }
    if (self.delegate) {
        [self.delegate deviceSelectionViewController:self
                                     didSelectDevice:nil];
    }
}

#pragma mark - Properties

- (NSString *) usageText
{
    NSString *result = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁫󠁨󠁪󠁁󠁇󠁶󠁊󠁔󠁍󠀸󠁯󠁨󠁷󠁘󠀲󠁏󠁘󠁺󠁡󠁮󠁌󠁡󠀫󠁗󠁫󠁯󠀴󠁿*/ @"Device", nil);
    
    if (self.dataSource) {
        if ([self.dataSource respondsToSelector:@selector(deviceSelectionControllerUsageText:)]) {
            result = [self.dataSource deviceSelectionControllerUsageText:self];
        }
    }
    
    return result;
}

- (Device *) deviceClass
{
    Device *result = nil;
    
    if (self.dataSource) {
        if ([self.dataSource respondsToSelector:@selector(deviceSelectionViewControllerDeviceClass:)]) {
            result = [self.dataSource deviceSelectionViewControllerDeviceClass:self];
        }
    }
    
    return result;
}

#pragma mark - UIViewController

- (void) viewDidLoad
{
    // add a header label
    UILabel *headerLabel = [[UILabel alloc] init];
    headerLabel.font = [UIFont preferredFontForTextStyle:UIFontTextStyleCaption1];
    headerLabel.backgroundColor = [UIColor clearColor];
    headerLabel.numberOfLines = 2;
    CGFloat height = 44.0f;
    CGRect headerFrame = self.tableView.frame;
    headerFrame.size.height = height;
    headerLabel.frame = headerFrame;
    self.tableView.tableHeaderView = headerLabel;
    // section indexes
    _sectionCount = 0;
    _scanSectionIndex = NSUIntegerMax;
    _availableSectionIndex = 0;
    _unavailableSectionIndex = 1;
    // if we are scanning
    if (self.deviceClass.scanToIdentify) {
        _sectionCount++;
        _scanSectionIndex = 0;
        _availableSectionIndex = 1;
        _unavailableSectionIndex = 2;
        // set the header label
        headerLabel.text =
        [NSString stringWithFormat:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠀳󠁂󠁂󠁪󠁡󠁗󠁋󠁳󠁁󠀷󠁡󠁪󠁮󠁨󠁧󠁏󠁬󠁺󠁐󠁏󠁺󠀯󠁃󠁓󠁆󠁱󠁧󠁿*/ @"Please scan a device to use for %@", nil), self.usageText];
        // if we are showing a list
        if (DeviceSelectionModeNone != self.deviceClass.selectionMode) {
            headerLabel.text = [headerLabel.text stringByAppendingString:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁍󠁒󠁳󠁧󠁆󠁕󠁊󠁄󠁺󠁆󠁙󠁐󠁎󠀰󠁑󠁢󠁅󠀸󠀸󠀹󠀲󠁏󠁸󠁴󠁐󠁴󠀰󠁿*/ @" or select one from the list below", nil)];
        }
    }
    // if we are showing a list
    if (DeviceSelectionModeNone != self.deviceClass.selectionMode) {
        _sectionCount+=2;
        // set the header label
        headerLabel.text =
        [NSString stringWithFormat:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠀫󠁸󠀯󠀯󠁭󠁓󠁧󠁅󠁮󠁏󠁅󠁔󠁮󠀵󠁈󠁔󠁃󠁏󠁤󠁪󠁅󠁥󠁸󠁳󠁓󠁦󠁳󠁿*/ @"Please select a device to use for %@ from the list below", nil), self.usageText];
        // add a refresh control
        UIRefreshControl *refreshControl = [[UIRefreshControl alloc] init];
        [refreshControl addTarget:self
                           action:@selector(refreshTapped:)
                 forControlEvents:UIControlEventValueChanged];
        self.refreshControl = refreshControl;
        // add a refresh button
        self.navigationItem.rightBarButtonItem =
        [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemRefresh
                                                      target:self
                                                      action:@selector(refreshTapped:)];
    }
    // setup the table view
    self.tableView.estimatedRowHeight = 44.0f;
    [self.tableView registerClass:[EFTDeviceTableViewCell class]
           forCellReuseIdentifier:kDeviceCellReuseId];
    [self.tableView registerClass:[OCGTableViewCell class]
           forCellReuseIdentifier:kScanCellReuseId];
    // add the cancel button
    self.navigationItem.leftBarButtonItem =
    [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel
                                                  target:self
                                                  action:@selector(cancelTapped:)];
    [super viewDidLoad];
}

- (void) viewWillAppear:(BOOL)animated
{
    [self refreshTapped:nil];
    [super viewWillAppear:animated];
}

- (void) viewDidAppear:(BOOL)animated
{
    if (self.deviceClass.scanToIdentify /* && (DeviceSelectionModeNone == self.deviceClass.selectionMode) */) {
        [self startScanningForBarcode];
    }
    [super viewDidAppear:animated];
}

#pragma mark - UITableViewDataSource

- (NSString *) tableView:(UITableView *)tableView
 titleForHeaderInSection:(NSInteger)section
{
    NSString *result = nil;
    
    if (section == _availableSectionIndex) {
        result = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁙󠀱󠁕󠁊󠁥󠁍󠁴󠁥󠁮󠁇󠁭󠁧󠁧󠀹󠀯󠁲󠁳󠁣󠀸󠁮󠁱󠁰󠀵󠁡󠁋󠁒󠁉󠁿*/ @"Available", nil);
    } else if (section == _unavailableSectionIndex) {
        result = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁃󠁉󠀴󠁸󠁵󠁕󠀶󠁒󠁈󠁮󠁫󠁸󠀶󠁉󠁰󠁢󠁪󠁭󠁬󠁑󠁍󠁤󠁑󠁺󠀹󠁘󠁑󠁿*/ @"Unavailable", nil);
    }
    
    return result;
}

- (NSInteger) numberOfSectionsInTableView:(UITableView *)tableView
{
    return _sectionCount;
}

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSInteger result = 0;
    
    if (section == _scanSectionIndex) {
        result++;
    } else {
        for (id device in self.deviceList) {
            if ((section == _availableSectionIndex) && ([self deviceInitialized:device])) result++;
            if ((section == _unavailableSectionIndex) && (![self deviceInitialized:device])) result++;
        }
    }
    
    return result;
}

- (UITableViewCell *) tableView:(UITableView *)tableView
          cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *result = nil;
    
    if (indexPath.section == _scanSectionIndex) {
        OCGTableViewCell *scanCell = [tableView dequeueReusableCellWithIdentifier:kScanCellReuseId];
        if (scanCell) {
            scanCell.textLabel.text = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁢󠁳󠁣󠁺󠁧󠁐󠁱󠁁󠁙󠁸󠀶󠁢󠁫󠁏󠀳󠁢󠁍󠁎󠁁󠁯󠁮󠀯󠁖󠀫󠁭󠁍󠁕󠁿*/ @"Scan Device", nil);
            scanCell.imageView.image = [UIImage OCGExtensions_imageNamed:NSStringForPOSFunc(POSFuncContextMenuItemScan)];
            scanCell.minimumImageViewWidth = 44.0f;
            result = scanCell;
        }
    } else {
        EFTDeviceTableViewCell *deviceCell = [tableView dequeueReusableCellWithIdentifier:kDeviceCellReuseId];
        if (deviceCell) {
            // find the device for this row
            id rowDevice = [self deviceForIndexPath:indexPath];
            // update the cell
            if (rowDevice) {
                deviceCell.deviceNameLabel.text = [self deviceName:rowDevice];
                deviceCell.deviceStatusLabel.text = [self deviceStatus:rowDevice];
                deviceCell.accessoryType = UITableViewCellAccessoryNone;
                if ([self deviceSelected:rowDevice]) {
                    deviceCell.accessoryType = UITableViewCellAccessoryCheckmark;
                }
            }
            result = deviceCell;
        }
    }
    // skin
    result.tag = kTableCellSkinningTag;
    result.textLabel.tag = kTableCellTextSkinningTag;
    result.detailTextLabel.tag = 0;
    
    return result;
}

- (void) tableView:(UITableView *)tableView
   willDisplayCell:(UITableViewCell *)cell
 forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == _scanSectionIndex) {
        [[CRSSkinning currentSkin] applyCellSkin:cell];
    }
}

#pragma mark - UITableViewDelegate

- (BOOL)            tableView:(UITableView *)tableView
shouldHighlightRowAtIndexPath:(NSIndexPath *)indexPath
{
    BOOL result = NO;
    
    if (indexPath.section == _scanSectionIndex) {
        result = YES;
    } else {
        // find the device for this row
        id rowDevice = [self deviceForIndexPath:indexPath];
        if ([self deviceInitialized:rowDevice]) {
            result = YES;
        }
    }
    
    return result;
}

- (void)      tableView:(UITableView *)tableView
didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == _scanSectionIndex) {
        [self startScanningForBarcode];
    } else {
        // find the device for this row
        id rowDevice = [self deviceForIndexPath:indexPath];
        if ([self deviceInitialized:rowDevice]) {
            if ([BarcodeScannerController sharedInstance].scanning) {
                [[BarcodeScannerController sharedInstance] stopScanning];
            }
            if (self.delegate) {
                [self.delegate deviceSelectionViewController:self
                                             didSelectDevice:rowDevice];
            }
        }
    }
    [tableView deselectRowAtIndexPath:indexPath
                             animated:YES];
}

#pragma mark - KVO

- (void) observeValueForKeyPath:(NSString *)keyPath
                       ofObject:(id)object
                         change:(NSDictionary *)change
                        context:(void *)context
{
    // refresh
    [self.tableView reloadData];
}

@end
