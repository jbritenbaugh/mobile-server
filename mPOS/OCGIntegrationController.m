//
//  IntegrationController.m
//  mPOS
//
//  Created by Antonio Strijdom on 12/08/2013.
//  Copyright (c) 2013 Clarity. All rights reserved.
//

#import "OCGIntegrationController.h"
#import "AppDelegate.h"
#import "BasketController.h"
#import "TestModeController.h"
#import "TillSetupController.h"
#import "CRSLocationController.h"
#import "OCGProgressOverlay.h"
#import "ServerErrorHandler.h"
#import "SettingsTableViewController.h"
#import <objc/runtime.h>

@interface OCGIntegrationController ()
@property (nonatomic, strong) NSDictionary *currentDict;
@property (nonatomic, weak) SettingsTableViewController *settingsVC;
- (void) settingsDidPresentNotificationHandler:(NSNotification *)note;
- (void) settingsDidDismissNotificationHandler:(NSNotification *)note;
- (void) basketDidCompleteNotificationHandler:(NSNotification *)note;
- (NSString *) buildReturnURLWithResultCode:(NSString *)resultCode
                               errorMessage:(NSString *)errorMessage;
- (void) callReturnURLWithResultCode:(NSString *)resultCode
                        errorMessage:(NSString *)errorMessage;
- (void) applySettingsChanges;
@end

static NSString *kURLSchemeMPOSIntegrationIdentifier = @"MPOSIntegrationIdentifier";

@implementation OCGIntegrationController

#pragma mark - Private

- (void) settingsDidPresentNotificationHandler:(NSNotification *)note
{
    self.settingsVC = note.object;
}

- (void) settingsDidDismissNotificationHandler:(NSNotification *)note
{
    self.settingsVC = nil;
}

- (void) basketDidCompleteNotificationHandler:(NSNotification *)note
{
    // pull out the completion reason
    NSString *transactionResult = note.userInfo[kBasketControllerBasketCompleteResultKey];
    // call back to the return URL
    [self callReturnURLWithResultCode:transactionResult
                         errorMessage:nil];
}

- (NSString *) buildReturnURLWithResultCode:(NSString *)resultCode
                               errorMessage:(NSString *)errorMessage
{
    NSMutableString *result = nil;
    
    NSString *returnURL = self.currentDict[@"returnURL"];
    if (returnURL != nil) {
        result = [NSMutableString stringWithFormat:@"%@://", returnURL];
        // add the current server mode
        ServerMode mode = [TestModeController serverMode];
        switch (mode) {
            case ServerModeOffline:
                [result appendString:@"?server=SECONDARY"];
                break;
            default:
                [result appendString:@"?server=PRIMARY"];
                break;
        }
        // add the transaction result
        if (resultCode != nil) {
            [result appendFormat:@"&result=%@", resultCode];
        }
        // add the error message
        if (errorMessage != nil) {
            errorMessage = [errorMessage stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
            [result appendFormat:@"&errorMessage=%@", [errorMessage stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
        }
    }
    
    return result;
}

- (void) callReturnURLWithResultCode:(NSString *)resultCode
                        errorMessage:(NSString *)errorMessage
{
    if (self.currentDict != nil) {
        // get the return URL
        NSString *returnURLString = [self buildReturnURLWithResultCode:resultCode
                                                          errorMessage:errorMessage];
        // if we have one
        if (returnURLString != nil) {
            // and it is a valid url
            NSURL *returnURL = [NSURL URLWithString:returnURLString];
            if (returnURL != nil) {
                // call it
                DebugLog(@"Calling back to: %@", returnURL);
                if ([[UIApplication sharedApplication] openURL:returnURL]) {
                    // clear the current dict
                    self.currentDict = nil;
                } else {
                    ErrorLog(@"Could not call back as return URL is invalid: %@", returnURL);
                }
            }
        }
    }
}

- (void) applySettingsChanges
{
    // if we have a dictionary
    if (self.currentDict != nil) {
        // check for settings changes
        BOOL serverChanged = NO;
        BOOL tillChanged = NO;
        BOOL storeChanged = NO;
        // check for a server mode switch
        ServerMode newMode = [TestModeController serverMode];
        NSString *newServerMode = self.currentDict[@"server"];
        if (newServerMode != nil) {
            // parse
            if ([newServerMode caseInsensitiveCompare:@"PRIMARY"] == NSOrderedSame) {
                newMode = ServerModeOnline;
            } else if ([newServerMode caseInsensitiveCompare:@"SECONDARY"] == NSOrderedSame) {
                newMode = ServerModeOffline;
            }
            // check if the new server mode is different to the mode we are in
            if ([TestModeController serverMode] != newMode) {
                serverChanged = YES;
            }
        }
        // check for till id switch
        NSString *tillId = nil;
        NSString *newTillId = self.currentDict[@"tillid"];
        if (newTillId != nil) {
            // check if the new till id is different to the till id we currently have
            if (serverChanged) {
                // if we are switching servers, check against the new server
                switch (newMode) {
                    case ServerModeOnline: {
                        if (![[TillSetupController getOnlineTillNumber] isEqualToString:newTillId]) {
                            tillChanged = YES;
                        }
                        break;
                    }
                    case ServerModeOffline: {
                        if (![[TillSetupController getOfflineTillNumber] isEqualToString:newTillId]) {
                            tillChanged = YES;
                        }
                        break;
                    }
                    default:
                        break;
                }
            } else {
                // just compare to current
                if (![[TillSetupController getTillNumber] isEqualToString:newTillId]) {
                    tillChanged = YES;
                }
            }
        } else {
            newTillId = [TillSetupController getTillNumber];
        }
        // check for store id switch
        NSString *storeId = nil;
        NSString *newStoreId = self.currentDict[@"storeid"];
        if (newStoreId != nil) {
            // check if the new store id is different to the store id we currently have
            if (serverChanged) {
                // if we are switching servers, check against the new server
                switch (newMode) {
                    case ServerModeOnline: {
                        if (![[CRSLocationController getOnlineLocationKey] isEqualToString:newStoreId]) {
                            storeChanged = YES;
                        }
                        break;
                    }
                    case ServerModeOffline: {
                        if (![[CRSLocationController getOfflineLocationKey] isEqualToString:newStoreId]) {
                            storeChanged = YES;
                        }
                        break;
                    }
                    default:
                        break;
                }
            } else {
                if (![[CRSLocationController getLocationKey] isEqualToString:newStoreId]) {
                    storeChanged = YES;
                }
            }
        } else {
            newStoreId = [CRSLocationController getLocationKey];
        }
        // process changes, if any
        if (serverChanged || tillChanged || storeChanged) {
            // they are, clear down state
            [[BasketController sharedInstance] prepareForServerSwitch];
            // switch to mode
            if (serverChanged) {
                switch (newMode) {
                    case ServerModeOnline:
                        // go online
                        [TestModeController setServerMode:ServerModeOnline];
                        break;
                    case ServerModeOffline:
                        // go offline
                        [TestModeController setServerMode:ServerModeOffline];
                    default:
                        break;
                }
            }
            // if the store or till has changed
            if ((storeChanged || tillChanged) &&
                (newStoreId != nil) && (newTillId != nil)) {
                // update this device's registration
                [OCGProgressOverlay show];
                [TillSetupController setDeviceProfileTillNumber:newTillId
                                                    locationKey:newStoreId
                                                 serviceBaseURL:[TestModeController getServerAddress]
                                                       complete:^(NSError *error, ServerError *serverError) {
                                                           [OCGProgressOverlay hide];
                                                           if ((error != nil) || (serverError != nil)) {
                                                               NSString *errorMessage =
                                                               [[ServerErrorHandler sharedInstance] buildServerErrorMessage:serverError
                                                                                                             transportError:error
                                                                                                                withContext:nil];
                                                               [self callReturnURLWithResultCode:@"ERROR"
                                                                                    errorMessage:errorMessage];
                                                           } else {
                                                               // update store and till id
                                                               switch ([TestModeController serverMode]) {
                                                                   case ServerModeOnline: {
                                                                       [CRSLocationController setOnlineLocationKey:newStoreId];
                                                                       [TillSetupController setOnlineTillNumber:newTillId];
                                                                       break;
                                                                   }
                                                                   case ServerModeOffline: {
                                                                       [CRSLocationController setOfflineLocationKey:newStoreId];
                                                                       [TillSetupController setOfflineTillNumber:newTillId];
                                                                       break;
                                                                   }
                                                                   default:
                                                                       break;
                                                               }
                                                               // reload configuration
                                                               [[BasketController sharedInstance] refreshConfiguration:YES];
                                                               // update the settings view (if visible)
                                                               if (self.settingsVC != nil) {
                                                                   [self.settingsVC checkView];
                                                                   [self.settingsVC.collectionView reloadData];
                                                               }
                                                           }
                                                       }];
            } else {
                // force refresh config
                [[BasketController sharedInstance] refreshConfiguration:YES];
                // update the settings view (if visible)
                if (self.settingsVC != nil) {
                    [self.settingsVC checkView];
                    [self.settingsVC.collectionView reloadData];
                }
            }
        } else {
            // refresh config
            [[BasketController sharedInstance] refreshConfiguration:NO];
            [[BasketController sharedInstance] getActiveBasket];
        }
    }
}

#pragma mark - Properties

@synthesize currentDict = _currentDict;
- (NSDictionary *) currentDict
{
    // if we have no dictionary
    if (_currentDict == nil) {
        // try and load one from user defaults
        _currentDict =
        [[NSUserDefaults standardUserDefaults] objectForKey:@"state_last_integration_dict"];
        if (_currentDict != nil) {
            DebugLog(@"Loaded last URL dictionary: %@", _currentDict);
        }
    }
    
    return _currentDict;
}

- (void) setCurrentDict:(NSDictionary *)currentDict
{
    _currentDict = currentDict;
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if (_currentDict != nil) {
        [defaults setObject:_currentDict
                     forKey:@"state_last_integration_dict"];
    } else {
        [defaults removeObjectForKey:@"state_last_integration_dict"];
    }
    [defaults synchronize];
}

@synthesize wasLauchedFromURL;
- (BOOL) wasLauchedFromURL
{
    return (self.currentDict != nil);
}

#pragma mark - Init

static char SHARED_INSTANCE_IDENTIFER;

+ (void) setSharedInstance:(OCGIntegrationController *) sharedInstance
{
    objc_setAssociatedObject(self, &SHARED_INSTANCE_IDENTIFER, sharedInstance, OBJC_ASSOCIATION_RETAIN);
}

+ (OCGIntegrationController *) sharedInstance
{
    __block id sharedInstance = objc_getAssociatedObject(self, &SHARED_INSTANCE_IDENTIFER);
    if (sharedInstance != nil) {
        return sharedInstance;
    }
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[self alloc] init];
        self.sharedInstance = sharedInstance;
    });
    
    return sharedInstance;
}

- (id) init
{
    self = [super init];
    if (self) {
        // register for the basket did complete notification
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(basketDidCompleteNotificationHandler:)
                                                     name:kBasketControllerBasketCompleteNotification
                                                   object:nil];
        // register for the settings present/dismiss notifications
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(settingsDidPresentNotificationHandler:)
                                                     name:kAppDelegateSettingsPresentedNotification
                                                   object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(settingsDidDismissNotificationHandler:)
                                                     name:kAppDelegateSettingsDismissedNotification
                                                   object:nil];
    }
    return self;
}

- (void) dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - Methods

- (NSString *) returnURLScheme
{
    NSString *returnURLScheme = nil;
    
    for (NSDictionary *urlType in [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleURLTypes"]) {
        if ([urlType[@"CFBundleURLName"] isEqualToString:kURLSchemeMPOSIntegrationIdentifier]) {
            returnURLScheme = urlType[@"CFBundleURLSchemes"][0];
            break;
        }
    }
    
    return returnURLScheme;
}

- (BOOL) application:(UIApplication *)application
             openURL:(NSURL *)url
   sourceApplication:(NSString *)sourceApplication
          annotation:(id)annotation
{
    BOOL result =
    [super application:application
               openURL:url
     sourceApplication:sourceApplication
            annotation:annotation];
    
    if (result) {
        DebugLog(@"Opened by %@ with URL %@", sourceApplication, url);
        self.currentDict = [self dictionaryForQuery:url.query];
        // check for a server mode change
        [self applySettingsChanges];
    }
    
    return result;
}

@end
