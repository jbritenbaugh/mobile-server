//
//  POSFuncController.m
//  mPOS
//
//  Created by Antonio Strijdom on 20/01/2014.
//  Copyright (c) 2014 Clarity. All rights reserved.
//

#import "POSFuncController.h"
#import <objc/runtime.h>
#import "BasketController.h"
#import "OCGMenuCell.h"
#import "POSFunc.h"
#import "AppDelegate.h"
#import "BasketListViewController.h"
#import "OCGEmailTextFieldValidator.h"
#import "DeviceStationViewController.h"
#import "OCGProgressOverlay.h"
#import "DiscountListViewController.h"
#import "BasketHistoryViewController.h"
#import "GiftReceiptViewController.h"
#import "OCGPeripheralManager.h"
#import "OCGPeripheralDevice.h"
#import "ContextViewController.h"
#import "BasketDiscountViewController.h"
#import "BasketItemQuantityViewController.h"
#import "ReceiptScanViewController.h"
#import "BarcodeScannerController.h"
#import "TaxFreeSaleViewController.h"
#import "SwipeCardViewController.h"
#import "OCGReceiptManager.h"
#import "UIImage+OCGExtensions.h"
#import "FlightDetailsItemViewController.h"
#import "OCGEFTManager.h"
#import "OCGKeyboardTypeButtonItem.h"

@interface POSFuncController () <DiscountListViewControllerDelegate, BasketHistoryViewControllerDelegate, BasketListViewControllerDelegate, UITextFieldDelegate>
@property (nonatomic, strong) MoreOptionsTableViewController *moreOptionsVC;
@property (nonatomic, strong) UIAlertView *associateAlert;
@property (nonatomic, strong) UIAlertView *emailAlert;
@property (nonatomic, strong) UIAlertView *descriptionAlert;
@property (nonatomic, strong) OCGEmailTextFieldValidator *emailValidator;
- (void) presentViewController:(UIViewController *)viewController;
- (void) presentViewController:(UIViewController *)viewController
                  makeDelegate:(BOOL)delegate;
@end

@implementation POSFuncController

#pragma mark - Private

NSString * const kDATA_CAPTURE_SWIPE = @"kDATA_CAPTURE_SWIPE";

- (void) presentViewController:(UIViewController *)viewController
{
    [self presentViewController:viewController
                   makeDelegate:NO];
}

- (void) presentViewController:(UIViewController *)viewController
                  makeDelegate:(BOOL)delegate
{
    UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:viewController];
    navController.modalPresentationStyle = UIModalPresentationFormSheet;
    navController.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
    if (delegate) {
        navController.delegate = (UIViewController<UINavigationControllerDelegate> *)viewController;
    }
    [self.splitViewController presentViewController:navController
                                           animated:YES
                                         completion:nil];
}

#pragma mark - Init

static char SHARED_INSTANCE_IDENTIFER;

+ (void) setSharedInstance:(POSFuncController *) sharedInstance
{
    objc_setAssociatedObject(self, &SHARED_INSTANCE_IDENTIFER, sharedInstance, OBJC_ASSOCIATION_RETAIN);
}

+ (POSFuncController *) sharedInstance
{
    __block id sharedInstance = objc_getAssociatedObject(self, &SHARED_INSTANCE_IDENTIFER);
    if (sharedInstance != nil) {
        return sharedInstance;
    }
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[self alloc] init];
        self.sharedInstance = sharedInstance;
    });
    
    return sharedInstance;
}

#pragma mark - MoreOptionsTableViewControllerDelegate

- (BOOL) isMenuOptionActive:(SoftKey*)softKey
{
    BOOL result = NO;
    
    POSFunc posFunc = POSFuncForString(softKey.posFunc);
    
    switch (posFunc) {
        case POSFuncEditBasket:
            result = BasketController.sharedInstance.basket.basketItems.count > 0;
            break;
            
        case POSFuncCancelSale:
            result = BasketController.sharedInstance.canClearBasket;
            break;
            
        case POSFuncSuspendSale:
            result = BasketController.sharedInstance.canSuspendBasket;
            break;
            
        case POSFuncResumeSale:
            result = BasketController.sharedInstance.canRecallBaskets;
            break;
            
        case POSFuncReloadOptions:
            result = BasketController.sharedInstance.canReloadOptions;
            break;
            
        case POSFuncModifySaleDiscount:
            result = BasketController.sharedInstance.canDiscountBasket;
            break;
            
        case POSFuncLogoutOperator:
            result = BasketController.sharedInstance.canLogout;
            break;
            
        case POSFuncCheckoutSale:
            result = BasketController.sharedInstance.canCheckOut;
            break;
            
        case POSFuncReprintReceipt:
        case POSFuncPrintLastTransaction:
            result = BasketController.sharedInstance.canReprintReceipts;
            break;
            
        case POSFuncToggleTrainingMode:
            result = !BasketController.sharedInstance.canClearBasket;
            break;
            
        case POSFuncPrintGiftReceipt:
            result = BasketController.sharedInstance.canClearBasket;
            break;
            
        case POSFuncOpenCashDrawer:
            result = [BasketController.sharedInstance.basket.basketID length] == 0;
            break;
            
        case POSFuncEftCancelTx:
        case POSFuncEftFinishTx:
        case POSFuncEftConfirmSignature:
        case POSFuncEftUnconfirmSignature: {
            // only allow EFT pos funcs if we are in an EFT transaction
            result = [OCGEFTManager sharedInstance].processing;
            if (posFunc == POSFuncEftFinishTx) {
                // only allow finish if the payment is complete
                result = ([OCGEFTManager sharedInstance].currentPayment.paymentStatus == kEFTPaymentStatusComplete);
            }
            break;
        }
        case POSFuncEftRetryStoredTx:
            // only allow retry for Payware (or Fake)
            result = ((OCGEFTManagerProviderPayware == [[OCGEFTManager sharedInstance] getConfiguredProvider])
#if DEBUG
                      || (OCGEFTManagerProviderFake == [[OCGEFTManager sharedInstance] getConfiguredProvider])
#endif
                       );
            break;
        case POSFuncAssignAffiliation:
            result = [BasketController sharedInstance].canScanToBasket;
            break;
            
        case POSFuncNull:
        case POSFuncAddDiscount:
        case POSFuncAddReturnItem:
        case POSFuncAssignEmailAddress:
        case POSFuncContextMenuItemBasket:
        case POSFuncContextMenuItemMenu:
        case POSFuncContextMenuItemScan:
        case POSFuncDeviceStationStatus:
        case POSFuncFinishSale:
        case POSFuncItemDetails:
        case POSFuncModifyPrice:
        case POSFuncModifyQuantity:
        case POSFuncModifySalesperson:
        case POSFuncModifySettings:
        case POSFuncOptionalModifiers:
        case POSFuncRemoveDiscount:
        case POSFuncRemoveItem:
        case POSFuncReturnInternet:
        case POSFuncReturnManualEntry:
        case POSFuncReturnNoReceipt:
        case POSFuncSwipeData:
        case POSFuncTaxFreeSale:
        case POSFuncUncheckoutSale:
        case POSFuncPrintPrintline:
        case POSFuncSkipPrintline:
        case POSFuncContextMenuItemSearch:
            result = YES;
            break;
            
        case POSFuncUnknown:
        case POSFuncItemSearch:
        case POSFuncLookupCustomer:
        default:
            result = NO;
            break;
    }
    return result;
}

- (BOOL) isMenuOptionLocked:(SoftKey*)key
{
    BOOL result = YES;
    
    POSFunc posFunc = POSFuncForString(key.posFunc);
    
    switch (posFunc) {
        case POSFuncModifySettings:
            // always allow settings
            result = NO;
            break;
            
        case POSFuncCancelSale:
            result = ![[BasketController sharedInstance] doesUserHavePermissionTo:POSPermissionCancelSale];
            break;
            
        case POSFuncSuspendSale:
            result = ![[BasketController sharedInstance] doesUserHavePermissionTo:POSPermissionSuspendResumeSale];
            break;
            
        case POSFuncResumeSale:
            result = ![[BasketController sharedInstance] doesUserHavePermissionTo:POSPermissionSuspendResumeSale];
            break;
            
        case POSFuncSwipeData:
            result = NO;
            break;
            
        case POSFuncLogoutOperator:
            // always allow logout
            result = NO;
            break;
            
        case POSFuncModifySaleDiscount:
            result = ![[BasketController sharedInstance] doesUserHavePermissionTo:POSPermissionModifySaleDiscount];
            break;
            
        case POSFuncCheckoutSale:
            result = ![[BasketController sharedInstance] doesUserHavePermissionTo:POSPermissionModifyCheckout];
            break;
            
        case POSFuncReprintReceipt:
            result = ![[BasketController sharedInstance] doesUserHavePermissionTo:POSPermissionReprintReceipt];
            break;
            
        case POSFuncAddReturnItem:
            result = ![[BasketController sharedInstance] doesUserHavePermissionTo:POSPermissionAddReturnItem];
            break;
            
        case POSFuncAssignEmailAddress:
            result = ![[BasketController sharedInstance] doesUserHavePermissionTo:POSPermissionAssignEmail];
            break;
            
        case POSFuncToggleTrainingMode:
            result = ![[BasketController sharedInstance] doesUserHavePermissionTo:POSPermissionToggleTrainingMode];
            break;
            
        case POSFuncItemDetails:
            result = ![[BasketController sharedInstance] doesUserHavePermissionTo:POSPermissionItemDetails];
            break;
            
        case POSFuncDeviceStationStatus:
            result = ![[BasketController sharedInstance] doesUserHavePermissionTo:POSPermissionDeviceStationStatus];
            break;
            
        case POSFuncPrintLastTransaction:
            result = ![[BasketController sharedInstance] doesUserHavePermissionTo:POSPermissionPrintLastTransaction];
            break;
            
        case POSFuncOpenCashDrawer:
            result = ![[BasketController sharedInstance] doesUserHavePermissionTo:POSPermissionOpenCashDrawer];
            break;
            
        case POSFuncTaxFreeSale:
            result = ![[BasketController sharedInstance] doesUserHavePermissionTo:POSPermissionTaxFreeSale];
            break;
            
        case POSFuncReturnInternet:
            result = ![[BasketController sharedInstance] doesUserHavePermissionTo:POSPermissionAddReturnItemInternet];
            break;
        case POSFuncReturnManualEntry:
            result = ![[BasketController sharedInstance] doesUserHavePermissionTo:POSPermissionAddReturnItemLinked];
            break;
        case POSFuncReturnNoReceipt:
            result = ![[BasketController sharedInstance] doesUserHavePermissionTo:POSPermissionAddReturnItemNoreceipt];
            break;
        case POSFuncAssignAffiliation:
            result = ![[BasketController sharedInstance] doesUserHavePermissionTo:POSPermissionAssignAffiliation];
            break;
            
        case POSFuncContextMenuItemSearch:
            // search is active as long as the user has permission for to customer or item search
            result = ([[BasketController sharedInstance] doesUserHavePermissionTo:POSPermissionLookupCustomer] ||
                      [[BasketController sharedInstance] doesUserHavePermissionTo:POSPermissionItemSearch]);
            break;
            
        default:
            // default to not locked
            result = NO;
            break;
            
    }
    
    return result;
}

- (void) moreOptionsController:(MoreOptionsTableViewController *)moreVC
               didSelectOption:(SoftKey*)optionKey
{
    [[ContextViewController sharedInstance] didSelectContextMenuItem:optionKey];
}

#pragma mark - DiscountListViewControllerDelegate

- (void) dismissDiscountListViewController:(DiscountListViewController *)discountListViewController
{
    [self.splitViewController dismissViewControllerAnimated:YES
                                                 completion:nil];
}

#pragma mark - BasketHistoryViewControllerDelegate

- (void) basketHistory:(BasketHistoryViewController *)historyVC
       didSelectBasket:(BasketDTO *)basket
{
    [self.splitViewController dismissViewControllerAnimated:YES
                                                 completion:^{
                                                     // take over the selected basket
                                                     
                                                     basket.reprint = YES;
                                                     BasketController.sharedInstance.basket = basket;
                                                     [BasketController.sharedInstance kick];
                                                     [[BasketController sharedInstance] getActiveBasket];
                                                 }];
}

- (void) basketHistoryDidCancel:(BasketHistoryViewController *)historyVC
{
    [self.splitViewController dismissViewControllerAnimated:YES
                                                 completion:nil];
}

#pragma mark - BasketListViewControllerDelegate

- (void) basketListViewController:(BasketListViewController *)basketListController
                     recallBasket:(BasketDTO*)basket
{
    __weak OCGSplitViewController *splitViewController = self.splitViewController;
    
    [[BasketController sharedInstance] resumeBasket:basket.basketID
                                        fromBarcode:nil
                                     withTillNumber:basket.tillNumber
                                            success:^
     {
         [splitViewController dismissViewControllerAnimated:YES
                                                 completion:nil];
     }];
}

- (void) basketListViewController:(BasketListViewController *)basketListController recallBarcode:(NSString *)basketBarcode
{
    __weak OCGSplitViewController *splitViewController = self.splitViewController;
    
    [[BasketController sharedInstance] resumeBasket:nil
                                        fromBarcode:basketBarcode
                                     withTillNumber:nil
                                            success:^
     {
         [splitViewController dismissViewControllerAnimated:YES
                                                 completion:nil];
     }];
}

- (void) dismissBasketListViewController:(BasketListViewController *)basketListViewController
{
    [self.splitViewController dismissViewControllerAnimated:YES
                                                 completion:nil];
}

#pragma mark - UITextFieldDelegate

- (BOOL) textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    BOOL result = YES;
    
    if (self.associateAlert) {
        NSString *text = [textField.text stringByReplacingCharactersInRange:range
                                                                 withString:string];
        if ([text length] > 10) {
            result = NO;
        }
    }
    if (self.emailAlert) {
        result = [self.emailValidator textField:textField
                  shouldChangeCharactersInRange:range
                              replacementString:string];
    }
    
    return result;
}

- (BOOL) textFieldShouldReturn:(UITextField *)textField
{
    if (self.descriptionAlert) {
        [textField resignFirstResponder];
        [self.descriptionAlert dismissWithClickedButtonIndex:1
                                                    animated:YES];
    }
    if (self.associateAlert) {
        [textField resignFirstResponder];
        [self.associateAlert dismissWithClickedButtonIndex:1
                                                  animated:YES];
    }
    if (self.emailAlert) {
        if ([self.emailValidator isStringValid:textField.text]) {
            [textField resignFirstResponder];
            [self.emailAlert dismissWithClickedButtonIndex:1
                                                  animated:YES];
        }
    } else {
        [textField resignFirstResponder];
    }
    return NO;
}

#pragma mark - Methods

+ (void) presentPermissionAlert:(POSPermission)perm
                      withBlock:(CancelBlock)block
{
    NSString *alertMessage = nil;
    switch (perm) {
        case POSPermissionAddReturnItem:
            alertMessage = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁁󠁯󠀱󠀳󠁏󠁭󠀴󠁑󠁔󠁎󠀷󠀱󠁯󠁰󠁎󠁗󠁹󠀹󠀲󠁁󠁴󠁅󠁆󠁣󠀹󠁏󠁫󠁿*/ @"You do not have permission to return an item", nil);
            break;
        case POSPermissionModifyCheckout : {
            if ([BasketController sharedInstance].isCheckedOut) {
                alertMessage = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁺󠀵󠁁󠁡󠀳󠁐󠁧󠁩󠁙󠁳󠁱󠁚󠁉󠁁󠁋󠁙󠁮󠁺󠁈󠁇󠁭󠀴󠁗󠁓󠁇󠁣󠁁󠁿*/ @"You do not have permission to cancel a checkout", nil);
            } else {
                alertMessage = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁒󠁘󠀴󠁘󠀰󠁬󠁊󠁊󠁴󠁳󠀰󠀹󠁋󠁡󠁴󠀳󠁈󠁘󠁦󠁍󠁺󠁏󠁨󠀷󠁫󠁵󠀸󠁿*/ @"You do not have permission to checkout a basket", nil);
            }
            break;
        }
        case POSPermissionAddTender:
            alertMessage = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁯󠁱󠁦󠁲󠁧󠀷󠀹󠁅󠁥󠁑󠀹󠁵󠁰󠁱󠁕󠁥󠁪󠁇󠁷󠁫󠁗󠀲󠁇󠁰󠁩󠁄󠀴󠁿*/ @"You do not have permission to pay for this basket", nil);
            break;
        case POSPermissionCancelSale:
            alertMessage = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁖󠀷󠁰󠁎󠀰󠀷󠁸󠁍󠀷󠁧󠁬󠁨󠁄󠁬󠁔󠁗󠁸󠁹󠁏󠁈󠁬󠀫󠁅󠁪󠁱󠁩󠀸󠁿*/ @"You do not have permission to cancel a sale", nil);
            break;
        case POSPermissionCancelItem:
            alertMessage = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁑󠁲󠁔󠁨󠁲󠁳󠁺󠀴󠁕󠀫󠁏󠁋󠁌󠁖󠁮󠁚󠀶󠁬󠁗󠁣󠀷󠁣󠁅󠁧󠁎󠁓󠁷󠁿*/ @"You do not have permission to remove an item", nil);
            break;
        case POSPermissionFinishSale:
            alertMessage = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁊󠁢󠁴󠁒󠁌󠀷󠀲󠁳󠁊󠁒󠁘󠁆󠁣󠀳󠁱󠁺󠁫󠁴󠁸󠀫󠁦󠁯󠀹󠁰󠀹󠀹󠀰󠁿*/ @"You do not have permission to finish a sale", nil);
            break;
        case POSPermissionModifySaleDiscount:
            alertMessage = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁏󠁰󠁥󠀰󠁂󠁢󠁙󠁘󠁴󠀳󠁹󠀴󠀰󠁍󠀴󠁺󠁮󠁔󠀳󠁢󠁭󠁩󠁮󠁥󠁹󠁲󠁷󠁿*/ @"You do not have permission to add or remove a sale discount", nil);
            break;
        case POSPermissionModifyItemDiscount:
            alertMessage = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁡󠁳󠁑󠀸󠁶󠁏󠁉󠁃󠁲󠁇󠁮󠀳󠀶󠀲󠁇󠁥󠁯󠁥󠁍󠀶󠁭󠁌󠁑󠁙󠁓󠁰󠁯󠁿*/ @"You do not have permission to add or remove an item discount", nil);
            break;
        case POSPermissionModifyItemQuantity:
            alertMessage = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠀴󠀵󠁕󠁏󠁎󠀵󠁹󠁺󠁤󠁵󠁤󠀫󠀳󠁏󠁌󠁏󠁅󠀷󠀷󠁆󠁩󠁧󠁋󠀱󠁳󠁂󠁧󠁿*/ @"You do not have permission to change the quantity of an item", nil);
            break;
        case POSPermissionModifyItemPrice:
            alertMessage = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠀶󠁊󠁒󠁒󠁉󠁑󠁧󠁒󠁋󠁷󠁴󠀰󠁸󠁤󠁏󠁪󠁔󠁡󠁭󠁑󠁩󠁵󠁖󠁙󠁚󠁩󠀰󠁿*/ @"You do not have permission to specify an item's price", nil);
            break;
        case POSPermissionSuspendResumeSale: {
            if ([BasketController sharedInstance].canRecallBaskets) {
                alertMessage = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁬󠀫󠁄󠁰󠀱󠁖󠁩󠁍󠁫󠀯󠁎󠀸󠁶󠀯󠁏󠁅󠁔󠁪󠁹󠁕󠁦󠀫󠁈󠁏󠀶󠁕󠀰󠁿*/ @"You do not have permission to resume a sale", nil);
            } else {
                alertMessage = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁪󠁙󠁶󠁙󠁃󠀵󠁌󠀫󠁖󠁁󠁨󠁘󠀸󠀷󠁖󠁂󠁵󠁲󠁎󠀶󠀲󠁶󠁅󠀳󠁎󠁈󠁁󠁿*/ @"You do not have permission to suspend a sale", nil);
            }
            break;
        }
        case POSPermissionReprintReceipt:
            alertMessage = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁍󠁄󠀶󠀷󠁕󠁦󠁐󠁅󠀯󠁚󠁔󠀷󠀫󠀴󠁕󠁘󠁦󠁮󠁺󠀫󠁃󠁩󠁃󠁤󠁰󠁡󠁍󠁿*/ @"You do not have permission to reprint a receipt", nil);
            break;
        case POSPermissionModifySettings:
            alertMessage = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁇󠁋󠁺󠁨󠁶󠁎󠁦󠁺󠁋󠁔󠁉󠁏󠁕󠁇󠁕󠁁󠁷󠁆󠁈󠁐󠁪󠁥󠁙󠁃󠁺󠁔󠀰󠁿*/ @"You do not have permission to modify application settings", nil);
            break;
        case POSPermissionAssignEmail:
            alertMessage = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁒󠁊󠁰󠁸󠁏󠁕󠁳󠀲󠁵󠁔󠁖󠀸󠁌󠁅󠁮󠁘󠁢󠁌󠀸󠀶󠀶󠀹󠁏󠀷󠁒󠀯󠁫󠁿*/ @"You do not have permission to assign an email address to a sale", nil);
            break;
        case POSPermissionAssignCustomercard:
            alertMessage = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁚󠁡󠁍󠁯󠁇󠁴󠀵󠁄󠁚󠁺󠁂󠁬󠁆󠀲󠁕󠁑󠁉󠁷󠁰󠁨󠁁󠁋󠁭󠁏󠀸󠁎󠀴󠁿*/ @"You do not have permission to assign a customer card to a sale", nil);
            break;
        case POSPermissionAddItemWithPrice:
            alertMessage = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁑󠁓󠀯󠁥󠁫󠀫󠁲󠁦󠁒󠁶󠁔󠁴󠁎󠁡󠀲󠀴󠀱󠁕󠁧󠁐󠁯󠁃󠀳󠁧󠁫󠁎󠁙󠁿*/ @"You do not have permission to add an item without a price", nil);
            break;
        case POSPermissionToggleTrainingMode:
            alertMessage = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁆󠁩󠁍󠀶󠁊󠁈󠁋󠁦󠁱󠀶󠁎󠁱󠁸󠁸󠁂󠁹󠁁󠁌󠁍󠀴󠀲󠀱󠁒󠁭󠁍󠁢󠁅󠁿*/ @"You do not have permission to toggle training mode", nil);
            break;
        case POSPermissionItemDetails:
            alertMessage = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁙󠁨󠁚󠁊󠀰󠁫󠁹󠁪󠁪󠁳󠁰󠁨󠁮󠀵󠁗󠁐󠁆󠁸󠀶󠁄󠀳󠁨󠁈󠀵󠁏󠀳󠁕󠁿*/ @"You do not have permission to view item details", nil);
            break;
        case POSPermissionModifySalesperson:
            alertMessage = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁒󠁸󠁃󠀸󠀵󠁏󠁥󠁨󠀯󠁩󠁈󠁧󠁣󠁈󠁚󠁊󠁂󠁡󠁢󠁖󠁋󠁯󠁲󠀵󠁓󠁰󠁧󠁿*/ @"You do not have permission to assign a sales associate to this item", nil);
            break;
        case POSPermissionPrintLastTransaction:
            alertMessage = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁖󠁮󠁕󠁁󠁒󠁗󠁃󠀳󠁢󠀵󠁳󠁹󠁳󠀸󠁪󠁐󠁵󠁨󠁫󠁏󠁐󠁢󠁄󠁔󠁍󠁁󠁣󠁿*/ @"You do not have permission to reprint the last receipt", nil);
            break;
        case POSPermissionOpenCashDrawer:
            alertMessage = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁪󠁔󠁘󠁚󠀳󠁕󠀫󠁆󠁺󠁶󠁫󠀫󠁗󠁮󠁐󠁪󠁆󠁳󠁫󠁋󠁒󠁘󠀱󠀹󠁑󠁂󠁧󠁿*/ @"You do not have permission to open the cash drawer", nil);
            break;
        case POSPermissionTaxFreeSale:
            alertMessage = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁙󠁭󠀴󠁩󠁐󠁑󠁵󠁇󠁮󠀷󠁈󠁶󠁷󠁭󠀷󠀫󠁗󠁲󠁆󠁄󠁐󠀶󠁙󠁲󠁩󠁅󠁍󠁿*/ @"You do not have permission to assign tax free status to a sale", nil);
            break;
        case POSPermissionAssignAffiliation:
            alertMessage = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁭󠁣󠀶󠀸󠁤󠀷󠀹󠁙󠁂󠁄󠀯󠁙󠁭󠁭󠁅󠁫󠀰󠀲󠁺󠁡󠁚󠁅󠁶󠁚󠁣󠀷󠁫󠁿*/ @"You do not have permission to add an affiliation to a sale", nil);
            break;
        default:
            alertMessage = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁉󠁏󠀷󠁤󠁯󠁶󠁎󠁚󠀵󠁏󠁭󠀲󠁏󠁕󠁐󠀴󠀵󠀴󠁺󠁎󠀳󠁫󠁐󠁅󠀸󠁭󠀴󠁿*/ @"You do not have permission to perform this action", nil);
            break;
    }
    
    UIAlertView *alert =
    [UIAlertView alertViewWithTitle:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁨󠁘󠁔󠁂󠁤󠁚󠁨󠁪󠁃󠁂󠁦󠁂󠁺󠁔󠁦󠁎󠁓󠁭󠁄󠁓󠁣󠁘󠁕󠀶󠁁󠁖󠁑󠁿*/ @"Access Denied", nil)
                            message:alertMessage
                  cancelButtonTitle:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁄󠁴󠁍󠁉󠁣󠀫󠁺󠁔󠁗󠁦󠁹󠁡󠁯󠀷󠁨󠁔󠁩󠀸󠁯󠀫󠁨󠁘󠀹󠁱󠁌󠀶󠀸󠁿*/ @"OK", nil)
                  otherButtonTitles:nil
                          onDismiss:nil
                           onCancel:block];
    [alert show];
}

+(void)presentUnavailableWithBlock:(CancelBlock)block
{
    UIAlertView *alert =
    [UIAlertView alertViewWithTitle:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁃󠁉󠀴󠁸󠁵󠁕󠀶󠁒󠁈󠁮󠁫󠁸󠀶󠁉󠁰󠁢󠁪󠁭󠁬󠁑󠁍󠁤󠁑󠁺󠀹󠁘󠁑󠁿*/ @"Unavailable", nil)
                            message:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁪󠀲󠁵󠁣󠁲󠁄󠁃󠁙󠁥󠁳󠁤󠁊󠁗󠁬󠁺󠀹󠁺󠁒󠁒󠁪󠁵󠁹󠀰󠁫󠁙󠁖󠁁󠁿*/ @"This function is not available in this version of the mPOS client", nil)
                  cancelButtonTitle:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁄󠁴󠁍󠁉󠁣󠀫󠁺󠁔󠁗󠁦󠁹󠁡󠁯󠀷󠁨󠁔󠁩󠀸󠁯󠀫󠁨󠁘󠀹󠁱󠁌󠀶󠀸󠁿*/ @"OK", nil)
                  otherButtonTitles:nil
                          onDismiss:nil
                           onCancel:block];
    [alert show];
}

+ (void) getKeyInfoWithKey:(SoftKey *)softKey
                     title:(NSString **)title
                     image:(UIImage **)image
{
    // initialise
    *title = nil;
    *image = nil;
    
    *title = softKey.keyDescription;
    
    if ([softKey.keyType isEqualToString:@"MENU"]) {
        *image = [UIImage OCGExtensions_imageNamed:@"MENU"];
    } else if (softKey.upImage.length > 0) {
        *image = [UIImage OCGExtensions_imageNamed:softKey.upImage];
    } else {
        *image = [UIImage OCGExtensions_imageNamed:softKey.posFunc];
    }
}

- (MoreOptionsTableViewController *) buildMoreOptionsVCForKey:(SoftKey *)menuSoftKey
{
    // build and present the view
    if (self.moreOptionsVC == nil) {
        self.moreOptionsVC = [[MoreOptionsTableViewController alloc] init];
        self.moreOptionsVC.delegate = self;
    }
    self.moreOptionsVC.softKey = menuSoftKey;
    
    return self.moreOptionsVC;
}

#pragma POS Functions

- (BOOL) assignAffiliation:(Affiliation *)affiliation
                completion:(void (^)(void))completion
{
    BOOL result = NO;
    
    if ([[BasketController sharedInstance] doesUserHavePermissionTo:POSPermissionAssignAffiliation]) {
        UIAlertView *alertView =
        [[UIAlertView alloc] initWithTitle:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁰󠁑󠁢󠁺󠁍󠁱󠁵󠀸󠀵󠁏󠁴󠁕󠁒󠀯󠁳󠁎󠀫󠁇󠁙󠀯󠁳󠁤󠁍󠁸󠁗󠁒󠁍󠁿*/ @"Affiliation Code Required", nil)
                                   message:[NSString stringWithFormat:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁴󠁱󠁙󠁄󠁲󠁭󠁪󠁸󠀰󠁌󠀸󠀸󠁋󠁏󠁓󠁴󠁕󠁒󠀰󠁓󠀲󠁥󠁵󠁏󠀵󠁫󠀴󠁿*/ @"Please enter the %@", nil), affiliation.affiliationName]
                                  delegate:[UIAlertView class]
                         cancelButtonTitle:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁩󠁢󠁺󠁬󠁏󠁶󠁣󠁧󠁫󠁍󠁋󠁧󠁫󠀱󠁶󠁐󠁐󠀹󠁘󠀴󠁘󠁡󠁕󠁭󠁹󠁷󠀰󠁿*/ @"Cancel", nil)
                         otherButtonTitles:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁃󠀶󠀵󠁺󠁙󠀱󠁚󠁓󠁰󠀴󠁮󠁫󠁵󠁑󠁩󠀲󠁲󠁱󠀯󠁋󠁰󠁯󠁸󠁧󠀲󠁒󠁅󠁿*/ @"Enter", nil), nil];
        alertView.alertViewStyle = UIAlertViewStylePlainTextInput;
        
        UITextField *textField = [alertView textFieldAtIndex:0];
        [OCGKeyboardTypeButtonItem setKeyboardType:kOCGKeyboardTypeNumberPad
                                         forTarget:textField];
        textField.placeholder = affiliation.affiliationName;
        
        alertView.dismissBlock = ^(int buttonIndex) {
            if (buttonIndex == 0) {
                NSString *cardNumber = textField.text;
                MPOSBasketCustomerCard *basketCustomerCard = [[MPOSBasketCustomerCard alloc] init];
                basketCustomerCard.customerCardNumber = cardNumber;
                basketCustomerCard.affiliationType = affiliation.affiliationType;
                [BasketController.sharedInstance.basket addBasketItem:basketCustomerCard];
                [BasketController.sharedInstance kick];
                [[BasketController sharedInstance] addItem:basketCustomerCard
                                                completion:completion];
            }
        };
        
        alertView.shouldEnableFirstOtherButtonBlock = ^BOOL {
            return [textField.text length] > 0 && [textField.text length] <= 24;
        };
        
        [alertView show];
        result = YES;
    }
    else
    {
        [self.class presentPermissionAlert:POSPermissionAssignAffiliation
                                 withBlock:NULL];
    }
    
    return result;
}

- (BOOL) assignSalesAssociateToBasketItems:(NSArray *)basketItems
{
    BOOL result = NO;
    self.associateAlert = nil;
    
    if ((basketItems != nil) && (basketItems.count > 0)) {
        if ([[BasketController sharedInstance] doesUserHavePermissionTo:POSPermissionModifySalesperson]) {
            UIAlertView *alert =
            [[UIAlertView alloc] initWithTitle:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁴󠁷󠁖󠁑󠁎󠁅󠁊󠀱󠁚󠀱󠁨󠁧󠁵󠀫󠁥󠁱󠁡󠁯󠁯󠁗󠁎󠁃󠁄󠀫󠁎󠁁󠁙󠁿*/ @"Sales person", nil)
                                       message:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠀵󠁶󠁂󠀴󠀴󠁸󠁇󠁴󠁴󠁗󠁙󠁐󠁒󠁆󠁳󠁐󠁥󠁎󠁑󠁡󠁇󠁊󠁷󠀫󠁸󠁆󠁯󠁿*/ @"Please enter the ID of the sales associate you with to assign", nil)
                                      delegate:[UIAlertView class]
                             cancelButtonTitle:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁩󠁢󠁺󠁬󠁏󠁶󠁣󠁧󠁫󠁍󠁋󠁧󠁫󠀱󠁶󠁐󠁐󠀹󠁘󠀴󠁘󠁡󠁕󠁭󠁹󠁷󠀰󠁿*/ @"Cancel", nil)
                             otherButtonTitles:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠀹󠁂󠀹󠀹󠀹󠁲󠁺󠁷󠁈󠁴󠁉󠀴󠀸󠁉󠁐󠁪󠁈󠁋󠀫󠁥󠁄󠁷󠁥󠁅󠁈󠁚󠁅󠁿*/ @"Assign", nil), nil];
            alert.alertViewStyle = UIAlertViewStylePlainTextInput;
            UITextField *alertTextField = [alert textFieldAtIndex:0];
            alertTextField.delegate = self;
            alertTextField.placeholder = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁁󠀶󠀸󠀲󠁙󠀵󠁅󠁭󠀱󠁑󠀵󠀫󠁃󠁨󠁒󠀲󠁓󠁰󠁦󠁯󠁸󠁡󠁙󠁚󠁍󠁩󠀸󠁿*/ @"Associate ID", nil);
            alertTextField.text = nil;
            // TFS59176 - User ids are numeric only
            alertTextField.keyboardType = UIKeyboardTypeASCIICapable;
            __weak POSFuncController *weakSelf = self;
            alert.dismissBlock = ^(int buttonIndex) {
                if (buttonIndex == 0) {
                    // send request
                    NSString *operatorId = alertTextField.text;
                    [BasketController.sharedInstance modifyBasketItems:basketItems
                                                           salesPerson:operatorId];
                }
                weakSelf.associateAlert = nil;
            };
            alert.shouldEnableFirstOtherButtonBlock = ^BOOL {
                BOOL result = NO;
                
                if (alertTextField.text.length > 0) {
                    result = YES;
                }
                
                return result;
            };
            self.associateAlert = alert;
            [alert show];
            result = YES;
        } else {
            [POSFuncController presentPermissionAlert:POSPermissionModifySalesperson
                                            withBlock:nil];
        }
    } else {
        [[[UIAlertView alloc] initWithTitle:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁳󠀯󠁔󠁚󠁍󠀷󠀳󠁡󠁬󠁌󠁆󠁓󠁦󠁤󠁏󠁯󠁙󠀶󠁔󠁉󠁎󠁊󠁩󠁲󠁔󠁬󠁫󠁿*/ @"No item selected", nil)
                                    message:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁳󠁶󠁱󠁩󠁦󠁙󠁑󠁣󠁨󠀸󠁡󠀸󠀳󠀯󠁶󠁇󠁔󠁓󠀹󠁨󠁬󠁯󠁇󠁦󠁪󠁥󠀴󠁿*/ @"Please select at least one item", nil)
                                   delegate:nil
                          cancelButtonTitle:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁄󠁴󠁍󠁉󠁣󠀫󠁺󠁔󠁗󠁦󠁹󠁡󠁯󠀷󠁨󠁔󠁩󠀸󠁯󠀫󠁨󠁘󠀹󠁱󠁌󠀶󠀸󠁿*/ @"OK", nil)
                          otherButtonTitles:nil] show];
    }
    
    return result;
}

- (BOOL) cancelActiveBasket
{
    if ([[BasketController sharedInstance] doesUserHavePermissionTo:POSPermissionCancelSale]) {
        // present confirm prompt
        [UIAlertView alertViewWithTitle:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁎󠁒󠁎󠁤󠁃󠁥󠁄󠁐󠁃󠁕󠀳󠁅󠁧󠀯󠁉󠁪󠁵󠁧󠁺󠀳󠁫󠁡󠁇󠁎󠁇󠁘󠁷󠁿*/ @"Discard Sale", nil)
                                message:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁶󠁐󠁴󠁅󠀫󠁰󠁆󠁁󠁚󠁹󠁭󠁬󠁮󠁵󠁹󠀯󠀹󠁉󠁘󠁚󠁅󠀹󠁥󠁭󠀹󠁋󠀴󠁿*/ @"The current basket will be discarded.", nil)
                      cancelButtonTitle:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁩󠁢󠁺󠁬󠁏󠁶󠁣󠁧󠁫󠁍󠁋󠁧󠁫󠀱󠁶󠁐󠁐󠀹󠁘󠀴󠁘󠁡󠁕󠁭󠁹󠁷󠀰󠁿*/ @"Cancel", nil)
                      otherButtonTitles:[NSArray arrayWithObject: NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁘󠁭󠀶󠀰󠀯󠁙󠁇󠀵󠁕󠁲󠁒󠁇󠁉󠀶󠁫󠁤󠁭󠁉󠁕󠁓󠁈󠁹󠁱󠁍󠁋󠀳󠁍󠁿*/ @"Discard", nil)]
                              onDismiss:(DismissBlock) ^{
                                  [[BasketController sharedInstance] clearBasket];
                              }
                               onCancel:(CancelBlock) ^{
                                   // ignore; the user cancelled.
                               }];
        return YES;
    } else {
        [POSFuncController presentPermissionAlert:POSPermissionCancelSale
                                        withBlock:nil];
        return NO;
    }
}

- (BOOL) captureEmail
{
    if ([[BasketController sharedInstance] doesUserHavePermissionTo:POSPermissionAssignEmail]) {
        self.emailAlert =
        [[UIAlertView alloc] initWithTitle:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁺󠁂󠁦󠀹󠁄󠁹󠁂󠁙󠁎󠀸󠁬󠁘󠁤󠁮󠁳󠁋󠁃󠁧󠁊󠀱󠁉󠀶󠁤󠁊󠁊󠀶󠁁󠁿*/ @"Add Email Address", nil)
                                   message:nil
                                  delegate:[UIAlertView class]
                         cancelButtonTitle:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁩󠁢󠁺󠁬󠁏󠁶󠁣󠁧󠁫󠁍󠁋󠁧󠁫󠀱󠁶󠁐󠁐󠀹󠁘󠀴󠁘󠁡󠁕󠁭󠁹󠁷󠀰󠁿*/ @"Cancel", nil)
                         otherButtonTitles:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁷󠀸󠁬󠁧󠁢󠁍󠁺󠁱󠀹󠀴󠁳󠁧󠁕󠁧󠀫󠁒󠁑󠁂󠁙󠁚󠁔󠁡󠁦󠁄󠁧󠁏󠁑󠁿*/ @"Save", nil), nil];
        
        self.emailAlert.alertViewStyle = UIAlertViewStylePlainTextInput;
        
        UITextField *emailTextField = [self.emailAlert textFieldAtIndex:0];
        emailTextField.delegate = self;
        emailTextField.keyboardType = UIKeyboardTypeEmailAddress;
        emailTextField.text = [BasketController sharedInstance].basket.emailAddress;
        emailTextField.placeholder = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁱󠁁󠁏󠀸󠁎󠁒󠁚󠀯󠁥󠁈󠁏󠀵󠀳󠀵󠁵󠀳󠁏󠁐󠁑󠁕󠁔󠀳󠁕󠁳󠁴󠁬󠁣󠁿*/ @"Email Address", nil);
        
        self.emailValidator = [[OCGEmailTextFieldValidator alloc] init];
        emailTextField.delegate = self;
        
        __weak POSFuncController *weakSelf = self;
        self.emailAlert.dismissBlock = ^(int buttonIndex) {
            if (buttonIndex == 0) {
                [[BasketController sharedInstance] assignEmail:emailTextField.text];
            }
            weakSelf.emailAlert = nil;
        };
        
        self.emailAlert.shouldEnableFirstOtherButtonBlock = ^ {
            return [weakSelf.emailValidator isStringValid:emailTextField.text];
        };
        
        [self.emailAlert show];
        return YES;
    } else {
        [POSFuncController presentPermissionAlert:POSPermissionAssignEmail
                                        withBlock:nil];
        return NO;
    }
    
}

- (BOOL) changeBasketItemQuantity:(MPOSBasketItem *)basketItem
{
    if ([[BasketController sharedInstance] doesUserHavePermissionTo:POSPermissionModifyItemQuantity]) {
        BasketItemQuantityViewController *changeItemQuantityViewController =
        [[BasketItemQuantityViewController alloc] initWithNibName:@"BasketItemQuantityView"
                                                           bundle:nil];
        changeItemQuantityViewController.basketItem = basketItem;
        [self presentViewController:changeItemQuantityViewController];
         
        return YES;
    } else {
        [POSFuncController presentPermissionAlert:POSPermissionModifyItemQuantity
                                        withBlock:nil];
        return NO;
    }
}

- (BOOL) checkoutBasket
{
    if ([[BasketController sharedInstance] doesUserHavePermissionTo:POSPermissionModifyCheckout]) {
        [[BasketController sharedInstance] checkoutBasket];
        return YES;
    } else {
        [POSFuncController presentPermissionAlert:POSPermissionModifyCheckout
                                        withBlock:nil];
        return NO;
    }
}

- (BOOL) deviceStationStatus
{
    if ([[BasketController sharedInstance] doesUserHavePermissionTo:POSPermissionDeviceStationStatus]) {
        // present the device station view controller
        DeviceStationViewController *deviceVC = [[DeviceStationViewController alloc] init];
        // present the scan screen
        [deviceVC getDeviceStatus];
        // show it
        [self presentViewController:deviceVC makeDelegate:YES];
        return YES;
    } else {
        return NO;
    }
}

- (void) eftConfirmSignature:(UIImage *)signature
{
    [[OCGEFTManager sharedInstance] confirmSignature:signature];
}

- (void) eftUnConfirmSignature:(UIImage *)signature
{
    [[OCGEFTManager sharedInstance] unConfirmSignature:signature];
}

- (BOOL) finishBasket
{
    if ([[BasketController sharedInstance] doesUserHavePermissionTo:POSPermissionFinishSale]) {
        [[BasketController sharedInstance] finishBasketWithDeviceStationWithId:nil];
        return YES;
    } else {
        [POSFuncController presentPermissionAlert:POSPermissionFinishSale
                                        withBlock:nil];
        return NO;
    }
}

- (BOOL) logoutCurrentAssociate
{
    [OCGProgressOverlay show];
    [[BasketController sharedInstance] wipeLocalBasket];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^(void) {
        [[BasketController sharedInstance] logoutSucceeded:^(BasketController *controller) {
            dispatch_async(dispatch_get_main_queue(), ^(void) {
                [OCGProgressOverlay hide];
                ContextViewController *contextVC = [ContextViewController sharedInstance];
                [contextVC presentLoginViewAnimated:YES];
            });
            
        } failed:^(BasketController *controller, NSError *error, ServerError *serverError)
         {
             dispatch_async(dispatch_get_main_queue(), ^(void) {
                 [OCGProgressOverlay hide];
                 
                 NSMutableString *messageText = [NSMutableString string];
                 
                 for (ServerErrorMessage *message in serverError.messages) {
                     if (message.value != nil) {
                         [messageText appendString:message.value];
                     }
                 }
                 
                 if (error != nil) {
                     [messageText appendString:[error localizedDescription]];
                 }
                 
                 
                 [UIAlertView alertViewWithTitle:nil
                                         message:messageText
                               cancelButtonTitle:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁡󠁩󠁡󠁍󠁰󠀸󠁷󠁣󠁹󠁩󠁕󠀷󠁪󠁶󠀱󠀸󠁐󠁖󠀵󠁥󠀳󠁫󠁕󠁚󠁄󠁶󠁙󠁿*/ @"Dismiss", nil)
                               otherButtonTitles:nil
                                       onDismiss:nil
                                        onCancel:nil];
             });
         }];
    });
    
    return YES;
}

/** @brief Marks the specified basket items to be printed on the gift receipt.
 */
- (BOOL) markBasketItemsForGiftReceipt:(NSArray *)basketItems
{
    if (basketItems.count > 0)
    {
        BasketController *controller = [BasketController sharedInstance];
        [controller markBasketItemsForGiftReceipt:basketItems];
        return YES;
    }
    else
    {
        [[[UIAlertView alloc] initWithTitle:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁳󠀯󠁔󠁚󠁍󠀷󠀳󠁡󠁬󠁌󠁆󠁓󠁦󠁤󠁏󠁯󠁙󠀶󠁔󠁉󠁎󠁊󠁩󠁲󠁔󠁬󠁫󠁿*/ @"No item selected", nil)
                                    message:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁳󠁶󠁱󠁩󠁦󠁙󠁑󠁣󠁨󠀸󠁡󠀸󠀳󠀯󠁶󠁇󠁔󠁓󠀹󠁨󠁬󠁯󠁇󠁦󠁪󠁥󠀴󠁿*/ @"Please select at least one item", nil)
                                   delegate:nil
                          cancelButtonTitle:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁄󠁴󠁍󠁉󠁣󠀫󠁺󠁔󠁗󠁦󠁹󠁡󠁯󠀷󠁨󠁔󠁩󠀸󠁯󠀫󠁨󠁘󠀹󠁱󠁌󠀶󠀸󠁿*/ @"OK", nil)
                          otherButtonTitles:nil] show];
        return NO;
    }
}

- (BOOL) openCashDrawer
{
    BasketController *controller = [BasketController sharedInstance];
    if ([controller doesUserHavePermissionTo:POSPermissionOpenCashDrawer]) {
        // present the scan screen
        DeviceStationViewController *deviceVC = [[DeviceStationViewController alloc] init];
        [deviceVC requestDeviceScanComplete:^(NSString *deviceStationId) {
            // close the view controller
            [deviceVC dismissViewControllerAnimated:YES
                                         completion:^{
                                             // send the open drawer again
                                             [[BasketController sharedInstance] openCashDrawerOnDeviceStation:deviceStationId];
                                         }];
        }
                                allowCancel:YES];
        // show it
        [self presentViewController:deviceVC];
        
        return YES;
    } else {
        [POSFuncController presentPermissionAlert:POSPermissionOpenCashDrawer
                                        withBlock:nil];
        
        return NO;
    }
}

- (BOOL) presentBasketDiscounts
{
    if ([[BasketController sharedInstance] doesUserHavePermissionTo:POSPermissionModifySaleDiscount]) {
        DiscountListViewController* discountListViewController = [[DiscountListViewController alloc] initWithNibName: @"DiscountListView" bundle: nil];
        discountListViewController.delegate = self;
        discountListViewController.basketItem = nil;
        discountListViewController.showAddDiscountNavigationButton = YES;
        discountListViewController.showDismissNavigationButton = YES;
        
        [self presentViewController:discountListViewController];
        return YES;
    } else {
        [POSFuncController presentPermissionAlert:POSPermissionModifySaleDiscount
                                        withBlock:nil];
        return NO;
    }
}

- (BOOL) presentBasketItemDiscount:(NSArray *)basketItems
{
    BOOL result = NO;
    
    if ((basketItems != nil) && (basketItems.count > 0)) {
        if ([[BasketController sharedInstance] doesUserHavePermissionTo:POSPermissionModifyItemPrice]) {
            BasketDiscountViewController *discountViewController = [[BasketDiscountViewController alloc] initWithNibName: @"BasketDiscountView" bundle: nil];
            
            discountViewController.discountType = DiscountTypeBasketItemDiscount;
            discountViewController.basketItems = basketItems;
            
            [self presentViewController:discountViewController];
            result = YES;
        } else {
            [POSFuncController presentPermissionAlert:POSPermissionModifyItemPrice
                                            withBlock:nil];
        }
    } else {
        [[[UIAlertView alloc] initWithTitle:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁳󠀯󠁔󠁚󠁍󠀷󠀳󠁡󠁬󠁌󠁆󠁓󠁦󠁤󠁏󠁯󠁙󠀶󠁔󠁉󠁎󠁊󠁩󠁲󠁔󠁬󠁫󠁿*/ @"No item selected", nil)
                                    message:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁳󠁶󠁱󠁩󠁦󠁙󠁑󠁣󠁨󠀸󠁡󠀸󠀳󠀯󠁶󠁇󠁔󠁓󠀹󠁨󠁬󠁯󠁇󠁦󠁪󠁥󠀴󠁿*/ @"Please select at least one item", nil)
                                   delegate:nil
                          cancelButtonTitle:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁄󠁴󠁍󠁉󠁣󠀫󠁺󠁔󠁗󠁦󠁹󠁡󠁯󠀷󠁨󠁔󠁩󠀸󠁯󠀫󠁨󠁘󠀹󠁱󠁌󠀶󠀸󠁿*/ @"OK", nil)
                          otherButtonTitles:nil] show];
    }
    
    return result;
}

- (BOOL) presentBasketItemPriceOverride:(MPOSBasketItem *)basketItem
{
    if ([[BasketController sharedInstance] doesUserHavePermissionTo:POSPermissionModifyItemPrice]) {
        BasketDiscountViewController *discountViewController = [[BasketDiscountViewController alloc] initWithNibName: @"BasketDiscountView" bundle: nil];
        
        discountViewController.discountType = DiscountTypeBasketItemPriceOverride;
        discountViewController.basketItems = @[basketItem];

        [self presentViewController:discountViewController];
        return YES;
    } else {
        [POSFuncController presentPermissionAlert:POSPermissionModifyItemPrice
                                        withBlock:nil];
        return NO;
    }
}

- (BOOL) presentCustomerSearch
{
    [POSFuncController presentUnavailableWithBlock:NULL];
    
//    // clear the existing search
//    [[CustomerController sharedInstance] clearCustomerSearch];
//    // present the view controller
//    if (self.customerNavController == nil) {
//        CustomerSearchViewController *customerSearchController =
//        [[CustomerSearchViewController alloc] initWithStyle:UITableViewStylePlain];
//        customerSearchController.delegate = self;
//        customerSearchController.allowSearch = YES;
//        customerSearchController.allowAdd = YES;
//        self.customerNavController = [[UINavigationController alloc] initWithRootViewController:customerSearchController];
//        self.customerNavController.modalPresentationStyle = UIModalPresentationFormSheet;
//        
//        //self.customerNavController.modalPresentationStyle = UIModalPresentationCurrentContext;
//        self.customerNavController.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
//    } else {
//        [self.customerNavController popToRootViewControllerAnimated:NO];
//    }
//    
//    [self presentViewController:self.customerNavController
//                       animated:YES
//                     completion:NULL];
    return NO;
}

- (BOOL) presentItemSearch:(SoftKey *)softKey
{
    [POSFuncController presentUnavailableWithBlock:NULL];

//    ItemSearchViewController *itemSearchController =
//    [[ItemSearchViewController alloc] initWithStyle:UITableViewStylePlain];
//    UINavigationController *itemSearchNavController = [[UINavigationController alloc] initWithRootViewController:itemSearchController];
//    //self.customerNavController.modalPresentationStyle = UIModalPresentationCurrentContext;
//    itemSearchNavController.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
//    itemSearchController.title = softKey.keyDescription;
//    
//    [self presentViewController:itemSearchNavController
//                       animated:YES
//                     completion:NULL];
    return NO;
}

- (BOOL) presentReprint
{
    if ([[BasketController sharedInstance] doesUserHavePermissionTo:POSPermissionReprintReceipt]) {
        // build and present the view
        BasketHistoryViewController *historyVC =
        [[BasketHistoryViewController alloc] initWithStyle:UITableViewStylePlain];
        historyVC.delegate = self;

        [self presentViewController:historyVC];
        return YES;
    } else {
        [POSFuncController presentPermissionAlert:POSPermissionReprintReceipt
                                        withBlock:nil];
        return NO;
    }
}

- (BOOL) presentSettingsView
{
    AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    [appDelegate presentSettingsViewAnimated:YES
                              viewController:self.splitViewController];
    return YES;
}

- (BOOL) presentTaxFreeSaleView
{
    if ([[BasketController sharedInstance] doesUserHavePermissionTo:POSPermissionTaxFreeSale]) {
        TaxFreeSaleViewController *viewController = [[TaxFreeSaleViewController alloc] init];
        [self presentViewController:viewController];
        return YES;
    } else {
        [POSFuncController presentPermissionAlert:POSPermissionTaxFreeSale
                                        withBlock:nil];
        return NO;
    }
}

- (BOOL) printGiftReceiptForItem:(MPOSBasketItem *)selectedItem
{
    if (selectedItem) {
        BasketController *controller = [BasketController sharedInstance];
        NSMutableArray *giftReceiptItems = [NSMutableArray arrayWithCapacity:controller.basket.basketItems.count];
        for (MPOSBasketItem *basketItem in controller.basket.basketItems) {
            if ([basketItem isKindOfClass:[MPOSSaleItem class]]) {
                if ([basketItem.basketItemId isEqualToString:selectedItem.basketItemId]) {
                    // if this is the item we are changing
                    if (!((MPOSSaleItem *)basketItem).generateGiftReceiptValue) {
                        // if it isn't marked for gift receipt, add it
                        [giftReceiptItems addObject:basketItem.basketItemId];
                    } else {
                        // already a gift receipt item so leave it out
                    }
                } else {
                    // if this item is marked as gift receipt, keep it as one
                    if (((MPOSSaleItem *)basketItem).generateGiftReceiptValue) {
                        [giftReceiptItems addObject:basketItem.basketItemId];
                    }
                }
            }
        }
        [controller markBasketItemsForGiftReceipt:giftReceiptItems];
    } else {
        GiftReceiptViewController *giftReceiptViewController = [[GiftReceiptViewController alloc] init];
        // get the items that have already been marked for gift receipt
        [self presentViewController:giftReceiptViewController];
    }
    return YES;
}

- (BOOL) recallActiveBasket
{
    if ([[BasketController sharedInstance] doesUserHavePermissionTo:POSPermissionSuspendResumeSale]) {
        BasketListViewController* recallBasketViewController =
        [[BasketListViewController alloc] init];
        recallBasketViewController.delegate = self;
        [self presentViewController:recallBasketViewController];
        return YES;
    } else {
        [POSFuncController presentPermissionAlert:POSPermissionSuspendResumeSale
                                        withBlock:nil];
        return NO;
    }
}

- (BOOL) removeBasketItems:(NSArray *)basketItems
{
    BOOL result = NO;
    
    if ((basketItems != nil) && (basketItems.count > 0)) {
        if ([[BasketController sharedInstance] doesUserHavePermissionTo:POSPermissionCancelItem]) {
            // check for promotional items and EFT tenders
            BOOL promotionRemoved = NO;
            MPOSEFTPayment *eftPayment = nil;
            for (NSString *basketItemId in basketItems) {
                // find the item
                for (MPOSBasketItem *item in BasketController.sharedInstance.basket.basketItems) {
                    if ([item.basketItemId isEqualToString:basketItemId]) {
                        promotionRemoved = [item isKindOfClass:[MPOSBasketRewardSalePromotionAmount class]];
                        if ([item isKindOfClass:[MPOSBasketTender class]]) {
                            MPOSBasketTender *tenderItem = (MPOSBasketTender *)item;
                            if (tenderItem.authorization && !tenderItem.authorization.providerVoided && [OCGEFTManager sharedInstance].configured) {
                                Tender *tender = [[BasketController sharedInstance] tenders][tenderItem.tenderType];
                                if (tender && tender.isEFTTender) {
                                    eftPayment = (MPOSEFTPayment *)tenderItem;
                                }
                            }
                        }
                        break;
                    }
                }
                if (promotionRemoved) break;
                if (eftPayment) break;
            }
            if (promotionRemoved) {
                [[[UIAlertView alloc] initWithTitle:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁕󠁪󠁬󠀰󠁦󠁬󠁯󠁬󠀯󠀴󠁏󠁙󠁰󠁂󠁫󠁈󠀷󠁧󠁯󠁺󠁨󠁵󠁙󠁃󠁺󠁤󠁫󠁿*/ @"Promotional reward selected", nil)
                                            message:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁗󠁑󠁧󠁤󠁎󠀴󠁈󠁓󠁥󠁮󠀫󠀴󠁴󠁔󠁪󠁯󠁖󠁨󠁃󠁔󠁥󠁘󠁲󠁅󠁲󠀷󠁯󠁿*/ @"Promotion rewards may not be manually removed from the sales basket.", nil)
                                           delegate:nil
                                  cancelButtonTitle:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁄󠁴󠁍󠁉󠁣󠀫󠁺󠁔󠁗󠁦󠁹󠁡󠁯󠀷󠁨󠁔󠁩󠀸󠁯󠀫󠁨󠁘󠀹󠁱󠁌󠀶󠀸󠁿*/ @"OK", nil)
                                  otherButtonTitles:nil] show];
            } else if (eftPayment) {
                [[OCGEFTManager sharedInstance] voidTransaction:eftPayment
                                                       complete:^(EFTProviderErrorCode resultCode, NSError *error, BOOL suppressError) {
                                                           if (kEFTProviderErrorCodeSuccess == resultCode) {
                                                               [[BasketController sharedInstance] removeBasketItems:@[eftPayment.basketItemId]
                                                                                                           complete:nil];
                                                           } else {
                                                               [[[UIAlertView alloc] initWithTitle:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁫󠀵󠁉󠁤󠁘󠁖󠁏󠁁󠁆󠀷󠁬󠀯󠁯󠁇󠀲󠀳󠁵󠁢󠀫󠁘󠁦󠀯󠁩󠁁󠁦󠁒󠁑󠁿*/ @"Error", nil)
                                                                                           message:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁪󠀯󠁭󠁉󠁧󠁬󠁳󠁗󠀲󠁚󠁔󠁏󠁖󠁤󠁓󠁩󠁁󠁘󠀹󠁵󠁲󠁗󠁳󠁃󠁸󠁺󠁕󠁿*/ @"EFT payment could not be voided.", nil)
                                                                                          delegate:nil
                                                                                 cancelButtonTitle:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁄󠁴󠁍󠁉󠁣󠀫󠁺󠁔󠁗󠁦󠁹󠁡󠁯󠀷󠁨󠁔󠁩󠀸󠁯󠀫󠁨󠁘󠀹󠁱󠁌󠀶󠀸󠁿*/ @"OK", nil)
                                                                                 otherButtonTitles:nil] show];

                                                           }
                                                       }];
                result = YES;
            } else {
                // queue the operation to be sent to the backend
                [[BasketController sharedInstance] removeBasketItems:basketItems
                                                            complete:nil];
                result = YES;
            }
        } else {
            [POSFuncController presentPermissionAlert:POSPermissionCancelItem
                                            withBlock:nil];
        }
    } else {
        [[[UIAlertView alloc] initWithTitle:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁳󠀯󠁔󠁚󠁍󠀷󠀳󠁡󠁬󠁌󠁆󠁓󠁦󠁤󠁏󠁯󠁙󠀶󠁔󠁉󠁎󠁊󠁩󠁲󠁔󠁬󠁫󠁿*/ @"No item selected", nil)
                                    message:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁳󠁶󠁱󠁩󠁦󠁙󠁑󠁣󠁨󠀸󠁡󠀸󠀳󠀯󠁶󠁇󠁔󠁓󠀹󠁨󠁬󠁯󠁇󠁦󠁪󠁥󠀴󠁿*/ @"Please select at least one item", nil)
                                   delegate:nil
                          cancelButtonTitle:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁄󠁴󠁍󠁉󠁣󠀫󠁺󠁔󠁗󠁦󠁹󠁡󠁯󠀷󠁨󠁔󠁩󠀸󠁯󠀫󠁨󠁘󠀹󠁱󠁌󠀶󠀸󠁿*/ @"OK", nil)
                          otherButtonTitles:nil] show];
    }
    
    return result;
}

- (BOOL) removeDiscountFromBasketItem:(MPOSBasketItem *)basketItem
{
    if ([[BasketController sharedInstance] doesUserHavePermissionTo:POSPermissionModifyItemDiscount]) {
        // check if we've got any discount to remove
        NSInteger discountCount = 0;
        
        if (basketItem != nil) {
            if (basketItem.basketRewards != nil) {
                for (MPOSBasketRewardLine *reward in basketItem.basketRewards) {
                    // specifically exclude price adjustments
                    if (![reward isKindOfClass:[MPOSBasketRewardLinePriceAdjust class]]) {
                        discountCount++;
                    }
                }
            }
        }

        if (discountCount > 0) {
            DiscountListViewController *removeItemDiscountViewController = [[DiscountListViewController alloc] initWithNibName: @"DiscountListView" bundle: nil];
            removeItemDiscountViewController.basketItem = basketItem;
            removeItemDiscountViewController.delegate = self;
            removeItemDiscountViewController.showDismissNavigationButton = YES;
            [self presentViewController:removeItemDiscountViewController];
        }
        
        return YES;
    } else {
        [POSFuncController presentPermissionAlert:POSPermissionModifyItemDiscount
                                        withBlock:nil];
        return NO;
    }
}

- (BOOL) reprintLastTx
{
    if ([OCGReceiptManager configuredReceiptManagerType] == ReceiptManagerTypeClient)
    {
        [[BasketController sharedInstance] reprintLastTxToDeviceStation:nil];
        return YES;
    }
    else
    {
        BasketController *controller = [BasketController sharedInstance];
        if ([controller doesUserHavePermissionTo:POSPermissionPrintLastTransaction]) {
            // present the scan screen
            DeviceStationViewController *deviceVC = [[DeviceStationViewController alloc] init];
            [deviceVC requestDeviceScanComplete:^(NSString *deviceStationId) {
                // close the view controller
                [deviceVC dismissViewControllerAnimated:YES
                                             completion:^{
                                                 // send the reprint again
                                                 [[BasketController sharedInstance] reprintLastTxToDeviceStation:deviceStationId];
                                             }];
            }
                                    allowCancel:YES];
            // show it
            [self presentViewController:deviceVC];
            return YES;
        } else {
            [POSFuncController presentPermissionAlert:POSPermissionPrintLastTransaction
                                            withBlock:nil];
            return NO;
        }
    }
}

- (BOOL) scanReturnToBasket
{
    if ([[BasketController sharedInstance] doesUserHavePermissionTo:POSPermissionAddReturnItemLinked] || [[BasketController sharedInstance] doesUserHavePermissionTo:POSPermissionAddReturnItemUnlinked])
    {
        ReceiptScanViewController *viewController = [[ReceiptScanViewController alloc] init];
        [self presentViewController:viewController];
        return YES;
    }
    else if ([[BasketController sharedInstance] doesUserHavePermissionTo:POSPermissionAddReturnItem])
    {
        [(ContextViewController *)self.splitViewController showBarcodeCaptureWithBarcodeUseType:BarcodeUseTypeReturn];
        return YES;
    }
    else
    {
        [POSFuncController presentPermissionAlert:POSPermissionAddReturnItem
                                        withBlock:nil];
        return NO;
    }
}

-(void)presentBasketFlightDetailsView
{
    FlightDetailsItemViewController *viewController = [[FlightDetailsItemViewController alloc] init];
    [self presentViewController:viewController];
}

- (void) showOperationsForBasketItem:(MPOSBasketItem *)basketItem
{
    [(ContextViewController *)self.splitViewController showDetailsForItem:basketItem];
}

- (BOOL) startItemLookup
{
    if ([[BasketController sharedInstance] doesUserHavePermissionTo:POSPermissionItemDetails]) {
//        [self showBarcodeCaptureWithBarcodeUseType:BarcodeUseTypeLookup];
        return YES;
    } else {
        [POSFuncController presentPermissionAlert:POSPermissionItemDetails
                                        withBlock:nil];
        return NO;
    }
}

- (BOOL) startSwipingData
{
    // listen for notifications
    [[OCGPeripheralManager sharedInstance] registerForNotifications:self
                                                       withSelector:@selector(peripheralNotification:)];
    
    // start the card reader
    [[OCGPeripheralManager sharedInstance].currentScannerDevice startMSRSwipeWithContext:kDATA_CAPTURE_SWIPE];
    
    [self presentViewController:[[SwipeCardViewController alloc] init]];
    
    return YES;
}

- (BOOL) suspendActiveBasket
{
    if ([[BasketController sharedInstance] doesUserHavePermissionTo:POSPermissionSuspendResumeSale]) {

        UIAlertView *alert =
        [[UIAlertView alloc] initWithTitle:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁹󠁯󠁈󠁏󠁱󠁧󠁦󠁫󠁖󠁐󠁆󠁄󠀰󠁋󠁖󠁗󠁌󠁲󠁊󠁥󠀱󠁷󠁕󠁉󠁉󠁧󠁅󠁿*/ @"Suspend Sale", nil)
                                   message:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁇󠁸󠁍󠁔󠁒󠁬󠁚󠁪󠁅󠁯󠁯󠁰󠀵󠀯󠁭󠁦󠁷󠁓󠁴󠀷󠁉󠁐󠁊󠁭󠁗󠁐󠁙󠁿*/ @"The current sale will be suspended and can be recalled later on.", nil)
                                  delegate:[UIAlertView class]
                         cancelButtonTitle:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁩󠁢󠁺󠁬󠁏󠁶󠁣󠁧󠁫󠁍󠁋󠁧󠁫󠀱󠁶󠁐󠁐󠀹󠁘󠀴󠁘󠁡󠁕󠁭󠁹󠁷󠀰󠁿*/ @"Cancel", nil)
                         otherButtonTitles:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠀲󠁺󠁧󠁆󠁢󠁨󠁈󠀫󠁆󠁉󠁲󠁃󠀶󠁄󠁄󠁇󠁱󠁫󠁓󠁴󠁲󠁵󠁗󠁢󠁖󠁱󠁧󠁿*/ @"Suspend", nil), nil];
        
        if ([[BasketController sharedInstance] promptForBasketDescription]) {
            alert.message = [alert.message stringByAppendingString:@"\n\n"];
            alert.message = [alert.message stringByAppendingString:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁯󠁗󠁄󠁆󠁒󠁍󠁨󠀳󠁊󠁂󠀫󠁲󠁌󠁓󠁋󠁑󠁳󠁋󠁘󠁕󠁕󠁧󠀲󠁤󠀯󠁯󠁙󠁿*/ @"Please enter a customer name for this suspended basket", nil)];
            alert.alertViewStyle = UIAlertViewStylePlainTextInput;
            __weak UITextField *textField = [alert textFieldAtIndex:0];
            textField.keyboardType = UIKeyboardTypeASCIICapable;
            textField.delegate = self;
            NSString *basketDescription = nil;
            for (id basketCustomerCard in BasketController.sharedInstance.basket.basketItems)
            {
                if ([basketCustomerCard isKindOfClass:[MPOSBasketCustomerCard class]])
                {
                    textField.text = [basketCustomerCard customerDisplayName];
                    break;
                }
            }
            
            __weak POSFuncController *weakSelf = self;
            alert.dismissBlock = ^(int buttonIndex) {
                if ((buttonIndex == 0) && (textField.text.length > 0)) {
                    [[BasketController sharedInstance] suspendBasketWithDescription:textField.text];
                }
                weakSelf.descriptionAlert = nil;
            };
            alert.shouldEnableFirstOtherButtonBlock = ^BOOL() {
                return textField.text.length > 0;
            };
        }
        else
        {
            alert.dismissBlock = ^(int buttonIndex) {
                if (buttonIndex == 0) {
                    [[BasketController sharedInstance] suspendBasketWithDescription:nil];
                }
            };
        }
        
        self.descriptionAlert = alert;
        [alert show];
        return YES;
    } else {
        [POSFuncController presentPermissionAlert:POSPermissionSuspendResumeSale
                                        withBlock:nil];
        return NO;
    }
}

- (BOOL) toggleTrainingMode
{
    if ([[BasketController sharedInstance] doesUserHavePermissionTo:POSPermissionToggleTrainingMode]) {
        [[BasketController sharedInstance] toggleTrainingMode];
        return YES;
    } else {
        [POSFuncController presentPermissionAlert:POSPermissionToggleTrainingMode
                                        withBlock:nil];
        return NO;
    }
}

#pragma mark - Barcode

/** @brief Handles notifications raised by the external payment device.
 */
- (void) peripheralNotification:(NSNotification *)note
{
    if ([note.name isEqualToString:kPeripheralNotificationCardSwiped]) {
        // card swiped
        NSString *context = note.userInfo[kPeripheralNotificationCardContextKey];
        NSString *track1 = note.userInfo[kPeripheralNotificationCardTrack1Key];
        NSString *track2 = note.userInfo[kPeripheralNotificationCardTrack2Key];
        NSString *track3 = note.userInfo[kPeripheralNotificationCardTrack3Key];
        // check the context
        if ([context isEqualToString:kDATA_CAPTURE_SWIPE]) {
            if ((track2 != nil) && (track2.length > 0))
            {
                [BasketController.sharedInstance addItemWithScanId:nil
                                                             price:nil
                                                           measure:nil
                                                          quantity:nil
                                                       entryMethod:@"SWIPED"
                                                            track1:track1
                                                            track2:track2
                                                            track3:track3
                                                       barcodeType:nil];
            }
            else
            {
            }
            // stop swiping
            [[OCGPeripheralManager sharedInstance].currentScannerDevice stopMSRSwipe];
            [[OCGPeripheralManager sharedInstance] unregisterForNotifications:self];
        } else {
            ErrorLog(@"Unexpected gift card swipe: %@", note.userInfo);
        }
    }
}

@end
