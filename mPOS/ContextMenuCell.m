//
//  ContextMenuCell.m
//  mPOS
//
//  Created by Antonio Strijdom on 14/01/2014.
//  Copyright (c) 2014 Clarity. All rights reserved.
//

#import "ContextMenuCell.h"
#import "ContextViewController.h"
#import "UIImage+CRSUniversal.h"
#import "POSFuncController.h"
#import "POSFunc.h"

@interface ContextMenuCell ()
@property (nonatomic, strong) UIButton *button;
@property (nonatomic, strong) UILabel *buttonCaption;
@end

@implementation ContextMenuCell
{
    NSLayoutConstraint *_buttonCaptionHeight;
}

#pragma mark - Properties

-(BOOL)isAccessibilityElement
{
    return YES;
}

- (void) setMenuItem:(SoftKey *)menuItem
{
    [super setMenuItem:menuItem];
    self.button.hidden = YES;
    self.buttonCaption.hidden = YES;
    if (self.menuItem) {
        if (([self.menuItem.keyType isEqualToString:@"POSFUNC"]) ||
            ([self.menuItem.keyType isEqualToString:@"MENU"]) ||
            ([self.menuItem.keyType isEqualToString:@"DISPLAY_INFO"])
            ) {
            // setup a menu item cell
            self.buttonCaption.hidden = NO;
            self.button.hidden = NO;
            // update the cell state
            if ([[POSFuncController sharedInstance] isMenuOptionActive:menuItem]) {
                self.button.alpha = 1.0f;
                self.button.opaque = YES;
                self.buttonCaption.alpha = 1.0f;
                self.buttonCaption.opaque = YES;
            } else {
                self.button.opaque = NO;
                self.button.alpha = 0.2f;
                self.buttonCaption.opaque = NO;
                self.buttonCaption.alpha = 0.2f;
            }
            // get the cell info
            UIImage *scaledImage = nil;
            NSString *title = nil;
            [POSFuncController getKeyInfoWithKey:self.menuItem
                                           title:&title
                                           image:&scaledImage];
            self.buttonCaption.text = title;
            [scaledImage imageTintedWithColor:self.foregroundColor];
            [self.button setImage:scaledImage
                         forState:UIControlStateNormal];
            // set the accessibility label
            self.accessibilityLabel = self.buttonCaption.text;
            // skin scanning button seperately
            if (POSFuncForString(self.menuItem.posFunc) == POSFuncContextMenuItemScan) {
                self.button.tag = kScanButtonSkinningTag;
            }
        }
        if ([self.menuItem.keyType isEqualToString:@"DISPLAY_INFO"])
        {
            _buttonCaptionHeight.constant = CGRectGetHeight(self.bounds);
        }
        else
        {
            _buttonCaptionHeight.constant = 15;
        }
    }
    [self updateColors];
}

- (void) setBackgroundColor:(UIColor *)backgroundColor
{
    [super setBackgroundColor:backgroundColor];
    [self updateColors];
}

- (void) setForegroundColor:(UIColor *)foregroundColor
{
    [super setForegroundColor:foregroundColor];
    [self updateColors];
}

-(void)setSelected:(BOOL)selected
{
    [super setSelected:selected];
    [self updateColors];
}

-(void)setFont:(UIFont *)font
{
    self.buttonCaption.font = font;
}

#pragma mark - Methods

- (id) initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // we are over drawing the buttons a bit, so clip to bounds
        self.contentView.clipsToBounds = YES;
        // setup the button background
        self.button = [UIButton buttonWithType:UIButtonTypeCustom];
        self.button.translatesAutoresizingMaskIntoConstraints = NO;
        self.button.userInteractionEnabled = NO;
        self.button.tag = kBasketMenuButtonSkinningTag;
        [self.contentView addSubview:self.button];
        [self.button constrain:@"top = top - 10"
                            to:self.contentView];
        [self.button constrain:@"left = left"
                            to:self.contentView];
        [self.button constrain:@"bottom = bottom"
                            to:self.contentView];
        [self.button constrain:@"right = right"
                            to:self.contentView];
        // setup the label
        self.buttonCaption = [[UILabel alloc] init];
        self.buttonCaption.backgroundColor = [UIColor clearColor];
        self.buttonCaption.translatesAutoresizingMaskIntoConstraints = NO;
        self.buttonCaption.userInteractionEnabled = NO;
        self.buttonCaption.tag = kBasketMenuButtonSkinningTag;
        self.buttonCaption.textAlignment = NSTextAlignmentCenter;
        [self.contentView addSubview:self.buttonCaption];
        [self.buttonCaption constrain:@"left = left"
                                   to:self.contentView];
        [self.buttonCaption constrain:@"bottom = bottom"
                                   to:self.contentView];
        [self.buttonCaption constrain:@"width = width"
                                   to:self.contentView];
        _buttonCaptionHeight = [self.buttonCaption constrain:@"height = 15.0"
                                                         to:nil];
    }
    return self;
}

- (NSString *) reuseIdentifier
{
    return @"ContextMenuCell";
}

- (void) prepareForReuse
{
    [super prepareForReuse];
    // default to hidden
    self.button.hidden = YES;
    self.buttonCaption.hidden = YES;
}

-(void)updateColors
{
    if (self.selected)
    {
        self.button.backgroundColor = self.foregroundColor;
        self.buttonCaption.textColor = self.backgroundColor;
    }
    else
    {
        self.button.backgroundColor = self.backgroundColor;
        self.buttonCaption.textColor = self.foregroundColor;
    }
    
    UIImage *scaledImage = [self.button imageForState:UIControlStateNormal];
    if (scaledImage != nil) {
        scaledImage = [scaledImage imageTintedWithColor:self.buttonCaption.textColor];
        [self.button setImage:scaledImage
                     forState:UIControlStateNormal];
    }
}


@end
