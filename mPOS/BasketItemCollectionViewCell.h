//
//  BasketItemCollectionViewCell.h
//  mPOS
//
//  Created by John Scott on 09/07/2014.
//  Copyright (c) 2014 Clarity. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BasketItemCollectionViewCell : UICollectionViewCell

/** @brief Method to update the cell for a basket item.
 *  @param basketItem The basket item to display.
 */
-(void)updateDisplayForBasketItem:(MPOSBasketItem*)basketItem;

/** @brief Method to update the cell for a basket item.
 *  @param basketItem The basket item to display.
 *  @param showSelection Whether the selection background is shown.
 */
-(void)updateDisplayForBasketItem:(MPOSBasketItem*)basketItem
          showSelectionBackground:(BOOL)showSelection;

/** @brief Method to update the cell for an item
 */
-(void)updateDisplayForItem:(MPOSItem*)item;

/** @brief Method to update the cell with the rewards total
 */
-(void)updateWithRewardTotal:(NSString*)rewardTotal;

@end
