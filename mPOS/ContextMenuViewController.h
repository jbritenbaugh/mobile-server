//
//  ContextMenuViewController.h
//  mPOS
//
//  Created by Antonio Strijdom on 14/01/2014.
//  Copyright (c) 2014 Clarity. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RESTController.h"

/** @file ContextMenuViewController.h */

/** @brief Block type for reporting menu selection.
 *  @param selectedMenuItem The menu item that was selected.
 *  @param switchedMenus The block was triggered from a menu switch.
 */
typedef void (^ContextMenuItemSelectedBlockType)(SoftKey *selectedMenuItem, BOOL switchedMenus);

/** @brief View controller that manages the contextual menu bar.
 */
@interface ContextMenuViewController : UIViewController

/** @property menuItemSelectedBlock
 *  @brief Called when the user selects an item on the context menu.
 *  @see ContextMenuItemSelectedBlockType
 */
@property (nonatomic, strong) ContextMenuItemSelectedBlockType menuItemSelectedBlock;

/** @property selectedMenuColor
 *  @brief The color of the highlighted menu option.
 */
@property (nonatomic, strong) UIColor *selectedMenuColor;

/** @property selectedItem
 *  @brief The currently selected menu item.
 */
@property (nonatomic, strong) SoftKey *selectedItem;

/** @property alternateCells
 *  @brief YES to display the alternate, larger, menu item cells.
 */
@property (nonatomic, assign) BOOL alternateCells;

/** @property enabled
 *  @brief Whether the menu is enabled or not.
 */
@property (nonatomic, assign) BOOL enabled;

/** @brief Reloads the menu.
 *  @param recalculate Whether or not to recalculate page metrics.
 */
- (void) reloadMenu:(BOOL)recalculate;

/** @brief Returns the key size for the keys on the currently displayed page.
 *  @return The key size.
 */
- (CGSize) keySizeForCurrentPage;

/** @brief Selects the first selectable tab.
 */
- (void) selectFirstTab;

/** @brief Sets the current menu.
 *  @param menu The new menu.
 *  @param selectFirstTab Whether or not to select the first tab.
 */
- (void)  setMenu:(SoftKeyContext *)menu
andSelectFirstTab:(BOOL)selectFirstTab;

@end
