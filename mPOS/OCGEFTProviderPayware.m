//
//  OCGEFTProviderPayware.m
//  mPOS
//
//  Created by Antonio Strijdom on 15/04/2013.
//  Copyright (c) 2013 Clarity. All rights reserved.
//

#import "OCGEFTProviderPayware.h"
#import "OCGEFTManager.h"
#import "RESTController.h"
#import "BasketController.h"

static NSString *kPAYWARE_CONFIG_RESPONDURL = @"VeriFone";
static NSString *kPAYWARE_CONFIG_BUNDLEURLNAME = @"PaywareIntegrationControllerURLIdentifier";
static NSString *kPAYWARE_REQUEST_RETURNURL = @"returnURL";

/** @brief Transaction types */

/** standard sale */
static NSString *kPAYWARE_TRANTYPE_SALE = @"SALE";
/** standard sale with gratuity prompting */
static NSString *kPAYWARE_TRANTYPE_SALEGRATUITY = @"SALE_GRATUITY";
/** standard refund */
static NSString *kPAYWARE_TRANTYPE_REFUND = @"REFUND";
/** refund by existing transaction ID */
static NSString *kPAYWARE_TRANTYPE_REFUNDID = @"REFUND_ID";

/** @brief Payware commands */
/** card is available for swiping/inserting */
static NSString *kPAYWARE_COMMAND_CARD = @"CARD";
/** card not present/not available */
static NSString *kPAYWARE_COMMAND_CNP = @"CNP";

/** @brief Payware request URL data keys */

/** Optional. An identifier which is returned back to your application. */
static NSString *kPAYWARE_REQUEST_IDENTIFIER = @"identifier";
/** Optional. Set to value of 1 to have PAYware Mobile launch and then immediately
 *  return control back to the calling application.
 *  PAYware Mobile will be retained in memory and sent to the background.
 *  Refer to Stored Transactions for more details.
 */
static NSString *kPAYWARE_REQUEST_INITPWM = @"initPWM";
/** Optional. Set to value of 1 to have PAYware Mobile immediately
 *  execute the transaction without a confirmation screen first. The
 *  default value is 0. (Confirmation screen will be displayed).
 */
static NSString *kPAYWARE_REQUEST_BYPASSCONFIRMATION = @"bypassConfirmation";
/** Optional. A valid value will bypass PAYware Mobile PIN entry screen.
 *  Otherwise, a valid PIN will be required to be entered
 *  upon launch of PAYware Mobile.
 */
static NSString *kPAYWARE_REQUEST_PIN = @"PIN";
/** Required. The amount of the transaction processed. Example: 4.95.
 */
static NSString *kPAYWARE_REQUEST_AMOUNT = @"amount";
/** Required. The type of transaction PAYware Mobile will perform.
 *  See kPAYWARE_TRANTYPE_*.
 */
static NSString *kPAYWARE_REQUEST_TRANSACTION = @"transaction";
/** Conditional. Required for transactions of type "SALE_GRATUITY".
 *  The first recommended amount prompt for a tip/gratuity on the GEN2 Device
 *  based on as a percentage of the total amount. This will default to 0.00 if not provided.
 *  Example: 15.00
 */
static NSString *kPAYWARE_REQUEST_TIP1 = @"tipPercent_1";
/** Conditional. Required for transactions of type "SALE_GRATUITY".
 *  The second recommended amount prompt for a tip/gratuity on the GEN2 Device
 *  based on as a percentage of the total amount. This will default to 0.00 if not provided.
 *  Example: 20.00
 */
static NSString *kPAYWARE_REQUEST_TIP2 = @"tipPercent_2";
/** Conditional. Required for transactions of type REFUND_ID.
 *  This would be the transaction ID of the initial transaction that is being refunded.
 *  In addition, for some regions the original transaction ID is also required for
 *  transactions of type REFUND. In such cases the original transaction ID
 *  would be passed in this field.
 */
static NSString *kPAYWARE_REQUEST_REFUNDTRANSID = @"transID";
/** Optional. The type of command PAYware Mobile will perform.
 *  If this is not provided, PAYware Mobile will prompt for command type before
 *  executing transaction.
 *  See kPAYWARE_COMMAND_*.
 */
static NSString *kPAYWARE_REQUEST_COMMAND = @"command";
/** Optional. A reference identifying the transaction for submission to the gateway
 *  and stored with the transaction. Example reference ID would be an invoice number.
 */
static NSString *kPAYWARE_REQUEST_REFERENCEID = @"referenceID";
/** Optional. The e-mail address of the owner of the credit card.
 *  If present, must contain only one @ symbol with characters before and after.
 *  Example: mail@mail.com
 */
static NSString *kPAYWARE_REQUEST_EMAIL = @"emailAddress";
/** Optional. Text label displayed at the top of the PAYware Mobile screen when
 *  transaction is processing.
 */
static NSString *kPAYWARE_REQUEST_APPLABEL = @"appLabel";
/** Optional. Set to value of 1 to have PAYware Mobile return an XML string
 *  containing the formatted customer and merchant receipts.
 */
static NSString *kPAYWARE_REQUEST_RECEIPT = @"returnReceiptData";
/** Optional. Used to retry a stored transaction. */
static NSString *kPAYWARE_REQUEST_RETRY_TRAN = @"retryStoredTran";
/** Optional. Set to 1 to have PAYware Mobile return the track data of the card
 *  without performing any transaction. This is useful for reading loyalty and other
 *  nonpayment cards.
 *  The default value is 0. If this parameter is set to 1, then any parameters
 *  related to transaction processing will be ignored.
 */
static NSString *kPAYWARE_REQUEST_GETCARDINFO = @"getCardInfo";

/** @brief Hard coded retry responseMsgs */
static NSString *kPAYWARE_RETRY_PENDING = @"There is a previously unfinished transaction. Please start PAYware Mobile and complete that transaction before starting a new transaction";
/** Stored transaction was successfully sent to succeeded gateway. */
static NSString *kPAYWARE_RETRY_SUCCEEDED = @"stored transaction retry succeeded";
/** Attempt to send retry failed. */
static NSString *kPAYWARE_RETRY_FAILED = @"stored transaction retry failed response";
/** Attempt to send retry failed due to communication error
 *  (transaction still stored).
 */
static NSString *kPAYWARE_RETRY_COMMS_FAILED = @"stored transaction retry failed communication error";
/** User pressed "Retry Later" button.
 *  (transaction still stored).
 */
static NSString *kPAYWARE_RETRY_DELAYED = @"stored transaction retry delayed";

/** @brief Payware response URL data keys */

/** This data element is echoed from the request.
 */
static NSString *kPAYWARE_RESULT_IDENTIFIER = @"identifier";
/** If the returned URL has data, then this value will be set to 1; otherwise, 0.
 */
static NSString *kPAYWARE_RESULT_HASDATA = @"hasData";
/** The transaction ID is a unique identifier for the transaction.
 */
static NSString *kPAYWARE_RESULT_TRANSID = @"transID";
/** The type of transaction which was processed in the corresponding request.
 */
static NSString *kPAYWARE_RESULT_TRANSACTION = @"transaction";
/** The authorization code received from the issuing bank.
 */
static NSString *kPAYWARE_RESULT_AUTHCODE = @"authCode";
/** The original amount of the transaction processed before tip.
 */
static NSString *kPAYWARE_RESULT_AMOUNT = @"amount";
/** The total amount of the tip/gratuity for the transaction processed.
 */
static NSString *kPAYWARE_RESULT_TIPAMOUNT = @"tipAmount";
/** The total amount of the transaction processed including tip.
 */
static NSString *kPAYWARE_RESULT_TOTALCHARGE = @"totalCharge";
/** Indicates, the request was received by PAYware Mobile, but an error occurred.
 *  See kPaywareIntegrationErrorCode enum.
 */
static NSString *kPAYWARE_RESULT_ISERROR = @"isError";
/** Transaction specific data that is returned by the processor, bank,
 *  or PAYware Mobile. Varies by transaction type and processor.
 */
static NSString *kPAYWARE_RESULT_RESPONSEMSG = @"responseMsg";
/** An XML string containing the formatted merchant and customer receipts.
 */
static NSString *kPAYWARE_RESULT_RECEIPT = @"receiptData";
/** For transactions that include signature, this field contains a base64 encoded string
 *  that represents the jpg of the signature.
 */
static NSString *kPAYWARE_RESULT_SIG = @"receiptSignature";
/** When the getCardInfo parameter is set to 1, this value will be set to the
 *  track 1 data from the swiped card.
 */
static NSString *kPAYWARE_RESULT_TRACK1DATA = @"track1Data";
/** When the getCardInfo parameter is set to 1, this value will be set to the
 *  track 2 data from the swiped card.
 */
static NSString *kPAYWARE_RESULT_TRACK2DATA = @"track2Data";
/** When the getCardInfo parameter is set to 1, this value will be set to the
 *  track 3 data from the swiped card.
 */
static NSString *kPAYWARE_RESULT_TRACK3DATA = @"track3Data";
/** OCGPay returns the masked card number in this field, which we pass to the
 *  server for receipt generation.
 */
static NSString *kPAYWARE_RESULT_MASKED_PAN = @"maskedPAN";
/** OCGPay returns the masked card expiry date in this field, which we pass to
 *  the server for receipt generation.
 */
static NSString *kPAYWARE_RESULT_MASKED_EXPIRY = @"maskedExpiry";
/** OCGPay returns a provider of US to indicate the transaction wasn't processed
 *  by payware. This is needed for server receipt generation.
 */
static NSString *kPAYWARE_RESULT_PROVIDER = @"provider";

@interface OCGEFTProviderPayware ()
@property (nonatomic, readonly) NSString *appName;
@property (nonatomic, readonly) NSString *PIN;
@property (nonatomic, readonly) NSString * bypassConfirmationSetting;
@end

@implementation OCGEFTProviderPayware

#pragma mark - Properties

- (NSString *) appName
{
    NSString *result = nil;
    
    result = BasketController.sharedInstance.clientSettings.DRAFT_verifoneAppName;
    
    return result;
}

- (NSString *) PIN
{
    NSString *result = nil;
    
    result = BasketController.sharedInstance.clientSettings.DRAFT_verifonePIN;
    
    return result;
}

- (NSString *) bypassConfirmationSetting
{
    BOOL result = BasketController.sharedInstance.clientSettings.DRAFT_verifoneBypassConfirmation;
    
    return result ? @"1" : @"0";
}

#pragma mark - Methods

+ (NSString *) returnURLScheme
{
    NSString *returnURLScheme = nil;
    
    for (NSDictionary *urlType in [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleURLTypes"])
    {
        if ([urlType[@"CFBundleURLName"] isEqualToString:kPAYWARE_CONFIG_BUNDLEURLNAME])
        {
            returnURLScheme = urlType[@"CFBundleURLSchemes"][0];
            break;
        }
    }
    return returnURLScheme;
}

+ (NSString *) providerURL
{
    return @"com-verifone-paywareVX";
}

- (ServerError *) getLastServerError
{
    ServerError *error = nil;
    
    // get error keys
    NSString *errorCodeVal = self.lastReplyDict[kPAYWARE_RESULT_ISERROR];
    NSInteger errorCode = [errorCodeVal integerValue];
    NSString *errorText = self.lastReplyDict[kPAYWARE_RESULT_RESPONSEMSG];
    
    // if there was an error
    if (errorCode != kEFTProviderErrorCodeSuccess) {
        // build the error
        ServerErrorMessage *message = [[ServerErrorMessage alloc] init];
        message.code = [NSNumber numberWithInteger:errorCode];
        message.value = errorText;
        error = [[ServerError alloc] init];
        error.messages = [NSArray arrayWithObject:message];
    }
    
    // return
    return error;
}

- (BasketTender *) updatePaymentFromDictionary:(NSDictionary *)options
{    
    // create the payment
    VerifonePayment *verifonePayment = [[VerifonePayment alloc] init];
    verifonePayment.tender = self.manager.currentPayment.tender;
    verifonePayment.basketTenderId = self.manager.currentPayment.basketTenderId;
    verifonePayment.basket = self.manager.currentPayment.basket;
    verifonePayment.salesBasketId = self.manager.currentPayment.basketId;
    verifonePayment.tenderType = self.manager.currentPayment.tender.tenderType;
    
    verifonePayment.paymentId = nil;
    verifonePayment.transaction = [options objectForKey:kPAYWARE_RESULT_TRANSACTION];
    verifonePayment.transId = [options objectForKey:kPAYWARE_RESULT_TRANSID];
    
    NSString *amountString = [options objectForKey:kPAYWARE_RESULT_AMOUNT];
    NSNumber *amount = [NSNumber numberWithFloat:amountString.floatValue];
    verifonePayment.amount = amountString;
    
    amountString = [options objectForKey:kPAYWARE_RESULT_TIPAMOUNT];
    amount = [NSNumber numberWithFloat:amountString.floatValue];
    verifonePayment.tipAmount = amountString;
    
    amountString = [options objectForKey:kPAYWARE_RESULT_TOTALCHARGE];
    amount = [NSNumber numberWithFloat:amountString.floatValue];
    verifonePayment.totalCharge = amountString;
    
    verifonePayment.authCode = [options objectForKey:kPAYWARE_RESULT_AUTHCODE];
    verifonePayment.hasDataValue = [[options objectForKey:kPAYWARE_RESULT_HASDATA] boolValue];
    verifonePayment.identifier = [options objectForKey:kPAYWARE_RESULT_IDENTIFIER];
    verifonePayment.isError = [options objectForKey:kPAYWARE_RESULT_ISERROR];
    verifonePayment.receiptData = [options objectForKey:kPAYWARE_RESULT_RECEIPT];
    verifonePayment.receiptSignature = [options objectForKey:kPAYWARE_RESULT_SIG];
    verifonePayment.responseMsg = [options objectForKey:kPAYWARE_RESULT_RESPONSEMSG];
    verifonePayment.track1Data = [options objectForKey:kPAYWARE_RESULT_TRACK1DATA];
    verifonePayment.track2Data = [options objectForKey:kPAYWARE_RESULT_TRACK2DATA];
    verifonePayment.track3Data = [options objectForKey:kPAYWARE_RESULT_TRACK3DATA];
    verifonePayment.maskedPAN = [options objectForKey:kPAYWARE_RESULT_MASKED_PAN];
    verifonePayment.maskedExpiry = [options objectForKey:kPAYWARE_RESULT_MASKED_EXPIRY];
    verifonePayment.provider = [options objectForKey:kPAYWARE_RESULT_PROVIDER];
    verifonePayment.cardScheme = [options valueForKey:@"cardScheme"];
    
    return verifonePayment;
}

- (NSString *) buildOptionStringFromDict:(NSDictionary *)options
{
    NSString *result = nil;
    
    if (options) {
        NSMutableDictionary *finalParams = [options mutableCopy];
        // add the app name
        [finalParams setObject:self.appName
                        forKey:kPAYWARE_REQUEST_APPLABEL];
        // and return URL
        
        NSString *returnURL = [[self class] returnURLScheme];
        
        if ([finalParams objectForKey:@"identifier"])
        {
            returnURL = [returnURL stringByAppendingFormat:@":%@", [finalParams objectForKey:@"identifier"]];
        }
        
        [finalParams setObject:returnURL
                        forKey:kPAYWARE_REQUEST_RETURNURL];
        
        NSMutableString *params = [NSMutableString string];
        for (int i = 0; i < [finalParams allKeys].count; i++) {
            NSString *name = [[finalParams allKeys] objectAtIndex:i];
            NSString *param = nil;
            if (i == 0) {
                param = [NSString stringWithFormat:@"?%@=%@", name, [finalParams objectForKey:name]];
            } else {
                param = [NSString stringWithFormat:@"&%@=%@", name, [finalParams objectForKey:name]];
            }
            [params appendString:param];
        }
        result = [NSString stringWithFormat:@"%@%@", kPAYWARE_CONFIG_RESPONDURL, params];
    }
    
    return result;
}

- (NSDictionary *) buildDictionaryFromURL:(NSURL *)url
{
    NSDictionary *result = nil;
    DebugLog(@"Parsing Payware response");
    
    NSString *URLString = [url absoluteString];
    DebugLog(@"Received URL: %@", URLString);
    NSString *decodedURLString =
    [URLString stringByReplacingPercentEscapesUsingEncoding:NSASCIIStringEncoding];
    
    // split params and the URL address
    NSArray *URLComponents = [decodedURLString componentsSeparatedByString:@"?"];
    if ([URLComponents count] > 1) {
        NSString *addressString = [URLComponents objectAtIndex:0];
        NSString *paramString = [URLComponents objectAtIndex:1];
        // make sure data is from Payware
        if ([addressString hasSuffix:kPAYWARE_CONFIG_RESPONDURL]) {
            // process params
            NSArray *paramArray = [paramString componentsSeparatedByString:@"&"];
            if ([paramArray count] > 0) {
                DebugLog(@"Processing %i name/value pairs", [paramArray count]);
                NSMutableDictionary *nvpDict = [[NSMutableDictionary alloc] initWithCapacity:[paramArray count]];
                for (NSString *nvpString in paramArray) {
                    // split into name and value
                    // get the pos of the first '='
                    NSRange equalsRange = [nvpString rangeOfString:@"="];
                    if (equalsRange.location != NSNotFound) {
                        NSString *name = [nvpString substringToIndex:equalsRange.location];
                        NSString *value = [nvpString substringFromIndex:equalsRange.location + 1];
                        // fix card swipe data
                        if ([name hasPrefix:@"track"]) {
                            // ? == |
                            value = [value stringByReplacingOccurrencesOfString:@"|"
                                                                     withString:@"?"];
                            // = == +
                            value = [value stringByReplacingOccurrencesOfString:@"+"
                                                                     withString:@"="];
                        }
                        [nvpDict setObject:value
                                    forKey:name];
                    } else {
                        DebugLog(@"Couldn't parse name/value pair from data: %@", nvpString);
                    }
                }
                
                NSArray *addressStringComponents = [addressString componentsSeparatedByString:@":"];
#if DEBUG
                DebugLog(@"addressStringComponents: %@", addressStringComponents);
#endif
                
                if ([addressStringComponents count] > 2)
                {
                    [nvpDict setObject:[addressStringComponents objectAtIndex:1] forKey:@"identifier"];
                }
                
                result = nvpDict;
            } else {
                DebugLog(@"No name/value pairs to process");
                result = nil;
            }
        } else {
            DebugLog(@"Response isn't from Payware!");
            result = nil;
        }
    } else {
        DebugLog(@"URL didn't contain complete reply");
        result = nil;
    }
    
    return result;
}

- (BOOL) isRetryRequest:(NSDictionary *)requestDict
{
    BOOL result = NO;
    
    NSString *retryParam = [requestDict objectForKey:kPAYWARE_REQUEST_RETRY_TRAN];
    if (retryParam) {
        result = [retryParam isEqualToString:@"1"];
    }
    
    return result;
}

- (EFTProviderErrorCode) checkForErrorInResponse:(NSDictionary *)params
{
    NSInteger result = kEFTProviderErrorCodeSuccess;
    
    if (params) {
        NSString *errorVal = [params objectForKey:kPAYWARE_RESULT_ISERROR];
        if (errorVal) {
            result = [errorVal integerValue];
            // error is any error code other than 0
            if (result != kEFTProviderErrorCodeSuccess) {
                DebugLog(@"Payware request failed with error: %i", result);
            }
        }
    }
    
    return result;
}

- (EFTPaymentCompleteReason) updateStatusFromSuccessfulResponse:(NSDictionary *)response
{
    EFTPaymentCompleteReason result = kEFTPaymentCompleteReasonApproved;
    
    // we got a valid reply, therefore we're in a sled
    self.status = kEFTProviderStatusAvailable;
    // check for pending transaction status messages
    NSString *responseMsg = [response objectForKey:kPAYWARE_RESULT_RESPONSEMSG];
    if (responseMsg) {
        if ([responseMsg isEqualToString:kPAYWARE_RETRY_COMMS_FAILED] ||
            [responseMsg isEqualToString:kPAYWARE_RETRY_DELAYED] ||
            [responseMsg isEqualToString:kPAYWARE_RETRY_PENDING]) {
            // we have a pending transaction
            self.status = kEFTProviderStatusStoredPending;
            result = kEFTPaymentCompleteReasonError;
        }
    }
    
    return result;
}

- (void) updateStatusFromFailedResponse:(NSDictionary *)response
                          withErrorCode:(EFTProviderErrorCode)errorCode
{
    // check if we are in a retry state
    if ((errorCode == kEFTProviderErrorCodeSaveCommsFailure) ||
        (errorCode == kEFTProviderErrorCodeRetryCommsFailure)) {
        // disable payware until retry is completed
        self.status = kEFTProviderStatusStoredPending;
    } else {
        // check for pending transaction status messages
        NSString *responseMsg = [response objectForKey:kPAYWARE_RESULT_RESPONSEMSG];
        if (responseMsg) {
            if ([responseMsg isEqualToString:kPAYWARE_RETRY_PENDING]) {
                // we have a pending transaction
                self.status = kEFTProviderStatusStoredPending;
            } else {
                self.status = kEFTProviderStatusAvailable;
            }
        } else {
            self.status = kEFTProviderStatusAvailable;
        }
    }
}

- (EFTProviderErrorCode) retryStoredTransaction
{    
    EFTProviderErrorCode result = kEFTProviderErrorCodeSuccess;
    
    // build request
    NSMutableDictionary *options = [[NSMutableDictionary alloc] init];
    // required
    [options setObject:@"1"
                forKey:kPAYWARE_REQUEST_RETRY_TRAN];
    // optional
    [options setObject:self.bypassConfirmationSetting
                forKey:kPAYWARE_REQUEST_BYPASSCONFIRMATION];
    result = [self sendRequestWithParams:options];
    
    // done
    return result;
}

- (void) cardSaleWithTender:(Tender *)tender
                     amount:(NSNumber *)amount
            forBasketWithID:(NSString *)basketId
          withCustomerEmail:(NSString *)customerEmail
                   complete:(EFTProviderCompleteBlock)complete
{
    [super cardSaleWithTender:tender
                       amount:amount
              forBasketWithID:basketId
            withCustomerEmail:customerEmail
                     complete:^(EFTProviderErrorCode resultCode, NSError *error, BOOL suppressError) {
                         // init
                         EFTProviderErrorCode result = resultCode;
                         
                         if (result == kEFTProviderErrorCodeSuccess) {
                             // build request
                             NSMutableDictionary *options = [[NSMutableDictionary alloc] init];
                             
                             // required
                             [options setObject:basketId
                                         forKey:kPAYWARE_REQUEST_REFERENCEID];
                             [options setObject:[NSString stringWithFormat:@"%010.2f", amount.doubleValue]
                                         forKey:kPAYWARE_REQUEST_AMOUNT];
                             if (tender.promptForTip) {
                                 [options setObject:kPAYWARE_TRANTYPE_SALEGRATUITY
                                             forKey:kPAYWARE_REQUEST_TRANSACTION];
                             } else {
                                 [options setObject:kPAYWARE_TRANTYPE_SALE
                                             forKey:kPAYWARE_REQUEST_TRANSACTION];
                             }
                             
                             // optional
                             
                             if ((customerEmail != nil)
                                 && ([customerEmail length] > 0)) {
                                 
                                 [options setObject: customerEmail
                                             forKey: kPAYWARE_REQUEST_EMAIL];
                             }
                             
                             // We need to set this so we can get the basket id back for sending the results to the server.
                             
                             [options setObject:basketId forKey:kPAYWARE_REQUEST_IDENTIFIER];
                             
                             
                             [options setObject:self.bypassConfirmationSetting
                                         forKey:kPAYWARE_REQUEST_BYPASSCONFIRMATION];
                             [options setObject:kPAYWARE_COMMAND_CARD
                                         forKey:kPAYWARE_REQUEST_COMMAND];
                             [options setObject:@"1" forKey:kPAYWARE_REQUEST_RECEIPT];
                             NSNumber *gratuity = [BasketController sharedInstance].clientSettings.presetTip1;
                             NSNumber *gratuity2 = [BasketController sharedInstance].clientSettings.presetTip2;
                             if (gratuity && tender.promptForTip) {
                                 [options setObject:[NSString stringWithFormat:@"%02.2f", gratuity.doubleValue]
                                             forKey:kPAYWARE_REQUEST_TIP1];
                             } else {
                                 [options setObject:@"0.00" forKey:kPAYWARE_REQUEST_TIP1];
                             }
                             if (gratuity2 && tender.promptForTip) {
                                 [options setObject:[NSString stringWithFormat:@"%02.2f", gratuity2.doubleValue]
                                             forKey:kPAYWARE_REQUEST_TIP2];
                             } else {
                                 [options setObject:@"0.00" forKey:kPAYWARE_REQUEST_TIP2];
                             }
                             result = [self sendRequestWithParams:options];
                         }
                         
                         // done
                         if (kEFTProviderErrorCodeSuccess != result) {
                             // let the manager know there was an error
                             complete(result, error, NO);
                         } else {
                             // don't call complete here, as the returning URL will continue the payment later
                         }
                     }];
}

- (void) cardRefundWithTender:(Tender *)tender
                   withAmount:(NSNumber *)amount
              forBasketWithID:(NSString *)basketId
            withCustomerEmail:(NSString *)customerEmail
                     complete:(EFTProviderCompleteBlock)complete
{
    [super cardRefundWithTender:tender
                     withAmount:amount
                forBasketWithID:basketId
              withCustomerEmail:customerEmail
                       complete:^(EFTProviderErrorCode resultCode, NSError *error, BOOL suppressError) {
                           // init
                           EFTProviderErrorCode result = resultCode;
                           
                           if (result == kEFTProviderErrorCodeSuccess) {
                               // build request
                               NSMutableDictionary *options = [[NSMutableDictionary alloc] init];
                               
                               // required
                               [options setObject:basketId
                                           forKey:kPAYWARE_REQUEST_REFERENCEID];
                               [options setObject:[NSString stringWithFormat:@"%010.2f", amount.doubleValue]
                                           forKey:kPAYWARE_REQUEST_AMOUNT];
                               [options setObject:kPAYWARE_TRANTYPE_REFUND
                                           forKey:kPAYWARE_REQUEST_TRANSACTION];
                               
                               // optional
                               
                               if ((customerEmail != nil)
                                   && ([customerEmail length] > 0)) {
                                   
                                   [options setObject: customerEmail
                                               forKey: kPAYWARE_REQUEST_EMAIL];
                               }
                               
                               // We need to set this so we can get the basket id back for sending the results to the server.
                               
                               [options setObject:basketId forKey:kPAYWARE_REQUEST_IDENTIFIER];
                               
                               
                               [options setObject:self.bypassConfirmationSetting
                                           forKey:kPAYWARE_REQUEST_BYPASSCONFIRMATION];
                               [options setObject:kPAYWARE_COMMAND_CARD
                                           forKey:kPAYWARE_REQUEST_COMMAND];
                               [options setObject:@"1" forKey:kPAYWARE_REQUEST_RECEIPT];
                               [options setObject:@"0.00" forKey:kPAYWARE_REQUEST_TIP1];
                               [options setObject:@"0.00" forKey:kPAYWARE_REQUEST_TIP2];
                               result = [self sendRequestWithParams:options];
                           }
                           
                           // done
                           complete(result, error, NO);
                       }];
}

- (BOOL) isProviderURL:(NSURL *)url
{
    // First check we're responding to the correct scheme for this class.
    
    if (![url.absoluteString hasPrefix:[[self class] returnURLScheme]])
    {
        return NO;
    }
    
    
    BOOL result = NO;
    
    // log it
    [self logPaymentURL:url
               incoming:YES];
    
    DebugLog(@"Validating URL: %@", url);
    
    NSString *URLString = [url absoluteString];
    NSString *decodedURLString =
    [URLString stringByReplacingPercentEscapesUsingEncoding:NSASCIIStringEncoding];
    
    // split params and the URL address
    NSArray *URLComponents = [decodedURLString componentsSeparatedByString:@"?"];
    if ([URLComponents count] > 1) {
        NSString *addressString = [URLComponents objectAtIndex:0];
        // make sure data is from Payware
        if ([addressString hasSuffix:kPAYWARE_CONFIG_RESPONDURL]) {
            DebugLog(@"URL is a valid payware URL");
            result = YES;
        }
    }
    
    return result;
}

- (BasketTender *) updatePaymentFromTransaction
{
    NSURL *url = [NSURL URLWithString:self.manager.currentPayment.transaction];
    NSDictionary *params = [self buildDictionaryFromURL:url];
    return [self updatePaymentFromDictionary:params];
}

@end
