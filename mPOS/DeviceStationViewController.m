//
//  DeviceStationViewController.m
//  mPOS
//
//  Created by Antonio Strijdom on 16/10/2013.
//  Copyright (c) 2013 Clarity. All rights reserved.
//

#import "DeviceStationViewController.h"
#import "CRSSkinning.h"
#import "BarcodeScannerController.h"
#import "BasketController.h"
#import "UIAlertView+MKBlockAdditions.h"
#import "DeviceStationStatusViewController.h"
#import "RESTController.h"
#import "ServerErrorHandler.h"
#import "UIImage+OCGExtensions.h"

typedef BOOL(^barcodeScannedBlockType)(NSString *barcode);

@interface DeviceStationViewController () <UINavigationControllerDelegate> {
    BOOL _startScanner;
}
@property (nonatomic, strong) barcodeScannedBlockType scannedBlock;
@property (nonatomic, strong) deviceScanCompleteBlockType barcodeProcessedBlock;
@property (nonatomic, strong) UIImageView *promptImage;
@property (nonatomic, strong) UILabel *infoLabel;
@property (nonatomic, strong) NSString *trimmedBarcode;
@property (nonatomic, strong) NSString *infoLabelText;
@property (nonatomic, strong) DeviceStation *station;
@property (nonatomic, assign) BOOL showDismiss;
@property (nonatomic, strong) NSString *scanAddButtonTitle;
@property (nonatomic, assign) BOOL canCancel;
- (void) startScan;
- (void) processDeviceStationWithBarcode:(NSString *)barcode;
- (void) dismissTapped:(id)sender;
@end

@implementation DeviceStationViewController

#pragma mark - Private

- (void) startScan
{
    if (_startScanner) {
        _startScanner = NO;
        BarcodeScannerController *controller = [BarcodeScannerController sharedInstance];
        // are we scanning?
        if (!controller.scanning) {
            // no, start scanning
            UIViewController *parentViewController = self.view.window.rootViewController;
            if (self.splitViewController == nil) {
                parentViewController = self;
            }
            [controller beginScanningInViewController:parentViewController
                                            forReason:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁫󠁆󠁤󠁷󠁧󠁷󠁰󠁁󠁫󠁁󠁦󠁋󠁃󠀴󠁑󠀲󠁺󠀸󠁳󠁨󠁷󠁂󠀫󠀹󠁉󠀴󠀸󠁿*/ @"Device Station", nil)
                                       withButtonSize:CGSizeZero
                                         withAddTitle:self.scanAddButtonTitle
                                             mustScan:!self.canCancel
                                  alphaNumericAllowed:YES
                                            withBlock:^(NSString *barcode, ScannerEntryMethod entryMethod, SMBarcodeType barcodeType, BOOL *continueScanning) {
                                                // debug assertions
                                                NSAssert(barcode != nil,
                                                         @"The reference to the scanned barcode has not been initialized.");
                                                NSAssert([[barcode stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length] > 0,
                                                         @"The returned barcode does not contain any characters.");
                                                // we only want to scan the one barcode
                                                *continueScanning = NO;
                                                // grab the barcode we scanned
                                                self.trimmedBarcode =
                                                [barcode stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                                            }
                                           terminated:^{
                                               _startScanner = YES;
                                               if (self.trimmedBarcode != nil) {
                                                   // process the barcode
                                                   [self processDeviceStationWithBarcode:self.trimmedBarcode];
                                                   self.trimmedBarcode = nil;
                                               } else {
                                                   if (self.canCancel) {
                                                       // just dismiss the view
                                                       [self dismissViewControllerAnimated:YES
                                                                                completion:nil];
                                                   } else {
                                                       // TFS67316 - kick the scanner off again
                                                       [self viewDidAppear:NO];
                                                   }
                                               }
                                           }
                                                error:^(NSString *errorDesc) {
                                                    if (errorDesc) {
                                                        // display an alert
                                                        UIAlertView *alert =
                                                        [[UIAlertView alloc] initWithTitle:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁯󠀫󠁋󠁆󠁢󠁚󠁱󠁩󠁈󠁕󠀲󠀸󠁫󠁲󠁧󠁦󠀲󠁴󠁘󠁣󠁈󠁎󠀶󠀲󠁯󠀶󠁳󠁿*/ @"Scanning Error", nil)
                                                                                   message:errorDesc
                                                                                  delegate:nil
                                                                         cancelButtonTitle:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁄󠁴󠁍󠁉󠁣󠀫󠁺󠁔󠁗󠁦󠁹󠁡󠁯󠀷󠁨󠁔󠁩󠀸󠁯󠀫󠁨󠁘󠀹󠁱󠁌󠀶󠀸󠁿*/ @"OK", nil)
                                                                         otherButtonTitles:nil];
                                                        [alert show];
                                                    }
                                                }];
        }
    }
}

- (void) processDeviceStationWithBarcode:(NSString *)barcode
{
    // make sure there was a barcode scanned
    if ((barcode != nil) && (![barcode isEqualToString:@""])) {
        // attempt to link to this station
        ocg_async_background_overlay(^{
            // let the block process the barcode
            if (self.scannedBlock != nil) {
                if (self.scannedBlock(barcode)) {
                    // barcode processed ok
                    if (self.barcodeProcessedBlock != nil) {
                        dispatch_async(dispatch_get_main_queue(), ^{
                            self.barcodeProcessedBlock(barcode);
                        });
                    }
                }
            }
        });
    }
}

- (void) dismissTapped:(id)sender
{
    [[BarcodeScannerController sharedInstance] stopScanning];
}

#pragma mark - UIViewController

- (id) initWithNibName:(NSString *)nibNameOrNil
                bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil
                           bundle:nibBundleOrNil];
    if (self) {
        self.scannedBlock = nil;
        self.barcodeProcessedBlock = nil;
        self.trimmedBarcode = nil;
        self.scanAddButtonTitle = nil;
        self.canCancel = YES;
    }
    
    return self;
}

- (void) viewDidLoad
{
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    // image
    self.promptImage = [[UIImageView alloc] init];
    self.promptImage.translatesAutoresizingMaskIntoConstraints = NO;
    [self.view addSubview:self.promptImage];
    [self.promptImage constrain:@"left = left" to:self.view];
    [self.promptImage constrain:@"right = right" to:self.view];
    [self.promptImage constrain:@"height = 50" to:nil];
    [self.promptImage constrain:@"top = top + 20" to:self.view];
    self.promptImage.contentMode = UIViewContentModeBottom;
    self.promptImage.image = [UIImage OCGExtensions_imageNamed:@"CONTEXT_MENU_ITEM_SCAN"];
    // message label
    self.infoLabel = [[UILabel alloc] init];
    self.infoLabel.translatesAutoresizingMaskIntoConstraints = NO;
    
    [self.view addSubview:self.infoLabel];
    [self.infoLabel constrain:@"left = left + 5" to:self.view];
    [self.infoLabel constrain:@"right = right - 5" to:self.view];
    [self.infoLabel constrain:@"top = bottom + 5" to:self.promptImage];
    [self.infoLabel constrain:@"height = 42" to:nil];
    self.infoLabel.textAlignment = NSTextAlignmentCenter;
    self.infoLabel.adjustsFontSizeToFitWidth = YES;
    self.infoLabel.minimumScaleFactor = 0.5;
    self.infoLabel.numberOfLines = 2;
    self.infoLabel.font = [UIFont systemFontOfSize:20];
    [[CRSSkinning currentSkin] applyViewSkin:self];
}

- (void) didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) viewWillAppear:(BOOL)animated
{
    self.title = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁫󠁆󠁤󠁷󠁧󠁷󠁰󠁁󠁫󠁁󠁦󠁋󠁃󠀴󠁑󠀲󠁺󠀸󠁳󠁨󠁷󠁂󠀫󠀹󠁉󠀴󠀸󠁿*/ @"Device Station", nil);
    [[CRSSkinning currentSkin] applyViewSkin:self];
    [super viewWillAppear:animated];
    // TFS 58535 - display a dismiss button (if required)
    if (self.showDismiss) {
        UIBarButtonItem *dismissButton =
        [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel
                                                      target:self
                                                      action:@selector(dismissTapped:)];
        self.navigationItem.rightBarButtonItem = dismissButton;
    } else {
        self.navigationItem.rightBarButtonItem = nil;
    }
    // update the info label
    self.infoLabel.text = self.infoLabelText;
}

- (void) viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    // display the scanner
    dispatch_async(dispatch_get_main_queue(), ^{
        [self startScan];
    });
}

- (void) dealloc
{
    if (self.navigationController) {
        self.navigationController.delegate = nil;
    }
}

#pragma mark - UINavigationControllerDelegate

- (id <UIViewControllerAnimatedTransitioning>)navigationController:(UINavigationController *)navigationController
                                   animationControllerForOperation:(UINavigationControllerOperation)operation
                                                fromViewController:(UIViewController *)fromVC
                                                  toViewController:(UIViewController *)toVC
{
    if ([fromVC isKindOfClass:[DeviceStationStatusViewController class]]) {
        if (toVC == self) {
            _startScanner = YES;
        }
    }
    
    return nil;
}

#pragma mark - Methods

- (void) presentAlertForDeviceStationError:(ServerErrorMessage*)errorCode
                       withDeviceStationId:(NSString *)deviceStationId
                      andDeviceStationName:(NSString *)deviceStationName
                                 andTillId:(NSString *)tillId
                               andTerminal:(NSString *)terminal
                                   andUser:(NSString *)user
                 andUnavailablePeripherals:(NSString *)unavailable
                                 dismissed:(dispatch_block_t)dismissedBlock
{
    // build the message based on the error code
    NSString *message = nil;
    switch ([errorCode.code integerValue]) {
        case MShopperDeviceStationNotFoundExceptionCode: {
            message = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠀸󠁅󠁑󠀱󠁕󠁃󠁪󠁧󠁶󠁡󠁨󠁪󠁴󠁴󠀶󠁺󠀷󠁅󠀶󠀰󠁋󠁳󠁦󠁭󠁷󠁳󠁁󠁿*/ @"The device station entered was not found.", nil);
            break;
        }
        case MShopperDeviceStationClaimedExceptionCode: {
            message = [NSString stringWithFormat:@"%@ %@ %@ %@.",
                       NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠀯󠀶󠁳󠁹󠀶󠁕󠁵󠀸󠁐󠁙󠁖󠁪󠁊󠁌󠁦󠁄󠁕󠁇󠁖󠁣󠁨󠁘󠀯󠁨󠁗󠁪󠁷󠁿*/ @"The device station has already been claimed on terminal", nil),
                       terminal,
                       NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁷󠁹󠁦󠁦󠁹󠁌󠁦󠁷󠁎󠁎󠀱󠁬󠁣󠁪󠁢󠀴󠁗󠁶󠁧󠁯󠁇󠁪󠁔󠁘󠁤󠁕󠀸󠁿*/ @"by user", nil),
                       user];
            break;
        }
        case MShopperDeviceStationConfigurationErrorExceptionCode: {
            message = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠀱󠁩󠁚󠁌󠁏󠁫󠁙󠁷󠁵󠀲󠀵󠁊󠁮󠁚󠁶󠁇󠁮󠁭󠁴󠁭󠀷󠁋󠁩󠁊󠁪󠁧󠀴󠁿*/ @"Device station configuration error.", nil);
            break;
        }
        case MShopperDeviceStationDeviceErrorExceptionCode: {
            message = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁷󠁎󠁵󠁕󠁥󠁔󠁓󠁅󠁧󠁏󠁑󠁫󠁄󠁋󠁉󠁳󠁕󠁏󠁲󠁷󠁧󠁣󠁲󠀯󠀫󠀸󠁙󠁿*/ @"There was an error with the device station.", nil);
            break;
        }
        case MShopperDeviceStationNotClaimedExceptionCode: {
            message = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁖󠀫󠁸󠁔󠀯󠁲󠁪󠁰󠁩󠁸󠁃󠁁󠁖󠁱󠀵󠁍󠀶󠁃󠁥󠁰󠁳󠁗󠁌󠁦󠁂󠁰󠀴󠁿*/ @"A device station has not been claimed. Please select a device station.", nil);
            break;
        }
        case MShopperDeviceStationUnavailableExceptionCode: {
            message = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁎󠁖󠁙󠁚󠀸󠁸󠁢󠁫󠁊󠁨󠀯󠀵󠀶󠁘󠁓󠁉󠁍󠀶󠁮󠁋󠁐󠁪󠁥󠁫󠁰󠁬󠀴󠁿*/ @"The device station is unavailable.", nil);
            break;
        }
        case MShopperDeviceStationDrawerNotAssignedExceptionCode: {
            message = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁂󠁧󠀰󠀵󠁍󠁅󠁣󠁙󠁈󠁐󠀴󠁶󠁸󠁹󠁙󠁂󠁏󠁖󠁤󠁄󠁩󠁗󠀫󠁷󠁶󠁴󠁧󠁿*/ @"No drawer insert is assigned to this station. Please assign a drawer insert.", nil);
            break;
        }
        case MShopperDeviceStationPrinterCoverOpenExceptionCode: {
            message = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁮󠁑󠁁󠁶󠁋󠁉󠁦󠁮󠁅󠀳󠁰󠁲󠁴󠀹󠀶󠁓󠀵󠁁󠁷󠀴󠁆󠁏󠁗󠁂󠁭󠁕󠁳󠁿*/ @"The printer cover is open.", nil);
            break;
        }
        case MShopperDeviceStationPrinterPaperLowExceptionCode: {
            message = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁋󠁅󠀴󠀷󠁹󠁔󠁨󠁐󠁴󠁫󠀹󠁑󠀯󠁭󠁨󠀶󠁚󠁴󠁳󠁈󠁘󠁕󠁫󠁉󠀶󠁆󠁣󠁿*/ @"The printer is low on paper.", nil);
            break;
        }
        case MShopperDeviceStationPrinterPaperOutExceptionCode: {
            message = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁮󠁢󠁱󠁦󠁨󠁵󠁙󠁌󠀹󠁢󠁆󠁌󠁊󠁲󠀷󠁎󠀷󠁒󠁘󠁪󠁆󠁯󠁏󠁧󠁕󠀴󠁯󠁿*/ @"The printer is out of paper.", nil);
            break;
        }
        case MShopperDeviceStationPrinterSlipInsertionRequiredExceptionCode: {
            message = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁷󠁄󠁮󠁕󠁸󠁁󠁃󠁐󠁙󠁍󠁏󠁌󠁢󠀹󠀲󠀵󠁖󠁯󠁢󠁧󠁧󠁦󠁸󠀫󠀹󠀴󠁳󠁿*/ @"Printer slip insertion required.", nil);
            break;
        }
        case MShopperDeviceStationPrinterSlipRemovalRequiredExceptionCode: {
            message = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁓󠀸󠁗󠀶󠁓󠁩󠁖󠁌󠁤󠁸󠀱󠀰󠁺󠁏󠁩󠁰󠁪󠁖󠀷󠁯󠁖󠁅󠀫󠁓󠀷󠁧󠁙󠁿*/ @"Printer slip removal required.", nil);
            break;
        }
        case MShopperDeviceStationPrinterErrorExceptionCode: {
            message = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁊󠁨󠁐󠁒󠁵󠀵󠀰󠀫󠁬󠁃󠁤󠁸󠁃󠁬󠁋󠀶󠁤󠁁󠁯󠁒󠀴󠀶󠁶󠁢󠁊󠁤󠀴󠁿*/ @"There was an error with the printer.", nil);
            break;
        }
        case MShopperDeviceStationCashDrawerErrorExceptionCode: {
            message = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠀴󠁸󠁩󠁇󠀸󠁖󠀸󠁊󠁅󠁴󠁊󠁗󠁬󠀳󠁎󠁹󠁔󠁧󠁎󠁰󠁔󠁆󠁲󠀯󠁪󠁤󠁑󠁿*/ @"There was an error with the cash drawer.", nil);
            break;
        }
        case MShopperDeviceStationDrawerValueInUseExceptionCode: {
            message = [NSString stringWithFormat:@"%@ %@ %@ %@.",
                       NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁕󠁙󠁗󠁳󠁔󠀱󠁖󠁶󠁨󠀷󠁲󠁧󠀫󠁸󠁚󠀳󠁬󠀷󠀯󠁈󠁓󠁒󠁦󠁆󠁴󠀸󠀸󠁿*/ @"Drawer insert", nil),
                       tillId,
                       NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠀯󠀲󠁢󠁐󠁆󠁰󠀵󠁑󠁔󠁭󠁫󠁂󠁭󠁄󠁒󠀶󠁃󠁌󠁒󠁂󠁤󠁶󠀹󠁕󠁎󠁙󠁁󠁿*/ @"is already in use in device station", nil),
                       deviceStationId];
            break;
        }
        case MShopperDeviceStationInvalidDrawerValueProvidedExceptionCode: {
            message = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁧󠁬󠁥󠁫󠀷󠁅󠀷󠁲󠁱󠁥󠁧󠀱󠁈󠁴󠁎󠁃󠀵󠀰󠁷󠀹󠁪󠁪󠀷󠁆󠁭󠁗󠁕󠁿*/ @"The drawer insert id entered is invalid.", nil);
            break;
        }
        case MShopperDeviceStationPeripheralNotConfiguredExceptionCode: {
            // add unavailble peripherals
            NSMutableString *unavailablePeripheralString = [NSMutableString string];
            if (unavailable != nil) {
                NSArray *unavailableArray = [unavailable componentsSeparatedByString:@","];
                for (int i = 0; i < unavailableArray.count; i++) {
                    NSString *unavailablePeripheral = unavailableArray[i];
                    if ([[unavailablePeripheral uppercaseString] isEqualToString:@"PRINTER"]) {
                        [unavailablePeripheralString appendString:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁚󠁸󠁉󠁭󠁗󠁡󠁏󠁷󠁨󠁃󠁣󠁖󠀱󠀵󠁙󠀯󠁐󠁏󠁒󠁋󠁮󠁔󠁨󠀲󠁌󠁂󠁑󠁿*/ @"Printer", nil)];
                    } else if ([[unavailablePeripheral uppercaseString] isEqualToString:@"CASHDRAWER"]) {
                        [unavailablePeripheralString appendString:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁸󠁂󠁵󠁍󠁰󠁒󠀫󠁱󠀰󠁵󠁅󠁉󠁚󠁐󠁍󠁊󠁢󠀳󠁊󠁗󠁕󠁗󠁏󠁱󠁵󠁢󠁷󠁿*/ @"Cash Drawer", nil)];
                    } else {
                        [unavailablePeripheralString appendString:unavailablePeripheral];
                    }
                    if (i < unavailableArray.count - 1) {
                        [unavailablePeripheralString appendString:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁭󠁫󠁃󠁮󠀷󠁎󠁇󠁭󠁘󠁹󠁃󠁬󠁅󠁅󠁔󠁁󠁅󠁫󠀫󠁚󠁤󠁮󠀳󠁰󠁭󠁚󠀰󠁿*/ @", ", nil)];
                    }
                }
                message = [NSString stringWithFormat:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁦󠁎󠁥󠀶󠁌󠁅󠁮󠁔󠁊󠁱󠁗󠀸󠁭󠁭󠁳󠁯󠁡󠁓󠀶󠁸󠁢󠁣󠁌󠁷󠁸󠁖󠁕󠁿*/ @"No %@ - please select and scan a different device station", nil), unavailablePeripheralString];
            }
            break;
        }
        default: {
            message = [ServerErrorHandler.sharedInstance translateServerErrorMessage:errorCode withContext:nil] ;
            break;
        }
    }
    // build the alert
    UIAlertView *deviceStationAlert =
    [UIAlertView alertViewWithTitle:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁨󠁚󠁖󠁡󠁨󠁈󠁆󠁊󠁧󠁃󠁲󠀲󠁰󠁤󠁉󠁨󠁯󠀹󠁰󠁩󠀹󠁘󠁌󠁸󠁩󠀯󠁫󠁿*/ @"Device Station Error", nil)
                            message:message
                  cancelButtonTitle:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁄󠁴󠁍󠁉󠁣󠀫󠁺󠁔󠁗󠁦󠁹󠁡󠁯󠀷󠁨󠁔󠁩󠀸󠁯󠀫󠁨󠁘󠀹󠁱󠁌󠀶󠀸󠁿*/ @"OK", nil)
                  otherButtonTitles:nil
                          onDismiss:nil
                           onCancel:^{
                               if (dismissedBlock != nil)
                                   dismissedBlock();
                           }];
    [deviceStationAlert show];
}

- (void) requestDeviceScanComplete:(deviceScanCompleteBlockType)complete
                       allowCancel:(BOOL)allowCancel
{
    _startScanner = YES;
    self.infoLabelText = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁮󠀯󠁳󠁃󠁕󠁊󠁨󠁶󠀫󠁫󠁗󠁫󠁱󠁯󠁍󠀹󠁕󠁋󠁩󠁨󠁇󠁨󠁍󠁖󠀶󠁎󠁯󠁿*/ @"Please scan the device station you wish to use", nil);
    self.scanAddButtonTitle = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁈󠀰󠁋󠁩󠁩󠁥󠁥󠁙󠁦󠁰󠁆󠁰󠁓󠁪󠁔󠀯󠁙󠁫󠁶󠁵󠀫󠁭󠁱󠁤󠁗󠁢󠀰󠁿*/ @"Use", nil);
    self.canCancel = allowCancel;
    self.barcodeProcessedBlock = complete;
    __block DeviceStationViewController *vc = self;
    self.scannedBlock = ^BOOL(NSString *barcode) {
        // don't do anything with the barcode for now
        return YES;
    };
}

- (void) requestDeviceScanForLinkComplete:(deviceScanCompleteBlockType)complete
{
    _startScanner = YES;
    self.infoLabelText = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁮󠀯󠁳󠁃󠁕󠁊󠁨󠁶󠀫󠁫󠁗󠁫󠁱󠁯󠁍󠀹󠁕󠁋󠁩󠁨󠁇󠁨󠁍󠁖󠀶󠁎󠁯󠁿*/ @"Please scan the device station you wish to use", nil);
    self.scanAddButtonTitle = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁢󠀷󠀯󠁑󠁤󠁮󠀵󠁲󠁰󠀫󠀰󠁚󠀯󠀱󠁅󠁴󠁖󠁘󠁢󠁌󠁪󠁒󠁙󠁔󠁓󠁳󠁯󠁿*/ @"Link", nil);
    self.barcodeProcessedBlock = complete;
    __block DeviceStationViewController *vc = self;
    self.scannedBlock = ^BOOL(NSString *barcode) {
        __block BOOL result = NO;
        [RESTController.sharedInstance linkDeviceStationWithDeviceStationId:barcode
                                                                      force:NO
                                                                   complete:^(DeviceStation *deviceStation, NSError *restError, ServerError *restServerError)
         {
             if ((restError == nil) && (restServerError == nil)) {
                 // no errors
                 result = YES;
             } else {
                 // an error occurred
                 result = NO;
                 // get the error
                 ServerErrorMessage *errorMessage = nil;
                 for (ServerErrorMessage *message in restServerError.messages) {
                     errorMessage = message;
                     break;
                 }
                 dispatch_async(dispatch_get_main_queue(), ^{
                     // if we need to assign a drawer
                     if (errorMessage.code.integerValue == MShopperDeviceStationDrawerNotAssignedExceptionCode) {
                         // present the associate alert
                         DeviceStationStatusViewController *statusVC = [[DeviceStationStatusViewController alloc] initWithStyle:UITableViewStyleGrouped];
                         [statusVC associateStationWithId:barcode
                                                 complete:^(DeviceStation *deviceStation) {
                                                     // if we didn't link
                                                     if (deviceStation == nil) {
                                                         // start the scanner again
                                                         [vc startScan];
                                                     } else {
                                                         // try and link again
                                                         [vc processDeviceStationWithBarcode:barcode];
                                                     }
                                                 }];
                     } else {
                         NSString *errorDeviceStationId = nil;
                         NSString *errorDeviceStationName = nil;
                         NSString *errorTerminal = nil;
                         NSString *errorUser = nil;
                         NSString *errorUnavailable = nil;
                         if ([errorMessage isKindOfClass:[ServerErrorDeviceStationMessage class]]) {
                             errorDeviceStationId = [(ServerErrorDeviceStationMessage *)errorMessage deviceStationId];
                             errorDeviceStationName = [(ServerErrorDeviceStationMessage *)errorMessage deviceStationName];
                             errorTerminal = [(ServerErrorDeviceStationMessage *)errorMessage terminal];
                             errorUser = [(ServerErrorDeviceStationMessage *)errorMessage user];
                             errorUnavailable = [(ServerErrorDeviceStationMessage *)errorMessage peripherals];
                         }
                         // display the error
                         [vc presentAlertForDeviceStationError:errorMessage
                                           withDeviceStationId:errorDeviceStationId
                                          andDeviceStationName:errorDeviceStationName
                                                     andTillId:nil
                                                   andTerminal:errorTerminal
                                                       andUser:errorUser
                                     andUnavailablePeripherals:errorUnavailable
                                                     dismissed:^{
                                                         // start the scanner again
                                                         [vc startScan];
                                                     }];
                     }
                 });
             }
         }];
        
        return result;
    };
}

- (void) getDeviceStatus
{
    _startScanner = YES;
    self.infoLabelText = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁹󠁤󠁄󠁆󠁪󠁆󠁸󠁍󠀱󠁵󠀰󠁳󠁥󠁊󠁢󠀶󠁍󠁊󠁶󠁎󠀯󠁤󠁍󠁄󠁕󠁦󠁅󠁿*/ @"Please scan the device station you wish to get the status of", nil);
    self.scanAddButtonTitle = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁥󠀷󠁶󠁸󠀲󠁙󠁈󠁆󠁗󠁋󠁇󠁷󠁕󠁗󠁘󠁦󠁚󠁉󠀵󠁙󠁺󠁈󠁂󠀰󠀵󠁱󠁍󠁿*/ @"Status", nil);
    __block DeviceStationViewController *vc = self;
    self.barcodeProcessedBlock = ^(NSString *deviceStationId) {
        // present the device status view controller
        DeviceStationStatusViewController *statusVC = [[DeviceStationStatusViewController alloc] initWithStyle:UITableViewStyleGrouped];
        statusVC.station = vc.station;
        [vc.navigationController pushViewController:statusVC
                                           animated:YES];
    };
    self.scannedBlock = ^BOOL(NSString *barcode) {
        __block BOOL result = NO;
        [RESTController.sharedInstance  getDeviceStationWithDeviceStationId:barcode
                                                                   complete:^(DeviceStation *deviceStation, NSError *restError, ServerError *restServerError)
         {
             vc.station = deviceStation;
             
             if ((restError == nil) && (restServerError == nil)) {
                 // no errors
                 result = YES;
             } else {
                 // an error occurred
                 result = NO;
                 // get the error
                 ServerErrorMessage *errorMessage = nil;
                 for (ServerErrorMessage *message in restServerError.messages) {
                     errorMessage = message;
                     break;
                 }
                 NSString *errorDeviceStationId = nil;
                 NSString *errorDeviceStationName = nil;
                 NSString *errorTerminal = nil;
                 NSString *errorUser = nil;
                 NSString *errorUnavailable = nil;
                 if ([errorMessage isKindOfClass:[ServerErrorDeviceStationMessage class]]) {
                     errorDeviceStationId = [(ServerErrorDeviceStationMessage *)errorMessage deviceStationId];
                     errorDeviceStationName = [(ServerErrorDeviceStationMessage *)errorMessage deviceStationName];
                     errorTerminal = [(ServerErrorDeviceStationMessage *)errorMessage terminal];
                     errorUser = [(ServerErrorDeviceStationMessage *)errorMessage user];
                     errorUnavailable = [(ServerErrorDeviceStationMessage *)errorMessage peripherals];
                 }
                 dispatch_async(dispatch_get_main_queue(), ^{
                     // display the error
                     [vc presentAlertForDeviceStationError:errorMessage
                                       withDeviceStationId:errorDeviceStationId
                                      andDeviceStationName:errorDeviceStationName
                                                 andTillId:nil
                                               andTerminal:errorTerminal
                                                   andUser:errorUser
                                 andUnavailablePeripherals:errorUnavailable
                                                 dismissed:^{
                                                     // start the scanner again
                                                     [vc startScan];
                                                 }];
                 });
             }
         }];
        
        return result;
    };
}

@end
