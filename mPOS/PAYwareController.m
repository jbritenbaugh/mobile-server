//
//  PAYwareController.m
//  mPOS
//
//  Created by John Scott on 20/03/2013.
//  Copyright (c) 2013 Clarity. All rights reserved.
//

#import "PAYwareController.h"
#import <objc/runtime.h>
#import "OCGEFTManager.h"

#define PAYwareController_URLIdentifier @"PAYwareControllerURLIdentifier"

#define CONTEXT_RESPONSE_USER_KEY @"key"
#define CONTEXT_RESPONSE_CALL_KEY @"call"

typedef NS_ENUM(NSInteger, ContextResponseCall) {
    ContextResponseCallCardData,
};

NSString * const kPAYwareControllerNotification = @"PAYwareControllerNotification";

NSString * const kPAYwareControllerUserKey = @"PAYwareControllerUserKey";
NSString * const kPAYwareControllerTrack1DataKey = @"PAYwareControllerTrack1DataKey";
NSString * const kPAYwareControllerTrack2DataKey = @"PAYwareControllerTrack2DataKey";
NSString * const kPAYwareControllerTrack3DataKey = @"PAYwareControllerTrack3DataKey";
NSString * const kPAYwareControllerResponseMessageKey = @"PAYwareControllerResponseMessageKey";

@implementation PAYwareController

#pragma mark - Init

static char SHARED_INSTANCE_IDENTIFER;

+ (void) setSharedInstance:(PAYwareController *) sharedInstance
{
    objc_setAssociatedObject(self, &SHARED_INSTANCE_IDENTIFER, sharedInstance, OBJC_ASSOCIATION_RETAIN);
}

+ (PAYwareController *) sharedInstance
{
    __block id sharedInstance = objc_getAssociatedObject(self, &SHARED_INSTANCE_IDENTIFER);
    if (sharedInstance != nil) {
        return sharedInstance;
    }
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[self alloc] init];
        self.sharedInstance = sharedInstance;
    });
    
    return sharedInstance;
}

#pragma mark - Methods

-(void)requestCardDataWithKey:(NSString*)key
{
    NSDictionary *context = @{CONTEXT_RESPONSE_USER_KEY : key,
                              CONTEXT_RESPONSE_CALL_KEY : @(ContextResponseCallCardData)};
    
    NSDictionary *request = @{@"getCardInfo" : @(YES)};
    
    [self sendRequest:request context:context];
}

-(void)cardDataResponse:(NSDictionary*)response context:(NSDictionary*)context
{
    NSMutableDictionary *userInfo = [NSMutableDictionary dictionary];
    
    if (response[@"track1Data"])
    {
        NSString *value = response[@"track1Data"];
        value = [value stringByReplacingOccurrencesOfString:@"|"
                                                 withString:@"?"];
        value = [value stringByReplacingOccurrencesOfString:@"+"
                                                 withString:@"="];
        userInfo[kPAYwareControllerTrack1DataKey] = value;
    }
    
    if (response[@"track2Data"])
    {
        NSString *value = response[@"track2Data"];
        value = [value stringByReplacingOccurrencesOfString:@"|"
                                                 withString:@"?"];
        value = [value stringByReplacingOccurrencesOfString:@"+"
                                                 withString:@"="];
        userInfo[kPAYwareControllerTrack2DataKey] = value;
    }

    if (response[@"track3Data"])
    {
        NSString *value = response[@"track3Data"];
        value = [value stringByReplacingOccurrencesOfString:@"|"
                                                 withString:@"?"];
        value = [value stringByReplacingOccurrencesOfString:@"+"
                                                 withString:@"="];
        userInfo[kPAYwareControllerTrack3DataKey] = value;
    }

    if (response[@"responseMsg"])
    {
        userInfo[kPAYwareControllerResponseMessageKey] = response[@"responseMsg"];
    }

    userInfo[kPAYwareControllerUserKey] = context[CONTEXT_RESPONSE_USER_KEY];
    
    [self postNotificationWithUserInfo:userInfo];
}

#pragma mark - Low level communications

-(void)postNotificationWithUserInfo:(NSDictionary*)userInfo
{
    DebugLog(@"PAYwareControllerNotification: %@", userInfo);
    NSNotificationCenter *defaultNotificationCenter = [NSNotificationCenter defaultCenter];
    
    [defaultNotificationCenter postNotificationName:kPAYwareControllerNotification object:self userInfo:userInfo];
}


-(void)sendRequest:(NSDictionary*)request context:(NSDictionary*)context
{
    NSMutableDictionary *parameters = [NSMutableDictionary dictionaryWithDictionary:request];
    
    // Add the return URL
        
    parameters[@"returnURL"] = [NSString stringWithFormat:@"%@://%@/", [self returnURLScheme], [self hexStringForString:[self queryForDictionary:context]]];
    
    parameters[@"appName"] = NSBundle.mainBundle.infoDictionary[@"CFBundleDisplayName"];    
    
    NSString *serviceURL = nil;
    switch ([[OCGEFTManager sharedInstance] getConfiguredProvider]) {
        case OCGEFTManagerProviderPayware:
            serviceURL = @"com-verifone-paywareVX";
            break;
#if DEBUG
        case OCGEFTManagerProviderFake:
            serviceURL = @"com-omnicogroup-paywareVX-fake";
            break;
#endif
        default:
            break;
    }
    
    BOOL urlOpened = NO;
    if (serviceURL) {
        NSString *reqestURL = [NSString stringWithFormat:@"%@://VeriFone?%@", serviceURL, [self queryForDictionary:parameters]];
        
        DebugLog(@"PAYwareController Request:\nrequest: %@\ncontext: %@\nurl: %@", parameters, context, reqestURL);
        
        urlOpened = [[UIApplication sharedApplication] openURL:[NSURL URLWithString:reqestURL]];
    }

    if (!urlOpened)
    {
        [self handleResponse:nil context:context];
    }
}

- (BOOL)application:(UIApplication *)application
            openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication
         annotation:(id)annotation
{
    BOOL result = [super application:application
                             openURL:url
                   sourceApplication:sourceApplication
                          annotation:annotation];
    if (result) {
        NSDictionary *context = [self dictionaryForQuery:[self stringForHexString:url.host]];
        
        NSDictionary *response = [self dictionaryForQuery:url.query];
        
        if ([response[@"isError"] boolValue])
        {
            ErrorLog(@"PAYwareController Response:\nresponse: %@\ncontext: %@\nurl: %@\nsourceApplication: %@\nannotation: %@", response, context, url, sourceApplication, annotation);
        }
        else
        {
            DebugLog(@"PAYwareController Response:\nresponse: %@\ncontext: %@\nurl: %@\nsourceApplication: %@\nannotation: %@", response, context, url, sourceApplication, annotation);
        }
        
        [self handleResponse:response context:context];
    }
    
    return result;
}

-(void)handleResponse:(NSDictionary*)response context:(NSDictionary*)context
{    
    ContextResponseCall call = [context[CONTEXT_RESPONSE_CALL_KEY] integerValue];
    
    switch (call)
    {
        case ContextResponseCallCardData:
            [self cardDataResponse:response context:context];
            break;
            
        default:
            break;
    }
}

- (NSString *) returnURLScheme
{
    NSString *returnURLScheme = nil;
    
    for (NSDictionary *urlType in [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleURLTypes"])
    {
        if ([urlType[@"CFBundleURLName"] isEqualToString:PAYwareController_URLIdentifier])
        {
            returnURLScheme = urlType[@"CFBundleURLSchemes"][0];
            break;
        }
    }
    
    return returnURLScheme;
}

@end
