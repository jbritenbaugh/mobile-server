//
//  SignatureCaptureViewController.h
//  mPOS
//
//  Created by Antonio Strijdom on 13/02/2015.
//  Copyright (c) 2015 Clarity. All rights reserved.
//

#import <UIKit/UIKit.h>

/** @brief View controller for capturing signatures.
 */
@interface SignatureCaptureViewController : UIViewController

/** @property signatureContext
 *  @brief The soft key layout to use for signature capture.
 */
@property (nonatomic, assign) SoftKeyContext *signatureContext;

@end
