//
//  DeviceSelectionViewController.h
//  mPOS
//
//  Created by Antonio Strijdom on 28/01/2015.
//  Copyright (c) 2015 Clarity. All rights reserved.
//

#import <UIKit/UIKit.h>

@class DeviceSelectionViewController;

typedef void (^DeviceRefreshBlock)(NSArray *devices);

typedef BOOL(^DeviceCurrentlySelectedBlock)(id device);

@protocol DeviceSelectionViewControllerDataSource;
@protocol DeviceSelectionViewControllerDelegate;

@interface DeviceSelectionViewController : UITableViewController

@property (nonatomic, assign) id<DeviceSelectionViewControllerDataSource>dataSource;
@property (nonatomic, assign) id<DeviceSelectionViewControllerDelegate>delegate;
@property (nonatomic, strong) DeviceCurrentlySelectedBlock deviceCurrentlySelectedBlock;

@end

@protocol DeviceSelectionViewControllerDataSource <NSObject>
@optional
- (NSString *) deviceSelectionControllerUsageText:(DeviceSelectionViewController *)deviceSelectionViewController;
- (Device *) deviceSelectionViewControllerDeviceClass:(DeviceSelectionViewController *)deviceSelectionViewController;
- (BOOL) deviceSelectionController:(DeviceSelectionViewController *)deviceSelectionController
                         getStatus:(NSString **)status
                          ofDevice:(id)device;
- (NSString *) deviceSelectionController:(DeviceSelectionViewController *)deviceSelectionController
                            nameOfDevice:(id)device;
@required
- (void) deviceSelectionController:(DeviceSelectionViewController *)deviceSelectionController
            refreshDevicesComplete:(DeviceRefreshBlock)complete
                    currentDevices:(NSArray *)currentDevices;
@end

@protocol DeviceSelectionViewControllerDelegate <NSObject>
@optional
- (void) deviceSelectionViewController:(DeviceSelectionViewController *)controller
                       didSelectDevice:(id)device;
- (void) deviceSelectionViewController:(DeviceSelectionViewController *)controller
                         didScanDevice:(NSString *)deviceBarcode;
@required
@end
