//
//  DeviceSelectionRDMDatasource.m
//  mPOS
//
//  Created by Antonio Strijdom on 16/03/2015.
//  Copyright (c) 2015 Clarity. All rights reserved.
//

#import "DeviceSelectionRDMDatasource.h"
#import "RESTController.h"
#import "CRSLocationController.h"

@interface DeviceSelectionRDMDatasource ()

@end

@implementation DeviceSelectionRDMDatasource

#pragma mark - Init

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.deviceType = nil;
    }
    return self;
}

#pragma mark - DeviceSelectionViewControllerDataSource

- (NSString *) deviceSelectionControllerUsageText:(DeviceSelectionViewController *)deviceSelectionViewController
{
    return @"Remote Device";
}

- (Device *) deviceSelectionViewControllerDeviceClass:(DeviceSelectionViewController *)deviceSelectionViewController
{
    return nil;
}

- (BOOL) deviceSelectionController:(DeviceSelectionViewController *)deviceSelectionController
                         getStatus:(NSString **)status
                          ofDevice:(id)device
{
    *status = @"Unknown";
    return NO;
}

- (NSString *) deviceSelectionController:(DeviceSelectionViewController *)deviceSelectionController
                            nameOfDevice:(id)device
{
    DeviceStation *station = (DeviceStation *)device;
    return station.deviceStationId;
}

- (void) deviceSelectionController:(DeviceSelectionViewController *)deviceSelectionController
            refreshDevicesComplete:(DeviceRefreshBlock)complete
                    currentDevices:(NSArray *)currentDevices
{
    ocg_async_background_overlay(^{
        [RESTController.sharedInstance getDeviceStationsWithDeviceType:self.deviceType
                                                               storeId:[CRSLocationController getLocationKey]
                                                              complete:^(NSArray *newDevices, NSError *error, ServerError *serverError)
         {
             ocg_sync_main(^{
                 if ((error != nil) || (serverError != nil)) {
                     // there was an error
                     ServerError *mposerror = serverError;
                     // send the notification
                     [[NSNotificationCenter defaultCenter] postNotificationName:kBasketControllerGeneralErrorNotification
                                                                         object:self
                                                                       userInfo:@{kBasketControllerGeneralErrorKey:mposerror}];
                 } else {
                     self.deviceStations = newDevices;
                     if (complete) complete(newDevices);
                 }
             });
             
         }];
    });
}

@end
