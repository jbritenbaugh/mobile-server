//
//  ReturnReceiptScanViewController.h
//  mPOS
//
//  Created by John Scott on 04/02/2014.
//  Copyright (c) 2014 Clarity. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "FNKScanViewController.h"

@interface ReceiptScanViewController : FNKScanViewController

@end
