//
//  ButtonTableFieldCell.h
//  mPOS
//
//  Created by Meik Schuetz on 16/08/2012.
//  Copyright (c) 2012 Clarity. All rights reserved.
//

#import <UIKit/UIKit.h>

/** @file ButtonTableFieldCell.h */

@protocol ButtonTableFieldCellDelegate <NSObject>
@required
/** @brief Called when the user tapped the button with the specified index.
 *  @param index The index of the tapped button.
 */
- (void) tappedButtonWithIndex:(int)index;
@end

/** @brief Class that contains all methods and properties that are necessary for 
 *  a cell that contains a number of buttons.
 */
@interface ButtonTableFieldCell : UITableViewCell

/** @brief The delegate which received notifications sent by this object instance.
 */
@property (weak, nonatomic) id<ButtonTableFieldCellDelegate> delegate;

/** @brief The array of buttons that are displayed in the table view cell.
 */
@property (strong, nonatomic, readonly) NSArray *buttons;

/** @brief Designated constructor.
 *  @param buttonTitles The array of button titles to be shown in this cell.
 *  @return A newly initialized object of this class.
 */
- (id) initWithButtonTitles:(NSArray *)buttonTitles;

/** @brief Returns the reference to the button at the specified index.
 *  @param index The index of the requested button.
 *  @return A reference to the requested button.
 */
- (UIButton *)buttonAtIndex:(int)index;

@end
