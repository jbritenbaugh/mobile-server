//
//  OCGEFTProvider.m
//  mPOS
//
//  Created by Antonio Strijdom on 15/04/2013.
//  Copyright (c) 2013 Clarity. All rights reserved.
//

#import "OCGURLEFTProvider.h"
#import "RESTController.h"
#import "OCGPeripheralManager.h"
#import "OCGEFTManager.h"

@interface OCGURLEFTProvider () {
@private
    __strong NSFileHandle *_EFTLogFile;
}
- (NSURL *) applicationDocumentsDirectory;
- (void) handleTransactionCompletedNotification:(NSNotification *)note;
@end

@implementation OCGURLEFTProvider

#pragma mark - Private

/** @brief Returns the URL to the application's Documents directory.
 */
- (NSURL *) applicationDocumentsDirectory
{
    NSFileManager *manager = [NSFileManager defaultManager];
    NSURL *result = [[manager URLsForDirectory:NSApplicationSupportDirectory inDomains:NSUserDomainMask] lastObject];
    if (result != nil) {
        result = [result URLByAppendingPathComponent:@"mPOS"
                                         isDirectory:YES];
        NSError *error = nil;
        if (![manager createDirectoryAtURL:result
               withIntermediateDirectories:YES
                                attributes:nil
                                     error:&error]) {
            ErrorLog(@"COULD NOT CREATE APPLICATION SUPPORT PATH:\n%@", result);
            result = nil;
        }
    } else {
        ErrorLog(@"Could not get application support path");
    }
    
    return result;
}

/** Handles transaction complete notificiations.
 *  @param note The notification.
 */
- (void) handleTransactionCompletedNotification:(NSNotification *)note
{
    // clear the last reply
    self.lastReplyDict = nil;
}

#pragma mark - Properties

@synthesize manager = _manager;
@synthesize lastEFTReply = _lastEFTReply;

#pragma mark - Init

- (instancetype) initForManager:(OCGEFTManager *)manager
{
    self = [super init];
    if (self) {
        _manager = manager;
        self.status = kEFTProviderStatusAvailable;
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(handleTransactionCompletedNotification:)
                                                     name:kBasketControllerBasketCheckedOutChangeNotification
                                                   object:nil];
    }
    return self;
}

- (void) dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - OCGEFTProviderProtocol

- (BOOL) isURLProvider
{
    return YES;
}

- (ServerError *) getLastServerError
{
    // do nothing in base class
    return nil;
}

- (NSString *) localizedDescriptionForEFTErrorCode:(EFTProviderErrorCode)errorCode
{
    NSString *result = nil;
    
    switch (errorCode) {
        case kEFTProviderErrorCodeNotInstalled:
            result = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁊󠁭󠁘󠁭󠁅󠁭󠁤󠁕󠁱󠁰󠀴󠁬󠁊󠁚󠁔󠁘󠁴󠀹󠁶󠀯󠁗󠀹󠁋󠁮󠁌󠁘󠁙󠁿*/ @"The EFT application is not installed.", nil);
            break;
        case kEFTProviderErrorCodeNotInSled:
            result = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁔󠁑󠁊󠁹󠁎󠁄󠁭󠁥󠁷󠁂󠁣󠁨󠁸󠁬󠀳󠁬󠀯󠁙󠁉󠁊󠀵󠁡󠁮󠁏󠁏󠁷󠁯󠁿*/ @"The external payment sled is either missing or not connected properly.", nil);
            break;
        case kEFTProviderErrorCodeBusy:
            result = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁫󠀵󠁣󠁭󠁦󠁁󠁒󠁉󠁵󠀫󠀫󠀹󠀱󠁁󠀵󠁆󠁺󠀴󠁍󠁚󠁡󠁷󠁵󠁮󠁓󠁱󠁯󠁿*/ @"Payment already in progress. Please complete the current payment before starting a new one.", nil);
            break;
        case kEFTProviderErrorCodeInvalidSource:
            result = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠀳󠁖󠀶󠀲󠀲󠁸󠁴󠁕󠁩󠀶󠀶󠁯󠁐󠁃󠁈󠀰󠁍󠁊󠀷󠁢󠁰󠀳󠁹󠁯󠁖󠁰󠁅󠁿*/ @"Invalid response format.", nil);
            break;
        case kEFTProviderErrorCodeSuccess:
            result = nil;
            break;
        case kEFTProviderErrorCodeApplicationError:
            result = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁒󠁥󠁂󠀱󠁌󠁊󠁱󠁴󠁖󠁏󠁣󠁶󠀳󠀴󠁦󠀶󠁥󠁏󠀲󠀷󠁕󠁓󠁘󠁧󠀫󠁏󠁳󠁿*/ @"Application error", nil);
            break;
        case kEFTProviderErrorCodeSaveCommsFailure:
            result = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠀳󠁮󠀯󠁙󠁳󠀱󠀹󠁁󠁑󠁊󠁈󠁁󠁯󠁌󠁅󠁍󠁧󠁱󠀹󠀱󠁂󠀴󠀱󠀰󠁳󠁔󠁯󠁿*/ @"Communication failure during authorization request. Transaction will be cancelled.", nil);
            break;
        case kEFTProviderErrorCodeAuthCommsFailure:
            result = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠀶󠁌󠁶󠁹󠁳󠁩󠁙󠁺󠁋󠁢󠁯󠀲󠁰󠁷󠁗󠁥󠁋󠀵󠁵󠁗󠀯󠁂󠁙󠁹󠁫󠁶󠁕󠁿*/ @"Communication failure during the validation request. Payment must be retried before further payments can be taken.", nil);
            break;
        case kEFTProviderErrorCodeRetryCommsFailure:
            result = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠀶󠁅󠁹󠁭󠁑󠁡󠁪󠁳󠀶󠁈󠀴󠁗󠀲󠁹󠁆󠁰󠁙󠁋󠁴󠁄󠁣󠁌󠁸󠁎󠁧󠁶󠁕󠁿*/ @"Retry failed. Please try again.", nil);
            break;
        default:
            result = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠀫󠁪󠁤󠁓󠀶󠁲󠁹󠀯󠁊󠁙󠁏󠁸󠁍󠁈󠁣󠁫󠁐󠁚󠁃󠁺󠀸󠁄󠁤󠁌󠁱󠁘󠁍󠁿*/ @"Unknown error", nil);
            break;
    }
    
    return result;
}

- (void) preflightCheckForTransaction:(BOOL)forTx
                             complete:(EFTProviderCompleteBlock)complete
{
    // just make sure the EFT application is installed
    EFTProviderErrorCode result = kEFTProviderErrorCodeSuccess;
    NSString *urlString = [NSString stringWithFormat:@"%@://", [[self class] providerURL]];
    BOOL installed = [[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:urlString]];
    // update the provider state
    if (installed) {
        self.status = kEFTProviderStatusAvailable;
    } else {
        self.status = kEFTProviderStatusNotAvailable;
        result = kEFTProviderErrorCodeNotInstalled;
    }
    if (complete) {
        complete(result, nil, NO);
    }
}

- (void) cardSaleWithTender:(Tender *)tender
                     amount:(NSNumber *)amount
            forBasketWithID:(NSString *)basketId
          withCustomerEmail:(NSString *)customerEmail
                   complete:(EFTProviderCompleteBlock)complete
{
    complete(kEFTProviderErrorCodeSuccess, nil, NO);
}

- (void) cardRefundWithTender:(Tender *)tender
                   withAmount:(NSNumber *)amount
              forBasketWithID:(NSString *)basketId
            withCustomerEmail:(NSString *)customerEmail
                     complete:(EFTProviderCompleteBlock)complete
{
    complete(kEFTProviderErrorCodeSuccess, nil, NO);
}

- (BOOL) isProviderURL:(NSURL *)url
{
    // do nothing in base class
    return NO;
}

- (void) resumeTransactionComplete:(EFTProviderTransactionCompleteBlock)complete
{
    // init
    EFTProviderErrorCode result = kEFTProviderErrorCodeSuccess;
    EFTPaymentCompleteReason reason = kEFTPaymentCompleteReasonError;
    
    // get the stored transaction URL
    DebugLog(@"Checking for stored transaction");
    _lastEFTReply = nil;
    NSString *URLstring = nil;
    if (self.manager.currentPayment != nil) {
        URLstring = self.manager.currentPayment.transaction;
    }
    if (URLstring) {
        _lastEFTReply = [NSURL URLWithString:URLstring];
        DebugLog(@"Found stored transaction: %@", _lastEFTReply);
    } else {
        DebugLog(@"Error retreiving EFT Tx details");
        _lastEFTReply = nil;
    }
    
    if (self.lastEFTReply) {
        result = [self handlePaymentURL:self.lastEFTReply
                  paymentCompleteReason:&reason];
    }
    
    complete(reason, result, nil);
}

- (NSString *) RawStringForTransaction
{
    return self.manager.currentPayment.transaction;
}

- (BOOL) voidsAllowed
{
    return NO;
}

- (void) voidTransactionWithReference:(NSString *)originalTxReference
                             complete:(EFTProviderCompleteBlock)complete
{
    complete(kEFTProviderErrorCodeApplicationError, nil, NO);
}

#pragma mark - OCGURLProviderProtocol

- (BOOL) application:(UIApplication *)application
             openURL:(NSURL *)url
   sourceApplication:(NSString *)sourceApplication
          annotation:(id)annotation
{
    return [self isProviderURL:url];
}

#pragma mark - Methods

+ (NSString *) returnURLScheme
{
    // do nothing in base class
    return nil;
}

+ (NSString *) providerURL
{
    // do nothing in base class
    return nil;
}

- (BasketTender *) updatePaymentFromDictionary:(NSDictionary *)options
{
    // do nothing in base class
    return nil;
}

- (NSString *) buildOptionStringFromDict:(NSDictionary *)options
{
    // do nothing in base class
    return nil;
}

- (NSDictionary *) buildDictionaryFromURL:(NSURL *)url
{
    // do nothing in base class
    return nil;
}

- (EFTProviderErrorCode) sendRequestWithParams:(NSDictionary *)params
{
    EFTProviderErrorCode result = kEFTProviderErrorCodeSuccess;
    
    BOOL retry = NO;
    
    // check if we are retrying
    retry = [self isRetryRequest:params];
    
    // only send messages if the system is available
    if ((self.status == kEFTProviderStatusAvailable) ||
        // or we are retrying
        ((self.status == kEFTProviderStatusStoredPending) && retry)) {
        // build the final URL
        NSString *request = [NSString stringWithFormat:@"%@://%@",
                             [[self class] providerURL],
                             [self buildOptionStringFromDict:params]];
        // percent escape
        request = [request stringByAddingPercentEscapesUsingEncoding:NSASCIIStringEncoding];
        NSURL *url = [NSURL URLWithString:request];
        DebugLog2(@"Sending request to provider: {}", url);
        [self logPaymentURL:url
                   incoming:NO];
        BOOL urlOpened = [[UIApplication sharedApplication] openURL:url];
        if (!urlOpened) {
            self.status = kEFTProviderStatusNotAvailable;
            result = kEFTProviderErrorCodeNotInstalled;
        }
    } else {
        if (self.status == kEFTProviderStatusStoredPending) {
            result = kEFTProviderErrorCodeBusy;
        } else {
            result = kEFTProviderErrorCodeNotInSled;
        }
    }
    
    return result;
}

- (BOOL) isRetryRequest:(NSDictionary *)requestDict
{
    // do nothing in base class
    return NO;
}

- (EFTProviderErrorCode) checkForErrorInResponse:(NSDictionary *)params
{
    // do nothing in base class
    return kEFTProviderErrorCodeApplicationError;
}

- (EFTPaymentCompleteReason) updateStatusFromSuccessfulResponse:(NSDictionary *)response
{
    // do nothing in base class
    return kEFTPaymentCompleteReasonError;
}

- (void) updateStatusFromFailedResponse:(NSDictionary *)response
                          withErrorCode:(EFTProviderErrorCode)errorCode
{
    // do nothing in base class
}

- (EFTProviderErrorCode) handlePaymentURL:(NSURL *)url
                    paymentCompleteReason:(EFTPaymentCompleteReason *)completeReason
{
    // initialise
    EFTProviderErrorCode result = kEFTProviderErrorCodeSuccess;
    
    DebugLog(@"handlePaymentURL enter (%@)", [url absoluteString]);
    
    // process URL
    NSDictionary *params = [self buildDictionaryFromURL:url];
    self.lastReplyDict = params;
    DebugLog(@"params: %@", params);
    
    if (params) {
        result = [self checkForErrorInResponse:params];
        // check if there was an error
        if (result == kEFTProviderErrorCodeSuccess) {
            // update the provider status
            *completeReason = [self updateStatusFromSuccessfulResponse:params];
        } else {
            // update the provider status
            [self updateStatusFromFailedResponse:params
                                   withErrorCode:result];
            *completeReason = kEFTPaymentCompleteReasonError;
        }
    }
    
    // done
    DebugLog(@"handlePaymentURL leave (%@)", [url absoluteString]);
    return result;
}

- (void) logPaymentURL:(NSURL *)url
              incoming:(BOOL)incoming
{
    // build final log line data
    // TIMESTAMP - IN/OUT - URL
    NSDate *now = [NSDate date];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss.SSS"];
    NSString *finalLogLine = [NSString stringWithFormat:@"%@ - %@ - %@\n",
                              [formatter stringFromDate:now],
                              incoming ? @"IN " : @"OUT",
                              url.absoluteString];
    NSData *logData = [finalLogLine dataUsingEncoding:NSUTF8StringEncoding];
    // open the log if it isn't open yet
    if (_EFTLogFile == nil) {
        NSURL *logpath = [self applicationDocumentsDirectory];
        logpath = [logpath URLByAppendingPathComponent:@"eft.log"];
        // try to open the file, if it exists
        NSError *error = nil;
        _EFTLogFile = [NSFileHandle fileHandleForWritingToURL:logpath
                                                        error:&error];
        // if it doesn't exist
        if (_EFTLogFile == nil) {
            error = nil;
            // create log file
            BOOL result = [logData writeToURL:logpath
                                      options:NSDataWritingAtomic
                                        error:&error];
            if (result) {
                // open it
                error = nil;
                _EFTLogFile = [NSFileHandle fileHandleForWritingToURL:logpath
                                                                error:&error];
            } else {
                ErrorLog(@"Error creating eft log:\n%@\n%@", logpath, error);
            }
        }
        if (_EFTLogFile) {
            [_EFTLogFile seekToEndOfFile];
        } else {
            ErrorLog(@"Error opening eft log:\n%@\n%@", logpath, error);
            // fallback
            ErrorLog(@"%@", finalLogLine);
        }
    }
    if (_EFTLogFile != nil) {
        // log
        [_EFTLogFile writeData:logData];
        [_EFTLogFile synchronizeFile];
    }
}

@end
