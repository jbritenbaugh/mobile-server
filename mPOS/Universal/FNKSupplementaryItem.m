//
//  FNKSupplementaryItem.m
//  mPOS
//
//  Created by John Scott on 12/08/2014.
//  Copyright (c) 2014 Clarity. All rights reserved.
//

#import "FNKSupplementaryItem.h"

@implementation FNKSupplementaryItem

- (NSString *)description
{
    NSMutableArray *options = [NSMutableArray array];
    
    if (_options & FNKSupplementaryOptionDefault)
    {
        [options addObject:@"Default"];
    }
    if (_options & FNKSupplementaryOptionTabbed)
    {
        [options addObject:@"Tabbed"];
    }
    if (_options & FNKSupplementaryOptionDropdown)
    {
        [options addObject:@"Dropdown"];
    }
    if (_options & FNKSupplementaryOptionLockToDefault)
    {
        [options addObject:@"Locked"];
    }
    
    if (!options.count)
    {
        [options addObject:@"None"];
    }
    
    return [NSString stringWithFormat:@"<title: %@, options: %@>", _title, [options componentsJoinedByString:@", "]];
}

@end
