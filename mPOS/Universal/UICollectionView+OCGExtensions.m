//
//  UICollectionView+OCGExtensions.m
//  mPOS
//
//  Created by John Scott on 09/07/2014.
//  Copyright (c) 2014 Clarity. All rights reserved.
//

#import "UICollectionView+OCGExtensions.h"

@implementation UICollectionView (OCGExtensions)

-(NSIndexPath*)firstIndexPath
{
    NSIndexPath *indexPath = nil;
    if ([self numberOfSections] > 0)
    {
        if ([self numberOfItemsInSection:0] > 0)
        {
            indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
        }
    }
    
    return indexPath;
}

-(NSIndexPath*)lastIndexPath
{
    return [self indexPathFromIndexPath:self.firstIndexPath offset:-1 wrap:YES];
}

- (NSIndexPath *)indexPathFromIndexPath:(NSIndexPath *)indexPath offset:(NSInteger)offset wrap:(BOOL)wrap
{
    NSInteger section = indexPath.section;
    NSInteger row = indexPath.row;
    
    row += offset;
    
    while (row >= [self numberOfItemsInSection:section])
    {
        row -= [self numberOfItemsInSection:section];
        section++;
        if (section >= [self numberOfSections])
        {
            if (wrap)
            {
                section = 0;
            }
            else
            {
                return nil;
            }
        }
    }
    
    while (row < 0)
    {
        if (section == 0)
        {
            if (wrap)
            {
                section = [self numberOfSections];
            }
            else
            {
                return nil;
            }
        }
        section--;
        row += [self numberOfItemsInSection:section];
    }
    
    return [NSIndexPath indexPathForRow:row inSection:section];
}

@end
