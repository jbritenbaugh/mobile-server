//
//  NSLocale+OCGAdditions.h
//  mPOS
//
//  Created by John Scott on 11/04/2014.
//  Copyright (c) 2014 Clarity. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSLocale (OCGAdditions)

/*
   ocg_runtimeLocale is a class property that, when set, overrides +[NSLocal currentLocale].
   This locale will be used everywhere.
*/

+ (void)OCGAdditions_setRuntimeLocale:(NSLocale*)currentLocale;
+ (id)OCGAdditions_runtimeLocale;

@end
