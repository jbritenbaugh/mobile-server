//
//  FNKCollectionView.h
//  mPOS
//
//  Created by John Scott on 29/08/2014.
//  Copyright (c) 2014 Clarity. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FNKCollectionView : UICollectionView
/*
 DO NOT put anything in here.  This is here to avoid the need for creating a class at run time.
*/
@end

@interface FNKCollectionView (Safe)

/** @brief Switches the collection views class to this class
    This is so we can track didMoveToWindow.
 */

+(void)ensureThisKindOfClass:(UICollectionView*)collectionView;

@end
