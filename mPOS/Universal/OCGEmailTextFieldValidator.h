//
//  OCGEmailTextFieldValidator.h
//  mPOS
//
//  Created by John Scott on 09/04/2013.
//  Copyright (c) 2013 Clarity. All rights reserved.
//

#import "OCGTextFieldValidator.h"

@interface OCGEmailTextFieldValidator : OCGTextFieldValidator

@end
