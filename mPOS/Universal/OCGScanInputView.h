//
//  OCGScanInputView.h
//  mPOS
//
//  Created by John Scott on 09/04/2015.
//  Copyright (c) 2015 Clarity. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "BarcodeInput.h"

@interface OCGScanInputView : UIInputView

@end
