//
//  FNKCollectionViewLayout.h
//  Frank
//
//  Created by John Scott on 11/07/2014.
//  Copyright (c) 2014 Omnico Group. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "FNKSupplementaryItem.h"

@protocol FNKCollectionViewDataSourceDelegate;

@interface FNKCollectionViewLayout : UICollectionViewLayout

@property (nonatomic, strong) NSIndexPath *selectedNodePath;

@property (nonatomic, assign) BOOL pinLastSectionToBottom;

@end

typedef NS_ENUM(NSInteger, FNKLayoutOptions) {
    FNKLayoutOptionNone = 0,
    FNKLayoutOptionEmptyCell = 1 << 0,
    FNKLayoutOptionDefaultCell = 1 << 1,
};

@protocol FNKCollectionViewDataSourceDelegate <UICollectionViewDataSource>
@optional

- (NSArray * /* FNKSupplementaryItem */)collectionView:(UICollectionView *)collectionView
                                                layout:(UICollectionViewLayout*)collectionViewLayout
               supplementaryItemsForHeadersAtIndexPath:(NSIndexPath *)indexPath;

- (NSArray * /* FNKSupplementaryItem */)collectionView:(UICollectionView *)collectionView
                                                layout:(UICollectionViewLayout*)collectionViewLayout
               supplementaryItemsForFootersAtIndexPath:(NSIndexPath *)indexPath;

- (FNKLayoutOptions)collectionView:(UICollectionView *)collectionView
                            layout:(UICollectionViewLayout*)collectionViewLayout
          layoutOptionsAtIndexPath:(NSIndexPath *)indexPath;

- (CGFloat)collectionView:(UICollectionView *)collectionView
 heightForItemAtIndexPath:(NSIndexPath *)indexPath;

@end
