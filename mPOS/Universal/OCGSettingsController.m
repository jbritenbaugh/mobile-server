//
//  OCGSettingsController.m
//  mPOS
//
//  Created by Antonio Strijdom on 22/01/2015.
//  Copyright (c) 2015 Omnico. All rights reserved.
//

#import "OCGSettingsController.h"
#import <objc/runtime.h>

@implementation Device (Device_Extensions)

- (NSDictionary *) propertyDict
{
    NSMutableDictionary *result = [NSMutableDictionary dictionaryWithCapacity:self.properties.count];
    for (Property *property in self.properties) {
        [result setObject:property.value
                   forKey:property.name];
    }
    
    return result;
}

@end

@implementation OCGSettingsController

#pragma mark - Private

NSString * const kSettingsControllerSettingDidChangeNotification = @"kSettingsControllerSettingDidChangeNotification";
NSString * const kSettingsControllerSettingDidChangeNotificationSettingNameKey = @"kSettingsControllerSettingDidChangeNotificationSettingNameKey";
NSString * const kSettingsControllerSettingDidChangeNotificationSettingValueKey = @"kSettingsControllerSettingDidChangeNotificationSettingValueKey";
NSString * const kDeviceTypeEFT = @"EFT";
NSString * const kDeviceTypeScanner = @"Scanner";
NSString * const kDeviceTypePrinter = @"Printer";
NSString * const kDevicePropertyPrinterConnectionType = @"ConnectionType";
NSString * const kDevicePropertyPrinterConnection = @"Connection";

#pragma mark - Init

static char SHARED_INSTANCE_IDENTIFER;

+ (void) setSharedInstance:(OCGSettingsController *) sharedInstance
{
    objc_setAssociatedObject(self, &SHARED_INSTANCE_IDENTIFER, sharedInstance, OBJC_ASSOCIATION_RETAIN);
}

+ (OCGSettingsController *) sharedInstance
{
    __block id sharedInstance = objc_getAssociatedObject(self, &SHARED_INSTANCE_IDENTIFER);
    if (sharedInstance != nil) {
        return sharedInstance;
    }
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[self alloc] init];
        self.sharedInstance = sharedInstance;
    });
    
    return sharedInstance;
}

#pragma mark - Methods

- (id) valueForSettingWithName:(NSString *)settingName
{
    id result = nil;
    
    // check for overrides
    if (self.delegate) {
        if ([self.delegate respondsToSelector:@selector(controller:overrideSettingWithName:)]) {
            result = [self.delegate controller:self
                       overrideSettingWithName:settingName];
        }
    }
    // check if the setting is locally configurable
    if ([self configurableCheckForSettingWithName:settingName]) {
        // it is, use the local setting (if available)
        id localSettingValue = [[NSUserDefaults standardUserDefaults] objectForKey:settingName];
        if (localSettingValue) {
            result = localSettingValue;
        }
    }
    // no override, retreive user defaults setting
    if (result == nil) {
        result = [[NSUserDefaults standardUserDefaults] objectForKey:settingName];
    }
    
    return result;
}

- (void)  setValue:(id)value
forSettingWithName:(NSString *)settingName
{
    if (value) {
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        [defaults setObject:value
                     forKey:settingName];
        [defaults synchronize];
    
        // broadcast a notification
        NSDictionary *settingDict = @{ kSettingsControllerSettingDidChangeNotificationSettingNameKey  : settingName,
                                       kSettingsControllerSettingDidChangeNotificationSettingValueKey : value };
        NSNotification *note = [[NSNotification alloc] initWithName:kSettingsControllerSettingDidChangeNotification
                                                             object:self
                                                           userInfo:settingDict];
        [[NSNotificationCenter defaultCenter] postNotification:note];
    } else {
        [self removeSettingWithName:settingName];
    }
}

- (void) removeSettingWithName:(NSString *)settingName
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults removeObjectForKey:settingName];
    [defaults synchronize];
}

- (BOOL) enabledCheckForSettingWithName:(NSString *)settingName
{
    BOOL result = YES;
    
    // check with the delegate
    if (self.delegate) {
        if ([self.delegate respondsToSelector:@selector(controller:settingEnabledWithName:)]) {
            result = [self.delegate controller:self
                        settingEnabledWithName:settingName];
        }
    }
    
    return result;
}

- (BOOL) configurableCheckForSettingWithName:(NSString *)settingName
{
    BOOL result = YES;
    
    // check with the delegate
    if (self.delegate) {
        if ([self.delegate respondsToSelector:@selector(controller:settingConfigurableWithName:)]) {
            result = [self.delegate controller:self
                   settingConfigurableWithName:settingName];
        }
    }
    
    return result;
}

- (BOOL) overridePresentForSettingWithName:(NSString *)settingName
{
    BOOL result = NO;
    
    // check for overrides
    if (self.delegate) {
        if ([self.delegate respondsToSelector:@selector(controller:overrideSettingWithName:)]) {
            result = ([self.delegate controller:self
                        overrideSettingWithName:settingName] != nil);
        }
    }
    
    return result;
}

@end
