//
//  OCGFirstResponderAccessoryView.h
//  mPOS
//
//  Created by John Scott on 05/02/2014.
//  Copyright (c) 2014 Clarity. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol OCGFirstResponderAccessoryViewDelegate;

@interface OCGFirstResponderAccessoryView : UIInputView

@property (strong, nonatomic) id currentPosition;
@property (weak, nonatomic) id <OCGFirstResponderAccessoryViewDelegate> delegate;

-(void)moveFirstResponderByOffset:(NSInteger)offset;

@end

@protocol OCGFirstResponderAccessoryViewDelegate <NSObject>

@optional

- (UIResponder*)OCGFirstResponderAccessoryView:(OCGFirstResponderAccessoryView*)firstResponderAccessoryView firstResponderViewForPosition:(id)position;

- (id)OCGFirstResponderAccessoryView:(OCGFirstResponderAccessoryView*)firstResponderAccessoryView positionFromPosition:(id)position offset:(NSInteger)offset;

- (NSArray*)OCGFirstResponderAccessoryView:(OCGFirstResponderAccessoryView*)firstResponderAccessoryView toolBarItemsForPosition:(id)position;

@end
