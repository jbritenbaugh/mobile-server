//
//  UITableView+OCGExtensions.m
//  mPOS
//
//  Created by John Scott on 03/12/2013.
//  Copyright (c) 2013 Clarity. All rights reserved.
//

#import "UITableView+OCGExtensions.h"

@implementation UITableView (OCGExtensions)

-(NSIndexPath*)firstIndexPath
{
    NSIndexPath *indexPath = nil;
    if ([self numberOfSections] > 0)
    {
        if ([self numberOfRowsInSection:0] > 0)
        {
            indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
        }
    }

    return indexPath;
}

-(NSIndexPath*)lastIndexPath
{
    NSIndexPath *indexPath = nil;
    NSInteger section = [self numberOfSections];
    NSInteger row;
    while (section >= 0)
    {
        section--;
        row = [self numberOfRowsInSection:section];
        if (row > 0)
        {
            row--;
            break;
        }
    }
    if (section >= 0 && row >= 0)
    {
        indexPath = [NSIndexPath indexPathForRow:row inSection:section];
    }
    return indexPath;
}



- (NSIndexPath *)indexPathFromIndexPath:(NSIndexPath *)indexPath offset:(NSInteger)offset wrap:(BOOL)wrap
{
    NSInteger section = indexPath.section;
    NSInteger row = indexPath.row;
    
    row += offset;
    
    while (row >= [self numberOfRowsInSection:section])
    {
        row -= [self numberOfRowsInSection:section];
        section++;
        if (section >= [self numberOfSections])
        {
            if (wrap)
            {
                section = 0;
            }
            else
            {
                return nil;
            }
        }
    }
    
    while (row < 0)
    {
        if (section == 0)
        {
            if (wrap)
            {
                section = [self numberOfSections];
            }
            else
            {
                return nil;
            }
        }
        section--;
        row += [self numberOfRowsInSection:section];
    }
    
    return [NSIndexPath indexPathForRow:row inSection:section];
}

@end
