//
//  UIDevice+OCGIdentifier.h
//  mPOS
//
//  Created by John Scott on 03/04/2014.
//  Copyright (c) 2014 Clarity. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIDevice (OCGIdentifier)

- (NSString *) uniqueDeviceIdentifier;

@end
