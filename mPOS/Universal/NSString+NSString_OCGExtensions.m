//
//  NSString+NSString_OCGExtensions.m
//  mPOS
//
//  Created by Antonio Strijdom on 09/03/2015.
//  Copyright (c) 2015 Clarity. All rights reserved.
//

#import "NSString+NSString_OCGExtensions.h"

@implementation NSString (NSString_OCGExtensions)

- (NSString *) divisionWithDividend:(NSString *)dividend
                            andBase:(NSString *)base
                            divisor:(long)divisor
                          remainder:(long *)remainder
{
    NSString *result = nil;
    
    // perform long division on the string
    // init
    NSMutableString *quotient = [NSMutableString string];
    long temp = 0;
    // loop through the dividend
    for (NSUInteger positionIndex = 0; positionIndex < dividend.length; positionIndex++) {
        // get the next digit
        NSString *digit = [dividend substringWithRange:NSMakeRange(positionIndex, 1)];
        // add to temp
        temp = temp * base.length + [base rangeOfString:digit].location;
        // add to the quotient
        if (temp < divisor) {
            if (quotient.length > 0) {
                [quotient appendString:[base substringWithRange:NSMakeRange(0, 1)]];
            }
        } else {
            [quotient appendString:[base substringWithRange:NSMakeRange((temp / divisor), 1)]];
            // carry over the remainder
            temp = temp % divisor;
        }
    }
    
    // return the remainder
    if (remainder) {
        *remainder = temp;
    }
    
    // return the quotient
    if (quotient.length > 0) {
        result = quotient;
    } else {
        result = [base substringWithRange:NSMakeRange(0, 1)];
    }
    
    return result;
}

- (NSString *) convertValue:(NSString *)value
                   fromBase:(NSString *)from
                     toBase:(NSString *)to
{
    NSMutableString *result = [NSMutableString string];
    long remainder = 0;
    
    while (![value isEqualToString:[from substringWithRange:NSMakeRange(0, 1)]]) {
        value = [self divisionWithDividend:value
                                   andBase:from
                                   divisor:to.length
                                 remainder:&remainder];
        NSString *toDigit = [to substringWithRange:NSMakeRange(remainder, 1)];
        [result insertString:toDigit
                     atIndex:0];
    }
    if (result.length == 0) {
        [result appendString:[to substringWithRange:NSMakeRange(0, 1)]];
    }
    return result;
}

- (NSString *) hexFromDecimalString
{
    NSString *result = nil;
    
    result = [self convertValue:self
                       fromBase:@"0123456789"
                         toBase:@"0123456789ABCDEF"];
    
    return result;
}

@end
