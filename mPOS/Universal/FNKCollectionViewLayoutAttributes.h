//
//  FNKCollectionViewLayoutAttributes.h
//  mPOS
//
//  Created by John Scott on 30/07/2014.
//  Copyright (c) 2014 Clarity. All rights reserved.
//

#import <UIKit/UIKit.h>

@class FNKSupplementaryItem;

@interface FNKCollectionViewLayoutAttributes : UICollectionViewLayoutAttributes

@property (nonatomic, strong) FNKSupplementaryItem *item;
@property (nonatomic, assign) BOOL selected;
@property (nonatomic, strong) NSIndexPath *nodePath;

@end
