//
//  OCGPOSKeyboardToolbar.h
//  mPOS
//
//  Created by Antonio Strijdom on 15/11/2013.
//  Copyright (c) 2013 Clarity. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OCGPOSKeyboardToolbar : UIToolbar

/** @property textField
 *  @brief The text field who's keyboard this toolbar is connected to.
 */
@property (nonatomic, weak) UITextField *textField;

/** @brief Updates the virtual keyboard's toolbar to
 *  display the currently used keyboard type
 */
- (void) updateVirtualKeyboardToolbar;

@end
