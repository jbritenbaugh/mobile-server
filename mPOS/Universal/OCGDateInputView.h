//
//  OCGDateInputView.h
//  mPOS
//
//  Created by John Scott on 08/04/2015.
//  Copyright (c) 2015 Clarity. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol OCGDateInput <NSObject>

@property (nonatomic, copy) NSDate *date;
@property (nonatomic, copy) NSDateFormatter *dateFormatter;

@end

@interface OCGDateInputView : UIDatePicker

@end
