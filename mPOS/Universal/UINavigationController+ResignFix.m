//
//  UINavigationController+ResignFix.m
//  mPOS
//
//  Created by Antonio Strijdom on 31/03/2014.
//  Copyright (c) 2014 Clarity. All rights reserved.
//

#import "UINavigationController+ResignFix.h"

@implementation UINavigationController (ResignFix)

- (BOOL) disablesAutomaticKeyboardDismissal
{
    return NO;
}

@end
