//
//  CGGeometry+OCGExtensions.m
//  mPOS
//
//  Created by John Scott on 14/08/2014.
//  Copyright (c) 2014 Clarity. All rights reserved.
//

#import "CGGeometry+OCGExtensions.h"

/** @brief Slices a part of rect off amount points from the edge specified.
 *  @param rect      The rectangle to slice a part off of.
 *  @param remainder The remaining rectangle after amount has been sliced from edge.
 *  @note Pass nil for remainder if you don't want the remainder returned.
 *  @param amount    The amount in points to slice from the edge specified from rect.
 *  @param edge      The edge to slice amount points from.
 *  @return The slice from rect, amount points from edge.
 */
CGRect CGRectSlice(CGRect rect, CGRect *remainder, CGFloat amount, CGRectEdge edge)
{
    CGRect slice;
    CGRect nullRemainder;
    
    CGRectDivide(rect, &slice, remainder ?: &nullRemainder, amount, edge);
    
    return slice;
}
