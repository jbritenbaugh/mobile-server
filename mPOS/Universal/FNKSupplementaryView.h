//
//  FNKLabelSupplementaryView.h
//  Frank
//
//  Created by John Scott on 14/07/2014.
//  Copyright (c) 2014 Omnico Group. All rights reserved.
//

#import <UIKit/UIKit.h>

static NSString *FNKSupplementaryKindSectionHeader = @"FNKSupplementaryKindSectionHeader";
static NSString *FNKSupplementaryKindSectionFooter = @"FNKSupplementaryKindSectionFooter";
static NSString *FNKSupplementaryKindSectionTab = @"FNKSupplementaryKindSectionTab";
static NSString *FNKSupplementaryKindCell = @"FNKSupplementaryKindCell";
static NSString *FNKSupplementaryKindSectionSeperator = @"FNKSupplementaryKindSectionSeperator";

@interface FNKSupplementaryView : UICollectionReusableView

+ (CGFloat)heightForText:(NSString*)text width:(CGFloat)width;

@end
