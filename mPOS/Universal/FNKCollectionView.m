//
//  FNKCollectionView.m
//  mPOS
//
//  Created by John Scott on 29/08/2014.
//  Copyright (c) 2014 Clarity. All rights reserved.
//

#import "FNKCollectionView.h"

#import <objc/runtime.h>

@implementation FNKCollectionView
/*
 DO NOT put anything in here.  This is here to avoid the need for creating a class at run time.
 */
@end

@implementation FNKCollectionView (Safe)

+(void)ensureThisKindOfClass:(FNKCollectionView*)collectionView
{
    if (![collectionView isKindOfClass:FNKCollectionView.class])
    {
        object_setClass(collectionView, FNKCollectionView.class);
        [collectionView didSwitchKindOfClass];
    }
}

-(void)didSwitchKindOfClass
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillChangeFrame:)
                                                 name:UIKeyboardWillChangeFrameNotification
                                               object:nil];
}

-(void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillChangeFrameNotification
                                                  object:nil];
}

-(void)keyboardWillChangeFrame:(NSNotification*)notification
{
    CGRect keyboardRect = [notification.userInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue];

    keyboardRect = [self.superview convertRect:keyboardRect fromView:nil];
    
    CGFloat viewBottom = CGRectGetHeight(self.frame);
    
    CGFloat keyboardTop = CGRectGetMinY(keyboardRect);
    
    CGFloat bottom = MAX(0, viewBottom - keyboardTop);
    
    [UIView animateWithDuration:[notification.userInfo[UIKeyboardAnimationDurationUserInfoKey] floatValue]
                          delay:0
                        options:[notification.userInfo[UIKeyboardAnimationCurveUserInfoKey] integerValue]
                     animations:^
     {
         UIEdgeInsets contentInset = self.contentInset;
         contentInset.bottom = bottom;
         self.contentInset = contentInset;
         
         UIEdgeInsets scrollIndicatorInsets = self.scrollIndicatorInsets;
         scrollIndicatorInsets.bottom = bottom;
         self.scrollIndicatorInsets = scrollIndicatorInsets;
     }
                     completion:NULL];
}

-(void)setContentSize:(CGSize)contentSize
{
    [super setContentSize:contentSize];
    if (self.contentSize.height <= self.bounds.size.height && self.contentOffset.y != 0)
    {
        CGPoint contentOffset = self.contentOffset;
        contentOffset.y = 0;
        self.contentOffset = contentOffset;
    }
}

-(void)setFrame:(CGRect)frame
{
    [super setFrame:frame];
    [self.collectionViewLayout invalidateLayout];
}

@end