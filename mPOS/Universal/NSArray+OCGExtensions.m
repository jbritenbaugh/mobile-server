//
//  NSArray+OCGExtensions.m
//  mPOS
//
//  Created by John Scott on 04/08/2014.
//  Copyright (c) 2014 Clarity. All rights reserved.
//

#import "NSArray+OCGExtensions.h"

@implementation NSArray (OCGExtensions)

- (id)OCGExtensions_valueAtIndex:(NSUInteger)index
{
    id value = nil;
    if (index < self.count)
    {
        value = [self objectAtIndex:index];
    }
    return value;
}

-(NSDictionary*)OCGExtensions_dictionaryWithKeyPath:(NSString*)keyPath
{
    NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];
    
    for (id object in self)
    {
        id key = nil;
        @try
        {
            key = [object valueForKeyPath:keyPath];
        }
        @catch (NSException *exception)
        {
        }
        
        if (key)
        {
            dictionary[key] = object;
        }
    }
    
    return [dictionary copy];
}

@end
