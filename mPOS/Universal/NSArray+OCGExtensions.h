//
//  NSArray+OCGExtensions.h
//  mPOS
//
//  Created by John Scott on 04/08/2014.
//  Copyright (c) 2014 Clarity. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSArray (OCGExtensions)

-(id)OCGExtensions_valueAtIndex:(NSUInteger)index;

-(NSDictionary*)OCGExtensions_dictionaryWithKeyPath:(NSString*)keyPath;

@end
