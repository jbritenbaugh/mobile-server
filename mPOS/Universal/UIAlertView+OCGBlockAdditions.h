//
//  UIAlertView+OCGBlockAdditions.h
//
//  Created by John Scott on 10/04/13.
//  Copyright 2013 Omnico Group. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "UIAlertView+MKBlockAdditions.h"

typedef BOOL (^ShouldEnableFirstOtherButtonBlock)(void);


@interface UIAlertView (MKBlockAdditions) <UIAlertViewDelegate>

@property (nonatomic, copy) ShouldEnableFirstOtherButtonBlock shouldEnableFirstOtherButtonBlock;

+ (void)OCGAdditions_cancelAnimated:(BOOL)animated;

@end
