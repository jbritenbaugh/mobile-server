//
//  OCGNumberTextFieldValidator.h
//  mPOS
//
//  Created by John Scott on 09/04/2013.
//  Copyright (c) 2013 Clarity. All rights reserved.
//

#import "OCGTextFieldValidator.h"

@interface OCGNumberTextFieldValidator : OCGTextFieldValidator

@property (strong, nonatomic) NSString *currencyCode;

/** @propery scale
 *  @brief The number of decimal places to format the number by
 */
@property (assign, nonatomic) NSInteger scale;

/** @propery maxValue
 *  @brief The maximum number vallowed
 */
@property (assign, nonatomic) double maxValue;

/** @propery minValue
 *  @brief The minimum number vallowed
 */
@property (assign, nonatomic) double minValue;

-(NSNumber*)numberFromString:(NSString *)string;

-(NSDecimalNumber*)decimalNumberFromString:(NSString *)string;

-(NSString*)stringFromNumber:(NSNumber*)number;

@property (assign, nonatomic) NSNumberFormatterStyle style;

@end
