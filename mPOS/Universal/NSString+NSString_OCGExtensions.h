//
//  NSString+NSString_OCGExtensions.h
//  mPOS
//
//  Created by Antonio Strijdom on 09/03/2015.
//  Copyright (c) 2015 Clarity. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (NSString_OCGExtensions)
- (NSString *) hexFromDecimalString;
@end
