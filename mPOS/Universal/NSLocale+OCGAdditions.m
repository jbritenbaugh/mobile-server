//
//  NSLocale+OCGAdditions.m
//  mPOS
//
//  Created by John Scott on 11/04/2014.
//  Copyright (c) 2014 Clarity. All rights reserved.
//

#import "NSLocale+OCGAdditions.h"

#import <objc/runtime.h>

@implementation NSLocale (OCGAdditions)

+ (void)load {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        Class class = [self class];
        
        NSArray *selectors = @[
                               @"autoupdatingCurrentLocale",
                               @"currentLocale",
                               @"systemLocale",
                               @"availableLocaleIdentifiers",
                               @"ISOLanguageCodes",
                               @"ISOCountryCodes",
                               @"ISOCurrencyCodes",
                               @"commonISOCurrencyCodes",
                               @"preferredLanguages",
                               @"componentsFromLocaleIdentifier:",
                               @"localeIdentifierFromComponents:",
                               @"canonicalLocaleIdentifierFromString:",
                               @"canonicalLanguageIdentifierFromString:",
                               @"localeIdentifierFromWindowsLocaleCode:",
                               @"windowsLocaleCodeFromLocaleIdentifier:",
                               ];
        
        for (NSString *selector in selectors)
        {
            SEL originalSelector = NSSelectorFromString(selector);
            SEL swizzledSelector = NSSelectorFromString([@"ocg_" stringByAppendingString:selector]);
            
            Method originalMethod = class_getClassMethod(class, originalSelector);
            Method swizzledMethod = class_getClassMethod(class, swizzledSelector);
            
            method_exchangeImplementations(originalMethod, swizzledMethod);
        }
        
        [self ocg_recacheRuntimeLocale:nil];
    });
}

static NSString * OCGLocaleAdditionsRuntimeLocaleKey = @"OCGLocaleAdditionsRuntimeLocaleKey";

+ (void)OCGAdditions_setRuntimeLocale:(NSLocale*)runtimeLocale
{
    NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
    [standardUserDefaults setValue:[runtimeLocale localeIdentifier] forKey:OCGLocaleAdditionsRuntimeLocaleKey];
    [standardUserDefaults synchronize];
    [self ocg_recacheRuntimeLocale:runtimeLocale];
}

+ (id)OCGAdditions_runtimeLocale
{
    return objc_getAssociatedObject(self, &OCGLocaleAdditionsRuntimeLocaleKey);
}

+(void)ocg_recacheRuntimeLocale:(NSLocale*)runtimeLocale
{
    if (runtimeLocale == nil)
    {
        
        NSString *languageIdentifier = [[NSUserDefaults standardUserDefaults] objectForKey:OCGLocaleAdditionsRuntimeLocaleKey];
        
        NSString *localizedString = nil;
        
        if ([languageIdentifier length] > 0)
        {
            runtimeLocale = [[NSLocale alloc] initWithLocaleIdentifier:languageIdentifier];
        }
    }

    objc_setAssociatedObject(self, &OCGLocaleAdditionsRuntimeLocaleKey, runtimeLocale, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

#pragma mark - Method Swizzling

+ (id)ocg_autoupdatingCurrentLocale
{
    id result = [self ocg_autoupdatingCurrentLocale];
    return result;
}

+ (id)ocg_currentLocale
{
    NSLocale *currentLocale = [self OCGAdditions_runtimeLocale];
    
    if (currentLocale == nil)
    {
        currentLocale = [self ocg_currentLocale];
    }
    
    return currentLocale;
}

+ (id)ocg_systemLocale
{
    id result = [self ocg_systemLocale];
    return result;
}

+ (NSArray *)ocg_availableLocaleIdentifiers
{
    NSArray * result = [self ocg_availableLocaleIdentifiers];
    return result;
}

+ (NSArray *)ocg_ISOLanguageCodes
{
    NSArray * result = [self ocg_ISOLanguageCodes];
    return result;
}

+ (NSArray *)ocg_ISOCountryCodes
{
    NSArray * result = [self ocg_ISOCountryCodes];
    return result;
}

+ (NSArray *)ocg_ISOCurrencyCodes
{
    NSArray * result = [self ocg_ISOCurrencyCodes];
    return result;
}

+ (NSArray *)ocg_commonISOCurrencyCodes
{
    NSArray * result = [self ocg_commonISOCurrencyCodes];
    return result;
}

+ (NSArray *)ocg_preferredLanguages
{
    NSArray * result = [self ocg_preferredLanguages];
    return result;
}

+ (NSDictionary *)ocg_componentsFromLocaleIdentifier:(NSString *)string
{
    NSDictionary * result = [self ocg_componentsFromLocaleIdentifier:string];
    return result;
}

+ (NSString *)ocg_localeIdentifierFromComponents:(NSDictionary *)dict
{
    NSString * result = [self ocg_localeIdentifierFromComponents:dict];
    return result;
}

+ (NSString *)ocg_canonicalLocaleIdentifierFromString:(NSString *)string
{
    NSString * result = [self ocg_canonicalLocaleIdentifierFromString:string];
    return result;
}

+ (NSString *)ocg_canonicalLanguageIdentifierFromString:(NSString *)string
{
    NSString * result = [self ocg_canonicalLanguageIdentifierFromString:string];
    return result;
}

+ (NSString *)ocg_localeIdentifierFromWindowsLocaleCode:(uint32_t)lcid
{
    NSString * result = [self ocg_localeIdentifierFromWindowsLocaleCode:lcid];
    return result;
}

+ (uint32_t)ocg_windowsLocaleCodeFromLocaleIdentifier:(NSString *)localeIdentifier
{
    uint32_t result = [self ocg_windowsLocaleCodeFromLocaleIdentifier:localeIdentifier];
    return result;
}


@end

/*
 This category is required to ensure NSLocalizedString() et al explicity check our ocg_runtimeLocale.
 */

@implementation NSBundle (OCGAdditions)
+ (void)load {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        Class class = [self class];
        
        NSArray *selectors = @[@"localizedStringForKey:value:table:"];
        
        for (NSString *selector in selectors)
        {
            SEL originalSelector = NSSelectorFromString(selector);
            SEL swizzledSelector = NSSelectorFromString([@"ocg_" stringByAppendingString:selector]);
            
            Method originalMethod = class_getInstanceMethod(class, originalSelector);
            Method swizzledMethod = class_getInstanceMethod(class, swizzledSelector);
            
            method_exchangeImplementations(originalMethod, swizzledMethod);
        }
    });
}

- (NSString *)ocg_localizedStringForKey:(NSString *)key value:(NSString *)value table:(NSString *)tableName
{
    NSMutableArray *preferredLanguages = [NSMutableArray array];
    
    NSString *runtimeLocaleIdentifier = [[NSLocale OCGAdditions_runtimeLocale] localeIdentifier];
    NSString *localizedString = [self ocg_localizedStringForKey:key value:nil table:tableName language:runtimeLocaleIdentifier];
    
    if (localizedString == nil)
    {
        localizedString = [self ocg_localizedStringForKey:key value:value table:tableName];
    }
    
    return localizedString;
}

- (NSString *)ocg_localizedStringForKey:(NSString *)key value:(NSString *)value table:(NSString *)tableName language:(NSString*)languageIdentifier
{
    NSString *path = [[ NSBundle mainBundle ] pathForResource:languageIdentifier ofType:@"lproj" ];
    return [[NSBundle bundleWithPath:path] ocg_localizedStringForKey:key value:value table:tableName];
}

@end

