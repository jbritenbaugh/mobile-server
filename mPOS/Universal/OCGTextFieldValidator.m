//
//  OCGTextFieldValidator.m
//  mPOS
//
//  Created by John Scott on 09/04/2013.
//  Copyright (c) 2013 Clarity. All rights reserved.
//

#import "OCGTextFieldValidator.h"

@implementation OCGTextFieldValidator

- (BOOL) isStringValid:(NSString *)string
{
    return YES;
}

-(NSString*)stringFromString:(NSString*)string
{
    return string;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    // the float value is managed through a normal numeric keypad with no keys
    // for decimal charactes; the input is comparable with typing in an amount
    // in an ATM (for 123,00 you need to type 1-2-3-0-0.
    
    NSString *text = @"";
    
    if ([textField.text length] == 0)
    {
        text = string;
    }
    else
    {
        text = [textField.text stringByReplacingCharactersInRange:range withString:string];
    }
    
    textField.text = [self stringFromString:text];
    
    /*
     Since we've changed the text ourselves, we have to return NO from this method. However, this means that
     alertViewShouldEnableFirstOtherButton: is not called (returning YES here causes a UIControlEventEditingChanged
     event). So we need to send one ourselves.
     */
    
    [textField sendActionsForControlEvents:UIControlEventEditingChanged];
    return NO;
}

@end
