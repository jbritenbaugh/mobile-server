//
//  OCGTextField.h
//  mPOS
//
//  Created by John Scott on 27/05/2014.
//  Copyright (c) 2014 Clarity. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "BarcodeInput.h"

@interface OCGTextField : UITextField <BarcodeInput>

@end
