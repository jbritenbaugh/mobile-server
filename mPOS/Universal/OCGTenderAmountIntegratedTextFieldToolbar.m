//
//  OCGTenderAmountIntegratedTextFieldToolbar.m
//  mPOS
//
//  Created by Antonio Strijdom on 14/02/2014.
//  Copyright (c) 2014 Clarity. All rights reserved.
//

#import "OCGTenderAmountIntegratedTextFieldToolbar.h"
#import "OCGNumberTextFieldValidator.h"
#import "BasketController.h"
#import "OCGKeyboardTypeButtonItem.h"
#import "OCGInputAccessoryView.h"

@interface OCGTenderAmountIntegratedTextFieldToolbar () <UITextFieldDelegate>
@property (nonatomic, weak) Payment *payment;
@property (nonatomic, strong) OCGNumberTextFieldValidator *amountTextFieldValidator;
@property (nonatomic, strong) TenderAmountEnteredBlockType amountEnteredBlock;
@property (nonatomic, strong) dispatch_block_t terminatedBlock;
@property (nonatomic, strong) NSString *addButtonTitle;
@property (nonatomic, assign) BOOL addButtonEnabled;
- (void) setupKeyboard;
@end

@implementation OCGTenderAmountIntegratedTextFieldToolbar
{
    UIBarButtonItem *_doneButton;
    OCGInputAccessoryView *_inputAccessoryView;
}

#pragma mark - Private

- (void) setupKeyboard
{
    
    
    [OCGKeyboardTypeButtonItem setKeyboardType:kOCGKeyboardTypeNumberPad forTarget:self.textField];
    
    _inputAccessoryView = [[OCGInputAccessoryView alloc] init];
    _doneButton = [[UIBarButtonItem alloc] initWithTitle:@"???"
                                                   style:UIBarButtonItemStyleDone
                                                  target:self
                                                  action:@selector(doneTapped)];
    
    UIBarButtonItem *dismissButton = [[UIBarButtonItem alloc] initWithTitle:@"\uF005"
                                                                      style:UIBarButtonItemStylePlain
                                                                     target:self
                                                                     action:@selector(dismissTapped)];
    [dismissButton setTitleTextAttributes:@{
                                            NSFontAttributeName: [UIFont fontWithName:@"OmnicoGroup-Regular" size:18.0],
                                            } forState:UIControlStateNormal];
    
    _inputAccessoryView.leftButtons = @[dismissButton];
    self.textField.frame = CGRectInset(_inputAccessoryView.contentView.bounds, 0, (44-30)/2);
    self.textField.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    [_inputAccessoryView.contentView addSubview: self.textField];
    self.textField.inputAccessoryView = _inputAccessoryView;
    
    // get the active basket
    BasketController *basketController = [BasketController sharedInstance];
    MPOSBasket *basket = basketController.basket;
    
    if (self.payment != nil) {
        // TFS59246 - set the fractional digits based on the foreign currency
        if (self.payment.tender.isForeignCurrency) {
            self.amountTextFieldValidator.currencyCode = self.payment.tender.currencyCode;
        } else {
            self.amountTextFieldValidator.currencyCode = basketController.currencyCode;
        }
        // validator min/max
        self.amountTextFieldValidator.minValue = [self.payment.tender.minimumAmount doubleValue];
        double maximumAmount = [self.payment.tender.maximumAmount doubleValue];
        if ([basketController.basketTotal doubleValue] > 0) {
            // TFS 59708 - don't allow over tender on EFT tenders
            if (!self.payment.tender.allowsChange) {
                maximumAmount = MIN(maximumAmount, [basketController.basketTotal doubleValue]);
            }
        } else {
            double basketTotal = [basketController.basketTotal doubleValue];
            if (self.payment.tender.isForeignCurrency &&
                (basket.foreignSubtotal.foreignAmount != nil)) {
                basketTotal = [basket.foreignSubtotal.foreignAmount.amount doubleValue];
            }
            maximumAmount = MIN(maximumAmount, 0 - basketTotal);
        }
        self.amountTextFieldValidator.maxValue = maximumAmount;
        // TFS57373 - If refunding display a message in the placeholder
        if ([basket.amountDue.amount floatValue] < 0) {
            self.textField.placeholder = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠀫󠁴󠁹󠁳󠁏󠁤󠁤󠀷󠁅󠀯󠁑󠀶󠁳󠀰󠁔󠁯󠁓󠁏󠁵󠁏󠁤󠁯󠀸󠁕󠀵󠁲󠁅󠁿*/ @"Refund Amount", nil);
        } else {
            // format the default amount
            self.textField.placeholder = [self.amountTextFieldValidator stringFromNumber:@(0)];
        }
        // set the add button total to the basket total
        [self resetButtonTitleToTotal:@""];
        // enable the add button
        self.addButtonEnabled = YES;
        // clear the text field
        self.textField.text = nil;
    } else {
        // setup allowed keyboard types
        // clear the text field
        self.textField.placeholder = @"0.00";
        self.textField.text = nil;
        // disable the add button
        self.addButtonEnabled = NO;
    }
    self.textField.autocapitalizationType = UITextAutocapitalizationTypeNone;
    self.textField.autocorrectionType = UITextAutocorrectionTypeNo;
    self.textField.spellCheckingType = UITextSpellCheckingTypeNo;
}

- (void) resetButtonTitleToTotal:(NSString*)amount
{
    // get the active basket
    BasketController *basketController = [BasketController sharedInstance];
    MPOSBasket *basket = basketController.basket;
    
    if (self.payment.tender.isForeignCurrency) {
        _inputAccessoryView.rightButtons = @[_doneButton];
        if (self.amountTextFieldValidator.maxValue > [basket.foreignSubtotal.foreignAmount.amount doubleValue]) {
            if (amount.length > 0)
            {
                self.addButtonTitle = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁧󠁙󠁊󠁁󠁔󠁑󠁹󠁨󠀫󠁬󠁦󠀫󠁮󠁫󠁂󠁃󠁪󠁬󠁧󠁸󠁋󠁇󠁱󠁴󠁕󠀵󠁍󠁿*/ @"Add", nil);
            }
            else
            {
                self.addButtonTitle = basket.foreignSubtotal.foreignAmount.display;
            }
        } else {
            self.addButtonTitle = [self.amountTextFieldValidator stringFromNumber:@(self.amountTextFieldValidator.maxValue)];
        }
    } else {
        BOOL displayAdd = YES;
        double amountDouble = 0;
        if (amount.length == 0)
        {
            amount = basket.amountDue.amount;
            amountDouble = amount.doubleValue;
            displayAdd = NO;
        }
        else
        {
            amountDouble = [[self.amountTextFieldValidator numberFromString:amount] doubleValue];
        }
        
        if (self.payment.tender.minimumDenomination > 0 && fmod(amountDouble, self.payment.tender.minimumDenomination))
        {
            _inputAccessoryView.rightButtons = @[];
        }
        else
        {
            _inputAccessoryView.rightButtons = @[_doneButton];
        }
        
        if (self.amountTextFieldValidator.maxValue > amountDouble) {
            if (displayAdd)
            {
                self.addButtonTitle = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁧󠁙󠁊󠁁󠁔󠁑󠁹󠁨󠀫󠁬󠁦󠀫󠁮󠁫󠁂󠁃󠁪󠁬󠁧󠁸󠁋󠁇󠁱󠁴󠁕󠀵󠁍󠁿*/ @"Add", nil);
            }
            else
            {
                self.addButtonTitle = amount;
            }
        } else {
            self.addButtonTitle = [self.amountTextFieldValidator stringFromNumber:@(self.amountTextFieldValidator.maxValue)];
        }
    }
}

#pragma mark - Properties

@synthesize addButtonTitle = _addButtonTitle;
- (void) setAddButtonTitle:(NSString *)addButtonTitle
{
    _addButtonTitle = addButtonTitle;
    _doneButton.title = _addButtonTitle;
}

@synthesize addButtonEnabled = _addButtonEnabled;
- (void) setAddButtonEnabled:(BOOL)addButtonEnabled
{
    _addButtonEnabled = addButtonEnabled;
    _doneButton.enabled = _addButtonEnabled;
}

#pragma mark - Init

- (id) init
{
    self = [super init];
    if (self) {
        // create the validator
        self.amountTextFieldValidator = [[OCGNumberTextFieldValidator alloc] init];
        // create the text field
        self.textField = [[FNKTextField alloc] init];
        self.textField.borderStyle = UITextBorderStyleRoundedRect;
        self.textField.delegate = self;
    }
    return self;
}

#pragma mark - UITextFieldDelegate

- (BOOL) textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    BOOL result = [self.amountTextFieldValidator textField:textField
                             shouldChangeCharactersInRange:range
                                         replacementString:string];
    
    double amount = [[self.amountTextFieldValidator numberFromString:textField.text] doubleValue];
    
    if (amount == 0) {
        textField.text = nil;
    }
    
    [self resetButtonTitleToTotal:textField.text];
    
    return result;
}

- (void)dismissTapped
{
    [self.textField resignFirstResponder];
    self.terminatedBlock();
}

-(void)doneTapped
{
    // if the user entered something
    NSDecimalNumber *amount = [self.amountTextFieldValidator decimalNumberFromString:self.textField.text];
    if ([amount floatValue] > 0) {
        // use the tendered amount
    } else {
        // just use the balance
        MPOSBasket *basket = [BasketController sharedInstance].basket;
        if (self.payment.tender.isForeignCurrency) {
            amount = [NSDecimalNumber decimalNumberWithString:basket.foreignSubtotal.foreignAmount.amount];
        } else {
            amount = [NSDecimalNumber decimalNumberWithString:basket.amountDue.amount];
        }
        // check for negative balance
        if ([amount compare:[NSDecimalNumber zero]] == NSOrderedAscending) {
            // get the positive value
            NSDecimalNumber *negativeOne = [NSDecimalNumber decimalNumberWithMantissa:1
                                                                             exponent:0
                                                                           isNegative:YES];
            amount = [amount decimalNumberByMultiplyingBy:negativeOne];
        }
    }
    self.amountEnteredBlock(amount);
    [self.textField resignFirstResponder];
}

#pragma mark - Methods

- (void) showTenderAmountKeyboardForPayment:(Payment *)payment
                     withAmountEnteredBlock:(TenderAmountEnteredBlockType)amountEnteredBlock
                         andTerminatedBlock:(dispatch_block_t)terminatedBlock
{
    self.payment = payment;
    self.amountEnteredBlock = amountEnteredBlock;
    self.terminatedBlock = terminatedBlock;
    [self setupKeyboard];
    [self.textField becomeFirstResponder];
}

@end
