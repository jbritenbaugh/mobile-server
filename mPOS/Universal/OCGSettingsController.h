//
//  OCGSettingsController.h
//  mPOS
//
//  Created by Antonio Strijdom on 22/01/2015.
//  Copyright (c) 2015 Omnico. All rights reserved.
//

#import <Foundation/Foundation.h>

/** @file OCGSettingsController.h */

/** @brief Notification raised when a setting's value is updated.
 */
extern NSString * const kSettingsControllerSettingDidChangeNotification;
/** @brief The key in the userinfo dictionary that contains the setting name.
 */
extern NSString * const kSettingsControllerSettingDidChangeNotificationSettingNameKey;
/** @brief The key in the userinfo dictionary that contains the setting value.
 */
extern NSString * const kSettingsControllerSettingDidChangeNotificationSettingValueKey;

/** @brief The device name for the EFT device.
 */
extern NSString * const kDeviceTypeEFT;
/** @brief The device name for the scanner device.
 */
extern NSString * const kDeviceTypeScanner;
/** @brief The device name for the printer device.
 */
extern NSString * const kDeviceTypePrinter;
/** @brief The device property name for the printer connection type.
 */
extern NSString * const kDevicePropertyPrinterConnectionType;
/** @brief The device property name for the printer connection details.
 */
extern NSString * const kDevicePropertyPrinterConnection;

/** @brief Device category that provides convenience methods.
 */
@interface Device (Device_Extensions)
@property (nonatomic, readonly) NSDictionary *propertyDict;
@end

@protocol OCGSettingsControllerDelegate;

/** @brief Controller class for providing access to settings.
 */
@interface OCGSettingsController : NSObject
/** @property delegate
 *  @brief Optional delegate for customising setting retreival. Must implement OCGSettingsControllerDelegate.
 *  @see OCGSettingsControllerDelegate
 */
@property (nonatomic, assign) id<OCGSettingsControllerDelegate> delegate;

/** @brief Gets the shared settings controller instance for mPOS.
 *  @return The shared settings controller.
 */
+ (OCGSettingsController *) sharedInstance;

/** @brief Retreives the value for the setting name specified.
 *  @param settingName The setting to retreive.
 *  @return The value of the setting or nil if no value exists.
 */
- (id) valueForSettingWithName:(NSString *)settingName;

/** @brief Sets the value for the setting specified.
 *  @param value       The new value to store for the setting.
 *  @param settingName The name of the setting to store the value against.
 */
- (void)  setValue:(id)value
forSettingWithName:(NSString *)settingName;

/** @brief Clears the setting specified.
 *  @param settingName The name of the setting to clear.
 */
- (void) removeSettingWithName:(NSString *)settingName;

/** @brief Checks if a setting is enabled or not.
 *  @param settingName The name of the setting to check.
 *  @return YES if the setting is enabled, otherwise NO.
 */
- (BOOL) enabledCheckForSettingWithName:(NSString *)settingName;

/** @brief Checks is a setting is configurable or not.
 *  @param settingName The name of the setting to check.
 *  @return YES if the setting is configurable, otherwise NO.
 */
- (BOOL) configurableCheckForSettingWithName:(NSString *)settingName;

/** @brief Determines if a setting has been overridden by the delegate.
 *  @discussion Use this method to determine if the setting can be set locally.
 *  @param settingName The name of the setting to check.
 *  @return YES if the setting has been overridden, otherwise NO.
 *  @see OCGSettingsControllerDelegate
 */
- (BOOL) overridePresentForSettingWithName:(NSString *)settingName;
@end

/** @protocol OCGSettingsControllerDelegate
 *  @brief Delegate protocol for the settings controller.
 */
@protocol OCGSettingsControllerDelegate <NSObject>
@optional
/** @brief Provides access to the device specified
 *  @param settingName The device to access.
 *  @return The device for the setting specified (if configured) or nil.
 */
- (Device *) deviceForSettingWithName:(NSString *)settingName;
/** @brief Allows the delegate to determine whether a setting is enabled or not.
 *  @param controller  The controller performing the check.
 *  @param settingName The name of the setting we are checking.
 *  @return YES if the setting is enabled, otherwise NO.
 */
- (BOOL)    controller:(OCGSettingsController *)controller
settingEnabledWithName:(NSString *)settingName;
/** @brief Allows to delegate to determine whether a setting is locally configurable of not.
 *  @param controller  The controller performing the check.
 *  @param settingName The name of the setting we are checking.
 *  @return YES if the setting can be configured locally, otherwise NO.
 */
- (BOOL)         controller:(OCGSettingsController *)controller
settingConfigurableWithName:(NSString *)settingName;
/** @brief Allows the delegate to override whatever the setting is set to
 *  in local user defaults.
 *  This method is called before the value is retreived from user defaults.
 *  @param controller  The controller requesting the override.
 *  @param settingName The name of the setting we are retreiving the value for.
 *  @return Return either the override value or nil to specify no override.
 */
- (id)       controller:(OCGSettingsController *)controller
overrideSettingWithName:(NSString *)settingName;
@end
