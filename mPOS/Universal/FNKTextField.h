//
//  FNKStuff.h
//  Flow
//
//  Created by John Scott on 15/08/2014.
//  Copyright (c) 2014 Omnico Group. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FNKTextField : UITextField

@property (nonatomic, strong) UIResponder* nextResponder;

@end

@protocol FNKTextFieldDelegate <UITextFieldDelegate>
- (void)textFieldDidDismiss;
@end