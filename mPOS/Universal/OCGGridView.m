//
//  OCGGridView.m
//  mPOS
//
//  Created by John Scott on 26/02/2013.
//  Copyright (c) 2013 Omnico Group. All rights reserved.
//

#import "OCGGridView.h"

@implementation OCGGridView

-(void)setItems:(NSArray *)items
{
    for (NSArray *row in _items)
    {
        for (UIView *item in row)
        {
            [item removeFromSuperview];
        }
    }
    _items = items;
    
    for (NSArray *row in _items)
    {
        for (UIView *itemView in row)
        {
            [self addSubview:itemView];
        }
    }
    [self setNeedsLayout];
    
}

-(void)layoutSubviews
{
    [super layoutSubviews];
    
    CGFloat rowCount = [self.items count];
    CGFloat columnCount = 0;
    
    for (NSArray *row in self.items)
    {
        columnCount = MAX(columnCount, [row count]);
    }
    
    CGFloat insetWidth = self.spacing.width/2;
    CGFloat insetHeight = self.spacing.height/2;
    
    CGRect viewBox = CGRectInset(self.bounds, insetWidth, insetHeight);
    
    CGFloat itemWidth = viewBox.size.width / columnCount;
    CGFloat itemHeight = viewBox.size.height / rowCount;
    
    for (NSInteger rowIndex=0;rowIndex < rowCount; rowIndex++)
    {
        for (NSInteger columnIndex=0;columnIndex < columnCount; columnIndex++)
        {
            CGRect itemFrame = CGRectMake(CGRectGetMinX(viewBox) + columnIndex*itemWidth, CGRectGetMinY(viewBox) + rowIndex*itemHeight, itemWidth, itemHeight);
            itemFrame = CGRectInset(itemFrame, insetWidth, insetHeight);
            itemFrame = CGRectIntegral(itemFrame);

            UIView *itemView = self.items[rowIndex][columnIndex];
            itemView.frame = itemFrame;
        }
    }
}

@end
