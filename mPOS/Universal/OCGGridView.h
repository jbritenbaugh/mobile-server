//
//  OCGGridView.h
//  mPOS
//
//  Created by John Scott on 26/02/2013.
//  Copyright (c) 2013 Omnico Group. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OCGGridView : UIView

@property (strong, nonatomic) NSArray *items;
@property (assign) CGSize spacing;

@end
