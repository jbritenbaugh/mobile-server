//
//  OCGFirstResponderAccessoryView.m
//  mPOS
//
//  Created by John Scott on 05/02/2014.
//  Copyright (c) 2014 Clarity. All rights reserved.
//

#import "OCGFirstResponderAccessoryView.h"

@implementation OCGFirstResponderAccessoryView
{
    UIToolbar *_toolBar;
    UISegmentedControl *_navigationControl;
    UIBarButtonItem *_navigationItem;
    UIBarButtonItem *_doneButton;
    UIBarButtonItem *_backButton;
    UIBarButtonItem *_nextButton;
}

- (id)initWithFrame:(CGRect)frame
{
    frame.size.height = 44;
    self = [super initWithFrame:frame];
    if (self) {
        _toolBar = [[UIToolbar alloc] initWithFrame:frame];
        
        [self addSubview:_toolBar];
        
        _toolBar.translatesAutoresizingMaskIntoConstraints = NO;
        
        [_toolBar setBackgroundImage:[UIImage new]
                  forToolbarPosition:UIBarPositionAny
                          barMetrics:UIBarMetricsDefault];
        [_toolBar setShadowImage:[UIImage new]
              forToolbarPosition:UIToolbarPositionAny];
        
        [_toolBar constrain:@"top = top" to:self];
        [_toolBar constrain:@"left = left" to:self];
        [_toolBar constrain:@"bottom = bottom" to:self];
        [_toolBar constrain:@"right = right" to:self];
        
        if ([UIDevice.currentDevice.systemVersion floatValue] >= 7.0)
        {
            _backButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"UIImages/UIButtonBarArrowLeft"]
                                                           style:UIBarButtonItemStylePlain
                                                          target:self
                                                          action:@selector(moveFirstResponderBack)];
            _nextButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"UIImages/UIButtonBarArrowRight"]
                                                           style:UIBarButtonItemStylePlain
                                                          target:self
                                                          action:@selector(moveFirstResponderNext)];
        }
        else
        {
            _navigationControl = [[UISegmentedControl alloc] initWithItems:@[NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁸󠁶󠁬󠁔󠁈󠁅󠁆󠁲󠁌󠁲󠁵󠁓󠁢󠀱󠁴󠁒󠁐󠁦󠁬󠁊󠁐󠁤󠁓󠁰󠁧󠀱󠁧󠁿*/ @"Previous", nil), NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁑󠁙󠀵󠀰󠀫󠁦󠁄󠁴󠁉󠁭󠁺󠁗󠁈󠁘󠁋󠁥󠁳󠁆󠁮󠁂󠁓󠁉󠁧󠀰󠁧󠀵󠁯󠁿*/ @"Next", nil)]];
            [_navigationControl addTarget:self
                                   action:@selector(navigationControlChanged)
                         forControlEvents:UIControlEventValueChanged];
            
            _navigationItem = [[UIBarButtonItem alloc] initWithCustomView:_navigationControl];
        }
        
        _doneButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone
                                                                    target:self
                                                                    action:@selector(doneButtonTapped)];
    }
    return self;
}


-(void)layoutSubviews
{
    id firstResponder = [self nextResponder];

    UIColor *barItemColor = nil;
    if ([firstResponder respondsToSelector:@selector(keyboardAppearance)])
    {
        if (UIDevice.currentDevice.systemVersion.floatValue >= 7.0 && [firstResponder keyboardAppearance] == UIKeyboardAppearanceDark)
        {
            barItemColor = [UIColor colorWithWhite:0.85 alpha:1.0];
        }
        else
        {
            barItemColor = [UIColor colorWithWhite:0.20 alpha:1.0];
        }
        
        _toolBar.tintColor = barItemColor;
    }
    
    NSMutableArray *items = [NSMutableArray array];
    
    id backPosition = [self positionFromPosition:self.currentPosition offset:-1];
    BOOL backButtonEnabled = self.currentPosition != backPosition && backPosition != nil && self.currentPosition != nil && ![self.currentPosition isEqual:backPosition] && [[self firstResponderViewForPosition:backPosition] canBecomeFirstResponder];
    [_navigationControl setEnabled:backButtonEnabled forSegmentAtIndex:0];
    _backButton.enabled = backButtonEnabled;

    id nextPosition = [self positionFromPosition:self.currentPosition offset:1];
    
    BOOL nextButtonEnabled = self.currentPosition != nextPosition && nextPosition && self.currentPosition != nil && ![self.currentPosition isEqual:nextPosition] && [[self firstResponderViewForPosition:nextPosition] canBecomeFirstResponder];
    [_navigationControl setEnabled:nextButtonEnabled forSegmentAtIndex:1];
    _nextButton.enabled = nextButtonEnabled;

    if (backButtonEnabled || nextButtonEnabled)
    {
        if ([UIDevice.currentDevice.systemVersion floatValue] >= 7.0)
        {
            [items addObject:_backButton];
            
            UIBarButtonItem *fixedSpaceItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace
                                                                                            target:nil
                                                                                            action:NULL];
            fixedSpaceItem.width = 16;
            [items addObject:fixedSpaceItem];
            
            [items addObject:_nextButton];
        }
        else
        {
            [items addObject:_navigationItem];
        }
    }
    
    if ([self.delegate respondsToSelector:@selector(OCGFirstResponderAccessoryView:toolBarItemsForPosition:)])
    {
        NSArray *toolBarItems = [self.delegate OCGFirstResponderAccessoryView:self toolBarItemsForPosition:self.currentPosition];
        if (toolBarItems != nil)
        {
            [items addObjectsFromArray:toolBarItems];
        }
    }
    
    if (UI_USER_INTERFACE_IDIOM() != UIUserInterfaceIdiomPad && [self firstResponderViewForPosition:self.currentPosition])
    {
        UIBarButtonItem *flexibleSpaceItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace
                                                                                           target:nil
                                                                                           action:NULL];
        [items addObject:flexibleSpaceItem];
        [items addObject:_doneButton];
    }
    
    _toolBar.items = items;
    
    [super layoutSubviews];
}

-(void)navigationControlChanged
{
    if (_navigationControl.selectedSegmentIndex == 0)
    {
        [self moveFirstResponderByOffset:-1];
    }
    else
    {
        [self moveFirstResponderByOffset:1];
    }
    _navigationControl.selectedSegmentIndex = -1;
}

-(void)moveFirstResponderBack
{
    [self moveFirstResponderByOffset:-1];
}

-(void)moveFirstResponderNext
{
    [self moveFirstResponderByOffset:1];
}

-(void)moveFirstResponderByOffset:(NSInteger)offset
{
    id nextPosition = self.currentPosition;
    do
    {
        nextPosition = [self positionFromPosition:nextPosition offset:offset];
        
        UIResponder * nextFirstResponderView = [self firstResponderViewForPosition:nextPosition];
        
        if ([nextFirstResponderView becomeFirstResponder])
        {
            return;
        }
    } while (![self.currentPosition isEqual:nextPosition]);
}

-(void)doneButtonTapped
{
    UIResponder * firstResponderView = [self firstResponderViewForPosition:self.currentPosition];
    [firstResponderView resignFirstResponder];
}

-(void)willMoveToSuperview:(UIView *)newSuperview
{
    self.frame = CGRectMake(0, 0, newSuperview.frame.size.width, 44);
}

-(void)setCurrentPosition:(id)currentPosition
{
    _currentPosition = currentPosition;
    if (_currentPosition)
    {
        [self setNeedsLayout];
    }
}

- (UIResponder*)firstResponderViewForPosition:(id)position
{
    if ([self.delegate respondsToSelector:@selector(OCGFirstResponderAccessoryView:firstResponderViewForPosition:)])
    {
        return [self.delegate OCGFirstResponderAccessoryView:self firstResponderViewForPosition:position];
    }
    else
    {
        return nil;
    }
}

- (id)positionFromPosition:(id)position offset:(NSInteger)offset
{
    if ([self.delegate respondsToSelector:@selector(OCGFirstResponderAccessoryView:positionFromPosition:offset:)])
    {
        return [self.delegate OCGFirstResponderAccessoryView:self positionFromPosition:position offset:offset];
    }
    else
    {
        return self.currentPosition;
    }
}

@end