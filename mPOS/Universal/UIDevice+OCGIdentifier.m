//
//  UIDevice+OCGIdentifier.m
//  mPOS
//
//  Created by John Scott on 03/04/2014.
//  Copyright (c) 2014 Clarity. All rights reserved.
//

#import "UIDevice+OCGIdentifier.h"

@implementation UIDevice (OCGIdentifier)

static NSString *DEVICE_IDENITIFIER_KEY = @"UIDevice_OCGIdentifier_DEVICE_IDENITIFIER_KEY";

- (NSString *) uniqueDeviceIdentifier
{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSString *uniqueDeviceIdentifier = [userDefaults stringForKey:DEVICE_IDENITIFIER_KEY];
    if (uniqueDeviceIdentifier == nil)
    {
        NSUUID *uuid = [[NSUUID alloc] init];
        uniqueDeviceIdentifier = [uuid UUIDString];
        [userDefaults setObject:uniqueDeviceIdentifier forKey:DEVICE_IDENITIFIER_KEY];
        [userDefaults synchronize];
    }
    return uniqueDeviceIdentifier;
}

@end
