//
//  OCGCollectionViewCell.m
//  mPOS
//
//  Created by John Scott on 10/07/2014.
//  Copyright (c) 2014 Clarity. All rights reserved.
//

#import "OCGCollectionViewCell.h"

#import "OCGTextField.h"
#import "UIImage+CRSUniversal.h"
#import "NSIndexPath+OCGExtensions.h"
#import "CGGeometry+OCGExtensions.h"

#import <objc/runtime.h>

id _OCGCollectionViewCellCast(id self, SEL _cmd, Class aClass, NSObject* anObject)
{
    NSAssert(anObject == nil || [anObject isKindOfClass:aClass], @"%@ is not a %@", NSStringFromClass([anObject class]), NSStringFromClass(aClass));
    return (id) anObject;
}

#import <objc/runtime.h>

Class _OCGCollectionViewCellDetailViewClassForClass(Class aClass)
{
#if DEBUG
    Class requestedClass = aClass;
    
    const char *className = [[@"_OCGCollectionViewCellDetailView" stringByAppendingString:NSStringFromClass(requestedClass)] UTF8String];
    
    aClass = objc_getClass(className);
    
    if (aClass == NULL)
    {
        aClass = objc_allocateClassPair(requestedClass, className, 0);
        objc_registerClassPair(aClass);
    }
#endif
    return aClass;
}

@interface UIView ()

/**@property OCGCollectionViewCell_collectionViewCell
 @brief Points to the parent collectionView cell if the view is a -[OCGCollectionViewCell detailView].
 */

@property (nonatomic, weak) OCGCollectionViewCell* OCGCollectionViewCell_collectionViewCell;

@end

@implementation OCGCollectionViewCell
{
    OCGCollectionViewCellDetailOptions _detailOptions;
    UILabel* _textLabel;
    UIView *_detailView;
    UILabel *_detailRequirementLabel;
    OCGCollectionViewCellDetailRequirement _detailRequirement;
    UIImageView *_imageView;
    UIImageView *_accessoryTypeView;
    NSIndexPath *_indexPath;
}

+(NSString*)reuseIdentifierForDetailClass:(Class)aClass
                                  options:(OCGCollectionViewCellDetailOptions)options
{
    return [NSString stringWithFormat:@"__%@__%@__%@", NSStringFromClass([self class]), (aClass == Nil) ? @"" : NSStringFromClass(aClass), @(options)];
}

+(Class)detailClassForReuseIdentifier:(NSString*)reuseIdentifier
{
    NSArray *parts = [reuseIdentifier componentsSeparatedByString:@"__"];
    NSString *className = parts[2];
    if (className.length > 0) {
        return _OCGCollectionViewCellDetailViewClassForClass(NSClassFromString(className));
    } else {
        return Nil;
    }
}

+(OCGCollectionViewCellDetailOptions)optionsForReuseIdentifier:(NSString*)reuseIdentifier
{
    NSArray *parts = [reuseIdentifier componentsSeparatedByString:@"__"];
    return [parts[3] integerValue];
}

+(NSString*)registerForDetailClass:(Class)aClass
                           options:(OCGCollectionViewCellDetailOptions)options
                    collectionView:(UICollectionView *)collectionView
{
    NSString* reuseIdentifier = [self reuseIdentifierForDetailClass:aClass options:options];
    [collectionView registerClass:[self class] forCellWithReuseIdentifier:reuseIdentifier];
    return reuseIdentifier;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.contentView.clipsToBounds = YES;
        
        _imageView = [[UIImageView alloc] init];
        _imageView.translatesAutoresizingMaskIntoConstraints = NO;
        _imageView.backgroundColor = [UIColor clearColor];
        _imageView.contentMode = UIViewContentModeScaleAspectFit;
        [self.contentView addSubview:_imageView];
        
        _textLabel = [[UILabel alloc] init];
        _textLabel.translatesAutoresizingMaskIntoConstraints = NO;
        _textLabel.backgroundColor = [UIColor clearColor];
        _textLabel.textAlignment = NSTextAlignmentLeft;
        [self.contentView addSubview:_textLabel];
        
        _detailRequirementLabel = [[UILabel alloc] init];
        _detailRequirementLabel.translatesAutoresizingMaskIntoConstraints = NO;
        _detailRequirementLabel.backgroundColor = [UIColor clearColor];
        _detailRequirementLabel.textAlignment = NSTextAlignmentCenter;
        [self.contentView addSubview:_detailRequirementLabel];
        
        _accessoryTypeView = [[UIImageView alloc] init];
        _accessoryTypeView.translatesAutoresizingMaskIntoConstraints = NO;
        _accessoryTypeView.backgroundColor = [UIColor clearColor];
        _accessoryTypeView.contentMode = UIViewContentModeScaleAspectFit;
        [self.contentView addSubview:_accessoryTypeView];
        
        if ([UIDevice currentDevice].systemVersion.floatValue >= 7.0)
        {
            _textLabel.highlightedTextColor = nil;
        }
        else
        {
            _textLabel.highlightedTextColor = [UIColor whiteColor];
        }
        self.clipsToBounds = YES;
        self.forceHideTextLabel = NO;
    }
    return self;
}

- (void)constrainsFromFrame:(CGRect)frame
                    forView:(UIView *)view
{
    NSMutableArray *constraintsToRemove = [NSMutableArray array];
    for (NSLayoutConstraint *constraint in self.constraints) {
        if ((constraint.firstItem == view) || (constraint.secondItem == view)) {
            [constraintsToRemove addObject:constraint];
        }
    }
    [self removeConstraints:constraintsToRemove];
    [view removeConstraints:constraintsToRemove];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:view
                                                     attribute:NSLayoutAttributeLeft
                                                     relatedBy:NSLayoutRelationEqual
                                                        toItem:self
                                                     attribute:NSLayoutAttributeLeft
                                                    multiplier:1.0f
                                                      constant:frame.origin.x]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:view
                                                     attribute:NSLayoutAttributeTop
                                                     relatedBy:NSLayoutRelationEqual
                                                        toItem:self
                                                     attribute:NSLayoutAttributeTop
                                                    multiplier:1.0f
                                                      constant:frame.origin.y]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:view
                                                     attribute:NSLayoutAttributeWidth
                                                     relatedBy:NSLayoutRelationEqual
                                                        toItem:nil
                                                     attribute:NSLayoutAttributeNotAnAttribute
                                                    multiplier:1.0f
                                                      constant:frame.size.width]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:view
                                                     attribute:NSLayoutAttributeHeight
                                                     relatedBy:NSLayoutRelationEqual
                                                        toItem:nil
                                                     attribute:NSLayoutAttributeNotAnAttribute
                                                    multiplier:1.0f
                                                      constant:frame.size.height]];
}

-(void)layoutSubviews
{
    [super layoutSubviews];
    
    CGRect frame = self.bounds;
    
//    NSLog(@"%@ frame %@ bounds %@", [_indexPath OCGExtensions_description], NSStringFromCGRect(self.frame), NSStringFromCGRect(self.bounds));
    
    CGSize detailViewSize = [_detailView sizeThatFits:frame.size];
    
    if ((CGRectGetWidth(frame) > 280 || (_detailView == nil) || self.forceTextLabelDisplay) && (!self.forceHideTextLabel || (_detailView == nil)))
    {
        if (_imageView.image.size.width && !_minimumImageViewWidth)
        {
            CGRectSlice(frame, &frame, 10, CGRectMinXEdge);
        }
        
        CGFloat imageViewWidth = MAX(_minimumImageViewWidth, _imageView.image.size.width);
        CGRectSlice(frame, &frame, MAX((_minimumImageViewWidth - _imageView.image.size.width)/2., 0), CGRectMinXEdge);
        
//        _imageView.frame = CGRectSlice(frame, &frame, _imageView.image.size.width, CGRectMinXEdge);
        [self constrainsFromFrame:CGRectSlice(frame, &frame, _imageView.image.size.width, CGRectMinXEdge)
                          forView:_imageView];
        
        if (imageViewWidth)
        {
            CGRectSlice(frame, &frame, MAX((_minimumImageViewWidth - _imageView.image.size.width)/2., 0), CGRectMinXEdge);
        }
        
        if (_accessoryTypeView.image.size.width)
        {
            CGRectSlice(frame, &frame, 5, CGRectMaxXEdge);
        }
        
//        _accessoryTypeView.frame = CGRectSlice(frame, &frame, _accessoryTypeView.image.size.width, CGRectMaxXEdge);
        [self constrainsFromFrame:CGRectSlice(frame, &frame, _accessoryTypeView.image.size.width, CGRectMaxXEdge)
                          forView:_accessoryTypeView];
        
        CGRect textLabelTextRect = [_textLabel textRectForBounds:frame limitedToNumberOfLines:_textLabel.numberOfLines];
        
        CGFloat textLabelWidth = CGRectGetWidth(textLabelTextRect);

        if (textLabelWidth && !_minimumImageViewWidth)
        {
            CGRectSlice(frame, &frame, 10, CGRectMinXEdge);
        }
        
//        _textLabel.frame = CGRectSlice(frame, &frame, textLabelWidth, CGRectMinXEdge);
        [self constrainsFromFrame:CGRectSlice(frame, &frame, textLabelWidth, CGRectMinXEdge)
                          forView:_textLabel];
        
        CGRect detailRequirementLabelTextRect = [_detailRequirementLabel textRectForBounds:frame limitedToNumberOfLines:_detailRequirementLabel.numberOfLines];
        
        if (CGRectGetWidth(textLabelTextRect) && CGRectGetWidth(detailRequirementLabelTextRect))
        {
            CGRectSlice(frame, &frame, 1, CGRectMinXEdge);
        }
        
//        _detailRequirementLabel.frame = CGRectSlice(frame, &frame, CGRectGetWidth(detailRequirementLabelTextRect), CGRectMinXEdge);
        [self constrainsFromFrame:CGRectSlice(frame, &frame, CGRectGetWidth(detailRequirementLabelTextRect), CGRectMinXEdge)
                          forView:_detailRequirementLabel];
        
        if (_textLabelWidth > 0)
        {
            CGRectSlice(frame, &frame, MAX(0, _textLabelWidth - (CGRectGetWidth(textLabelTextRect) + CGRectGetWidth(detailRequirementLabelTextRect))), CGRectMinXEdge);
        }
        
        if (detailViewSize.width)
        {
            CGRectSlice(frame, &frame, 10, CGRectMinXEdge);
            CGRectSlice(frame, &frame, 10, CGRectMaxXEdge);
        }
        
        if (_detailOptions & OCGCollectionViewCellDetailOptionUseTextLabelBaseline)
        {
            // do nothing
        }
        else
        {
            frame = CGRectSlice(frame, NULL, detailViewSize.width, CGRectMaxXEdge);
            frame = CGRectInset(frame, 0, (CGRectGetHeight(frame) - detailViewSize.height)/2.);
        }
    }
    else
    {
        if (_accessoryTypeView.image.size.width)
        {
            CGRectSlice(frame, &frame, 5, CGRectMaxXEdge);
        }
        
//        _accessoryTypeView.frame = CGRectSlice(frame, &frame, _accessoryTypeView.image.size.width, CGRectMaxXEdge);
        [self constrainsFromFrame:CGRectSlice(frame, &frame, _accessoryTypeView.image.size.width, CGRectMaxXEdge)
                          forView:_accessoryTypeView];
        
        if (detailViewSize.width)
        {
            CGRectSlice(frame, &frame, 3, CGRectMinXEdge);
            CGRectSlice(frame, &frame, 3, CGRectMaxXEdge);
        }
        
//        _imageView.frame = CGRectZero;
        [self constrainsFromFrame:CGRectZero
                          forView:_imageView];
//        _textLabel.frame = CGRectZero;
        [self constrainsFromFrame:CGRectZero
                          forView:_textLabel];
//        _detailRequirementLabel.frame = CGRectZero;
        [self constrainsFromFrame:CGRectZero
                          forView:_detailRequirementLabel];
//        _accessoryTypeView.frame = CGRectZero;
        [self constrainsFromFrame:CGRectZero
                          forView:_accessoryTypeView];
        
        if (_detailOptions & OCGCollectionViewCellDetailOptionUseTextLabelBaseline)
        {
            // do nothing
        }
        else
        {
            frame = CGRectInset(frame, 0, (CGRectGetHeight(frame) - detailViewSize.height)/2.);
            frame = CGRectInset(frame, (CGRectGetWidth(frame) - detailViewSize.width)/2., 0);
        }
    }
    
//    _detailView.frame = frame;
    if (_detailView) {
        [self constrainsFromFrame:frame
                          forView:_detailView];
    }
    
//    DebugLog(@"%@ %@ (T %@ - D %@)", self.textLabel.text, self.detailTextLabel.text, NSStringFromCGRect(self.textLabel.frame), NSStringFromCGRect(self.detailView.frame));
    
//    self.contentView.backgroundColor = [[UIColor brownColor] colorWithAlphaComponent:0.5];
//    _imageView.backgroundColor = [[UIColor redColor] colorWithAlphaComponent:0.5];
//    _textLabel.backgroundColor = [[UIColor orangeColor] colorWithAlphaComponent:0.5];
//    _detailView.backgroundColor = [[UIColor greenColor] colorWithAlphaComponent:0.5];
//    _detailRequirementLabel.backgroundColor = [[UIColor yellowColor] colorWithAlphaComponent:0.5];
//    _accessoryTypeView.backgroundColor = [[UIColor blueColor] colorWithAlphaComponent:0.5];
}

-(void)setMinimumImageViewWidth:(CGFloat)imageViewWidth
{
    _minimumImageViewWidth = imageViewWidth;
    [self setNeedsLayout];
}

-(UIImageView*)imageView
{
    return _imageView;
}

-(void)setAccessoryType:(UITableViewCellAccessoryType)accessoryType
{
    _accessoryType = accessoryType;
    
    if (accessoryType == UITableViewCellAccessoryDisclosureIndicator)
    {
        _accessoryTypeView.image = [[UIImage imageNamed:@"UIImages/UITableNext"] imageTintedWithColor:[UIColor colorWithRed:0.78 green:0.78 blue:0.80 alpha:1.0]];
    }
    else if (accessoryType == UITableViewCellAccessoryCheckmark)
    {
        _accessoryTypeView.image = [UIImage imageNamed:@"UIImages/GreenCheckSelected"];
    }
    else
    {
        _accessoryTypeView.image = nil;
    }
    [self setNeedsLayout];
}

-(void)setTextLabelWidth:(CGFloat)textLabelWidth
{
    _textLabelWidth = textLabelWidth + 16;
    [self setNeedsLayout];
}

-(UILabel*)textLabel
{
    return _textLabel;
}

-(UILabel*)detailTextLabel
{
    return [self detailViewWithClass:[UILabel class]];
}

-(id)detailViewWithClass:(Class)aClass
{
    if ([_detailView isKindOfClass:aClass])
    {
        return _detailView;
    }
    else
    {
        return nil;
    }
}

-(UIView*)detailView
{
    return _detailView;
}

- (void)applyLayoutAttributes:(UICollectionViewLayoutAttributes *)layoutAttributes;
{
    [self applyLayoutWithdDtailViewClass:[self.class detailClassForReuseIdentifier:self.reuseIdentifier]
                           detailOptions:[self.class optionsForReuseIdentifier:self.reuseIdentifier]
                               indexPath:layoutAttributes.indexPath];
}

- (void)applyLayoutWithdDtailViewClass:(Class)detailViewClass
                         detailOptions:(OCGCollectionViewCellDetailOptions)detailOptions
                             indexPath:(NSIndexPath*)indexPath
{
    _detailOptions = detailOptions;
    _indexPath = indexPath;

    if ((!_detailView) && (detailViewClass != Nil))
    {
        _detailView = [[detailViewClass alloc] init];
        _detailView.OCGCollectionViewCell_collectionViewCell = self;
        _detailView.translatesAutoresizingMaskIntoConstraints = NO;
        [self.contentView addSubview:_detailView];
        
        if ([_detailView isKindOfClass:[UILabel class]])
        {
            self.detailTextLabel.backgroundColor = [UIColor clearColor];
        }
        else if ([_detailView isKindOfClass:[UITextField class]])
        {
            self.detailTextField.clearButtonMode = UITextFieldViewModeAlways;
            self.detailTextField.backgroundColor = [UIColor clearColor];
        }
        
    }
    [self setNeedsLayout];
}

- (void)prepareForReuse
{
    self.forceTextLabelDisplay = NO;
    self.detailRequirement = OCGCollectionViewCellDetailRequirementNone;
    [super prepareForReuse];
}

-(void)setDetailRequirement:(OCGCollectionViewCellDetailRequirement)detailRequirement
{
    _detailRequirement = detailRequirement;
    
    switch (_detailRequirement) {
        case OCGCollectionViewCellDetailRequirementNone:
            _detailRequirementLabel.text = nil;
            break;
        case OCGCollectionViewCellDetailRequirementRequired:
            _detailRequirementLabel.text = @"✱";
            _detailRequirementLabel.font = [UIFont fontWithName:@"Menlo-Regular" size:10];
            _detailRequirementLabel.textColor = [UIColor colorWithRed:1 green:0 blue:0 alpha:0.5];
            break;
        default:
            break;
    }
    [self setNeedsLayout];
}

// http://stackoverflow.com/a/15484395/542244

-(void) setHighlighted:(BOOL)highlighted
{
    [super setHighlighted:highlighted];
    if (highlighted && _selectionStyle != UITableViewCellSelectionStyleNone)
    {
        self.backgroundColor = [UIColor colorWithRed:0.85 green:0.85 blue:0.85 alpha:1.0];
    }
    else
    {
        self.backgroundColor = [UIColor whiteColor];
    }
}

@end

@implementation UIView (OCGCollectionViewCell)

static char collectionViewCellKey;

- (void)setOCGCollectionViewCell_collectionViewCell:(OCGCollectionViewCell *)collectionViewCell
{
    objc_setAssociatedObject(self, &collectionViewCellKey, collectionViewCell, OBJC_ASSOCIATION_ASSIGN);
}

- (OCGCollectionViewCell*)OCGCollectionViewCell_collectionViewCell
{
    
    OCGCollectionViewCell *collectionViewCell = objc_getAssociatedObject(self, &collectionViewCellKey);
    
    return collectionViewCell;
}

@end

@implementation  OCGCollectionViewCell (OCGTextFieldCollectionViewCell)

-(UITextField *)detailTextField
{
    return [self detailViewWithClass:[UITextField class]];
}

@end
