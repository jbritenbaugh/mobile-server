//
//  OCGTextField.m
//  mPOS
//
//  Created by John Scott on 27/05/2014.
//  Copyright (c) 2014 Clarity. All rights reserved.
//

#import "OCGTextField.h"

@implementation OCGTextField

/*
 The code below is there to ensure that when the user taps on the text field, but the
 text field should not be edited (see textFieldShouldBeginEditing:), then the tap is passed
 through to the table view cell where it can be handled normally.
 
 Note that only the main view is affected, other sub views (like the clear button) are left
 unaffected.
 */

BOOL _OCGTextFieldShouldBeginEditing(OCGTextField* textField)
{
    return ![textField.delegate respondsToSelector:@selector(textFieldShouldBeginEditing:)]
    || [textField.delegate textFieldShouldBeginEditing:textField];
}

- (UIView *)hitTest:(CGPoint)point withEvent:(UIEvent *)event
{
    UIView *view = [super hitTest:point withEvent:event];
    if (self == view)
    {
        if (!_OCGTextFieldShouldBeginEditing(self))
        {
            return nil;
        }
    }
    return view;
}

-(BOOL)canBecomeFirstResponder
{
    BOOL canBecomeFirstResponder = [super canBecomeFirstResponder];
    if (!_OCGTextFieldShouldBeginEditing(self))
    {
        canBecomeFirstResponder = NO;
    }
    return canBecomeFirstResponder;
}

#pragma mark BarcodeInput

-(void)insertBarcode:(NSString *)barcode entryMethod:(ScannerEntryMethod)entryMethod barcodeType:(SMBarcodeType)barcodeType
{
    self.text = nil;
    [self insertText:barcode];
    [self resignFirstResponder];
}

@end
