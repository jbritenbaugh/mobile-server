//
//  OCGPOSKeyboardToolbar.m
//  mPOS
//
//  Created by Antonio Strijdom on 15/11/2013.
//  Copyright (c) 2013 Clarity. All rights reserved.
//

#import "OCGPOSKeyboardToolbar.h"
#import "CRSSkinning.h"
#import "OCGKeyboardTypeButtonItem.h"

@interface OCGPOSKeyboardToolbar ()
@property (nonatomic, strong) UIBarButtonItem *keyboardTypeNumeric;
@property (nonatomic, strong) UIBarButtonItem *keyboardTypeAlphaNumeric;
- (IBAction) switchToNumericKeyboard:(id)sender;
- (IBAction) switchToAlphaNumericKeyboard:(id)sender;
@end

@implementation OCGPOSKeyboardToolbar

#pragma mark - Properties

@synthesize textField = _textField;
- (void) setTextField:(UITextField *)textField
{
    _textField = textField;
    [self updateVirtualKeyboardToolbar];
}

#pragma mark - Actions

/** @brief Activates the numeric virtual keyboard.
 */
- (IBAction) switchToNumericKeyboard:(id)sender
{
    [OCGKeyboardTypeButtonItem setKeyboardType:kOCGKeyboardTypeNumberPad forTarget:self.textField];
    [self.textField resignFirstResponder];
    [self.textField becomeFirstResponder];
    [self updateVirtualKeyboardToolbar];
    [[CRSSkinning currentSkin] applyViewSkin:self
                             withTagOverride:self.tag];
}

/** @brief Activates the alphanumeric virtual keyboard.
 */
- (IBAction) switchToAlphaNumericKeyboard:(id)sender
{
    self.textField.keyboardType = UIKeyboardTypeASCIICapable;
    [self.textField resignFirstResponder];
    [self.textField becomeFirstResponder];
    [self updateVirtualKeyboardToolbar];
    [[CRSSkinning currentSkin] applyViewSkin:self
                             withTagOverride:self.tag];
}

#pragma mark - Methods

/** @brief Updates the virtual keyboard's toolbar to
 *  display the currently used keyboard type
 */
- (void) updateVirtualKeyboardToolbar
{
    if ((self.keyboardTypeNumeric == nil) ||
        (self.keyboardTypeAlphaNumeric == nil)) {
        // setup the keyboard input view for switching between number and alphanumeric
        self.keyboardTypeNumeric =
        [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁭󠁌󠁔󠁣󠁩󠁫󠁌󠁅󠁕󠁷󠁗󠁩󠁇󠁍󠁲󠁈󠁄󠁱󠁗󠀹󠁆󠁔󠁡󠁓󠀸󠁺󠁁󠁿*/ @"0..9", nil)
                                         style:UIBarButtonItemStylePlain
                                        target:self
                                        action:@selector(switchToNumericKeyboard:)];
        self.keyboardTypeNumeric.tag = kPrimaryButtonSkinningTag;
        
        self.keyboardTypeAlphaNumeric =
        [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠀳󠁶󠁌󠁮󠁃󠁑󠁙󠁃󠁖󠀰󠁔󠁡󠁚󠀳󠁒󠁫󠁚󠁕󠁶󠁄󠀸󠀷󠁤󠁊󠁺󠁄󠁷󠁿*/ @"a..Z", nil)
                                         style:UIBarButtonItemStylePlain
                                        target:self
                                        action:@selector(switchToAlphaNumericKeyboard:)];
        self.keyboardTypeAlphaNumeric.tag = kPrimaryButtonSkinningTag;
    }
    // update the buttons
    if (self.textField.keyboardType == UIKeyboardTypeNumberPad) {
        [self setItems:@[self.keyboardTypeAlphaNumeric]
              animated:YES];
    } else {
        [self setItems:@[self.keyboardTypeNumeric]
              animated:YES];
    }
}

@end
