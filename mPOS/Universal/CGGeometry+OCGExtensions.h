//
//  CGGeometry+OCGExtensions.h
//  mPOS
//
//  Created by John Scott on 14/08/2014.
//  Copyright (c) 2014 Clarity. All rights reserved.
//

CGRect CGRectSlice(CGRect rect, CGRect *remainder, CGFloat amount, CGRectEdge edge);
