//
//  NSIndexPath+OCGExtensions.h
//  mPOS
//
//  Created by John Scott on 08/08/2014.
//  Copyright (c) 2014 Clarity. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSIndexPath (OCGExtensions)

-(NSString*)OCGExtensions_description;

-(NSUInteger)OCGExtensions_lastIndexAtPosition;

@end
