//
//  OCGDateInputView.m
//  mPOS
//
//  Created by John Scott on 08/04/2015.
//  Copyright (c) 2015 Clarity. All rights reserved.
//

#import "OCGDateInputView.h"

@implementation OCGDateInputView

-(void)willMoveToSuperview:(UIView *)newSuperview
{
    if (newSuperview)
    {
        [self performBlock:^(id<OCGDateInput> firstResponder)
         {
             super.date = firstResponder.date ?: NSDate.date;
             super.datePickerMode = UIDatePickerModeDate;
         }];
    }
}

- (void)sendActionsForControlEvents:(UIControlEvents)controlEvents
{
    if (controlEvents & UIControlEventValueChanged)
    {
        [self performBlock:^(id<OCGDateInput> firstResponder)
         {
             if (![self.date isEqualToDate:firstResponder.date])
             {
                 firstResponder.date = self.date;
             }
         }];
    }
    
    [super sendActionsForControlEvents:controlEvents];
}

-(BOOL)performBlock:(void (^)(id <OCGDateInput> firstResponder))block
{
    id <OCGDateInput> firstResponder = (id <OCGDateInput>) [self nextResponder];
    if ([firstResponder conformsToProtocol:@protocol(OCGDateInput)])
    {
        block(firstResponder);
        return YES;
    }
    else
    {
        return NO;
    }
}

@end
