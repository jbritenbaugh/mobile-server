//
//  OCGDateField.m
//  mPOS
//
//  Created by John Scott on 08/04/2015.
//  Copyright (c) 2015 Clarity. All rights reserved.
//

#import "OCGDateField.h"
#import "OCGTextField.h"

NSString * const OCGDateFieldDateDidBeginEditingNotification = @"OCGDateFieldDateDidBeginEditingNotification";
NSString * const OCGDateFieldDateDidEndEditingNotification = @"OCGDateFieldDateDidEndEditingNotification";
NSString * const OCGDateFieldDateDidChangeNotification = @"OCGDateFieldDateDidChangeNotification";

@interface OCGDateField () <UITextFieldDelegate>

@end

@implementation OCGDateField
{
    OCGTextField *_internalView;
}

-(instancetype)init
{
    self = [super init];
    if (self)
    {
        _internalView = [[OCGTextField alloc] init];
        _internalView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        _internalView.delegate = self;
        
        [self addSubview:_internalView];
    }
    return self;
}

-(CGSize)sizeThatFits:(CGSize)size
{
    return [_internalView sizeThatFits:size];
}

-(void)setDate:(NSDate *)date
{
    _date = date;
    [NSNotificationCenter.defaultCenter postNotificationName:OCGDateFieldDateDidChangeNotification object:self];
    [self sendActionsForControlEvents:UIControlEventEditingChanged];
    [self updateLabel];
}

-(void)setDateFormatter:(NSDateFormatter *)dateFormatter
{
    _dateFormatter = dateFormatter;
    [self updateLabel];
}

-(void)updateLabel
{
    _internalView.text = [_dateFormatter stringFromDate:_date];
}

-(BOOL)canBecomeFirstResponder
{
    return YES;
}

-(BOOL)becomeFirstResponder
{
    if ([self canBecomeFirstResponder])
    {
        [NSNotificationCenter.defaultCenter postNotificationName:OCGDateFieldDateDidBeginEditingNotification object:self];
        [self sendActionsForControlEvents:UIControlEventEditingDidBegin];
    }
    BOOL result = [super becomeFirstResponder];
    if (result)
    {
        [super resignFirstResponder];
        [super becomeFirstResponder];
    }
    if (result && !_date)
    {
        self.date = [NSDate date];
    }
    return result;
}

-(BOOL)resignFirstResponder
{
    BOOL result = [super resignFirstResponder];
    if (result)
    {
        [self sendActionsForControlEvents:UIControlEventEditingDidEnd];
        [NSNotificationCenter.defaultCenter postNotificationName:OCGDateFieldDateDidEndEditingNotification object:self];
    }
    return result;
}

-(void)setTextAlignment:(NSTextAlignment)textAlignment
{
    _internalView.textAlignment = textAlignment;
}

-(NSTextAlignment)textAlignment
{
    return _internalView.textAlignment;
}

-(void)setClearButtonMode:(UITextFieldViewMode)clearButtonMode
{
    [_internalView setClearButtonMode:clearButtonMode];
}

-(UITextFieldViewMode)clearButtonMode
{
    return [_internalView clearButtonMode];
}

#pragma mark - UIKeyInput

/*
 The following methods are here for the sole purpose of letting the inputAccessoryView color be correct.
 UIKeyInout seems to be the thing it checks for.
 */

- (BOOL)hasText
{
    return _date != nil;
}
- (void)insertText:(NSString *)text
{
    
}

- (void)deleteBackward
{
    
}

- (UIView *)hitTest:(CGPoint)point withEvent:(UIEvent *)event
{
    CGPoint pointInSubview = [self convertPoint:point toView:_internalView];
    return [_internalView hitTest:pointInSubview withEvent:event];
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    return NO;
}

- (BOOL)textFieldShouldClear:(UITextField *)textField
{
    self.date = nil;
    return NO;
}

-(void)setTag:(NSInteger)tag
{
    [_internalView setTag:tag];
}

-(NSInteger)tag
{
    return [_internalView tag];
}

@end
