//
//  OCGIntegratedTextFieldKeyboardToolbar.h
//  mPOS
//
//  Created by Antonio Strijdom on 15/11/2013.
//  Copyright (c) 2013 Clarity. All rights reserved.
//

#import <UIKit/UIKit.h>

/** @file OCGIntegratedTextFieldKeyboardToolbar.h */

/** @brief View controller for managing the text entry tool bar.
 */
@interface OCGIntegratedTextFieldKeyboardToolbar : UIViewController

/** @property additionalView
 *  @brief Additional view to show above keyboard entry (e.g. scanning toolbar.
 */
@property (nonatomic, strong) UIView *additionalView;

/** @property utilityButtonView
 *  @brief View that holds the utility button to the left of the text field.
 */
@property (nonatomic, strong) UIView *utilityButtonView;

/** @property textField
 *  @brief The text field who's keyboard this toolbar is connected to.
 */
@property (nonatomic, strong) UITextField *textField;

/** @property doneButtonTitle
 *  @brief The title on the done button.
 */
@property (nonatomic, strong) NSString *addButtonTitle;

/** @property addButtonEnabled
 *  @brief Bool that determines if the add button should be enabled or not.
 */
@property (nonatomic, assign) BOOL addButtonEnabled;

/** @property parsedTextFieldText
 *  @brief id that returns a parsed version of the textfield's text
 */
@property (nonatomic, strong) id parsedTextFieldText;

/** @property addButtonTappedBlock
 *  @brief Block called when the add button is tapped.
 */
@property (nonatomic, strong) dispatch_block_t addButtonTappedBlock;

/** @brief Adds the keyboard toolbar to the view controller specified.
 *  @param parentViewController The view controller hosting the keyboard.
 */
- (void) addToViewController:(UIViewController *)parentViewController;

/** @brief Removes the keyboard toolbar from the view controller specified.
 *  @param parentViewController The view controller hosting the keyboard.
 */
- (void) removeFromViewController:(UIViewController *)parentViewController;

/** @brief Kicks off the keyboard display animation.
 */
- (void) showKeyboard;

/** @brief Hides the keyboard.
 *  @param complete the block to execute once the keyboard is hidden.
 */
- (void) hideKeyboardCompleted:(dispatch_block_t)complete;

@end
