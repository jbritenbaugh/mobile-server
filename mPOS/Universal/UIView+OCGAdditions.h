//
//  UIView+OCGAdditions.h
//  mPOS
//
//  Created by John Scott on 18/03/2014.
//  Copyright (c) 2014 Clarity. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (OCGAdditions)

-(void)rainbowBackgroundColor;

@end
