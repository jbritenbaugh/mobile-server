//
//  OCGTableViewCell.m
//  mPOS
//
//  Created by John Scott on 21/05/2014.
//  Copyright (c) 2014 Clarity. All rights reserved.
//

#import "OCGTableViewCell.h"

#import "OCGTextField.h"

#import <objc/runtime.h>

id _OCGTableViewCellCast(id self, SEL _cmd, Class aClass, NSObject* anObject)
{
    NSAssert(anObject == nil || [anObject isKindOfClass:aClass], @"%@ is not a %@", NSStringFromClass([anObject class]), NSStringFromClass(aClass));
    return (id) anObject;
}

#import <objc/runtime.h>

#define DEFINE_EMPTY_CLASS(aClass, aSuperClass) @interface aClass : aSuperClass @end @implementation aClass @end

DEFINE_EMPTY_CLASS(_OCGTableViewCellDetailViewWrapper, UIView)
DEFINE_EMPTY_CLASS(_OCGTableViewCellDetailRequirementLabel, UILabel)
DEFINE_EMPTY_CLASS(_OCGTableViewCellTextLabel, UILabel)
DEFINE_EMPTY_CLASS(_OCGTableViewCellImageView, UIImageView)

Class _OCGTableViewCellDetailViewClassForClass(Class aClass)
{
#if DEBUG
    Class requestedClass = aClass;
    
    const char *className = [[@"_OCGTableViewCellDetailView" stringByAppendingString:NSStringFromClass(requestedClass)] UTF8String];
    
    aClass = objc_getClass(className);
    
    if (aClass == NULL)
    {
        aClass = objc_allocateClassPair(requestedClass, className, 0);
        objc_registerClassPair(aClass);
    }
#endif
    return aClass;
}


@interface UIView (OCGTableViewCellPrivate)

/**@property OCGTableViewCell_tableViewCell
 @brief Points to the parent tableView cell if the view is a -[OCGTableViewCell detailView].
 */

@property (nonatomic, weak) OCGTableViewCell* OCGTableViewCell_tableViewCell;

@end


@implementation OCGTableViewCell
{
    UILabel* _textLabel;
    UIView *_detailView;
    UIView* _detailViewWrapper;
    NSLayoutConstraint *_textLabelWidthConstraint;
    UILabel *_detailRequirementLabel;
    OCGTableViewCellDetailRequirement _detailRequirement;
    UIImageView *_imageView;
    NSLayoutConstraint *_textImageOverlapConstraint;
    NSLayoutConstraint *_imageViewWidthContraint;
    BOOL _hasDetailView;
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        _imageView = [[_OCGTableViewCellImageView alloc] init];
        _imageView.translatesAutoresizingMaskIntoConstraints = NO;
        _imageView.backgroundColor = [UIColor clearColor];
        _imageView.contentMode = UIViewContentModeScaleAspectFit;

        [self.contentView addSubview:_imageView];
        
        _textLabel = [[_OCGTableViewCellTextLabel alloc] init];
        _textLabel.translatesAutoresizingMaskIntoConstraints = NO;
        _textLabel.backgroundColor = [UIColor clearColor];
        _textLabel.textAlignment = NSTextAlignmentLeft;
        [self.contentView addSubview:_textLabel];
        
        _detailRequirementLabel = [[_OCGTableViewCellDetailRequirementLabel alloc] init];
        _detailRequirementLabel.translatesAutoresizingMaskIntoConstraints = NO;
        _detailRequirementLabel.backgroundColor = [UIColor clearColor];
        _detailRequirementLabel.textAlignment = NSTextAlignmentCenter;
        [self.contentView addSubview:_detailRequirementLabel];
        
        _detailViewWrapper = [[_OCGTableViewCellDetailViewWrapper alloc] init];
        _detailViewWrapper.translatesAutoresizingMaskIntoConstraints = NO;
        _detailViewWrapper.backgroundColor = [UIColor clearColor];
        [self.contentView addSubview:_detailViewWrapper];
        
        
        if ([UIDevice currentDevice].systemVersion.floatValue >= 7.0)
        {
            _textLabel.highlightedTextColor = nil;
        }
        else
        {
            _textLabel.highlightedTextColor = [UIColor whiteColor];
        }
        
        [_imageView constrain:@"left = left + 15" to:self.contentView];
        [_imageView constrain:@"centerY = centerY" to:self.contentView];
        _imageViewWidthContraint = [_imageView constrain:@"width > 0" to:nil];
        
        [_textLabel setContentHuggingPriority:UILayoutPriorityRequired forAxis:UILayoutConstraintAxisHorizontal];
        
        _textImageOverlapConstraint = [_textLabel constrain:@"left > right" to:_imageView];
//        [[_textLabel constrain:@"left = left + 15" to:self.contentView] setPriority:999];
        NSLayoutConstraint *leftConstraint =
        [NSLayoutConstraint constraintWithItem:_textLabel
                                     attribute:NSLayoutAttributeLeft
                                     relatedBy:NSLayoutRelationEqual
                                        toItem:self.contentView
                                     attribute:NSLayoutAttributeLeft
                                    multiplier:1.0f
                                      constant:15.0f];
        leftConstraint.priority = 999;
        [self addConstraint:leftConstraint];
        [_textLabel constrain:@"centerY = centerY" to:self.contentView];
        
        [_detailRequirementLabel constrain:@"left = trailing + 2" to:_textLabel];
        [_detailRequirementLabel constrain:@"baseline = baseline - 5" to:_textLabel];
        
//        _textLabelWidthConstraint = [_detailViewWrapper constrain:@"left = right" to:_imageView];
        _textLabelWidthConstraint =
        [NSLayoutConstraint constraintWithItem:_detailViewWrapper
                                     attribute:NSLayoutAttributeLeft
                                     relatedBy:NSLayoutRelationEqual
                                        toItem:_imageView
                                     attribute:NSLayoutAttributeRight
                                    multiplier:1.0f
                                      constant:0.0f];
        _textLabelWidthConstraint.priority = UILayoutPriorityDefaultHigh;
        [self addConstraint:_textLabelWidthConstraint];
        self.textLabelWidth = 100;

        [_detailViewWrapper constrain:@"left > right + 5" to:_detailRequirementLabel];
        
        [_detailViewWrapper constrain:@"right = right - 10" to:self.contentView];
        [_detailViewWrapper constrain:@"top = top" to:self.contentView];
        [_detailViewWrapper constrain:@"bottom = bottom" to:self.contentView];
        
//        _imageView.backgroundColor = [UIColor colorWithRed:1 green:0 blue:0 alpha:0.5];
//        _textLabel.backgroundColor = [UIColor colorWithRed:0 green:1 blue:0 alpha:0.5];
//        _detailViewWrapper.backgroundColor = [UIColor colorWithRed:0 green:0 blue:1 alpha:0.5];
    }
    return self;
}

-(void)layoutSubviews
{
    if (_minimumImageViewWidth > _imageView.image.size.width)
    {
        _imageViewWidthContraint.constant =_minimumImageViewWidth;
    }
    else
    {
        _imageViewWidthContraint.constant = _imageView.image.size.width;
    }
    
    if ( _imageViewWidthContraint.constant > 0)
    {
        _textImageOverlapConstraint.constant = 15;
    }
    else
    {
        _textImageOverlapConstraint.constant = 0;
    }
    

    [super layoutSubviews];
}

-(void)setMinimumImageViewWidth:(CGFloat)imageViewWidth
{
    _minimumImageViewWidth = imageViewWidth;
    [self setNeedsLayout];
}

-(UIImageView*)imageView
{
    return _imageView;
}

@dynamic textLabelWidth;
-(void)setTextLabelWidth:(CGFloat)textLabelWidth
{
    _textLabelWidthConstraint.constant = textLabelWidth + 16;
}

-(CGFloat)textLabelWidth
{
    return _textLabelWidthConstraint.constant - 16;
}

-(UILabel*)textLabel
{
//    _textLabel.backgroundColor = [UIColor colorWithRed:0 green:1 blue:0 alpha:0.5];
    return _textLabel;
}

-(UILabel*)detailTextLabel
{
    if (!_hasDetailView || _OCGTableViewCellDetailViewClassForClass([UILabel class]) == [_detailView class])
    {
        BOOL created = NO;
        UILabel* detailTextLabel = [self detailViewWithClass:[UILabel class]
                                                     options:OCGTableViewCellDetailOptionUseTextLabelBaseline
                                                     created:&created];
        if (created)
        {
            detailTextLabel.backgroundColor = [UIColor clearColor];
        }
        return detailTextLabel;
    }
    return nil;
}

-(UIView*)detailView
{
    return _detailView;
}

- (id)detailViewWithClass:(Class)detailViewClass
{
    return [self detailViewWithClass:detailViewClass options:OCGTableViewCellDetailOptionNone created:NULL];
}

- (id)detailViewWithClass:(Class)detailViewClass options:(OCGTableViewCellDetailOptions)detailOptions created:(BOOL*)created
{
    detailViewClass = _OCGTableViewCellDetailViewClassForClass(detailViewClass);
    if (!_hasDetailView && _detailView)
    {
        [_detailView removeFromSuperview];
        _detailView = nil;
    }
    
    NSAssert(!_detailView || detailViewClass == [_detailView class], @"detailView class can't change while cell in use");
    
    if (created)
    {
        *created = !_detailView;
    }
    
    if (!_detailView)
    {
        
        _detailView = [[detailViewClass alloc] init];
        _detailView.OCGTableViewCell_tableViewCell = self;
        _detailView.translatesAutoresizingMaskIntoConstraints = NO;
        [_detailViewWrapper addSubview:_detailView];
        
        [_detailView constrain:@"right = right" to:_detailViewWrapper];
        
        if (detailOptions & OCGTableViewCellDetailOptionUseTextLabelBaseline)
        {
            [_detailView constrain:@"left = left" to:_detailViewWrapper];
            [_detailView constrain:@"baseline = baseline" to:_textLabel];
        }
        else
        {
            [_detailView constrain:@"centerY = centerY" to:_detailViewWrapper];
        }
        _hasDetailView = YES;
    }

    return _detailView;
}

- (void)prepareForReuse
{
    _hasDetailView = NO;
    self.detailRequirement = OCGTableViewCellDetailRequirementNone;
}

-(void)setDetailRequirement:(OCGTableViewCellDetailRequirement)detailRequirement
{
    _detailRequirement = detailRequirement;
    
    switch (_detailRequirement) {
        case OCGTableViewCellDetailRequirementNone:
            _detailRequirementLabel.text = nil;
            break;
        case OCGTableViewCellDetailRequirementRequired:
            _detailRequirementLabel.text = @"✱";
            _detailRequirementLabel.font = [UIFont fontWithName:@"Menlo-Regular" size:10];
            _detailRequirementLabel.textColor = [UIColor colorWithRed:1 green:0 blue:0 alpha:0.5];
            break;
        default:
            break;
    }
}

@end

@implementation UIView (OCGTableViewCell)

static char tableViewCellKey;

- (void)setOCGTableViewCell_tableViewCell:(OCGTableViewCell *)tableViewCell
{
    objc_setAssociatedObject(self, &tableViewCellKey, tableViewCell, OBJC_ASSOCIATION_ASSIGN);
}

- (OCGTableViewCell*)OCGTableViewCell_tableViewCell
{
    
    OCGTableViewCell *tableViewCell = objc_getAssociatedObject(self, &tableViewCellKey);
    
    return tableViewCell;
}

@end

@implementation  OCGTableViewCell (OCGTextFieldTableViewCell)

-(UITextField *)detailTextField
{
    if (!_hasDetailView || _OCGTableViewCellDetailViewClassForClass([OCGTextField class]) == [self.detailView class])
    {
        BOOL created = NO;
        UITextField *detailTextField = [self detailViewWithClass:[OCGTextField class]
                                                        options:OCGTableViewCellDetailOptionUseTextLabelBaseline
                                                        created:&created];
        if (created)
        {
            detailTextField.clearButtonMode = UITextFieldViewModeAlways;
            detailTextField.backgroundColor = [UIColor clearColor];
        }
        return detailTextField;
    }
    return nil;
}

@end
