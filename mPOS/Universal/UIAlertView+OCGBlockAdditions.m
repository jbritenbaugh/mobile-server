//
//  UIAlertView+OCGBlockAdditions.m
//
//  Created by John Scott on 10/04/13.
//  Copyright 2013 Omnico Group. All rights reserved.
//

#import "UIAlertView+OCGBlockAdditions.h"
#import <objc/runtime.h>

static char SHOULD_ENABLE_FIRST_OTHER_BUTTON_IDENTIFER;

@implementation UIAlertView (MKBlockAdditions)

- (void)setShouldEnableFirstOtherButtonBlock:(ShouldEnableFirstOtherButtonBlock)shouldEnableFirstOtherButtonBlock
{
    objc_setAssociatedObject(self, &SHOULD_ENABLE_FIRST_OTHER_BUTTON_IDENTIFER, shouldEnableFirstOtherButtonBlock, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

- (ShouldEnableFirstOtherButtonBlock)shouldEnableFirstOtherButtonBlock
{
    return objc_getAssociatedObject(self, &SHOULD_ENABLE_FIRST_OTHER_BUTTON_IDENTIFER);
}

+ (BOOL)alertViewShouldEnableFirstOtherButton:(UIAlertView *)alertView
{
    BOOL result = YES;
    if (alertView.shouldEnableFirstOtherButtonBlock)
    {
        result = alertView.shouldEnableFirstOtherButtonBlock();
    }
    return result;
}

__weak static UIAlertView *_showingAlertView;

+ (void)load {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _showingAlertView = nil;

        Class class = [self class];
            
        NSArray *selectors = @[@"show"];
        
        for (NSString *selector in selectors)
        {
            SEL originalSelector = NSSelectorFromString(selector);
            SEL swizzledSelector = NSSelectorFromString([@"OCGAdditions_" stringByAppendingString:selector]);
            
            Method originalMethod = class_getInstanceMethod(class, originalSelector);
            Method swizzledMethod = class_getInstanceMethod(class, swizzledSelector);
            
            method_exchangeImplementations(originalMethod, swizzledMethod);
        }
    });
}

- (void)OCGAdditions_show
{
    _showingAlertView = self;
    [self OCGAdditions_show];
}

+ (void)OCGAdditions_cancelAnimated:(BOOL)animated
{
    [_showingAlertView dismissWithClickedButtonIndex:_showingAlertView.cancelButtonIndex
                                            animated:animated];
}

@end