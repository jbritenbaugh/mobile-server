//
//  NSDate+OCGExtensions.m
//  mPOS
//
//  Created by John Scott on 18/05/2015.
//  Copyright (c) 2015 Clarity. All rights reserved.
//

#import "NSDate+OCGExtensions.h"

@implementation NSDate (OCGExtensions)

- (NSString *)OCGExtensions_humanReadableDescription
{
    NSDate *fromDate = self;
    NSDate *toDate = [NSDate date];
    NSCalendar *calendar = [NSCalendar currentCalendar];
    
    NSDateComponents *components = [calendar components: NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay|NSCalendarUnitHour|NSCalendarUnitMinute
                                               fromDate: fromDate
                                                 toDate: toDate
                                                options: 0];
    if (components.year != NSDateComponentUndefined)
    {
        if (components.year == 1)
        {
            return NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁡󠁏󠁘󠁳󠁧󠁌󠁥󠁬󠁷󠁗󠁊󠁴󠁑󠀶󠁒󠁍󠁃󠀷󠁳󠁕󠁉󠁙󠁙󠁗󠁋󠁏󠁷󠁿*/ @"one minute", nil);
        }
        else if (components.year > 1)
        {
            return [NSString stringWithFormat:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁑󠁇󠁺󠁨󠁸󠀵󠁐󠁷󠁃󠁃󠁩󠁊󠁂󠀱󠁺󠁦󠁫󠀸󠁌󠁄󠁡󠁉󠁅󠁌󠁄󠁢󠀸󠁿*/ @"%i minutes", nil), components.year];
        }
    }
    
    if (components.month != NSDateComponentUndefined)
    {
        if (components.month == 1)
        {
            return NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁊󠀶󠁮󠁥󠁶󠁈󠁗󠁭󠁨󠁆󠁆󠁘󠁚󠁧󠁎󠀷󠁨󠁯󠁊󠁵󠁳󠀵󠁰󠀷󠁹󠁁󠁁󠁿*/ @"one month", nil);
        }
        else if (components.month > 1)
        {
            return [NSString stringWithFormat:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁍󠁷󠀴󠀱󠁄󠁺󠁲󠁖󠀫󠁎󠁋󠁓󠁋󠁡󠁢󠁺󠁈󠁏󠁁󠁘󠁱󠁢󠁔󠁁󠁌󠁋󠁳󠁿*/ @"%i months", nil), components.month];
        }
    }
    
    if (components.day != NSDateComponentUndefined)
    {
        if (components.day == 7)
        {
            return NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁨󠁧󠁍󠁌󠁲󠁁󠁐󠁮󠁱󠁹󠁆󠁌󠁨󠁇󠁉󠁙󠁵󠀫󠁢󠁱󠁩󠁨󠁆󠁈󠀯󠁤󠁉󠁿*/ @"one week", nil);
        }
        else if ((components.day == 14) && (components.day < 21))
        {
            return NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁑󠁥󠁯󠁭󠁶󠁷󠀴󠁷󠁓󠁤󠀰󠁊󠀰󠁤󠁕󠁣󠁢󠁆󠁨󠁙󠁃󠁊󠁗󠁐󠁳󠁥󠀸󠁿*/ @"two weeks", nil);
        }
        else if ((components.day == 21) && (components.day < 32))
        {
            return NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁱󠁁󠁑󠁐󠁃󠁊󠁴󠁆󠁧󠁐󠀹󠁳󠁉󠀰󠁧󠁹󠁂󠁍󠀷󠁲󠁨󠀹󠁴󠁮󠁙󠀷󠀴󠁿*/ @"three weeks", nil);
        }
        else if (components.day == 1)
        {
            return NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁎󠁣󠀳󠀯󠁵󠀱󠀶󠁧󠁒󠁨󠁖󠁚󠀴󠁂󠁄󠁇󠁘󠁍󠀰󠀰󠁂󠁚󠀷󠁯󠁣󠁊󠁷󠁿*/ @"one day", nil);
        }
        else if (components.day > 0)
        {
            return [NSString stringWithFormat:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁉󠁯󠁇󠁌󠁆󠁒󠀶󠁰󠁯󠁊󠁹󠁩󠀯󠁮󠁓󠀫󠁘󠁨󠀸󠀯󠀴󠁱󠁳󠁃󠀹󠁖󠁯󠁿*/ @"%i days", nil), components.day];
        }
    }
    
    if (components.hour != NSDateComponentUndefined)
    {
        if (components.hour == 1)
        {
            return NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁑󠁦󠁨󠁓󠁩󠁊󠀯󠁪󠁹󠀫󠁨󠁸󠁸󠁦󠀰󠁹󠁆󠀲󠀸󠁕󠁙󠀳󠁂󠁡󠀯󠀵󠁧󠁿*/ @"one hour", nil);
        }
        else if (components.hour > 1)
        {
            return [NSString stringWithFormat:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁺󠁵󠁒󠁮󠁄󠀹󠁧󠀯󠁄󠁰󠁬󠁌󠁤󠁊󠁅󠁇󠁏󠀹󠁤󠁬󠁪󠀵󠁋󠁸󠀵󠁁󠀸󠁿*/ @"%i hours", nil), components.hour];
        }
    }
    
    if (components.minute != NSDateComponentUndefined)
    {
        if (components.minute == 1)
        {
            return NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁡󠁏󠁘󠁳󠁧󠁌󠁥󠁬󠁷󠁗󠁊󠁴󠁑󠀶󠁒󠁍󠁃󠀷󠁳󠁕󠁉󠁙󠁙󠁗󠁋󠁏󠁷󠁿*/ @"one minute", nil);
        }
        else if (components.minute > 1)
        {
            return [NSString stringWithFormat:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁑󠁇󠁺󠁨󠁸󠀵󠁐󠁷󠁃󠁃󠁩󠁊󠁂󠀱󠁺󠁦󠁫󠀸󠁌󠁄󠁡󠁉󠁅󠁌󠁄󠁢󠀸󠁿*/ @"%i minutes", nil), components.minute];
        }
    }
    
    return NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁄󠁐󠁃󠀶󠁳󠁰󠁳󠀫󠁭󠁧󠁵󠁮󠁸󠁍󠁒󠁁󠁰󠀴󠁍󠁌󠁕󠁓󠀶󠁺󠁯󠁬󠁕󠁿*/ @"a few seconds", nil);
}

@end
