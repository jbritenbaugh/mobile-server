//
//  OCGKeyboardTypeButtonItem.h
//  mPOS
//
//  Created by John Scott on 25/02/2015.
//  Copyright (c) 2015 Clarity. All rights reserved.
//

#import <UIKit/UIKit.h>

#ifndef IMPORTED_BY_OWN_IMPLEMENTION

extern NSString * const kOCGKeyboardTypeDefault;
extern NSString * const kOCGKeyboardTypeASCIICapable;
extern NSString * const kOCGKeyboardTypeNumbersAndPunctuation;
extern NSString * const kOCGKeyboardTypeURL;
extern NSString * const kOCGKeyboardTypeNumberPad;
extern NSString * const kOCGKeyboardTypePhonePad;
extern NSString * const kOCGKeyboardTypeNamePhonePad;
extern NSString * const kOCGKeyboardTypeEmailAddress;
extern NSString * const kOCGKeyboardTypeDecimalPad;

extern NSString * const kOCGKeyboardTypeDate;
extern NSString * const kOCGKeyboardTypeScan;

extern NSArray * const kOCGKeyboardTypesDefaultUPC;

#endif

@protocol OCGKeyboardTypeTraits <NSObject>

@property(nonatomic) UIKeyboardType keyboardType;
@property (readwrite, retain) UIView *inputView;
@property (readwrite, retain) UIView *inputAccessoryView;

@end

@interface OCGKeyboardTypeButtonItem : UIBarButtonItem

- (instancetype)initWithKeyboardTypes:(NSArray *)keyboardTypes style:(UIBarButtonItemStyle)style target:(UIResponder <OCGKeyboardTypeTraits> *)target;

+ (void)setKeyboardType:(NSString *)keyboardType forTarget:(UIResponder <OCGKeyboardTypeTraits> *)target;

@property (nonatomic, readonly) NSArray *keyboardTypes;

@end


@interface UITextField (OCGKeyboardTypeButtonItem) <OCGKeyboardTypeTraits>
@end

@interface UITextView (OCGKeyboardTypeButtonItem) <OCGKeyboardTypeTraits>
@end