//
//  OCGTableViewCell.h
//  mPOS
//
//  Created by John Scott on 21/05/2014.
//  Copyright (c) 2014 Clarity. All rights reserved.
//

/*
 OCGTableViewCell is a drop-in replacement for UITableViewCell (as apposed to OCGTableViewCell
 which uses a different interface).
 
 This class has four main features:
 
 detailView
 ^^^^^^^^^^
 
 detailView replaces detailTextLabel (which is still available). On first use detailViewWithClass:...
 to set which class the view will be. See detailViewWithClass:options:created: for details.
 
 detailRequirement
 ^^^^^^^^^^^^^^^^^
 
 Sets decals visible in the cell instructing the operator of required fields.
 
 textLabelWidth
 ^^^^^^^^^^^^^^
 
 Forces the textLabel to be a certain width (handy for aligning content in multiple cells)
 
 -[UIView OCGTableViewCell_tableViewCell]
 ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
 
 Points to the parent tableView cell if the view is a -[OCGTableViewCell detailView].
 
 */

#import <UIKit/UIKit.h>

id _OCGTableViewCellCast(id self, SEL _cmd, Class aClass, NSObject* anObject);

#define CAST(aClass, anObject) ((aClass*)_OCGTableViewCellCast(self, _cmd, [aClass class], anObject))

typedef NS_ENUM(NSUInteger, OCGTableViewCellDetailRequirement)
{
    OCGTableViewCellDetailRequirementNone,
    OCGTableViewCellDetailRequirementRequired, // Suffixes a red star to to the textLabel content.
};

typedef NS_ENUM(NSUInteger, OCGTableViewCellDetailOptions)
{
    OCGTableViewCellDetailOptionNone = 0,
    OCGTableViewCellDetailOptionUseTextLabelBaseline = 1 << 0, // Sets detailView.baseline == textLabel.baseline
};

@interface OCGTableViewCell : UITableViewCell

/** @brief Sets the minimum width of the imageView.
 */
@property (nonatomic, assign) CGFloat minimumImageViewWidth;

/** @brief Sets the width of the textLabel.
 where the textLabel can cut off longer text.
 */
@property (nonatomic, assign) CGFloat textLabelWidth;

/** @brief detailView
 detailView is a replacement for detailTextView. The view is created using detailViewWithClass:options:created:
 
 Note that detailTextView can still be used, it just defaults to detailView with a UITextField.
 DO NOT ABUSE THIS.
 */
@property (nonatomic, readonly) UIView* detailView;

- (id)detailViewWithClass:(Class)detailViewClass;

/** @brief detailViewWithClass:options:created:
 @arg detailViewClass the class of the detailView
 @arg options Options used when creating the detailView.
 @arg created retuns whether the detailView was created by this call. Handy for adding control event actions
 @return The detailView
 */
- (id)detailViewWithClass:(Class)detailViewClass options:(OCGTableViewCellDetailOptions)detailOptions created:(BOOL*)created;

/** @brief Use to set a decal for whether the field is required

 */
@property (nonatomic, assign) OCGTableViewCellDetailRequirement detailRequirement;

@end

@interface UIView (OCGTableViewCell)

/**@property OCGTableViewCell_tableViewCell
@brief Points to the parent tableView cell if the view is a -[OCGTableViewCell detailView].
 */

@property (nonatomic, readonly) OCGTableViewCell* OCGTableViewCell_tableViewCell;

@end

/*
 OCGTextFieldTableViewCell contains an extra convient method which sets up a UITextField as the detailView.
 
 This replicates the the old -[OCGTextFieldTableViewCell valueTextField].
 */

@interface OCGTableViewCell (OCGTextFieldTableViewCell)

@property(nonatomic, readonly) UITextField *detailTextField;

@end
