//
//  OCGInputView.h
//  mPOS
//
//  Created by John Scott on 25/02/2015.
//  Copyright (c) 2015 Clarity. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OCGInputAccessoryView : UIInputView

@property (nonatomic, strong) NSArray *leftButtons;
@property (nonatomic, strong) NSArray *rightButtons;

@property (nonatomic, readonly) UIView *contentView;

@end
