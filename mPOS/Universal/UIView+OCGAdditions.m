//
//  UIView+OCGAdditions.m
//  mPOS
//
//  Created by John Scott on 18/03/2014.
//  Copyright (c) 2014 Clarity. All rights reserved.
//

#import "UIView+OCGAdditions.h"
#import <objc/runtime.h>

@implementation UIView (OCGAdditions)

-(void)rainbowBackgroundColor
{
    CGFloat redLevel    = rand() / (float) RAND_MAX;
    CGFloat greenLevel  = rand() / (float) RAND_MAX;
    CGFloat blueLevel   = rand() / (float) RAND_MAX;
    
    self.backgroundColor = [UIColor colorWithRed: redLevel
                                           green: greenLevel
                                            blue: blueLevel
                                           alpha: 1.0];
    
    [self.subviews makeObjectsPerformSelector:_cmd];
}

+ (void)load {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        if ([UIDevice currentDevice].systemVersion.floatValue < 7.0)
        {
            Class class = [self class];
            
            NSArray *selectors = @[@"addSubview:"];
            
            for (NSString *selector in selectors)
            {
                SEL originalSelector = NSSelectorFromString(selector);
                SEL swizzledSelector = NSSelectorFromString([@"OCGAdditions_" stringByAppendingString:selector]);
                
                Method originalMethod = class_getInstanceMethod(class, originalSelector);
                Method swizzledMethod = class_getInstanceMethod(class, swizzledSelector);
                
                method_exchangeImplementations(originalMethod, swizzledMethod);
            }
        }
    });
}

- (void)OCGAdditions_addSubview:(UIView *)view
{
    /* http://stackoverflow.com/a/19818152/542244 This is a big hack to deal with an issue on iOS 6 */
    if (view.superview != nil && [view isMemberOfClass:[UICollectionView class]])
    {
        [view removeFromSuperview];
    }
    [self OCGAdditions_addSubview:view];
}

@end


