//
//  NSDate+OCGExtensions.h
//  mPOS
//
//  Created by John Scott on 18/05/2015.
//  Copyright (c) 2015 Clarity. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDate (OCGExtensions)

/**
 @brief Returns a human readable description of the date, eg "three weeks"
 
 */
- (NSString *) OCGExtensions_humanReadableDescription;

@end
