//
//  OCGIntegratedTextFieldKeyboardToolbar.m
//  mPOS
//
//  Created by Antonio Strijdom on 15/11/2013.
//  Copyright (c) 2013 Clarity. All rights reserved.
//

#import "OCGIntegratedTextFieldKeyboardToolbar.h"
#import "OCGSplitViewController.h"
#import "CRSSkinning.h"
#import "CRSGlossyButton.h"

@interface OCGIntegratedTextFieldKeyboardToolbar ()
@property (nonatomic, strong) UIView *textFieldView;
@property (nonatomic, strong) CRSGlossyButton *addButton;
@property (nonatomic, readonly) CGFloat toolBarHeight;
@property (nonatomic, strong) NSLayoutConstraint *bottomConstraint;
@property (nonatomic, strong) NSLayoutConstraint *utilityViewWidthConstraint;
@property (nonatomic, strong) NSLayoutConstraint *addButtonWidthConstraint;
@property (nonatomic, strong) dispatch_block_t hideCompleteBlock;
- (void) handleKeyboardWillShowNotification:(NSNotification *)note;
- (void) handleKeyboardWillHideNotification:(NSNotification *)note;
- (void) showHideKeyboardWithNotification:(NSNotification *)note
                            completeBlock:(void (^)(BOOL finished))completion;
- (void) keyboardWillShow;
- (IBAction) addButtonTapped:(id)sender;
@end

@implementation OCGIntegratedTextFieldKeyboardToolbar

#pragma mark - Private

const CGFloat kScannerKeyboardToolbarHeight = 100.0f;
const CGFloat kKeyboardToolbarHeight = 50.0f;

- (void) handleKeyboardWillShowNotification:(NSNotification *)note
{
    [self showHideKeyboardWithNotification:note
                             completeBlock:^(BOOL finished) {
                                 // show the view
                                 self.view.hidden = NO;
                             }];
}

- (void) handleKeyboardWillHideNotification:(NSNotification *)note
{
    [self showHideKeyboardWithNotification:note
                             completeBlock:^(BOOL finished) {
                                 // hide the view
                                 self.view.hidden = YES;
                                 if (self.hideCompleteBlock != nil) {
                                     self.hideCompleteBlock();
                                 }
                             }];
}

- (void) showHideKeyboardWithNotification:(NSNotification *)note
                            completeBlock:(void (^)(BOOL finished))completion
{
    // get the keyboard start rect
    NSValue *endValue = note.userInfo[UIKeyboardFrameEndUserInfoKey];
    if (endValue != nil) {
        CGRect endFrame = [endValue CGRectValue];
        CGRect localEndFrame = [self.parentViewController.view convertRect:endFrame
                                                                  fromView:nil];
        NSValue *startValue = note.userInfo[UIKeyboardFrameBeginUserInfoKey];
        CGRect startFrame = [startValue CGRectValue];
        CGRect localStartFrame = [self.parentViewController.view convertRect:startFrame
                                                                    fromView:nil];
        NSNumber *animationDuration = note.userInfo[UIKeyboardAnimationDurationUserInfoKey];
        NSNumber *animationCurve = note.userInfo[UIKeyboardAnimationCurveUserInfoKey];
        // reset the constraint
        self.bottomConstraint.constant = localStartFrame.origin.y - self.toolBarHeight;
        // show the view
        self.view.hidden = NO;
        // adjust the toolbar's bottom constraint
        [UIView animateWithDuration:animationDuration.doubleValue
                              delay:0.0f
                            options:animationCurve.unsignedIntegerValue
                         animations:^{
                             self.bottomConstraint.constant = localEndFrame.origin.y - self.toolBarHeight;
                             [self.parentViewController.view layoutIfNeeded];
                         }
                         completion:completion];
    }
}

- (void) keyboardWillShow
{
    // show/hide the utility view if required
    if (self.utilityButtonView.subviews.count == 0) {
        self.utilityViewWidthConstraint.constant = 0.0f;
    } else {
        self.utilityViewWidthConstraint.constant = 30.0f;
    }
    // register for keyboard notifications
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(handleKeyboardWillShowNotification:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(handleKeyboardWillHideNotification:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    // reset the text field
    self.textField.text = nil;
    // bring to front
    [self.view.superview bringSubviewToFront:self.view];
}

#pragma mark - Properties

@synthesize additionalView = _additionalView;
- (void) setAdditionalView:(UIView *)additionalView
{
    if (_additionalView != nil) {
        [self.view removeConstraints:_additionalView.constraints];
        [_additionalView removeFromSuperview];
    }
    _additionalView = additionalView;
    if ((_additionalView != nil) && (self.textFieldView != nil)) {
        _additionalView.translatesAutoresizingMaskIntoConstraints = NO;
        [self.view addSubview:_additionalView];
        [_additionalView constrain:@"top = top" to:self.view];
        [_additionalView constrain:@"left = left" to:self.view];
        [_additionalView constrain:@"right = right" to:self.view];
        [_additionalView constrain:@"bottom = top" to:self.textFieldView];
    }
}

@synthesize addButtonEnabled = _addButtonEnabled;
- (void) setAddButtonEnabled:(BOOL)addButtonEnabled
{
    _addButtonEnabled = addButtonEnabled;
    self.addButton.enabled = _addButtonEnabled;
}

@synthesize addButtonTitle = _addButtonTitle;
- (void) setAddButtonTitle:(NSString *)addButtonTitle
{
    _addButtonTitle = addButtonTitle;
    [self.addButton setTitle:_addButtonTitle
                    forState:UIControlStateNormal];
    self.addButtonWidthConstraint.constant =
    MAX(50.0f, [_addButtonTitle sizeWithAttributes:@{ NSFontAttributeName :self.addButton.titleLabel.font }].width + 10.0f);
    [self.addButton setNeedsLayout];
}

- (CGFloat) toolBarHeight
{
    CGFloat result = 0.0f;
    
    if (self.additionalView == nil) {
        result = kKeyboardToolbarHeight;
    } else {
        result = kScannerKeyboardToolbarHeight;
    }
    
    return result;
}

@synthesize textField = _textField;
- (void) setTextField:(UITextField *)textField
{
    if (_textField != nil) {
        [self.textFieldView removeConstraints:_textField.constraints];
        [_textField removeFromSuperview];
    }
    _textField = textField;
    if ((_textField != nil) && (self.textFieldView != nil)) {
        _textField.tag = kKeyboardAccessoryTextFieldTag;
        _textField.translatesAutoresizingMaskIntoConstraints = NO;
        [self.textFieldView addSubview:_textField];
        [_textField constrain:@"left = right + 5" to:self.utilityButtonView];
        [_textField constrain:@"right = left - 5" to:self.addButton];
        [_textField constrain:@"centerY = centerY" to:self.textFieldView];
        [_textField constrain:@"height = 25" to:nil];
        [[CRSSkinning currentSkin] applyViewSkin:_textField
                                 withTagOverride:kKeyboardAccessoryTextFieldTag];
    }
}

-(id)parsedTextFieldText
{
    return self.textField.text;
}


#pragma mark - UIView

- (void) viewDidLoad
{
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor clearColor];
    self.view.hidden = YES;

    // create sub views
    // text field view
    self.textFieldView = [[UIView alloc] init];
    self.textFieldView.tag = kKeyboardAccessoryTag;
    self.textFieldView.translatesAutoresizingMaskIntoConstraints = NO;
    [self.view addSubview:self.textFieldView];
    [self.textFieldView constrain:@"left = left" to:self.view];
    [self.textFieldView constrain:@"right = right" to:self.view];
    [self.textFieldView constrain:@"bottom = bottom" to:self.view];
    [self.textFieldView constrain:[NSString stringWithFormat:@"height = %f", kKeyboardToolbarHeight]
                               to:nil];
    // add the done button
    self.addButton = [[CRSGlossyButton alloc] init];
    self.addButton.tag = kKeyboardAccessoryButtonTag;
    self.addButton.translatesAutoresizingMaskIntoConstraints = NO;
    [self.textFieldView addSubview:self.addButton];
    [self.addButton constrain:@"right = right - 5" to:self.textFieldView];
    self.addButtonWidthConstraint = [self.addButton constrain:@"width = 50" to:nil];
    [self.addButton constrain:@"centerY = centerY" to:self.textFieldView];
    [self.addButton constrain:@"height = 30" to:nil];
    [self.addButton setTitle:self.addButtonTitle
                    forState:UIControlStateNormal];
    [self.addButton addTarget:self
                       action:@selector(addButtonTapped:)
             forControlEvents:UIControlEventTouchUpInside];
    // utility view
    self.utilityButtonView = [[UIView alloc] init];
    self.utilityButtonView.backgroundColor = [UIColor clearColor];
    self.utilityButtonView.translatesAutoresizingMaskIntoConstraints = NO;
    [self.textFieldView addSubview:self.utilityButtonView];
    [self.utilityButtonView constrain:@"left = left + 5" to:self.textFieldView];
    self.utilityViewWidthConstraint = [self.utilityButtonView constrain:@"width = 30" to:nil];
    [self.utilityButtonView constrain:@"centerY = centerY" to:self.textFieldView];
    [self.utilityButtonView constrain:@"height = 30" to:nil];
    // add the text field
    if (self.textField != nil) {
        self.textField.tag = kKeyboardAccessoryTextFieldTag;
        self.textField.translatesAutoresizingMaskIntoConstraints = NO;
        [self.textFieldView addSubview:self.textField];
        [_textField constrain:@"left = right + 5" to:self.utilityButtonView];
        [_textField constrain:@"right = left - 5" to:self.addButton];
        [_textField constrain:@"centerY = centerY" to:self.textFieldView];
        [_textField constrain:@"height = 25" to:nil];
    }
    // add the additional view
    if (self.additionalView != nil) {
        _additionalView.translatesAutoresizingMaskIntoConstraints = NO;
        [self.view addSubview:self.additionalView];
        [self.additionalView constrain:@"top = top" to:self.view];
        [self.additionalView constrain:@"left = left" to:self.view];
        [self.additionalView constrain:@"right = right" to:self.view];
        [self.additionalView constrain:@"bottom = top" to:self.textFieldView];
    }
}

- (void) viewWillAppear:(BOOL)animated
{
    [[CRSSkinning currentSkin] applyViewSkin:self];
    [super viewWillAppear:animated];
}

- (void) dealloc
{
    // unregister for keyboard notifications
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - Actions

- (IBAction) addButtonTapped:(id)sender
{
    if (self.addButtonTappedBlock != nil) {
        self.addButtonTappedBlock();
    }
}

#pragma mark - Methods

- (void) addToViewController:(UIViewController *)parentViewController
{
    UIViewController *hostViewController = parentViewController;
    UIView *parentView = parentViewController.view;
    if (parentViewController.splitViewController != nil) {
        hostViewController = parentViewController.splitViewController;
        parentView = parentViewController.splitViewController.mainViewWrapper;
    }
    
    [self willMoveToParentViewController:hostViewController];
    [hostViewController addChildViewController:self];
    [parentView addSubview:self.view];
    [parentView bringSubviewToFront:self.view];
    [self didMoveToParentViewController:hostViewController];
    self.view.translatesAutoresizingMaskIntoConstraints = NO;
    [self.view constrain:@"left = left" to:parentView];
    [self.view constrain:@"right = right" to:parentView];
    [self.view constrain:[NSString stringWithFormat:@"height = %f", self.toolBarHeight]
                      to:nil];
    // default to off the screen (hopefully)
    self.bottomConstraint =
    [self.view constrain:@"top = top + 999" to:parentView];
}

- (void) removeFromViewController:(UIViewController *)parentViewController
{
    UIViewController *hostViewController = parentViewController;
    UIView *parentView = parentViewController.view;
    if (parentViewController.splitViewController != nil) {
        hostViewController = parentViewController.splitViewController;
        parentView = parentViewController.splitViewController.mainViewWrapper;
    }
    
    [self willMoveToParentViewController:nil];
    [self removeFromParentViewController];
    [parentView removeConstraints:self.view.constraints];
    [self.view removeFromSuperview];
    [self didMoveToParentViewController:nil];
    self.bottomConstraint = nil;
}

- (void) showKeyboard
{
    // show the keyboard
    [self keyboardWillShow];
    [self.textField becomeFirstResponder];
}

- (void) hideKeyboardCompleted:(dispatch_block_t)complete
{
    if ([self.textField isFirstResponder]) {
        __weak OCGIntegratedTextFieldKeyboardToolbar *vc = self;
        self.hideCompleteBlock = ^() {
            // unregister for keyboard notifications
            [[NSNotificationCenter defaultCenter] removeObserver:vc];
            if (complete != nil) {
                complete();
            }
        };
        [self.textField resignFirstResponder];
    } else {
        if (complete != nil) {
            complete();
        }
    }
}

@end
