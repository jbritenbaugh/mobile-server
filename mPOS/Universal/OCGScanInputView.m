//
//  OCGScanInputView.m
//  mPOS
//
//  Created by John Scott on 09/04/2015.
//  Copyright (c) 2015 Clarity. All rights reserved.
//

#import "OCGScanInputView.h"

#import "BarcodeScannerController.h"

@implementation OCGScanInputView

-(void)didMoveToWindow
{
    if (self.window)
    {
        __weak OCGScanInputView *scanInputView = self;
        [BarcodeScannerController.sharedInstance beginScanningWithBlock:^(NSString *barcode, ScannerEntryMethod entryMethod, SMBarcodeType barcodeType, BOOL *continueScanning)
         {
             if (scanInputView.window)
             {
                 if ([scanInputView.nextResponder  respondsToSelector:@selector(insertBarcode:entryMethod:barcodeType:)])
                 {
                     [scanInputView.nextResponder insertBarcode:barcode entryMethod:entryMethod barcodeType:barcodeType];
                 }
                 else if ([scanInputView.nextResponder respondsToSelector:@selector(setText:)])
                 {
                     id input = (id) scanInputView.nextResponder;
                     [input setText:barcode];
                 }
             }
         } terminated:^{
             if (scanInputView.window)
             {
                 [scanInputView.nextResponder resignFirstResponder];
             }
         } error:^(NSString *errorDesc) {
             if (scanInputView.window)
             {
                 [scanInputView.nextResponder resignFirstResponder];
             }
         }];
    }
    else
    {
        [BarcodeScannerController.sharedInstance stopScanning];
    }
}

@end
