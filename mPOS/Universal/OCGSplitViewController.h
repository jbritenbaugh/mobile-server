//
//  OCGSplitViewController.h
//  SplitView
//
//  Created by John Scott on 28/11/2013.
//  Copyright (c) 2013 John Scott. All rights reserved.
//

#import <UIKit/UIKit.h>

/** @file OCGSplitViewController.h */

/** @enum OCGSplitViewSideDisplayMode 
 *  @brief The split view display mode. Controls where the side view is displayed.
 */
typedef NS_ENUM(NSInteger, OCGSplitViewSideDisplayMode) {
    /** @brief Side view is displayed below the main view.
     */
    OCGSplitViewSideDisplayModeBottom = 0,
    /** @brief Side view is displayed above the main view.
     */
    OCGSplitViewSideDisplayModeTop = 2
};

/** @brief Container view controller for hosting a main view and
 a "side" view.
 */
@interface OCGSplitViewController : UIViewController

/** @property displayMode
 *  @brief The display mode of the split view controller.
 */
@property (nonatomic, assign) OCGSplitViewSideDisplayMode displayMode;

/** @property mainViewWrapper
 *  @brief The container for the main view.
 */
@property (readonly, nonatomic) UIView *mainViewWrapper;

/** @property mainViewController
 *  @brief The view controller that takes up the majority of the split view.
 */
@property (strong, nonatomic) UIViewController *mainViewController;

/** @property topSideViewController
 *  @brief The view controller that is currently being displayed as the side view.
 */
@property (readonly, nonatomic) UIViewController *topSideViewController;

/** @brief Initialiser - creates an instance with a main view controller.
 *  @param mainViewController The view controller contained in the main body of the split view.
 *  @see mainViewController
 *  @return An instance of OCGSplitViewController.
 */
- (id) initWithMainViewController:(UIViewController *)mainViewController;

/** @brief Pushes a "side" view controller onto the stack. The view controller is displayed immediately.
 *  @param sideViewController The view controller to push onto the stack. This then becomes the new side view controller.
 */
- (void) pushSideViewController:(UIViewController *)sideViewController;

/** @brief Pops a "side" view controller off the stack. The previous view controller then becomes the side view controller. The view controller is displayed immediately.
 */
- (void) popSideViewController;

/** @brief Replaces the current view controller stack with a new view controller stack. The view controller is displayed immediately.
 *  @param viewControllers The new view controller stack.
 *  @note This will completely replace the current stack.
 */
- (void) setSideViewControllers:(NSArray *)viewControllers;

@end

/** @brief UIViewController category for view controllers hosted in the OCGSplitViewController.
 *  Hosted view controllers don't have to import this category, but it helps!
 */
@interface UIViewController (OCGSplitViewController)

/** @property splitViewController
 *  @brief The split view controller that hosts this view controller.
 */
@property (weak, nonatomic) OCGSplitViewController *splitViewController;

@end