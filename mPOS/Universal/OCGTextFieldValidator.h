//
//  OCGTextFieldValidator.h
//  mPOS
//
//  Created by John Scott on 09/04/2013.
//  Copyright (c) 2013 Clarity. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface OCGTextFieldValidator : NSObject <UITextFieldDelegate>


/**
 Returns whether the given string is valid within the rules of the validator
 @param string to be validated
 @return A boolean representing the validity of the given string
 */

- (BOOL) isStringValid:(NSString *)string;

/**
 Transforms a given string into a more correct string.
 
 For example, the default OCGNumberTextFieldValidator object will transform "1.234" to "12.34"
 
 @param string to be transformed
 @return the transformed string
 */

-(NSString*)stringFromString:(NSString*)string;

@end
