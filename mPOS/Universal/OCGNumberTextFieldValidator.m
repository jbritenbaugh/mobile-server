//
//  OCGNumberTextFieldValidator.m
//  mPOS
//
//  Created by John Scott on 09/04/2013.
//  Copyright (c) 2013 Clarity. All rights reserved.
//

#import "OCGNumberTextFieldValidator.h"

@implementation OCGNumberTextFieldValidator
{
    NSNumberFormatter *_numberFormatter;
}

- (instancetype)init
{
    self = [super init];
    if (self)
    {
        _numberFormatter = [[NSNumberFormatter alloc] init];
    }
    return self;
}

-(void)setStyle:(NSNumberFormatterStyle)style
{
    _numberFormatter.numberStyle = style;
    _style = style;
}

-(void)setCurrencyCode:(NSString *)currencyCode
{
    _numberFormatter = [[NSNumberFormatter alloc] init];
    _numberFormatter.currencyCode = currencyCode;
    _numberFormatter.numberStyle = NSNumberFormatterCurrencyStyle;
    _style = NSNumberFormatterCurrencyStyle;
   _currencyCode = currencyCode;
}

-(void)setMaxValue:(double)maxValue
{
    _numberFormatter.maximum = @(maxValue);
    _maxValue = maxValue;
}

-(void)setMinValue:(double)minValue
{
    _numberFormatter.minimum = @(minValue);
    _minValue = minValue;
}

-(void)setScale:(NSInteger)scale
{
    _numberFormatter.minimumFractionDigits = scale;
    _numberFormatter.numberStyle = NSNumberFormatterDecimalStyle;
    _scale = scale;
}


- (BOOL)isStringValid:(NSString *)string
{
    NSNumber *number = [_numberFormatter numberFromString:string];
    
    return number != nil;
}

-(NSString*)stringFromString:(NSString*)string
{
    if ([string length] > 0)
    {
        string = [_numberFormatter stringFromNumber:[self numberFromString:string]];
    }
    return string;
}

-(NSNumber*)numberFromString:(NSString *)string
{
    return [self decimalNumberFromString:string];
}

-(NSDecimalNumber*)decimalNumberFromString:(NSString *)string
{
    string = [[string componentsSeparatedByCharactersInSet:[[NSCharacterSet decimalDigitCharacterSet] invertedSet]] componentsJoinedByString:@""];
    
    NSDecimalNumber *number = [[NSDecimalNumber alloc] initWithString:string];
    
    if ([[NSDecimalNumber notANumber] isEqual:number])
    {
        return nil;
    }
    
    number = [number decimalNumberByMultiplyingByPowerOf10:-_numberFormatter.minimumFractionDigits];
    
    return number;
}

-(NSString*)stringFromNumber:(NSNumber*)number
{
    NSString *string = nil;
    if (number != nil)
    {
        string = [_numberFormatter stringFromNumber:number];
    }
    return string;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSString *text = [[textField.text componentsSeparatedByCharactersInSet:[[NSCharacterSet decimalDigitCharacterSet] invertedSet]] componentsJoinedByString:@""];

    if (range.length > 0)
    {
        if (range.length > text.length)
        {
            range.length = text.length;
        }
        range.location = [text length] - range.length;
        text = [text stringByReplacingCharactersInRange:range withString:@""];
    }
    else
    {
        string = [[string componentsSeparatedByCharactersInSet:[[NSCharacterSet decimalDigitCharacterSet] invertedSet]] componentsJoinedByString:@""];
        text = [text stringByAppendingString:string];
    }
    
    if ([text length] == 0)
    {
        text = @"0";
    }
    
    NSDecimalNumber *number = [[NSDecimalNumber alloc] initWithString:text];
    short power = _numberFormatter.minimumFractionDigits;
    if (self.style == NSNumberFormatterPercentStyle)
    {
        power += 2;
    }
    number = [number decimalNumberByMultiplyingByPowerOf10:-power];
    if ((self.maxValue > 0) && (number.doubleValue > self.maxValue)) {
        number = [[NSDecimalNumber alloc] initWithDouble:self.maxValue];
    }
    text = [_numberFormatter stringFromNumber:number];
    
    textField.text = text;
    
    /*
     Since we've changed the text ourselves, we have to return NO from this method. However, this means that
     alertViewShouldEnableFirstOtherButton: is not called (returning YES here causes a UIControlEventEditingChanged
     event). So we need to send one ourselves.
     */
    
    [textField sendActionsForControlEvents:UIControlEventEditingChanged];
    return NO;
}


@end
