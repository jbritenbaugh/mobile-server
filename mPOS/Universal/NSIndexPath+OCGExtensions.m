//
//  NSIndexPath+OCGExtensions.m
//  mPOS
//
//  Created by John Scott on 08/08/2014.
//  Copyright (c) 2014 Clarity. All rights reserved.
//

#import "NSIndexPath+OCGExtensions.h"

@implementation NSIndexPath (OCGExtensions)

-(NSString*)OCGExtensions_description
{
    NSMutableString *string = [NSMutableString string];
    NSUInteger length = self.length;
    NSUInteger indexes[length];
    [self getIndexes:indexes];
    
    for (NSInteger position = 0; position < length; position++)
    {
        if (position)
        {
//            [string appendString:@"‣"];
            [string appendString:@"~"];
        }
        
        [string appendFormat:@"%d", indexes[position]];
    }
    return string;
}

-(NSUInteger)OCGExtensions_lastIndexAtPosition
{
    return [self indexAtPosition:self.length-1];
}

@end
