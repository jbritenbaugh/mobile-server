//
//  OCGInputView.m
//  mPOS
//
//  Created by John Scott on 25/02/2015.
//  Copyright (c) 2015 Clarity. All rights reserved.
//

#import "OCGInputAccessoryView.h"

#define CONTENTVIEW_MARGIN (0)

#define DEBUG_CONTENTVIEW_LAYOUT 0

@interface _OCGContentViewMargin : UIView

@property (nonatomic, weak) OCGInputAccessoryView* parentInputAccessoryView;

@end

@implementation OCGInputAccessoryView
{
    UIToolbar *_toolbar;
    UIBarButtonItem *_contentViewLeftMarginItem;
    UIBarButtonItem *_contentViewWidthItem;
    UIBarButtonItem *_contentViewRightMarginItem;
    UIView *_contentView;
}

-(instancetype)init
{
    self = [super initWithFrame:CGRectMake(0, 0, 200, 44) inputViewStyle:UIInputViewStyleKeyboard];
    if (self)
    {
        _contentView =
        [[UIView alloc] initWithFrame:self.bounds];
        [self addSubview:_contentView];
        
#if DEBUG_CONTENTVIEW_LAYOUT
        _contentView.backgroundColor = [UIColor blueColor];
#endif
    }
    return self;
}

-(instancetype)initWithFrame:(CGRect)frame inputViewStyle:(UIInputViewStyle)inputViewStyle
{
    NSAssert(0, @"Use init");
    return nil;
}

-(void)setLeftButtons:(NSArray *)leftButtons
{
    _leftButtons = leftButtons;
    [self resetToolbarContent];
}

-(void)setRightButtons:(NSArray *)rightButtons
{
    _rightButtons = rightButtons;
    [self resetToolbarContent];
}

-(void)resetToolbarContent
{
    if (_toolbar)
    {
        UIColor *barItemColor = nil;
        UIColor *barColor = nil;
        
        id nextResponder = [self nextResponder];
        
        if ([nextResponder respondsToSelector:@selector(keyboardAppearance)])
        {
            if (UIDevice.currentDevice.systemVersion.floatValue >= 7.0 && [nextResponder keyboardAppearance] == UIKeyboardAppearanceDark)
            {
                barColor = [UIColor colorWithWhite:0.35 alpha:1.0];
                barItemColor = [UIColor colorWithWhite:0.85 alpha:1.0];
            }
            else
            {
                barColor = [UIColor colorWithRed:0.82 green:0.83 blue:0.861 alpha:1.000];
                barItemColor = [UIColor colorWithWhite:0.20 alpha:1.0];
            }
            
            _toolbar.tintColor = barItemColor;
            _toolbar.barTintColor = barColor;
        }
        
        NSMutableArray *items = [NSMutableArray array];
        
        if (_leftButtons)
        {
            [items addObjectsFromArray:_leftButtons];
        }
        
        [items addObject:_contentViewLeftMarginItem];
        [items addObject:_contentViewWidthItem];
        [items addObject:_contentViewRightMarginItem];
        
        if (_rightButtons)
        {
            [items addObjectsFromArray:_rightButtons];
        }
        
        _toolbar.items = items;
    }
}

-(void)willMoveToSuperview:(UIView *)newSuperview
{
    if (newSuperview)
    {
        _OCGContentViewMargin *contentViewLeftMarginView =
        [[_OCGContentViewMargin alloc] initWithFrame:CGRectMake(0, 0, CONTENTVIEW_MARGIN, 44)];
        contentViewLeftMarginView.parentInputAccessoryView = self;
        
        _contentViewLeftMarginItem =
        [[UIBarButtonItem alloc] initWithCustomView:contentViewLeftMarginView];

#if DEBUG_CONTENTVIEW_LAYOUT
        contentViewLeftMarginView.backgroundColor = [UIColor greenColor];
#endif
        
        _contentViewWidthItem =
        [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace
                                                      target:nil
                                                      action:NULL];
        
        
        _OCGContentViewMargin *contentViewRightMarginView =
        [[_OCGContentViewMargin alloc] initWithFrame:CGRectMake(0, 0, CONTENTVIEW_MARGIN, 44)];
        contentViewRightMarginView.parentInputAccessoryView = self;
        
        _contentViewRightMarginItem =
        [[UIBarButtonItem alloc] initWithCustomView:contentViewRightMarginView];
        
#if DEBUG_CONTENTVIEW_LAYOUT
        contentViewRightMarginView.backgroundColor = [UIColor redColor];
#endif
        
        _toolbar = [[UIToolbar alloc] initWithFrame:self.bounds];
        
        _toolbar.autoresizingMask = UIViewAutoresizingFlexibleWidth;
        [_toolbar setBackgroundImage:[UIImage new]
                  forToolbarPosition:UIBarPositionAny
                          barMetrics:UIBarMetricsDefault];
        [_toolbar setShadowImage:[UIImage new]
              forToolbarPosition:UIToolbarPositionAny];
        
        /*
         Set up the toolbar colors to match the keyboard
         */
        
        [self addSubview:_toolbar];
        
        [self resetToolbarContent];
    }
    else
    {
        [_toolbar removeFromSuperview];
        _toolbar = nil;
    }
}

-(void)layoutContentView
{
    CGRect contentViewLeftMarginFrame =
    [self convertRect:_contentViewLeftMarginItem.customView.frame
             fromView:_contentViewLeftMarginItem.customView.superview];
    
    CGRect contentViewRightMarginFrame =
    [self convertRect:_contentViewRightMarginItem.customView.frame
             fromView:_contentViewRightMarginItem.customView.superview];
    
#if DEBUG_CONTENTVIEW_LAYOUT
    NSLog(@"Content width: %f - %f", CGRectGetMaxX(contentViewLeftMarginFrame), CGRectGetMinX(contentViewRightMarginFrame));
#endif
    
    CGRect contentFrame = _contentView.frame;
    contentFrame.origin.x = CGRectGetMaxX(contentViewLeftMarginFrame);
    contentFrame.size.width = CGRectGetMinX(contentViewRightMarginFrame) - CGRectGetMaxX(contentViewLeftMarginFrame);
    _contentView.frame = contentFrame;
}

-(void)layoutSubviews
{
    [super layoutSubviews];
    [self layoutContentView];
}

@end

@implementation _OCGContentViewMargin

-(void)setFrame:(CGRect)frame
{
    [super setFrame:frame];
    [_parentInputAccessoryView layoutContentView];
}

@end
