//
//  OCGSplitViewController.m
//  SplitView
//
//  Created by John Scott on 28/11/2013.
//  Copyright (c) 2013 John Scott. All rights reserved.
//

#import "OCGSplitViewController.h"
#import <objc/runtime.h>
#import <QuartzCore/QuartzCore.h>

@interface OCGSplitViewController ()
- (void) setSideViewController:(UIViewController *)sideViewController;
@end

@implementation OCGSplitViewController
{
    UIViewController *_mainViewController;
    BOOL _hasAddedMainViewControllerView;
    
    UIViewController *_sideViewController;
    NSMutableArray *_sideViewControllers;
    BOOL _hasAddedSideViewControllerView;
    UIView *_sideViewWrapper;
    
    UIView *_seperatorView;
    
    NSLayoutConstraint *_sideViewWrapperConstraint;
    NSLayoutConstraint *_seperatorViewConstraint;
}

@synthesize mainViewWrapper = _mainViewWrapper;
@synthesize topSideViewController;
- (UIViewController *) topSideViewController
{
    return _sideViewController;
}

- (id) init
{
    self = [super init];
    if (self) {
        _sideViewControllers = [NSMutableArray array];
        // default to bottom display mode
        self.displayMode = OCGSplitViewSideDisplayModeBottom;
    }
    
    return self;
}

- (id)initWithMainViewController:(UIViewController *)mainViewController
{
    self = [self init];
    if (self)
    {
        _mainViewController = mainViewController;
    }
    return self;
}

- (void) dealloc
{
    if (_sideViewController != nil) {
        [_sideViewController removeObserver:self forKeyPath:@"preferredContentSize"];
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    //self.view.backgroundColor = [UIColor yellowColor];
    
    _mainViewWrapper = [[UIView alloc] init];
    _mainViewWrapper.translatesAutoresizingMaskIntoConstraints = NO;
//    _mainViewWrapper.backgroundColor = [UIColor blueColor];
    [self.view addSubview:_mainViewWrapper];
    
    _sideViewWrapper = [[UIView alloc] init];
    _sideViewWrapper.translatesAutoresizingMaskIntoConstraints = NO;
    _sideViewWrapper.clipsToBounds = YES;
//    _sideViewWrapper.backgroundColor = [UIColor greenColor];
    [self.view addSubview:_sideViewWrapper];
    
    _seperatorView = [[UIView alloc] init];
    _seperatorView.translatesAutoresizingMaskIntoConstraints = NO;
    _seperatorView.backgroundColor = [UIColor colorWithRed:0.74
                                                     green:0.74
                                                      blue:0.74
                                                     alpha:1.0];
    
    // setup constrains based on display mode
    switch (self.displayMode) {
        case OCGSplitViewSideDisplayModeBottom: {
            [self.mainViewWrapper constrain:@"top = top" to:self.view];
            [self.mainViewWrapper constrain:@"left = left" to:self.view];
            [self.mainViewWrapper constrain:@"right = right" to:self.view];
//            [self.mainViewWrapper constrain:@"bottom = top" to:_sideViewWrapper];
            // iOS 8 seems to introduce some kind of additional layout constraint that
            // conflicts with ours, so lower the priority on ours
            NSLayoutConstraint *bottomConstraint =
            [NSLayoutConstraint constraintWithItem:self.mainViewWrapper
                                         attribute:NSLayoutAttributeBottom
                                         relatedBy:NSLayoutRelationEqual
                                            toItem:_sideViewWrapper
                                         attribute:NSLayoutAttributeTop
                                        multiplier:1.0f
                                          constant:0.0f];
            bottomConstraint.priority = 999;
            [self.view addConstraint:bottomConstraint];
            
            [_sideViewWrapper constrain:@"left = left" to:self.view];
            [_sideViewWrapper constrain:@"right = right" to:self.view];
            [_sideViewWrapper constrain:@"bottom = bottom" to:self.view];
            _sideViewWrapperConstraint = [_sideViewWrapper constrain:@"height = 0" to:nil];
            break;
        }
        case OCGSplitViewSideDisplayModeTop: {
            [_sideViewWrapper constrain:@"top = top" to:self.view];
            [_sideViewWrapper constrain:@"left = left" to:self.view];
            [_sideViewWrapper constrain:@"right = right" to:self.view];
            _sideViewWrapperConstraint = [_sideViewWrapper constrain:@"height = 0" to:nil];
            [self.mainViewWrapper constrain:@"top = bottom" to:_sideViewWrapper];
            [self.mainViewWrapper constrain:@"left = left" to:self.view];
            [self.mainViewWrapper constrain:@"right = right" to:self.view];
            [self.mainViewWrapper constrain:@"bottom = bottom" to:self.view];
            break;
        }
        default:
            break;
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated
{
    if (!_hasAddedMainViewControllerView && _mainViewController) {
        [self setupMainViewController];
    }
    
    if (!_hasAddedSideViewControllerView && _sideViewController) {
        // adjust the constraints
        _sideViewWrapperConstraint.constant = _sideViewController.preferredContentSize.height;
        if (_sideViewController != nil) {
            // show the new side view controller
            [self showSideViewController];
        }
    }
    
    [super viewWillAppear:animated];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (BOOL) disablesAutomaticKeyboardDismissal
{
    return NO;
}

-(void)setMainViewController:(UIViewController *)mainViewController
{
    if (_hasAddedMainViewControllerView)
    {
        [_mainViewController willMoveToParentViewController:nil];
        _mainViewController.splitViewController = nil;
        [self.mainViewWrapper removeConstraints:self.mainViewWrapper.constraints];
        [_mainViewController.view removeFromSuperview];
        [_mainViewController removeFromParentViewController];
        _hasAddedMainViewControllerView = NO;
    }
    _mainViewController = mainViewController;
    if (self.view.window != nil && _mainViewController)
    {
        [self setupMainViewController];
    }
}

-(void)setupMainViewController
{
    [self addChildViewController:_mainViewController];
    _mainViewController.splitViewController = self;
    [self.mainViewWrapper addSubview:_mainViewController.view];
    if (_mainViewController.view.translatesAutoresizingMaskIntoConstraints)
    {
        _mainViewController.view.autoresizesSubviews = YES;
        _mainViewController.view.frame = self.mainViewWrapper.bounds;
        _mainViewController.view.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    }
    else
    {
        [_mainViewController.view constrain:@"top = top" to:self.mainViewWrapper];
        [_mainViewController.view constrain:@"left = left" to:self.mainViewWrapper];
        [_mainViewController.view constrain:@"right = right" to:self.mainViewWrapper];
        [_mainViewController.view constrain:@"bottom = bottom" to:self.mainViewWrapper];
    }
    [_mainViewController didMoveToParentViewController:self];
    _hasAddedMainViewControllerView = YES;
}

-(void)setSideViewController:(UIViewController *)sideViewController
{
    if (_hasAddedSideViewControllerView) {
        // remove the existing side view controller
        _hasAddedSideViewControllerView = NO;
        [_sideViewWrapper removeConstraints:_sideViewWrapper.constraints];
        // remove the actual side view controller
        if (_sideViewController != nil) {
            [_sideViewController removeObserver:self forKeyPath:@"preferredContentSize"];
            [_sideViewController willMoveToParentViewController:nil];
            _sideViewController.splitViewController = nil;
            [_sideViewController.view removeFromSuperview];
            [_sideViewController removeFromParentViewController];
        }
        _sideViewController = nil;
    }
    // add the new side view
    _sideViewController = sideViewController;
    if (self.view.window != nil && _sideViewController) {
        // adjust the constraints
        _sideViewWrapperConstraint.constant = _sideViewController.preferredContentSize.height;
        if (_sideViewController != nil) {
            // show the new side view controller
            [self showSideViewController];
        }
    }
}

- (void) showSideViewController
{
    // setup the side view controller
    [self addChildViewController:_sideViewController];
    _sideViewController.splitViewController = self;
    [_sideViewWrapper addSubview: _sideViewController.view];
    if (_sideViewController.view.translatesAutoresizingMaskIntoConstraints) {
        _sideViewController.view.autoresizesSubviews = YES;
        _sideViewController.view.frame = _sideViewWrapper.bounds;
        _sideViewController.view.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    } else {
        [_sideViewController.view constrain:@"top = top" to:_sideViewWrapper];
        [_sideViewController.view constrain:@"left = left" to:_sideViewWrapper];
        [_sideViewController.view constrain:@"right = right" to:_sideViewWrapper];
        [_sideViewController.view constrain:@"bottom = bottom" to:_sideViewWrapper];
    }
    [_sideViewController didMoveToParentViewController:self];
    [_sideViewController addObserver:self
                          forKeyPath:@"preferredContentSize"
                             options:NSKeyValueObservingOptionNew
                             context:NULL];
    _hasAddedSideViewControllerView = YES;
}

- (void)observeValueForKeyPath:(NSString *)keyPath
                      ofObject:(id)object
                        change:(NSDictionary *)change
                       context:(void *)context
{
    if ([keyPath isEqualToString:@"preferredContentSize"]) {
        CGSize _sideViewPreferredContentSize = [change[NSKeyValueChangeNewKey] CGSizeValue];
        if (_sideViewWrapperConstraint.constant != _sideViewPreferredContentSize.height) {
            _sideViewWrapperConstraint.constant = _sideViewPreferredContentSize.height;
        }
    }
}

- (void) pushSideViewController:(UIViewController *)sideViewController
{
    if (sideViewController) {
        [_sideViewControllers addObject:sideViewController];
        [self setSideViewController:[_sideViewControllers lastObject]];
    }
}

- (void) popSideViewController
{
    [_sideViewControllers removeLastObject];
    [self setSideViewController:[_sideViewControllers lastObject]];
}

- (void) setSideViewControllers:(NSArray *)viewControllers
{
    _sideViewControllers = [viewControllers mutableCopy];
    [self setSideViewController:[_sideViewControllers lastObject]];
}

@end

@implementation UIViewController (OCGSplitViewController)

static char splitControllerKey;

- (void)setSplitViewController:(OCGSplitViewController *)splitViewController
{
    objc_setAssociatedObject(self, &splitControllerKey, splitViewController, OBJC_ASSOCIATION_ASSIGN);
}

-(OCGSplitViewController*)splitViewController
{
    
    OCGSplitViewController *splitViewController = objc_getAssociatedObject(self, &splitControllerKey);
    
    if (!splitViewController)
    {
        splitViewController = self.parentViewController.splitViewController;
    }
    
    return splitViewController;
}

static char preferredContentSizeKey;


- (CGSize)preferredContentSize
{
    return [objc_getAssociatedObject(self, &preferredContentSizeKey) CGSizeValue];

}

-(void)setPreferredContentSize:(CGSize)preferredContentSize
{
    objc_setAssociatedObject(self, &preferredContentSizeKey, [NSValue valueWithCGSize:preferredContentSize], OBJC_ASSOCIATION_RETAIN);
}


@end
