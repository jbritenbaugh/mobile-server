//
//  FNKCollectionViewLayout.m
//  Frank
//
//  Created by John Scott on 11/07/2014.
//  Copyright (c) 2014 Omnico Group. All rights reserved.
//

#import "FNKCollectionViewLayout.h"

#import "FNKSupplementaryView.h"

#import "FNKCollectionViewLayoutAttributes.h"

#import "UICollectionView+OCGExtensions.h"
#import "NSArray+OCGIndexPath.h"
#import "NSArray+OCGExtensions.h"
#import "NSIndexPath+OCGExtensions.h"
#import "CGGeometry+OCGExtensions.h"

#import "FNKCollectionView.h"

#import "ClientSettings+MPOSExtensions.h"

#define DROPDOWN_BUTTON_WIDTH (80)
#define DEFAULT_HEIGHT (44)
#define SUMMARY_NODE_LEFT_SPLIT (44)
#define SUMMARY_NODE_RIGHT_SPLIT (80)
#define SUMMARY_NODE_MIDDLE_SPLIT (22)
#define DEFAULT_NON_SUMMARY_SQUASH (10)

@interface FNKNode : NSObject

@property (nonatomic, weak) FNKNode *parent;

@property (nonatomic, strong) FNKSupplementaryItem *headerItem;

@property (nonatomic, readonly) NSMutableArray *subnodes;

@property (nonatomic, strong) FNKSupplementaryItem *footerItem;

@property (nonatomic, readonly) NSIndexPath *indexPath;

-(void)setValue:(BOOL)value forOption:(FNKSupplementaryOptions)option recurse:(BOOL)recurse;
-(BOOL)valueForOption:(FNKSupplementaryOptions)option;

@end

@implementation FNKNode

-(id)init
{
    self = [super init];
    if (self)
    {
        _subnodes = [NSMutableArray array];
    }
    return self;
}

-(NSIndexPath *)indexPath
{
    if (_parent)
    {
        return [_parent.indexPath indexPathByAddingIndex:[_parent.subnodes indexOfObject:self]];
    }
    else
    {
        return [NSIndexPath new];
    }
}

-(NSArray*)subnodesWithSupplementaryOption:(FNKSupplementaryOptions)supplementaryOption
{
    NSMutableArray *matchingSubnodes = [NSMutableArray array];
    
    for (NSInteger subnodeIndex=0; subnodeIndex < _subnodes.count; subnodeIndex++)
    {
        FNKNode *subnode = _subnodes[subnodeIndex];
        if ([subnode isKindOfClass:[FNKNode class]] && subnode.headerItem.options & supplementaryOption)
        {
            [matchingSubnodes addObject:subnode];
        }
    }
    return matchingSubnodes;
}

-(void)setValue:(BOOL)value forOption:(FNKSupplementaryOptions)option recurse:(BOOL)recurse
{
    _headerItem.options &= ~option;
    if (value)
    {
        _headerItem.options |= option;
    }
    if (recurse)
    {
        for (FNKNode *subnode in _subnodes)
        {
            if ([subnode isKindOfClass:[FNKNode class]])
            {
                [subnode setValue:value forOption:option recurse:recurse];
            }
        }
    }
}

-(BOOL)valueForOption:(FNKSupplementaryOptions)option
{
    return (_headerItem.options & option);
}

#if DEBUG
/*
 See http://stackoverflow.com/a/23936634/542244. isNSDictionary__ is set here to ensire that NSDictionary
 and NSArray call descriptionWithLocale:indent:
 
 THIS IS A HACK and must not be part of a release build.
 */

- (BOOL) isNSArray__
{
	return YES;
}

- (NSString *) description
{
    return [self descriptionWithLocale:nil indent:0];
}

- (NSString *)descriptionWithLocale:(id)locale indent:(NSUInteger)level
{
    NSString *description = [NSString stringWithFormat:@"%@ %@", [self.indexPath OCGExtensions_description], _headerItem];
    
    
    if (_subnodes.count)
    {
        return [[description stringByAppendingString:@" : "] stringByAppendingString:[_subnodes descriptionWithLocale:locale indent:level+1]];
    }
    else
    {
        return description;
    }
    
}
#endif

@end

@implementation FNKCollectionViewLayout
{
    CGSize _blockSize;
    CGSize _padding;
    //    CGFloat _sectionPadding;
    NSMutableDictionary *_itemAttributes;
    NSMutableDictionary *_headerAttributes;
    NSMutableDictionary *_separatorAttributes;
    NSMutableDictionary *_footerAttributes;
    CGFloat _maxY;
    NSUInteger _numberOfColumns;
    CGFloat _columnWidth;
    NSUInteger _decorationIndex;
    NSMutableArray *_pinnedAttributes;
}

+(Class)layoutAttributesClass
{
    return [FNKCollectionViewLayoutAttributes class];
}

-(instancetype)init
{
    self = [super init];
    if (self)
    {
        _blockSize = CGSizeMake(320, 44.0);
        //        _sectionPadding = -1.;
        
        [self registerClass:[FNKSupplementaryView class] forDecorationViewOfKind:FNKSupplementaryKindSectionHeader];
        [self registerClass:[FNKSupplementaryView class] forDecorationViewOfKind:FNKSupplementaryKindSectionTab];
        [self registerClass:[FNKSupplementaryView class] forDecorationViewOfKind:FNKSupplementaryKindCell];
        [self registerClass:[FNKSupplementaryView class] forDecorationViewOfKind:FNKSupplementaryKindSectionFooter];
        [self registerClass:[FNKSupplementaryView class] forDecorationViewOfKind:FNKSupplementaryKindSectionSeperator];
    }
    return self;
}

-(void)setSelectedNodePath:(NSIndexPath *)selectedNodePath
{
//    NSLog(@"selected: %@", [selectedNodePath OCGExtensions_description]);
    
    //    if (_numberOfColumns > 1)
    {
        if ([selectedNodePath isEqual:_selectedNodePath])
        {
            selectedNodePath = nil;
        }
        
        if (selectedNodePath != _selectedNodePath && ![selectedNodePath isEqual:_selectedNodePath])
        {
            [self invalidateLayout];
        }
        
        _selectedNodePath = selectedNodePath;
        
    }
}

- (void)invalidateLayoutWithContext:(UICollectionViewLayoutInvalidationContext *)context
{
    if (context.invalidateDataSourceCounts)
    {
        _selectedNodePath = nil;
    }
    [super invalidateLayoutWithContext:context];

}

- (CGSize)collectionViewContentSize
{
	CGSize contentSize = self.collectionView.frame.size;
	if (self.collectionView.numberOfSections)
    {
        contentSize.height = MAX(_maxY, CGRectGetMaxY(self.collectionView.frame));
    }
    else
    {
		contentSize = CGSizeZero;
	}
	return contentSize;
}

#pragma mark - Methods to Override

- (void)prepareLayout
{
	[super prepareLayout];
    
    [self layoutEverything];
}

- (void)layoutEverything
{
    [FNKCollectionView ensureThisKindOfClass:self.collectionView];
    
    _numberOfColumns=1;
    _columnWidth = 0;
    
    _padding = CGSizeMake(5, 5);
    
    if (BasketLayoutGrid == BasketController.sharedInstance.clientSettings.parsedBasketLayout) {
        CGFloat frameWidth = CGRectGetWidth(self.collectionView.frame) -  _padding.width;
        for (; _numberOfColumns<15 ; _numberOfColumns++)
        {
            CGFloat itemWidth =  frameWidth / _numberOfColumns;
            
            if (itemWidth < _blockSize.width)
            {
                if (_columnWidth == 0)
                {
                    _columnWidth = itemWidth;
                }
                else
                {
                    _numberOfColumns--;
                }
                break;
            }
            
            _columnWidth = itemWidth;
        }
    } else {
        _numberOfColumns = 1;
        _columnWidth = 0.0f;
    }
    
    if (_numberOfColumns == 1)
    {
        _padding.width = 0;
        _columnWidth =  CGRectGetWidth(self.collectionView.frame);
    }
    
	NSInteger numberOfSections = self.collectionView.numberOfSections;
    
    _itemAttributes = [NSMutableDictionary dictionaryWithCapacity:numberOfSections*10];
    _headerAttributes = [NSMutableDictionary dictionaryWithCapacity:numberOfSections*10];
    _footerAttributes = [NSMutableDictionary dictionaryWithCapacity:numberOfSections*10];
    _separatorAttributes = [NSMutableDictionary dictionaryWithCapacity:numberOfSections*10];
    _pinnedAttributes = [NSMutableArray array];
    
    
    _maxY = 0;
    
    NSUInteger columnIndex = 0;
    
    NSInteger level = 0;
    NSUInteger hiddenLevel = 0;
    
    NSMutableArray *stack = [NSMutableArray array];
    
    FNKNode *root = [[FNKNode alloc] init];
    
    [stack addObject:root];
    
    for (NSInteger sectionIndex = 0; sectionIndex < numberOfSections; sectionIndex++)
    {
        NSInteger numberOfItemsInSection = [self.collectionView numberOfItemsInSection:sectionIndex];
        
        for (NSInteger itemIndex = 0; itemIndex < numberOfItemsInSection; itemIndex++)
        {
            NSIndexPath *indexPath = [NSIndexPath indexPathForItem:itemIndex inSection:sectionIndex];
            
            {
                NSArray *items = [self supplementaryItemsForHeadersAtIndexPath:indexPath];
                NSUInteger itemsCount = items.count;
                //                NSLog(@"> %@ : %@", IndexPathString(indexPath), [items componentsJoinedByString:@", "]);
                for (NSInteger itemIndex = 0; itemIndex < itemsCount; itemIndex++)
                {
                    FNKSupplementaryItem *item = items[itemIndex];
                    NSAssert([item isKindOfClass:[FNKSupplementaryItem class]], @"");
                    
                    
                    FNKNode *node = [[FNKNode alloc] init];
                    node.headerItem = item;
                    
                    FNKNode *parent = stack.lastObject;
                    
                    {
                        /*
                         All of this should be moved out into the data source or beyond
                         */
                        
                        //                        if (stack.count== 2)
                        //                        {
                        //                            if ([parent.parent.subnodes indexOfObject:parent] > 0)
                        //                            {
                        ////                                node.headerItem.title = @"Flibble";
                        //                                node.headerItem.options |= FNKSupplementaryOptionDropdown;
                        //                            }
                        ////                            else
                        ////                            {
                        ////                                node.headerItem.options |= FNKSupplementaryOptionTabbed;
                        ////                            }
                        //                        }
                    }
                    
                    [parent.subnodes addObject:node];
                    node.parent = parent;
                    
                    [stack addObject:node];
                }
            }
            
            {
                FNKNode *parent = stack.lastObject;
                [parent.subnodes addObject:indexPath];
            }
            
            {
                NSArray *items = [self supplementaryItemsForFootersAtIndexPath:indexPath];
                NSUInteger itemsCount = items.count;
                //                NSLog(@"< %@ : %@", IndexPathString(indexPath), [items componentsJoinedByString:@", "]);
                for (NSInteger itemIndex = 0; itemIndex < itemsCount; itemIndex++)
                {
                    FNKSupplementaryItem *item = items[itemIndex];
                    NSAssert([item isKindOfClass:[FNKSupplementaryItem class]], @"");
                    FNKNode *node = stack.lastObject;
                    
                    node.footerItem = item;
                    
                    [stack removeLastObject];
                }
            }
        }
    }
    
//    NSLog(@"root: %@", [self annotatedNodeForNode:root]);
    
    _maxY = [self layoutNode:root
                     originY:_maxY
                 columnIndex:0
                    isPinned:NO
                     isBoxed:NO
                   isSummary:NO];
    
    _maxY += _padding.height;
    
    if (_maxY < CGRectGetHeight(self.collectionView.frame))
    {
        _maxY = CGRectGetHeight(self.collectionView.frame);
    }
    //
    //    {
    //        /*
    //         This is an extra decoration view placed at the back of everything
    //         to allow users to reset the tabs back to all summaries.
    //         */
    //
    //        FNKCollectionViewLayoutAttributes *attributes =
    //            [FNKCollectionViewLayoutAttributes layoutAttributesForDecorationViewOfKind:FNKSupplementaryKindSectionHeader
    //                                                                         withIndexPath:[NSIndexPath indexPathWithIndex:_decorationIndex++]];
    //        CGFloat savedMaxY = _maxY;
    //        attributes.frame = CGRectMake(0, 0, CGRectGetWidth(self.collectionView.frame), _maxY);
    //        attributes.zIndex = -100;
    //        FNKSupplementaryItem *item = [[FNKSupplementaryItem alloc] init];
    //        item.title = @"";
    //        attributes.item = item;
    //
    //        _headerAttributes[attributes.indexPath] = attributes;
    //    }
    //    _maxY = savedMaxY;
}

-(id)annotatedNodeForNode:(id)node
{
    if ([node isKindOfClass:[NSIndexPath class]])
    {
        FNKLayoutOptions layoutOptions = [self layoutOptionsAtIndexPath:node];
        
        NSMutableArray *options = [NSMutableArray array];
        
        if (layoutOptions & FNKLayoutOptionDefaultCell)
        {
            [options addObject:@"Default"];
        }
        if (layoutOptions & FNKLayoutOptionEmptyCell)
        {
            [options addObject:@"Empty"];
        }
        
        if (!options.count)
        {
            [options addObject:@"None"];
        }
        
        
        
        return [NSString stringWithFormat:@"<%@ options: %@>", [(NSIndexPath*)node OCGExtensions_description], [options componentsJoinedByString:@", "]];
    }
    else if ([node isKindOfClass:[FNKNode class]])
    {
        
        FNKNode *annotatedNode = [[FNKNode alloc] init];
        annotatedNode.headerItem = [node headerItem];
        
        for (id subnode in [node subnodes])
        {
            id annotatedSubnode = [self annotatedNodeForNode:subnode];
            if ([annotatedSubnode isKindOfClass:[FNKNode class]])
            {
                [annotatedSubnode setParent:annotatedNode];
            }
            [annotatedNode.subnodes addObject:annotatedSubnode];
        }
        return annotatedNode;
    }
    else
    {
        return node;
    }
}

#define BOX(v) @ #v : @(v)

-(CGFloat)layoutNode:(FNKNode*)node
             originY:(CGFloat)originY
         columnIndex:(NSInteger)columnIndex
            isPinned:(BOOL)isPinned
             isBoxed:(BOOL)isBoxed
           isSummary:(BOOL)isSummary
{
    // header
    
    BOOL areSubnodesBoxed = NO;
    BOOL isTabbed = node.headerItem.options & FNKSupplementaryOptionTabbed;
    BOOL isDropdown = node.headerItem.options & FNKSupplementaryOptionDropdown;
    BOOL isLockedToDefaultSubnode = node.headerItem.options & FNKSupplementaryOptionLockToDefault || (isTabbed && node.indexPath.length > 2);
    BOOL isNodeSelected = [node.indexPath isEqual:_selectedNodePath];
    BOOL areSubnodesSummarised = isTabbed;
    BOOL canSquashSubnodes = (node.headerItem.options & FNKSupplementaryOptionDefault) && _numberOfColumns > 1;
    BOOL excludeDefaultSubNode = (_numberOfColumns == 1 && node.indexPath.length == 0);
    BOOL isLastSubnodePinnedToTheBottom = (node.indexPath.length == 0) && _pinLastSectionToBottom;
    
    if (node.indexPath.length == 2 && _numberOfColumns > 1)
    {
        areSubnodesSummarised = NO;
    }
    
    NSInteger defaultSubnodeIndex = -1;
    NSInteger selectedSubnodeIndex = -1;
    
    if (node.subnodes.count > 1)
    {
        for (NSInteger subnodeIndex=0; subnodeIndex < node.subnodes.count; subnodeIndex++)
        {
            id subnode = node.subnodes[subnodeIndex];
            if ([subnode isKindOfClass:[FNKNode class]])
            {
                if ([subnode headerItem].options & FNKSupplementaryOptionDefault)
                {
                    defaultSubnodeIndex = subnodeIndex;
                }
                
                if ([[subnode indexPath] isEqual:_selectedNodePath])
                {
                    selectedSubnodeIndex = subnodeIndex;
                }
            }
            else if ([subnode isKindOfClass:[NSIndexPath class]])
            {
                if ([self layoutOptionsAtIndexPath:subnode] & FNKLayoutOptionDefaultCell)
                {
                    defaultSubnodeIndex = subnodeIndex;
                }
            }
            
        }
    }
    else if (node.subnodes.count == 1)
    {
        excludeDefaultSubNode = NO;
        defaultSubnodeIndex = 0;
        selectedSubnodeIndex = 0;
    }
    else
    {
        defaultSubnodeIndex = -1;
        selectedSubnodeIndex = 0;
    }
    
    if (isLockedToDefaultSubnode || selectedSubnodeIndex < 0)
    {
        selectedSubnodeIndex = defaultSubnodeIndex;
    }
    
    // Display old style section headers
    
    if (node.indexPath.length == 1)
    {
        FNKSupplementaryItem *supplementaryItem = node.headerItem;
        
        if (supplementaryItem.title.length)
        {
            CGRect frame = [self defaultRectForOriginY:originY
                                           columnIndex:columnIndex];
            frame.size.height = [FNKSupplementaryView heightForText:supplementaryItem.title width:CGRectGetWidth(frame)];
            
            originY = [self layoutSupplementaryItem:supplementaryItem
                                               kind:FNKSupplementaryKindSectionHeader
                                               rect:frame
                                           isPinned:isPinned
                                         attributes:NULL];
        }
    }
    
    if (_numberOfColumns > 1 && node.indexPath.length == 1)
    {
        areSubnodesBoxed = YES;
    }
    else if (_numberOfColumns == 1 && node.indexPath.length == 0)
    {
        areSubnodesBoxed = YES;
    }
    
    
    CGFloat minSubViewOriginY = originY;
    CGFloat maxSubViewOriginY = originY;
    
    CGFloat leftIntraSubnodeSeparatorIndent = 60;
    CGFloat rightIntraSubnodeSeparatorIndent = 60;
    
    if (_numberOfColumns == 1)
    {
        rightIntraSubnodeSeparatorIndent = 0;
    }
    
//    NSLog(@"layoutNode %@\n%@", [node.indexPath OCGExtensions_description],
//          @{BOX(areSubnodesBoxed),
//            BOX(isTabbed),
//            BOX(isDropdown),
//            BOX(isLockedToDefaultSubnode),
//            BOX(isNodeSelected),
//            BOX(areSubnodesSummarised),
//            BOX(canSquashSubnodes),
//            BOX(excludeDefaultSubNode),
//            BOX(isLastSubnodePinnedToTheBottom),
//            BOX(isSummary),
//            BOX(areSubnodesBoxed),
//            BOX(selectedSubnodeIndex),
//            BOX(defaultSubnodeIndex),
//            });
    
    NSInteger numberOfSubnodesDrawn = 0;
    CGFloat heightOfFirstSubnode = 0;
    for (NSInteger subnodeIndex=0; subnodeIndex < node.subnodes.count; subnodeIndex++)
    {
        id subnode = node.subnodes[subnodeIndex];
        
        BOOL isLastSubNode = (subnodeIndex + 1) == node.subnodes.count;
        
        BOOL isSubNodePinnedToTheBottom = isPinned || (isLastSubnodePinnedToTheBottom && isLastSubNode);
        
//        NSLog(@"layout sub node %@~%d\n%@", [node.indexPath OCGExtensions_description],subnodeIndex,
//                @{BOX(isLockedToDefaultSubnode)
//                  BOX(excludeDefaultSubNode)
//                  });

        
        if ((isLockedToDefaultSubnode || isTabbed) && selectedSubnodeIndex != subnodeIndex)
        {
            continue;
        }
        
        if (isDropdown && !isNodeSelected && defaultSubnodeIndex != subnodeIndex)
        {
            continue;
        }
        
        if (excludeDefaultSubNode && defaultSubnodeIndex == subnodeIndex)
        {
            continue;
        }
        
        NSInteger subViewColumnIndex = columnIndex;
        
        if (node.indexPath.length == 1)
        {
            subViewColumnIndex = numberOfSubnodesDrawn % _numberOfColumns;
            
            if (0 == subViewColumnIndex)
            {
                minSubViewOriginY = maxSubViewOriginY;
                
                if (!isBoxed)
                {
                    minSubViewOriginY += _padding.height;
                }
            }
        }
        
        if (_numberOfColumns == 1 && areSubnodesBoxed)
        {
            minSubViewOriginY += _padding.height;
        }
        
        CGFloat subViewOriginY = minSubViewOriginY;
        BOOL canUpdateMinSubViewOriginY = YES;
        
        if ([subnode isKindOfClass:[FNKNode class]])
        {
            subViewOriginY = [self layoutNode:subnode
                                      originY:subViewOriginY
                                  columnIndex:subViewColumnIndex
                                     isPinned:isSubNodePinnedToTheBottom
                                      isBoxed:areSubnodesBoxed
                                    isSummary:areSubnodesSummarised];
            // determine if this node is the last node of a group of summary cells
            // where each summary cell has more than 4 display keys
            if (areSubnodesSummarised && isLastSubNode && (((FNKNode *)subnode).subnodes.count > 4)) {
                // check if this node is the last node in the parent's subnode
                if ([node.parent.subnodes indexOfObject:node] + 1 == node.parent.subnodes.count) {
                    // take off the padding
                    // (if we don't do this, the bottom line of the final box will be off
                    subViewOriginY -= _padding.height;
                }
            }
        }
        else if ([subnode isKindOfClass:[NSIndexPath class]])
        {
            FNKLayoutOptions cellLayoutOptions = [self layoutOptionsAtIndexPath:subnode];
            BOOL isCellEmpty = cellLayoutOptions& FNKLayoutOptionEmptyCell;
            
            if (isSummary || !isCellEmpty)
            {
                CGRect frame = [self defaultRectForOriginY:subViewOriginY
                                               columnIndex:subViewColumnIndex];
                
                if (isSummary)
                {
                    if (node.subnodes.count <= 4) {
                        // calculate the number of "rows" required
                        NSUInteger summaryRowCount = 1;
                        if (node.subnodes.count > 4) {
                            // if there are more than 4 parts
                            // subtract the left and right splits
                            summaryRowCount = node.subnodes.count - 2;
                        }
                        // "row" height
                        frame.size.height = MAX(SUMMARY_NODE_MIDDLE_SPLIT * summaryRowCount, 44);
                        /*
                         
                         +------+------------------------+---------+  -
                         |      |                        |         |  |
                         |   2  |            0           |    3    |  |
                         |      |                        |         |  |
                         |      +------------------------+         |  - middleSplit
                         |      |            1           |         |
                         |      |                        |         |
                         +------+------------------------+---------+
                         
                         |------| leftSplit              |---------| rightSplit
                         
                         */
                        
                        // the number of subnodes now affects where the cells are laid out
                        // so the subnodeIndex means different things
                        // if there are only 3 subnodes, we don't need to split the middle cell, so "1" above is skipped
                        if (node.subnodes.count <= 3) {
                            switch (subnodeIndex) {
                                case 0: {
                                    CGRectSlice(frame, &frame, SUMMARY_NODE_LEFT_SPLIT, CGRectMinXEdge);
                                    CGRectSlice(frame, &frame, SUMMARY_NODE_RIGHT_SPLIT, CGRectMaxXEdge);
                                    break;
                                }
                                case 1: {
                                    frame = CGRectSlice(frame, NULL, SUMMARY_NODE_LEFT_SPLIT, CGRectMinXEdge);
                                    break;
                                }
                                case 2: {
                                    frame = CGRectSlice(frame, NULL, SUMMARY_NODE_RIGHT_SPLIT+1, CGRectMaxXEdge);
                                    break;
                                }
                                default:
                                    break;
                            }
                        } else if (node.subnodes.count >= 4) {
                            switch (subnodeIndex) {
                                case 0: {
                                    CGRectSlice(frame, &frame, SUMMARY_NODE_LEFT_SPLIT, CGRectMinXEdge);
                                    CGRectSlice(frame, &frame, SUMMARY_NODE_RIGHT_SPLIT, CGRectMaxXEdge);
                                    frame = CGRectSlice(frame, NULL, SUMMARY_NODE_MIDDLE_SPLIT, CGRectMinYEdge);
                                    break;
                                }
                                case 1: {
                                    // slice off the previous row
                                    CGRectSlice(frame, &frame, SUMMARY_NODE_MIDDLE_SPLIT, CGRectMinYEdge);
                                    CGRectSlice(frame, &frame, SUMMARY_NODE_LEFT_SPLIT, CGRectMinXEdge);
                                    CGRectSlice(frame, &frame, SUMMARY_NODE_RIGHT_SPLIT, CGRectMaxXEdge);
                                    frame = CGRectSlice(frame, NULL, SUMMARY_NODE_MIDDLE_SPLIT, CGRectMinYEdge);
                                    break;
                                }
                                case 2: {
                                    frame = CGRectSlice(frame, NULL, SUMMARY_NODE_LEFT_SPLIT, CGRectMinXEdge);
                                    break;
                                }
                                case 3: {
                                    frame = CGRectSlice(frame, NULL, SUMMARY_NODE_RIGHT_SPLIT+1, CGRectMaxXEdge);
                                    break;
                                }
                                default: {
                                    // slice off the previous rows
                                    CGRectSlice(frame, &frame, SUMMARY_NODE_MIDDLE_SPLIT * (subnodeIndex - 2), CGRectMinYEdge);
                                    CGRectSlice(frame, &frame, SUMMARY_NODE_LEFT_SPLIT, CGRectMinXEdge);
                                    CGRectSlice(frame, &frame, SUMMARY_NODE_RIGHT_SPLIT, CGRectMaxXEdge);
                                    frame = CGRectSlice(frame, NULL, SUMMARY_NODE_MIDDLE_SPLIT, CGRectMinYEdge);
                                    break;
                                }
                            }
                        }
                    } else {
                        // if more than 4 subnodes, adopt a grid layout
                        // calculate the number of "rows" required
                        NSUInteger summaryRowCount = node.subnodes.count / 2;
                        if (node.subnodes.count % 2 != 0) {
                            summaryRowCount++;
                        }
                        // "row" height
                        frame.size.height = MAX(SUMMARY_NODE_MIDDLE_SPLIT * summaryRowCount, 44);
                        if (subnodeIndex % 2 == 0) {
                            // slice off the previous rows
                            CGRectSlice(frame, &frame, SUMMARY_NODE_MIDDLE_SPLIT * (subnodeIndex / 2), CGRectMinYEdge);
                            frame = CGRectSlice(frame, NULL, SUMMARY_NODE_MIDDLE_SPLIT, CGRectMinYEdge);
                            // slice off the left half of the row
                            frame = CGRectSlice(frame, NULL, frame.size.width / 2.0, CGRectMinXEdge);
                        } else {
                            // slice off the previous rows
                            CGRectSlice(frame, &frame, SUMMARY_NODE_MIDDLE_SPLIT * (subnodeIndex / 2), CGRectMinYEdge);
                            frame = CGRectSlice(frame, NULL, SUMMARY_NODE_MIDDLE_SPLIT, CGRectMinYEdge);
                            // slice off the right half of the row
                            frame = CGRectSlice(frame, NULL, frame.size.width / 2.0, CGRectMaxXEdge);
                        }
                    }
                    
                    if (!isLastSubNode)
                    {
                        canUpdateMinSubViewOriginY = NO;
                    }
                }
                else
                {
                    frame.size.height = [self heightForItemAtIndexPath:subnode];
                    
                    if (canSquashSubnodes)
                    {
                        frame.size.height -= DEFAULT_NON_SUMMARY_SQUASH;
                    }
                    
                    if (isDropdown && !isLockedToDefaultSubnode && numberOfSubnodesDrawn == 0)
                    {
                        // leave space for the drop down button
                        CGRectSlice(frame, &frame, DROPDOWN_BUTTON_WIDTH, CGRectMaxXEdge);
                    }
                }
                
//                NSLog(@"cell: %@ %@", [subnode OCGExtensions_description], NSStringFromCGRect(frame));
                
                subViewOriginY = [self layoutCellIndexPath:subnode
                                                      rect:frame
                                                  isPinned:isSubNodePinnedToTheBottom
                                                   isBoxed:areSubnodesBoxed];
            }
        }
        else
        {
            
        }
        
        // for summary cells with more than 4 display keys
        if (isSummary && (node.subnodes.count > 4)) {
            // if this is the last cell add a small gap to seperate out summaries
            if (isLastSubNode) {
                // add a little padding to seperate them out
                subViewOriginY += _padding.height;
            }
            // box the first and last row
            if ((subnodeIndex == node.subnodes.count - 1) || (subnodeIndex == 0)) {
                isBoxed = YES;
            } else {
                isBoxed = NO;
            }
        } else {
            if (node.indexPath.length > 0 && canUpdateMinSubViewOriginY)
            {
                // draw the intra box cell dividing line
                
                [self layoutSeperatorX1:_columnWidth * subViewColumnIndex + _padding.width + leftIntraSubnodeSeparatorIndent
                                     y1:subViewOriginY
                                     x2:_columnWidth * (subViewColumnIndex + 1) - rightIntraSubnodeSeparatorIndent
                                     y2:subViewOriginY
                               isPinned:isSubNodePinnedToTheBottom];
            }
        }
        
        if (canUpdateMinSubViewOriginY && node.indexPath.length != 1 && minSubViewOriginY < subViewOriginY)
        {
            minSubViewOriginY = subViewOriginY;
        }
        
        if (maxSubViewOriginY < subViewOriginY)
        {
            maxSubViewOriginY = subViewOriginY;
        }
        
        if (numberOfSubnodesDrawn == 0)
        {
            heightOfFirstSubnode = subViewOriginY - originY;
        }
        
        numberOfSubnodesDrawn++;
    }
    
    if (isDropdown && !isLockedToDefaultSubnode)
    {
        if (numberOfSubnodesDrawn == 0)
        {
            // draw a fake default subnode if there are no default values in the drop down.
            
            FNKCollectionViewLayoutAttributes *attributes = nil;
            
            FNKSupplementaryItem *supplementaryItem = node.headerItem;
            
            CGRect frame = [self defaultRectForOriginY:originY
                                           columnIndex:columnIndex];
            
            frame.size.height = 44;
            
            CGRectSlice(frame, &frame, DROPDOWN_BUTTON_WIDTH, CGRectMaxXEdge);
            
            
            CGFloat supplementaryViewOriginY = [self layoutSupplementaryItem:supplementaryItem
                                                                        kind:FNKSupplementaryKindCell
                                                                        rect:frame
                                                                    isPinned:isPinned
                                                                  attributes:&attributes];
            
            if (maxSubViewOriginY < supplementaryViewOriginY)
            {
                maxSubViewOriginY = supplementaryViewOriginY;
            }
            
            heightOfFirstSubnode = frame.size.height;
            
            attributes.nodePath = node.indexPath;
        }
        else
        {
            // All decoration indexes need to be static over a relayout due to drop down being opened and closed.
            _decorationIndex++;
        }
        
        
        FNKCollectionViewLayoutAttributes *attributes = nil;
        
        FNKSupplementaryItem *supplementaryItem = [[FNKSupplementaryItem alloc] init];
        
        CGRect frame = [self defaultRectForOriginY:originY
                                       columnIndex:columnIndex];
        
        frame.size.height = heightOfFirstSubnode;
        
        frame = CGRectSlice(frame, NULL, DROPDOWN_BUTTON_WIDTH, CGRectMaxXEdge);
        
        
        [self layoutSupplementaryItem:supplementaryItem
                                 kind:FNKSupplementaryKindSectionTab
                                 rect:frame
                             isPinned:isPinned
                           attributes:&attributes];
        
        attributes.nodePath = node.indexPath;
        
        if (isNodeSelected)
        {
            attributes.selected = YES;
            supplementaryItem.image = [UIImage imageNamed:@"UIImages/UIButtonBarArrowUp"];
        }
        else
        {
            supplementaryItem.image = [UIImage imageNamed:@"UIImages/UIButtonBarArrowDown"];
        }
        
        // left edge of drop down button
        
        [self layoutSeperatorX1:CGRectGetMinX(frame)
                             y1:CGRectGetMinY(frame)
                             x2:CGRectGetMinX(frame)
                             y2:CGRectGetMaxY(frame)
                       isPinned:isPinned];
        
        // bottom edge of drop down button
        
        [self layoutSeperatorX1:CGRectGetMinX(frame)
                             y1:CGRectGetMaxY(frame)
                             x2:CGRectGetMaxX(frame)
                             y2:CGRectGetMaxY(frame)
                       isPinned:isPinned];
    }
    
    if (isTabbed && !isLockedToDefaultSubnode  && _numberOfColumns > 1)
    {
        [self layoutSeperatorX1:_columnWidth * columnIndex + _padding.width
                             y1:maxSubViewOriginY
                             x2:_columnWidth * (columnIndex + 1)
                             y2:maxSubViewOriginY
                       isPinned:isPinned];
        
        CGFloat tabHeight = 44;
        
        NSUInteger numberOfTabs = node.subnodes.count;
        
        if (defaultSubnodeIndex >= 0)
        {
            numberOfTabs--;
        }
        
        if (numberOfTabs > 0)
        {
            CGFloat tabWidth = (_columnWidth - _padding.width)/numberOfTabs;
            
            NSInteger numberOfTabsDrawn = 0;
            for (NSInteger subnodeIndex=0; subnodeIndex < node.subnodes.count; subnodeIndex++)
            {
                if (defaultSubnodeIndex == subnodeIndex)
                {
                    continue;
                }
                
                FNKNode *subnode = node.subnodes[subnodeIndex];
                
                FNKCollectionViewLayoutAttributes *attributes = nil;
                
                FNKSupplementaryItem *supplementaryItem = subnode.headerItem;
                
                CGRect frame = [self defaultRectForOriginY:maxSubViewOriginY
                                               columnIndex:columnIndex];
                
                frame.origin.x += (tabWidth * numberOfTabsDrawn);
                frame.size.width = tabWidth;
                frame.size.height = tabHeight;
                
                [self layoutSupplementaryItem:supplementaryItem
                                         kind:FNKSupplementaryKindSectionTab
                                         rect:frame
                                     isPinned:isPinned
                                   attributes:&attributes];
                
                attributes.nodePath = subnode.indexPath;
                
                if (selectedSubnodeIndex == subnodeIndex)
                {
                    attributes.selected = YES;
                }
                
                if (numberOfTabsDrawn > 0)
                {
                    [self layoutSeperatorX1:frame.origin.x
                                         y1:maxSubViewOriginY
                                         x2:frame.origin.x
                                         y2:maxSubViewOriginY + tabHeight
                                   isPinned:isPinned];
                }
                
                numberOfTabsDrawn++;
            }
            
            
            
            maxSubViewOriginY += tabHeight;
        }
    }
    
    if (isBoxed)
    {
        // a little adjustment is neccessary at this point
        CGFloat padding = 0.0f;
        // if we are rendering a summary cell with more than 4 keys
        if (isSummary && (node.subnodes.count > 4)) {
            // take the excess padding we added off when rendering the box
            padding = _padding.height;
        }
        
        [self layoutBoxX1:_columnWidth * columnIndex + _padding.width
                       y1:originY
                       x2:_columnWidth * (columnIndex + 1)
                       y2:maxSubViewOriginY - padding
                 isPinned:isPinned];
    }
    
    originY = maxSubViewOriginY;
    
    // Display old style section headers
    
    if (node.indexPath.length == 1)
    {
        FNKSupplementaryItem *supplementaryItem = node.footerItem;
        
        if (supplementaryItem.title.length)
        {
            CGRect frame = [self defaultRectForOriginY:originY
                                           columnIndex:columnIndex];
            frame.size.height = [FNKSupplementaryView heightForText:supplementaryItem.title width:CGRectGetWidth(frame)];
            
            originY = [self layoutSupplementaryItem:supplementaryItem
                                               kind:FNKSupplementaryKindSectionHeader
                                               rect:frame
                                           isPinned:isPinned
                                         attributes:NULL];
        }
    }
    
    return originY;
}

-(CGFloat)layoutCellIndexPath:(NSIndexPath*)cellIndexPath
                         rect:(CGRect)frame
                     isPinned:(BOOL)isPinned
                      isBoxed:(BOOL)isBoxed
{
    FNKLayoutOptions layoutOptions = [self layoutOptionsAtIndexPath:cellIndexPath];
    
    //    CGRect frame = CGRectZero;
    //    // Item layout
    //
    //    frame.origin.x = _columnWidth * columnIndex + _padding.width;
    //    frame.origin.y = originY;
    //    frame.size.height = [self heightForItemAtIndexPath:cellIndexPath];
    //    frame.size.width = _columnWidth - _padding.width;
    //
    //    frame = UIEdgeInsetsInsetRect(frame, edgeInsets);
    
    UICollectionViewLayoutAttributes *attributes = [UICollectionViewLayoutAttributes layoutAttributesForCellWithIndexPath:cellIndexPath];
    attributes.frame = frame;
    _itemAttributes[cellIndexPath] = attributes;
    
    if (isPinned)
    {
        [_pinnedAttributes addObject:attributes];
    }
    
    if (isBoxed)
    {
        [self layoutBoxX1:CGRectGetMinX(frame)
                       y1:CGRectGetMinY(frame)
                       x2:CGRectGetMaxX(frame)
                       y2:CGRectGetMaxY(frame)
                 isPinned:isPinned];
    }
    
    return CGRectGetMaxY(frame);
}


-(CGFloat)layoutSupplementaryItem:(FNKSupplementaryItem*)supplementaryItem
                             kind:(NSString*)kind
                             rect:(CGRect)frame
                         isPinned:(BOOL)isPinned
                       attributes:(FNKCollectionViewLayoutAttributes **)attributes
{
    FNKCollectionViewLayoutAttributes *internalAttributes =
    [FNKCollectionViewLayoutAttributes layoutAttributesForDecorationViewOfKind:kind
                                                                 withIndexPath:[NSIndexPath indexPathWithIndex:_decorationIndex++]];
    
    internalAttributes.frame = frame;
    internalAttributes.item = supplementaryItem;
    
    _headerAttributes[internalAttributes.indexPath] = internalAttributes;
    
    if (isPinned)
    {
        [_pinnedAttributes addObject:internalAttributes];
    }
    
    if (attributes)
    {
        *attributes = internalAttributes;
    }
    
    return CGRectGetMaxY(frame);
}

-(CGRect)defaultRectForOriginY:(CGFloat)originY
                   columnIndex:(NSInteger)columnIndex
{
    CGRect frame = CGRectZero;
    frame.origin.x = _columnWidth * columnIndex + _padding.width;
    frame.origin.y = originY;
    frame.size.width = _columnWidth - _padding.width;
    return frame;
}


-(void)layoutBoxX1:(CGFloat)x1
                y1:(CGFloat)y1
                x2:(CGFloat)x2
                y2:(CGFloat)y2
          isPinned:(BOOL)isPinned
{
    // top sep
    
    [self layoutSeperatorX1:MIN(x1, x2)
                         y1:MIN(y1, y2)
                         x2:MAX(x1, x2)
                         y2:MIN(y1, y2)
                   isPinned:isPinned];
    
    if (_numberOfColumns > 1)
    {
        // left sep
        [self layoutSeperatorX1:MIN(x1, x2)
                             y1:MIN(y1, y2)
                             x2:MIN(x1, x2)
                             y2:MAX(y1, y2)
                       isPinned:isPinned];
        
        // right sep
        
        [self layoutSeperatorX1:MAX(x1, x2)
                             y1:MIN(y1, y2)
                             x2:MAX(x1, x2)
                             y2:MAX(y1, y2)
                       isPinned:isPinned];
    }
    
    // bottom sep
    
    [self layoutSeperatorX1:MIN(x1, x2)
                         y1:MAX(y1, y2)
                         x2:MAX(x1, x2)
                         y2:MAX(y1, y2)
                   isPinned:isPinned];
}


-(void)layoutSeperatorX1:(CGFloat)x1
                      y1:(CGFloat)y1
                      x2:(CGFloat)x2
                      y2:(CGFloat)y2
                isPinned:(BOOL)isPinned
{
    NSIndexPath *indexPath = [NSIndexPath indexPathWithIndex:_separatorAttributes.count];
    
    FNKCollectionViewLayoutAttributes *attributes = nil;
    attributes =
    [FNKCollectionViewLayoutAttributes layoutAttributesForDecorationViewOfKind:FNKSupplementaryKindSectionSeperator
                                                                 withIndexPath:indexPath];
    
    attributes.frame = CGRectMake(MIN(x1, x2), MIN(y1, y2), MAX(ABS(x1 - x2), 1), MAX(ABS(y1 - y2), 1));
    attributes.zIndex = 1;
    
    _separatorAttributes[indexPath] = attributes;
    
    if (isPinned)
    {
        [_pinnedAttributes addObject:attributes];
    }
}


- (UICollectionViewLayoutAttributes *)layoutAttributesForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewLayoutAttributes *layoutAttributes = _itemAttributes[indexPath];
    if (!layoutAttributes)
    {
        NSIndexPath *actuallyDrawnIndexPath = indexPath;
        UICollectionViewLayoutAttributes *actuallyDrawnLayoutAttributes = nil;
        do
        {
            actuallyDrawnIndexPath = [self.collectionView indexPathFromIndexPath:actuallyDrawnIndexPath
                                                                          offset:-1
                                                                            wrap:NO];
            actuallyDrawnLayoutAttributes = _itemAttributes[actuallyDrawnIndexPath];
        }
        while (!actuallyDrawnLayoutAttributes);
        
        if (actuallyDrawnLayoutAttributes)
        {
            layoutAttributes = [UICollectionViewLayoutAttributes layoutAttributesForCellWithIndexPath:indexPath];
            layoutAttributes.frame = CGRectSlice(actuallyDrawnLayoutAttributes.frame, NULL, 0.1, CGRectMaxYEdge);
            layoutAttributes.hidden = YES;
            _itemAttributes[indexPath] = layoutAttributes;
        }
        NSAssert(layoutAttributes, @"");
    }
    
	return layoutAttributes;
}

- (UICollectionViewLayoutAttributes *)layoutAttributesForDecorationViewOfKind:(NSString *)decorationViewKind atIndexPath:(NSIndexPath *)indexPath
{
    //    NSAssert(indexPath.length == 3, @"");
    
    UICollectionViewLayoutAttributes *layoutAttributes = nil;
    if ([decorationViewKind isEqualToString:FNKSupplementaryKindSectionHeader])
    {
        layoutAttributes = _headerAttributes[indexPath];
    }
    else if ([decorationViewKind isEqualToString:FNKSupplementaryKindSectionTab])
    {
        layoutAttributes = _headerAttributes[indexPath];
    }
    else if ([decorationViewKind isEqualToString:FNKSupplementaryKindSectionFooter])
    {
        layoutAttributes = _footerAttributes[indexPath];
    }
    else if ([decorationViewKind isEqualToString:FNKSupplementaryKindSectionSeperator])
    {
        layoutAttributes = _separatorAttributes[indexPath];
    }
    
    if (layoutAttributes == nil)
    {
        /*
         UICollectionView crashes if we return nil here. So, we create a new layoutAttributes object
         just to keep it happy.
         
         Note, we could do some optimisation which would mean we didn't completely refresh all the decorations,
         but we'd need to rewrite most of the decorations code.
         */
        
        layoutAttributes =
        [FNKCollectionViewLayoutAttributes layoutAttributesForDecorationViewOfKind:decorationViewKind
                                                                     withIndexPath:indexPath];
        layoutAttributes.hidden = YES;
        layoutAttributes.frame = CGRectZero;
        
    }
    NSAssert(layoutAttributes, @"");
    return layoutAttributes;
}

- (NSArray *)layoutAttributesForElementsInRect:(CGRect)rect {
    //    NSLog(@"%@ %@",NSStringFromSelector(_cmd), NSStringFromCGRect(self.collectionView.bounds));
    CGFloat maxPinnedY = 0;
    for (FNKCollectionViewLayoutAttributes *attributes in _pinnedAttributes)
    {
        CGRect frame = attributes.frame;
        
        if (maxPinnedY < CGRectGetMaxY(frame))
        {
            maxPinnedY = CGRectGetMaxY(frame);
        }
    }
    
    CGFloat pinnedOffset = CGRectGetMaxY(self.collectionView.frame) - maxPinnedY - _padding.height;
    //    NSLog(@"pinneOffset: %f", pinnedOffset);
    if (pinnedOffset > 0)
    {
        for (FNKCollectionViewLayoutAttributes *attributes in _pinnedAttributes)
        {
            CGRect frame = attributes.frame;
            frame.origin.y += pinnedOffset;
            attributes.frame = frame;
        }
    }
    
	//possibly optimize this
	NSMutableArray *layoutAttributes = [NSMutableArray array];
    for (NSIndexPath *indexPath in _itemAttributes)
    {
        UICollectionViewLayoutAttributes *attributes = _itemAttributes[indexPath];
        if (CGRectIntersectsRect(rect, attributes.frame))
        {
            [layoutAttributes addObject:attributes];
        }
    }
    for (NSIndexPath *indexPath in _headerAttributes)
    {
        UICollectionViewLayoutAttributes *attributes = _headerAttributes[indexPath];
        if (CGRectIntersectsRect(rect, attributes.frame))
        {
            [layoutAttributes addObject:attributes];
        }
    }
    for (NSIndexPath *indexPath in _footerAttributes)
    {
        UICollectionViewLayoutAttributes *attributes = _footerAttributes[indexPath];
        if (CGRectIntersectsRect(rect, attributes.frame))
        {
            [layoutAttributes addObject:attributes];
        }
    }
    for (NSIndexPath *indexPath in _separatorAttributes)
    {
        UICollectionViewLayoutAttributes *attributes = _separatorAttributes[indexPath];
        if (CGRectIntersectsRect(rect, attributes.frame))
        {
            [layoutAttributes addObject:attributes];
        }
    }
    
	return layoutAttributes;
}

- (BOOL)shouldInvalidateLayoutForBoundsChange:(CGRect)newBounds
{
    return CGRectGetWidth(self.collectionView.bounds) != CGRectGetWidth(newBounds);
}

#define OPTIONAL(anObject, aSelector) ([(anObject) respondsToSelector:@selector(aSelector)] ? (id)(anObject) : nil)

#pragma mark - FNKCollectionViewDataSourceDelegate calls

- (NSArray *)supplementaryItemsForHeadersAtIndexPath:(NSIndexPath *)indexPath
{
    id dataSource = OPTIONAL(self.collectionView.dataSource, collectionView:layout:supplementaryItemsForHeadersAtIndexPath:);
    return [dataSource collectionView:self.collectionView
                               layout:self
supplementaryItemsForHeadersAtIndexPath:indexPath];
}

- (NSArray *)supplementaryItemsForFootersAtIndexPath:(NSIndexPath *)indexPath
{
    id dataSource = OPTIONAL(self.collectionView.dataSource, collectionView:layout:supplementaryItemsForFootersAtIndexPath:);
    
    return [dataSource collectionView:self.collectionView
                               layout:self
supplementaryItemsForFootersAtIndexPath:indexPath];
}

- (FNKLayoutOptions)layoutOptionsAtIndexPath:(NSIndexPath *)indexPath
{
    id dataSource = OPTIONAL(self.collectionView.dataSource, collectionView:layout:layoutOptionsAtIndexPath:);
    
    return [dataSource collectionView:self.collectionView
                               layout:self
             layoutOptionsAtIndexPath:indexPath];
}

- (CGFloat)heightForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat result = _blockSize.height;
    
    id dataSource = OPTIONAL(self.collectionView.dataSource, collectionView:heightForItemAtIndexPath:);
    if (dataSource) {
        result = [dataSource collectionView:self.collectionView
                   heightForItemAtIndexPath:indexPath];
    }
    
    return result;
}

@end
