//
//  UICollectionView+OCGExtensions.h
//  mPOS
//
//  Created by John Scott on 09/07/2014.
//  Copyright (c) 2014 Clarity. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UICollectionView (OCGExtensions)

/** @brief The first index path of the table view, or nil if the table is empty.
 **/
@property (nonatomic, readonly) NSIndexPath *firstIndexPath;

/** @brief The last index path of the table view, or nil if the table is empty.
 **/
@property (nonatomic, readonly) NSIndexPath *lastIndexPath;

/** @brief Returns the index path at a given offset from another index path.
 @param indexPath The given indexPath
 @param offset the offset from the given indexPath
 @param wrap Should the method wrap round if the offset is off the end of the
 table. This method will return nil if wrap is false and the offset
 is off the end.
 **/
- (NSIndexPath *)indexPathFromIndexPath:(NSIndexPath *)indexPath offset:(NSInteger)offset wrap:(BOOL)wrap;

@end
