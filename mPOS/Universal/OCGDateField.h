//
//  OCGDateField.h
//  mPOS
//
//  Created by John Scott on 08/04/2015.
//  Copyright (c) 2015 Clarity. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OCGKeyboardTypeButtonItem.h"
#import "OCGDateInputView.h"

extern NSString *const OCGDateFieldDateDidBeginEditingNotification;
extern NSString *const OCGDateFieldDateDidEndEditingNotification;
extern NSString *const OCGDateFieldDateDidChangeNotification;

@interface OCGDateField : UIControl <OCGKeyboardTypeTraits, UIKeyInput, OCGDateInput>

@property (nonatomic, copy) NSDate *date;
@property (nonatomic, copy) NSDateFormatter *dateFormatter;

@property (readwrite, strong) UIView *inputView;
@property (readwrite, strong) UIView *inputAccessoryView;

@property(nonatomic, assign) NSTextAlignment textAlignment;
@property(nonatomic, assign) UITextFieldViewMode clearButtonMode;

@property(nonatomic) UITextAutocapitalizationType autocapitalizationType;
@property(nonatomic) UITextAutocorrectionType autocorrectionType;
@property(nonatomic) UITextSpellCheckingType spellCheckingType;
@property(nonatomic) UIKeyboardType keyboardType;
@property(nonatomic) UIKeyboardAppearance keyboardAppearance;
@property(nonatomic) UIReturnKeyType returnKeyType;
@property(nonatomic) BOOL enablesReturnKeyAutomatically;
@property(nonatomic,getter=isSecureTextEntry) BOOL secureTextEntry;

@end
