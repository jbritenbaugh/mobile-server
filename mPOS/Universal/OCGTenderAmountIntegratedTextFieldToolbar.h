//
//  OCGTenderAmountIntegratedTextFieldToolbar.h
//  mPOS
//
//  Created by Antonio Strijdom on 14/02/2014.
//  Copyright (c) 2014 Clarity. All rights reserved.
//

#import "OCGIntegratedTextFieldKeyboardToolbar.h"
#import "RESTController.h"
#import "FNKTextField.h"

/** @file OCGIntegratedTextFieldKeyboardToolbar.h */

typedef void(^TenderAmountEnteredBlockType)(NSDecimalNumber *amountTendered);

/** @brief Keyboard accessory view for entering a tender amount.
 */
@interface OCGTenderAmountIntegratedTextFieldToolbar : NSObject

@property (nonatomic, strong) FNKTextField *textField;

/** @brief Shows an input view for capturing the tendered amount
 *  @param payment            The payment we are capturing the amount for.
 *  @param amountEnteredBlock The block called when the tendered amount is captured.
 *  @param terminatedBlock    The block called when the amount capture process is cancelled.
 */
- (void) showTenderAmountKeyboardForPayment:(Payment *)payment
                     withAmountEnteredBlock:(TenderAmountEnteredBlockType)amountEnteredBlock
                         andTerminatedBlock:(dispatch_block_t)terminatedBlock;


@end
