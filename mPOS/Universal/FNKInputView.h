//
//  FNKInputView.h
//  Flow
//
//  Created by John Scott on 15/08/2014.
//  Copyright (c) 2014 Omnico Group. All rights reserved.
//



#import <UIKit/UIKit.h>

@interface FNKeyboardType : NSObject

@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSDictionary *titleAttributes;
@property (nonatomic, strong) UIView *inputView;
@property (nonatomic, assign) BOOL hideFirstResponder;

@end

@interface FNKInputView : UIInputView

@end

@interface UIResponder (FNKInputView)

@property (nonatomic, strong) NSArray *FNKInputView_keyboardTypes;
@property (nonatomic, assign) NSInteger FNKInputView_selectedKeyboardTypeIndex;
@property (nonatomic, strong) NSString *FNKInputView_doneButtonTitle;
@property (nonatomic, assign) BOOL FNKInputView_doneButtonEnabled;
@property (nonatomic, assign) BOOL FNKInputView_switchingKeyboardType;

@end
