//
//  FNKInputView.m
//  Flow
//
//  Created by John Scott on 15/08/2014.
//  Copyright (c) 2014 Omnico Group. All rights reserved.
//

#import "FNKInputView.h"

#import "FNKTextField.h"
#import "UIImage+CRSUniversal.h"
#import <objc/runtime.h>
#import "POSInputView.h"

@interface FNKeyboardType ()

@property (nonatomic, assign) UIKeyboardType keyboardType;

@end

@implementation FNKeyboardType
@end

@implementation FNKInputView
{
    UIToolbar *_toolbar;
    UIBarButtonItem *_leftButton;
    UIBarButtonItem *_rightButton;
    UIBarButtonItem *_textFieldButton;
    UIBarButtonItem *_dismissButton;
}

-(void)didMoveToWindow
{
    if (!_toolbar)
    {
//        [[NSNotificationCenter defaultCenter] addObserver:self
//                                                 selector:@selector(textFieldTextDidChangeNotification:)
//                                                     name:UITextFieldTextDidChangeNotification object:nil];
        
        UIButton *dismissButton = [UIButton buttonWithType:UIButtonTypeCustom];
        dismissButton.contentMode = UIViewContentModeCenter;
        [dismissButton sizeToFit];
        [dismissButton setImage:[UIImage imageNamed:@"UIImages/bold_dismiss_split"]
                      forState:UIControlStateNormal];
        [dismissButton addTarget:self
                         action:@selector(dismissTapped:)
               forControlEvents:UIControlEventTouchUpInside];
        _dismissButton = [[UIBarButtonItem alloc] initWithTitle:@"\uF005"
                                                          style:UIBarButtonItemStylePlain
                                                         target:self
                                                         action:@selector(dismissTapped:)];

        _leftButton = [[UIBarButtonItem alloc] initWithTitle:@"Left"
                                                       style:UIBarButtonItemStylePlain
                                                      target:self
                                                      action:@selector(leftButtonTapped)];
        
        
        [_leftButton setTitleTextAttributes:@{
                                              NSFontAttributeName: [UIFont fontWithName:@"OmnicoGroup-Regular" size:18.0],
                                              } forState:UIControlStateNormal];
        
        [_dismissButton setTitleTextAttributes:@{
                                              NSFontAttributeName: [UIFont fontWithName:@"OmnicoGroup-Regular" size:18.0],
                                              } forState:UIControlStateNormal];
        
        _rightButton = [[UIBarButtonItem alloc] initWithTitle:@"Right"
                                                        style:UIBarButtonItemStyleDone
                                                       target:self
                                                       action:@selector(rightButtonTapped)];
        
        _textFieldButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace
                                                                         target:nil
                                                                         action:NULL];

        
        _toolbar = [[UIToolbar alloc] initWithFrame:self.bounds];
        
        _toolbar.autoresizingMask = UIViewAutoresizingFlexibleWidth;
        [_toolbar setBackgroundImage:[UIImage new]
                  forToolbarPosition:UIBarPositionAny
                          barMetrics:UIBarMetricsDefault];
        [_toolbar setShadowImage:[UIImage new]
              forToolbarPosition:UIToolbarPositionAny];
        
        _toolbar.items = @[_leftButton, _textFieldButton, _rightButton];
        
        [self addSubview:_toolbar];
    }
    
    FNKTextField *firstResponder = (FNKTextField *) [self nextResponder];
    
    if (self.window)
    {
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(textFieldTextDidChangeNotification:)
                                                     name:UITextFieldTextDidChangeNotification object:nil];
    }
    else
    {
        [[NSNotificationCenter defaultCenter] removeObserver:self];
    }
}

- (void)textFieldTextDidChangeNotification:(NSNotification*)aNotification
{
    [self updateToolbar];
}

-(void)layoutSubviews
{
    [super layoutSubviews];
    
    [self updateToolbar];
    
    FNKTextField *firstResponder = (FNKTextField *) [self nextResponder];
    
    if ([firstResponder isKindOfClass:[FNKTextField class]])
    {
        CGFloat left = FLT_MAX;
        CGFloat right = FLT_MIN;
        
        for (UIView *subview in _toolbar.subviews)
        {
            if (left > CGRectGetMaxX(subview.frame))
            {
                left = CGRectGetMaxX(subview.frame);
            }
            
            if (right < CGRectGetMinX(subview.frame))
            {
                right = CGRectGetMinX(subview.frame);
            }
        }

        CGRect frame = CGRectMake(left, CGRectGetMinY(_toolbar.bounds), right - left, CGRectGetHeight(_toolbar.bounds));
        firstResponder.frame = CGRectInset(frame, 10, (CGRectGetHeight(_toolbar.bounds) - 30)/2.);
    }
}

-(FNKeyboardType *)boxedKeyboardTypeForKeyboardType:(id)keyboardType
{
    FNKeyboardType *boxedKeyboardType = nil;
    if ([keyboardType isKindOfClass:[NSNumber class]])
    {
        boxedKeyboardType = [[FNKeyboardType alloc] init];
        
        boxedKeyboardType.keyboardType = [keyboardType integerValue];

        
        switch (boxedKeyboardType.keyboardType)
        {
            case UIKeyboardTypeASCIICapable:
                boxedKeyboardType.title = @"\uF001";
                break;
                
            case UIKeyboardTypeNumberPad:
                boxedKeyboardType.title = @"\uF002";
                boxedKeyboardType.inputView = [[POSInputView alloc] init];
                break;
                
            default:
                boxedKeyboardType.title = @"?";
                break;
        }
        
        boxedKeyboardType.titleAttributes = @{
                                              NSFontAttributeName: [UIFont fontWithName:@"OmnicoGroup-Regular" size:18.0],
                                              };

    }
    else
    {
        boxedKeyboardType = keyboardType;
    }
    return boxedKeyboardType;
}

-(void)updateToolbar
{
    FNKTextField *firstResponder = (FNKTextField *) [self nextResponder];
    
    UIColor *barItemColor = nil;
    UIColor *barColor = nil;
    
    NSMutableArray *visibleItems = [NSMutableArray arrayWithCapacity:3];
    [_toolbar setItems:visibleItems animated:NO];
    [visibleItems addObjectsFromArray:@[_leftButton, _textFieldButton, _rightButton]];
    
    if ([firstResponder respondsToSelector:@selector(keyboardAppearance)])
    {
        if (UIDevice.currentDevice.systemVersion.floatValue >= 7.0 && [firstResponder keyboardAppearance] == UIKeyboardAppearanceDark)
        {
            barColor = [UIColor colorWithWhite:0.35 alpha:1.0];
            barItemColor = [UIColor colorWithWhite:0.85 alpha:1.0];
        }
        else
        {
            barColor = [UIColor colorWithRed:0.82 green:0.83 blue:0.861 alpha:1.000];
            barItemColor = [UIColor colorWithWhite:0.20 alpha:1.0];
        }
        
        _toolbar.tintColor = barItemColor;
        [_rightButton setTitleTextAttributes:@{NSForegroundColorAttributeName: barItemColor}
                                    forState:UIControlStateNormal];
    }
    
    if ([firstResponder respondsToSelector:@selector(keyboardType)] &&
        firstResponder.FNKInputView_keyboardTypes.count > 0 &&
        firstResponder.FNKInputView_selectedKeyboardTypeIndex >= 0)
    {
        FNKeyboardType *keyboardType = firstResponder.FNKInputView_keyboardTypes[firstResponder.FNKInputView_selectedKeyboardTypeIndex];
        keyboardType = [self boxedKeyboardTypeForKeyboardType:keyboardType];
        
        firstResponder.keyboardType = keyboardType.keyboardType;
        firstResponder.inputView = keyboardType.inputView;
        
        // if there is only one allowed keyboard type
        if (firstResponder.FNKInputView_keyboardTypes.count == 1) {
            // hide the left button
            if ([visibleItems containsObject:_leftButton]) {
                [visibleItems removeObject:_leftButton];
                [visibleItems insertObject:_dismissButton atIndex:0];
            }
            // tint the dismiss button image
            UIButton *dismissButton = (UIButton *)_dismissButton.customView;
            UIImage *dismissImage = [dismissButton imageForState:UIControlStateNormal];
            dismissImage = [dismissImage imageTintedWithColor:barItemColor];
            [dismissButton setImage:dismissImage
                           forState:UIControlStateNormal];
        } else if (firstResponder.FNKInputView_keyboardTypes.count > 1) {
            // if there is more than one keyboard type, show the left button
            if ([visibleItems containsObject:_dismissButton]) {
                [visibleItems removeObject:_dismissButton];
                [visibleItems insertObject:_leftButton atIndex:0];
            }
            
            NSInteger nextKeyboardTypeIndex = (firstResponder.FNKInputView_selectedKeyboardTypeIndex + 1) % firstResponder.FNKInputView_keyboardTypes.count;
            FNKeyboardType *nextKeyboardType = firstResponder.FNKInputView_keyboardTypes[nextKeyboardTypeIndex];
            nextKeyboardType = [self boxedKeyboardTypeForKeyboardType:nextKeyboardType];

            _leftButton.title = nextKeyboardType.title;
        }
        
        NSMutableDictionary *titleAttributes = [NSMutableDictionary dictionaryWithDictionary:keyboardType.titleAttributes];
        if (!titleAttributes[NSForegroundColorAttributeName])
        {
            titleAttributes[NSForegroundColorAttributeName] = barItemColor;
        }
        
        [_leftButton setTitleTextAttributes:titleAttributes forState:UIControlStateNormal];
        
#ifdef __IPHONE_8_0
        // http://stackoverflow.com/questions/24546339
        // reloadInputViews doesn't seem to do the trick in iOS 8
        if ([UIDevice currentDevice].systemVersion.floatValue >= 8.0) {
            // hide the quick type bar while we're at it
            if (firstResponder.autocorrectionType != UITextAutocorrectionTypeNo) {
                firstResponder.autocorrectionType = UITextAutocorrectionTypeNo;
                [firstResponder resignFirstResponder];
                [firstResponder becomeFirstResponder];
            }
            if (firstResponder.FNKInputView_switchingKeyboardType) {
                // http://stackoverflow.com/questions/24031362
                [firstResponder resignFirstResponder];
                [firstResponder becomeFirstResponder];
            }
        }
#endif
        if (firstResponder.FNKInputView_switchingKeyboardType) {
            [firstResponder reloadInputViews];
        }
    }
    
    _rightButton.title = firstResponder.FNKInputView_doneButtonTitle;
    _rightButton.enabled = firstResponder.FNKInputView_doneButtonEnabled;

    [_toolbar setItems:visibleItems];
}

- (void)leftButtonTapped
{
    UIResponder *firstResponder = [self nextResponder];
    if (firstResponder.FNKInputView_keyboardTypes.count > 1)
    {
        firstResponder.FNKInputView_switchingKeyboardType = YES;
        firstResponder.FNKInputView_selectedKeyboardTypeIndex = (firstResponder.FNKInputView_selectedKeyboardTypeIndex + 1) % firstResponder.FNKInputView_keyboardTypes.count;
    }
}

- (void)rightButtonTapped
{
    UITextField *firstResponder = (UITextField *) [self nextResponder];
    
    if ([firstResponder isKindOfClass:[UITextField class]]) {
        if ([firstResponder.delegate respondsToSelector:@selector(textFieldShouldReturn:)]) {
            [firstResponder.delegate textFieldShouldReturn:firstResponder];
        }
    }
}

- (void)dismissTapped:(id)sender
{
    UITextField *firstResponder = (UITextField *) [self nextResponder];
    [firstResponder resignFirstResponder];
    if ([firstResponder isKindOfClass:[UITextField class]]) {
        if ([firstResponder.delegate respondsToSelector:@selector(textFieldDidDismiss)]) {
            [(id<FNKTextFieldDelegate>)firstResponder.delegate textFieldDidDismiss];
        }
    }
}

@end

@implementation UIResponder (FNKInputView)

static char FNKInputViewKeyboardTypes;
static char FNKInputViewSelectedKeyboardTypeIndex;
static char FNKInputViewDoneButtonTitle;
static char FNKInputViewDoneButtonEnabled;
static char FNKInputViewSwitchingKeyboardType;

-(void)setFNKInputView_keyboardTypes:(NSArray *)keyboardTypes
{
    objc_setAssociatedObject(self, &FNKInputViewKeyboardTypes, keyboardTypes, OBJC_ASSOCIATION_RETAIN);
    self.FNKInputView_selectedKeyboardTypeIndex = 0;
}

-(NSArray *)FNKInputView_keyboardTypes
{
    NSArray *keyboardTypes = objc_getAssociatedObject(self, &FNKInputViewKeyboardTypes);
    
    if (keyboardTypes.count == 0)
    {
        keyboardTypes = @[
                          @(UIKeyboardTypeASCIICapable),
                          @(UIKeyboardTypeNumberPad),
                          ];
    }
    
    return keyboardTypes;
}

-(void)setFNKInputView_selectedKeyboardTypeIndex:(NSInteger)selectedKeyboardTypeIndex
{
    objc_setAssociatedObject(self, &FNKInputViewSelectedKeyboardTypeIndex, @(selectedKeyboardTypeIndex), OBJC_ASSOCIATION_RETAIN);
    [self.inputAccessoryView setNeedsLayout];
}

-(NSInteger)FNKInputView_selectedKeyboardTypeIndex
{
    NSUInteger selectedKeyboardTypeIndex = [objc_getAssociatedObject(self, &FNKInputViewSelectedKeyboardTypeIndex) unsignedIntegerValue];
    return selectedKeyboardTypeIndex;
}

- (NSString *)FNKInputView_doneButtonTitle
{
    NSString *result = objc_getAssociatedObject(self, &FNKInputViewDoneButtonTitle);
    return result;
}

- (void)setFNKInputView_doneButtonTitle:(NSString *)doneButtonTitle
{
    objc_setAssociatedObject(self, &FNKInputViewDoneButtonTitle, doneButtonTitle, OBJC_ASSOCIATION_RETAIN);
    [self.inputAccessoryView setNeedsLayout];
}

- (BOOL)FNKInputView_doneButtonEnabled
{
    BOOL result = YES;
    
    NSNumber *enabled = objc_getAssociatedObject(self, &FNKInputViewDoneButtonEnabled);
    if (enabled) {
        result = [enabled boolValue];
    }
    
    return result;
}

- (void)setFNKInputView_doneButtonEnabled:(BOOL)enabled
{
    objc_setAssociatedObject(self, &FNKInputViewDoneButtonEnabled, @(enabled), OBJC_ASSOCIATION_RETAIN);
    [self.inputAccessoryView setNeedsLayout];
}

- (BOOL)FNKInputView_switchingKeyboardType
{
    BOOL result = YES;
    
    NSNumber *enabled = objc_getAssociatedObject(self, &FNKInputViewSwitchingKeyboardType);
    if (enabled) {
        result = [enabled boolValue];
    }
    
    return result;
}

- (void)setFNKInputView_switchingKeyboardType:(BOOL)FNKInputView_switchingKeyboardType
{
    objc_setAssociatedObject(self, &FNKInputViewSwitchingKeyboardType, @(FNKInputView_switchingKeyboardType), OBJC_ASSOCIATION_RETAIN);
}

@end
