//
//  FNKSupplementaryItem.h
//  mPOS
//
//  Created by John Scott on 12/08/2014.
//  Copyright (c) 2014 Clarity. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSInteger, FNKSupplementaryOptions) {
    FNKSupplementaryOptionNone = 0,
    FNKSupplementaryOptionDefault = 1 << 0,
    FNKSupplementaryOptionTabbed = 1 << 2,
    FNKSupplementaryOptionDropdown = 1 << 3,
    FNKSupplementaryOptionLockToDefault = 1 << 4,
};

@interface FNKSupplementaryItem : NSObject

@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) UIImage *image;
@property (nonatomic, assign) FNKSupplementaryOptions options;

@end
