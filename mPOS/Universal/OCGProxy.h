//
//  OCGProxy.h
//  Proxy
//
//  Created by John Scott on 01/05/2014.
//  Copyright (c) 2014 Omnico Group. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface OCGProxy : NSProxy

@property (nonatomic, readonly) id target;

+ (id)proxyWithTarget:(id)target;

- (void)logInvocation:(NSInvocation *)invocation;

@end
