//
//  OCGKeyboardTypeButtonItem.m
//  mPOS
//
//  Created by John Scott on 25/02/2015.
//  Copyright (c) 2015 Clarity. All rights reserved.
//

#define IMPORTED_BY_OWN_IMPLEMENTION 1
#import "OCGKeyboardTypeButtonItem.h"
#undef IMPORTED_BY_OWN_IMPLEMENTION

#import "POSInputView.h"
#import "OCGDateInputView.h"
#import "OCGScanInputView.h"
#import "CameraInputView.h"

NSString * const kOCGKeyboardTypeDefault = @"kOCGKeyboardTypeDefault";
NSString * const kOCGKeyboardTypeASCIICapable = @"kOCGKeyboardTypeASCIICapable";
NSString * const kOCGKeyboardTypeNumbersAndPunctuation = @"kOCGKeyboardTypeNumbersAndPunctuation";
NSString * const kOCGKeyboardTypeURL = @"kOCGKeyboardTypeURL";
NSString * const kOCGKeyboardTypeNumberPad = @"kOCGKeyboardTypeNumberPad";
NSString * const kOCGKeyboardTypePhonePad = @"kOCGKeyboardTypePhonePad";
NSString * const kOCGKeyboardTypeNamePhonePad = @"kOCGKeyboardTypeNamePhonePad";
NSString * const kOCGKeyboardTypeEmailAddress = @"kOCGKeyboardTypeEmailAddress";
NSString * const kOCGKeyboardTypeDecimalPad = @"kOCGKeyboardTypeDecimalPad";

NSString * const kOCGKeyboardTypeDate = @"kOCGKeyboardTypeDate";
NSString * const kOCGKeyboardTypeScan = @"kOCGKeyboardTypeScan";

NSArray * kOCGKeyboardTypesDefaultUPC;

@interface _MessageInputView : UIInputView

@property (nonatomic, readonly) UILabel *textLabel;

@end

@implementation OCGKeyboardTypeButtonItem
{
    NSArray *_keyboardTypes;
    NSUInteger _keyboardTypeIndex;
    __weak UIResponder <OCGKeyboardTypeTraits> *_target;
}

+(void)load
{
    kOCGKeyboardTypesDefaultUPC = @[kOCGKeyboardTypeNumberPad, kOCGKeyboardTypeASCIICapable];
}

- (instancetype)initWithKeyboardTypes:(NSArray *)keyboardTypes style:(UIBarButtonItemStyle)style target:(UIResponder <OCGKeyboardTypeTraits> *)target
{
    self = [super initWithTitle:@"" style:style target:self action:@selector(didTapButtonItem)];
    
    if (self)
    {
        _keyboardTypes = [keyboardTypes copy];
        _target = target;
        
        NSDictionary *titleAttributes = @{
                                          NSFontAttributeName: [UIFont fontWithName:@"OmnicoGroup-Regular" size:18.0],
                                          };
        [self setTitleTextAttributes:titleAttributes forState:UIControlStateNormal];
        
        [self updateButtonItem];
    }
    return self;
}

CGFloat _KeyboardHeight()
{
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
    {
        return 216;
    }
    else
    {
        return 264;
    }
}

+ (void)setKeyboardType:(NSString *)keyboardType forTarget:(UIResponder <OCGKeyboardTypeTraits> *)target
{
    UIKeyboardType currentKeyboardType = target.keyboardType;
    UIView *currentInputView = target.inputView;
    
    DebugLog2(@"setKeyboardType: {}", keyboardType);
    if ([keyboardType isEqualToString:kOCGKeyboardTypeDefault])
    {
        target.keyboardType = UIKeyboardTypeDefault;
        target.inputView = nil;
    }
    else if ([keyboardType isEqualToString:kOCGKeyboardTypeASCIICapable])
    {
        target.keyboardType = UIKeyboardTypeASCIICapable;
        target.inputView = nil;
    }
    else if ([keyboardType isEqualToString:kOCGKeyboardTypeNumbersAndPunctuation])
    {
        target.keyboardType = UIKeyboardTypeNumbersAndPunctuation;
        target.inputView = nil;
    }
    else if ([keyboardType isEqualToString:kOCGKeyboardTypeURL])
    {
        target.keyboardType = UIKeyboardTypeURL;
        target.inputView = nil;
    }
    else if ([keyboardType isEqualToString:kOCGKeyboardTypeNumberPad])
    {
        target.keyboardType = UIKeyboardTypeNumberPad;
        target.inputView = [[POSInputView alloc] initWithKeyboardType:UIKeyboardTypeNumberPad];
    }
    else if ([keyboardType isEqualToString:kOCGKeyboardTypePhonePad])
    {
        target.keyboardType = UIKeyboardTypePhonePad;
        target.inputView = nil;
    }
    else if ([keyboardType isEqualToString:kOCGKeyboardTypeNamePhonePad])
    {
        target.keyboardType = UIKeyboardTypeNamePhonePad;
        target.inputView = nil;
    }
    else if ([keyboardType isEqualToString:kOCGKeyboardTypeEmailAddress])
    {
        target.keyboardType = UIKeyboardTypeEmailAddress;
        target.inputView = nil;
    }
    else if ([keyboardType isEqualToString:kOCGKeyboardTypeDecimalPad])
    {
        target.keyboardType = UIKeyboardTypeDecimalPad;
        target.inputView = [[POSInputView alloc] initWithKeyboardType:UIKeyboardTypeDecimalPad];
    }
    else if ([keyboardType isEqualToString:kOCGKeyboardTypeDate])
    {
        target.keyboardType = UIKeyboardTypeDefault;
        OCGDateInputView *datePicker = [[OCGDateInputView alloc] initWithFrame:CGRectMake(0, 0, 200, _KeyboardHeight())];
        target.inputView = datePicker;
    }
    else if ([keyboardType isEqualToString:kOCGKeyboardTypeScan])
    {
        if (BarcodeScannerController.sharedInstance.sourceType == SMScannerSourceTypeInternal)
        {
            target.keyboardType = UIKeyboardTypeDefault;
            CameraInputView *inputView = [[CameraInputView alloc] initWithFrame:CGRectMake(0, 0, 200, _KeyboardHeight())];
            target.inputView = inputView;
        }
        else if (BarcodeScannerController.sharedInstance.sourceType == SMScannerSourceTypeExternal)
        {
            target.keyboardType = UIKeyboardTypeDefault;
            OCGScanInputView *inputView = [[OCGScanInputView alloc] initWithFrame:CGRectMake(0, 0, 200, 0.01)];
            target.inputView = inputView;
        }
        else
        {
            target.keyboardType = UIKeyboardTypeDefault;
            _MessageInputView *inputView = [[_MessageInputView alloc] initWithFrame:CGRectMake(0, 0, 200, 44)];
            inputView.textLabel.text = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁎󠁤󠁡󠁥󠁘󠁙󠁴󠁫󠁬󠁁󠁕󠁬󠁷󠁦󠁘󠁊󠁚󠀲󠁷󠁵󠁄󠁹󠁈󠁔󠁓󠁑󠁧󠁿*/ @"No scanner has been set in Settings", nil);
            target.inputView = inputView;
        }
    }
    
    if (target.isFirstResponder && (currentKeyboardType != target.keyboardType || currentInputView != target.inputView))
    {
        [target resignFirstResponder];
        [target becomeFirstResponder];
    }
}

-(void)updateButtonItem
{
    {
        NSString *keyboardType = _keyboardTypes[_keyboardTypeIndex];
        [self.class setKeyboardType:keyboardType forTarget:_target];
    }
    
    {
        NSString *title = nil;
        
        NSString *keyboardType = _keyboardTypes[(_keyboardTypeIndex + 1) % _keyboardTypes.count];
        
        if ([keyboardType isEqualToString:kOCGKeyboardTypeDefault])
        {
            title = @"Default";
        }
        else if ([keyboardType isEqualToString:kOCGKeyboardTypeASCIICapable])
        {
            title = @"\uF001";
        }
        else if ([keyboardType isEqualToString:kOCGKeyboardTypeNumbersAndPunctuation])
        {
            title = @"NumbersAndPunctuation";
        }
        else if ([keyboardType isEqualToString:kOCGKeyboardTypeURL])
        {
            title = @"URL";
        }
        else if ([keyboardType isEqualToString:kOCGKeyboardTypeNumberPad])
        {
            title = [POSInputView layoutTitle];
        }
        else if ([keyboardType isEqualToString:kOCGKeyboardTypePhonePad])
        {
            title = @"PhonePad";
        }
        else if ([keyboardType isEqualToString:kOCGKeyboardTypeNamePhonePad])
        {
            title = @"NamePhonePad";
        }
        else if ([keyboardType isEqualToString:kOCGKeyboardTypeEmailAddress])
        {
            title = @"EmailAddress";
        }
        else if ([keyboardType isEqualToString:kOCGKeyboardTypeDate])
        {
            title = @"\uF00A";
        }
        else if ([keyboardType isEqualToString:kOCGKeyboardTypeScan])
        {
            if (BarcodeScannerController.sharedInstance.sourceType == SMScannerSourceTypeInternal)
            {
                title = @"\uF003";
            }
            else if (BarcodeScannerController.sharedInstance.sourceType == SMScannerSourceTypeExternal)
            {
                title = @"\uF004";
            }
            else
            {
                title = @"\uF004";
            }
        }
        
        if (title == nil)
        {
            title = [keyboardType stringByReplacingOccurrencesOfString:@"kOCGKeyboardType" withString:@""];
        }
        
        
        self.title = title;
    }
 }

-(void)didTapButtonItem
{
    _keyboardTypeIndex = (_keyboardTypeIndex + 1) % _keyboardTypes.count;
    [self updateButtonItem];
}

@end

@implementation _MessageInputView

-(instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if  (self)
    {
        _textLabel = [[UILabel alloc] initWithFrame:self.bounds];
        _textLabel.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        _textLabel.textAlignment = NSTextAlignmentCenter;
        _textLabel.backgroundColor = [UIColor clearColor];
        
        [self addSubview:_textLabel];
    }
    return self;
}

-(UIKeyboardAppearance)keyboardAppearance
{
    id responder = [self nextResponder];
    
    while (responder)
    {
        if ([responder respondsToSelector:@selector(keyboardAppearance)])
        {
            return [responder keyboardAppearance];
        }
        
        responder = [responder nextResponder];
    }
    return UIKeyboardAppearanceLight;
}

-(void)willMoveToWindow:(UIWindow *)newWindow
{
    if (newWindow)
    {
        if (UIDevice.currentDevice.systemVersion.floatValue >= 7.0 && [self keyboardAppearance] == UIKeyboardAppearanceDark)
        {
            _textLabel.textColor = [UIColor colorWithWhite:0.85 alpha:1.0];
        }
        else
        {
            _textLabel.textColor = [UIColor colorWithWhite:0.20 alpha:1.0];
        }

    }
}

@end

@implementation UITextField (OCGKeyboardTypeButtonItem)
@end

@implementation UITextView (OCGKeyboardTypeButtonItem)
@end
