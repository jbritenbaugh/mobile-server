//
//  OCGProxy.m
//  Proxy
//
//  Created by John Scott on 01/05/2014.
//  Copyright (c) 2014 Omnico Group. All rights reserved.
//

#import "OCGProxy.h"

@implementation OCGProxy
{
    id _target;
}

+(id)proxyWithTarget:(id)target
{
    OCGProxy *proxy = [self alloc];
    if (proxy)
    {
        proxy->_target = target;
   }
    return proxy;
}


-(BOOL)respondsToSelector:(SEL)aSelector
{
    return [_target respondsToSelector:aSelector];
}

-(NSMethodSignature*)methodSignatureForSelector:(SEL)aSelector
{
    return [_target methodSignatureForSelector:aSelector];
}

- (void) forwardInvocation:(NSInvocation *)invocation;
{
    [self logInvocation:invocation];
    [invocation invokeWithTarget:_target];
}

- (void)logInvocation:(NSInvocation *)invocation
{
    DebugLog2(@"{}: {}", _target, NSStringFromSelector([invocation selector]));
}

@end
