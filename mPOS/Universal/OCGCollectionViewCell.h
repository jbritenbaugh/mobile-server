//
//  OCGCollectionViewCell.h
//  mPOS
//
//  Created by John Scott on 10/07/2014.
//  Copyright (c) 2014 Clarity. All rights reserved.
//

#import <UIKit/UIKit.h>

id _OCGCollectionViewCellCast(id self, SEL _cmd, Class aClass, NSObject* anObject);

#define CAST(aClass, anObject) ((aClass*)_OCGCollectionViewCellCast(self, _cmd, [aClass class], anObject))

typedef NS_ENUM(NSUInteger, OCGCollectionViewCellDetailRequirement)
{
    OCGCollectionViewCellDetailRequirementNone,
    OCGCollectionViewCellDetailRequirementRequired, // Suffixes a red star to to the textLabel content.
};

typedef NS_ENUM(NSUInteger, OCGCollectionViewCellDetailOptions)
{
    OCGCollectionViewCellDetailOptionNone = 0,
    OCGCollectionViewCellDetailOptionUseTextLabelBaseline = 1 << 0, // Sets detailView.baseline == textLabel.baseline
};

@interface OCGCollectionViewCell : UICollectionViewCell

+(NSString*)reuseIdentifierForDetailClass:(Class)detailClass
                                  options:(OCGCollectionViewCellDetailOptions)options;

+(NSString*)registerForDetailClass:(Class)detailClass
                           options:(OCGCollectionViewCellDetailOptions)detailOptions
                    collectionView:(UICollectionView *)collectionView;

- (void)applyLayoutWithdDtailViewClass:(Class)detailViewClass
                         detailOptions:(OCGCollectionViewCellDetailOptions)detailOptions
                             indexPath:(NSIndexPath*)indexPath;

/** @brief Sets the minimum width of the imageView.
 Setting this value to anything but zero will remove the automatic margins
 around the imageView.
 */
@property (nonatomic, assign) CGFloat minimumImageViewWidth;

@property (nonatomic, readonly) UIImageView *imageView;

/** @brief Sets the width of the textLabel.
 where the textLabel can cut off longer text.
 */
@property (nonatomic, assign) CGFloat textLabelWidth;


/** @brief detailView
 detailView is a replacement for detailTextView. The view is created using detailViewWithClass:options:created:
 
 Note that detailTextView can still be used, it just defaults to detailView with a UITextField.
 DO NOT ABUSE THIS.
 */
@property (nonatomic, readonly) UIView* detailView;
-(id)detailViewWithClass:(Class)aClass;

/** @brief Use to set a decal for whether the field is required
 
 */
@property (nonatomic, assign) OCGCollectionViewCellDetailRequirement detailRequirement;

/** @property forceTextLabelDisplay
 *  @brief Forces the display of the text label no matter how wide (or not) the cell is
 */
@property (nonatomic, assign) BOOL forceTextLabelDisplay;

/** @property forceHideTextLabel
 *  @brief Forces hiding the text label no matter how wide (or not) the cell is
 */
@property (nonatomic, assign) BOOL forceHideTextLabel;

@property (nonatomic, readonly, retain) UILabel     *textLabel;
@property (nonatomic, readonly, retain) UILabel     *detailTextLabel;
@property (nonatomic) UITableViewCellAccessoryType    accessoryType;
@property (nonatomic) UITableViewCellSelectionStyle   selectionStyle;
@property (nonatomic, retain) UIView                 *accessoryView;

@end

@interface UIView (OCGCollectionViewCell)

/**@property OCGCollectionViewCell_tableViewCell
 @brief Points to the parent tableView cell if the view is a -[OCGCollectionViewCell detailView].
 */

@property (nonatomic, readonly) OCGCollectionViewCell* OCGCollectionViewCell_collectionViewCell;

@end

/*
 OCGTextFieldCollectionViewCell contains an extra convient method which sets up a UITextField as the detailView.
 
 This replicates the the old -[OCGTextFieldCollectionViewCell valueTextField].
 */

@interface OCGCollectionViewCell (OCGTextFieldCollectionViewCell)

@property(nonatomic, readonly) UITextField *detailTextField;

@end
