//
//  FNKStuff.m
//  Flow
//
//  Created by John Scott on 15/08/2014.
//  Copyright (c) 2014 Omnico Group. All rights reserved.
//

#import "FNKTextField.h"

@implementation FNKTextField
{
    BOOL _resigningFirstResponderCount;
    
    /*
     _window is used to store the the keyWindow at the point we became the first responder. Turns out bad
     things can happen if you resign straight into and alert view with a text inout view.
     */
    __weak UIWindow *_window;
}

-(BOOL)becomeFirstResponder
{
    _window = UIApplication.sharedApplication.keyWindow;
    BOOL result = [super becomeFirstResponder];
    if (!result)
    {
        _window = nil;
    }
    return result;
}

-(BOOL)canBecomeFirstResponder
{
    return YES;
}

-(BOOL)canResignFirstResponder
{
    return YES;
}

-(BOOL)resignFirstResponder
{
    _resigningFirstResponderCount++;
    BOOL result = [super resignFirstResponder];
    _resigningFirstResponderCount--;
    if (result)
    {
        _window = nil;
    }
    return result;
}

-(UIWindow *)window
{
    UIWindow *window = _window ?: UIApplication.sharedApplication.keyWindow;
    return window;
}

-(UIResponder *)nextResponder
{
    UIResponder *responder = _nextResponder ?: _window ?: UIApplication.sharedApplication.keyWindow;
    return responder;
}

-(void)reloadInputViews
{
    if (!_resigningFirstResponderCount)
    {
        [super reloadInputViews];
    }
}

@end
