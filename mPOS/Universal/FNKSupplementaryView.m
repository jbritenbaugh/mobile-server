//
//  FNKLabelSupplementaryView.m
//  Frank
//
//  Created by John Scott on 14/07/2014.
//  Copyright (c) 2014 Omnico Group. All rights reserved.
//

#import "FNKSupplementaryView.h"

#import "FNKCollectionViewLayoutAttributes.h"

#import "FNKCollectionViewLayout.h"

#import "UIImage+CRSUniversal.h"

#import "NSIndexPath+OCGExtensions.h"

@interface UICollectionViewLayout (FNKSupplementaryTextView)

-(void)selectedSupplementaryTextView:(FNKSupplementaryView*)supplementaryTextView;

@end

@interface FNKSupplementaryView ()

@property (nonatomic, strong) NSIndexPath *nodePath;

@end

@implementation FNKSupplementaryView
{
    UIButton *_button;
    UILabel *_label;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
    }
    return self;
}

+ (CGFloat)heightForText:(NSString*)text width:(CGFloat)width;
{
    CGFloat height = 0;
    if (text)
    {
        CGRect rect = CGRectMake(0, 0, width, CGFLOAT_MAX);
        
        rect = CGRectInset(rect, 10, 0);
        
        rect = [text boundingRectWithSize:rect.size
                                  options:NSStringDrawingUsesLineFragmentOrigin
                               attributes:@{NSFontAttributeName : [UIFont systemFontOfSize:14]}
                                  context:nil];
        
        rect = CGRectInset(rect, 0, -10);
        
        height = CGRectGetHeight(rect);
    }
    return height;
}

- (void)applyLayoutAttributes:(FNKCollectionViewLayoutAttributes *)layoutAttributes
{
    if (!_button && ![layoutAttributes.representedElementKind isEqual:FNKSupplementaryKindSectionSeperator])
    {
        _button = [[UIButton alloc] initWithFrame:CGRectInset(self.bounds, 10, 0)];
        _button.titleLabel.font = [UIFont systemFontOfSize:14];
        _button.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
        _button.backgroundColor = [UIColor clearColor];
//        [_button setTitleColor:[UIColor colorWithRed:0.49 green:0.49 blue:0.50 alpha:1] forState:UIControlStateNormal];
        [_button setTitleColor:[UIColor colorWithRed:0.67 green:0.73 blue:0.15 alpha:1.0] forState:UIControlStateSelected];
        [_button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        _button.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        _button.titleLabel.numberOfLines = 0;
        [self addSubview:_button];
        
        [_button addTarget:self
                    action:@selector(selectedSupplementaryTextView:forEvent:)
          forControlEvents:UIControlEventTouchUpInside];
    }
    
    if (!_label && layoutAttributes.item.title && ![layoutAttributes.representedElementKind isEqual:FNKSupplementaryKindSectionSeperator])
    {
        _label = [[UILabel alloc] initWithFrame:CGRectInset(self.bounds, 10, 0)];
        _label.font = [UIFont systemFontOfSize:14];
        _label.textAlignment = NSTextAlignmentLeft;
        _label.backgroundColor = [UIColor clearColor];
        _label.textColor = [UIColor colorWithRed:0.49 green:0.49 blue:0.50 alpha:1];
        _label.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        _label.numberOfLines = 0;
        [self addSubview:_label];
    }

    _button.selected = layoutAttributes.selected;

    if ([layoutAttributes.representedElementKind isEqual:FNKSupplementaryKindSectionHeader])
    {
        _label.text = layoutAttributes.item.title;
    }
    else if ([layoutAttributes.representedElementKind isEqual:FNKSupplementaryKindSectionTab])
    {
        UIImage *image = nil;
        if (layoutAttributes.item.image)
        {
            image = layoutAttributes.item.image;
        }
        
        _button.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;

        if (image)
        {
            [_button setImage:[image imageTintedWithColor:[UIColor colorWithRed:0.49 green:0.49 blue:0.50 alpha:0.3]] forState:UIControlStateNormal];
            [_button setImage:[image imageTintedWithColor:[_button titleColorForState:UIControlStateSelected]] forState:UIControlStateSelected];
            [_button setTitle:nil forState:UIControlStateNormal];
        }
        else if (layoutAttributes.item.title.length)
        {
            [_button setImage:nil forState:UIControlStateNormal];
            [_button setImage:nil forState:UIControlStateSelected];
            [_button setTitle:layoutAttributes.item.title forState:UIControlStateNormal];
        }
        else
        {
            [_button setImage:nil forState:UIControlStateNormal];
            [_button setImage:nil forState:UIControlStateSelected];

            NSString *title = @" ";
#if DEBUG
            title = [layoutAttributes.nodePath OCGExtensions_description];
#endif
            [_button setTitle:title forState:UIControlStateNormal];
        }
        
        
        self.backgroundColor = [UIColor whiteColor];
    }
    else if ([layoutAttributes.representedElementKind isEqual:FNKSupplementaryKindCell])
    {
        UIImage *image = nil;
        if (layoutAttributes.item.image)
        {
            image = layoutAttributes.item.image;
        }
        
        if (image)
        {
            [_button setImage:[image imageTintedWithColor:[UIColor colorWithRed:0.49 green:0.49 blue:0.50 alpha:0.3]] forState:UIControlStateNormal];
            [_button setImage:[image imageTintedWithColor:[_button titleColorForState:UIControlStateSelected]] forState:UIControlStateSelected];
            [_button setTitle:nil forState:UIControlStateNormal];
            _button.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
        }
        else if (layoutAttributes.item.title.length)
        {
            [_button setImage:nil forState:UIControlStateNormal];
            [_button setImage:nil forState:UIControlStateSelected];
            [_button setTitle:layoutAttributes.item.title forState:UIControlStateNormal];
            _button.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
        }
        else
        {
            [_button setImage:nil forState:UIControlStateNormal];
            [_button setImage:nil forState:UIControlStateSelected];
//            [_button setTitle:[layoutAttributes.nodePath OCGExtensions_description] forState:UIControlStateNormal];
        }
        
        
        self.backgroundColor = [UIColor whiteColor];
    }
    else if ([layoutAttributes.representedElementKind isEqual:FNKSupplementaryKindSectionFooter])
    {
        _label.text = layoutAttributes.item.title;
    }
    else if ([layoutAttributes.representedElementKind isEqual:FNKSupplementaryKindSectionSeperator])
    {
        self.backgroundColor = [UIColor colorWithRed:0.89 green:0.89 blue:0.90 alpha:1.000];
//#warning Remove when finished.
//        self.backgroundColor = [UIColor redColor];
    }
    
    self.userInteractionEnabled = _button.titleLabel.text.length || _button.imageView.image;

    _nodePath = layoutAttributes.nodePath;
}

-(void)selectedSupplementaryTextView:(UIButton*)button forEvent:(UIEvent *)event
{
    [UIApplication.sharedApplication sendAction:@selector(selectedSupplementaryTextView:)
                                             to:self.nextResponder
                                           from:self
                                       forEvent:event];
}

@end

@implementation UICollectionView (FNKSupplementaryTextView)

-(void)selectedSupplementaryTextView:(FNKSupplementaryView*)supplementaryTextView
{
    if ([self.collectionViewLayout respondsToSelector:@selector(setSelectedNodePath:)])
    {
        [(id)self.collectionViewLayout setSelectedNodePath:supplementaryTextView.nodePath];
    }
}

@end

