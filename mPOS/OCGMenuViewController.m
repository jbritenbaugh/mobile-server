//
//  OCGMenuViewController.m
//  MenuTest
//
//  Created by Antonio Strijdom on 14/03/2013.
//  Copyright (c) 2013 Antonio Strijdom. All rights reserved.
//

#import "OCGMenuViewController.h"
#import "CRSSkinning.h"

@interface OCGMenuViewController () <UINavigationControllerDelegate>
@property (nonatomic, strong) NSMutableDictionary *cellRegistryDict;
@property (nonatomic, assign) CGRect initialRect;
@property (nonatomic, strong) UIBarButtonItem *totalItem;
@property (nonatomic, strong) UIBarButtonItem *closeItem;
@property (nonatomic, strong) UILabel *totalLabel;
@property (nonatomic, strong) UISwipeGestureRecognizer *leftSwipeGesture;
@property (nonatomic, strong) UISwipeGestureRecognizer *rightSwipeGesture;
- (void) showGestureForSwipe:(UISwipeGestureRecognizer *)swipe;
- (OCGMenuPageViewController *) menuPageViewControllerForMenu:(id)menu;
@end

@implementation OCGMenuViewController

#pragma mark - Private

static NSTimeInterval kSwipeAnimationDuration = 0.25;

- (void) showGestureForSwipe:(UISwipeGestureRecognizer *)swipe
{
    // only respond to left or right gestures
    UISwipeGestureRecognizerDirection direction = swipe.direction;
    if ((direction == UISwipeGestureRecognizerDirectionLeft) ||
        (direction == UISwipeGestureRecognizerDirectionRight)) {
        // show or hide the view
        if (self.view.hidden) {
            [self showMenuViewAnimated:YES
                         fromDirection:direction];
        } else {
            // only allow swipe off in the opposite direction we displayed from
            if (direction != self.displayDirection) {
                [self hideMenuViewAnimated:YES
                               inDirection:direction];
            }
        }
    }
}

- (OCGMenuPageViewController *) menuPageViewControllerForMenu:(id)menu
{
    // TODO : cache pages?
    
    // setup a new page collection view
    OCGMenuPageViewController *result =
    [[OCGMenuPageViewController alloc] initWithMenu:menu
                                   withCellReuseIds:self.cellRegistryDict];
   
    return result;
}

#pragma mark - Properties

@synthesize rootMenu = _rootMenu;
- (void) setRootMenu:(MPOSSoftKeyContext *)rootMenu
{
    _rootMenu = rootMenu;
    // show the root menu
    if (self.rootMenu) {
        OCGMenuPageViewController *rootPage = [self menuPageViewControllerForMenu:self.rootMenu];
        [self setViewControllers:@[rootPage]
                        animated:NO];
    }
}

@synthesize total = _total;
- (void) setTotal:(NSString *)total
{
    _total = total;
    if (total == nil) {
        self.totalLabel.text = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁑󠁩󠁉󠁺󠁮󠁕󠁡󠁍󠁕󠁆󠁉󠁯󠁧󠁕󠁌󠁖󠁉󠁕󠀱󠁳󠁒󠁪󠁇󠀹󠁓󠀯󠁫󠁿*/ @"0.00", nil);
    } else {
        self.totalLabel.text = _total;
    }
}

#pragma mark - Init

- (id) initWithRootMenu:(MPOSSoftKeyContext *)menu
                 inRect:(CGRect)rect
{
    self = [super init];
    if (self) {
        self.rootMenu = menu;
        if ([UIDevice currentDevice].systemVersion.floatValue >= 7.0)
        {
            rect.origin.y += 20;
            rect.size.height -= 20;
        }
        self.initialRect = rect;
        self.menuNavigationStyle = OCGMenuViewControllerNavigationStyleDefault;
    }
    return self;
}

#pragma mark - UIViewController

- (void) viewDidLoad
{
    [super viewDidLoad];
    
    self.view.tag = kMenuSkinningTag;
    self.view.frame = self.initialRect;
    
    // setup the swipe gestures
    self.leftSwipeGesture =
    [[UISwipeGestureRecognizer alloc] initWithTarget:self
                                              action:@selector(showGestureForSwipe:)];
    self.leftSwipeGesture.direction = UISwipeGestureRecognizerDirectionLeft;
    self.rightSwipeGesture =
    [[UISwipeGestureRecognizer alloc] initWithTarget:self
                                              action:@selector(showGestureForSwipe:)];
    self.rightSwipeGesture.direction = UISwipeGestureRecognizerDirectionRight;
    // register the gestures with our view
    [self.view addGestureRecognizer:self.leftSwipeGesture];
    [self.view addGestureRecognizer:self.rightSwipeGesture];
    
    // default to hidden
    [self hideMenuViewAnimated:NO
                   inDirection:UISwipeGestureRecognizerDirectionLeft];
    
    // build the total view
    CGRect totalFrame = CGRectMake(0.0, 0.0,
                                   self.toolbar.bounds.size.width - 24.0f,
                                   20.0f);
    UIView *totalView = [[UIView alloc] initWithFrame:totalFrame];
    totalView.clipsToBounds = YES;
    totalView.backgroundColor = [UIColor clearColor];
    UILabel *totalTitleLabel = [[UILabel alloc] init];
    totalTitleLabel.translatesAutoresizingMaskIntoConstraints = NO;
    totalTitleLabel.backgroundColor = [UIColor clearColor];
    totalTitleLabel.text = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁱󠀰󠁕󠁥󠁳󠁡󠁷󠀲󠁵󠁭󠁣󠀰󠁦󠁮󠁍󠁯󠁺󠁐󠁣󠁘󠀶󠁬󠁩󠁓󠁢󠀴󠁯󠁿*/ @"sales total", nil);
    [totalView addSubview:totalTitleLabel];
    self.totalLabel = [[UILabel alloc] init];
    self.totalLabel.translatesAutoresizingMaskIntoConstraints = NO;
    self.totalLabel.backgroundColor = [UIColor clearColor];
//    self.totalLabel.tag = kSubTotalSkinningTag;
    self.totalLabel.textAlignment = NSTextAlignmentRight;
    self.totalLabel.text = @"0.00";
    [totalView addSubview:self.totalLabel];
    NSDictionary *viewDict = @{ @"totalTitleLabel": totalTitleLabel,
                                @"totalLabel": self.totalLabel };
    [totalView addConstraints:
     [NSLayoutConstraint constraintsWithVisualFormat:@"|-0-[totalTitleLabel]-[totalLabel]-0-|"
                                             options:0
                                             metrics:nil
                                               views:viewDict]];
    [totalView addConstraints:
     [NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[totalTitleLabel]-0-|"
                                             options:0
                                             metrics:nil
                                               views:viewDict]];
    [totalView addConstraints:
     [NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[totalLabel]-0-|"
                                             options:0
                                             metrics:nil
                                               views:viewDict]];
    // setup the tool bar
    self.totalItem = [[UIBarButtonItem alloc] initWithCustomView:totalView];
    self.totalItem.width = totalFrame.size.width;
    self.toolbarHidden = NO;
    
    // setup the navigation controller
    // show the root menu
    if (self.rootMenu) {
        OCGMenuPageViewController *rootPage = [self menuPageViewControllerForMenu:self.rootMenu];
        [self setViewControllers:@[rootPage]
                        animated:NO];
    }
    self.delegate = self;
    
    self.closeItem =
    [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemStop
                                                  target:self
                                                  action:@selector(closeMenu:)];
    
    [[CRSSkinning currentSkin] applyViewSkin:self];
}

#pragma mark - UINavigationControllerDelegate

#pragma mark - OCGMenuViewControllerDataSource

- (OCGMenuItemCell *) menuViewController:(OCGMenuViewController *)controller
                         cellForMenuItem:(MPOSSoftKey *)menuItem
                                  onPage:(OCGMenuPageViewController *)page
{
    OCGMenuItemCell *result = nil;
    
    if (self.menuDataSource) {
        if ([self.menuDataSource respondsToSelector:@selector(menuViewController:cellForMenuItem:onPage:)]) {
            result = [self.menuDataSource menuViewController:self
                                             cellForMenuItem:menuItem
                                                      onPage:page];
        }
    }

    return result;
}

#pragma mark - OCGMenuViewControllerDelegate

- (void) menuViewController:(OCGMenuViewController *)controller
           didHighlightItem:(MPOSSoftKey *)menuItem
                     onPage:(OCGMenuPageViewController *)page
{
    if (self.menuDelegate) {
        if ([self.menuDelegate respondsToSelector:@selector(menuViewController:didHighlightItem:onPage:)]) {
            [self.menuDelegate menuViewController:self
                                 didHighlightItem:menuItem
                                           onPage:page];
        }
    }
}

- (void) menuViewController:(OCGMenuViewController *)controller
              didSelectItem:(MPOSSoftKey *)menuItem
                     onPage:(OCGMenuPageViewController *)page
{
    if (self.menuDelegate) {
        if ([self.menuDelegate respondsToSelector:@selector(menuViewController:didSelectItem:onPage:)]) {
            [self.menuDelegate menuViewController:self
                                    didSelectItem:menuItem
                                           onPage:page];
        }
    }
}

- (void) menuViewController:(OCGMenuViewController *)controller
         didUnhighlightItem:(MPOSSoftKey *)menuItem
                     onPage:(OCGMenuPageViewController *)page
{
    if (self.menuDelegate) {
        if ([self.menuDelegate respondsToSelector:@selector(menuViewController:didUnhighlightItem:onPage:)]) {
            [self.menuDelegate menuViewController:self
                               didUnhighlightItem:menuItem
                                           onPage:page];
        }
    }
}

#pragma mark - Methods

- (void) showMenuViewAnimated:(BOOL)animated
                fromDirection:(UISwipeGestureRecognizerDirection)direction
{
    if (self.view.hidden) {
        if (animated) {
            // make a note of the direction we are appearing from
            self.displayDirection = direction;
            // get the view rect
            CGRect viewRect = self.initialRect;
            if (direction == UISwipeGestureRecognizerDirectionRight) {
                // start from off screen right
                self.view.transform =
                CGAffineTransformMakeTranslation(viewRect.size.width * -1,
                                                 0.0f);
            } else if (direction == UISwipeGestureRecognizerDirectionLeft) {
                // start from off screen left
                self.view.transform =
                CGAffineTransformMakeTranslation(viewRect.size.width,
                                                 0.0f);
            }
            // show the view
            self.view.hidden = NO;
            [self.visibleViewController viewWillAppear:YES];
            // animate the view on
            [UIView animateWithDuration:kSwipeAnimationDuration
                             animations:^{
                                 self.view.transform = CGAffineTransformIdentity;
                             }];
        } else {
            self.view.hidden = NO;
            self.view.transform = CGAffineTransformIdentity;
        }
    }
}

- (void) hideMenuViewAnimated:(BOOL)animated
                  inDirection:(UISwipeGestureRecognizerDirection)direction
{
    if (!self.view.hidden) {
        if (animated) {
            if (self.displayDirection != direction) {
                // get the view rect
                CGRect viewRect = self.initialRect;
                // animate the view off
                [UIView animateWithDuration:kSwipeAnimationDuration
                                 animations:^{
                     if (direction == UISwipeGestureRecognizerDirectionLeft) {
                         self.view.transform =
                         CGAffineTransformMakeTranslation(viewRect.size.width * -1,
                                                          0.0f);
                     } else if (direction == UISwipeGestureRecognizerDirectionRight) {
                         self.view.transform =
                         CGAffineTransformMakeTranslation(viewRect.size.width,
                                                          0.0f);
                     }
                 }
                 completion:^(BOOL finished) {
                     self.view.hidden = YES;
                 }];
            }
        } else {
            self.view.hidden = YES;
        }
    }
}

- (void) registerSwipeGesture:(UIView *)view
{
    [view addGestureRecognizer:self.leftSwipeGesture];
    [view addGestureRecognizer:self.rightSwipeGesture];
}

- (void) unregisterSwipeGesture:(UIView *)view
{
    [view removeGestureRecognizer:self.leftSwipeGesture];
    [view removeGestureRecognizer:self.rightSwipeGesture];
}

- (void) registerMenuCellFromXib:(UINib *)nibOrNil
                         orClass:(Class)classOrNil
                     withReuseId:(NSString *)reuseId
{
    if (self.cellRegistryDict == nil) {
        self.cellRegistryDict = [[NSMutableDictionary alloc] init];
    }
    if (nibOrNil != nil) {
        [self.cellRegistryDict setValue:nibOrNil
                                 forKey:reuseId];
    } else if (classOrNil != nil) {
        [self.cellRegistryDict setValue:classOrNil
                                 forKey:reuseId];
    }
}

- (void) pushMenu:(MPOSSoftKey *)menu
         animated:(BOOL)animated
{
    OCGMenuPageViewController *pageViewController = [self menuPageViewControllerForMenu:menu];
    if (self.menuNavigationStyle == OCGMenuViewControllerNavigationStyleDefault) {
        [self pushViewController:pageViewController
                        animated:animated];
    } else {
        [self setViewControllers:@[pageViewController]
                        animated:animated];
    }
}

- (void) popMenuAnimated:(BOOL)animated
{
    [self popViewControllerAnimated:animated];
}

- (void) popToRootMenuAnimated:(BOOL)animated
{
    [self popToRootViewControllerAnimated:animated];
}

- (void) closeMenu:(id)sender
{
    if (self.displayDirection == UISwipeGestureRecognizerDirectionLeft) {
        [self hideMenuViewAnimated:YES
                       inDirection:UISwipeGestureRecognizerDirectionRight];
    } else {
        [self hideMenuViewAnimated:YES
                       inDirection:UISwipeGestureRecognizerDirectionLeft];
    }
}

@end
