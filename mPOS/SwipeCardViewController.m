//
//  SwipeCardViewController.m
//  mPOS
//
//  Created by Antonio Strijdom on 25/06/2014.
//  Copyright (c) 2014 Clarity. All rights reserved.
//

#import "SwipeCardViewController.h"
#import "UIImage+OCGExtensions.h"
#import "OCGPeripheralManager.h"
#import "CRSSkinning.h"

@interface SwipeCardViewController ()
- (IBAction) cancelTapped:(id)sender;
- (void) peripheralNotification:(NSNotification *)note;
@property (nonatomic, strong) UIImageView *scanImage;
@property (nonatomic, strong) UILabel *messageLabel;
@end

@implementation SwipeCardViewController

#pragma mark - Private

- (IBAction) cancelTapped:(id)sender
{
    [[OCGPeripheralManager sharedInstance].currentScannerDevice stopMSRSwipe];
    [self dismissViewControllerAnimated:YES
                             completion:nil];
}

- (void) peripheralNotification:(NSNotification *)note
{
    [self dismissViewControllerAnimated:YES
                             completion:nil];
}

#pragma mark - UIViewController

- (void) viewDidLoad
{
    [super viewDidLoad];
	// UI
    self.view.backgroundColor = [UIColor whiteColor];
    // image
    self.scanImage = [[UIImageView alloc] init];
    self.scanImage.translatesAutoresizingMaskIntoConstraints = NO;
    [self.view addSubview:self.scanImage];
    [self.scanImage constrain:@"left = left" to:self.view];
    [self.scanImage constrain:@"right = right" to:self.view];
    [self.scanImage constrain:@"height = 50" to:nil];
    [self.scanImage constrain:@"centerY = centerY - 50" to:self.view];
    self.scanImage.contentMode = UIViewContentModeBottom;
    self.scanImage.image = [UIImage OCGExtensions_imageNamed:@"SWIPE_DATA"];
    // message label
    self.messageLabel = [[UILabel alloc] init];
    self.messageLabel.translatesAutoresizingMaskIntoConstraints = NO;
    [self.view addSubview:self.messageLabel];
    [self.messageLabel constrain:@"left = left" to:self.view];
    [self.messageLabel constrain:@"right = right" to:self.view];
    [self.messageLabel constrain:@"top = bottom + 5" to:self.scanImage];
    [self.messageLabel constrain:@"height = 21" to:nil];
    self.messageLabel.textAlignment = NSTextAlignmentCenter;
    
    self.navigationItem.leftBarButtonItem =
    [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel
                                                  target:self
                                                  action:@selector(cancelTapped:)];
}

- (void) viewWillAppear:(BOOL)animated
{
    self.title = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁷󠁗󠁩󠀯󠁇󠀹󠁥󠁹󠀵󠁸󠁯󠀲󠁍󠁴󠀸󠀳󠀲󠁐󠁰󠁤󠁘󠀸󠁃󠀲󠀵󠁕󠁙󠁿*/ @"Swipe card...", nil);
    self.messageLabel.text = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁷󠁗󠁩󠀯󠁇󠀹󠁥󠁹󠀵󠁸󠁯󠀲󠁍󠁴󠀸󠀳󠀲󠁐󠁰󠁤󠁘󠀸󠁃󠀲󠀵󠁕󠁙󠁿*/ @"Swipe card...", nil);
    // listen for notifications
    [[OCGPeripheralManager sharedInstance] registerForNotifications:self
                                                       withSelector:@selector(peripheralNotification:)];
    [[CRSSkinning currentSkin] applyViewSkin:self];
    [super viewWillAppear:animated];
}

- (void) viewWillDisappear:(BOOL)animated
{
    [[OCGPeripheralManager sharedInstance] unregisterForNotifications:self];
    [super viewWillDisappear:animated];
}

@end
