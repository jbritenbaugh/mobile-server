//
//  ItemDetailQuantityCell.h
//  mPOS
//
//  Created by Antonio Strijdom on 23/05/2013.
//  Copyright (c) 2013 Clarity. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CRSGlossyButton.h"

@interface ItemDetailQuantityCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *quantityDescLabel;
@property (weak, nonatomic) IBOutlet UILabel *quantityLabel;
@property (weak, nonatomic) IBOutlet CRSGlossyButton *bodyView;

@end
