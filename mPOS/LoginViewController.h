//
//  LoginViewController.h
//  mPOS
//
//  Created by Meik Schuetz on 05/09/2012.
//  Copyright (c) 2012 Clarity. All rights reserved.
//

#import <UIKit/UIKit.h>

/** @file LoginViewController.h */

@protocol LoginViewControllerDelegate <NSObject>
@optional

/** @brief Called whenever the modal login view controller should be dismissed.
 *  @param sender The LoginViewController that send the message.
 */
- (void) dismissModalLoginViewController:(id)sender;

@end

@interface LoginViewController : UICollectionViewController

/** @property delegate
 *  @brief A reference to the delegate to which we'll send notifications.
 */
@property (weak, nonatomic) id<LoginViewControllerDelegate> delegate;

@end
