//
//  OCGMenuPageViewController.m
//  MenuTest
//
//  Created by Antonio Strijdom on 18/03/2013.
//  Copyright (c) 2013 Antonio Strijdom. All rights reserved.
//

#import "OCGMenuPageViewController.h"
#import "CRSSkinning.h"
#import "RESTController.h"

#define MAX_FONT_SIZE (25)
#define MIN_FONT_SIZE (12.0)

@interface OCGMenuPageViewController ()
@property (nonatomic, strong) NSString *menuTitle;
@property (nonatomic, assign) NSInteger numberOfColumns;
@property (nonatomic, assign) NSInteger numberOfRows;
@property (nonatomic, strong) UIFont *displayKeyFont;
@property (nonatomic, strong) UIFont *normalKeyFont;
@property (nonatomic, assign) CGSize keySize;
- (SoftKey *) menuKeyForIndexPath:(NSIndexPath *)indexPath;
- (UIFont *) sizeLabelFontToFitText:(NSString *)text
                             inSize:(CGSize)size;
- (UIColor *) colorFromRGBString:(NSString *)string;
@end

@implementation OCGMenuPageViewController

#pragma mark - Private

- (SoftKey *) menuKeyForIndexPath:(NSIndexPath *)indexPath
{
    SoftKey *result = nil;
    
    if (self.menu) {
        NSInteger menuItemIndex = indexPath.item;
        if (menuItemIndex < self.keys.count) {
            result = [self.keys objectAtIndex:menuItemIndex];
        }
    }
    
    return result;
}

- (UIFont *) sizeLabelFontToFitText:(NSString *)text
                             inSize:(CGSize)size
{
    UIFont *result = [UIFont systemFontOfSize:MAX_FONT_SIZE];
    
    if (text != nil) {
        CGFloat fontSize = [result pointSize];
        UIFont *newFont = result;
        NSStringDrawingContext *context = [[NSStringDrawingContext alloc] init];
        context.minimumScaleFactor = 1.0f;
        CGRect textBounds = [text boundingRectWithSize:CGSizeMake(CGFLOAT_MAX, CGFLOAT_MAX)
                                               options:NSStringDrawingUsesLineFragmentOrigin
                                            attributes:@{ NSFontAttributeName : newFont }
                                               context:context];
        CGSize newSize = CGSizeMake(ceil(textBounds.size.width), ceil(textBounds.size.height));
        while (((newSize.height > size.height) || (newSize.width > size.width)) &&
               (fontSize > MIN_FONT_SIZE)) {
            fontSize--;
            newFont = [UIFont fontWithName:result.fontName
                                      size:fontSize];
            textBounds = [text boundingRectWithSize:CGSizeMake(CGFLOAT_MAX, CGFLOAT_MAX)
                                            options:NSStringDrawingUsesLineFragmentOrigin
                                         attributes:@{ NSFontAttributeName : newFont }
                                            context:context];
            newSize = CGSizeMake(ceil(textBounds.size.width), ceil(textBounds.size.height));
        }
        
        result = [UIFont systemFontOfSize:fontSize];
    }
    
    return result;
}

- (UIColor *) colorFromRGBString:(NSString *)string
{
    UIColor *result = nil;
    
    if ((string != nil) && (![string isEqualToString:@""])) {
        // split the colour into RGB
        NSRange compRange = NSMakeRange(0, 2);
        NSString *r = [@"0x" stringByAppendingString:[string substringWithRange:compRange]];
        compRange.location = 2;
        NSString *g = [@"0x" stringByAppendingString:[string substringWithRange:compRange]];
        compRange.location = 4;
        NSString *b = [@"0x" stringByAppendingString:[string substringWithRange:compRange]];
        // convert
        NSScanner *rScanner = [NSScanner scannerWithString:r];
        float rValue = 0;
        NSScanner *gScanner = [NSScanner scannerWithString:g];
        float gValue = 0;
        NSScanner *bScanner = [NSScanner scannerWithString:b];
        float bValue = 0;
        if ([rScanner scanHexFloat:&rValue] &&
            [gScanner scanHexFloat:&gValue] &&
            [bScanner scanHexFloat:&bValue]) {
            // build the colour
            result = [UIColor colorWithRed:rValue / 255.0
                                     green:gValue / 255.0
                                      blue:bValue / 255.0
                                     alpha:1.0];
        }
    }
    
    return result;
}


- (CGSize)sizeForItemMenuItem:(SoftKey *)menuItem
{
    CGSize result = self.keySize;
    
    if (self.menuDelegate != nil) {
        if ([self.menuDelegate respondsToSelector:@selector(cellWidthForMenuItem:onPage:)]) {
            // get the menu item for this cell
            CGFloat width =
            [self.menuDelegate cellWidthForMenuItem:menuItem
                                             onPage:self];
            result = CGSizeMake(width, self.keySize.height);
        }
    }
    
    return result;
}

#pragma mark - Properties

@synthesize menu = _menu;
- (void) setMenu:(SoftKey *)menu
{
    _menu = menu;
    if (_menu) {
        [self calculatePageMetricsAndMenu];
    }
    [self.collectionView reloadData];
}

@synthesize calculatedKeySize;
 - (CGSize) calculatedKeySize
{
    return self.keySize;
}

@synthesize horizontalScrolling = _horizontalScrolling;
- (void) setHorizontalScrolling:(BOOL)horizontalScrolling
{
    _horizontalScrolling = horizontalScrolling;
    if (_horizontalScrolling) {
        self.collectionView.showsHorizontalScrollIndicator = YES;
        self.collectionView.showsVerticalScrollIndicator = NO;
    } else {
        self.collectionView.showsHorizontalScrollIndicator = NO;
        self.collectionView.showsVerticalScrollIndicator = YES;
    }
}

#pragma mark - Init

- (id) initWithCollectionViewLayout:(UICollectionViewLayout *)layout
                               menu:(SoftKey *)menu
                   withCellReuseIds:(NSDictionary *)cellDict
{
    self = [super initWithCollectionViewLayout:layout];
    if (self) {
        self.cellRegistryDict = cellDict;
        self.menu = menu;
        self.horizontalScrolling = NO;
    }
    
    return self;
}

- (id) initWithMenu:(SoftKey *)menu
   withCellReuseIds:(NSDictionary *)cellDict
{
    UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
    self = [self initWithCollectionViewLayout:layout
                                         menu:menu
                             withCellReuseIds:cellDict];
    return self;
}

- (id) initWithMenu:(id)menu
   withCellReuseIds:(NSDictionary *)cellDict
     withDataSource:(id<OCGMenuPageViewControllerDataSource>)menuDataSource
        andDelegate:(id<OCGMenuPageViewControllerDelegate>)menuDelegate
{
    self = [self initWithMenu:menu
             withCellReuseIds:cellDict];
    self.menuDataSource = menuDataSource;
    self.menuDelegate = menuDelegate;
    [self calculatePageMetricsAndMenu];
    return self;
}

#pragma mark - UIViewController

- (void) viewDidLoad
{    
    [super viewDidLoad];
    
    self.title = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁫󠁎󠁤󠁤󠁕󠁙󠁴󠁆󠁒󠁐󠁥󠀫󠁤󠁊󠁯󠁇󠁥󠀴󠁱󠀱󠁦󠁭󠁊󠁚󠁦󠁘󠁉󠁿*/ @"Menu", nil);
    
    self.collectionView.directionalLockEnabled = YES;
    if (self.horizontalScrolling) {
        self.collectionView.showsHorizontalScrollIndicator = YES;
        self.collectionView.showsVerticalScrollIndicator = NO;
    } else {
        self.collectionView.showsHorizontalScrollIndicator = NO;
        self.collectionView.showsVerticalScrollIndicator = YES;
    }
    
    self.collectionView.backgroundColor = [UIColor clearColor];
    self.collectionView.delaysContentTouches = NO;

    // calculate cell sizes
    self.keySize = CGSizeZero;
    if (self.menu) {
        [self calculatePageMetricsAndMenu];
    }
    
    // register cell reuse ids
    if (self.cellRegistryDict != nil) {
        for (NSString *reuseId in self.cellRegistryDict.allKeys) {
            id nibOrClass = [self.cellRegistryDict objectForKey:reuseId];
            if ([nibOrClass isKindOfClass:[UINib class]]) {
                [self.collectionView registerNib:nibOrClass
                      forCellWithReuseIdentifier:reuseId];
            } else {
                [self.collectionView registerClass:nibOrClass
                        forCellWithReuseIdentifier:reuseId];
            }
        }
    }
    [self.collectionView registerClass:[UICollectionViewCell class]
            forCellWithReuseIdentifier:@"MenuPageEmptyKeyReuseId"];
    
    // setup collection view layout constraints
    self.collectionView.translatesAutoresizingMaskIntoConstraints = NO;
    // top
    [self.view addConstraint:
     [NSLayoutConstraint constraintWithItem:self.collectionView
                                  attribute:NSLayoutAttributeTop
                                  relatedBy:NSLayoutRelationEqual
                                     toItem:self.view
                                  attribute:NSLayoutAttributeTop
                                 multiplier:1.0f
                                   constant:0.0f]
     ];
    // left
    [self.view addConstraint:
     [NSLayoutConstraint constraintWithItem:self.collectionView
                                  attribute:NSLayoutAttributeLeft
                                  relatedBy:NSLayoutRelationEqual
                                     toItem:self.view
                                  attribute:NSLayoutAttributeLeft
                                 multiplier:1.0f
                                   constant:0.0f]
     ];
    // bottom
    [self.view addConstraint:
     [NSLayoutConstraint constraintWithItem:self.collectionView
                                  attribute:NSLayoutAttributeBottom
                                  relatedBy:NSLayoutRelationEqual
                                     toItem:self.view
                                  attribute:NSLayoutAttributeBottom
                                 multiplier:1.0f
                                   constant:0.0f]
     ];
    // right
    [self.view addConstraint:
     [NSLayoutConstraint constraintWithItem:self.collectionView
                                  attribute:NSLayoutAttributeRight
                                  relatedBy:NSLayoutRelationEqual
                                     toItem:self.view
                                  attribute:NSLayoutAttributeRight
                                 multiplier:1.0f
                                   constant:0.0f]
     ];
}

- (void) viewWillAppear:(BOOL)animated
{
    [[CRSSkinning currentSkin] applyViewSkin:self];
    [super viewWillAppear:animated];
    
    // setup the navigation controller and tool bar
    if (self.navigationController != nil) {
        self.navigationItem.rightBarButtonItem = nil;
        if ((self.menuDataSource != nil) &&
            ([self.menuDataSource respondsToSelector:@selector(rightNavigationItemForMenuPageViewController:)])) {
            self.navigationItem.rightBarButtonItem =
            [self.menuDataSource rightNavigationItemForMenuPageViewController:self];
        }
        [self setToolbarItems:nil];
        if ((self.menuDataSource != nil) &&
            ([self.menuDataSource respondsToSelector:@selector(toolbarItemsForMenuPageViewController:)])) {
            [self setToolbarItems:[self.menuDataSource toolbarItemsForMenuPageViewController:self]];
        }
    }
}

#pragma mark - UICollectionViewDataSource

- (NSInteger) collectionView:(UICollectionView *)collectionView
      numberOfItemsInSection:(NSInteger)section
{
    NSInteger result = self.keys.count;
    return result;
}

- (UICollectionViewCell *) collectionView:(UICollectionView *)collectionView
                   cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    OCGMenuCell *result = nil;
    
    // get the menu item for this cell
    SoftKey *menuKeyForCell = [self menuKeyForIndexPath:indexPath];
    if (menuKeyForCell) {
        // check for blank keys
        if (![menuKeyForCell isKindOfClass:[NSNull class]]) {
            // get the cell for this menu key
            if ((self.menuDataSource != nil) &&
                ([self.menuDataSource respondsToSelector:@selector(menuPageViewController:cellForMenuItem:atIndexPath:)])) {
                result = [self.menuDataSource menuPageViewController:self
                                                     cellForMenuItem:menuKeyForCell
                                                         atIndexPath:indexPath];
            }
            if (result) {
                result.font = [menuKeyForCell.keyType isEqualToString:@"DISPLAY_INFO"] ? self.displayKeyFont : self.normalKeyFont;
                // override the skin colour with key background and foreground
                // if specified
                SoftKey *key = (SoftKey *)menuKeyForCell;
                UIColor *foreground = [self colorFromRGBString:key.foregroundColor];
                if (foreground != nil) {
                    result.foregroundColor = foreground;
                }
                UIColor *background = [self colorFromRGBString:key.backgroundColor];
                if (background != nil) {
                    result.backgroundColor = background;
                }
                else
                {
                    result.backgroundColor = self.view.backgroundColor;
                }
            }
        } else {
            // return an empty cell
            result = [self.collectionView dequeueReusableCellWithReuseIdentifier:@"MenuPageEmptyKeyReuseId"
                                                                    forIndexPath:indexPath];
            result.backgroundColor = [UIColor clearColor];
        }
    } else {
        ErrorLog2(@"Could not find cell for indexpath {}", indexPath);
        // return an empty cell
        result = [self.collectionView dequeueReusableCellWithReuseIdentifier:@"MenuPageEmptyKeyReuseId"
                                                                forIndexPath:indexPath];
        result.backgroundColor = [UIColor clearColor];
    }
    
    return result;
}

#pragma mark - UICollectionViewDelegate

- (void) collectionView:(UICollectionView *)collectionView
didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    // get the menu item for this cell
    id menuKeyForCell = [self menuKeyForIndexPath:indexPath];

    if (menuKeyForCell) {
        if (![menuKeyForCell isKindOfClass:[NSNull class]]) {
            if ((self.menuDelegate != nil) &&
                ([self.menuDelegate respondsToSelector:@selector(menuPageViewController:didSelectItem:)])) {
                [self.menuDelegate menuPageViewController:self
                                            didSelectItem:menuKeyForCell];
            }
        }
    }
}

- (void) collectionView:(UICollectionView *)collectionView
didHighlightItemAtIndexPath:(NSIndexPath *)indexPath
{
    // get the menu item for this cell
    SoftKey *menuKeyForCell = [self menuKeyForIndexPath:indexPath];
    
    if (menuKeyForCell) {
        if ((self.menuDelegate != nil) &&
            ([self.menuDelegate respondsToSelector:@selector(menuPageViewController:didHighlightItem:)])) {
            [self.menuDelegate menuPageViewController:self
                                     didHighlightItem:menuKeyForCell];
        }
    }
}

- (void) collectionView:(UICollectionView *)collectionView
didUnhighlightItemAtIndexPath:(NSIndexPath *)indexPath
{
    // get the menu item for this cell
    SoftKey *menuKeyForCell = [self menuKeyForIndexPath:indexPath];
    
    if (menuKeyForCell) {
        if ((self.menuDelegate != nil) &&
            ([self.menuDelegate respondsToSelector:@selector(menuPageViewController:didUnhighlightItem:)])) {
            [self.menuDelegate menuPageViewController:self
                                   didUnhighlightItem:menuKeyForCell];
        }
    }
}

#pragma mark - UICollectionViewDelegateFlowLayout

- (CGSize) collectionView:(UICollectionView *)collectionView
                   layout:(UICollectionViewLayout *)collectionViewLayout
   sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    SoftKey *menuKeyForCell = [self menuKeyForIndexPath:indexPath];
    CGSize result = [self sizeForItemMenuItem:menuKeyForCell];
    return result;
}

- (UIEdgeInsets) collectionView:(UICollectionView *)collectionView
                         layout:(UICollectionViewLayout *)collectionViewLayout
         insetForSectionAtIndex:(NSInteger)section
{
    // get the padding value from the delegate
    CGPoint padding = CGPointZero;
    
    if ((self.menuDelegate != nil) &&
        ([self.menuDelegate respondsToSelector:@selector(cellPaddingForMenuPageViewController:)])) {
        padding = [self.menuDelegate cellPaddingForMenuPageViewController:self];
    }
    UIEdgeInsets result = UIEdgeInsetsMake(padding.y, padding.x, padding.y, padding.x);
    return UIEdgeInsetsZero;
}

- (CGFloat) collectionView:(UICollectionView *)collectionView
                    layout:(UICollectionViewLayout *)collectionViewLayout
minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
    // get the padding value from the delegate
    CGPoint padding = CGPointZero;
    
    if ((self.menuDelegate != nil) &&
        ([self.menuDelegate respondsToSelector:@selector(cellPaddingForMenuPageViewController:)])) {
        padding = [self.menuDelegate cellPaddingForMenuPageViewController:self];
    }
    
    CGFloat result = padding.y;
    if (((UICollectionViewFlowLayout *)collectionViewLayout).scrollDirection == UICollectionViewScrollDirectionHorizontal) {
        result = padding.x;
    }
    
    return 0;
}

- (CGFloat) collectionView:(UICollectionView *)collectionView
                    layout:(UICollectionViewLayout *)collectionViewLayout
minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
    // get the padding value from the delegate
    CGPoint padding = CGPointZero;
    
    if ((self.menuDelegate != nil) &&
        ([self.menuDelegate respondsToSelector:@selector(cellPaddingForMenuPageViewController:)])) {
        padding = [self.menuDelegate cellPaddingForMenuPageViewController:self];
    }
    
    CGFloat result = padding.x;
    if (((UICollectionViewFlowLayout *)collectionViewLayout).scrollDirection == UICollectionViewScrollDirectionHorizontal) {
        result = padding.y;
    }
    
    return 0;
}

- (CGSize) collectionView:(UICollectionView *)collectionView
                   layout:(UICollectionViewLayout *)collectionViewLayout
referenceSizeForHeaderInSection:(NSInteger)section
{
    return CGSizeZero;
}

- (CGSize) collectionView:(UICollectionView *)collectionView
                   layout:(UICollectionViewLayout *)collectionViewLayout
referenceSizeForFooterInSection:(NSInteger)section
{
    return CGSizeZero;
}

#pragma mark - Methods

- (OCGMenuCell *) dequeueMenuItemCellWithReuseId:(NSString *)reuseId
                                    forIndexPath:(NSIndexPath *)indexPath
{
    OCGMenuCell *result = nil;
    
    if (indexPath) {
        result = [self.collectionView dequeueReusableCellWithReuseIdentifier:reuseId
                                                                forIndexPath:indexPath];
    }
    
    return result;
}

- (void) calculatePageMetricsAndMenu
{
    // precalculate page metrics and build the keys array
    self.numberOfColumns = 0;
    self.numberOfRows = 0;
    self.keySize = CGSizeZero;
    self.normalKeyFont = [UIFont systemFontOfSize:MAX_FONT_SIZE];
    self.displayKeyFont = [UIFont systemFontOfSize:MAX_FONT_SIZE];
    
    if (self.menu != nil) {
        NSString *menuKeysString = nil;
        SoftKeyContext *context = nil;
        
        if ([self.menu isKindOfClass:[SoftKeyContext class]]) {
            menuKeysString = [(SoftKeyContext *)self.menu rootMenu];
            context = (SoftKeyContext *)self.menu;
            self.numberOfColumns = [(SoftKeyContext *)self.menu columns].integerValue;
            self.menuTitle = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠀶󠀫󠀶󠁲󠀴󠁋󠁢󠁓󠁊󠁁󠁕󠁰󠁫󠀯󠁲󠀶󠁌󠁔󠁮󠁕󠀹󠀴󠁖󠁶󠁎󠀲󠁕󠁿*/ @"Sales Menu", nil);
        } else if ([self.menu isKindOfClass:[SoftKey class]]) {
            menuKeysString = [(SoftKey *)self.menu softKeyMenu];
            context = (SoftKeyContext *)[(SoftKey *)self.menu context];
            self.numberOfColumns = [[(SoftKey *)self.menu context] columns].integerValue;
            self.menuTitle = [(SoftKey *)self.menu keyDescription];
        }
        
        // get the layouts key ids from the key string (space seperated)
        NSMutableArray *menuKeys = [[NSMutableArray alloc] init];
        NSArray *keyIds = [menuKeysString componentsSeparatedByString:@" "];
        // build the keys arrays
        for (NSString *keyId in keyIds) {
            if ([keyId isEqualToString:@"K0"]) {
                // add a blank key
                [menuKeys addObject:[NSNull null]];
            } else if ([keyId stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]].length > 0) {
                // find the key
                SoftKey *key = [BasketController.sharedInstance softKeyForKeyId:keyId softKeyContext:context];
                if (key)
                {
                    [menuKeys addObject:key];
                }
            }
        }
        self.keys = menuKeys;
        
        // get the number of columns from the datasource (always overrides the data value)
        if (self.menuDataSource != nil) {
            if ([self.menuDataSource respondsToSelector:@selector(numberOfColumnsOnPage:)]) {
                self.numberOfColumns = [self.menuDataSource numberOfColumnsOnPage:self];
            }
        }
        
        // range check number of columns
        if (self.numberOfColumns > menuKeys.count) {
            self.numberOfColumns = menuKeys.count;
        }
        // calculate number of rows
        if (menuKeys.count == 0) {
            self.numberOfRows = 0;
        } else if (menuKeys.count <= self.numberOfColumns) {
            self.numberOfRows = 1;
        } else {
            self.numberOfRows = ceil((double)menuKeys.count / (double)self.numberOfColumns);
        }
        
        // calculate the key size
        if ((self.numberOfColumns != 0) && (self.numberOfRows != 0)) {
            // calculate the cell size
            CGFloat width = self.collectionView.frame.size.width / (CGFloat)self.numberOfColumns;
            CGFloat height = 44.0f;
            if ((self.menuDelegate != nil) &&
                ([self.menuDelegate respondsToSelector:@selector(cellHeightForMenuPageViewController:)])) {
                height = [self.menuDelegate cellHeightForMenuPageViewController:self];
            }
            // get the padding value from the delegate
            CGPoint padding = CGPointZero;
            if ((self.menuDelegate != nil) &&
                ([self.menuDelegate respondsToSelector:@selector(cellPaddingForMenuPageViewController:)])) {
                padding = [self.menuDelegate cellPaddingForMenuPageViewController:self];
            }
            self.keySize = CGSizeMake(width - (padding.x * 2.0f), height);
        }
        
        // update the key font - find the smallest font and use that, so that they are uniform
        for (SoftKey *key in self.keys) {
            if ([key isKindOfClass:[SoftKey class]]) {
                // get the font for this key
                CGSize itemSize = [self sizeForItemMenuItem:key];
                
                if ([key.keyType isEqual:@"POSFUNC"] && itemSize.height > 15)
                {
                    itemSize.height = 15;
                }
                
                
                UIFont *newKeyFont =
                [self sizeLabelFontToFitText:key.keyDescription
                                      inSize:itemSize];
                // use this font if it is smaller
                if (newKeyFont.pointSize < self.normalKeyFont.pointSize) {
                    self.normalKeyFont = newKeyFont;
                }
                
                if ([key.keyType isEqual:@"DISPLAY_INFO"])
                {
                    self.displayKeyFont = newKeyFont;
                }
            }
        }
    }
}

- (void) reloadMenu:(BOOL)recalculate
{
    if (recalculate) {
        [self calculatePageMetricsAndMenu];
    }
    [self.collectionView reloadData];
}

@end
