//
//  POSFuncController.h
//  mPOS
//
//  Created by Antonio Strijdom on 20/01/2014.
//  Copyright (c) 2014 Clarity. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UIAlertView+OCGBlockAdditions.h"
#import "POSPermission.h"
#import "MoreOptionsTableViewController.h"
#import "OCGSplitViewController.h"

@interface POSFuncController : NSObject <MoreOptionsTableViewControllerDelegate>

extern NSString * const kDATA_CAPTURE_SWIPE;

/** @property splitViewController
 *  @brief A reference to mPOS' main split view controller.
 */
@property (nonatomic, strong) OCGSplitViewController *splitViewController;

/** @brief Returns the singleton instance of the POS func controller.
 */
+ (POSFuncController *) sharedInstance;

/** @brief Presents a permission error for the specified permission.
 *  @param perm The permission we are displaying an error for.
 *  @param block The block to execute when the alert is dismissed.
 */
+ (void) presentPermissionAlert:(POSPermission)perm
                      withBlock:(CancelBlock)block;

/** @brief Presents a generic availability error.
 *  @param block The block to execute when the alert is dismissed.
 */
+(void)presentUnavailableWithBlock:(CancelBlock)block;

/** @brief Gets the menu option info for the specified key.
 *  @param key The key to get information for.
 *  @param title The string to place the title in.
 *  @param image The image to place the title in.
 */
+ (void) getKeyInfoWithKey:(SoftKey *)softKey
                     title:(NSString **)title
                     image:(UIImage **)image;

/** @brief Build the more options menu.
 *  @return The more options view controller for display.
 */
- (MoreOptionsTableViewController *) buildMoreOptionsVCForKey:(SoftKey *)menuSoftKey;

/** @brief Assigns the affiliation specified to the basket.
 *  @param completion Optional completion block.
 *  @return YES if the user has permission to access the function.
 */
- (BOOL) assignAffiliation:(Affiliation *)affiliation
                completion:(void (^)(void))completion;

/** @brief Assigns a sales associate to basket items.
 *  @param basketItems The basket items to assign the sales associate to.
 * @return YES if the user has permission to access the function
 */
- (BOOL) assignSalesAssociateToBasketItems:(NSArray *)basketItems;

/** @brief User requests to clear the basket items.
  * @return YES if the user has permission to access the function 
  */
- (BOOL) cancelActiveBasket;

/** @brief Captures an email address against the basket.
  * @return YES if the user has permission to access the function */
- (BOOL) captureEmail;

/** @brief Changes the quantity of specified item.
 *  @param basketItem The item to change the quantity of.
  * @return YES if the user has permission to access the function */
- (BOOL) changeBasketItemQuantity:(MPOSBasketItem *)basketItem;

/** @brief User requests the basket to be checked out.
  * @return YES if the user has permission to access the function */
- (BOOL) checkoutBasket;

/** @brief Gets the status of a specific device station.
  * @return YES if the user has permission to access the function */
- (BOOL) deviceStationStatus;

/** @brief The signature was reported as confirmed as matching the source sample.
 *  @param signature  The signature captured.
 */
- (void) eftConfirmSignature:(UIImage *)signature;

/** @brief The signature was reported as NOT matching the source sample.
 *  @param signature  The signature captured.
 */
- (void) eftUnConfirmSignature:(UIImage *)signature;

/** @brief Finishes the current tendered sale.
 *  @return YES if the user has permission to access the function */
- (BOOL) finishBasket;

/** @brief User requests to be logged of.
  * @return YES if the user has permission to access the function */
- (BOOL) logoutCurrentAssociate;

/** @brief Marks the specified basket items to be printed on the gift receipt.
 */
- (BOOL) markBasketItemsForGiftReceipt:(NSArray *)basketItems;

/** @brief Opens a cash drawer.
  * @return YES if the user has permission to access the function */
- (BOOL) openCashDrawer;

/** @brief User requests to add/ remove basket discounts
  * @return YES if the user has permission to access the function */
- (BOOL) presentBasketDiscounts;

/** @brief User requests to add basket item discount
  * @return YES if the user has permission to access the function */
- (BOOL) presentBasketItemDiscount:(NSArray *)basketItems;

/** @brief User wants to modify the price of a basket item.
 *  @param basketItem The basket item to override the price on.
  * @return YES if the user has permission to access the function */
- (BOOL) presentBasketItemPriceOverride:(MPOSBasketItem *)basketItem;

/** @brief User requests to perform a customer search.
  * @return YES if the user has permission to access the function */
- (BOOL) presentCustomerSearch;

/** @brief Presents the item search view.
 *  @param The softkey that represents the item search function.
  * @return YES if the user has permission to access the function */
- (BOOL) presentItemSearch:(SoftKey *)softKey;

/** @brief Presents the reprint receipt view.
  * @return YES if the user has permission to access the function */
- (BOOL) presentReprint;

/** @brief User requests to view the application settings.
  * @return YES if the user has permission to access the function */
- (BOOL) presentSettingsView;

/** @brief User requests to make the sale tax free.
 * @return YES if the user has permission to access the function */
- (BOOL) presentTaxFreeSaleView;

/** @brief Presents the gift receipt view.
  * @param selectedItem If BasketItem is non nil, the item specified's gift receipt flag is simply toggled.
  * @return YES if the user has permission to access the function */
- (BOOL) printGiftReceiptForItem:(MPOSBasketItem *)selectedItem;

/** @brief Server request for flight details
 **/
- (void) presentBasketFlightDetailsView;

/** @brief User requests to recall a previously suspended basket.
  * @return YES if the user has permission to access the function */
- (BOOL) recallActiveBasket;

/** @brief Removes the items specified from the basket.
 *  @param basketItems Items to remove (void).
  * @return YES if the user has permission to access the function */
- (BOOL) removeBasketItems:(NSArray *)basketItems;

/** @brief Removes a discount from a basket item.
 *  @param basketItem The basket item to remove a discount from.
  * @return YES if the user has permission to access the function */
- (BOOL) removeDiscountFromBasketItem:(MPOSBasketItem *)basketItem;

/** @brief Reprints the last transaction.
  * @return YES if the user has permission to access the function */
- (BOOL) reprintLastTx;

/** @brief Scans a new return to the current basket.
  * @return YES if the user has permission to access the function */
- (BOOL) scanReturnToBasket;

/** @brief Shows the item detail for the specified item.
 *  @param basketItem The item to display details for.
 */
- (void) showOperationsForBasketItem:(MPOSBasketItem *)basketItem;

/** @brief Starts the item lookup process.
  * @return YES if the user has permission to access the function */
- (BOOL) startItemLookup;

/** @brief Starts the process of capturing a card swipe.
  * @return YES if the user has permission to access the function */
- (BOOL) startSwipingData;

/** @brief User requests to suspend basket.
  * @return YES if the user has permission to access the function */
- (BOOL) suspendActiveBasket;

/** @brief Toggles training mode on or off.
  * @return YES if the user has permission to access the function */
- (BOOL) toggleTrainingMode;

@end
