//
//  Device+MPOSExtensions.m
//  mPOS
//
//  Created by Antonio Strijdom on 25/02/2015.
//  Copyright (c) 2015 Clarity. All rights reserved.
//

#import "Device+MPOSExtensions.h"

@implementation Device (MPOSExtensions)

- (DeviceSelectionMode) selectionMode
{
    DeviceSelectionMode result = DeviceSelectionModeNone;
    
    if ([self.selection isEqualToString:@"NONE"]) {
        result = DeviceSelectionModeNone;
    } else if ([self.selection isEqualToString:@"RDM"]) {
        result = DeviceSelectionModeRDM;
    } else if ([self.selection isEqualToString:@"PROVIDER"]) {
        result = DeviceSelectionModeProvider;
    }
    
    return result;
}

- (DeviceConnectionType) connectionType
{
    DeviceConnectionType result = DeviceConnectionTypeIP;
    
    if ([self.connection isEqualToString:@"IP"]) {
        result = DeviceConnectionTypeIP;
    } else if ([self.connection isEqualToString:@"BT"]) {
        result = DeviceConnectionTypeBT;
    }
    
    return result;
}

@end
