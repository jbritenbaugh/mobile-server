//
//  SoftKey+MPOSExtensions.m
//  mPOS
//
//  Created by John Scott on 23/04/2015.
//  Copyright (c) 2015 Clarity. All rights reserved.
//

#import "SoftKey+MPOSExtensions.h"

@implementation SoftKey (MPOSExtensions)

@dynamic context;
@dynamic layoutStyle;

@end
