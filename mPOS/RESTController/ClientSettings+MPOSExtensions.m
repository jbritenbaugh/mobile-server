//
//  ClientSettings+MPOSExtensions.m
//  mPOS
//
//  Created by Antonio Strijdom on 08/05/2015.
//  Copyright (c) 2015 Clarity. All rights reserved.
//

#import "ClientSettings+MPOSExtensions.h"

@implementation ClientSettings (MPOSExtensions)

- (BasketLayout) parsedBasketLayout
{
    BasketLayout result = BasketLayoutTable;
    
    if (self.basketLayout.length == 0) {
        // no setting, default based on idiom
        if (UIUserInterfaceIdiomPad == UI_USER_INTERFACE_IDIOM()) {
            // ipad defaults to grid
            result = BasketLayoutGrid;
        } else if (UIUserInterfaceIdiomPhone == UI_USER_INTERFACE_IDIOM()) {
            // ipod defaults to table
            result = BasketLayoutTable;
        }
    } else if ([[self.basketLayout lowercaseString] isEqualToString:@"grid"]) {
        result = BasketLayoutGrid;
    } else if ([[self.basketLayout lowercaseString] isEqualToString:@"table"]) {
        result = BasketLayoutTable;
    }
    
    return result;
}

@end
