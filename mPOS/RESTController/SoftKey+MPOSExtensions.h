//
//  SoftKey+MPOSExtensions.h
//  mPOS
//
//  Created by John Scott on 23/04/2015.
//  Copyright (c) 2015 Clarity. All rights reserved.
//

#import "RESTController.h"

@interface SoftKey (MPOSExtensions)

@property (nonatomic, strong) SoftKeyContext *context;

@property (nonatomic, strong) NSString * layoutStyle;

@end
