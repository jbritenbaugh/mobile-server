//
//  RESTControllerBase.h
//  mPOS
//
//  Created by John Scott on 14/02/2014.
//  Copyright (c) 2014 Clarity. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ocgdispatch.h"

@class Credentials;

@class RESTController;
@class ServerError;
@class RESTInvocation;

typedef void(^RESTControllerSendBlockType)(RESTInvocation *invocation, NSString *server, NSString *etag);
typedef void(^RESTControllerCompleteBlockType)(RESTInvocation *invocation, NSString*server, id result, NSError *error, ServerError *serverError, NSString *etag);

@protocol RESTControllerDelegate;

@interface RESTControllerBase : NSObject

@property (nonatomic, strong) NSString* baseURL;

- (id)sendSynchronousRequest:(NSURLRequest *)originalRequest
                  returnType:(NSString*)returnType
                       error:(NSError **)error
                 serverError:(id*)serverError
                        etag:(NSString**)etag;

- (void)invokeSynchronous:(RESTInvocation *)invocation
                   server:(NSString*)server
                     etag:(NSString*)etag
                 complete:(RESTControllerCompleteBlockType)complete;

- (NSMutableURLRequest*)requestWithServer:(NSString*)server method:(NSString*)method path:(NSString*)path parameters:(NSDictionary*)parameters;

- (id)objectForData:(NSData*)data type:(NSString*)type;
- (NSData*)dataForObject:(id)anObject type:(NSString*)type;
- (NSString*)stringForObject:(id)anObject type:(NSString *)type;
- (Class)classForContextName:(NSString*)type;

@property (nonatomic, readonly) NSDateFormatter *dateFormatter; 

+ (instancetype) sharedInstance;

@property (nonatomic, weak) id <RESTControllerDelegate> delegate;

@end


@protocol RESTControllerDelegate <NSObject>

@optional

-(void)RESTController:(RESTController*)controller
           willInvoke:(RESTInvocation*)invocation
               server:(NSString*)server
                 etag:(NSString*)etag
             complete:(RESTControllerCompleteBlockType)complete
                 send:(RESTControllerSendBlockType)send;

-(void)RESTController:(RESTController*)controller
            didInvoke:(RESTInvocation*)invocation
               server:(NSString*)server
               result:(id)result
                error:(NSError *)error
          serverError:(ServerError*)serverError
                 etag:(NSString*)etag
             complete:(RESTControllerCompleteBlockType)complete;


@end