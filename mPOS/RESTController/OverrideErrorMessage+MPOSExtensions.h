//
//  OverrideErrorMessage+MPOSExtensions.h
//  mPOS
//
//  Created by Antonio Strijdom on 17/03/2015.
//  Copyright (c) 2015 Clarity. All rights reserved.
//

#import "RESTController.h"

/** @file OverrideErrorMessage+MPOSExtensions.h */

/** @enum OverrideErrorMessageLevel
 *  @brief Override levels for overridable errors.
 */
typedef NS_ENUM(NSInteger, OverrideErrorMessageLevel) {
    /** @brief Unknown.
     */
    OverrideErrorMessageLevelUnknown = -1,
    /** @brief The error cannot be overridden.
     */
    OverrideErrorMessageLevelAbort = 0,
    /** @brief The operator is required to accept or decline the override.
     */
    OverrideErrorMessageLevelOperatorConfirm = 1,
    /** @brief A manager override is required by entering the user ID of a manager.
     */
    OverrideErrorMessageLevelManagerIdRequired = 2,
    /** @brief A manager override is required by entering the user ID and password of a manager.
     */
    OverrideErrorMessageLevelManagerPasswordRequired = 3,
    /** @brief The error should be displayed to the operator for informational purposes.
     *  @discussion The operator cannot decline it but in order for the operation to progress 
     *  the operator must have confirmed that they have seen it.
     */
    OverrideErrorMessageLevelInformational = 4
};

/** @brief Catagory for extending the REST Object. */
@interface OverrideErrorMessage (MPOSExtensions)

/** @property parsedErrorLevel
 *  @brief The parsed error override level.
 */
@property (nonatomic, readonly) OverrideErrorMessageLevel parsedErrorLevel;

@end
