//
//  ocgdispatch.m
//  mPOS
//
//  Created by John Scott on 28/04/2014.
//  Copyright (c) 2014 Clarity. All rights reserved.
//

#import "ocgdispatch.h"
#import "OCGProgressOverlay.h"
#import <objc/NSObjCRuntime.h>

void ocg_sync(dispatch_block_t block)
{
    block();
}

void ocg_sync_main(dispatch_block_t block)
{
    dispatch_sync(dispatch_get_main_queue(), block);
}

void ocg_async_background(dispatch_block_t block)
{
    static dispatch_queue_t background_queue;
    
    ocg_once(^{
        background_queue = dispatch_queue_create("com.omnicogroup.background", DISPATCH_QUEUE_SERIAL);
        dispatch_set_target_queue(background_queue, dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0));
    });
    
    dispatch_async(background_queue, block);
}

void ocg_async_background_overlay(dispatch_block_t block)
{
    ocg_async_background(^{
        static NSInteger count = 0;
        
        if (!count)
        {
            ocg_sync_main(^{
                [OCGProgressOverlay show];
            });
        }
        
        count++;
        
        block();
        
        count--;
        
        if (!count)
        {
            ocg_sync_main(^{
                [OCGProgressOverlay hide];
            });
        }
    });
}
