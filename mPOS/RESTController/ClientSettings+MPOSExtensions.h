//
//  ClientSettings+MPOSExtensions.h
//  mPOS
//
//  Created by Antonio Strijdom on 08/05/2015.
//  Copyright (c) 2015 Clarity. All rights reserved.
//

#import "RESTController.h"

typedef NS_ENUM(NSInteger, BasketLayout)
{
    BasketLayoutTable = 0,
    BasketLayoutGrid = 1
};

@interface ClientSettings (MPOSExtensions)

@property (nonatomic, readonly) BasketLayout parsedBasketLayout;

@end
