//
//  OverrideErrorMessage+MPOSExtensions.m
//  mPOS
//
//  Created by Antonio Strijdom on 17/03/2015.
//  Copyright (c) 2015 Clarity. All rights reserved.
//

#import "OverrideErrorMessage+MPOSExtensions.h"

@implementation OverrideErrorMessage (MPOSExtensions)

#pragma mark - Properties

- (OverrideErrorMessageLevel) parsedErrorLevel
{
    OverrideErrorMessageLevel result = OverrideErrorMessageLevelUnknown;
    
    switch (self.errorLevel) {
        case 0:
            result = OverrideErrorMessageLevelAbort;
            break;
        case 1:
            result = OverrideErrorMessageLevelOperatorConfirm;
            break;
        case 2:
            result = OverrideErrorMessageLevelManagerIdRequired;
            break;
        case 3:
            result = OverrideErrorMessageLevelManagerPasswordRequired;
            break;
        case 4:
            result = OverrideErrorMessageLevelInformational;
            break;
        default:
            result = OverrideErrorMessageLevelUnknown;
            break;
    }
    
    return result;
}

@end
