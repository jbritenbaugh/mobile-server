//
//  RESTControllerBase.m
//  mPOS
//
//  Created by John Scott on 14/02/2014.
//  Copyright (c) 2014 Clarity. All rights reserved.
//

#import "RESTControllerBase.h"

#import "OCGProgressOverlay.h"
#import "RXMLElement.h"
#import "RESTObject.h"
#import "OCGProgressOverlay.h"
#import "RESTInvocation.h"

#if USE_MANAGED_OBJECT_CONTEXT_PROXY
#import 
#endif

@implementation RESTControllerBase
{
    NSDateFormatter *_dateFormatter;
}

+ (instancetype) sharedInstance
{
    static id _controller = nil;
    
    if (_controller == nil) {
        ocg_once(^{
            _controller = [[[self class] alloc] init];
        });

    }
    
    return _controller;
}

-(id)init
{
    self = [super init];
    if (self)
    {
        _dateFormatter = [[NSDateFormatter alloc] init];
        /* http://www.w3.org/TR/NOTE-datetime YYYY-MM-DDThh:mm:ss.sTZD */
        _dateFormatter.dateFormat = @"yyyy-MM-dd'T'HH:mm:ssZ";
    }
    return self;
}

- (id)sendSynchronousRequest:(NSURLRequest *)originalRequest
                  returnType:(NSString*)returnType
                       error:(NSError **)error
                 serverError:(id*)serverError
                        etag:(NSString**)etag
{
    NSHTTPURLResponse *response = nil;
    
    NSMutableURLRequest *request = [originalRequest mutableCopy];
    
    if (etag)
    {
        [request setValue:*etag forHTTPHeaderField:@"Etag"];
    }
    
    id result = nil;
#if DEBUG
    DebugLog2(@"REQUEST:\nurl: {}\nmethod: {}\nheaders: {}\nbody: {}", request.URL, request.HTTPMethod, request.allHTTPHeaderFields, request.HTTPBody);
#endif
    
    NSData* responseData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:error];
    
#if DEBUG
    if (response.statusCode == 200 && *error == nil)
    {
        DebugLog2(@"REQUEST:\nurl: {}\nmethod: {}\nheaders: {}\nbody: {}\n\nRESPONSE:\ntranport: {}\nstatus: {}\nbody: {}\nheaders: {}\n", request.URL, request.HTTPMethod, request.allHTTPHeaderFields, request.HTTPBody,*error, @(response.statusCode), responseData,response.allHeaderFields);
    }
    else
    {
        ErrorLog2(@"REQUEST:\nurl: {}\nmethod: {}\nheaders: {}\nbody: {}\n\nRESPONSE:\ntranport: {}\nstatus: {}\nbody: {}\nheaders: {}\n", request.URL, request.HTTPMethod, request.allHTTPHeaderFields, request.HTTPBody,*error, @(response.statusCode), responseData, response.allHeaderFields);
    }
#endif
    
    if (response.statusCode == 200)
    {
        if (etag)
        {
            *etag = response.allHeaderFields[@"Etag"];
        }

        if ((returnType == NULL) || (responseData.length <= 0))
        {
        }
        else if ([returnType isEqual:[NSString class]])
        {
            result = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
        }
        else if ([returnType isEqual:[NSData class]])
        {
            result = responseData;
        }
        else
        {
            result = [self objectForData:responseData type:returnType];
        }
    }
    else if (response.statusCode == 304)
    {
        if (etag)
        {
            *etag = response.allHeaderFields[@"Etag"];
        }
    }
    else if (!*error)
    {
        id responseObject = nil;
        if ([responseData length] > 0)
        {
            responseObject = [self objectForData:responseData type:nil];
        }
        
        if ([responseObject isKindOfClass:ServerError.class])
        {
            *serverError = responseObject;
        }
        else
        {
            NSString *message = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁌󠁓󠀲󠁁󠀶󠁔󠁦󠁔󠁑󠁋󠁴󠀱󠁊󠁒󠁔󠁍󠁩󠁏󠁆󠁤󠁃󠁣󠁫󠁧󠁣󠁳󠀰󠁿*/ @"Could not connect to server", nil);
            
#if DEBUG
            message = [message stringByAppendingFormat:@" for URL %@", response.URL];
#endif
            
            *error = [NSError errorWithDomain:NSURLErrorDomain
                                         code:kCFURLErrorBadServerResponse
                                     userInfo:@{NSLocalizedDescriptionKey : message}];
        }

    }
    return result;
}

- (void)invokeSynchronous:(RESTInvocation *)invocation
                   server:(NSString*)server
                     etag:(NSString*)etag
                 complete:(RESTControllerCompleteBlockType)complete
{
    BOOL shouldContinue = YES;
    __block __weak RESTControllerBase *weakSelf = self;
    RESTControllerSendBlockType send = ^(RESTInvocation *invocation, NSString *server, NSString *etag)
    {
        NSData *body = nil;
        NSString *path = invocation.RESTPath;
        
        NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
        for (NSString *parameter in invocation.RESTParameterTypes)
        {
            id value = [invocation valueForKey:parameter];
            if ([invocation.RESTBodyParameter isEqualToString:parameter])
            {
                body = [weakSelf dataForObject:value type:invocation.RESTParameterTypes[parameter]];
            }
            else if ([value isKindOfClass:NSArray.class])
            {
                [parameters setValue:value forKey:parameter];
            }
            else if (value)
            {
                NSString *RESTPathMatchingString = [NSString stringWithFormat:@"{%@}", parameter];
                
                NSMutableString *buffer = [NSMutableString string];
                [weakSelf stringForObject:value type:invocation.RESTParameterTypes[parameter] tagged:YES buffer:buffer];
                
                if ([path containsString:RESTPathMatchingString])
                {
                    path = [path stringByReplacingOccurrencesOfString:RESTPathMatchingString withString:buffer];
                }
                else
                {
                    [parameters setValue:buffer forKey:parameter];
                }
            }
        }

        NSMutableURLRequest *httpRequest = [self requestWithServer:server
                                                            method:invocation.RESTMethod
                                                              path:path
                                                        parameters:parameters];
        
        httpRequest.HTTPBody = body;
        httpRequest.timeoutInterval = invocation.RESTTimeoutInterval;
        
        NSError *error = nil;
        ServerError *serverError = nil;
        id result = [self sendSynchronousRequest:httpRequest
                                      returnType:invocation.RESTReturnType
                                           error:&error
                                     serverError:&serverError
                                            etag:&etag];
        
        if ([self.delegate respondsToSelector:@selector(RESTController:didInvoke:server:result:error:serverError:etag:complete:)])
        {
            [self.delegate RESTController:(RESTController*)self
                                didInvoke:invocation
                                   server:server
                                   result:result
                                    error:error
                              serverError:serverError
                                     etag:etag
                                 complete:complete];
        }
        else
        {
            complete(invocation, server, result, error, serverError, etag);
        }
    };
    
    if ([self.delegate respondsToSelector:@selector(RESTController:willInvoke:server:etag:complete:send:)])
    {
        [self.delegate RESTController:(RESTController*)self
                           willInvoke:invocation
                               server:server
                                 etag:etag
                             complete:complete
                                 send:send];
        
    }
    else
    {
        send(invocation, server, etag);
    }
}

- (NSMutableURLRequest*)requestWithServer:(NSString*)server method:(NSString*)method path:(NSString*)path parameters:(NSDictionary*)parameters
{
    NSMutableDictionary *mutableParameters = [NSMutableDictionary dictionaryWithDictionary:parameters];

    NSString *urlString = [NSString stringWithFormat:@"%@/MShopper%@?%@", server, path, [self queryForDictionary:mutableParameters]];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]];
    request.HTTPMethod = method;
    NSDictionary *headers = [[NSMutableDictionary alloc] init];
    if (![request.HTTPMethod isEqual:@"GET"])
    {
        [headers setValue:@"application/xml" forKey:@"Content-Type"];
    }
    [request setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
    [request setAllHTTPHeaderFields:headers];
    return request;
}

-(id)objectForData:(NSData*)data type:(NSString*)type
{
    id object = nil;
    if (data != nil)
    {
        RXMLElement *element = [RXMLElement elementFromXMLData:data];
        NSMutableDictionary *skipped = nil;
#if DEBUG
        skipped = [NSMutableDictionary dictionary];
#endif
        object = [self objectForElement:element type:[self typeForTag:element.tag] ?: type parent:nil skipped:skipped];
#if DEBUG
        if ([skipped count] > 0)
        {
            NSMutableString *iniContent = [NSMutableString stringWithString:@"Skipping unhandled tags:\n"];
            for (NSString *aClass in [[skipped allKeys] sortedArrayUsingSelector:@selector(localizedCompare:)])
            {
                [iniContent appendFormat:@"\n[%@]", aClass];
                for (NSString *tag in [[skipped[aClass] allObjects] sortedArrayUsingSelector:@selector(localizedCompare:)])
                {
                    [iniContent appendFormat:@"\n %@ = ", tag];
                }
            }
            
            ErrorLog2(@"{}", iniContent);
        }
#endif
    }
    return object;
}

-(id)objectForElement:(RXMLElement*)element type:(NSString*)type parent:(id)parent skipped:(NSMutableDictionary*)skipped;
{
    id object = nil;
    if ([type isEqual:@"NSString"])
    {
        object = element.text;
    }
    else if ([type isEqual:@"NSNumber"])
    {
        NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
        object = [formatter numberFromString:element.text];
    }
    else if ([type isEqual:@"NSDate"])
    {
        object = [_dateFormatter dateFromString:element.text];
    }
    else if ([type isEqual:@"BOOL"])
    {
        object = [NSNumber numberWithBool:[element.text boolValue]];
    }
    else if ([type isEqual:@"double"])
    {
        object = [NSNumber numberWithDouble:[element.text doubleValue]];
    }
    else if ([type isEqual:@"NSInteger"])
    {
        object = [NSNumber numberWithInteger:[element.text integerValue]];
    }
    else if ([type isEqual:@"NSArray"])
    {
        object = [NSMutableArray array];
        [element iterate:@"*"
                   usingBlock:^(RXMLElement *childElement) {
                       NSString *type = [self typeForTag:childElement.tag];
                       if (type != nil)
                       {
                           
                           id value = [self objectForElement:childElement type:type parent:parent skipped:skipped];
                           if (value != nil)
                           {
                               [object addObject:value];
                           }
                       }
#if DEBUG
                       else
                       {
                           [skipped setValue:[(skipped[NSStringFromClass([parent class])] ?: [NSSet set]) setByAddingObject:childElement.tag] forKey:NSStringFromClass([parent class])];
                       }
#endif
                   }];
    }
    else if (NSClassFromString(type) != nil)
    {
        object = [[NSClassFromString(type) alloc] init];
        if ([object conformsToProtocol:@protocol(RESTObject)])
        {
            if ([object xmlAlias])
            {
                id value = [self objectForElement:element type:[object xmlAlias] parent:parent skipped:skipped];
                [object setValue:value forKey:[object xmlTag]];
                NSAssert([object valueForKey:[object xmlTag]], @"");
            }
            else
            {
                NSDictionary *attributes = [object xmlAttributes];
                
                [element iterate:@"*"
                      usingBlock:^(RXMLElement *childElement) {
                          NSString *type = attributes[childElement.tag];
                          if (type == nil)
                          {
                              NSString *lowerCaseTag = childElement.tag.lowercaseString;
                              for (NSString *xmlAttribute in attributes)
                              {
                                  NSString *lowerCaseXmlAttribute = xmlAttribute.lowercaseString;
                                  if ([lowerCaseTag isEqualToString:lowerCaseXmlAttribute])
                                  {
                                      type = attributes[xmlAttribute];
                                  }
                              }
                          }
                          
                          if (type != nil)
                          {
                              id value = [self objectForElement:childElement type:type parent:object skipped:skipped];
                              if (value != nil)
                              {
                                  [object setValue:value forKey:childElement.tag];
                              }
                          }
#if DEBUG
                          else
                          {
                              [skipped setValue:[(skipped[NSStringFromClass([parent class])] ?: [NSSet set]) setByAddingObject:childElement.tag] forKey:NSStringFromClass([object class])];
                          }
#endif
                      }];
            }
        }
    }
    else
    {
        object = element.text;
    }

    if ([object conformsToProtocol:@protocol(RESTObject)] && [object parentAttribute] != nil)
    {
        [object setValue:parent forKey:[object parentAttribute]];
    }

    return object;
}

- (NSString *) queryForDictionary:(NSDictionary *)dictionary
{
    NSMutableString *result = [NSMutableString string];
    
    // enumerate through the keys
    for (NSString *key in dictionary) {
        if ([dictionary[key] isKindOfClass:[NSArray class]])
        {
            for (id value in dictionary[key])
            {
                [self appendKey:key value:value toQuery:result];
            }
        }
        else
        {
            [self appendKey:key value:dictionary[key] toQuery:result];
        }
    }
    
    return result;
}

static BOOL shouldPercentEscapeCharacter[] =
{
    YES,     // Null character
    YES,     // Start of Heading
    YES,     // Start of Text
    YES,     // End-of-text character
    YES,     // End-of-transmission character
    YES,     // Enquiry character
    YES,     // Acknowledge character
    YES,     // Bell character
    YES,     // Backspace
    YES,     // Horizontal tab
    YES,     // Line feed
    YES,     // Vertical tab
    YES,     // Form feed
    YES,     // Carriage return
    YES,     // Shift Out
    YES,     // Shift In
    YES,     // Data Link Escape
    YES,     // Device Control 1
    YES,     // Device Control 2
    YES,     // Device Control 3
    YES,     // Device Control 4
    YES,     // Negative-acknowledge character
    YES,     // Synchronous Idle
    YES,     // End of Transmission Block
    YES,     // Cancel character
    YES,     // End of Medium
    YES,     // Substitute character
    YES,     // Escape character
    YES,     // File Separator
    YES,     // Group Separator
    YES,     // Record Separator
    YES,     // Unit Separator
    YES,     // Space
    YES,     // Exclamation mark
    YES,     // Quotation mark
    YES,     // Number sign
    YES,     // Dollar sign
    YES,     // Percent sign
    YES,     // Ampersand
    YES,     // Apostrophe
    YES,     // Left parenthesis
    YES,     // Right parenthesis
    YES,     // Asterisk
    YES,     // Plus sign
    YES,     // Comma
    NO,      // Hyphen-minus
    YES,     // Full stop
    YES,     // Slash
    NO,      // Digit Zero
    NO,      // Digit One
    NO,      // Digit Two
    NO,      // Digit Three
    NO,      // Digit Four
    NO,      // Digit Five
    NO,      // Digit Six
    NO,      // Digit Seven
    NO,      // Digit Eight
    NO,      // Digit Nine
    YES,     // Colon
    YES,     // Semicolon
    YES,     // Less-than sign
    NO,      // Equal sign
    YES,     // Greater-than sign
    YES,     // Question mark
    YES,     // At sign
    NO,      // Latin Capital letter A
    NO,      // Latin Capital letter B
    NO,      // Latin Capital letter C
    NO,      // Latin Capital letter D
    NO,      // Latin Capital letter E
    NO,      // Latin Capital letter F
    NO,      // Latin Capital letter G
    NO,      // Latin Capital letter H
    NO,      // Latin Capital letter I
    NO,      // Latin Capital letter J
    NO,      // Latin Capital letter K
    NO,      // Latin Capital letter L
    NO,      // Latin Capital letter M
    NO,      // Latin Capital letter N
    NO,      // Latin Capital letter O
    NO,      // Latin Capital letter P
    NO,      // Latin Capital letter Q
    NO,      // Latin Capital letter R
    NO,      // Latin Capital letter S
    NO,      // Latin Capital letter T
    NO,      // Latin Capital letter U
    NO,      // Latin Capital letter V
    NO,      // Latin Capital letter W
    NO,      // Latin Capital letter X
    NO,      // Latin Capital letter Y
    NO,      // Latin Capital letter Z
    YES,     // Left Square Bracket
    YES,     // Backslash [A]
    YES,     // Right Square Bracket
    YES,     // Circumflex accent
    YES,     // Low line
    YES,     // Grave accent
    NO,      // Latin Small Letter A
    NO,      // Latin Small Letter B
    NO,      // Latin Small Letter C
    NO,      // Latin Small Letter D
    NO,      // Latin Small Letter E
    NO,      // Latin Small Letter F
    NO,      // Latin Small Letter G
    NO,      // Latin Small Letter H
    NO,      // Latin Small Letter I
    NO,      // Latin Small Letter J
    NO,      // Latin Small Letter K
    NO,      // Latin Small Letter L
    NO,      // Latin Small Letter M
    NO,      // Latin Small Letter N
    NO,      // Latin Small Letter O
    NO,      // Latin Small Letter P
    NO,      // Latin Small Letter Q
    NO,      // Latin Small Letter R
    NO,      // Latin Small Letter S
    NO,      // Latin Small Letter T
    NO,      // Latin Small Letter U
    NO,      // Latin Small Letter V
    NO,      // Latin Small Letter W
    NO,      // Latin Small Letter X
    NO,      // Latin Small Letter Y
    NO,      // Latin Small Letter Z
    YES,     // Left Curly Bracket
    YES,     // Vertical bar
    YES,     // Right Curly Bracket
    YES,     // Tilde
    YES,     // Delete
};

-(void)appendKey:(NSString*)key value:(NSString*)value toQuery:(NSMutableString*)query
{
    // add the seperator
    if ([query length] > 0) {
        [query appendString:@"&"];
    }
    [query appendString:key];
    [query appendString:@"="];
    
    // get the value for this key
    NSString *valueString = [value description];
    // percent escape
    NSData *valueData = [valueString dataUsingEncoding:NSUTF8StringEncoding];
    const unsigned char *valueCharacters = valueData.bytes;
    const NSUInteger valueCharacterCount = valueData.length;
    for (NSUInteger characterIndex=0; characterIndex<valueCharacterCount; characterIndex++)
    {
        const unsigned char character = valueCharacters[characterIndex];
        if (shouldPercentEscapeCharacter[character] || character > 0x7f)
        {
            [query appendFormat:@"%%%02x", valueCharacters[characterIndex]];
        }
        else
        {
            [query appendFormat:@"%c", valueCharacters[characterIndex]];
        }
    }
}

-(NSString*)typeForTag:(NSString*)tag
{
    return nil;
}

- (Class)classForContextName:(NSString*)type
{
    return Nil;
}

- (NSData*)dataForObject:(id)anObject type:(NSString *)type
{
    if ([type isEqual:@"NSData"])
    {
        return anObject;
    }
    else
    {
        NSString *string = [self stringForObject:anObject type:type];
        return [string dataUsingEncoding:NSUTF8StringEncoding];
    }
}

- (NSString*)stringForObject:(id)anObject type:(NSString *)type
{
    NSMutableString *buffer = [NSMutableString string];
    [buffer appendString:@"<?xml version='1.0' encoding='UTF-8' standalone='yes'?>"];
    [self stringForObject:anObject type:type tagged:NO buffer:buffer ];
    return buffer;
}

-(void)stringForObject:(id)anObject type:(NSString *)type tagged:(BOOL)tagged buffer:(NSMutableString*)buffer
{
    if (anObject == nil)
    {
        // Append the empty string (ie do nothing)
    }
    else if ([anObject isKindOfClass:NSString.class] && ![type isEqual:@"NSString"])
    {
        DebugLog(@"WARNING Type mismatch! property type = %@, object type = %@", type, NSStringFromClass([anObject class]));
        [buffer appendString:[self escapeString:[anObject description]]];
    }
    else if ([anObject isKindOfClass:NSDate.class] && ![type isEqual:@"NSDate"])
    {
        DebugLog(@"WARNING Type mismatch! property type = %@, object type = %@", type, NSStringFromClass([anObject class]));
        [buffer appendString:[_dateFormatter stringFromDate:anObject]];
    }
    else if ([type hasPrefix:@"NSArray"])
    {
        for (RESTObject *restObject in anObject)
        {
            [self stringForObject:restObject type:nil tagged:NO buffer:buffer];
        }
    }
    else if ([type isEqual:@"NSString"])
    {
        [buffer appendString:[self escapeString:[anObject description]]];
    }
    else if ([type isEqual:@"NSDate"])
    {
        [buffer appendString:[_dateFormatter stringFromDate:anObject]];
    }
    else if ([type isEqual:@"BOOL"])
    {
        [buffer appendString:[anObject boolValue] ? @"true" : @"false"];
    }
    else if ([anObject conformsToProtocol:@protocol(RESTObject)] && ([anObject xmlTag] != nil || tagged))
    {
        if (!tagged)
        {
            [buffer appendFormat:@"<%@>", [anObject xmlTag]];
        }
        
        if ([anObject xmlAlias])
        {
            [self stringForObject:[anObject valueForKey:[anObject xmlTag]] type:[anObject xmlAlias] tagged:YES buffer:buffer];
        }
        else
        {
            NSDictionary *attributes = [anObject xmlAttributes];
            NSArray *attributeNames = [attributes allKeys];
#if DEBUG
            attributeNames = [attributeNames sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
#endif
            for (NSString *property in attributeNames)
            {
                if (![property isEqual:[anObject parentAttribute]])
                {
                    id value = [anObject valueForKey:property];
                    if (value != nil)
                    {
                        [buffer appendFormat:@"<%@>", property];
                        [self stringForObject:value type:attributes[property] tagged:YES buffer:buffer];
                        [buffer appendFormat:@"</%@>", property];
                    }
                }
            }
        }
        
        if (!tagged)
        {
            [buffer appendFormat:@"</%@>", [anObject xmlTag]];
        }
    }
    else
    {
        [buffer appendString:[self escapeString:[anObject description]]];
    }
}

- (NSString *)escapeString:(NSString *)value
{
    NSMutableString *result = [NSMutableString stringWithString:value];
    [result replaceOccurrencesOfString:@"&"  withString:@"&amp;"  options:NSLiteralSearch range:NSMakeRange(0, [result length])];
    [result replaceOccurrencesOfString:@"\"" withString:@"&quot;" options:NSLiteralSearch range:NSMakeRange(0, [result length])];
	[result replaceOccurrencesOfString:@"'"  withString:@"&#x27;" options:NSLiteralSearch range:NSMakeRange(0, [result length])];
	[result replaceOccurrencesOfString:@">"  withString:@"&gt;"   options:NSLiteralSearch range:NSMakeRange(0, [result length])];
	[result replaceOccurrencesOfString:@"<"  withString:@"&lt;"   options:NSLiteralSearch range:NSMakeRange(0, [result length])];
    
	return result;
}

@end
