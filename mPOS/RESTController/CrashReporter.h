//
//  CrashReporter.h
//  mPOS
//
//  Created by John Scott on 12/08/2014.
//  Copyright (c) 2014 Clarity. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CrashReporter : NSObject

+ (instancetype) sharedInstance;

- (BOOL)didCrashLastTime;

@end
