//
//  CrashReporter.m
//  mPOS
//
//  Created by John Scott on 12/08/2014.
//  Copyright (c) 2014 Clarity. All rights reserved.
//

#import "CrashReporter.h"

#import "RESTController.h"
#import "TillSetupController.h"
#import "CRSLocationController.h"

#include <libkern/OSAtomic.h>
#include <execinfo.h>

#include "sys/types.h"
#include <sys/sysctl.h>
#include <string.h>

#import "VersionDetails.h"

@interface CrashReporter ()

-(void)handleException:(NSException *)exception;
-(void)handleSignal:(NSInteger)signal;

@property (nonatomic, assign) BOOL didCrashLastTime;

@end

void __CrashReporterHandleException(NSException *exception)
{
    [CrashReporter.sharedInstance handleException:exception];
}

void __CrashReporterHandleSignal(NSInteger signal)
{
    [CrashReporter.sharedInstance handleSignal:signal];
}

@implementation CrashReporter

+(void)load
{
    if (!TARGET_IPHONE_SIMULATOR && !OCGDebuggerIsApplicationBeingDebugged())
    {
        NSSetUncaughtExceptionHandler(&__CrashReporterHandleException);
        signal(SIGABRT, __CrashReporterHandleSignal);
        signal(SIGILL, __CrashReporterHandleSignal);
        signal(SIGSEGV, __CrashReporterHandleSignal);
        signal(SIGFPE, __CrashReporterHandleSignal);
        signal(SIGBUS, __CrashReporterHandleSignal);
        signal(SIGPIPE, __CrashReporterHandleSignal);
    }
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    NSString *crashFile = [self.sharedInstance crashFile];
    
    if ([fileManager fileExistsAtPath:crashFile])
    {
        CrashReporter.sharedInstance.didCrashLastTime = YES;
        NSDictionary *crashContent = [NSJSONSerialization JSONObjectWithData:[NSData dataWithContentsOfFile:crashFile]
                                                                     options:0
                                                                       error:NULL];
        
        [fileManager removeItemAtPath:crashFile error:NULL];
        
        ocg_async_background_overlay(^{
            NSError *error = nil;
            ServerError *serverError = nil;
            
            [RESTController.sharedInstance logMobileClientCrashEventWithCrashText:crashContent[@"crashText"]
                                                                        crashTime:[NSDate dateWithTimeIntervalSince1970:[crashContent[@"crashTime"] integerValue]]
                                                                          storeID:crashContent[@"storeID"]
                                                                          version:crashContent[@"version"]
                                                                    workStationId:crashContent[@"workStationId"]
                                                                            complete:NULL];
        });
    }

}

// Returns the URL to the application's Documents directory.
- (NSString *)crashFile
{
    NSFileManager *manager = [NSFileManager defaultManager];
    NSURL *result = [[manager URLsForDirectory:NSApplicationSupportDirectory inDomains:NSUserDomainMask] lastObject];
    if (result != nil) {
        result = [result URLByAppendingPathComponent:@"mPOS"
                                         isDirectory:YES];
        NSError *error = nil;
        if (![manager createDirectoryAtURL:result
               withIntermediateDirectories:YES
                                attributes:nil
                                     error:&error]) {
            ErrorLog(@"COULD NOT CREATE APPLICATION SUPPORT PATH:\n%@", result);
            result = nil;
        }
    } else {
        ErrorLog(@"Could not get application support path");
    }
    
    return [[result URLByAppendingPathComponent:@"crash.json"] path];
}

+ (instancetype) sharedInstance
{
    static id _controller = nil;
    
    if (_controller == nil) {
        static dispatch_once_t onceToken;
        dispatch_once(&onceToken, ^{
            _controller = [[[self class] alloc] init];
        });
    }
    
    return _controller;
}

-(void)handleException:(NSException *)exception
{
    [self logMobileClientCrashEventWithMessage:[NSString stringWithFormat:@"Exception: %@", [exception description]]];
}

-(void)handleSignal:(NSInteger)signal
{
    [self logMobileClientCrashEventWithMessage:[NSString stringWithFormat:@"Signal: %d", signal]];
}

-(void)logMobileClientCrashEventWithMessage:(NSString*)message
{
    NSMutableString *crashText = [NSMutableString stringWithString:message];
    
    void *callstack[128];
    
    NSInteger frames = backtrace(callstack, 128);
    char **strs = backtrace_symbols(callstack, frames);
    
    NSInteger i;
    NSMutableArray *backtrace = [NSMutableArray arrayWithCapacity:frames];
    NSUInteger start = 0;
    for (i = start; i < MIN(start + 10, frames); i++) {
        [crashText appendFormat:@"\n%s", strs[i]];
    }
    free(strs);
    
    [crashText appendFormat:@"\n\ncommit: %@", VERSION_DETAILS_GIT_HASH];
    
    NSMutableDictionary *crashContent = [NSMutableDictionary dictionary];
    crashContent[@"crashText"] = crashText;
    crashContent[@"crashTime"] = @([NSDate.date timeIntervalSince1970]);
    crashContent[@"storeID"] = [CRSLocationController getLocationKey];
    crashContent[@"version"] = NSBundle.mainBundle.infoDictionary[@"CFBundleVersion"];
    crashContent[@"workStationId"] = [TillSetupController getTillNumber];
    
    [[NSJSONSerialization dataWithJSONObject:crashContent
                                    options:0
                                      error:NULL] writeToFile:[self crashFile]
                                                   atomically:NO];
    exit(1);
}
@end
