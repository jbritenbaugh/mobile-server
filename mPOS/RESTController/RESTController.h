/* web definitions header */

#import "RESTControllerBase.h"
#import "RESTObject.h"
#import "RESTInvocation.h"

@class AddItemInvocation;
@class AddItemsInvocation;
@class AdjustBasketItemInvocation;
@class AdjustBasketItemPriceInvocation;
@class AdjustBasketItemQuantityInvocation;
@class AdjustSaleInvocation;
@class AdjustTaxFreeFormInvocation;
@class Affiliation;
@class AirportCode;
@class AirportZone;
@class AirsideOption;
@class ApplyBasketDiscountInvocation;
@class ApplyBasketItemDiscountInvocation;
@class AssignCustomerInvocation;
@class AssignEmailAddressInvocation;
@class Associate;
@class AssociateDeviceStationInvocation;
@class AssociateSalesHistoryRequest;
@class AssociateTextReceiptInvocation;
@class BasketAssociateDTO;
@class BasketAssociateInfo;
@class BasketCustomer;
@class BasketCustomerAffiliation;
@class BasketCustomerCard;
@class BasketDTO;
@class BasketFlightDetails;
@class BasketForeignSubTotalDTO;
@class BasketItem;
@class BasketPricingDTO;
@class BasketRewardLine;
@class BasketRewardLineDiscountAmount;
@class BasketRewardLineDiscountPercent;
@class BasketRewardLinePriceAdjust;
@class BasketRewardSale;
@class BasketRewardSaleDiscountAmount;
@class BasketRewardSaleDiscountPercent;
@class BasketRewardSalePromotionAmount;
@class BasketTaxFree;
@class BasketTaxFreeDocumentDTO;
@class BasketTender;
@class BasketTenderAuthorization;
@class BasketTenderForeignCurrency;
@class CancelBasketInvocation;
@class CancelCheckoutBasketInvocation;
@class CancelTaxFreeFormInvocation;
@class CardsForCustomerInvocation;
@class ChangeCashDrawerInvocation;
@class CheckoutBasketInvocation;
@class ClientSettings;
@class Configuration;
@class ControlVoidTransactionInvocation;
@class Country;
@class CreateBasketInvocation;
@class CreateCustomerEmailInvocation;
@class CreateEFTTenderInvocation;
@class CreateStandardTenderInvocation;
@class CreateVerifoneTenderInvocation;
@class CreateYespayTenderInvocation;
@class Credentials;
@class Customer;
@class CustomerAddress;
@class CustomerCard;
@class CustomerEmail;
@class CustomerIdentificationType;
@class CustomerSearch;
@class CustomersWithEmailInvocation;
@class CustomerWithCardNoInvocation;
@class CustomerWithCustomerIdInvocation;
@class DataEventDTO;
@class Device;
@class DeviceProfile;
@class DeviceStation;
@class DeviceStationPeripheral;
@class DisassociateDeviceStationInvocation;
@class DiscountCode;
@class DiscountReason;
@class DiscountReasonGroup;
@class DumpCode;
@class EFTPayment;
@class FindItemInvocation;
@class FindLineIdInvocation;
@class FinishBasketInvocation;
@class FinishTaxFreeBasketInvocation;
@class FormConfigurationDTO;
@class GetActiveBasketInvocation;
@class GetAssociateSalesHistoryInvocation;
@class GetBasketInvocation;
@class GetConfigurationInvocation;
@class GetDeviceStationInvocation;
@class GetDeviceStationsInvocation;
@class GetItemAvailabilityInvocation;
@class GetStoreDetailsInvocation;
@class GiftReceiptRequest;
@class InternetReturnInvocation;
@class Item;
@class ItemAvailability;
@class ItemAvailabilityRecord;
@class ItemImages;
@class ItemModifier;
@class ItemModifierGroup;
@class ItemProperties;
@class KeyValueDTO;
@class KeyValueGroup;
@class KeyValues;
@class Language;
@class LinkDeviceStationInvocation;
@class LinkedReturnInvocation;
@class ListStoresInvocation;
@class Localizable;
@class LocalizableTable;
@class LoginAssociateInvocation;
@class LogMobileClientCrashEventInvocation;
@class LogoutAssociateInvocation;
@class LookupSaleInvocation;
@class MandatoryModifierGroup;
@class NoReceiptInvocation;
@class NotesForCustomerInvocation;
@class OpenCashDrawerInvocation;
@class OptionalModifierGroup;
@class OverrideErrorMessage;
@class PasswordRule;
@class Payment;
@class Permission;
@class PermissionGroup;
@class POSError;
@class POSErrorMessage;
@class POSFunctionSoftKeyEventDTO;
@class PrintDataLine;
@class PrintElement;
@class ProductSearch;
@class Property;
@class RegisterCustomerInvocation;
@class RemoveBasketDiscountInvocation;
@class RemoveBasketItemInvocation;
@class RemoveBasketItemDiscountInvocation;
@class ReprintLastTxInvocation;
@class ResumeBasketInvocation;
@class ReturnableSaleItem;
@class ReturnBasketItemInvocation;
@class ReturnInstance;
@class ReturnItem;
@class ReturnWithTypeInvocation;
@class SaleItem;
@class SalesPerson;
@class SearchItemsInvocation;
@class SelectedModifier;
@class ServerError;
@class ServerErrorAffiliationRequiredMessage;
@class ServerErrorAgeRestrictionMessage;
@class ServerErrorBarcodeItemNotReturnedMessage;
@class ServerErrorDeviceStationClaimedMessage;
@class ServerErrorDeviceStationMessage;
@class ServerErrorMessage;
@class ServerErrorUnitOfMeasureMessage;
@class ServerRequest;
@class SetNotesForCustomerInvocation;
@class ShopperDetailsDTO;
@class SoftKey;
@class SoftKeyContext;
@class SoftKeyEventDTO;
@class SoftKeyInputEventDTO;
@class StandardPayment;
@class Store;
@class Stores;
@class SuspendBasketInvocation;
@class Tender;
@class Till;
@class ToggleTrainingModeInvocation;
@class UIContextEventDTO;
@class UnlinkDeviceInvocation;
@class UnlinkedReturnInvocation;
@class UpdateCustomerInvocation;
@class UpdateDeviceInvocation;
@class UpdateDeviceStationInvocation;
@class UpdateGiftReceiptInvocation;
@class VerifonePayment;
@class YespayPayment;

@protocol RESTControllerMethods <NSObject>
@optional

-(BasketAssociateDTO*)addItemWithAgeRestriction:(NSString*)ageRestriction barcodeType:(NSString*)barcodeType basketID:(NSString*)basketID entryMethod:(NSString*)entryMethod itemDescription:(NSArray*)itemDescription itemScanID:(NSString*)itemScanID itemSerialNumber:(NSString*)itemSerialNumber measure:(NSString*)measure originalItemDescription:(NSString*)originalItemDescription originalItemScanId:(NSString*)originalItemScanId price:(NSString*)price quantity:(NSString*)quantity track1:(NSString*)track1 track2:(NSString*)track2 track3:(NSString*)track3 error:(NSError **)error serverError:(ServerError **)serverError  __attribute__((deprecated));

-(BasketAssociateDTO*)addItemsWithBasketID:(NSString*)basketID entryMethod:(NSString*)entryMethod itemDescription:(NSArray*)itemDescription itemScanID:(NSArray*)itemScanID error:(NSError **)error serverError:(ServerError **)serverError  __attribute__((deprecated));

-(BasketAssociateDTO*)adjustBasketItemWithBasketID:(NSString*)basketID basketItemID:(NSArray*)basketItemID operatorID:(NSString*)operatorID error:(NSError **)error serverError:(ServerError **)serverError  __attribute__((deprecated));

-(BasketAssociateDTO*)adjustBasketItemPriceWithBasketID:(NSString*)basketID basketItemID:(NSString*)basketItemID price:(NSString*)price reasonDescription:(NSString*)reasonDescription reasonID:(NSString*)reasonID error:(NSError **)error serverError:(ServerError **)serverError  __attribute__((deprecated));

-(BasketAssociateDTO*)adjustBasketItemQuantityWithBasketID:(NSString*)basketID basketItemID:(NSString*)basketItemID quantity:(NSInteger)quantity error:(NSError **)error serverError:(ServerError **)serverError  __attribute__((deprecated));

-(BasketDTO*)adjustSaleWithBasket:(BasketDTO*)basket error:(NSError **)error serverError:(ServerError **)serverError  __attribute__((deprecated));

-(BasketTaxFreeDocumentDTO*)adjustTaxFreeFormWithBasket:(BasketTaxFreeDocumentDTO*)basket error:(NSError **)error serverError:(ServerError **)serverError  __attribute__((deprecated));

-(BasketAssociateDTO*)applyBasketDiscountWithAmount:(NSString*)amount basketID:(NSString*)basketID discountCode:(NSString*)discountCode discountType:(NSString*)discountType reasonDescription:(NSString*)reasonDescription reasonID:(NSString*)reasonID error:(NSError **)error serverError:(ServerError **)serverError  __attribute__((deprecated));

-(BasketAssociateDTO*)applyBasketItemDiscountWithAmount:(NSString*)amount basketID:(NSString*)basketID basketItemID:(NSArray*)basketItemID discountCode:(NSString*)discountCode discountType:(NSString*)discountType reasonDescription:(NSString*)reasonDescription reasonID:(NSString*)reasonID error:(NSError **)error serverError:(ServerError **)serverError  __attribute__((deprecated));

-(BasketAssociateDTO*)assignCustomerWithAffiliationType:(NSString*)affiliationType basketID:(NSString*)basketID customer:(NSString*)customer error:(NSError **)error serverError:(ServerError **)serverError  __attribute__((deprecated));

-(BasketAssociateDTO*)assignEmailAddressWithBasketID:(NSString*)basketID emailAddress:(NSString*)emailAddress error:(NSError **)error serverError:(ServerError **)serverError  __attribute__((deprecated));

-(DeviceStation*)associateDeviceStationWithDeviceStationId:(NSString*)deviceStationId tillId:(NSString*)tillId error:(NSError **)error serverError:(ServerError **)serverError  __attribute__((deprecated));

-(NSString*)associateTextReceiptWithReceiptId:(NSString*)receiptId error:(NSError **)error serverError:(ServerError **)serverError  __attribute__((deprecated));

-(BasketAssociateDTO*)cancelBasketWithBasketID:(NSString*)basketID error:(NSError **)error serverError:(ServerError **)serverError  __attribute__((deprecated));

-(BasketAssociateDTO*)cancelCheckoutBasketWithBasketID:(NSString*)basketID error:(NSError **)error serverError:(ServerError **)serverError  __attribute__((deprecated));

-(BasketTaxFreeDocumentDTO*)cancelTaxFreeFormWithBasketID:(NSString*)basketID error:(NSError **)error serverError:(ServerError **)serverError  __attribute__((deprecated));

-(NSArray*)cardsForCustomerWithCustomerId:(NSString*)customerId error:(NSError **)error serverError:(ServerError **)serverError  __attribute__((deprecated));

-(void)changeCashDrawerWithDeviceStationId:(NSString*)deviceStationId error:(NSError **)error serverError:(ServerError **)serverError  __attribute__((deprecated));

-(BasketAssociateDTO*)checkoutBasketWithBasketID:(NSString*)basketID currencyCode:(NSString*)currencyCode error:(NSError **)error serverError:(ServerError **)serverError  __attribute__((deprecated));

-(id)controlVoidTransactionWithBasketID:(NSString*)basketID businessDay:(NSDate*)businessDay confirmedAmount:(NSString*)confirmedAmount scanData:(NSString*)scanData storeID:(NSString*)storeID transactionType:(NSString*)transactionType workstationID:(NSString*)workstationID error:(NSError **)error serverError:(ServerError **)serverError  __attribute__((deprecated));

-(id)createBasketWithBasketID:(NSString*)basketID businessDay:(NSDate*)businessDay deviceID:(NSString*)deviceID issuerCountryId:(NSString*)issuerCountryId lastTransaction:(NSString*)lastTransaction scanData:(NSString*)scanData storeID:(NSString*)storeID transactionType:(NSString*)transactionType workstationID:(NSString*)workstationID error:(NSError **)error serverError:(ServerError **)serverError  __attribute__((deprecated));

-(void)createCustomerEmailWithEmailAddress:(NSString*)emailAddress includeGiftReceipt:(BOOL)includeGiftReceipt receiptID:(NSString*)receiptID error:(NSError **)error serverError:(ServerError **)serverError  __attribute__((deprecated));

-(BasketAssociateDTO*)createEFTTenderWithDeviceStationId:(NSString*)deviceStationId tender:(EFTPayment*)tender error:(NSError **)error serverError:(ServerError **)serverError  __attribute__((deprecated));

-(BasketAssociateDTO*)createStandardTenderWithTender:(StandardPayment*)tender error:(NSError **)error serverError:(ServerError **)serverError  __attribute__((deprecated));

-(BasketAssociateDTO*)createVerifoneTenderWithTender:(VerifonePayment*)tender error:(NSError **)error serverError:(ServerError **)serverError  __attribute__((deprecated));

-(BasketAssociateDTO*)createYespayTenderWithTender:(YespayPayment*)tender error:(NSError **)error serverError:(ServerError **)serverError  __attribute__((deprecated));

-(NSArray*)customersWithEmail:(NSString*)email error:(NSError **)error serverError:(ServerError **)serverError  __attribute__((deprecated));

-(Customer*)customerWithCardNo:(NSString*)cardNo cardType:(NSString*)cardType error:(NSError **)error serverError:(ServerError **)serverError  __attribute__((deprecated));

-(Customer*)customerWithCustomerId:(NSString*)customerId error:(NSError **)error serverError:(ServerError **)serverError  __attribute__((deprecated));

-(DeviceStation*)disassociateDeviceStationWithDeviceStationId:(NSString*)deviceStationId tillId:(NSString*)tillId error:(NSError **)error serverError:(ServerError **)serverError  __attribute__((deprecated));

-(Item*)findItemWithScanID:(NSString*)scanID storeID:(NSString*)storeID error:(NSError **)error serverError:(ServerError **)serverError  __attribute__((deprecated));

-(ReturnableSaleItem*)findLineIdWithItemScanId:(NSString*)itemScanId originalBasketId:(NSString*)originalBasketId originalStoreId:(NSString*)originalStoreId originalTillNumber:(NSString*)originalTillNumber originalTransactionDate:(NSString*)originalTransactionDate error:(NSError **)error serverError:(ServerError **)serverError  __attribute__((deprecated));

-(BasketAssociateDTO*)finishBasketWithBasketID:(NSString*)basketID deviceStationId:(NSString*)deviceStationId emailAddress:(NSString*)emailAddress emailReceipt:(BOOL)emailReceipt enablePrint:(BOOL)enablePrint printSummaryReceipt:(BOOL)printSummaryReceipt tillNumber:(NSString*)tillNumber error:(NSError **)error serverError:(ServerError **)serverError  __attribute__((deprecated));

-(BasketTaxFreeDocumentDTO*)finishTaxFreeBasketWithBasketID:(NSString*)basketID deviceStationId:(NSString*)deviceStationId emailAddress:(NSString*)emailAddress emailReceipt:(BOOL)emailReceipt enablePrint:(BOOL)enablePrint printSummaryReceipt:(BOOL)printSummaryReceipt tillNumber:(NSString*)tillNumber error:(NSError **)error serverError:(ServerError **)serverError  __attribute__((deprecated));

-(BasketAssociateDTO*)getActiveBasketWithDeviceID:(NSString*)deviceID error:(NSError **)error serverError:(ServerError **)serverError  __attribute__((deprecated));

-(NSArray*)getAssociateSalesHistoryWithAssociateId:(NSString*)associateId from:(NSDate*)from locationId:(NSString*)locationId status:(NSString*)status to:(NSDate*)to error:(NSError **)error serverError:(ServerError **)serverError  __attribute__((deprecated));

-(BasketAssociateDTO*)getBasketWithBasketID:(NSString*)basketID error:(NSError **)error serverError:(ServerError **)serverError  __attribute__((deprecated));

-(Configuration*)getConfigurationWithClearServerCache:(BOOL)clearServerCache storeID:(NSString*)storeID tillNumber:(NSString*)tillNumber error:(NSError **)error serverError:(ServerError **)serverError etag:(NSString **)etag  __attribute__((deprecated));

-(DeviceStation*)getDeviceStationWithDeviceStationId:(NSString*)deviceStationId error:(NSError **)error serverError:(ServerError **)serverError  __attribute__((deprecated));

-(NSArray*)getDeviceStationsWithDeviceType:(NSString*)deviceType storeId:(NSString*)storeId error:(NSError **)error serverError:(ServerError **)serverError  __attribute__((deprecated));

-(NSArray*)getItemAvailabilityWithScanID:(NSString*)scanID storeID:(NSString*)storeID error:(NSError **)error serverError:(ServerError **)serverError  __attribute__((deprecated));

-(Store*)getStoreDetailsWithStoreId:(NSString*)storeId server:(NSString *)server error:(NSError **)error serverError:(ServerError **)serverError  __attribute__((deprecated));

-(BasketAssociateDTO*)internetReturnWithBasketId:(NSString*)basketId itemScanId:(NSString*)itemScanId itemSerialNumber:(NSString*)itemSerialNumber originalItemDescription:(NSString*)originalItemDescription originalItemScanId:(NSString*)originalItemScanId originalReferenceId:(NSString*)originalReferenceId price:(NSString*)price reasonDescription:(NSString*)reasonDescription reasonId:(NSString*)reasonId error:(NSError **)error serverError:(ServerError **)serverError  __attribute__((deprecated));

-(DeviceStation*)linkDeviceStationWithDeviceStationId:(NSString*)deviceStationId force:(BOOL)force error:(NSError **)error serverError:(ServerError **)serverError  __attribute__((deprecated));

-(BasketAssociateDTO*)linkedReturnWithBasketId:(NSString*)basketId itemScanId:(NSString*)itemScanId itemSerialNumber:(NSString*)itemSerialNumber lineId:(NSString*)lineId originalItemDescription:(NSString*)originalItemDescription originalItemScanId:(NSString*)originalItemScanId originalTransactionDate:(NSString*)originalTransactionDate originalTransactionId:(NSString*)originalTransactionId originalTransactionStoreId:(NSString*)originalTransactionStoreId originalTransactionTill:(NSString*)originalTransactionTill reasonDescription:(NSString*)reasonDescription reasonId:(NSString*)reasonId error:(NSError **)error serverError:(ServerError **)serverError  __attribute__((deprecated));

-(Stores*)listStoresWithBrandId:(NSString*)brandId isHomeStore:(BOOL)isHomeStore latitude:(double)latitude longitude:(double)longitude server:(NSString *)server error:(NSError **)error serverError:(ServerError **)serverError  __attribute__((deprecated));

-(Credentials*)loginAssociateWithCredentials:(Credentials*)credentials signOffExistingOperator:(BOOL)signOffExistingOperator error:(NSError **)error serverError:(ServerError **)serverError  __attribute__((deprecated));

-(void)logMobileClientCrashEventWithCrashText:(NSString*)crashText crashTime:(NSDate*)crashTime storeID:(NSString*)storeID version:(NSString*)version workStationId:(NSString*)workStationId error:(NSError **)error serverError:(ServerError **)serverError  __attribute__((deprecated));

-(Credentials*)logoutAssociateWithCredentials:(Credentials*)credentials error:(NSError **)error serverError:(ServerError **)serverError  __attribute__((deprecated));

-(BasketAssociateDTO*)lookupSaleWithBarcode:(NSString*)barcode originalBasketId:(NSString*)originalBasketId originalStoreId:(NSString*)originalStoreId originalTillNumber:(NSString*)originalTillNumber originalTransactionDate:(NSString*)originalTransactionDate error:(NSError **)error serverError:(ServerError **)serverError  __attribute__((deprecated));

-(BasketAssociateDTO*)noReceiptWithBasketId:(NSString*)basketId itemScanId:(NSString*)itemScanId itemSerialNumber:(NSString*)itemSerialNumber originalItemDescription:(NSString*)originalItemDescription originalItemScanId:(NSString*)originalItemScanId price:(NSString*)price reasonDescription:(NSString*)reasonDescription reasonId:(NSString*)reasonId error:(NSError **)error serverError:(ServerError **)serverError  __attribute__((deprecated));

-(NSArray*)notesForCustomerWithCustomerId:(NSString*)customerId error:(NSError **)error serverError:(ServerError **)serverError  __attribute__((deprecated));

-(void)openCashDrawerWithDeviceStationId:(NSString*)deviceStationId error:(NSError **)error serverError:(ServerError **)serverError  __attribute__((deprecated));

-(Customer*)registerCustomerWithCustomer:(Customer*)customer error:(NSError **)error serverError:(ServerError **)serverError  __attribute__((deprecated));

-(BasketAssociateDTO*)removeBasketDiscountWithBasketDiscountID:(NSArray*)basketDiscountID basketID:(NSString*)basketID error:(NSError **)error serverError:(ServerError **)serverError  __attribute__((deprecated));

-(BasketAssociateDTO*)removeBasketItemWithBasketID:(NSString*)basketID basketItemID:(NSArray*)basketItemID deviceStationId:(NSString*)deviceStationId error:(NSError **)error serverError:(ServerError **)serverError  __attribute__((deprecated));

-(BasketAssociateDTO*)removeBasketItemDiscountWithBasketID:(NSString*)basketID basketItemDiscountID:(NSArray*)basketItemDiscountID basketItemID:(NSString*)basketItemID error:(NSError **)error serverError:(ServerError **)serverError  __attribute__((deprecated));

-(BasketAssociateDTO*)reprintLastTxWithDeviceStationId:(NSString*)deviceStationId error:(NSError **)error serverError:(ServerError **)serverError  __attribute__((deprecated));

-(BasketAssociateDTO*)resumeBasketWithBasketBarcode:(NSString*)basketBarcode basketID:(NSString*)basketID deviceID:(NSString*)deviceID tillNumber:(NSString*)tillNumber error:(NSError **)error serverError:(ServerError **)serverError  __attribute__((deprecated));

-(BasketAssociateDTO*)returnBasketItemWithBasketID:(NSString*)basketID itemScanID:(NSString*)itemScanID itemSerialNumber:(NSString*)itemSerialNumber originalItemDescription:(NSString*)originalItemDescription originalItemScanId:(NSString*)originalItemScanId originalTransactionBarcode:(NSString*)originalTransactionBarcode originalTransactionDate:(NSDate*)originalTransactionDate originalTransactionId:(NSNumber*)originalTransactionId originalTransactionStoreId:(NSNumber*)originalTransactionStoreId originalTransactionTill:(NSNumber*)originalTransactionTill price:(NSString*)price reasonId:(NSString*)reasonId error:(NSError **)error serverError:(ServerError **)serverError  __attribute__((deprecated));

-(BasketAssociateDTO*)returnWithBasketId:(NSString*)basketId itemScanId:(NSString*)itemScanId itemSerialNumber:(NSString*)itemSerialNumber lineId:(NSString*)lineId originalCustomerCard:(NSString*)originalCustomerCard originalItemDescription:(NSString*)originalItemDescription originalItemScanId:(NSString*)originalItemScanId originalReferenceId:(NSString*)originalReferenceId originalSalesPerson:(NSString*)originalSalesPerson originalTransactionDate:(NSString*)originalTransactionDate originalTransactionId:(NSString*)originalTransactionId originalTransactionStoreId:(NSString*)originalTransactionStoreId originalTransactionTill:(NSString*)originalTransactionTill price:(NSString*)price reasonDescription:(NSString*)reasonDescription reasonId:(NSString*)reasonId type:(NSString*)type error:(NSError **)error serverError:(ServerError **)serverError  __attribute__((deprecated));

-(NSArray*)searchItemsWithDepartment:(NSString*)department itemDescription:(NSString*)itemDescription pageNumber:(NSInteger)pageNumber storeID:(NSString*)storeID error:(NSError **)error serverError:(ServerError **)serverError  __attribute__((deprecated));

-(NSArray*)setNotesForCustomerWithCustomerId:(NSString*)customerId customerNotes:(NSArray*)customerNotes error:(NSError **)error serverError:(ServerError **)serverError  __attribute__((deprecated));

-(BasketAssociateDTO*)suspendBasketWithBasketDescription:(NSString*)basketDescription basketID:(NSString*)basketID error:(NSError **)error serverError:(ServerError **)serverError  __attribute__((deprecated));

-(BOOL)toggleTrainingModeError:(NSError **)error serverError:(ServerError **)serverError  __attribute__((deprecated));

-(DeviceStation*)unlinkDeviceWithDeviceStationId:(NSString*)deviceStationId error:(NSError **)error serverError:(ServerError **)serverError  __attribute__((deprecated));

-(BasketAssociateDTO*)unlinkedReturnWithBasketId:(NSString*)basketId itemScanId:(NSString*)itemScanId itemSerialNumber:(NSString*)itemSerialNumber originalCustomerCard:(NSString*)originalCustomerCard originalItemDescription:(NSString*)originalItemDescription originalItemScanId:(NSString*)originalItemScanId originalSalesPerson:(NSString*)originalSalesPerson originalTransactionDate:(NSString*)originalTransactionDate originalTransactionId:(NSString*)originalTransactionId originalTransactionStoreId:(NSString*)originalTransactionStoreId originalTransactionTill:(NSString*)originalTransactionTill price:(NSString*)price reasonDescription:(NSString*)reasonDescription reasonId:(NSString*)reasonId error:(NSError **)error serverError:(ServerError **)serverError  __attribute__((deprecated));

-(void)updateCustomerWithCustomer:(Customer*)customer customerId:(NSString*)customerId error:(NSError **)error serverError:(ServerError **)serverError  __attribute__((deprecated));

-(void)updateDeviceWithDevice:(DeviceProfile*)device server:(NSString *)server error:(NSError **)error serverError:(ServerError **)serverError  __attribute__((deprecated));

-(DeviceStation*)updateDeviceStationWithDeviceStation:(DeviceStation*)deviceStation deviceStationId:(NSString*)deviceStationId error:(NSError **)error serverError:(ServerError **)serverError  __attribute__((deprecated));

-(BasketAssociateDTO*)updateGiftReceiptWithBasket:(BasketAssociateDTO*)basket error:(NSError **)error serverError:(ServerError **)serverError  __attribute__((deprecated));

-(void)addItemWithAgeRestriction:(NSString*)ageRestriction barcodeType:(NSString*)barcodeType basketID:(NSString*)basketID entryMethod:(NSString*)entryMethod itemDescription:(NSArray*)itemDescription itemScanID:(NSString*)itemScanID itemSerialNumber:(NSString*)itemSerialNumber measure:(NSString*)measure originalItemDescription:(NSString*)originalItemDescription originalItemScanId:(NSString*)originalItemScanId price:(NSString*)price quantity:(NSString*)quantity track1:(NSString*)track1 track2:(NSString*)track2 track3:(NSString*)track3 complete:(void (^)(BasketAssociateDTO* result, NSError* error, ServerError* serverError))complete;

-(void)addItemsWithBasketID:(NSString*)basketID entryMethod:(NSString*)entryMethod itemDescription:(NSArray*)itemDescription itemScanID:(NSArray*)itemScanID complete:(void (^)(BasketAssociateDTO* result, NSError* error, ServerError* serverError))complete;

-(void)adjustBasketItemWithBasketID:(NSString*)basketID basketItemID:(NSArray*)basketItemID operatorID:(NSString*)operatorID complete:(void (^)(BasketAssociateDTO* result, NSError* error, ServerError* serverError))complete;

-(void)adjustBasketItemPriceWithBasketID:(NSString*)basketID basketItemID:(NSString*)basketItemID price:(NSString*)price reasonDescription:(NSString*)reasonDescription reasonID:(NSString*)reasonID complete:(void (^)(BasketAssociateDTO* result, NSError* error, ServerError* serverError))complete;

-(void)adjustBasketItemQuantityWithBasketID:(NSString*)basketID basketItemID:(NSString*)basketItemID quantity:(NSInteger)quantity complete:(void (^)(BasketAssociateDTO* result, NSError* error, ServerError* serverError))complete;

-(void)adjustSaleWithBasket:(BasketDTO*)basket complete:(void (^)(BasketDTO* result, NSError* error, ServerError* serverError))complete;

-(void)adjustTaxFreeFormWithBasket:(BasketTaxFreeDocumentDTO*)basket complete:(void (^)(BasketTaxFreeDocumentDTO* result, NSError* error, ServerError* serverError))complete;

-(void)applyBasketDiscountWithAmount:(NSString*)amount basketID:(NSString*)basketID discountCode:(NSString*)discountCode discountType:(NSString*)discountType reasonDescription:(NSString*)reasonDescription reasonID:(NSString*)reasonID complete:(void (^)(BasketAssociateDTO* result, NSError* error, ServerError* serverError))complete;

-(void)applyBasketItemDiscountWithAmount:(NSString*)amount basketID:(NSString*)basketID basketItemID:(NSArray*)basketItemID discountCode:(NSString*)discountCode discountType:(NSString*)discountType reasonDescription:(NSString*)reasonDescription reasonID:(NSString*)reasonID complete:(void (^)(BasketAssociateDTO* result, NSError* error, ServerError* serverError))complete;

-(void)assignCustomerWithAffiliationType:(NSString*)affiliationType basketID:(NSString*)basketID customer:(NSString*)customer complete:(void (^)(BasketAssociateDTO* result, NSError* error, ServerError* serverError))complete;

-(void)assignEmailAddressWithBasketID:(NSString*)basketID emailAddress:(NSString*)emailAddress complete:(void (^)(BasketAssociateDTO* result, NSError* error, ServerError* serverError))complete;

-(void)associateDeviceStationWithDeviceStationId:(NSString*)deviceStationId tillId:(NSString*)tillId complete:(void (^)(DeviceStation* result, NSError* error, ServerError* serverError))complete;

-(void)associateTextReceiptWithReceiptId:(NSString*)receiptId complete:(void (^)(NSString* result, NSError* error, ServerError* serverError))complete;

-(void)cancelBasketWithBasketID:(NSString*)basketID complete:(void (^)(BasketAssociateDTO* result, NSError* error, ServerError* serverError))complete;

-(void)cancelCheckoutBasketWithBasketID:(NSString*)basketID complete:(void (^)(BasketAssociateDTO* result, NSError* error, ServerError* serverError))complete;

-(void)cancelTaxFreeFormWithBasketID:(NSString*)basketID complete:(void (^)(BasketTaxFreeDocumentDTO* result, NSError* error, ServerError* serverError))complete;

-(void)cardsForCustomerWithCustomerId:(NSString*)customerId complete:(void (^)(NSArray* result, NSError* error, ServerError* serverError))complete;

-(void)changeCashDrawerWithDeviceStationId:(NSString*)deviceStationId complete:(void (^)(NSError* error, ServerError* serverError))complete;

-(void)checkoutBasketWithBasketID:(NSString*)basketID currencyCode:(NSString*)currencyCode complete:(void (^)(BasketAssociateDTO* result, NSError* error, ServerError* serverError))complete;

-(void)controlVoidTransactionWithBasketID:(NSString*)basketID businessDay:(NSDate*)businessDay confirmedAmount:(NSString*)confirmedAmount scanData:(NSString*)scanData storeID:(NSString*)storeID transactionType:(NSString*)transactionType workstationID:(NSString*)workstationID complete:(void (^)(id result, NSError* error, ServerError* serverError))complete;

-(void)createBasketWithBasketID:(NSString*)basketID businessDay:(NSDate*)businessDay deviceID:(NSString*)deviceID issuerCountryId:(NSString*)issuerCountryId lastTransaction:(NSString*)lastTransaction scanData:(NSString*)scanData storeID:(NSString*)storeID transactionType:(NSString*)transactionType workstationID:(NSString*)workstationID complete:(void (^)(id result, NSError* error, ServerError* serverError))complete;

-(void)createCustomerEmailWithEmailAddress:(NSString*)emailAddress includeGiftReceipt:(BOOL)includeGiftReceipt receiptID:(NSString*)receiptID complete:(void (^)(NSError* error, ServerError* serverError))complete;

-(void)createEFTTenderWithDeviceStationId:(NSString*)deviceStationId tender:(EFTPayment*)tender complete:(void (^)(BasketAssociateDTO* result, NSError* error, ServerError* serverError))complete;

-(void)createStandardTenderWithTender:(StandardPayment*)tender complete:(void (^)(BasketAssociateDTO* result, NSError* error, ServerError* serverError))complete;

-(void)createVerifoneTenderWithTender:(VerifonePayment*)tender complete:(void (^)(BasketAssociateDTO* result, NSError* error, ServerError* serverError))complete;

-(void)createYespayTenderWithTender:(YespayPayment*)tender complete:(void (^)(BasketAssociateDTO* result, NSError* error, ServerError* serverError))complete;

-(void)customersWithEmail:(NSString*)email complete:(void (^)(NSArray* result, NSError* error, ServerError* serverError))complete;

-(void)customerWithCardNo:(NSString*)cardNo cardType:(NSString*)cardType complete:(void (^)(Customer* result, NSError* error, ServerError* serverError))complete;

-(void)customerWithCustomerId:(NSString*)customerId complete:(void (^)(Customer* result, NSError* error, ServerError* serverError))complete;

-(void)disassociateDeviceStationWithDeviceStationId:(NSString*)deviceStationId tillId:(NSString*)tillId complete:(void (^)(DeviceStation* result, NSError* error, ServerError* serverError))complete;

-(void)findItemWithScanID:(NSString*)scanID storeID:(NSString*)storeID complete:(void (^)(Item* result, NSError* error, ServerError* serverError))complete;

-(void)findLineIdWithItemScanId:(NSString*)itemScanId originalBasketId:(NSString*)originalBasketId originalStoreId:(NSString*)originalStoreId originalTillNumber:(NSString*)originalTillNumber originalTransactionDate:(NSString*)originalTransactionDate complete:(void (^)(ReturnableSaleItem* result, NSError* error, ServerError* serverError))complete;

-(void)finishBasketWithBasketID:(NSString*)basketID deviceStationId:(NSString*)deviceStationId emailAddress:(NSString*)emailAddress emailReceipt:(BOOL)emailReceipt enablePrint:(BOOL)enablePrint printSummaryReceipt:(BOOL)printSummaryReceipt tillNumber:(NSString*)tillNumber complete:(void (^)(BasketAssociateDTO* result, NSError* error, ServerError* serverError))complete;

-(void)finishTaxFreeBasketWithBasketID:(NSString*)basketID deviceStationId:(NSString*)deviceStationId emailAddress:(NSString*)emailAddress emailReceipt:(BOOL)emailReceipt enablePrint:(BOOL)enablePrint printSummaryReceipt:(BOOL)printSummaryReceipt tillNumber:(NSString*)tillNumber complete:(void (^)(BasketTaxFreeDocumentDTO* result, NSError* error, ServerError* serverError))complete;

-(void)getActiveBasketWithDeviceID:(NSString*)deviceID complete:(void (^)(BasketAssociateDTO* result, NSError* error, ServerError* serverError))complete;

-(void)getAssociateSalesHistoryWithAssociateId:(NSString*)associateId from:(NSDate*)from locationId:(NSString*)locationId status:(NSString*)status to:(NSDate*)to complete:(void (^)(NSArray* result, NSError* error, ServerError* serverError))complete;

-(void)getBasketWithBasketID:(NSString*)basketID complete:(void (^)(BasketAssociateDTO* result, NSError* error, ServerError* serverError))complete;

-(void)getConfigurationWithClearServerCache:(BOOL)clearServerCache storeID:(NSString*)storeID tillNumber:(NSString*)tillNumber etag:(NSString *)etag complete:(void (^)(Configuration* result, NSError* error, ServerError* serverError, NSString* etag))complete;

-(void)getDeviceStationWithDeviceStationId:(NSString*)deviceStationId complete:(void (^)(DeviceStation* result, NSError* error, ServerError* serverError))complete;

-(void)getDeviceStationsWithDeviceType:(NSString*)deviceType storeId:(NSString*)storeId complete:(void (^)(NSArray* result, NSError* error, ServerError* serverError))complete;

-(void)getItemAvailabilityWithScanID:(NSString*)scanID storeID:(NSString*)storeID complete:(void (^)(NSArray* result, NSError* error, ServerError* serverError))complete;

-(void)getStoreDetailsWithStoreId:(NSString*)storeId server:(NSString *)server complete:(void (^)(Store* result, NSError* error, ServerError* serverError))complete;

-(void)internetReturnWithBasketId:(NSString*)basketId itemScanId:(NSString*)itemScanId itemSerialNumber:(NSString*)itemSerialNumber originalItemDescription:(NSString*)originalItemDescription originalItemScanId:(NSString*)originalItemScanId originalReferenceId:(NSString*)originalReferenceId price:(NSString*)price reasonDescription:(NSString*)reasonDescription reasonId:(NSString*)reasonId complete:(void (^)(BasketAssociateDTO* result, NSError* error, ServerError* serverError))complete;

-(void)linkDeviceStationWithDeviceStationId:(NSString*)deviceStationId force:(BOOL)force complete:(void (^)(DeviceStation* result, NSError* error, ServerError* serverError))complete;

-(void)linkedReturnWithBasketId:(NSString*)basketId itemScanId:(NSString*)itemScanId itemSerialNumber:(NSString*)itemSerialNumber lineId:(NSString*)lineId originalItemDescription:(NSString*)originalItemDescription originalItemScanId:(NSString*)originalItemScanId originalTransactionDate:(NSString*)originalTransactionDate originalTransactionId:(NSString*)originalTransactionId originalTransactionStoreId:(NSString*)originalTransactionStoreId originalTransactionTill:(NSString*)originalTransactionTill reasonDescription:(NSString*)reasonDescription reasonId:(NSString*)reasonId complete:(void (^)(BasketAssociateDTO* result, NSError* error, ServerError* serverError))complete;

-(void)listStoresWithBrandId:(NSString*)brandId isHomeStore:(BOOL)isHomeStore latitude:(double)latitude longitude:(double)longitude server:(NSString *)server complete:(void (^)(Stores* result, NSError* error, ServerError* serverError))complete;

-(void)loginAssociateWithCredentials:(Credentials*)credentials signOffExistingOperator:(BOOL)signOffExistingOperator complete:(void (^)(Credentials* result, NSError* error, ServerError* serverError))complete;

-(void)logMobileClientCrashEventWithCrashText:(NSString*)crashText crashTime:(NSDate*)crashTime storeID:(NSString*)storeID version:(NSString*)version workStationId:(NSString*)workStationId complete:(void (^)(NSError* error, ServerError* serverError))complete;

-(void)logoutAssociateWithCredentials:(Credentials*)credentials complete:(void (^)(Credentials* result, NSError* error, ServerError* serverError))complete;

-(void)lookupSaleWithBarcode:(NSString*)barcode originalBasketId:(NSString*)originalBasketId originalStoreId:(NSString*)originalStoreId originalTillNumber:(NSString*)originalTillNumber originalTransactionDate:(NSString*)originalTransactionDate complete:(void (^)(BasketAssociateDTO* result, NSError* error, ServerError* serverError))complete;

-(void)noReceiptWithBasketId:(NSString*)basketId itemScanId:(NSString*)itemScanId itemSerialNumber:(NSString*)itemSerialNumber originalItemDescription:(NSString*)originalItemDescription originalItemScanId:(NSString*)originalItemScanId price:(NSString*)price reasonDescription:(NSString*)reasonDescription reasonId:(NSString*)reasonId complete:(void (^)(BasketAssociateDTO* result, NSError* error, ServerError* serverError))complete;

-(void)notesForCustomerWithCustomerId:(NSString*)customerId complete:(void (^)(NSArray* result, NSError* error, ServerError* serverError))complete;

-(void)openCashDrawerWithDeviceStationId:(NSString*)deviceStationId complete:(void (^)(NSError* error, ServerError* serverError))complete;

-(void)registerCustomerWithCustomer:(Customer*)customer complete:(void (^)(Customer* result, NSError* error, ServerError* serverError))complete;

-(void)removeBasketDiscountWithBasketDiscountID:(NSArray*)basketDiscountID basketID:(NSString*)basketID complete:(void (^)(BasketAssociateDTO* result, NSError* error, ServerError* serverError))complete;

-(void)removeBasketItemWithBasketID:(NSString*)basketID basketItemID:(NSArray*)basketItemID deviceStationId:(NSString*)deviceStationId complete:(void (^)(BasketAssociateDTO* result, NSError* error, ServerError* serverError))complete;

-(void)removeBasketItemDiscountWithBasketID:(NSString*)basketID basketItemDiscountID:(NSArray*)basketItemDiscountID basketItemID:(NSString*)basketItemID complete:(void (^)(BasketAssociateDTO* result, NSError* error, ServerError* serverError))complete;

-(void)reprintLastTxWithDeviceStationId:(NSString*)deviceStationId complete:(void (^)(BasketAssociateDTO* result, NSError* error, ServerError* serverError))complete;

-(void)resumeBasketWithBasketBarcode:(NSString*)basketBarcode basketID:(NSString*)basketID deviceID:(NSString*)deviceID tillNumber:(NSString*)tillNumber complete:(void (^)(BasketAssociateDTO* result, NSError* error, ServerError* serverError))complete;

-(void)returnBasketItemWithBasketID:(NSString*)basketID itemScanID:(NSString*)itemScanID itemSerialNumber:(NSString*)itemSerialNumber originalItemDescription:(NSString*)originalItemDescription originalItemScanId:(NSString*)originalItemScanId originalTransactionBarcode:(NSString*)originalTransactionBarcode originalTransactionDate:(NSDate*)originalTransactionDate originalTransactionId:(NSNumber*)originalTransactionId originalTransactionStoreId:(NSNumber*)originalTransactionStoreId originalTransactionTill:(NSNumber*)originalTransactionTill price:(NSString*)price reasonId:(NSString*)reasonId complete:(void (^)(BasketAssociateDTO* result, NSError* error, ServerError* serverError))complete;

-(void)returnWithBasketId:(NSString*)basketId itemScanId:(NSString*)itemScanId itemSerialNumber:(NSString*)itemSerialNumber lineId:(NSString*)lineId originalCustomerCard:(NSString*)originalCustomerCard originalItemDescription:(NSString*)originalItemDescription originalItemScanId:(NSString*)originalItemScanId originalReferenceId:(NSString*)originalReferenceId originalSalesPerson:(NSString*)originalSalesPerson originalTransactionDate:(NSString*)originalTransactionDate originalTransactionId:(NSString*)originalTransactionId originalTransactionStoreId:(NSString*)originalTransactionStoreId originalTransactionTill:(NSString*)originalTransactionTill price:(NSString*)price reasonDescription:(NSString*)reasonDescription reasonId:(NSString*)reasonId type:(NSString*)type complete:(void (^)(BasketAssociateDTO* result, NSError* error, ServerError* serverError))complete;

-(void)searchItemsWithDepartment:(NSString*)department itemDescription:(NSString*)itemDescription pageNumber:(NSInteger)pageNumber storeID:(NSString*)storeID complete:(void (^)(NSArray* result, NSError* error, ServerError* serverError))complete;

-(void)setNotesForCustomerWithCustomerId:(NSString*)customerId customerNotes:(NSArray*)customerNotes complete:(void (^)(NSArray* result, NSError* error, ServerError* serverError))complete;

-(void)suspendBasketWithBasketDescription:(NSString*)basketDescription basketID:(NSString*)basketID complete:(void (^)(BasketAssociateDTO* result, NSError* error, ServerError* serverError))complete;

-(void)toggleTrainingModeComplete:(void (^)(BOOL result, NSError* error, ServerError* serverError))complete;

-(void)unlinkDeviceWithDeviceStationId:(NSString*)deviceStationId complete:(void (^)(DeviceStation* result, NSError* error, ServerError* serverError))complete;

-(void)unlinkedReturnWithBasketId:(NSString*)basketId itemScanId:(NSString*)itemScanId itemSerialNumber:(NSString*)itemSerialNumber originalCustomerCard:(NSString*)originalCustomerCard originalItemDescription:(NSString*)originalItemDescription originalItemScanId:(NSString*)originalItemScanId originalSalesPerson:(NSString*)originalSalesPerson originalTransactionDate:(NSString*)originalTransactionDate originalTransactionId:(NSString*)originalTransactionId originalTransactionStoreId:(NSString*)originalTransactionStoreId originalTransactionTill:(NSString*)originalTransactionTill price:(NSString*)price reasonDescription:(NSString*)reasonDescription reasonId:(NSString*)reasonId complete:(void (^)(BasketAssociateDTO* result, NSError* error, ServerError* serverError))complete;

-(void)updateCustomerWithCustomer:(Customer*)customer customerId:(NSString*)customerId complete:(void (^)(NSError* error, ServerError* serverError))complete;

-(void)updateDeviceWithDevice:(DeviceProfile*)device server:(NSString *)server complete:(void (^)(NSError* error, ServerError* serverError))complete;

-(void)updateDeviceStationWithDeviceStation:(DeviceStation*)deviceStation deviceStationId:(NSString*)deviceStationId complete:(void (^)(DeviceStation* result, NSError* error, ServerError* serverError))complete;

-(void)updateGiftReceiptWithBasket:(BasketAssociateDTO*)basket complete:(void (^)(BasketAssociateDTO* result, NSError* error, ServerError* serverError))complete;

@end

@interface RESTController : RESTControllerBase <RESTControllerMethods>
@end

@interface AddItemInvocation : RESTInvocation
@property (nonatomic, strong) NSString* ageRestriction;
@property (nonatomic, strong) NSString* barcodeType;
@property (nonatomic, strong) NSString* basketID;
@property (nonatomic, strong) NSString* entryMethod;
@property (nonatomic, strong) NSArray* itemDescription;
@property (nonatomic, strong) NSString* itemScanID;
@property (nonatomic, strong) NSString* itemSerialNumber;
@property (nonatomic, strong) NSString* measure;
@property (nonatomic, strong) NSString* originalItemDescription;
@property (nonatomic, strong) NSString* originalItemScanId;
@property (nonatomic, strong) NSString* price;
@property (nonatomic, strong) NSString* quantity;
@property (nonatomic, strong) NSString* track1;
@property (nonatomic, strong) NSString* track2;
@property (nonatomic, strong) NSString* track3;
@end

@interface AddItemsInvocation : RESTInvocation
@property (nonatomic, strong) NSString* basketID;
@property (nonatomic, strong) NSString* entryMethod;
@property (nonatomic, strong) NSArray* itemDescription;
@property (nonatomic, strong) NSArray* itemScanID;
@end

@interface AdjustBasketItemInvocation : RESTInvocation
@property (nonatomic, strong) NSString* basketID;
@property (nonatomic, strong) NSArray* basketItemID;
@property (nonatomic, strong) NSString* operatorID;
@end

@interface AdjustBasketItemPriceInvocation : RESTInvocation
@property (nonatomic, strong) NSString* basketID;
@property (nonatomic, strong) NSString* basketItemID;
@property (nonatomic, strong) NSString* price;
@property (nonatomic, strong) NSString* reasonDescription;
@property (nonatomic, strong) NSString* reasonID;
@end

@interface AdjustBasketItemQuantityInvocation : RESTInvocation
@property (nonatomic, strong) NSString* basketID;
@property (nonatomic, strong) NSString* basketItemID;
@property (nonatomic, assign) NSInteger quantity;
@end

@interface AdjustSaleInvocation : RESTInvocation
@property (nonatomic, strong) BasketDTO* basket;
@end

@interface AdjustTaxFreeFormInvocation : RESTInvocation
@property (nonatomic, strong) BasketTaxFreeDocumentDTO* basket;
@end

@interface ApplyBasketDiscountInvocation : RESTInvocation
@property (nonatomic, strong) NSString* amount;
@property (nonatomic, strong) NSString* basketID;
@property (nonatomic, strong) NSString* discountCode;
@property (nonatomic, strong) NSString* discountType;
@property (nonatomic, strong) NSString* reasonDescription;
@property (nonatomic, strong) NSString* reasonID;
@end

@interface ApplyBasketItemDiscountInvocation : RESTInvocation
@property (nonatomic, strong) NSString* amount;
@property (nonatomic, strong) NSString* basketID;
@property (nonatomic, strong) NSArray* basketItemID;
@property (nonatomic, strong) NSString* discountCode;
@property (nonatomic, strong) NSString* discountType;
@property (nonatomic, strong) NSString* reasonDescription;
@property (nonatomic, strong) NSString* reasonID;
@end

@interface AssignCustomerInvocation : RESTInvocation
@property (nonatomic, strong) NSString* affiliationType;
@property (nonatomic, strong) NSString* basketID;
@property (nonatomic, strong) NSString* customer;
@end

@interface AssignEmailAddressInvocation : RESTInvocation
@property (nonatomic, strong) NSString* basketID;
@property (nonatomic, strong) NSString* emailAddress;
@end

@interface AssociateDeviceStationInvocation : RESTInvocation
@property (nonatomic, strong) NSString* deviceStationId;
@property (nonatomic, strong) NSString* tillId;
@end

@interface AssociateTextReceiptInvocation : RESTInvocation
@property (nonatomic, strong) NSString* receiptId;
@end

@interface CancelBasketInvocation : RESTInvocation
@property (nonatomic, strong) NSString* basketID;
@end

@interface CancelCheckoutBasketInvocation : RESTInvocation
@property (nonatomic, strong) NSString* basketID;
@end

@interface CancelTaxFreeFormInvocation : RESTInvocation
@property (nonatomic, strong) NSString* basketID;
@end

@interface CardsForCustomerInvocation : RESTInvocation
@property (nonatomic, strong) NSString* customerId;
@end

@interface ChangeCashDrawerInvocation : RESTInvocation
@property (nonatomic, strong) NSString* deviceStationId;
@end

@interface CheckoutBasketInvocation : RESTInvocation
@property (nonatomic, strong) NSString* basketID;
@property (nonatomic, strong) NSString* currencyCode;
@end

@interface ControlVoidTransactionInvocation : RESTInvocation
@property (nonatomic, strong) NSString* basketID;
@property (nonatomic, strong) NSDate* businessDay;
@property (nonatomic, strong) NSString* confirmedAmount;
@property (nonatomic, strong) NSString* scanData;
@property (nonatomic, strong) NSString* storeID;
@property (nonatomic, strong) NSString* transactionType;
@property (nonatomic, strong) NSString* workstationID;
@end

@interface CreateBasketInvocation : RESTInvocation
@property (nonatomic, strong) NSString* basketID;
@property (nonatomic, strong) NSDate* businessDay;
@property (nonatomic, strong) NSString* deviceID;
@property (nonatomic, strong) NSString* issuerCountryId;
@property (nonatomic, strong) NSString* lastTransaction;
@property (nonatomic, strong) NSString* scanData;
@property (nonatomic, strong) NSString* storeID;
@property (nonatomic, strong) NSString* transactionType;
@property (nonatomic, strong) NSString* workstationID;
@end

@interface CreateCustomerEmailInvocation : RESTInvocation
@property (nonatomic, strong) NSString* emailAddress;
@property (nonatomic, assign) BOOL includeGiftReceipt;
@property (nonatomic, strong) NSString* receiptID;
@end

@interface CreateEFTTenderInvocation : RESTInvocation
@property (nonatomic, strong) NSString* deviceStationId;
@property (nonatomic, strong) EFTPayment* tender;
@end

@interface CreateStandardTenderInvocation : RESTInvocation
@property (nonatomic, strong) StandardPayment* tender;
@end

@interface CreateVerifoneTenderInvocation : RESTInvocation
@property (nonatomic, strong) VerifonePayment* tender;
@end

@interface CreateYespayTenderInvocation : RESTInvocation
@property (nonatomic, strong) YespayPayment* tender;
@end

@interface CustomersWithEmailInvocation : RESTInvocation
@property (nonatomic, strong) NSString* email;
@end

@interface CustomerWithCardNoInvocation : RESTInvocation
@property (nonatomic, strong) NSString* cardNo;
@property (nonatomic, strong) NSString* cardType;
@end

@interface CustomerWithCustomerIdInvocation : RESTInvocation
@property (nonatomic, strong) NSString* customerId;
@end

@interface DisassociateDeviceStationInvocation : RESTInvocation
@property (nonatomic, strong) NSString* deviceStationId;
@property (nonatomic, strong) NSString* tillId;
@end

@interface FindItemInvocation : RESTInvocation
@property (nonatomic, strong) NSString* scanID;
@property (nonatomic, strong) NSString* storeID;
@end

@interface FindLineIdInvocation : RESTInvocation
@property (nonatomic, strong) NSString* itemScanId;
@property (nonatomic, strong) NSString* originalBasketId;
@property (nonatomic, strong) NSString* originalStoreId;
@property (nonatomic, strong) NSString* originalTillNumber;
@property (nonatomic, strong) NSString* originalTransactionDate;
@end

@interface FinishBasketInvocation : RESTInvocation
@property (nonatomic, strong) NSString* basketID;
@property (nonatomic, strong) NSString* deviceStationId;
@property (nonatomic, strong) NSString* emailAddress;
@property (nonatomic, assign) BOOL emailReceipt;
@property (nonatomic, assign) BOOL enablePrint;
@property (nonatomic, assign) BOOL printSummaryReceipt;
@property (nonatomic, strong) NSString* tillNumber;
@end

@interface FinishTaxFreeBasketInvocation : RESTInvocation
@property (nonatomic, strong) NSString* basketID;
@property (nonatomic, strong) NSString* deviceStationId;
@property (nonatomic, strong) NSString* emailAddress;
@property (nonatomic, assign) BOOL emailReceipt;
@property (nonatomic, assign) BOOL enablePrint;
@property (nonatomic, assign) BOOL printSummaryReceipt;
@property (nonatomic, strong) NSString* tillNumber;
@end

@interface GetActiveBasketInvocation : RESTInvocation
@property (nonatomic, strong) NSString* deviceID;
@end

@interface GetAssociateSalesHistoryInvocation : RESTInvocation
@property (nonatomic, strong) NSString* associateId;
@property (nonatomic, strong) NSDate* from;
@property (nonatomic, strong) NSString* locationId;
@property (nonatomic, strong) NSString* status;
@property (nonatomic, strong) NSDate* to;
@end

@interface GetBasketInvocation : RESTInvocation
@property (nonatomic, strong) NSString* basketID;
@end

@interface GetConfigurationInvocation : RESTInvocation
@property (nonatomic, assign) BOOL clearServerCache;
@property (nonatomic, strong) NSString* storeID;
@property (nonatomic, strong) NSString* tillNumber;
@end

@interface GetDeviceStationInvocation : RESTInvocation
@property (nonatomic, strong) NSString* deviceStationId;
@end

@interface GetDeviceStationsInvocation : RESTInvocation
@property (nonatomic, strong) NSString* deviceType;
@property (nonatomic, strong) NSString* storeId;
@end

@interface GetItemAvailabilityInvocation : RESTInvocation
@property (nonatomic, strong) NSString* scanID;
@property (nonatomic, strong) NSString* storeID;
@end

@interface GetStoreDetailsInvocation : RESTInvocation
@property (nonatomic, strong) NSString* storeId;
@end

@interface InternetReturnInvocation : RESTInvocation
@property (nonatomic, strong) NSString* basketId;
@property (nonatomic, strong) NSString* itemScanId;
@property (nonatomic, strong) NSString* itemSerialNumber;
@property (nonatomic, strong) NSString* originalItemDescription;
@property (nonatomic, strong) NSString* originalItemScanId;
@property (nonatomic, strong) NSString* originalReferenceId;
@property (nonatomic, strong) NSString* price;
@property (nonatomic, strong) NSString* reasonDescription;
@property (nonatomic, strong) NSString* reasonId;
@end

@interface LinkDeviceStationInvocation : RESTInvocation
@property (nonatomic, strong) NSString* deviceStationId;
@property (nonatomic, assign) BOOL force;
@end

@interface LinkedReturnInvocation : RESTInvocation
@property (nonatomic, strong) NSString* basketId;
@property (nonatomic, strong) NSString* itemScanId;
@property (nonatomic, strong) NSString* itemSerialNumber;
@property (nonatomic, strong) NSString* lineId;
@property (nonatomic, strong) NSString* originalItemDescription;
@property (nonatomic, strong) NSString* originalItemScanId;
@property (nonatomic, strong) NSString* originalTransactionDate;
@property (nonatomic, strong) NSString* originalTransactionId;
@property (nonatomic, strong) NSString* originalTransactionStoreId;
@property (nonatomic, strong) NSString* originalTransactionTill;
@property (nonatomic, strong) NSString* reasonDescription;
@property (nonatomic, strong) NSString* reasonId;
@end

@interface ListStoresInvocation : RESTInvocation
@property (nonatomic, strong) NSString* brandId;
@property (nonatomic, assign) BOOL isHomeStore;
@property (nonatomic, assign) double latitude;
@property (nonatomic, assign) double longitude;
@end

@interface LogMobileClientCrashEventInvocation : RESTInvocation
@property (nonatomic, strong) NSString* crashText;
@property (nonatomic, strong) NSDate* crashTime;
@property (nonatomic, strong) NSString* storeID;
@property (nonatomic, strong) NSString* version;
@property (nonatomic, strong) NSString* workStationId;
@end

@interface LoginAssociateInvocation : RESTInvocation
@property (nonatomic, strong) Credentials* credentials;
@property (nonatomic, assign) BOOL signOffExistingOperator;
@end

@interface LogoutAssociateInvocation : RESTInvocation
@property (nonatomic, strong) Credentials* credentials;
@end

@interface LookupSaleInvocation : RESTInvocation
@property (nonatomic, strong) NSString* barcode;
@property (nonatomic, strong) NSString* originalBasketId;
@property (nonatomic, strong) NSString* originalStoreId;
@property (nonatomic, strong) NSString* originalTillNumber;
@property (nonatomic, strong) NSString* originalTransactionDate;
@end

@interface NoReceiptInvocation : RESTInvocation
@property (nonatomic, strong) NSString* basketId;
@property (nonatomic, strong) NSString* itemScanId;
@property (nonatomic, strong) NSString* itemSerialNumber;
@property (nonatomic, strong) NSString* originalItemDescription;
@property (nonatomic, strong) NSString* originalItemScanId;
@property (nonatomic, strong) NSString* price;
@property (nonatomic, strong) NSString* reasonDescription;
@property (nonatomic, strong) NSString* reasonId;
@end

@interface NotesForCustomerInvocation : RESTInvocation
@property (nonatomic, strong) NSString* customerId;
@end

@interface OpenCashDrawerInvocation : RESTInvocation
@property (nonatomic, strong) NSString* deviceStationId;
@end

@interface RegisterCustomerInvocation : RESTInvocation
@property (nonatomic, strong) Customer* customer;
@end

@interface RemoveBasketDiscountInvocation : RESTInvocation
@property (nonatomic, strong) NSArray* basketDiscountID;
@property (nonatomic, strong) NSString* basketID;
@end

@interface RemoveBasketItemInvocation : RESTInvocation
@property (nonatomic, strong) NSString* basketID;
@property (nonatomic, strong) NSArray* basketItemID;
@property (nonatomic, strong) NSString* deviceStationId;
@end

@interface RemoveBasketItemDiscountInvocation : RESTInvocation
@property (nonatomic, strong) NSString* basketID;
@property (nonatomic, strong) NSArray* basketItemDiscountID;
@property (nonatomic, strong) NSString* basketItemID;
@end

@interface ReprintLastTxInvocation : RESTInvocation
@property (nonatomic, strong) NSString* deviceStationId;
@end

@interface ResumeBasketInvocation : RESTInvocation
@property (nonatomic, strong) NSString* basketBarcode;
@property (nonatomic, strong) NSString* basketID;
@property (nonatomic, strong) NSString* deviceID;
@property (nonatomic, strong) NSString* tillNumber;
@end

@interface ReturnBasketItemInvocation : RESTInvocation
@property (nonatomic, strong) NSString* basketID;
@property (nonatomic, strong) NSString* itemScanID;
@property (nonatomic, strong) NSString* itemSerialNumber;
@property (nonatomic, strong) NSString* originalItemDescription;
@property (nonatomic, strong) NSString* originalItemScanId;
@property (nonatomic, strong) NSString* originalTransactionBarcode;
@property (nonatomic, strong) NSDate* originalTransactionDate;
@property (nonatomic, strong) NSNumber* originalTransactionId;
@property (nonatomic, strong) NSNumber* originalTransactionStoreId;
@property (nonatomic, strong) NSNumber* originalTransactionTill;
@property (nonatomic, strong) NSString* price;
@property (nonatomic, strong) NSString* reasonId;
@end

@interface ReturnWithTypeInvocation : RESTInvocation
@property (nonatomic, strong) NSString* basketId;
@property (nonatomic, strong) NSString* itemScanId;
@property (nonatomic, strong) NSString* itemSerialNumber;
@property (nonatomic, strong) NSString* lineId;
@property (nonatomic, strong) NSString* originalCustomerCard;
@property (nonatomic, strong) NSString* originalItemDescription;
@property (nonatomic, strong) NSString* originalItemScanId;
@property (nonatomic, strong) NSString* originalReferenceId;
@property (nonatomic, strong) NSString* originalSalesPerson;
@property (nonatomic, strong) NSString* originalTransactionDate;
@property (nonatomic, strong) NSString* originalTransactionId;
@property (nonatomic, strong) NSString* originalTransactionStoreId;
@property (nonatomic, strong) NSString* originalTransactionTill;
@property (nonatomic, strong) NSString* price;
@property (nonatomic, strong) NSString* reasonDescription;
@property (nonatomic, strong) NSString* reasonId;
@property (nonatomic, strong) NSString* type;
@end

@interface SearchItemsInvocation : RESTInvocation
@property (nonatomic, strong) NSString* department;
@property (nonatomic, strong) NSString* itemDescription;
@property (nonatomic, assign) NSInteger pageNumber;
@property (nonatomic, strong) NSString* storeID;
@end

@interface SetNotesForCustomerInvocation : RESTInvocation
@property (nonatomic, strong) NSString* customerId;
@property (nonatomic, strong) NSArray* customerNotes;
@end

@interface SuspendBasketInvocation : RESTInvocation
@property (nonatomic, strong) NSString* basketDescription;
@property (nonatomic, strong) NSString* basketID;
@end

@interface ToggleTrainingModeInvocation : RESTInvocation
@end

@interface UnlinkDeviceInvocation : RESTInvocation
@property (nonatomic, strong) NSString* deviceStationId;
@end

@interface UnlinkedReturnInvocation : RESTInvocation
@property (nonatomic, strong) NSString* basketId;
@property (nonatomic, strong) NSString* itemScanId;
@property (nonatomic, strong) NSString* itemSerialNumber;
@property (nonatomic, strong) NSString* originalCustomerCard;
@property (nonatomic, strong) NSString* originalItemDescription;
@property (nonatomic, strong) NSString* originalItemScanId;
@property (nonatomic, strong) NSString* originalSalesPerson;
@property (nonatomic, strong) NSString* originalTransactionDate;
@property (nonatomic, strong) NSString* originalTransactionId;
@property (nonatomic, strong) NSString* originalTransactionStoreId;
@property (nonatomic, strong) NSString* originalTransactionTill;
@property (nonatomic, strong) NSString* price;
@property (nonatomic, strong) NSString* reasonDescription;
@property (nonatomic, strong) NSString* reasonId;
@end

@interface UpdateCustomerInvocation : RESTInvocation
@property (nonatomic, strong) Customer* customer;
@property (nonatomic, strong) NSString* customerId;
@end

@interface UpdateDeviceInvocation : RESTInvocation
@property (nonatomic, strong) DeviceProfile* device;
@end

@interface UpdateDeviceStationInvocation : RESTInvocation
@property (nonatomic, strong) DeviceStation* deviceStation;
@property (nonatomic, strong) NSString* deviceStationId;
@end

@interface UpdateGiftReceiptInvocation : RESTInvocation
@property (nonatomic, strong) BasketAssociateDTO* basket;
@end

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wobjc-property-synthesis"


@interface Affiliation : RESTObject
@property (nonatomic, strong) NSString* affiliationDescription; // Localized, use primitiveAffiliationDescription to access _affiliationDescription
@property (nonatomic, readonly) NSString* primitiveAffiliationDescription;
@property (nonatomic, strong) NSArray* affiliationDescriptionLanguages;
@property (nonatomic, strong) NSString* affiliationName; // Localized, use primitiveAffiliationName to access _affiliationName
@property (nonatomic, readonly) NSString* primitiveAffiliationName;
@property (nonatomic, strong) NSArray* affiliationNameLanguages;
@property (nonatomic, strong) NSString* affiliationType;
@property (nonatomic, assign) BOOL isStaff;
@end

@interface AirportCode : RESTObject
@property (nonatomic, strong) NSString* airportCode;
@property (nonatomic, strong) NSString* airportInZone;
@end

@interface AirportZone : RESTObject
@property (nonatomic, strong) NSString* airportZoneId;
@property (nonatomic, assign) BOOL zoneDutyFree;
@property (nonatomic, strong) NSArray* zoneLanguagues;
@property (nonatomic, assign) BOOL zoneTaxExempt;
@end

@interface AirsideOption : RESTObject
@property (nonatomic, strong) NSArray* airportCodes;
@property (nonatomic, strong) NSArray* airportZones;
@property (nonatomic, strong) NSString* localAirportCode;
@end

@interface Associate : RESTObject
@property (nonatomic, strong) NSString* associateId;
@property (nonatomic, strong) NSString* displayName;
@property (nonatomic, strong) NSString* email1;
@property (nonatomic, strong) NSString* email2;
@property (nonatomic, strong) NSString* firstName;
@property (nonatomic, strong) NSString* language;
@property (nonatomic, strong) NSString* lastName;
@property (nonatomic, strong) NSString* phone1;
@property (nonatomic, strong) NSString* phone2;
@property (nonatomic, strong) NSString* title;
@end

@interface BasketCustomer : RESTObject
@property (nonatomic, strong) NSString* amountDue;
@property (nonatomic, strong) Associate* associate;
@property (nonatomic, strong) NSString* barcode;
@property (nonatomic, strong) NSString* basketID;
@property (nonatomic, strong) NSArray* basketItems;
@property (nonatomic, strong) NSArray* basketRewards;
@property (nonatomic, strong) NSString* created;
@property (nonatomic, strong) Customer* customer;
@property (nonatomic, strong) NSString* dateCompleted;
@property (nonatomic, strong) NSString* netAmount;
@property (nonatomic, strong) NSString* receipt;
@property (nonatomic, strong) NSString* resolution;
@property (nonatomic, strong) NSString* rewardTotal;
@property (nonatomic, strong) NSString* status;
@property (nonatomic, strong) Store* store;
@property (nonatomic, strong) NSArray* tenders;
@end

@interface BasketDTO : RESTObject
@property (nonatomic, strong) BasketPricingDTO* amountDue;
@property (nonatomic, strong) NSString* barcode;
@property (nonatomic, strong) NSString* basketDescription;
@property (nonatomic, strong) NSString* basketID;
@property (nonatomic, strong) NSArray* basketItems;
@property (nonatomic, strong) NSArray* basketRewards;
@property (nonatomic, strong) NSString* changeDue;
@property (nonatomic, strong) NSString* completed;
@property (nonatomic, strong) NSDate* created;
@property (nonatomic, strong) NSString* currencyCode;
@property (nonatomic, strong) NSArray* customerCards;
@property (nonatomic, strong) NSString* email;
@property (nonatomic, strong) BasketForeignSubTotalDTO* foreignSubtotal;
@property (nonatomic, strong) NSString* netAmount;
@property (nonatomic, strong) NSString* receipt;
@property (nonatomic, strong) NSString* resolution;
@property (nonatomic, strong) BasketPricingDTO* rewardTotal;
@property (nonatomic, strong) NSString* status;
@property (nonatomic, strong) Store* store;
@property (nonatomic, strong) BasketTaxFree* taxFree;
@property (nonatomic, strong) NSString* tillNumber;
@property (nonatomic, strong) NSString* total;
@property (nonatomic, assign) BOOL trainingMode;
@property (nonatomic, strong) NSString* transactionDate;
@property (nonatomic, strong) NSString* transactionNumber;
@end

@interface BasketAssociateDTO : BasketDTO
@property (nonatomic, strong) Customer* customer;
@property (nonatomic, strong) Associate* operator;
@end

@interface BasketTaxFreeDocumentDTO : BasketDTO
@property (nonatomic, strong) NSArray* basketItems;
@property (nonatomic, strong) NSString* issuerCountryId;
@property (nonatomic, strong) Associate* operator;
@property (nonatomic, strong) ShopperDetailsDTO* shopper;
@property (nonatomic, strong) NSString* shopperConfirmed;
@property (nonatomic, strong) ShopperDetailsDTO* shopperDetails;
@property (nonatomic, strong) NSString* shopperValidatedBy;
@property (nonatomic, strong) NSArray* taxFreeForm;
@property (nonatomic, strong) NSString* transactionNumber;
@property (nonatomic, strong) NSString* travellerId;
@end

@interface BasketFlightDetails : RESTObject
@property (nonatomic, strong) NSString* airlineCode;
@property (nonatomic, strong) NSString* boardingPassBarcode;
@property (nonatomic, strong) NSString* destinationAirportCode;
@property (nonatomic, strong) NSString* flightNumber;
@property (nonatomic, assign) BOOL manualEntry;
@property (nonatomic, strong) NSString* nationality;
@property (nonatomic, assign) BOOL notFlying;
@property (nonatomic, strong) NSString* originAirportCode;
@end

@interface BasketForeignSubTotalDTO : RESTObject
@property (nonatomic, strong) BasketAssociateDTO* basket;
@property (nonatomic, strong) NSString* currencyCode;
@property (nonatomic, strong) NSString* desc;
@property (nonatomic, strong) NSNumber* exchangeRate;
@property (nonatomic, strong) BasketPricingDTO* foreignAmount;
@end

@interface BasketItem : RESTObject
@property (nonatomic, strong) NSString* ageRestriction;
@property (nonatomic, strong) NSString* barcodeType;
@property (nonatomic, weak) BasketDTO* basket;
@property (nonatomic, assign) BOOL basketItemChangeResult;
@property (nonatomic, strong) ServerError* basketItemCreatedError;
@property (nonatomic, strong) NSString* basketItemId;
@property (nonatomic, strong) NSArray* basketRewards;
@property (nonatomic, strong) NSString* desc;
@property (nonatomic, assign) BOOL editable;
@property (nonatomic, strong) NSString* entryMethod;
@property (nonatomic, strong) NSArray* giftReceiptRequest;
@property (nonatomic, assign) BOOL hasBasketItemChanged;
@property (nonatomic, assign) BOOL hasBasketItemCreated;
@property (nonatomic, strong) NSString* imageUrl;
@property (nonatomic, strong) NSString* itemID;
@property (nonatomic, strong) NSString* itemScanId;
@property (nonatomic, strong) NSString* itemSerialNumber;
@property (nonatomic, strong) NSArray* mandatoryModifierGroups;
@property (nonatomic, strong) NSString* measure;
@property (nonatomic, strong) NSString* message;
@property (nonatomic, strong) NSString* netAmount;
@property (nonatomic, strong) NSArray* optionalModifierGroups;
@property (nonatomic, strong) NSString* orginalQuantity;
@property (nonatomic, strong) NSString* originalItemDescription;
@property (nonatomic, strong) NSString* originalItemScanId;
@property (nonatomic, strong) BasketPricingDTO* pricing;
@property (nonatomic, strong) NSString* quantity;
@property (nonatomic, strong) BasketPricingDTO* retailPrice;
@property (nonatomic, strong) NSString* rewardTotal;
@property (nonatomic, strong) NSString* saleAssociateID;
@property (nonatomic, strong) SalesPerson* salesPerson;
@property (nonatomic, strong) NSArray* selectedModifiers;
@property (nonatomic, strong) NSString* serialNumber;
@property (nonatomic, strong) BasketPricingDTO* totalPrice;
@property (nonatomic, strong) NSString* track1;
@property (nonatomic, strong) NSString* track2;
@property (nonatomic, strong) NSString* track3;
@end

@interface BasketAssociateInfo : BasketItem
@property (nonatomic, strong) NSString* employeeId;
@property (nonatomic, strong) NSString* operatorDisplayName;
@property (nonatomic, strong) NSString* operatorId;
@end

@interface BasketCustomerAffiliation : BasketItem
@property (nonatomic, strong) NSString* affiliationMembership;
@property (nonatomic, strong) NSString* affiliationName;
@property (nonatomic, strong) NSString* affiliationNumber;
@property (nonatomic, strong) NSString* affiliationTypeId;
@end

@interface BasketCustomerCard : BasketItem
@property (nonatomic, strong) NSString* affiliationType;
@property (nonatomic, strong) NSString* cardName;
@property (nonatomic, strong) NSString* cardNumber;
@property (nonatomic, strong) Customer* customer;
@property (nonatomic, strong) NSString* customerCardNumber;
@property (nonatomic, strong) NSString* customerDisplayName;
@end

@interface BasketRewardSale : BasketItem
@property (nonatomic, strong) id<NSCoding> code;
@property (nonatomic, strong) id<NSCoding> reason;
@property (nonatomic, strong) BasketPricingDTO* rewardAmount;
@property (nonatomic, strong) NSString* rewardId;
@property (nonatomic, strong) NSString* rewardText;
@property (nonatomic, strong) NSString* rewardType;
@end

@interface BasketRewardSaleDiscountAmount : BasketRewardSale
@end

@interface BasketRewardSaleDiscountPercent : BasketRewardSale
@end

@interface BasketRewardSalePromotionAmount : BasketRewardSale
@end

@interface BasketTender : BasketItem
@property (nonatomic, strong) NSString* amount;
@property (nonatomic, strong) BasketTenderAuthorization* authorization;
@property (nonatomic, strong) NSString* currencyCode;
@property (nonatomic, strong) NSString* desc;
@property (nonatomic, strong) NSString* displayAmount;
@property (nonatomic, strong) NSString* displayText;
@property (nonatomic, strong) BasketTenderForeignCurrency* foreignCurrency;
@property (nonatomic, strong) NSNumber *hasCreatedTender;
@property (nonatomic, assign) BOOL hasCreatedTenderValue;
@property (nonatomic, strong) Tender* tender;
@property (nonatomic, strong) BasketPricingDTO* tenderAmount;
@property (nonatomic, strong) NSString* tenderType;
@end

@interface EFTPayment : BasketTender
@property (nonatomic, strong) NSString* authorizationCode;
@property (nonatomic, strong) NSString* authorizingTermId;
@property (nonatomic, strong) NSString* basketId;
@property (nonatomic, strong) NSString* basketTenderId;
@property (nonatomic, strong) NSString* cardBin;
@property (nonatomic, strong) NSString* cardType;
@property (nonatomic, strong) NSString* cardTypeCode;
@property (nonatomic, strong) NSString* customerAlias;
@property (nonatomic, strong) NSNumber* dccCommissionFee;
@property (nonatomic, strong) NSNumber* dccConvertedAmount;
@property (nonatomic, strong) NSString* dccConvertedCurrency;
@property (nonatomic, strong) NSNumber* dccExchangeRate;
@property (nonatomic, strong) NSNumber* dccMarkup;
@property (nonatomic, strong) NSNumber* dccOriginalAmount;
@property (nonatomic, strong) NSString* dccOriginalCurrency;
@property (nonatomic, strong) NSString* dccSource;
@property (nonatomic, strong) NSString* eftProvider;
@property (nonatomic, strong) NSString* encryptedAccountNumber;
@property (nonatomic, strong) NSString* issuerCountryId;
@property (nonatomic, strong) NSString* providerCentralTenderId;
@property (nonatomic, strong) NSString* providerTenderId;
@property (nonatomic, strong) NSString* rawData;
@property (nonatomic, strong) NSString* receiptCustomer;
@property (nonatomic, strong) NSString* receiptMerchant;
@property (nonatomic, strong) NSString* referenceNumber;
@property (nonatomic, strong) NSNumber* resultCode;
@property (nonatomic, strong) NSString* signature;
@end

@interface StandardPayment : BasketTender
@property (nonatomic, strong) NSString* authCode;
@property (nonatomic, strong) NSString* basketID;
@property (nonatomic, strong) NSString* cardNumber;
@property (nonatomic, strong) NSString* currencyCode;
@property (nonatomic, strong) NSString* reference;
@property (nonatomic, strong) NSString* tenderType;
@end

@interface VerifonePayment : BasketTender
@property (nonatomic, strong) NSString* authCode;
@property (nonatomic, strong) NSString* basketTenderId;
@property (nonatomic, strong) NSString* cardScheme;
@property (nonatomic, strong) NSNumber *hasData;
@property (nonatomic, assign) BOOL hasDataValue;
@property (nonatomic, strong) NSNumber *hasReply;
@property (nonatomic, assign) BOOL hasReplyValue;
@property (nonatomic, strong) NSString* identifier;
@property (nonatomic, strong) NSString* isError;
@property (nonatomic, strong) NSString* maskedExpiry;
@property (nonatomic, strong) NSString* maskedPAN;
@property (nonatomic, strong) NSString* paymentId;
@property (nonatomic, strong) NSString* provider;
@property (nonatomic, strong) NSString* receiptData;
@property (nonatomic, strong) NSString* receiptSignature;
@property (nonatomic, strong) NSString* responseMsg;
@property (nonatomic, strong) NSString* salesBasketId;
@property (nonatomic, strong) NSString* tenderType;
@property (nonatomic, strong) NSString* tipAmount;
@property (nonatomic, strong) NSString* totalCharge;
@property (nonatomic, strong) NSString* track1Data;
@property (nonatomic, strong) NSString* track2Data;
@property (nonatomic, strong) NSString* track3Data;
@property (nonatomic, strong) NSString* transaction;
@property (nonatomic, strong) NSString* transId;
@end

@interface YespayPayment : BasketTender
@property (nonatomic, strong) NSString* accountType;
@property (nonatomic, strong) NSString* aid;
@property (nonatomic, strong) NSString* authCode;
@property (nonatomic, strong) NSString* basketId;
@property (nonatomic, strong) NSString* basketTenderId;
@property (nonatomic, strong) NSString* batchNumber;
@property (nonatomic, strong) NSString* cardExpiryDate;
@property (nonatomic, strong) NSString* cashAmount;
@property (nonatomic, strong) NSString* cic;
@property (nonatomic, strong) NSString* convertedDCCAmount;
@property (nonatomic, strong) NSString* currencyConversionRate;
@property (nonatomic, strong) NSString* currencyName;
@property (nonatomic, strong) NSString* customerDeclaration;
@property (nonatomic, strong) NSString* cvm;
@property (nonatomic, strong) NSString* cvvResponse;
@property (nonatomic, strong) NSString* eftSequenceNumber;
@property (nonatomic, strong) NSString* emvCardHolderName;
@property (nonatomic, strong) NSString* emvCardHolderNameExtended;
@property (nonatomic, strong) NSString* emvEffectiveDate;
@property (nonatomic, strong) NSString* emvLabel;
@property (nonatomic, strong) NSString* gcBalance;
@property (nonatomic, strong) NSString* gratuityAmount;
@property (nonatomic, strong) NSNumber *hasReply;
@property (nonatomic, assign) BOOL hasReplyValue;
@property (nonatomic, strong) NSString* loyaltyCode;
@property (nonatomic, strong) NSString* maskedPAN;
@property (nonatomic, strong) NSString* merchantAddress;
@property (nonatomic, strong) NSString* merchantId;
@property (nonatomic, strong) NSString* merchantName;
@property (nonatomic, strong) NSString* mswr;
@property (nonatomic, strong) NSString* panOrIssue;
@property (nonatomic, strong) NSString* panSHA1;
@property (nonatomic, strong) NSString* pdAmount;
@property (nonatomic, strong) NSString* pem;
@property (nonatomic, strong) NSString* pgtr;
@property (nonatomic, strong) NSString* receiptNumber;
@property (nonatomic, strong) NSString* refPhone1;
@property (nonatomic, strong) NSString* refPhone2;
@property (nonatomic, strong) NSString* refundCounts;
@property (nonatomic, strong) NSString* retentionReminder;
@property (nonatomic, strong) NSString* rrn;
@property (nonatomic, strong) NSString* salesCount;
@property (nonatomic, strong) NSString* startDate;
@property (nonatomic, strong) NSString* terminalId;
@property (nonatomic, strong) NSString* totalAmount;
@property (nonatomic, strong) NSString* totalRefundAmount;
@property (nonatomic, strong) NSString* totalSaleAmount;
@property (nonatomic, strong) NSString* transDate;
@property (nonatomic, strong) NSString* transReference;
@property (nonatomic, strong) NSString* transResult;
@property (nonatomic, strong) NSString* transStatusInfo;
@property (nonatomic, strong) NSString* transTime;
@property (nonatomic, strong) NSString* transType;
@property (nonatomic, strong) NSString* transVerificationResult;
@end

@interface CustomerEmail : BasketItem
@property (nonatomic, strong) NSString* address;
@end

@interface PrintDataLine : BasketItem
@property (nonatomic, strong) NSString* printData;
@property (nonatomic, assign) BOOL printed;
@property (nonatomic, strong) NSString* printResult;
@end

@interface ReturnItem : BasketItem
@property (nonatomic, strong) NSString* originalTransactionBarcode;
@property (nonatomic, strong) NSDate* originalTransactionDate;
@property (nonatomic, strong) NSNumber *originalTransactionId;
@property (nonatomic, assign) NSInteger originalTransactionIdValue;
@property (nonatomic, strong) NSNumber *originalTransactionStoreId;
@property (nonatomic, assign) NSInteger originalTransactionStoreIdValue;
@property (nonatomic, strong) NSNumber *originalTransactionTill;
@property (nonatomic, assign) NSInteger originalTransactionTillValue;
@property (nonatomic, strong) NSString* reasonId;
@end

@interface SaleItem : BasketItem
@property (nonatomic, strong) NSNumber *generateGiftReceipt;
@property (nonatomic, assign) BOOL generateGiftReceiptValue;
@property (nonatomic, strong) NSString* measure;
@property (nonatomic, strong) NSNumber *originalGenerateGiftReceipt;
@property (nonatomic, assign) BOOL originalGenerateGiftReceiptValue;
@end

@interface ReturnableSaleItem : SaleItem
@property (nonatomic, strong) NSString* returnedQuantity;
@property (nonatomic, strong) NSArray* returnInstances;
@end

@interface BasketPricingDTO : RESTObject
@property (nonatomic, strong) NSString* amount;
@property (nonatomic, strong) BasketTender* basketTender;
@property (nonatomic, strong) NSString* display;
@end

@interface BasketRewardLine : RESTObject
@property (nonatomic, strong) BasketAssociateDTO* basket;
@property (nonatomic, strong) BasketItem* basketItem;
@property (nonatomic, strong) id<NSCoding> code;
@property (nonatomic, strong) id<NSCoding> reason;
@property (nonatomic, strong) BasketPricingDTO* rewardAmount;
@property (nonatomic, strong) NSString* rewardId;
@property (nonatomic, strong) NSString* rewardText;
@property (nonatomic, strong) NSString* rewardType;
@end

@interface BasketRewardLineDiscountAmount : BasketRewardLine
@end

@interface BasketRewardLineDiscountPercent : BasketRewardLine
@end

@interface BasketRewardLinePriceAdjust : BasketRewardLine
@end

@interface BasketTaxFree : RESTObject
@property (nonatomic, weak) BasketDTO* basket;
@property (nonatomic, strong) NSString* documentId;
@property (nonatomic, strong) NSString* documentType;
@end

@interface BasketTenderAuthorization : RESTObject
@property (nonatomic, strong) NSString* authorizationCode;
@property (nonatomic, assign) BOOL providerVoided;
@property (nonatomic, strong) NSString* referenceNumber;
@end

@interface BasketTenderForeignCurrency : RESTObject
@property (nonatomic, strong) NSString* amount;
@property (nonatomic, strong) BasketTender* basketTender;
@property (nonatomic, strong) NSString* currencyCode;
@property (nonatomic, strong) NSString* displayAmount;
@property (nonatomic, strong) NSString* rate;
@end

@interface ClientSettings : RESTObject
@property (nonatomic, strong) NSString* adminPinCode;
@property (nonatomic, strong) NSString* authURL;
@property (nonatomic, assign) BOOL autoPrint;
@property (nonatomic, strong) NSString* basketLayout;
@property (nonatomic, assign) BOOL basketSuspendDescriptionPrompt;
@property (nonatomic, assign) NSInteger DRAFT_discountUnitPercentageDecimalDigits;
@property (nonatomic, strong) NSString* DRAFT_finishPredicate;
@property (nonatomic, strong) NSString* DRAFT_finishPredicateMessage;
@property (nonatomic, strong) NSString* DRAFT_fontFamily;
@property (nonatomic, strong) NSString* DRAFT_keyboardAppearance;
@property (nonatomic, assign) NSInteger logoutAfterInactivityPeriod;
@property (nonatomic, assign) BOOL logoutAfterSaleComplete;
@property (nonatomic, strong) NSString* maxItemQuantity;
@property (nonatomic, strong) NSString* nativeCurrencyCode;
@property (nonatomic, assign) BOOL DRAFT_numberPadIsReversed;
@property (nonatomic, strong) NSString* DRAFT_preprintReceiptImage;
@property (nonatomic, strong) NSNumber* presetTip1;
@property (nonatomic, strong) NSNumber* presetTip2;
@property (nonatomic, strong) NSString* DRAFT_primaryColor;
@property (nonatomic, assign) double printJobDelay;
@property (nonatomic, assign) BOOL promptForDumpCodeDescription;
@property (nonatomic, assign) BOOL promptQtyForDumpCode;
@property (nonatomic, strong) NSString* receiptManager;
@property (nonatomic, strong) NSString* receiptMethods;
@property (nonatomic, strong) NSString* releaseAuthSessionUrl;
@property (nonatomic, strong) NSString* salesAssociateIDMask;
@property (nonatomic, strong) NSString* scanMode;
@property (nonatomic, strong) NSString* DRAFT_secondaryColor;
@property (nonatomic, strong) NSString* signOnMethod;
@property (nonatomic, strong) NSString* DRAFT_verifoneAppName;
@property (nonatomic, assign) BOOL DRAFT_verifoneBypassConfirmation;
@property (nonatomic, strong) NSString* DRAFT_verifonePIN;
@end

@interface Configuration : RESTObject
@property (nonatomic, strong) NSArray* affiliations;
@property (nonatomic, strong) AirsideOption* airsideOption;
@property (nonatomic, strong) ClientSettings* clientSettings;
@property (nonatomic, strong) NSArray* countries;
@property (nonatomic, strong) NSArray* customerIdentificationTypes;
@property (nonatomic, strong) NSArray* devices;
@property (nonatomic, strong) NSArray* discountCodes;
@property (nonatomic, strong) NSArray* dumpCodes;
@property (nonatomic, strong) NSArray* inputFields;
@property (nonatomic, strong) NSArray* itemModifierGroups;
@property (nonatomic, strong) NSArray* keyValueGroups;
@property (nonatomic, strong) NSArray* localizableTables;
@property (nonatomic, strong) NSArray* passwordRules;
@property (nonatomic, strong) NSArray* permissionGroups;
@property (nonatomic, strong) NSArray* posErrors;
@property (nonatomic, strong) NSArray* reasonGroups;
@property (nonatomic, strong) NSArray* softKeyContexts;
@property (nonatomic, strong) NSArray* softKeys;
@property (nonatomic, strong) NSArray* tenders;
@end

@interface Country : RESTObject
@property (nonatomic, strong) NSString* countryId;
@property (nonatomic, strong) NSArray* countryLanguages;
@end

@interface Credentials : RESTObject
@property (nonatomic, strong) NSString* code;
@property (nonatomic, strong) NSString* deviceId;
@property (nonatomic, strong) NSString* displayName;
@property (nonatomic, strong) NSString* password;
@property (nonatomic, strong) NSString* permissionsGroup;
@property (nonatomic, strong) NSString* preferredLanguage;
@property (nonatomic, strong) NSString* storeId;
@property (nonatomic, strong) NSString* tillNumber;
@property (nonatomic, strong) NSString* token;
@property (nonatomic, strong) NSString* track1;
@property (nonatomic, strong) NSString* track2;
@property (nonatomic, strong) NSString* track3;
@property (nonatomic, assign) BOOL trainingMode;
@property (nonatomic, strong) NSString* updatedPassword;
@property (nonatomic, strong) NSString* username;
@end

@interface Customer : RESTObject
@property (nonatomic, strong) CustomerAddress* address;
@property (nonatomic, strong) NSArray* baskets;
@property (nonatomic, strong) NSString* cardNumber;
@property (nonatomic, strong) ServerError* cardsRefreshError;
@property (nonatomic, strong) NSNumber *cardsRefreshRequired;
@property (nonatomic, assign) BOOL cardsRefreshRequiredValue;
@property (nonatomic, strong) NSArray* customerCards;
@property (nonatomic, strong) ServerError* customerChangeError;
@property (nonatomic, strong) NSString* customerId;
@property (nonatomic, strong) NSNumber *editable;
@property (nonatomic, assign) BOOL editableValue;
@property (nonatomic, strong) NSString* email;
@property (nonatomic, strong) NSString* firstName;
@property (nonatomic, strong) NSString* gender;
@property (nonatomic, strong) NSNumber *hasCustomerChanged;
@property (nonatomic, assign) BOOL hasCustomerChangedValue;
@property (nonatomic, strong) NSNumber *hasWishlistChanged;
@property (nonatomic, assign) BOOL hasWishlistChangedValue;
@property (nonatomic, strong) NSString* lastName;
@property (nonatomic, strong) NSString* notes;
@property (nonatomic, strong) ServerError* notesRefreshError;
@property (nonatomic, strong) NSNumber *notesRefreshRequired;
@property (nonatomic, assign) BOOL notesRefreshRequiredValue;
@property (nonatomic, strong) NSString* phone1;
@property (nonatomic, strong) NSString* salutation;
@property (nonatomic, strong) NSArray* search;
@property (nonatomic, strong) NSArray* wallets;
@property (nonatomic, strong) NSNumber *walletUpdateRequired;
@property (nonatomic, assign) BOOL walletUpdateRequiredValue;
@property (nonatomic, strong) NSArray* wishList;
@property (nonatomic, strong) NSNumber *wishlistRefreshRequired;
@property (nonatomic, assign) BOOL wishlistRefreshRequiredValue;
@end

@interface CustomerAddress : RESTObject
@property (nonatomic, strong) NSString* city;
@property (nonatomic, strong) NSString* country;
@property (nonatomic, strong) Customer* customer;
@property (nonatomic, strong) NSString* line1;
@property (nonatomic, strong) NSString* line2;
@property (nonatomic, strong) NSString* postcode;
@property (nonatomic, strong) NSString* territory;
@end

@interface CustomerCard : RESTObject
@property (nonatomic, strong) NSString* cardName;
@property (nonatomic, strong) NSString* cardNumber;
@property (nonatomic, strong) Customer* customer;
@end

@interface CustomerIdentificationType : RESTObject
@property (nonatomic, strong) NSString* customerIdentificationTypeDescription;
@property (nonatomic, strong) NSArray* descriptionLanguages;
@property (nonatomic, strong) NSString* type;
@end

@interface Device : RESTObject
@property (nonatomic, strong) NSString* connection;
@property (nonatomic, strong) NSString* deviceName;
@property (nonatomic, assign) BOOL enabled;
@property (nonatomic, strong) NSString* manager;
@property (nonatomic, strong) NSArray* properties;
@property (nonatomic, assign) BOOL scanToIdentify;
@property (nonatomic, strong) NSString* selection;
@property (nonatomic, strong) NSString* timeout;
@property (nonatomic, assign) BOOL userSelectable;
@end

@interface DeviceProfile : RESTObject
@property (nonatomic, strong) NSString* deviceId;
@property (nonatomic, strong) NSString* storeId;
@property (nonatomic, strong) NSString* tillNumber;
@end

@interface DeviceStation : RESTObject
@property (nonatomic, assign) BOOL available;
@property (nonatomic, assign) BOOL claimed;
@property (nonatomic, strong) NSString* claimedUserId;
@property (nonatomic, strong) NSString* claimedUserName;
@property (nonatomic, strong) NSString* deviceStationId;
@property (nonatomic, strong) NSArray* peripherals;
@property (nonatomic, strong) NSString* terminal;
@property (nonatomic, strong) NSString* tillId;
@end

@interface DeviceStationPeripheral : RESTObject
@property (nonatomic, strong) NSString* codePage;
@property (nonatomic, strong) NSString* networkAddress;
@property (nonatomic, strong) NSString* peripheralDescription;
@property (nonatomic, strong) NSString* peripheralId;
@property (nonatomic, strong) NSString* peripheralStatus;
@property (nonatomic, strong) NSString* peripheralType;
@end

@interface DiscountCode : RESTObject
@property (nonatomic, strong) NSString* affiliationName;
@property (nonatomic, strong) NSString* affiliationType;
@property (nonatomic, strong) NSString* discountCodeID;
@property (nonatomic, strong) NSString* discountDescription; // Localized, use primitiveDiscountDescription to access _discountDescription
@property (nonatomic, readonly) NSString* primitiveDiscountDescription;
@property (nonatomic, strong) NSArray* discountDescriptionLanguages;
@property (nonatomic, strong) NSString* discountType;
@property (nonatomic, strong) NSString* discountValueType;
@property (nonatomic, assign) BOOL isDiscountValueRequired;
@property (nonatomic, assign) BOOL isReasonRequired;
@property (nonatomic, strong) NSNumber* numberOfDecimalPlaces;
@end

@interface DiscountReason : RESTObject
@property (nonatomic, strong) NSString* reasonId;
@property (nonatomic, strong) NSString* text; // Localized, use primitiveText to access _text
@property (nonatomic, readonly) NSString* primitiveText;
@property (nonatomic, strong) NSArray* textLanguages;
@end

@interface DiscountReasonGroup : RESTObject
@property (nonatomic, strong) NSString* name;
@property (nonatomic, strong) NSArray* reasons;
@end

@interface DumpCode : RESTObject
@property (nonatomic, strong) NSString* desc; // Localized, use primitiveDesc to access _desc
@property (nonatomic, readonly) NSString* primitiveDesc;
@property (nonatomic, strong) NSArray* descLanguages;
@property (nonatomic, strong) NSString* dumpCodeId;
@property (nonatomic, strong) NSString* itemSellingCode;
@end

@interface FormConfigurationDTO : RESTObject
@property (nonatomic, strong) NSNumber* columns;
@property (nonatomic, strong) NSString* contextType;
@property (nonatomic, strong) SoftKey* onCloseFunction;
@property (nonatomic, strong) SoftKey* onSubmitFunction;
@property (nonatomic, strong) NSString* presentation;
@property (nonatomic, strong) NSString* rootMenu;
@property (nonatomic, strong) NSArray* softKeys;
@property (nonatomic, strong) NSString* title; // Localized, use primitiveTitle to access _title
@property (nonatomic, readonly) NSString* primitiveTitle;
@property (nonatomic, strong) NSArray* titleLanguages;
@end

@interface SoftKeyContext : FormConfigurationDTO
@end

@interface Item : RESTObject
@property (nonatomic, strong) NSArray* additionalImages;
@property (nonatomic, strong) NSArray* availability;
@property (nonatomic, strong) ServerError* availabilityError;
@property (nonatomic, strong) NSString* displayPrice;
@property (nonatomic, strong) NSString* image;
@property (nonatomic, strong) NSArray* images;
@property (nonatomic, strong) NSString* itemRefId;
@property (nonatomic, strong) NSString* longDesc;
@property (nonatomic, strong) NSNumber *needsAvailability;
@property (nonatomic, assign) BOOL needsAvailabilityValue;
@property (nonatomic, strong) NSString* price;
@property (nonatomic, strong) ProductSearch* productSearch;
@property (nonatomic, strong) NSArray* properties;
@property (nonatomic, strong) NSString* quantity;
@property (nonatomic, strong) NSString* shortDesc;
@end

@interface ItemAvailability : RESTObject
@property (nonatomic, strong) NSString* distance;
@property (nonatomic, strong) Item* item;
@property (nonatomic, strong) NSString* quantity;
@property (nonatomic, strong) NSString* storeId;
@property (nonatomic, strong) NSString* storeName;
@end

@interface ItemAvailabilityRecord : RESTObject
@property (nonatomic, strong) NSString* distance;
@property (nonatomic, strong) NSString* quantity;
@property (nonatomic, strong) NSString* storeId;
@property (nonatomic, strong) NSString* storeName;
@end

@interface ItemImages : RESTObject
@property (nonatomic, strong) NSString* image;
@property (nonatomic, strong) Item* item;
@end

@interface ItemModifier : RESTObject
@property (nonatomic, strong) NSString* itemModifierGroupId;
@property (nonatomic, strong) NSString* itemModifierGroupName;
@property (nonatomic, strong) NSString* itemModifierId;
@property (nonatomic, strong) NSString* name;
@property (nonatomic, strong) BasketPricingDTO* retailPrice;
@property (nonatomic, strong) NSString* sku;
@property (nonatomic, strong) NSString* type;
@end

@interface ItemModifierGroup : RESTObject
@property (nonatomic, strong) NSString* itemModifierGroupId;
@property (nonatomic, strong) NSArray* itemModifiers;
@property (nonatomic, strong) BasketItem* mandatoryBasketItem;
@property (nonatomic, strong) NSString* name;
@property (nonatomic, strong) BasketItem* optionalBasketItem;
@property (nonatomic, strong) NSString* type;
@end

@interface MandatoryModifierGroup : ItemModifierGroup
@end

@interface OptionalModifierGroup : ItemModifierGroup
@end

@interface ItemProperties : RESTObject
@property (nonatomic, strong) Item* item;
@property (nonatomic, strong) NSString* key;
@property (nonatomic, strong) NSString* value;
@end

@interface KeyValueDTO : RESTObject
@property (nonatomic, strong) NSString* key;
@property (nonatomic, strong) NSString* value;
@end

@interface KeyValueGroup : RESTObject
@property (nonatomic, strong) NSString* groupName;
@property (nonatomic, strong) KeyValues* keyValues;
@end

@interface KeyValues : RESTObject
@property (nonatomic, strong) NSArray* array;
+(instancetype)keyValuesWithArray:(NSArray*)array;
@end

@interface Language : RESTObject
@property (nonatomic, strong) NSString* code;
@property (nonatomic, strong) NSString* value;
@end

@interface Localizable : RESTObject
@property (nonatomic, strong) NSString* comment;
@property (nonatomic, strong) NSString* identifier;
@property (nonatomic, strong) NSString* key;
@property (nonatomic, strong) NSString* value; // Localized, use primitiveValue to access _value
@property (nonatomic, readonly) NSString* primitiveValue;
@property (nonatomic, strong) NSArray* valueLanguages;
@end

@interface LocalizableTable : RESTObject
@property (nonatomic, strong) NSArray* localizables;
@property (nonatomic, strong) NSString* table;
@end

@interface POSError : RESTObject
@property (nonatomic, strong) NSString* errorLevel;
@property (nonatomic, strong) NSString* posErrorNumber;
@end

@interface PasswordRule : RESTObject
@property (nonatomic, strong) NSString* ruleDescription; // Localized, use primitiveRuleDescription to access _ruleDescription
@property (nonatomic, readonly) NSString* primitiveRuleDescription;
@property (nonatomic, strong) NSArray* ruleDescriptionLanguages;
@property (nonatomic, strong) NSString* ruleExpression;
@property (nonatomic, strong) NSString* ruleId;
@end

@interface Payment : RESTObject
@property (nonatomic, strong) NSNumber *amount;
@property (nonatomic, assign) double amountValue;
@property (nonatomic, strong) NSString* authCode;
@property (nonatomic, strong) BasketAssociateDTO* basket;
@property (nonatomic, strong) NSString* cardNumber;
@property (nonatomic, strong) NSString* reference;
@property (nonatomic, strong) Tender* tender;
@end

@interface Permission : RESTObject
@property (nonatomic, strong) NSString* permission;
+(instancetype)permissionWithString:(NSString*)string;
@end

@interface PermissionGroup : RESTObject
@property (nonatomic, strong) NSString* groupName;
@property (nonatomic, strong) NSArray* permissions;
@end

@interface PrintElement : RESTObject
@property (nonatomic, strong) NSString* align;
@property (nonatomic, strong) NSString* barcodetype;
@property (nonatomic, strong) NSString* elementtype;
@property (nonatomic, strong) NSString* printImageURL;
@property (nonatomic, strong) NSString* size;
@property (nonatomic, strong) NSString* style;
@property (nonatomic, strong) NSString* value;
@end

@interface Property : RESTObject
@property (nonatomic, strong) NSString* name;
@property (nonatomic, strong) NSString* value;
@end

@interface ReturnInstance : RESTObject
@property (nonatomic, strong) NSDate* date;
@property (nonatomic, assign) NSInteger quantity;
@property (nonatomic, strong) NSString* storeID;
@end

@interface SalesPerson : RESTObject
@property (nonatomic, strong) BasketItem* basketItem;
@property (nonatomic, strong) NSString* displayName;
@property (nonatomic, strong) NSString* operatorId;
@end

@interface SelectedModifier : RESTObject
@property (nonatomic, strong) BasketItem* basketItem;
@property (nonatomic, strong) NSString* itemModifierId;
@property (nonatomic, strong) NSString* name;
@property (nonatomic, strong) BasketPricingDTO* retailPrice;
@property (nonatomic, strong) NSString* sku;
@end

@interface ServerError : RESTObject
@property (nonatomic, strong) BasketItem* changedBasketItem;
@property (nonatomic, strong) NSString* context;
@property (nonatomic, strong) BasketItem* createdBasketItem;
@property (nonatomic, strong) NSArray* messages;
@end

@interface ServerErrorMessage : RESTObject
@property (nonatomic, strong) NSString* basketItemId;
@property (nonatomic, strong) NSNumber *code;
@property (nonatomic, assign) NSInteger codeValue;
@property (nonatomic, strong) NSString* name;
@property (nonatomic, strong) NSNumber *order;
@property (nonatomic, assign) NSInteger orderValue;
@property (nonatomic, strong) NSString* value;
@end

@interface OverrideErrorMessage : ServerErrorMessage
@property (nonatomic, strong) NSString* errorCode;
@property (nonatomic, assign) NSInteger errorLevel;
@property (nonatomic, strong) NSString* errorMessage;
@end

@interface POSErrorMessage : ServerErrorMessage
@property (nonatomic, assign) BOOL invalidatesBasket;
@end

@interface ServerErrorAffiliationRequiredMessage : ServerErrorMessage
@property (nonatomic, strong) NSString* affiliationName;
@property (nonatomic, strong) NSString* affiliationType;
@end

@interface ServerErrorAgeRestrictionMessage : ServerErrorMessage
@property (nonatomic, strong) NSString* minimumAge;
@end

@interface ServerErrorBarcodeItemNotReturnedMessage : ServerErrorMessage
@property (nonatomic, strong) NSString* originalItemPrice;
@property (nonatomic, strong) NSString* originalTransactionDate;
@property (nonatomic, strong) NSNumber *originalTransactionId;
@property (nonatomic, assign) NSInteger originalTransactionIdValue;
@property (nonatomic, strong) NSNumber *originalTransactionStoreId;
@property (nonatomic, assign) NSInteger originalTransactionStoreIdValue;
@property (nonatomic, strong) NSNumber *originalTransactionTill;
@property (nonatomic, assign) NSInteger originalTransactionTillValue;
@end

@interface ServerErrorDeviceStationClaimedMessage : ServerErrorMessage
@property (nonatomic, strong) NSString* terminal;
@property (nonatomic, strong) NSString* user;
@end

@interface ServerErrorDeviceStationMessage : ServerErrorMessage
@property (nonatomic, strong) NSString* deviceStationId;
@property (nonatomic, strong) NSString* deviceStationName;
@property (nonatomic, strong) NSString* peripherals;
@property (nonatomic, strong) NSString* terminal;
@property (nonatomic, strong) NSString* user;
@end

@interface ServerErrorUnitOfMeasureMessage : ServerErrorMessage
@property (nonatomic, strong) NSString* displayUnits;
@property (nonatomic, strong) NSString* maxValue;
@property (nonatomic, strong) NSString* minValue;
@property (nonatomic, strong) NSString* scale;
@end

@interface ServerRequest : RESTObject
@property (nonatomic, strong) ServerError* error;
@property (nonatomic, strong) NSNumber *result;
@property (nonatomic, assign) BOOL resultValue;
@end

@interface AssociateSalesHistoryRequest : ServerRequest
@property (nonatomic, strong) NSString* associateId;
@property (nonatomic, strong) NSArray* baskets;
@property (nonatomic, strong) NSString* from;
@property (nonatomic, strong) NSString* locationId;
@property (nonatomic, strong) NSString* status;
@property (nonatomic, strong) NSString* to;
@end

@interface CustomerSearch : ServerRequest
@property (nonatomic, strong) NSString* cardNo;
@property (nonatomic, strong) NSString* cardType;
@property (nonatomic, strong) NSString* customerId;
@property (nonatomic, strong) NSArray* customers;
@property (nonatomic, strong) NSString* email;
@end

@interface GiftReceiptRequest : ServerRequest
@property (nonatomic, strong) BasketAssociateDTO* basket;
@property (nonatomic, strong) NSArray* basketItems;
@property (nonatomic, strong) NSString* emailAddress;
@property (nonatomic, assign) BOOL emailRequested;
@property (nonatomic, assign) BOOL printRequested;
@property (nonatomic, strong) NSString* textReceipt;
@end

@interface ProductSearch : ServerRequest
@property (nonatomic, strong) NSArray* items;
@property (nonatomic, strong) NSString* location;
@property (nonatomic, strong) NSString* name;
@property (nonatomic, strong) NSNumber *reason;
@property (nonatomic, assign) NSInteger reasonValue;
@property (nonatomic, strong) NSString* sku;
@end

@interface ShopperDetailsDTO : RESTObject
@property (nonatomic, strong) NSArray* keyValue;
@property (nonatomic, strong) KeyValues* keyValues;
@end

@interface SoftKey : RESTObject
@property (nonatomic, strong) NSString* backgroundColor;
@property (nonatomic, strong) NSString* data;
@property (nonatomic, strong) NSString* dataLocation;
@property (nonatomic, strong) NSString* defaultSelection;
@property (nonatomic, strong) NSString* displayClass;
@property (nonatomic, strong) NSString* downImage;
@property (nonatomic, strong) NSString* foregroundColor;
@property (nonatomic, strong) NSString* keyDescription; // Localized, use primitiveKeyDescription to access _keyDescription
@property (nonatomic, readonly) NSString* primitiveKeyDescription;
@property (nonatomic, strong) NSArray* keyDescriptionLanguages;
@property (nonatomic, strong) NSString* keyId;
@property (nonatomic, strong) NSString* keyType;
@property (nonatomic, strong) NSString* posFunc;
@property (nonatomic, strong) NSArray* softKeyEvents;
@property (nonatomic, strong) NSString* softKeyMenu;
@property (nonatomic, strong) NSString* tenderType;
@property (nonatomic, strong) NSString* upc;
@property (nonatomic, strong) NSString* upImage;
@end

@interface SoftKeyEventDTO : RESTObject
@end

@interface DataEventDTO : SoftKeyEventDTO
@property (nonatomic, strong) NSString* dataLocation;
@property (nonatomic, strong) NSString* dataValue;
@end

@interface POSFunctionSoftKeyEventDTO : SoftKeyEventDTO
@property (nonatomic, strong) NSString* function;
@end

@interface SoftKeyInputEventDTO : SoftKeyEventDTO
@property (nonatomic, strong) NSString* dataLocation;
@property (nonatomic, strong) NSString* dataSource;
@property (nonatomic, strong) NSString* editClass;
@property (nonatomic, strong) NSString* fieldDescription;
@property (nonatomic, strong) NSString* fieldId;
@property (nonatomic, strong) NSString* keyboards;
@property (nonatomic, assign) BOOL required;
@end

@interface UIContextEventDTO : SoftKeyEventDTO
@property (nonatomic, strong) NSString* context;
@end

@interface Store : RESTObject
@property (nonatomic, strong) NSString* address1;
@property (nonatomic, strong) NSString* address2;
@property (nonatomic, strong) NSString* city;
@property (nonatomic, assign) double distance;
@property (nonatomic, strong) NSString* email;
@property (nonatomic, strong) NSString* hours1;
@property (nonatomic, strong) NSString* hours2;
@property (nonatomic, strong) NSString* hours3;
@property (nonatomic, strong) NSString* hours4;
@property (nonatomic, strong) NSString* hours5;
@property (nonatomic, strong) NSString* hours6;
@property (nonatomic, strong) NSString* hours7;
@property (nonatomic, strong) NSNumber* latitude;
@property (nonatomic, strong) NSNumber* longitude;
@property (nonatomic, strong) NSString* name;
@property (nonatomic, strong) NSString* phone;
@property (nonatomic, strong) NSString* shortName;
@property (nonatomic, strong) NSString* state;
@property (nonatomic, strong) NSString* storeId;
@property (nonatomic, strong) NSArray* tills;
@property (nonatomic, strong) NSString* zipCode;
@end

@interface Stores : RESTObject
@property (nonatomic, strong) NSArray* stores;
+(instancetype)storesWithArray:(NSArray*)array;
@end

@interface Tender : RESTObject
@property (nonatomic, assign) BOOL allowsChange;
@property (nonatomic, strong) NSString* currencyCode;
@property (nonatomic, assign) BOOL isEFTTender;
@property (nonatomic, assign) BOOL isForeignCurrency;
@property (nonatomic, strong) NSString* maximumAmount;
@property (nonatomic, strong) NSString* minimumAmount;
@property (nonatomic, assign) double minimumDenomination;
@property (nonatomic, assign) BOOL promptForAmount;
@property (nonatomic, assign) BOOL promptForAuthCode;
@property (nonatomic, assign) BOOL promptForCardNumber;
@property (nonatomic, strong) NSString* promptForCardNumberCaptureMethod;
@property (nonatomic, assign) BOOL promptForReference;
@property (nonatomic, assign) BOOL promptForTip;
@property (nonatomic, strong) NSString* tenderClass;
@property (nonatomic, strong) NSString* tenderName; // Localized, use primitiveTenderName to access _tenderName
@property (nonatomic, readonly) NSString* primitiveTenderName;
@property (nonatomic, strong) NSArray* tenderNameLanguages;
@property (nonatomic, strong) NSString* tenderType;
@property (nonatomic, assign) BOOL trainingModeTender;
@property (nonatomic, strong) NSString* validationRegex;
@end

@interface Till : RESTObject
@property (nonatomic, strong) NSString* till;
+(instancetype)tillWithString:(NSString*)string;
@end
#pragma clang diagnostic pop
