/* web definitions implementation */

#import "RESTController.h"
@implementation RESTController

-(BasketAssociateDTO*)addItemWithAgeRestriction:(NSString*)ageRestriction barcodeType:(NSString*)barcodeType basketID:(NSString*)basketID entryMethod:(NSString*)entryMethod itemDescription:(NSArray*)itemDescription itemScanID:(NSString*)itemScanID itemSerialNumber:(NSString*)itemSerialNumber measure:(NSString*)measure originalItemDescription:(NSString*)originalItemDescription originalItemScanId:(NSString*)originalItemScanId price:(NSString*)price quantity:(NSString*)quantity track1:(NSString*)track1 track2:(NSString*)track2 track3:(NSString*)track3 error:(NSError **)error serverError:(ServerError **)serverError
{
	AddItemInvocation *invocation = [[AddItemInvocation alloc] init];
	invocation.ageRestriction = ageRestriction;
	invocation.barcodeType = barcodeType;
	invocation.basketID = basketID;
	invocation.entryMethod = entryMethod;
	invocation.itemDescription = itemDescription;
	invocation.itemScanID = itemScanID;
	invocation.itemSerialNumber = itemSerialNumber;
	invocation.measure = measure;
	invocation.originalItemDescription = originalItemDescription;
	invocation.originalItemScanId = originalItemScanId;
	invocation.price = price;
	invocation.quantity = quantity;
	invocation.track1 = track1;
	invocation.track2 = track2;
	invocation.track3 = track3;
	__block BasketAssociateDTO* result;
	[self invokeSynchronous:invocation
	                 server:nil
	                   etag:nil
	               complete:^(RESTInvocation*invocation, NSString*invocationServer, id invocationResult, NSError *invocationError, ServerError *invocationServerError, NSString *invocationEtag) {
	                   result = invocationResult;
	                   if (error)
	                   {
	                       *error = invocationError;
	                   }
	                   if (serverError)
	                   {
	                       *serverError = invocationServerError;
	                   }
	               }];
	return result;
}

-(BasketAssociateDTO*)addItemsWithBasketID:(NSString*)basketID entryMethod:(NSString*)entryMethod itemDescription:(NSArray*)itemDescription itemScanID:(NSArray*)itemScanID error:(NSError **)error serverError:(ServerError **)serverError
{
	AddItemsInvocation *invocation = [[AddItemsInvocation alloc] init];
	invocation.basketID = basketID;
	invocation.entryMethod = entryMethod;
	invocation.itemDescription = itemDescription;
	invocation.itemScanID = itemScanID;
	__block BasketAssociateDTO* result;
	[self invokeSynchronous:invocation
	                 server:nil
	                   etag:nil
	               complete:^(RESTInvocation*invocation, NSString*invocationServer, id invocationResult, NSError *invocationError, ServerError *invocationServerError, NSString *invocationEtag) {
	                   result = invocationResult;
	                   if (error)
	                   {
	                       *error = invocationError;
	                   }
	                   if (serverError)
	                   {
	                       *serverError = invocationServerError;
	                   }
	               }];
	return result;
}

-(BasketAssociateDTO*)adjustBasketItemWithBasketID:(NSString*)basketID basketItemID:(NSArray*)basketItemID operatorID:(NSString*)operatorID error:(NSError **)error serverError:(ServerError **)serverError
{
	AdjustBasketItemInvocation *invocation = [[AdjustBasketItemInvocation alloc] init];
	invocation.basketID = basketID;
	invocation.basketItemID = basketItemID;
	invocation.operatorID = operatorID;
	__block BasketAssociateDTO* result;
	[self invokeSynchronous:invocation
	                 server:nil
	                   etag:nil
	               complete:^(RESTInvocation*invocation, NSString*invocationServer, id invocationResult, NSError *invocationError, ServerError *invocationServerError, NSString *invocationEtag) {
	                   result = invocationResult;
	                   if (error)
	                   {
	                       *error = invocationError;
	                   }
	                   if (serverError)
	                   {
	                       *serverError = invocationServerError;
	                   }
	               }];
	return result;
}

-(BasketAssociateDTO*)adjustBasketItemPriceWithBasketID:(NSString*)basketID basketItemID:(NSString*)basketItemID price:(NSString*)price reasonDescription:(NSString*)reasonDescription reasonID:(NSString*)reasonID error:(NSError **)error serverError:(ServerError **)serverError
{
	AdjustBasketItemPriceInvocation *invocation = [[AdjustBasketItemPriceInvocation alloc] init];
	invocation.basketID = basketID;
	invocation.basketItemID = basketItemID;
	invocation.price = price;
	invocation.reasonDescription = reasonDescription;
	invocation.reasonID = reasonID;
	__block BasketAssociateDTO* result;
	[self invokeSynchronous:invocation
	                 server:nil
	                   etag:nil
	               complete:^(RESTInvocation*invocation, NSString*invocationServer, id invocationResult, NSError *invocationError, ServerError *invocationServerError, NSString *invocationEtag) {
	                   result = invocationResult;
	                   if (error)
	                   {
	                       *error = invocationError;
	                   }
	                   if (serverError)
	                   {
	                       *serverError = invocationServerError;
	                   }
	               }];
	return result;
}

-(BasketAssociateDTO*)adjustBasketItemQuantityWithBasketID:(NSString*)basketID basketItemID:(NSString*)basketItemID quantity:(NSInteger)quantity error:(NSError **)error serverError:(ServerError **)serverError
{
	AdjustBasketItemQuantityInvocation *invocation = [[AdjustBasketItemQuantityInvocation alloc] init];
	invocation.basketID = basketID;
	invocation.basketItemID = basketItemID;
	invocation.quantity = quantity;
	__block BasketAssociateDTO* result;
	[self invokeSynchronous:invocation
	                 server:nil
	                   etag:nil
	               complete:^(RESTInvocation*invocation, NSString*invocationServer, id invocationResult, NSError *invocationError, ServerError *invocationServerError, NSString *invocationEtag) {
	                   result = invocationResult;
	                   if (error)
	                   {
	                       *error = invocationError;
	                   }
	                   if (serverError)
	                   {
	                       *serverError = invocationServerError;
	                   }
	               }];
	return result;
}

-(BasketDTO*)adjustSaleWithBasket:(BasketDTO*)basket error:(NSError **)error serverError:(ServerError **)serverError
{
	AdjustSaleInvocation *invocation = [[AdjustSaleInvocation alloc] init];
	invocation.basket = basket;
	__block BasketDTO* result;
	[self invokeSynchronous:invocation
	                 server:nil
	                   etag:nil
	               complete:^(RESTInvocation*invocation, NSString*invocationServer, id invocationResult, NSError *invocationError, ServerError *invocationServerError, NSString *invocationEtag) {
	                   result = invocationResult;
	                   if (error)
	                   {
	                       *error = invocationError;
	                   }
	                   if (serverError)
	                   {
	                       *serverError = invocationServerError;
	                   }
	               }];
	return result;
}

-(BasketTaxFreeDocumentDTO*)adjustTaxFreeFormWithBasket:(BasketTaxFreeDocumentDTO*)basket error:(NSError **)error serverError:(ServerError **)serverError
{
	AdjustTaxFreeFormInvocation *invocation = [[AdjustTaxFreeFormInvocation alloc] init];
	invocation.basket = basket;
	__block BasketTaxFreeDocumentDTO* result;
	[self invokeSynchronous:invocation
	                 server:nil
	                   etag:nil
	               complete:^(RESTInvocation*invocation, NSString*invocationServer, id invocationResult, NSError *invocationError, ServerError *invocationServerError, NSString *invocationEtag) {
	                   result = invocationResult;
	                   if (error)
	                   {
	                       *error = invocationError;
	                   }
	                   if (serverError)
	                   {
	                       *serverError = invocationServerError;
	                   }
	               }];
	return result;
}

-(BasketAssociateDTO*)applyBasketDiscountWithAmount:(NSString*)amount basketID:(NSString*)basketID discountCode:(NSString*)discountCode discountType:(NSString*)discountType reasonDescription:(NSString*)reasonDescription reasonID:(NSString*)reasonID error:(NSError **)error serverError:(ServerError **)serverError
{
	ApplyBasketDiscountInvocation *invocation = [[ApplyBasketDiscountInvocation alloc] init];
	invocation.amount = amount;
	invocation.basketID = basketID;
	invocation.discountCode = discountCode;
	invocation.discountType = discountType;
	invocation.reasonDescription = reasonDescription;
	invocation.reasonID = reasonID;
	__block BasketAssociateDTO* result;
	[self invokeSynchronous:invocation
	                 server:nil
	                   etag:nil
	               complete:^(RESTInvocation*invocation, NSString*invocationServer, id invocationResult, NSError *invocationError, ServerError *invocationServerError, NSString *invocationEtag) {
	                   result = invocationResult;
	                   if (error)
	                   {
	                       *error = invocationError;
	                   }
	                   if (serverError)
	                   {
	                       *serverError = invocationServerError;
	                   }
	               }];
	return result;
}

-(BasketAssociateDTO*)applyBasketItemDiscountWithAmount:(NSString*)amount basketID:(NSString*)basketID basketItemID:(NSArray*)basketItemID discountCode:(NSString*)discountCode discountType:(NSString*)discountType reasonDescription:(NSString*)reasonDescription reasonID:(NSString*)reasonID error:(NSError **)error serverError:(ServerError **)serverError
{
	ApplyBasketItemDiscountInvocation *invocation = [[ApplyBasketItemDiscountInvocation alloc] init];
	invocation.amount = amount;
	invocation.basketID = basketID;
	invocation.basketItemID = basketItemID;
	invocation.discountCode = discountCode;
	invocation.discountType = discountType;
	invocation.reasonDescription = reasonDescription;
	invocation.reasonID = reasonID;
	__block BasketAssociateDTO* result;
	[self invokeSynchronous:invocation
	                 server:nil
	                   etag:nil
	               complete:^(RESTInvocation*invocation, NSString*invocationServer, id invocationResult, NSError *invocationError, ServerError *invocationServerError, NSString *invocationEtag) {
	                   result = invocationResult;
	                   if (error)
	                   {
	                       *error = invocationError;
	                   }
	                   if (serverError)
	                   {
	                       *serverError = invocationServerError;
	                   }
	               }];
	return result;
}

-(BasketAssociateDTO*)assignCustomerWithAffiliationType:(NSString*)affiliationType basketID:(NSString*)basketID customer:(NSString*)customer error:(NSError **)error serverError:(ServerError **)serverError
{
	AssignCustomerInvocation *invocation = [[AssignCustomerInvocation alloc] init];
	invocation.affiliationType = affiliationType;
	invocation.basketID = basketID;
	invocation.customer = customer;
	__block BasketAssociateDTO* result;
	[self invokeSynchronous:invocation
	                 server:nil
	                   etag:nil
	               complete:^(RESTInvocation*invocation, NSString*invocationServer, id invocationResult, NSError *invocationError, ServerError *invocationServerError, NSString *invocationEtag) {
	                   result = invocationResult;
	                   if (error)
	                   {
	                       *error = invocationError;
	                   }
	                   if (serverError)
	                   {
	                       *serverError = invocationServerError;
	                   }
	               }];
	return result;
}

-(BasketAssociateDTO*)assignEmailAddressWithBasketID:(NSString*)basketID emailAddress:(NSString*)emailAddress error:(NSError **)error serverError:(ServerError **)serverError
{
	AssignEmailAddressInvocation *invocation = [[AssignEmailAddressInvocation alloc] init];
	invocation.basketID = basketID;
	invocation.emailAddress = emailAddress;
	__block BasketAssociateDTO* result;
	[self invokeSynchronous:invocation
	                 server:nil
	                   etag:nil
	               complete:^(RESTInvocation*invocation, NSString*invocationServer, id invocationResult, NSError *invocationError, ServerError *invocationServerError, NSString *invocationEtag) {
	                   result = invocationResult;
	                   if (error)
	                   {
	                       *error = invocationError;
	                   }
	                   if (serverError)
	                   {
	                       *serverError = invocationServerError;
	                   }
	               }];
	return result;
}

-(DeviceStation*)associateDeviceStationWithDeviceStationId:(NSString*)deviceStationId tillId:(NSString*)tillId error:(NSError **)error serverError:(ServerError **)serverError
{
	AssociateDeviceStationInvocation *invocation = [[AssociateDeviceStationInvocation alloc] init];
	invocation.deviceStationId = deviceStationId;
	invocation.tillId = tillId;
	__block DeviceStation* result;
	[self invokeSynchronous:invocation
	                 server:nil
	                   etag:nil
	               complete:^(RESTInvocation*invocation, NSString*invocationServer, id invocationResult, NSError *invocationError, ServerError *invocationServerError, NSString *invocationEtag) {
	                   result = invocationResult;
	                   if (error)
	                   {
	                       *error = invocationError;
	                   }
	                   if (serverError)
	                   {
	                       *serverError = invocationServerError;
	                   }
	               }];
	return result;
}

-(NSString*)associateTextReceiptWithReceiptId:(NSString*)receiptId error:(NSError **)error serverError:(ServerError **)serverError
{
	AssociateTextReceiptInvocation *invocation = [[AssociateTextReceiptInvocation alloc] init];
	invocation.receiptId = receiptId;
	__block NSString* result;
	[self invokeSynchronous:invocation
	                 server:nil
	                   etag:nil
	               complete:^(RESTInvocation*invocation, NSString*invocationServer, id invocationResult, NSError *invocationError, ServerError *invocationServerError, NSString *invocationEtag) {
	                   result = invocationResult;
	                   if (error)
	                   {
	                       *error = invocationError;
	                   }
	                   if (serverError)
	                   {
	                       *serverError = invocationServerError;
	                   }
	               }];
	return result;
}

-(BasketAssociateDTO*)cancelBasketWithBasketID:(NSString*)basketID error:(NSError **)error serverError:(ServerError **)serverError
{
	CancelBasketInvocation *invocation = [[CancelBasketInvocation alloc] init];
	invocation.basketID = basketID;
	__block BasketAssociateDTO* result;
	[self invokeSynchronous:invocation
	                 server:nil
	                   etag:nil
	               complete:^(RESTInvocation*invocation, NSString*invocationServer, id invocationResult, NSError *invocationError, ServerError *invocationServerError, NSString *invocationEtag) {
	                   result = invocationResult;
	                   if (error)
	                   {
	                       *error = invocationError;
	                   }
	                   if (serverError)
	                   {
	                       *serverError = invocationServerError;
	                   }
	               }];
	return result;
}

-(BasketAssociateDTO*)cancelCheckoutBasketWithBasketID:(NSString*)basketID error:(NSError **)error serverError:(ServerError **)serverError
{
	CancelCheckoutBasketInvocation *invocation = [[CancelCheckoutBasketInvocation alloc] init];
	invocation.basketID = basketID;
	__block BasketAssociateDTO* result;
	[self invokeSynchronous:invocation
	                 server:nil
	                   etag:nil
	               complete:^(RESTInvocation*invocation, NSString*invocationServer, id invocationResult, NSError *invocationError, ServerError *invocationServerError, NSString *invocationEtag) {
	                   result = invocationResult;
	                   if (error)
	                   {
	                       *error = invocationError;
	                   }
	                   if (serverError)
	                   {
	                       *serverError = invocationServerError;
	                   }
	               }];
	return result;
}

-(BasketTaxFreeDocumentDTO*)cancelTaxFreeFormWithBasketID:(NSString*)basketID error:(NSError **)error serverError:(ServerError **)serverError
{
	CancelTaxFreeFormInvocation *invocation = [[CancelTaxFreeFormInvocation alloc] init];
	invocation.basketID = basketID;
	__block BasketTaxFreeDocumentDTO* result;
	[self invokeSynchronous:invocation
	                 server:nil
	                   etag:nil
	               complete:^(RESTInvocation*invocation, NSString*invocationServer, id invocationResult, NSError *invocationError, ServerError *invocationServerError, NSString *invocationEtag) {
	                   result = invocationResult;
	                   if (error)
	                   {
	                       *error = invocationError;
	                   }
	                   if (serverError)
	                   {
	                       *serverError = invocationServerError;
	                   }
	               }];
	return result;
}

-(NSArray*)cardsForCustomerWithCustomerId:(NSString*)customerId error:(NSError **)error serverError:(ServerError **)serverError
{
	CardsForCustomerInvocation *invocation = [[CardsForCustomerInvocation alloc] init];
	invocation.customerId = customerId;
	__block NSArray* result;
	[self invokeSynchronous:invocation
	                 server:nil
	                   etag:nil
	               complete:^(RESTInvocation*invocation, NSString*invocationServer, id invocationResult, NSError *invocationError, ServerError *invocationServerError, NSString *invocationEtag) {
	                   result = invocationResult;
	                   if (error)
	                   {
	                       *error = invocationError;
	                   }
	                   if (serverError)
	                   {
	                       *serverError = invocationServerError;
	                   }
	               }];
	return result;
}

-(void)changeCashDrawerWithDeviceStationId:(NSString*)deviceStationId error:(NSError **)error serverError:(ServerError **)serverError
{
	ChangeCashDrawerInvocation *invocation = [[ChangeCashDrawerInvocation alloc] init];
	invocation.deviceStationId = deviceStationId;
	[self invokeSynchronous:invocation
	                 server:nil
	                   etag:nil
	               complete:^(RESTInvocation*invocation, NSString*invocationServer, id invocationResult, NSError *invocationError, ServerError *invocationServerError, NSString *invocationEtag) {
	                   if (error)
	                   {
	                       *error = invocationError;
	                   }
	                   if (serverError)
	                   {
	                       *serverError = invocationServerError;
	                   }
	               }];
}

-(BasketAssociateDTO*)checkoutBasketWithBasketID:(NSString*)basketID currencyCode:(NSString*)currencyCode error:(NSError **)error serverError:(ServerError **)serverError
{
	CheckoutBasketInvocation *invocation = [[CheckoutBasketInvocation alloc] init];
	invocation.basketID = basketID;
	invocation.currencyCode = currencyCode;
	__block BasketAssociateDTO* result;
	[self invokeSynchronous:invocation
	                 server:nil
	                   etag:nil
	               complete:^(RESTInvocation*invocation, NSString*invocationServer, id invocationResult, NSError *invocationError, ServerError *invocationServerError, NSString *invocationEtag) {
	                   result = invocationResult;
	                   if (error)
	                   {
	                       *error = invocationError;
	                   }
	                   if (serverError)
	                   {
	                       *serverError = invocationServerError;
	                   }
	               }];
	return result;
}

-(id)controlVoidTransactionWithBasketID:(NSString*)basketID businessDay:(NSDate*)businessDay confirmedAmount:(NSString*)confirmedAmount scanData:(NSString*)scanData storeID:(NSString*)storeID transactionType:(NSString*)transactionType workstationID:(NSString*)workstationID error:(NSError **)error serverError:(ServerError **)serverError
{
	ControlVoidTransactionInvocation *invocation = [[ControlVoidTransactionInvocation alloc] init];
	invocation.basketID = basketID;
	invocation.businessDay = businessDay;
	invocation.confirmedAmount = confirmedAmount;
	invocation.scanData = scanData;
	invocation.storeID = storeID;
	invocation.transactionType = transactionType;
	invocation.workstationID = workstationID;
	__block id result;
	[self invokeSynchronous:invocation
	                 server:nil
	                   etag:nil
	               complete:^(RESTInvocation*invocation, NSString*invocationServer, id invocationResult, NSError *invocationError, ServerError *invocationServerError, NSString *invocationEtag) {
	                   result = invocationResult;
	                   if (error)
	                   {
	                       *error = invocationError;
	                   }
	                   if (serverError)
	                   {
	                       *serverError = invocationServerError;
	                   }
	               }];
	return result;
}

-(id)createBasketWithBasketID:(NSString*)basketID businessDay:(NSDate*)businessDay deviceID:(NSString*)deviceID issuerCountryId:(NSString*)issuerCountryId lastTransaction:(NSString*)lastTransaction scanData:(NSString*)scanData storeID:(NSString*)storeID transactionType:(NSString*)transactionType workstationID:(NSString*)workstationID error:(NSError **)error serverError:(ServerError **)serverError
{
	CreateBasketInvocation *invocation = [[CreateBasketInvocation alloc] init];
	invocation.basketID = basketID;
	invocation.businessDay = businessDay;
	invocation.deviceID = deviceID;
	invocation.issuerCountryId = issuerCountryId;
	invocation.lastTransaction = lastTransaction;
	invocation.scanData = scanData;
	invocation.storeID = storeID;
	invocation.transactionType = transactionType;
	invocation.workstationID = workstationID;
	__block id result;
	[self invokeSynchronous:invocation
	                 server:nil
	                   etag:nil
	               complete:^(RESTInvocation*invocation, NSString*invocationServer, id invocationResult, NSError *invocationError, ServerError *invocationServerError, NSString *invocationEtag) {
	                   result = invocationResult;
	                   if (error)
	                   {
	                       *error = invocationError;
	                   }
	                   if (serverError)
	                   {
	                       *serverError = invocationServerError;
	                   }
	               }];
	return result;
}

-(void)createCustomerEmailWithEmailAddress:(NSString*)emailAddress includeGiftReceipt:(BOOL)includeGiftReceipt receiptID:(NSString*)receiptID error:(NSError **)error serverError:(ServerError **)serverError
{
	CreateCustomerEmailInvocation *invocation = [[CreateCustomerEmailInvocation alloc] init];
	invocation.emailAddress = emailAddress;
	invocation.includeGiftReceipt = includeGiftReceipt;
	invocation.receiptID = receiptID;
	[self invokeSynchronous:invocation
	                 server:nil
	                   etag:nil
	               complete:^(RESTInvocation*invocation, NSString*invocationServer, id invocationResult, NSError *invocationError, ServerError *invocationServerError, NSString *invocationEtag) {
	                   if (error)
	                   {
	                       *error = invocationError;
	                   }
	                   if (serverError)
	                   {
	                       *serverError = invocationServerError;
	                   }
	               }];
}

-(BasketAssociateDTO*)createEFTTenderWithDeviceStationId:(NSString*)deviceStationId tender:(EFTPayment*)tender error:(NSError **)error serverError:(ServerError **)serverError
{
	CreateEFTTenderInvocation *invocation = [[CreateEFTTenderInvocation alloc] init];
	invocation.deviceStationId = deviceStationId;
	invocation.tender = tender;
	__block BasketAssociateDTO* result;
	[self invokeSynchronous:invocation
	                 server:nil
	                   etag:nil
	               complete:^(RESTInvocation*invocation, NSString*invocationServer, id invocationResult, NSError *invocationError, ServerError *invocationServerError, NSString *invocationEtag) {
	                   result = invocationResult;
	                   if (error)
	                   {
	                       *error = invocationError;
	                   }
	                   if (serverError)
	                   {
	                       *serverError = invocationServerError;
	                   }
	               }];
	return result;
}

-(BasketAssociateDTO*)createStandardTenderWithTender:(StandardPayment*)tender error:(NSError **)error serverError:(ServerError **)serverError
{
	CreateStandardTenderInvocation *invocation = [[CreateStandardTenderInvocation alloc] init];
	invocation.tender = tender;
	__block BasketAssociateDTO* result;
	[self invokeSynchronous:invocation
	                 server:nil
	                   etag:nil
	               complete:^(RESTInvocation*invocation, NSString*invocationServer, id invocationResult, NSError *invocationError, ServerError *invocationServerError, NSString *invocationEtag) {
	                   result = invocationResult;
	                   if (error)
	                   {
	                       *error = invocationError;
	                   }
	                   if (serverError)
	                   {
	                       *serverError = invocationServerError;
	                   }
	               }];
	return result;
}

-(BasketAssociateDTO*)createVerifoneTenderWithTender:(VerifonePayment*)tender error:(NSError **)error serverError:(ServerError **)serverError
{
	CreateVerifoneTenderInvocation *invocation = [[CreateVerifoneTenderInvocation alloc] init];
	invocation.tender = tender;
	__block BasketAssociateDTO* result;
	[self invokeSynchronous:invocation
	                 server:nil
	                   etag:nil
	               complete:^(RESTInvocation*invocation, NSString*invocationServer, id invocationResult, NSError *invocationError, ServerError *invocationServerError, NSString *invocationEtag) {
	                   result = invocationResult;
	                   if (error)
	                   {
	                       *error = invocationError;
	                   }
	                   if (serverError)
	                   {
	                       *serverError = invocationServerError;
	                   }
	               }];
	return result;
}

-(BasketAssociateDTO*)createYespayTenderWithTender:(YespayPayment*)tender error:(NSError **)error serverError:(ServerError **)serverError
{
	CreateYespayTenderInvocation *invocation = [[CreateYespayTenderInvocation alloc] init];
	invocation.tender = tender;
	__block BasketAssociateDTO* result;
	[self invokeSynchronous:invocation
	                 server:nil
	                   etag:nil
	               complete:^(RESTInvocation*invocation, NSString*invocationServer, id invocationResult, NSError *invocationError, ServerError *invocationServerError, NSString *invocationEtag) {
	                   result = invocationResult;
	                   if (error)
	                   {
	                       *error = invocationError;
	                   }
	                   if (serverError)
	                   {
	                       *serverError = invocationServerError;
	                   }
	               }];
	return result;
}

-(NSArray*)customersWithEmail:(NSString*)email error:(NSError **)error serverError:(ServerError **)serverError
{
	CustomersWithEmailInvocation *invocation = [[CustomersWithEmailInvocation alloc] init];
	invocation.email = email;
	__block NSArray* result;
	[self invokeSynchronous:invocation
	                 server:nil
	                   etag:nil
	               complete:^(RESTInvocation*invocation, NSString*invocationServer, id invocationResult, NSError *invocationError, ServerError *invocationServerError, NSString *invocationEtag) {
	                   result = invocationResult;
	                   if (error)
	                   {
	                       *error = invocationError;
	                   }
	                   if (serverError)
	                   {
	                       *serverError = invocationServerError;
	                   }
	               }];
	return result;
}

-(Customer*)customerWithCardNo:(NSString*)cardNo cardType:(NSString*)cardType error:(NSError **)error serverError:(ServerError **)serverError
{
	CustomerWithCardNoInvocation *invocation = [[CustomerWithCardNoInvocation alloc] init];
	invocation.cardNo = cardNo;
	invocation.cardType = cardType;
	__block Customer* result;
	[self invokeSynchronous:invocation
	                 server:nil
	                   etag:nil
	               complete:^(RESTInvocation*invocation, NSString*invocationServer, id invocationResult, NSError *invocationError, ServerError *invocationServerError, NSString *invocationEtag) {
	                   result = invocationResult;
	                   if (error)
	                   {
	                       *error = invocationError;
	                   }
	                   if (serverError)
	                   {
	                       *serverError = invocationServerError;
	                   }
	               }];
	return result;
}

-(Customer*)customerWithCustomerId:(NSString*)customerId error:(NSError **)error serverError:(ServerError **)serverError
{
	CustomerWithCustomerIdInvocation *invocation = [[CustomerWithCustomerIdInvocation alloc] init];
	invocation.customerId = customerId;
	__block Customer* result;
	[self invokeSynchronous:invocation
	                 server:nil
	                   etag:nil
	               complete:^(RESTInvocation*invocation, NSString*invocationServer, id invocationResult, NSError *invocationError, ServerError *invocationServerError, NSString *invocationEtag) {
	                   result = invocationResult;
	                   if (error)
	                   {
	                       *error = invocationError;
	                   }
	                   if (serverError)
	                   {
	                       *serverError = invocationServerError;
	                   }
	               }];
	return result;
}

-(DeviceStation*)disassociateDeviceStationWithDeviceStationId:(NSString*)deviceStationId tillId:(NSString*)tillId error:(NSError **)error serverError:(ServerError **)serverError
{
	DisassociateDeviceStationInvocation *invocation = [[DisassociateDeviceStationInvocation alloc] init];
	invocation.deviceStationId = deviceStationId;
	invocation.tillId = tillId;
	__block DeviceStation* result;
	[self invokeSynchronous:invocation
	                 server:nil
	                   etag:nil
	               complete:^(RESTInvocation*invocation, NSString*invocationServer, id invocationResult, NSError *invocationError, ServerError *invocationServerError, NSString *invocationEtag) {
	                   result = invocationResult;
	                   if (error)
	                   {
	                       *error = invocationError;
	                   }
	                   if (serverError)
	                   {
	                       *serverError = invocationServerError;
	                   }
	               }];
	return result;
}

-(Item*)findItemWithScanID:(NSString*)scanID storeID:(NSString*)storeID error:(NSError **)error serverError:(ServerError **)serverError
{
	FindItemInvocation *invocation = [[FindItemInvocation alloc] init];
	invocation.scanID = scanID;
	invocation.storeID = storeID;
	__block Item* result;
	[self invokeSynchronous:invocation
	                 server:nil
	                   etag:nil
	               complete:^(RESTInvocation*invocation, NSString*invocationServer, id invocationResult, NSError *invocationError, ServerError *invocationServerError, NSString *invocationEtag) {
	                   result = invocationResult;
	                   if (error)
	                   {
	                       *error = invocationError;
	                   }
	                   if (serverError)
	                   {
	                       *serverError = invocationServerError;
	                   }
	               }];
	return result;
}

-(ReturnableSaleItem*)findLineIdWithItemScanId:(NSString*)itemScanId originalBasketId:(NSString*)originalBasketId originalStoreId:(NSString*)originalStoreId originalTillNumber:(NSString*)originalTillNumber originalTransactionDate:(NSString*)originalTransactionDate error:(NSError **)error serverError:(ServerError **)serverError
{
	FindLineIdInvocation *invocation = [[FindLineIdInvocation alloc] init];
	invocation.itemScanId = itemScanId;
	invocation.originalBasketId = originalBasketId;
	invocation.originalStoreId = originalStoreId;
	invocation.originalTillNumber = originalTillNumber;
	invocation.originalTransactionDate = originalTransactionDate;
	__block ReturnableSaleItem* result;
	[self invokeSynchronous:invocation
	                 server:nil
	                   etag:nil
	               complete:^(RESTInvocation*invocation, NSString*invocationServer, id invocationResult, NSError *invocationError, ServerError *invocationServerError, NSString *invocationEtag) {
	                   result = invocationResult;
	                   if (error)
	                   {
	                       *error = invocationError;
	                   }
	                   if (serverError)
	                   {
	                       *serverError = invocationServerError;
	                   }
	               }];
	return result;
}

-(BasketAssociateDTO*)finishBasketWithBasketID:(NSString*)basketID deviceStationId:(NSString*)deviceStationId emailAddress:(NSString*)emailAddress emailReceipt:(BOOL)emailReceipt enablePrint:(BOOL)enablePrint printSummaryReceipt:(BOOL)printSummaryReceipt tillNumber:(NSString*)tillNumber error:(NSError **)error serverError:(ServerError **)serverError
{
	FinishBasketInvocation *invocation = [[FinishBasketInvocation alloc] init];
	invocation.basketID = basketID;
	invocation.deviceStationId = deviceStationId;
	invocation.emailAddress = emailAddress;
	invocation.emailReceipt = emailReceipt;
	invocation.enablePrint = enablePrint;
	invocation.printSummaryReceipt = printSummaryReceipt;
	invocation.tillNumber = tillNumber;
	__block BasketAssociateDTO* result;
	[self invokeSynchronous:invocation
	                 server:nil
	                   etag:nil
	               complete:^(RESTInvocation*invocation, NSString*invocationServer, id invocationResult, NSError *invocationError, ServerError *invocationServerError, NSString *invocationEtag) {
	                   result = invocationResult;
	                   if (error)
	                   {
	                       *error = invocationError;
	                   }
	                   if (serverError)
	                   {
	                       *serverError = invocationServerError;
	                   }
	               }];
	return result;
}

-(BasketTaxFreeDocumentDTO*)finishTaxFreeBasketWithBasketID:(NSString*)basketID deviceStationId:(NSString*)deviceStationId emailAddress:(NSString*)emailAddress emailReceipt:(BOOL)emailReceipt enablePrint:(BOOL)enablePrint printSummaryReceipt:(BOOL)printSummaryReceipt tillNumber:(NSString*)tillNumber error:(NSError **)error serverError:(ServerError **)serverError
{
	FinishTaxFreeBasketInvocation *invocation = [[FinishTaxFreeBasketInvocation alloc] init];
	invocation.basketID = basketID;
	invocation.deviceStationId = deviceStationId;
	invocation.emailAddress = emailAddress;
	invocation.emailReceipt = emailReceipt;
	invocation.enablePrint = enablePrint;
	invocation.printSummaryReceipt = printSummaryReceipt;
	invocation.tillNumber = tillNumber;
	__block BasketTaxFreeDocumentDTO* result;
	[self invokeSynchronous:invocation
	                 server:nil
	                   etag:nil
	               complete:^(RESTInvocation*invocation, NSString*invocationServer, id invocationResult, NSError *invocationError, ServerError *invocationServerError, NSString *invocationEtag) {
	                   result = invocationResult;
	                   if (error)
	                   {
	                       *error = invocationError;
	                   }
	                   if (serverError)
	                   {
	                       *serverError = invocationServerError;
	                   }
	               }];
	return result;
}

-(BasketAssociateDTO*)getActiveBasketWithDeviceID:(NSString*)deviceID error:(NSError **)error serverError:(ServerError **)serverError
{
	GetActiveBasketInvocation *invocation = [[GetActiveBasketInvocation alloc] init];
	invocation.deviceID = deviceID;
	__block BasketAssociateDTO* result;
	[self invokeSynchronous:invocation
	                 server:nil
	                   etag:nil
	               complete:^(RESTInvocation*invocation, NSString*invocationServer, id invocationResult, NSError *invocationError, ServerError *invocationServerError, NSString *invocationEtag) {
	                   result = invocationResult;
	                   if (error)
	                   {
	                       *error = invocationError;
	                   }
	                   if (serverError)
	                   {
	                       *serverError = invocationServerError;
	                   }
	               }];
	return result;
}

-(NSArray*)getAssociateSalesHistoryWithAssociateId:(NSString*)associateId from:(NSDate*)from locationId:(NSString*)locationId status:(NSString*)status to:(NSDate*)to error:(NSError **)error serverError:(ServerError **)serverError
{
	GetAssociateSalesHistoryInvocation *invocation = [[GetAssociateSalesHistoryInvocation alloc] init];
	invocation.associateId = associateId;
	invocation.from = from;
	invocation.locationId = locationId;
	invocation.status = status;
	invocation.to = to;
	__block NSArray* result;
	[self invokeSynchronous:invocation
	                 server:nil
	                   etag:nil
	               complete:^(RESTInvocation*invocation, NSString*invocationServer, id invocationResult, NSError *invocationError, ServerError *invocationServerError, NSString *invocationEtag) {
	                   result = invocationResult;
	                   if (error)
	                   {
	                       *error = invocationError;
	                   }
	                   if (serverError)
	                   {
	                       *serverError = invocationServerError;
	                   }
	               }];
	return result;
}

-(BasketAssociateDTO*)getBasketWithBasketID:(NSString*)basketID error:(NSError **)error serverError:(ServerError **)serverError
{
	GetBasketInvocation *invocation = [[GetBasketInvocation alloc] init];
	invocation.basketID = basketID;
	__block BasketAssociateDTO* result;
	[self invokeSynchronous:invocation
	                 server:nil
	                   etag:nil
	               complete:^(RESTInvocation*invocation, NSString*invocationServer, id invocationResult, NSError *invocationError, ServerError *invocationServerError, NSString *invocationEtag) {
	                   result = invocationResult;
	                   if (error)
	                   {
	                       *error = invocationError;
	                   }
	                   if (serverError)
	                   {
	                       *serverError = invocationServerError;
	                   }
	               }];
	return result;
}

-(Configuration*)getConfigurationWithClearServerCache:(BOOL)clearServerCache storeID:(NSString*)storeID tillNumber:(NSString*)tillNumber error:(NSError **)error serverError:(ServerError **)serverError etag:(NSString **)etag
{
	GetConfigurationInvocation *invocation = [[GetConfigurationInvocation alloc] init];
	invocation.clearServerCache = clearServerCache;
	invocation.storeID = storeID;
	invocation.tillNumber = tillNumber;
	__block Configuration* result;
	[self invokeSynchronous:invocation
	                 server:nil
	                   etag:*etag
	               complete:^(RESTInvocation*invocation, NSString*invocationServer, id invocationResult, NSError *invocationError, ServerError *invocationServerError, NSString *invocationEtag) {
	                   result = invocationResult;
	                   if (error)
	                   {
	                       *error = invocationError;
	                   }
	                   if (serverError)
	                   {
	                       *serverError = invocationServerError;
	                   }
	                   if (etag)
	                   {
	                       *etag = invocationEtag;
	                   }
	               }];
	return result;
}

-(DeviceStation*)getDeviceStationWithDeviceStationId:(NSString*)deviceStationId error:(NSError **)error serverError:(ServerError **)serverError
{
	GetDeviceStationInvocation *invocation = [[GetDeviceStationInvocation alloc] init];
	invocation.deviceStationId = deviceStationId;
	__block DeviceStation* result;
	[self invokeSynchronous:invocation
	                 server:nil
	                   etag:nil
	               complete:^(RESTInvocation*invocation, NSString*invocationServer, id invocationResult, NSError *invocationError, ServerError *invocationServerError, NSString *invocationEtag) {
	                   result = invocationResult;
	                   if (error)
	                   {
	                       *error = invocationError;
	                   }
	                   if (serverError)
	                   {
	                       *serverError = invocationServerError;
	                   }
	               }];
	return result;
}

-(NSArray*)getDeviceStationsWithDeviceType:(NSString*)deviceType storeId:(NSString*)storeId error:(NSError **)error serverError:(ServerError **)serverError
{
	GetDeviceStationsInvocation *invocation = [[GetDeviceStationsInvocation alloc] init];
	invocation.deviceType = deviceType;
	invocation.storeId = storeId;
	__block NSArray* result;
	[self invokeSynchronous:invocation
	                 server:nil
	                   etag:nil
	               complete:^(RESTInvocation*invocation, NSString*invocationServer, id invocationResult, NSError *invocationError, ServerError *invocationServerError, NSString *invocationEtag) {
	                   result = invocationResult;
	                   if (error)
	                   {
	                       *error = invocationError;
	                   }
	                   if (serverError)
	                   {
	                       *serverError = invocationServerError;
	                   }
	               }];
	return result;
}

-(NSArray*)getItemAvailabilityWithScanID:(NSString*)scanID storeID:(NSString*)storeID error:(NSError **)error serverError:(ServerError **)serverError
{
	GetItemAvailabilityInvocation *invocation = [[GetItemAvailabilityInvocation alloc] init];
	invocation.scanID = scanID;
	invocation.storeID = storeID;
	__block NSArray* result;
	[self invokeSynchronous:invocation
	                 server:nil
	                   etag:nil
	               complete:^(RESTInvocation*invocation, NSString*invocationServer, id invocationResult, NSError *invocationError, ServerError *invocationServerError, NSString *invocationEtag) {
	                   result = invocationResult;
	                   if (error)
	                   {
	                       *error = invocationError;
	                   }
	                   if (serverError)
	                   {
	                       *serverError = invocationServerError;
	                   }
	               }];
	return result;
}

-(Store*)getStoreDetailsWithStoreId:(NSString*)storeId server:(NSString *)server error:(NSError **)error serverError:(ServerError **)serverError
{
	GetStoreDetailsInvocation *invocation = [[GetStoreDetailsInvocation alloc] init];
	invocation.storeId = storeId;
	__block Store* result;
	[self invokeSynchronous:invocation
	                 server:server
	                   etag:nil
	               complete:^(RESTInvocation*invocation, NSString*invocationServer, id invocationResult, NSError *invocationError, ServerError *invocationServerError, NSString *invocationEtag) {
	                   result = invocationResult;
	                   if (error)
	                   {
	                       *error = invocationError;
	                   }
	                   if (serverError)
	                   {
	                       *serverError = invocationServerError;
	                   }
	               }];
	return result;
}

-(BasketAssociateDTO*)internetReturnWithBasketId:(NSString*)basketId itemScanId:(NSString*)itemScanId itemSerialNumber:(NSString*)itemSerialNumber originalItemDescription:(NSString*)originalItemDescription originalItemScanId:(NSString*)originalItemScanId originalReferenceId:(NSString*)originalReferenceId price:(NSString*)price reasonDescription:(NSString*)reasonDescription reasonId:(NSString*)reasonId error:(NSError **)error serverError:(ServerError **)serverError
{
	InternetReturnInvocation *invocation = [[InternetReturnInvocation alloc] init];
	invocation.basketId = basketId;
	invocation.itemScanId = itemScanId;
	invocation.itemSerialNumber = itemSerialNumber;
	invocation.originalItemDescription = originalItemDescription;
	invocation.originalItemScanId = originalItemScanId;
	invocation.originalReferenceId = originalReferenceId;
	invocation.price = price;
	invocation.reasonDescription = reasonDescription;
	invocation.reasonId = reasonId;
	__block BasketAssociateDTO* result;
	[self invokeSynchronous:invocation
	                 server:nil
	                   etag:nil
	               complete:^(RESTInvocation*invocation, NSString*invocationServer, id invocationResult, NSError *invocationError, ServerError *invocationServerError, NSString *invocationEtag) {
	                   result = invocationResult;
	                   if (error)
	                   {
	                       *error = invocationError;
	                   }
	                   if (serverError)
	                   {
	                       *serverError = invocationServerError;
	                   }
	               }];
	return result;
}

-(DeviceStation*)linkDeviceStationWithDeviceStationId:(NSString*)deviceStationId force:(BOOL)force error:(NSError **)error serverError:(ServerError **)serverError
{
	LinkDeviceStationInvocation *invocation = [[LinkDeviceStationInvocation alloc] init];
	invocation.deviceStationId = deviceStationId;
	invocation.force = force;
	__block DeviceStation* result;
	[self invokeSynchronous:invocation
	                 server:nil
	                   etag:nil
	               complete:^(RESTInvocation*invocation, NSString*invocationServer, id invocationResult, NSError *invocationError, ServerError *invocationServerError, NSString *invocationEtag) {
	                   result = invocationResult;
	                   if (error)
	                   {
	                       *error = invocationError;
	                   }
	                   if (serverError)
	                   {
	                       *serverError = invocationServerError;
	                   }
	               }];
	return result;
}

-(BasketAssociateDTO*)linkedReturnWithBasketId:(NSString*)basketId itemScanId:(NSString*)itemScanId itemSerialNumber:(NSString*)itemSerialNumber lineId:(NSString*)lineId originalItemDescription:(NSString*)originalItemDescription originalItemScanId:(NSString*)originalItemScanId originalTransactionDate:(NSString*)originalTransactionDate originalTransactionId:(NSString*)originalTransactionId originalTransactionStoreId:(NSString*)originalTransactionStoreId originalTransactionTill:(NSString*)originalTransactionTill reasonDescription:(NSString*)reasonDescription reasonId:(NSString*)reasonId error:(NSError **)error serverError:(ServerError **)serverError
{
	LinkedReturnInvocation *invocation = [[LinkedReturnInvocation alloc] init];
	invocation.basketId = basketId;
	invocation.itemScanId = itemScanId;
	invocation.itemSerialNumber = itemSerialNumber;
	invocation.lineId = lineId;
	invocation.originalItemDescription = originalItemDescription;
	invocation.originalItemScanId = originalItemScanId;
	invocation.originalTransactionDate = originalTransactionDate;
	invocation.originalTransactionId = originalTransactionId;
	invocation.originalTransactionStoreId = originalTransactionStoreId;
	invocation.originalTransactionTill = originalTransactionTill;
	invocation.reasonDescription = reasonDescription;
	invocation.reasonId = reasonId;
	__block BasketAssociateDTO* result;
	[self invokeSynchronous:invocation
	                 server:nil
	                   etag:nil
	               complete:^(RESTInvocation*invocation, NSString*invocationServer, id invocationResult, NSError *invocationError, ServerError *invocationServerError, NSString *invocationEtag) {
	                   result = invocationResult;
	                   if (error)
	                   {
	                       *error = invocationError;
	                   }
	                   if (serverError)
	                   {
	                       *serverError = invocationServerError;
	                   }
	               }];
	return result;
}

-(Stores*)listStoresWithBrandId:(NSString*)brandId isHomeStore:(BOOL)isHomeStore latitude:(double)latitude longitude:(double)longitude server:(NSString *)server error:(NSError **)error serverError:(ServerError **)serverError
{
	ListStoresInvocation *invocation = [[ListStoresInvocation alloc] init];
	invocation.brandId = brandId;
	invocation.isHomeStore = isHomeStore;
	invocation.latitude = latitude;
	invocation.longitude = longitude;
	__block Stores* result;
	[self invokeSynchronous:invocation
	                 server:server
	                   etag:nil
	               complete:^(RESTInvocation*invocation, NSString*invocationServer, id invocationResult, NSError *invocationError, ServerError *invocationServerError, NSString *invocationEtag) {
	                   result = invocationResult;
	                   if (error)
	                   {
	                       *error = invocationError;
	                   }
	                   if (serverError)
	                   {
	                       *serverError = invocationServerError;
	                   }
	               }];
	return result;
}

-(Credentials*)loginAssociateWithCredentials:(Credentials*)credentials signOffExistingOperator:(BOOL)signOffExistingOperator error:(NSError **)error serverError:(ServerError **)serverError
{
	LoginAssociateInvocation *invocation = [[LoginAssociateInvocation alloc] init];
	invocation.credentials = credentials;
	invocation.signOffExistingOperator = signOffExistingOperator;
	__block Credentials* result;
	[self invokeSynchronous:invocation
	                 server:nil
	                   etag:nil
	               complete:^(RESTInvocation*invocation, NSString*invocationServer, id invocationResult, NSError *invocationError, ServerError *invocationServerError, NSString *invocationEtag) {
	                   result = invocationResult;
	                   if (error)
	                   {
	                       *error = invocationError;
	                   }
	                   if (serverError)
	                   {
	                       *serverError = invocationServerError;
	                   }
	               }];
	return result;
}

-(void)logMobileClientCrashEventWithCrashText:(NSString*)crashText crashTime:(NSDate*)crashTime storeID:(NSString*)storeID version:(NSString*)version workStationId:(NSString*)workStationId error:(NSError **)error serverError:(ServerError **)serverError
{
	LogMobileClientCrashEventInvocation *invocation = [[LogMobileClientCrashEventInvocation alloc] init];
	invocation.crashText = crashText;
	invocation.crashTime = crashTime;
	invocation.storeID = storeID;
	invocation.version = version;
	invocation.workStationId = workStationId;
	[self invokeSynchronous:invocation
	                 server:nil
	                   etag:nil
	               complete:^(RESTInvocation*invocation, NSString*invocationServer, id invocationResult, NSError *invocationError, ServerError *invocationServerError, NSString *invocationEtag) {
	                   if (error)
	                   {
	                       *error = invocationError;
	                   }
	                   if (serverError)
	                   {
	                       *serverError = invocationServerError;
	                   }
	               }];
}

-(Credentials*)logoutAssociateWithCredentials:(Credentials*)credentials error:(NSError **)error serverError:(ServerError **)serverError
{
	LogoutAssociateInvocation *invocation = [[LogoutAssociateInvocation alloc] init];
	invocation.credentials = credentials;
	__block Credentials* result;
	[self invokeSynchronous:invocation
	                 server:nil
	                   etag:nil
	               complete:^(RESTInvocation*invocation, NSString*invocationServer, id invocationResult, NSError *invocationError, ServerError *invocationServerError, NSString *invocationEtag) {
	                   result = invocationResult;
	                   if (error)
	                   {
	                       *error = invocationError;
	                   }
	                   if (serverError)
	                   {
	                       *serverError = invocationServerError;
	                   }
	               }];
	return result;
}

-(BasketAssociateDTO*)lookupSaleWithBarcode:(NSString*)barcode originalBasketId:(NSString*)originalBasketId originalStoreId:(NSString*)originalStoreId originalTillNumber:(NSString*)originalTillNumber originalTransactionDate:(NSString*)originalTransactionDate error:(NSError **)error serverError:(ServerError **)serverError
{
	LookupSaleInvocation *invocation = [[LookupSaleInvocation alloc] init];
	invocation.barcode = barcode;
	invocation.originalBasketId = originalBasketId;
	invocation.originalStoreId = originalStoreId;
	invocation.originalTillNumber = originalTillNumber;
	invocation.originalTransactionDate = originalTransactionDate;
	__block BasketAssociateDTO* result;
	[self invokeSynchronous:invocation
	                 server:nil
	                   etag:nil
	               complete:^(RESTInvocation*invocation, NSString*invocationServer, id invocationResult, NSError *invocationError, ServerError *invocationServerError, NSString *invocationEtag) {
	                   result = invocationResult;
	                   if (error)
	                   {
	                       *error = invocationError;
	                   }
	                   if (serverError)
	                   {
	                       *serverError = invocationServerError;
	                   }
	               }];
	return result;
}

-(BasketAssociateDTO*)noReceiptWithBasketId:(NSString*)basketId itemScanId:(NSString*)itemScanId itemSerialNumber:(NSString*)itemSerialNumber originalItemDescription:(NSString*)originalItemDescription originalItemScanId:(NSString*)originalItemScanId price:(NSString*)price reasonDescription:(NSString*)reasonDescription reasonId:(NSString*)reasonId error:(NSError **)error serverError:(ServerError **)serverError
{
	NoReceiptInvocation *invocation = [[NoReceiptInvocation alloc] init];
	invocation.basketId = basketId;
	invocation.itemScanId = itemScanId;
	invocation.itemSerialNumber = itemSerialNumber;
	invocation.originalItemDescription = originalItemDescription;
	invocation.originalItemScanId = originalItemScanId;
	invocation.price = price;
	invocation.reasonDescription = reasonDescription;
	invocation.reasonId = reasonId;
	__block BasketAssociateDTO* result;
	[self invokeSynchronous:invocation
	                 server:nil
	                   etag:nil
	               complete:^(RESTInvocation*invocation, NSString*invocationServer, id invocationResult, NSError *invocationError, ServerError *invocationServerError, NSString *invocationEtag) {
	                   result = invocationResult;
	                   if (error)
	                   {
	                       *error = invocationError;
	                   }
	                   if (serverError)
	                   {
	                       *serverError = invocationServerError;
	                   }
	               }];
	return result;
}

-(NSArray*)notesForCustomerWithCustomerId:(NSString*)customerId error:(NSError **)error serverError:(ServerError **)serverError
{
	NotesForCustomerInvocation *invocation = [[NotesForCustomerInvocation alloc] init];
	invocation.customerId = customerId;
	__block NSArray* result;
	[self invokeSynchronous:invocation
	                 server:nil
	                   etag:nil
	               complete:^(RESTInvocation*invocation, NSString*invocationServer, id invocationResult, NSError *invocationError, ServerError *invocationServerError, NSString *invocationEtag) {
	                   result = invocationResult;
	                   if (error)
	                   {
	                       *error = invocationError;
	                   }
	                   if (serverError)
	                   {
	                       *serverError = invocationServerError;
	                   }
	               }];
	return result;
}

-(void)openCashDrawerWithDeviceStationId:(NSString*)deviceStationId error:(NSError **)error serverError:(ServerError **)serverError
{
	OpenCashDrawerInvocation *invocation = [[OpenCashDrawerInvocation alloc] init];
	invocation.deviceStationId = deviceStationId;
	[self invokeSynchronous:invocation
	                 server:nil
	                   etag:nil
	               complete:^(RESTInvocation*invocation, NSString*invocationServer, id invocationResult, NSError *invocationError, ServerError *invocationServerError, NSString *invocationEtag) {
	                   if (error)
	                   {
	                       *error = invocationError;
	                   }
	                   if (serverError)
	                   {
	                       *serverError = invocationServerError;
	                   }
	               }];
}

-(Customer*)registerCustomerWithCustomer:(Customer*)customer error:(NSError **)error serverError:(ServerError **)serverError
{
	RegisterCustomerInvocation *invocation = [[RegisterCustomerInvocation alloc] init];
	invocation.customer = customer;
	__block Customer* result;
	[self invokeSynchronous:invocation
	                 server:nil
	                   etag:nil
	               complete:^(RESTInvocation*invocation, NSString*invocationServer, id invocationResult, NSError *invocationError, ServerError *invocationServerError, NSString *invocationEtag) {
	                   result = invocationResult;
	                   if (error)
	                   {
	                       *error = invocationError;
	                   }
	                   if (serverError)
	                   {
	                       *serverError = invocationServerError;
	                   }
	               }];
	return result;
}

-(BasketAssociateDTO*)removeBasketDiscountWithBasketDiscountID:(NSArray*)basketDiscountID basketID:(NSString*)basketID error:(NSError **)error serverError:(ServerError **)serverError
{
	RemoveBasketDiscountInvocation *invocation = [[RemoveBasketDiscountInvocation alloc] init];
	invocation.basketDiscountID = basketDiscountID;
	invocation.basketID = basketID;
	__block BasketAssociateDTO* result;
	[self invokeSynchronous:invocation
	                 server:nil
	                   etag:nil
	               complete:^(RESTInvocation*invocation, NSString*invocationServer, id invocationResult, NSError *invocationError, ServerError *invocationServerError, NSString *invocationEtag) {
	                   result = invocationResult;
	                   if (error)
	                   {
	                       *error = invocationError;
	                   }
	                   if (serverError)
	                   {
	                       *serverError = invocationServerError;
	                   }
	               }];
	return result;
}

-(BasketAssociateDTO*)removeBasketItemWithBasketID:(NSString*)basketID basketItemID:(NSArray*)basketItemID deviceStationId:(NSString*)deviceStationId error:(NSError **)error serverError:(ServerError **)serverError
{
	RemoveBasketItemInvocation *invocation = [[RemoveBasketItemInvocation alloc] init];
	invocation.basketID = basketID;
	invocation.basketItemID = basketItemID;
	invocation.deviceStationId = deviceStationId;
	__block BasketAssociateDTO* result;
	[self invokeSynchronous:invocation
	                 server:nil
	                   etag:nil
	               complete:^(RESTInvocation*invocation, NSString*invocationServer, id invocationResult, NSError *invocationError, ServerError *invocationServerError, NSString *invocationEtag) {
	                   result = invocationResult;
	                   if (error)
	                   {
	                       *error = invocationError;
	                   }
	                   if (serverError)
	                   {
	                       *serverError = invocationServerError;
	                   }
	               }];
	return result;
}

-(BasketAssociateDTO*)removeBasketItemDiscountWithBasketID:(NSString*)basketID basketItemDiscountID:(NSArray*)basketItemDiscountID basketItemID:(NSString*)basketItemID error:(NSError **)error serverError:(ServerError **)serverError
{
	RemoveBasketItemDiscountInvocation *invocation = [[RemoveBasketItemDiscountInvocation alloc] init];
	invocation.basketID = basketID;
	invocation.basketItemDiscountID = basketItemDiscountID;
	invocation.basketItemID = basketItemID;
	__block BasketAssociateDTO* result;
	[self invokeSynchronous:invocation
	                 server:nil
	                   etag:nil
	               complete:^(RESTInvocation*invocation, NSString*invocationServer, id invocationResult, NSError *invocationError, ServerError *invocationServerError, NSString *invocationEtag) {
	                   result = invocationResult;
	                   if (error)
	                   {
	                       *error = invocationError;
	                   }
	                   if (serverError)
	                   {
	                       *serverError = invocationServerError;
	                   }
	               }];
	return result;
}

-(BasketAssociateDTO*)reprintLastTxWithDeviceStationId:(NSString*)deviceStationId error:(NSError **)error serverError:(ServerError **)serverError
{
	ReprintLastTxInvocation *invocation = [[ReprintLastTxInvocation alloc] init];
	invocation.deviceStationId = deviceStationId;
	__block BasketAssociateDTO* result;
	[self invokeSynchronous:invocation
	                 server:nil
	                   etag:nil
	               complete:^(RESTInvocation*invocation, NSString*invocationServer, id invocationResult, NSError *invocationError, ServerError *invocationServerError, NSString *invocationEtag) {
	                   result = invocationResult;
	                   if (error)
	                   {
	                       *error = invocationError;
	                   }
	                   if (serverError)
	                   {
	                       *serverError = invocationServerError;
	                   }
	               }];
	return result;
}

-(BasketAssociateDTO*)resumeBasketWithBasketBarcode:(NSString*)basketBarcode basketID:(NSString*)basketID deviceID:(NSString*)deviceID tillNumber:(NSString*)tillNumber error:(NSError **)error serverError:(ServerError **)serverError
{
	ResumeBasketInvocation *invocation = [[ResumeBasketInvocation alloc] init];
	invocation.basketBarcode = basketBarcode;
	invocation.basketID = basketID;
	invocation.deviceID = deviceID;
	invocation.tillNumber = tillNumber;
	__block BasketAssociateDTO* result;
	[self invokeSynchronous:invocation
	                 server:nil
	                   etag:nil
	               complete:^(RESTInvocation*invocation, NSString*invocationServer, id invocationResult, NSError *invocationError, ServerError *invocationServerError, NSString *invocationEtag) {
	                   result = invocationResult;
	                   if (error)
	                   {
	                       *error = invocationError;
	                   }
	                   if (serverError)
	                   {
	                       *serverError = invocationServerError;
	                   }
	               }];
	return result;
}

-(BasketAssociateDTO*)returnBasketItemWithBasketID:(NSString*)basketID itemScanID:(NSString*)itemScanID itemSerialNumber:(NSString*)itemSerialNumber originalItemDescription:(NSString*)originalItemDescription originalItemScanId:(NSString*)originalItemScanId originalTransactionBarcode:(NSString*)originalTransactionBarcode originalTransactionDate:(NSDate*)originalTransactionDate originalTransactionId:(NSNumber*)originalTransactionId originalTransactionStoreId:(NSNumber*)originalTransactionStoreId originalTransactionTill:(NSNumber*)originalTransactionTill price:(NSString*)price reasonId:(NSString*)reasonId error:(NSError **)error serverError:(ServerError **)serverError
{
	ReturnBasketItemInvocation *invocation = [[ReturnBasketItemInvocation alloc] init];
	invocation.basketID = basketID;
	invocation.itemScanID = itemScanID;
	invocation.itemSerialNumber = itemSerialNumber;
	invocation.originalItemDescription = originalItemDescription;
	invocation.originalItemScanId = originalItemScanId;
	invocation.originalTransactionBarcode = originalTransactionBarcode;
	invocation.originalTransactionDate = originalTransactionDate;
	invocation.originalTransactionId = originalTransactionId;
	invocation.originalTransactionStoreId = originalTransactionStoreId;
	invocation.originalTransactionTill = originalTransactionTill;
	invocation.price = price;
	invocation.reasonId = reasonId;
	__block BasketAssociateDTO* result;
	[self invokeSynchronous:invocation
	                 server:nil
	                   etag:nil
	               complete:^(RESTInvocation*invocation, NSString*invocationServer, id invocationResult, NSError *invocationError, ServerError *invocationServerError, NSString *invocationEtag) {
	                   result = invocationResult;
	                   if (error)
	                   {
	                       *error = invocationError;
	                   }
	                   if (serverError)
	                   {
	                       *serverError = invocationServerError;
	                   }
	               }];
	return result;
}

-(BasketAssociateDTO*)returnWithBasketId:(NSString*)basketId itemScanId:(NSString*)itemScanId itemSerialNumber:(NSString*)itemSerialNumber lineId:(NSString*)lineId originalCustomerCard:(NSString*)originalCustomerCard originalItemDescription:(NSString*)originalItemDescription originalItemScanId:(NSString*)originalItemScanId originalReferenceId:(NSString*)originalReferenceId originalSalesPerson:(NSString*)originalSalesPerson originalTransactionDate:(NSString*)originalTransactionDate originalTransactionId:(NSString*)originalTransactionId originalTransactionStoreId:(NSString*)originalTransactionStoreId originalTransactionTill:(NSString*)originalTransactionTill price:(NSString*)price reasonDescription:(NSString*)reasonDescription reasonId:(NSString*)reasonId type:(NSString*)type error:(NSError **)error serverError:(ServerError **)serverError
{
	ReturnWithTypeInvocation *invocation = [[ReturnWithTypeInvocation alloc] init];
	invocation.basketId = basketId;
	invocation.itemScanId = itemScanId;
	invocation.itemSerialNumber = itemSerialNumber;
	invocation.lineId = lineId;
	invocation.originalCustomerCard = originalCustomerCard;
	invocation.originalItemDescription = originalItemDescription;
	invocation.originalItemScanId = originalItemScanId;
	invocation.originalReferenceId = originalReferenceId;
	invocation.originalSalesPerson = originalSalesPerson;
	invocation.originalTransactionDate = originalTransactionDate;
	invocation.originalTransactionId = originalTransactionId;
	invocation.originalTransactionStoreId = originalTransactionStoreId;
	invocation.originalTransactionTill = originalTransactionTill;
	invocation.price = price;
	invocation.reasonDescription = reasonDescription;
	invocation.reasonId = reasonId;
	invocation.type = type;
	__block BasketAssociateDTO* result;
	[self invokeSynchronous:invocation
	                 server:nil
	                   etag:nil
	               complete:^(RESTInvocation*invocation, NSString*invocationServer, id invocationResult, NSError *invocationError, ServerError *invocationServerError, NSString *invocationEtag) {
	                   result = invocationResult;
	                   if (error)
	                   {
	                       *error = invocationError;
	                   }
	                   if (serverError)
	                   {
	                       *serverError = invocationServerError;
	                   }
	               }];
	return result;
}

-(NSArray*)searchItemsWithDepartment:(NSString*)department itemDescription:(NSString*)itemDescription pageNumber:(NSInteger)pageNumber storeID:(NSString*)storeID error:(NSError **)error serverError:(ServerError **)serverError
{
	SearchItemsInvocation *invocation = [[SearchItemsInvocation alloc] init];
	invocation.department = department;
	invocation.itemDescription = itemDescription;
	invocation.pageNumber = pageNumber;
	invocation.storeID = storeID;
	__block NSArray* result;
	[self invokeSynchronous:invocation
	                 server:nil
	                   etag:nil
	               complete:^(RESTInvocation*invocation, NSString*invocationServer, id invocationResult, NSError *invocationError, ServerError *invocationServerError, NSString *invocationEtag) {
	                   result = invocationResult;
	                   if (error)
	                   {
	                       *error = invocationError;
	                   }
	                   if (serverError)
	                   {
	                       *serverError = invocationServerError;
	                   }
	               }];
	return result;
}

-(NSArray*)setNotesForCustomerWithCustomerId:(NSString*)customerId customerNotes:(NSArray*)customerNotes error:(NSError **)error serverError:(ServerError **)serverError
{
	SetNotesForCustomerInvocation *invocation = [[SetNotesForCustomerInvocation alloc] init];
	invocation.customerId = customerId;
	invocation.customerNotes = customerNotes;
	__block NSArray* result;
	[self invokeSynchronous:invocation
	                 server:nil
	                   etag:nil
	               complete:^(RESTInvocation*invocation, NSString*invocationServer, id invocationResult, NSError *invocationError, ServerError *invocationServerError, NSString *invocationEtag) {
	                   result = invocationResult;
	                   if (error)
	                   {
	                       *error = invocationError;
	                   }
	                   if (serverError)
	                   {
	                       *serverError = invocationServerError;
	                   }
	               }];
	return result;
}

-(BasketAssociateDTO*)suspendBasketWithBasketDescription:(NSString*)basketDescription basketID:(NSString*)basketID error:(NSError **)error serverError:(ServerError **)serverError
{
	SuspendBasketInvocation *invocation = [[SuspendBasketInvocation alloc] init];
	invocation.basketDescription = basketDescription;
	invocation.basketID = basketID;
	__block BasketAssociateDTO* result;
	[self invokeSynchronous:invocation
	                 server:nil
	                   etag:nil
	               complete:^(RESTInvocation*invocation, NSString*invocationServer, id invocationResult, NSError *invocationError, ServerError *invocationServerError, NSString *invocationEtag) {
	                   result = invocationResult;
	                   if (error)
	                   {
	                       *error = invocationError;
	                   }
	                   if (serverError)
	                   {
	                       *serverError = invocationServerError;
	                   }
	               }];
	return result;
}

-(BOOL)toggleTrainingModeError:(NSError **)error serverError:(ServerError **)serverError
{
	ToggleTrainingModeInvocation *invocation = [[ToggleTrainingModeInvocation alloc] init];
	__block NSNumber* result;
	[self invokeSynchronous:invocation
	                 server:nil
	                   etag:nil
	               complete:^(RESTInvocation*invocation, NSString*invocationServer, id invocationResult, NSError *invocationError, ServerError *invocationServerError, NSString *invocationEtag) {
	                   result = invocationResult;
	                   if (error)
	                   {
	                       *error = invocationError;
	                   }
	                   if (serverError)
	                   {
	                       *serverError = invocationServerError;
	                   }
	               }];
	return [result boolValue];
}

-(DeviceStation*)unlinkDeviceWithDeviceStationId:(NSString*)deviceStationId error:(NSError **)error serverError:(ServerError **)serverError
{
	UnlinkDeviceInvocation *invocation = [[UnlinkDeviceInvocation alloc] init];
	invocation.deviceStationId = deviceStationId;
	__block DeviceStation* result;
	[self invokeSynchronous:invocation
	                 server:nil
	                   etag:nil
	               complete:^(RESTInvocation*invocation, NSString*invocationServer, id invocationResult, NSError *invocationError, ServerError *invocationServerError, NSString *invocationEtag) {
	                   result = invocationResult;
	                   if (error)
	                   {
	                       *error = invocationError;
	                   }
	                   if (serverError)
	                   {
	                       *serverError = invocationServerError;
	                   }
	               }];
	return result;
}

-(BasketAssociateDTO*)unlinkedReturnWithBasketId:(NSString*)basketId itemScanId:(NSString*)itemScanId itemSerialNumber:(NSString*)itemSerialNumber originalCustomerCard:(NSString*)originalCustomerCard originalItemDescription:(NSString*)originalItemDescription originalItemScanId:(NSString*)originalItemScanId originalSalesPerson:(NSString*)originalSalesPerson originalTransactionDate:(NSString*)originalTransactionDate originalTransactionId:(NSString*)originalTransactionId originalTransactionStoreId:(NSString*)originalTransactionStoreId originalTransactionTill:(NSString*)originalTransactionTill price:(NSString*)price reasonDescription:(NSString*)reasonDescription reasonId:(NSString*)reasonId error:(NSError **)error serverError:(ServerError **)serverError
{
	UnlinkedReturnInvocation *invocation = [[UnlinkedReturnInvocation alloc] init];
	invocation.basketId = basketId;
	invocation.itemScanId = itemScanId;
	invocation.itemSerialNumber = itemSerialNumber;
	invocation.originalCustomerCard = originalCustomerCard;
	invocation.originalItemDescription = originalItemDescription;
	invocation.originalItemScanId = originalItemScanId;
	invocation.originalSalesPerson = originalSalesPerson;
	invocation.originalTransactionDate = originalTransactionDate;
	invocation.originalTransactionId = originalTransactionId;
	invocation.originalTransactionStoreId = originalTransactionStoreId;
	invocation.originalTransactionTill = originalTransactionTill;
	invocation.price = price;
	invocation.reasonDescription = reasonDescription;
	invocation.reasonId = reasonId;
	__block BasketAssociateDTO* result;
	[self invokeSynchronous:invocation
	                 server:nil
	                   etag:nil
	               complete:^(RESTInvocation*invocation, NSString*invocationServer, id invocationResult, NSError *invocationError, ServerError *invocationServerError, NSString *invocationEtag) {
	                   result = invocationResult;
	                   if (error)
	                   {
	                       *error = invocationError;
	                   }
	                   if (serverError)
	                   {
	                       *serverError = invocationServerError;
	                   }
	               }];
	return result;
}

-(void)updateCustomerWithCustomer:(Customer*)customer customerId:(NSString*)customerId error:(NSError **)error serverError:(ServerError **)serverError
{
	UpdateCustomerInvocation *invocation = [[UpdateCustomerInvocation alloc] init];
	invocation.customer = customer;
	invocation.customerId = customerId;
	[self invokeSynchronous:invocation
	                 server:nil
	                   etag:nil
	               complete:^(RESTInvocation*invocation, NSString*invocationServer, id invocationResult, NSError *invocationError, ServerError *invocationServerError, NSString *invocationEtag) {
	                   if (error)
	                   {
	                       *error = invocationError;
	                   }
	                   if (serverError)
	                   {
	                       *serverError = invocationServerError;
	                   }
	               }];
}

-(void)updateDeviceWithDevice:(DeviceProfile*)device server:(NSString *)server error:(NSError **)error serverError:(ServerError **)serverError
{
	UpdateDeviceInvocation *invocation = [[UpdateDeviceInvocation alloc] init];
	invocation.device = device;
	[self invokeSynchronous:invocation
	                 server:server
	                   etag:nil
	               complete:^(RESTInvocation*invocation, NSString*invocationServer, id invocationResult, NSError *invocationError, ServerError *invocationServerError, NSString *invocationEtag) {
	                   if (error)
	                   {
	                       *error = invocationError;
	                   }
	                   if (serverError)
	                   {
	                       *serverError = invocationServerError;
	                   }
	               }];
}

-(DeviceStation*)updateDeviceStationWithDeviceStation:(DeviceStation*)deviceStation deviceStationId:(NSString*)deviceStationId error:(NSError **)error serverError:(ServerError **)serverError
{
	UpdateDeviceStationInvocation *invocation = [[UpdateDeviceStationInvocation alloc] init];
	invocation.deviceStation = deviceStation;
	invocation.deviceStationId = deviceStationId;
	__block DeviceStation* result;
	[self invokeSynchronous:invocation
	                 server:nil
	                   etag:nil
	               complete:^(RESTInvocation*invocation, NSString*invocationServer, id invocationResult, NSError *invocationError, ServerError *invocationServerError, NSString *invocationEtag) {
	                   result = invocationResult;
	                   if (error)
	                   {
	                       *error = invocationError;
	                   }
	                   if (serverError)
	                   {
	                       *serverError = invocationServerError;
	                   }
	               }];
	return result;
}

-(BasketAssociateDTO*)updateGiftReceiptWithBasket:(BasketAssociateDTO*)basket error:(NSError **)error serverError:(ServerError **)serverError
{
	UpdateGiftReceiptInvocation *invocation = [[UpdateGiftReceiptInvocation alloc] init];
	invocation.basket = basket;
	__block BasketAssociateDTO* result;
	[self invokeSynchronous:invocation
	                 server:nil
	                   etag:nil
	               complete:^(RESTInvocation*invocation, NSString*invocationServer, id invocationResult, NSError *invocationError, ServerError *invocationServerError, NSString *invocationEtag) {
	                   result = invocationResult;
	                   if (error)
	                   {
	                       *error = invocationError;
	                   }
	                   if (serverError)
	                   {
	                       *serverError = invocationServerError;
	                   }
	               }];
	return result;
}

-(void)addItemWithAgeRestriction:(NSString*)ageRestriction barcodeType:(NSString*)barcodeType basketID:(NSString*)basketID entryMethod:(NSString*)entryMethod itemDescription:(NSArray*)itemDescription itemScanID:(NSString*)itemScanID itemSerialNumber:(NSString*)itemSerialNumber measure:(NSString*)measure originalItemDescription:(NSString*)originalItemDescription originalItemScanId:(NSString*)originalItemScanId price:(NSString*)price quantity:(NSString*)quantity track1:(NSString*)track1 track2:(NSString*)track2 track3:(NSString*)track3 complete:(void (^)(BasketAssociateDTO* result, NSError* error, ServerError* serverError))complete
{
	AddItemInvocation *invocation = [[AddItemInvocation alloc] init];
	invocation.ageRestriction = ageRestriction;
	invocation.barcodeType = barcodeType;
	invocation.basketID = basketID;
	invocation.entryMethod = entryMethod;
	invocation.itemDescription = itemDescription;
	invocation.itemScanID = itemScanID;
	invocation.itemSerialNumber = itemSerialNumber;
	invocation.measure = measure;
	invocation.originalItemDescription = originalItemDescription;
	invocation.originalItemScanId = originalItemScanId;
	invocation.price = price;
	invocation.quantity = quantity;
	invocation.track1 = track1;
	invocation.track2 = track2;
	invocation.track3 = track3;
	invocation.complete = 
	^(RESTInvocation*invocation, NSString*invocationServer, id invocationResult, NSError *invocationError, ServerError *invocationServerError, NSString *invocationEtag)
	{
		if (complete)
		{
			complete(invocationResult, invocationError, invocationServerError);
		}
	};
	[self invokeSynchronous:invocation
	                 server:nil
	                   etag:nil
	               complete:invocation.complete];
}

-(void)addItemsWithBasketID:(NSString*)basketID entryMethod:(NSString*)entryMethod itemDescription:(NSArray*)itemDescription itemScanID:(NSArray*)itemScanID complete:(void (^)(BasketAssociateDTO* result, NSError* error, ServerError* serverError))complete
{
	AddItemsInvocation *invocation = [[AddItemsInvocation alloc] init];
	invocation.basketID = basketID;
	invocation.entryMethod = entryMethod;
	invocation.itemDescription = itemDescription;
	invocation.itemScanID = itemScanID;
	invocation.complete = 
	^(RESTInvocation*invocation, NSString*invocationServer, id invocationResult, NSError *invocationError, ServerError *invocationServerError, NSString *invocationEtag)
	{
		if (complete)
		{
			complete(invocationResult, invocationError, invocationServerError);
		}
	};
	[self invokeSynchronous:invocation
	                 server:nil
	                   etag:nil
	               complete:invocation.complete];
}

-(void)adjustBasketItemWithBasketID:(NSString*)basketID basketItemID:(NSArray*)basketItemID operatorID:(NSString*)operatorID complete:(void (^)(BasketAssociateDTO* result, NSError* error, ServerError* serverError))complete
{
	AdjustBasketItemInvocation *invocation = [[AdjustBasketItemInvocation alloc] init];
	invocation.basketID = basketID;
	invocation.basketItemID = basketItemID;
	invocation.operatorID = operatorID;
	invocation.complete = 
	^(RESTInvocation*invocation, NSString*invocationServer, id invocationResult, NSError *invocationError, ServerError *invocationServerError, NSString *invocationEtag)
	{
		if (complete)
		{
			complete(invocationResult, invocationError, invocationServerError);
		}
	};
	[self invokeSynchronous:invocation
	                 server:nil
	                   etag:nil
	               complete:invocation.complete];
}

-(void)adjustBasketItemPriceWithBasketID:(NSString*)basketID basketItemID:(NSString*)basketItemID price:(NSString*)price reasonDescription:(NSString*)reasonDescription reasonID:(NSString*)reasonID complete:(void (^)(BasketAssociateDTO* result, NSError* error, ServerError* serverError))complete
{
	AdjustBasketItemPriceInvocation *invocation = [[AdjustBasketItemPriceInvocation alloc] init];
	invocation.basketID = basketID;
	invocation.basketItemID = basketItemID;
	invocation.price = price;
	invocation.reasonDescription = reasonDescription;
	invocation.reasonID = reasonID;
	invocation.complete = 
	^(RESTInvocation*invocation, NSString*invocationServer, id invocationResult, NSError *invocationError, ServerError *invocationServerError, NSString *invocationEtag)
	{
		if (complete)
		{
			complete(invocationResult, invocationError, invocationServerError);
		}
	};
	[self invokeSynchronous:invocation
	                 server:nil
	                   etag:nil
	               complete:invocation.complete];
}

-(void)adjustBasketItemQuantityWithBasketID:(NSString*)basketID basketItemID:(NSString*)basketItemID quantity:(NSInteger)quantity complete:(void (^)(BasketAssociateDTO* result, NSError* error, ServerError* serverError))complete
{
	AdjustBasketItemQuantityInvocation *invocation = [[AdjustBasketItemQuantityInvocation alloc] init];
	invocation.basketID = basketID;
	invocation.basketItemID = basketItemID;
	invocation.quantity = quantity;
	invocation.complete = 
	^(RESTInvocation*invocation, NSString*invocationServer, id invocationResult, NSError *invocationError, ServerError *invocationServerError, NSString *invocationEtag)
	{
		if (complete)
		{
			complete(invocationResult, invocationError, invocationServerError);
		}
	};
	[self invokeSynchronous:invocation
	                 server:nil
	                   etag:nil
	               complete:invocation.complete];
}

-(void)adjustSaleWithBasket:(BasketDTO*)basket complete:(void (^)(BasketDTO* result, NSError* error, ServerError* serverError))complete
{
	AdjustSaleInvocation *invocation = [[AdjustSaleInvocation alloc] init];
	invocation.basket = basket;
	invocation.complete = 
	^(RESTInvocation*invocation, NSString*invocationServer, id invocationResult, NSError *invocationError, ServerError *invocationServerError, NSString *invocationEtag)
	{
		if (complete)
		{
			complete(invocationResult, invocationError, invocationServerError);
		}
	};
	[self invokeSynchronous:invocation
	                 server:nil
	                   etag:nil
	               complete:invocation.complete];
}

-(void)adjustTaxFreeFormWithBasket:(BasketTaxFreeDocumentDTO*)basket complete:(void (^)(BasketTaxFreeDocumentDTO* result, NSError* error, ServerError* serverError))complete
{
	AdjustTaxFreeFormInvocation *invocation = [[AdjustTaxFreeFormInvocation alloc] init];
	invocation.basket = basket;
	invocation.complete = 
	^(RESTInvocation*invocation, NSString*invocationServer, id invocationResult, NSError *invocationError, ServerError *invocationServerError, NSString *invocationEtag)
	{
		if (complete)
		{
			complete(invocationResult, invocationError, invocationServerError);
		}
	};
	[self invokeSynchronous:invocation
	                 server:nil
	                   etag:nil
	               complete:invocation.complete];
}

-(void)applyBasketDiscountWithAmount:(NSString*)amount basketID:(NSString*)basketID discountCode:(NSString*)discountCode discountType:(NSString*)discountType reasonDescription:(NSString*)reasonDescription reasonID:(NSString*)reasonID complete:(void (^)(BasketAssociateDTO* result, NSError* error, ServerError* serverError))complete
{
	ApplyBasketDiscountInvocation *invocation = [[ApplyBasketDiscountInvocation alloc] init];
	invocation.amount = amount;
	invocation.basketID = basketID;
	invocation.discountCode = discountCode;
	invocation.discountType = discountType;
	invocation.reasonDescription = reasonDescription;
	invocation.reasonID = reasonID;
	invocation.complete = 
	^(RESTInvocation*invocation, NSString*invocationServer, id invocationResult, NSError *invocationError, ServerError *invocationServerError, NSString *invocationEtag)
	{
		if (complete)
		{
			complete(invocationResult, invocationError, invocationServerError);
		}
	};
	[self invokeSynchronous:invocation
	                 server:nil
	                   etag:nil
	               complete:invocation.complete];
}

-(void)applyBasketItemDiscountWithAmount:(NSString*)amount basketID:(NSString*)basketID basketItemID:(NSArray*)basketItemID discountCode:(NSString*)discountCode discountType:(NSString*)discountType reasonDescription:(NSString*)reasonDescription reasonID:(NSString*)reasonID complete:(void (^)(BasketAssociateDTO* result, NSError* error, ServerError* serverError))complete
{
	ApplyBasketItemDiscountInvocation *invocation = [[ApplyBasketItemDiscountInvocation alloc] init];
	invocation.amount = amount;
	invocation.basketID = basketID;
	invocation.basketItemID = basketItemID;
	invocation.discountCode = discountCode;
	invocation.discountType = discountType;
	invocation.reasonDescription = reasonDescription;
	invocation.reasonID = reasonID;
	invocation.complete = 
	^(RESTInvocation*invocation, NSString*invocationServer, id invocationResult, NSError *invocationError, ServerError *invocationServerError, NSString *invocationEtag)
	{
		if (complete)
		{
			complete(invocationResult, invocationError, invocationServerError);
		}
	};
	[self invokeSynchronous:invocation
	                 server:nil
	                   etag:nil
	               complete:invocation.complete];
}

-(void)assignCustomerWithAffiliationType:(NSString*)affiliationType basketID:(NSString*)basketID customer:(NSString*)customer complete:(void (^)(BasketAssociateDTO* result, NSError* error, ServerError* serverError))complete
{
	AssignCustomerInvocation *invocation = [[AssignCustomerInvocation alloc] init];
	invocation.affiliationType = affiliationType;
	invocation.basketID = basketID;
	invocation.customer = customer;
	invocation.complete = 
	^(RESTInvocation*invocation, NSString*invocationServer, id invocationResult, NSError *invocationError, ServerError *invocationServerError, NSString *invocationEtag)
	{
		if (complete)
		{
			complete(invocationResult, invocationError, invocationServerError);
		}
	};
	[self invokeSynchronous:invocation
	                 server:nil
	                   etag:nil
	               complete:invocation.complete];
}

-(void)assignEmailAddressWithBasketID:(NSString*)basketID emailAddress:(NSString*)emailAddress complete:(void (^)(BasketAssociateDTO* result, NSError* error, ServerError* serverError))complete
{
	AssignEmailAddressInvocation *invocation = [[AssignEmailAddressInvocation alloc] init];
	invocation.basketID = basketID;
	invocation.emailAddress = emailAddress;
	invocation.complete = 
	^(RESTInvocation*invocation, NSString*invocationServer, id invocationResult, NSError *invocationError, ServerError *invocationServerError, NSString *invocationEtag)
	{
		if (complete)
		{
			complete(invocationResult, invocationError, invocationServerError);
		}
	};
	[self invokeSynchronous:invocation
	                 server:nil
	                   etag:nil
	               complete:invocation.complete];
}

-(void)associateDeviceStationWithDeviceStationId:(NSString*)deviceStationId tillId:(NSString*)tillId complete:(void (^)(DeviceStation* result, NSError* error, ServerError* serverError))complete
{
	AssociateDeviceStationInvocation *invocation = [[AssociateDeviceStationInvocation alloc] init];
	invocation.deviceStationId = deviceStationId;
	invocation.tillId = tillId;
	invocation.complete = 
	^(RESTInvocation*invocation, NSString*invocationServer, id invocationResult, NSError *invocationError, ServerError *invocationServerError, NSString *invocationEtag)
	{
		if (complete)
		{
			complete(invocationResult, invocationError, invocationServerError);
		}
	};
	[self invokeSynchronous:invocation
	                 server:nil
	                   etag:nil
	               complete:invocation.complete];
}

-(void)associateTextReceiptWithReceiptId:(NSString*)receiptId complete:(void (^)(NSString* result, NSError* error, ServerError* serverError))complete
{
	AssociateTextReceiptInvocation *invocation = [[AssociateTextReceiptInvocation alloc] init];
	invocation.receiptId = receiptId;
	invocation.complete = 
	^(RESTInvocation*invocation, NSString*invocationServer, id invocationResult, NSError *invocationError, ServerError *invocationServerError, NSString *invocationEtag)
	{
		if (complete)
		{
			complete(invocationResult, invocationError, invocationServerError);
		}
	};
	[self invokeSynchronous:invocation
	                 server:nil
	                   etag:nil
	               complete:invocation.complete];
}

-(void)cancelBasketWithBasketID:(NSString*)basketID complete:(void (^)(BasketAssociateDTO* result, NSError* error, ServerError* serverError))complete
{
	CancelBasketInvocation *invocation = [[CancelBasketInvocation alloc] init];
	invocation.basketID = basketID;
	invocation.complete = 
	^(RESTInvocation*invocation, NSString*invocationServer, id invocationResult, NSError *invocationError, ServerError *invocationServerError, NSString *invocationEtag)
	{
		if (complete)
		{
			complete(invocationResult, invocationError, invocationServerError);
		}
	};
	[self invokeSynchronous:invocation
	                 server:nil
	                   etag:nil
	               complete:invocation.complete];
}

-(void)cancelCheckoutBasketWithBasketID:(NSString*)basketID complete:(void (^)(BasketAssociateDTO* result, NSError* error, ServerError* serverError))complete
{
	CancelCheckoutBasketInvocation *invocation = [[CancelCheckoutBasketInvocation alloc] init];
	invocation.basketID = basketID;
	invocation.complete = 
	^(RESTInvocation*invocation, NSString*invocationServer, id invocationResult, NSError *invocationError, ServerError *invocationServerError, NSString *invocationEtag)
	{
		if (complete)
		{
			complete(invocationResult, invocationError, invocationServerError);
		}
	};
	[self invokeSynchronous:invocation
	                 server:nil
	                   etag:nil
	               complete:invocation.complete];
}

-(void)cancelTaxFreeFormWithBasketID:(NSString*)basketID complete:(void (^)(BasketTaxFreeDocumentDTO* result, NSError* error, ServerError* serverError))complete
{
	CancelTaxFreeFormInvocation *invocation = [[CancelTaxFreeFormInvocation alloc] init];
	invocation.basketID = basketID;
	invocation.complete = 
	^(RESTInvocation*invocation, NSString*invocationServer, id invocationResult, NSError *invocationError, ServerError *invocationServerError, NSString *invocationEtag)
	{
		if (complete)
		{
			complete(invocationResult, invocationError, invocationServerError);
		}
	};
	[self invokeSynchronous:invocation
	                 server:nil
	                   etag:nil
	               complete:invocation.complete];
}

-(void)cardsForCustomerWithCustomerId:(NSString*)customerId complete:(void (^)(NSArray* result, NSError* error, ServerError* serverError))complete
{
	CardsForCustomerInvocation *invocation = [[CardsForCustomerInvocation alloc] init];
	invocation.customerId = customerId;
	invocation.complete = 
	^(RESTInvocation*invocation, NSString*invocationServer, id invocationResult, NSError *invocationError, ServerError *invocationServerError, NSString *invocationEtag)
	{
		if (complete)
		{
			complete(invocationResult, invocationError, invocationServerError);
		}
	};
	[self invokeSynchronous:invocation
	                 server:nil
	                   etag:nil
	               complete:invocation.complete];
}

-(void)changeCashDrawerWithDeviceStationId:(NSString*)deviceStationId complete:(void (^)(NSError* error, ServerError* serverError))complete
{
	ChangeCashDrawerInvocation *invocation = [[ChangeCashDrawerInvocation alloc] init];
	invocation.deviceStationId = deviceStationId;
	invocation.complete = 
	^(RESTInvocation*invocation, NSString*invocationServer, id invocationResult, NSError *invocationError, ServerError *invocationServerError, NSString *invocationEtag)
	{
		if (complete)
		{
			complete(invocationError, invocationServerError);
		}
	};
	[self invokeSynchronous:invocation
	                 server:nil
	                   etag:nil
	               complete:invocation.complete];
}

-(void)checkoutBasketWithBasketID:(NSString*)basketID currencyCode:(NSString*)currencyCode complete:(void (^)(BasketAssociateDTO* result, NSError* error, ServerError* serverError))complete
{
	CheckoutBasketInvocation *invocation = [[CheckoutBasketInvocation alloc] init];
	invocation.basketID = basketID;
	invocation.currencyCode = currencyCode;
	invocation.complete = 
	^(RESTInvocation*invocation, NSString*invocationServer, id invocationResult, NSError *invocationError, ServerError *invocationServerError, NSString *invocationEtag)
	{
		if (complete)
		{
			complete(invocationResult, invocationError, invocationServerError);
		}
	};
	[self invokeSynchronous:invocation
	                 server:nil
	                   etag:nil
	               complete:invocation.complete];
}

-(void)controlVoidTransactionWithBasketID:(NSString*)basketID businessDay:(NSDate*)businessDay confirmedAmount:(NSString*)confirmedAmount scanData:(NSString*)scanData storeID:(NSString*)storeID transactionType:(NSString*)transactionType workstationID:(NSString*)workstationID complete:(void (^)(id result, NSError* error, ServerError* serverError))complete
{
	ControlVoidTransactionInvocation *invocation = [[ControlVoidTransactionInvocation alloc] init];
	invocation.basketID = basketID;
	invocation.businessDay = businessDay;
	invocation.confirmedAmount = confirmedAmount;
	invocation.scanData = scanData;
	invocation.storeID = storeID;
	invocation.transactionType = transactionType;
	invocation.workstationID = workstationID;
	invocation.complete = 
	^(RESTInvocation*invocation, NSString*invocationServer, id invocationResult, NSError *invocationError, ServerError *invocationServerError, NSString *invocationEtag)
	{
		if (complete)
		{
			complete(invocationResult, invocationError, invocationServerError);
		}
	};
	[self invokeSynchronous:invocation
	                 server:nil
	                   etag:nil
	               complete:invocation.complete];
}

-(void)createBasketWithBasketID:(NSString*)basketID businessDay:(NSDate*)businessDay deviceID:(NSString*)deviceID issuerCountryId:(NSString*)issuerCountryId lastTransaction:(NSString*)lastTransaction scanData:(NSString*)scanData storeID:(NSString*)storeID transactionType:(NSString*)transactionType workstationID:(NSString*)workstationID complete:(void (^)(id result, NSError* error, ServerError* serverError))complete
{
	CreateBasketInvocation *invocation = [[CreateBasketInvocation alloc] init];
	invocation.basketID = basketID;
	invocation.businessDay = businessDay;
	invocation.deviceID = deviceID;
	invocation.issuerCountryId = issuerCountryId;
	invocation.lastTransaction = lastTransaction;
	invocation.scanData = scanData;
	invocation.storeID = storeID;
	invocation.transactionType = transactionType;
	invocation.workstationID = workstationID;
	invocation.complete = 
	^(RESTInvocation*invocation, NSString*invocationServer, id invocationResult, NSError *invocationError, ServerError *invocationServerError, NSString *invocationEtag)
	{
		if (complete)
		{
			complete(invocationResult, invocationError, invocationServerError);
		}
	};
	[self invokeSynchronous:invocation
	                 server:nil
	                   etag:nil
	               complete:invocation.complete];
}

-(void)createCustomerEmailWithEmailAddress:(NSString*)emailAddress includeGiftReceipt:(BOOL)includeGiftReceipt receiptID:(NSString*)receiptID complete:(void (^)(NSError* error, ServerError* serverError))complete
{
	CreateCustomerEmailInvocation *invocation = [[CreateCustomerEmailInvocation alloc] init];
	invocation.emailAddress = emailAddress;
	invocation.includeGiftReceipt = includeGiftReceipt;
	invocation.receiptID = receiptID;
	invocation.complete = 
	^(RESTInvocation*invocation, NSString*invocationServer, id invocationResult, NSError *invocationError, ServerError *invocationServerError, NSString *invocationEtag)
	{
		if (complete)
		{
			complete(invocationError, invocationServerError);
		}
	};
	[self invokeSynchronous:invocation
	                 server:nil
	                   etag:nil
	               complete:invocation.complete];
}

-(void)createEFTTenderWithDeviceStationId:(NSString*)deviceStationId tender:(EFTPayment*)tender complete:(void (^)(BasketAssociateDTO* result, NSError* error, ServerError* serverError))complete
{
	CreateEFTTenderInvocation *invocation = [[CreateEFTTenderInvocation alloc] init];
	invocation.deviceStationId = deviceStationId;
	invocation.tender = tender;
	invocation.complete = 
	^(RESTInvocation*invocation, NSString*invocationServer, id invocationResult, NSError *invocationError, ServerError *invocationServerError, NSString *invocationEtag)
	{
		if (complete)
		{
			complete(invocationResult, invocationError, invocationServerError);
		}
	};
	[self invokeSynchronous:invocation
	                 server:nil
	                   etag:nil
	               complete:invocation.complete];
}

-(void)createStandardTenderWithTender:(StandardPayment*)tender complete:(void (^)(BasketAssociateDTO* result, NSError* error, ServerError* serverError))complete
{
	CreateStandardTenderInvocation *invocation = [[CreateStandardTenderInvocation alloc] init];
	invocation.tender = tender;
	invocation.complete = 
	^(RESTInvocation*invocation, NSString*invocationServer, id invocationResult, NSError *invocationError, ServerError *invocationServerError, NSString *invocationEtag)
	{
		if (complete)
		{
			complete(invocationResult, invocationError, invocationServerError);
		}
	};
	[self invokeSynchronous:invocation
	                 server:nil
	                   etag:nil
	               complete:invocation.complete];
}

-(void)createVerifoneTenderWithTender:(VerifonePayment*)tender complete:(void (^)(BasketAssociateDTO* result, NSError* error, ServerError* serverError))complete
{
	CreateVerifoneTenderInvocation *invocation = [[CreateVerifoneTenderInvocation alloc] init];
	invocation.tender = tender;
	invocation.complete = 
	^(RESTInvocation*invocation, NSString*invocationServer, id invocationResult, NSError *invocationError, ServerError *invocationServerError, NSString *invocationEtag)
	{
		if (complete)
		{
			complete(invocationResult, invocationError, invocationServerError);
		}
	};
	[self invokeSynchronous:invocation
	                 server:nil
	                   etag:nil
	               complete:invocation.complete];
}

-(void)createYespayTenderWithTender:(YespayPayment*)tender complete:(void (^)(BasketAssociateDTO* result, NSError* error, ServerError* serverError))complete
{
	CreateYespayTenderInvocation *invocation = [[CreateYespayTenderInvocation alloc] init];
	invocation.tender = tender;
	invocation.complete = 
	^(RESTInvocation*invocation, NSString*invocationServer, id invocationResult, NSError *invocationError, ServerError *invocationServerError, NSString *invocationEtag)
	{
		if (complete)
		{
			complete(invocationResult, invocationError, invocationServerError);
		}
	};
	[self invokeSynchronous:invocation
	                 server:nil
	                   etag:nil
	               complete:invocation.complete];
}

-(void)customersWithEmail:(NSString*)email complete:(void (^)(NSArray* result, NSError* error, ServerError* serverError))complete
{
	CustomersWithEmailInvocation *invocation = [[CustomersWithEmailInvocation alloc] init];
	invocation.email = email;
	invocation.complete = 
	^(RESTInvocation*invocation, NSString*invocationServer, id invocationResult, NSError *invocationError, ServerError *invocationServerError, NSString *invocationEtag)
	{
		if (complete)
		{
			complete(invocationResult, invocationError, invocationServerError);
		}
	};
	[self invokeSynchronous:invocation
	                 server:nil
	                   etag:nil
	               complete:invocation.complete];
}

-(void)customerWithCardNo:(NSString*)cardNo cardType:(NSString*)cardType complete:(void (^)(Customer* result, NSError* error, ServerError* serverError))complete
{
	CustomerWithCardNoInvocation *invocation = [[CustomerWithCardNoInvocation alloc] init];
	invocation.cardNo = cardNo;
	invocation.cardType = cardType;
	invocation.complete = 
	^(RESTInvocation*invocation, NSString*invocationServer, id invocationResult, NSError *invocationError, ServerError *invocationServerError, NSString *invocationEtag)
	{
		if (complete)
		{
			complete(invocationResult, invocationError, invocationServerError);
		}
	};
	[self invokeSynchronous:invocation
	                 server:nil
	                   etag:nil
	               complete:invocation.complete];
}

-(void)customerWithCustomerId:(NSString*)customerId complete:(void (^)(Customer* result, NSError* error, ServerError* serverError))complete
{
	CustomerWithCustomerIdInvocation *invocation = [[CustomerWithCustomerIdInvocation alloc] init];
	invocation.customerId = customerId;
	invocation.complete = 
	^(RESTInvocation*invocation, NSString*invocationServer, id invocationResult, NSError *invocationError, ServerError *invocationServerError, NSString *invocationEtag)
	{
		if (complete)
		{
			complete(invocationResult, invocationError, invocationServerError);
		}
	};
	[self invokeSynchronous:invocation
	                 server:nil
	                   etag:nil
	               complete:invocation.complete];
}

-(void)disassociateDeviceStationWithDeviceStationId:(NSString*)deviceStationId tillId:(NSString*)tillId complete:(void (^)(DeviceStation* result, NSError* error, ServerError* serverError))complete
{
	DisassociateDeviceStationInvocation *invocation = [[DisassociateDeviceStationInvocation alloc] init];
	invocation.deviceStationId = deviceStationId;
	invocation.tillId = tillId;
	invocation.complete = 
	^(RESTInvocation*invocation, NSString*invocationServer, id invocationResult, NSError *invocationError, ServerError *invocationServerError, NSString *invocationEtag)
	{
		if (complete)
		{
			complete(invocationResult, invocationError, invocationServerError);
		}
	};
	[self invokeSynchronous:invocation
	                 server:nil
	                   etag:nil
	               complete:invocation.complete];
}

-(void)findItemWithScanID:(NSString*)scanID storeID:(NSString*)storeID complete:(void (^)(Item* result, NSError* error, ServerError* serverError))complete
{
	FindItemInvocation *invocation = [[FindItemInvocation alloc] init];
	invocation.scanID = scanID;
	invocation.storeID = storeID;
	invocation.complete = 
	^(RESTInvocation*invocation, NSString*invocationServer, id invocationResult, NSError *invocationError, ServerError *invocationServerError, NSString *invocationEtag)
	{
		if (complete)
		{
			complete(invocationResult, invocationError, invocationServerError);
		}
	};
	[self invokeSynchronous:invocation
	                 server:nil
	                   etag:nil
	               complete:invocation.complete];
}

-(void)findLineIdWithItemScanId:(NSString*)itemScanId originalBasketId:(NSString*)originalBasketId originalStoreId:(NSString*)originalStoreId originalTillNumber:(NSString*)originalTillNumber originalTransactionDate:(NSString*)originalTransactionDate complete:(void (^)(ReturnableSaleItem* result, NSError* error, ServerError* serverError))complete
{
	FindLineIdInvocation *invocation = [[FindLineIdInvocation alloc] init];
	invocation.itemScanId = itemScanId;
	invocation.originalBasketId = originalBasketId;
	invocation.originalStoreId = originalStoreId;
	invocation.originalTillNumber = originalTillNumber;
	invocation.originalTransactionDate = originalTransactionDate;
	invocation.complete = 
	^(RESTInvocation*invocation, NSString*invocationServer, id invocationResult, NSError *invocationError, ServerError *invocationServerError, NSString *invocationEtag)
	{
		if (complete)
		{
			complete(invocationResult, invocationError, invocationServerError);
		}
	};
	[self invokeSynchronous:invocation
	                 server:nil
	                   etag:nil
	               complete:invocation.complete];
}

-(void)finishBasketWithBasketID:(NSString*)basketID deviceStationId:(NSString*)deviceStationId emailAddress:(NSString*)emailAddress emailReceipt:(BOOL)emailReceipt enablePrint:(BOOL)enablePrint printSummaryReceipt:(BOOL)printSummaryReceipt tillNumber:(NSString*)tillNumber complete:(void (^)(BasketAssociateDTO* result, NSError* error, ServerError* serverError))complete
{
	FinishBasketInvocation *invocation = [[FinishBasketInvocation alloc] init];
	invocation.basketID = basketID;
	invocation.deviceStationId = deviceStationId;
	invocation.emailAddress = emailAddress;
	invocation.emailReceipt = emailReceipt;
	invocation.enablePrint = enablePrint;
	invocation.printSummaryReceipt = printSummaryReceipt;
	invocation.tillNumber = tillNumber;
	invocation.complete = 
	^(RESTInvocation*invocation, NSString*invocationServer, id invocationResult, NSError *invocationError, ServerError *invocationServerError, NSString *invocationEtag)
	{
		if (complete)
		{
			complete(invocationResult, invocationError, invocationServerError);
		}
	};
	[self invokeSynchronous:invocation
	                 server:nil
	                   etag:nil
	               complete:invocation.complete];
}

-(void)finishTaxFreeBasketWithBasketID:(NSString*)basketID deviceStationId:(NSString*)deviceStationId emailAddress:(NSString*)emailAddress emailReceipt:(BOOL)emailReceipt enablePrint:(BOOL)enablePrint printSummaryReceipt:(BOOL)printSummaryReceipt tillNumber:(NSString*)tillNumber complete:(void (^)(BasketTaxFreeDocumentDTO* result, NSError* error, ServerError* serverError))complete
{
	FinishTaxFreeBasketInvocation *invocation = [[FinishTaxFreeBasketInvocation alloc] init];
	invocation.basketID = basketID;
	invocation.deviceStationId = deviceStationId;
	invocation.emailAddress = emailAddress;
	invocation.emailReceipt = emailReceipt;
	invocation.enablePrint = enablePrint;
	invocation.printSummaryReceipt = printSummaryReceipt;
	invocation.tillNumber = tillNumber;
	invocation.complete = 
	^(RESTInvocation*invocation, NSString*invocationServer, id invocationResult, NSError *invocationError, ServerError *invocationServerError, NSString *invocationEtag)
	{
		if (complete)
		{
			complete(invocationResult, invocationError, invocationServerError);
		}
	};
	[self invokeSynchronous:invocation
	                 server:nil
	                   etag:nil
	               complete:invocation.complete];
}

-(void)getActiveBasketWithDeviceID:(NSString*)deviceID complete:(void (^)(BasketAssociateDTO* result, NSError* error, ServerError* serverError))complete
{
	GetActiveBasketInvocation *invocation = [[GetActiveBasketInvocation alloc] init];
	invocation.deviceID = deviceID;
	invocation.complete = 
	^(RESTInvocation*invocation, NSString*invocationServer, id invocationResult, NSError *invocationError, ServerError *invocationServerError, NSString *invocationEtag)
	{
		if (complete)
		{
			complete(invocationResult, invocationError, invocationServerError);
		}
	};
	[self invokeSynchronous:invocation
	                 server:nil
	                   etag:nil
	               complete:invocation.complete];
}

-(void)getAssociateSalesHistoryWithAssociateId:(NSString*)associateId from:(NSDate*)from locationId:(NSString*)locationId status:(NSString*)status to:(NSDate*)to complete:(void (^)(NSArray* result, NSError* error, ServerError* serverError))complete
{
	GetAssociateSalesHistoryInvocation *invocation = [[GetAssociateSalesHistoryInvocation alloc] init];
	invocation.associateId = associateId;
	invocation.from = from;
	invocation.locationId = locationId;
	invocation.status = status;
	invocation.to = to;
	invocation.complete = 
	^(RESTInvocation*invocation, NSString*invocationServer, id invocationResult, NSError *invocationError, ServerError *invocationServerError, NSString *invocationEtag)
	{
		if (complete)
		{
			complete(invocationResult, invocationError, invocationServerError);
		}
	};
	[self invokeSynchronous:invocation
	                 server:nil
	                   etag:nil
	               complete:invocation.complete];
}

-(void)getBasketWithBasketID:(NSString*)basketID complete:(void (^)(BasketAssociateDTO* result, NSError* error, ServerError* serverError))complete
{
	GetBasketInvocation *invocation = [[GetBasketInvocation alloc] init];
	invocation.basketID = basketID;
	invocation.complete = 
	^(RESTInvocation*invocation, NSString*invocationServer, id invocationResult, NSError *invocationError, ServerError *invocationServerError, NSString *invocationEtag)
	{
		if (complete)
		{
			complete(invocationResult, invocationError, invocationServerError);
		}
	};
	[self invokeSynchronous:invocation
	                 server:nil
	                   etag:nil
	               complete:invocation.complete];
}

-(void)getConfigurationWithClearServerCache:(BOOL)clearServerCache storeID:(NSString*)storeID tillNumber:(NSString*)tillNumber etag:(NSString *)etag complete:(void (^)(Configuration* result, NSError* error, ServerError* serverError, NSString* etag))complete
{
	GetConfigurationInvocation *invocation = [[GetConfigurationInvocation alloc] init];
	invocation.clearServerCache = clearServerCache;
	invocation.storeID = storeID;
	invocation.tillNumber = tillNumber;
	invocation.complete = 
	^(RESTInvocation*invocation, NSString*invocationServer, id invocationResult, NSError *invocationError, ServerError *invocationServerError, NSString *invocationEtag)
	{
		if (complete)
		{
			complete(invocationResult, invocationError, invocationServerError, invocationEtag);
		}
	};
	[self invokeSynchronous:invocation
	                 server:nil
	                   etag:etag
	               complete:invocation.complete];
}

-(void)getDeviceStationWithDeviceStationId:(NSString*)deviceStationId complete:(void (^)(DeviceStation* result, NSError* error, ServerError* serverError))complete
{
	GetDeviceStationInvocation *invocation = [[GetDeviceStationInvocation alloc] init];
	invocation.deviceStationId = deviceStationId;
	invocation.complete = 
	^(RESTInvocation*invocation, NSString*invocationServer, id invocationResult, NSError *invocationError, ServerError *invocationServerError, NSString *invocationEtag)
	{
		if (complete)
		{
			complete(invocationResult, invocationError, invocationServerError);
		}
	};
	[self invokeSynchronous:invocation
	                 server:nil
	                   etag:nil
	               complete:invocation.complete];
}

-(void)getDeviceStationsWithDeviceType:(NSString*)deviceType storeId:(NSString*)storeId complete:(void (^)(NSArray* result, NSError* error, ServerError* serverError))complete
{
	GetDeviceStationsInvocation *invocation = [[GetDeviceStationsInvocation alloc] init];
	invocation.deviceType = deviceType;
	invocation.storeId = storeId;
	invocation.complete = 
	^(RESTInvocation*invocation, NSString*invocationServer, id invocationResult, NSError *invocationError, ServerError *invocationServerError, NSString *invocationEtag)
	{
		if (complete)
		{
			complete(invocationResult, invocationError, invocationServerError);
		}
	};
	[self invokeSynchronous:invocation
	                 server:nil
	                   etag:nil
	               complete:invocation.complete];
}

-(void)getItemAvailabilityWithScanID:(NSString*)scanID storeID:(NSString*)storeID complete:(void (^)(NSArray* result, NSError* error, ServerError* serverError))complete
{
	GetItemAvailabilityInvocation *invocation = [[GetItemAvailabilityInvocation alloc] init];
	invocation.scanID = scanID;
	invocation.storeID = storeID;
	invocation.complete = 
	^(RESTInvocation*invocation, NSString*invocationServer, id invocationResult, NSError *invocationError, ServerError *invocationServerError, NSString *invocationEtag)
	{
		if (complete)
		{
			complete(invocationResult, invocationError, invocationServerError);
		}
	};
	[self invokeSynchronous:invocation
	                 server:nil
	                   etag:nil
	               complete:invocation.complete];
}

-(void)getStoreDetailsWithStoreId:(NSString*)storeId server:(NSString *)server complete:(void (^)(Store* result, NSError* error, ServerError* serverError))complete
{
	GetStoreDetailsInvocation *invocation = [[GetStoreDetailsInvocation alloc] init];
	invocation.storeId = storeId;
	invocation.complete = 
	^(RESTInvocation*invocation, NSString*invocationServer, id invocationResult, NSError *invocationError, ServerError *invocationServerError, NSString *invocationEtag)
	{
		if (complete)
		{
			complete(invocationResult, invocationError, invocationServerError);
		}
	};
	[self invokeSynchronous:invocation
	                 server:server
	                   etag:nil
	               complete:invocation.complete];
}

-(void)internetReturnWithBasketId:(NSString*)basketId itemScanId:(NSString*)itemScanId itemSerialNumber:(NSString*)itemSerialNumber originalItemDescription:(NSString*)originalItemDescription originalItemScanId:(NSString*)originalItemScanId originalReferenceId:(NSString*)originalReferenceId price:(NSString*)price reasonDescription:(NSString*)reasonDescription reasonId:(NSString*)reasonId complete:(void (^)(BasketAssociateDTO* result, NSError* error, ServerError* serverError))complete
{
	InternetReturnInvocation *invocation = [[InternetReturnInvocation alloc] init];
	invocation.basketId = basketId;
	invocation.itemScanId = itemScanId;
	invocation.itemSerialNumber = itemSerialNumber;
	invocation.originalItemDescription = originalItemDescription;
	invocation.originalItemScanId = originalItemScanId;
	invocation.originalReferenceId = originalReferenceId;
	invocation.price = price;
	invocation.reasonDescription = reasonDescription;
	invocation.reasonId = reasonId;
	invocation.complete = 
	^(RESTInvocation*invocation, NSString*invocationServer, id invocationResult, NSError *invocationError, ServerError *invocationServerError, NSString *invocationEtag)
	{
		if (complete)
		{
			complete(invocationResult, invocationError, invocationServerError);
		}
	};
	[self invokeSynchronous:invocation
	                 server:nil
	                   etag:nil
	               complete:invocation.complete];
}

-(void)linkDeviceStationWithDeviceStationId:(NSString*)deviceStationId force:(BOOL)force complete:(void (^)(DeviceStation* result, NSError* error, ServerError* serverError))complete
{
	LinkDeviceStationInvocation *invocation = [[LinkDeviceStationInvocation alloc] init];
	invocation.deviceStationId = deviceStationId;
	invocation.force = force;
	invocation.complete = 
	^(RESTInvocation*invocation, NSString*invocationServer, id invocationResult, NSError *invocationError, ServerError *invocationServerError, NSString *invocationEtag)
	{
		if (complete)
		{
			complete(invocationResult, invocationError, invocationServerError);
		}
	};
	[self invokeSynchronous:invocation
	                 server:nil
	                   etag:nil
	               complete:invocation.complete];
}

-(void)linkedReturnWithBasketId:(NSString*)basketId itemScanId:(NSString*)itemScanId itemSerialNumber:(NSString*)itemSerialNumber lineId:(NSString*)lineId originalItemDescription:(NSString*)originalItemDescription originalItemScanId:(NSString*)originalItemScanId originalTransactionDate:(NSString*)originalTransactionDate originalTransactionId:(NSString*)originalTransactionId originalTransactionStoreId:(NSString*)originalTransactionStoreId originalTransactionTill:(NSString*)originalTransactionTill reasonDescription:(NSString*)reasonDescription reasonId:(NSString*)reasonId complete:(void (^)(BasketAssociateDTO* result, NSError* error, ServerError* serverError))complete
{
	LinkedReturnInvocation *invocation = [[LinkedReturnInvocation alloc] init];
	invocation.basketId = basketId;
	invocation.itemScanId = itemScanId;
	invocation.itemSerialNumber = itemSerialNumber;
	invocation.lineId = lineId;
	invocation.originalItemDescription = originalItemDescription;
	invocation.originalItemScanId = originalItemScanId;
	invocation.originalTransactionDate = originalTransactionDate;
	invocation.originalTransactionId = originalTransactionId;
	invocation.originalTransactionStoreId = originalTransactionStoreId;
	invocation.originalTransactionTill = originalTransactionTill;
	invocation.reasonDescription = reasonDescription;
	invocation.reasonId = reasonId;
	invocation.complete = 
	^(RESTInvocation*invocation, NSString*invocationServer, id invocationResult, NSError *invocationError, ServerError *invocationServerError, NSString *invocationEtag)
	{
		if (complete)
		{
			complete(invocationResult, invocationError, invocationServerError);
		}
	};
	[self invokeSynchronous:invocation
	                 server:nil
	                   etag:nil
	               complete:invocation.complete];
}

-(void)listStoresWithBrandId:(NSString*)brandId isHomeStore:(BOOL)isHomeStore latitude:(double)latitude longitude:(double)longitude server:(NSString *)server complete:(void (^)(Stores* result, NSError* error, ServerError* serverError))complete
{
	ListStoresInvocation *invocation = [[ListStoresInvocation alloc] init];
	invocation.brandId = brandId;
	invocation.isHomeStore = isHomeStore;
	invocation.latitude = latitude;
	invocation.longitude = longitude;
	invocation.complete = 
	^(RESTInvocation*invocation, NSString*invocationServer, id invocationResult, NSError *invocationError, ServerError *invocationServerError, NSString *invocationEtag)
	{
		if (complete)
		{
			complete(invocationResult, invocationError, invocationServerError);
		}
	};
	[self invokeSynchronous:invocation
	                 server:server
	                   etag:nil
	               complete:invocation.complete];
}

-(void)loginAssociateWithCredentials:(Credentials*)credentials signOffExistingOperator:(BOOL)signOffExistingOperator complete:(void (^)(Credentials* result, NSError* error, ServerError* serverError))complete
{
	LoginAssociateInvocation *invocation = [[LoginAssociateInvocation alloc] init];
	invocation.credentials = credentials;
	invocation.signOffExistingOperator = signOffExistingOperator;
	invocation.complete = 
	^(RESTInvocation*invocation, NSString*invocationServer, id invocationResult, NSError *invocationError, ServerError *invocationServerError, NSString *invocationEtag)
	{
		if (complete)
		{
			complete(invocationResult, invocationError, invocationServerError);
		}
	};
	[self invokeSynchronous:invocation
	                 server:nil
	                   etag:nil
	               complete:invocation.complete];
}

-(void)logMobileClientCrashEventWithCrashText:(NSString*)crashText crashTime:(NSDate*)crashTime storeID:(NSString*)storeID version:(NSString*)version workStationId:(NSString*)workStationId complete:(void (^)(NSError* error, ServerError* serverError))complete
{
	LogMobileClientCrashEventInvocation *invocation = [[LogMobileClientCrashEventInvocation alloc] init];
	invocation.crashText = crashText;
	invocation.crashTime = crashTime;
	invocation.storeID = storeID;
	invocation.version = version;
	invocation.workStationId = workStationId;
	invocation.complete = 
	^(RESTInvocation*invocation, NSString*invocationServer, id invocationResult, NSError *invocationError, ServerError *invocationServerError, NSString *invocationEtag)
	{
		if (complete)
		{
			complete(invocationError, invocationServerError);
		}
	};
	[self invokeSynchronous:invocation
	                 server:nil
	                   etag:nil
	               complete:invocation.complete];
}

-(void)logoutAssociateWithCredentials:(Credentials*)credentials complete:(void (^)(Credentials* result, NSError* error, ServerError* serverError))complete
{
	LogoutAssociateInvocation *invocation = [[LogoutAssociateInvocation alloc] init];
	invocation.credentials = credentials;
	invocation.complete = 
	^(RESTInvocation*invocation, NSString*invocationServer, id invocationResult, NSError *invocationError, ServerError *invocationServerError, NSString *invocationEtag)
	{
		if (complete)
		{
			complete(invocationResult, invocationError, invocationServerError);
		}
	};
	[self invokeSynchronous:invocation
	                 server:nil
	                   etag:nil
	               complete:invocation.complete];
}

-(void)lookupSaleWithBarcode:(NSString*)barcode originalBasketId:(NSString*)originalBasketId originalStoreId:(NSString*)originalStoreId originalTillNumber:(NSString*)originalTillNumber originalTransactionDate:(NSString*)originalTransactionDate complete:(void (^)(BasketAssociateDTO* result, NSError* error, ServerError* serverError))complete
{
	LookupSaleInvocation *invocation = [[LookupSaleInvocation alloc] init];
	invocation.barcode = barcode;
	invocation.originalBasketId = originalBasketId;
	invocation.originalStoreId = originalStoreId;
	invocation.originalTillNumber = originalTillNumber;
	invocation.originalTransactionDate = originalTransactionDate;
	invocation.complete = 
	^(RESTInvocation*invocation, NSString*invocationServer, id invocationResult, NSError *invocationError, ServerError *invocationServerError, NSString *invocationEtag)
	{
		if (complete)
		{
			complete(invocationResult, invocationError, invocationServerError);
		}
	};
	[self invokeSynchronous:invocation
	                 server:nil
	                   etag:nil
	               complete:invocation.complete];
}

-(void)noReceiptWithBasketId:(NSString*)basketId itemScanId:(NSString*)itemScanId itemSerialNumber:(NSString*)itemSerialNumber originalItemDescription:(NSString*)originalItemDescription originalItemScanId:(NSString*)originalItemScanId price:(NSString*)price reasonDescription:(NSString*)reasonDescription reasonId:(NSString*)reasonId complete:(void (^)(BasketAssociateDTO* result, NSError* error, ServerError* serverError))complete
{
	NoReceiptInvocation *invocation = [[NoReceiptInvocation alloc] init];
	invocation.basketId = basketId;
	invocation.itemScanId = itemScanId;
	invocation.itemSerialNumber = itemSerialNumber;
	invocation.originalItemDescription = originalItemDescription;
	invocation.originalItemScanId = originalItemScanId;
	invocation.price = price;
	invocation.reasonDescription = reasonDescription;
	invocation.reasonId = reasonId;
	invocation.complete = 
	^(RESTInvocation*invocation, NSString*invocationServer, id invocationResult, NSError *invocationError, ServerError *invocationServerError, NSString *invocationEtag)
	{
		if (complete)
		{
			complete(invocationResult, invocationError, invocationServerError);
		}
	};
	[self invokeSynchronous:invocation
	                 server:nil
	                   etag:nil
	               complete:invocation.complete];
}

-(void)notesForCustomerWithCustomerId:(NSString*)customerId complete:(void (^)(NSArray* result, NSError* error, ServerError* serverError))complete
{
	NotesForCustomerInvocation *invocation = [[NotesForCustomerInvocation alloc] init];
	invocation.customerId = customerId;
	invocation.complete = 
	^(RESTInvocation*invocation, NSString*invocationServer, id invocationResult, NSError *invocationError, ServerError *invocationServerError, NSString *invocationEtag)
	{
		if (complete)
		{
			complete(invocationResult, invocationError, invocationServerError);
		}
	};
	[self invokeSynchronous:invocation
	                 server:nil
	                   etag:nil
	               complete:invocation.complete];
}

-(void)openCashDrawerWithDeviceStationId:(NSString*)deviceStationId complete:(void (^)(NSError* error, ServerError* serverError))complete
{
	OpenCashDrawerInvocation *invocation = [[OpenCashDrawerInvocation alloc] init];
	invocation.deviceStationId = deviceStationId;
	invocation.complete = 
	^(RESTInvocation*invocation, NSString*invocationServer, id invocationResult, NSError *invocationError, ServerError *invocationServerError, NSString *invocationEtag)
	{
		if (complete)
		{
			complete(invocationError, invocationServerError);
		}
	};
	[self invokeSynchronous:invocation
	                 server:nil
	                   etag:nil
	               complete:invocation.complete];
}

-(void)registerCustomerWithCustomer:(Customer*)customer complete:(void (^)(Customer* result, NSError* error, ServerError* serverError))complete
{
	RegisterCustomerInvocation *invocation = [[RegisterCustomerInvocation alloc] init];
	invocation.customer = customer;
	invocation.complete = 
	^(RESTInvocation*invocation, NSString*invocationServer, id invocationResult, NSError *invocationError, ServerError *invocationServerError, NSString *invocationEtag)
	{
		if (complete)
		{
			complete(invocationResult, invocationError, invocationServerError);
		}
	};
	[self invokeSynchronous:invocation
	                 server:nil
	                   etag:nil
	               complete:invocation.complete];
}

-(void)removeBasketDiscountWithBasketDiscountID:(NSArray*)basketDiscountID basketID:(NSString*)basketID complete:(void (^)(BasketAssociateDTO* result, NSError* error, ServerError* serverError))complete
{
	RemoveBasketDiscountInvocation *invocation = [[RemoveBasketDiscountInvocation alloc] init];
	invocation.basketDiscountID = basketDiscountID;
	invocation.basketID = basketID;
	invocation.complete = 
	^(RESTInvocation*invocation, NSString*invocationServer, id invocationResult, NSError *invocationError, ServerError *invocationServerError, NSString *invocationEtag)
	{
		if (complete)
		{
			complete(invocationResult, invocationError, invocationServerError);
		}
	};
	[self invokeSynchronous:invocation
	                 server:nil
	                   etag:nil
	               complete:invocation.complete];
}

-(void)removeBasketItemWithBasketID:(NSString*)basketID basketItemID:(NSArray*)basketItemID deviceStationId:(NSString*)deviceStationId complete:(void (^)(BasketAssociateDTO* result, NSError* error, ServerError* serverError))complete
{
	RemoveBasketItemInvocation *invocation = [[RemoveBasketItemInvocation alloc] init];
	invocation.basketID = basketID;
	invocation.basketItemID = basketItemID;
	invocation.deviceStationId = deviceStationId;
	invocation.complete = 
	^(RESTInvocation*invocation, NSString*invocationServer, id invocationResult, NSError *invocationError, ServerError *invocationServerError, NSString *invocationEtag)
	{
		if (complete)
		{
			complete(invocationResult, invocationError, invocationServerError);
		}
	};
	[self invokeSynchronous:invocation
	                 server:nil
	                   etag:nil
	               complete:invocation.complete];
}

-(void)removeBasketItemDiscountWithBasketID:(NSString*)basketID basketItemDiscountID:(NSArray*)basketItemDiscountID basketItemID:(NSString*)basketItemID complete:(void (^)(BasketAssociateDTO* result, NSError* error, ServerError* serverError))complete
{
	RemoveBasketItemDiscountInvocation *invocation = [[RemoveBasketItemDiscountInvocation alloc] init];
	invocation.basketID = basketID;
	invocation.basketItemDiscountID = basketItemDiscountID;
	invocation.basketItemID = basketItemID;
	invocation.complete = 
	^(RESTInvocation*invocation, NSString*invocationServer, id invocationResult, NSError *invocationError, ServerError *invocationServerError, NSString *invocationEtag)
	{
		if (complete)
		{
			complete(invocationResult, invocationError, invocationServerError);
		}
	};
	[self invokeSynchronous:invocation
	                 server:nil
	                   etag:nil
	               complete:invocation.complete];
}

-(void)reprintLastTxWithDeviceStationId:(NSString*)deviceStationId complete:(void (^)(BasketAssociateDTO* result, NSError* error, ServerError* serverError))complete
{
	ReprintLastTxInvocation *invocation = [[ReprintLastTxInvocation alloc] init];
	invocation.deviceStationId = deviceStationId;
	invocation.complete = 
	^(RESTInvocation*invocation, NSString*invocationServer, id invocationResult, NSError *invocationError, ServerError *invocationServerError, NSString *invocationEtag)
	{
		if (complete)
		{
			complete(invocationResult, invocationError, invocationServerError);
		}
	};
	[self invokeSynchronous:invocation
	                 server:nil
	                   etag:nil
	               complete:invocation.complete];
}

-(void)resumeBasketWithBasketBarcode:(NSString*)basketBarcode basketID:(NSString*)basketID deviceID:(NSString*)deviceID tillNumber:(NSString*)tillNumber complete:(void (^)(BasketAssociateDTO* result, NSError* error, ServerError* serverError))complete
{
	ResumeBasketInvocation *invocation = [[ResumeBasketInvocation alloc] init];
	invocation.basketBarcode = basketBarcode;
	invocation.basketID = basketID;
	invocation.deviceID = deviceID;
	invocation.tillNumber = tillNumber;
	invocation.complete = 
	^(RESTInvocation*invocation, NSString*invocationServer, id invocationResult, NSError *invocationError, ServerError *invocationServerError, NSString *invocationEtag)
	{
		if (complete)
		{
			complete(invocationResult, invocationError, invocationServerError);
		}
	};
	[self invokeSynchronous:invocation
	                 server:nil
	                   etag:nil
	               complete:invocation.complete];
}

-(void)returnBasketItemWithBasketID:(NSString*)basketID itemScanID:(NSString*)itemScanID itemSerialNumber:(NSString*)itemSerialNumber originalItemDescription:(NSString*)originalItemDescription originalItemScanId:(NSString*)originalItemScanId originalTransactionBarcode:(NSString*)originalTransactionBarcode originalTransactionDate:(NSDate*)originalTransactionDate originalTransactionId:(NSNumber*)originalTransactionId originalTransactionStoreId:(NSNumber*)originalTransactionStoreId originalTransactionTill:(NSNumber*)originalTransactionTill price:(NSString*)price reasonId:(NSString*)reasonId complete:(void (^)(BasketAssociateDTO* result, NSError* error, ServerError* serverError))complete
{
	ReturnBasketItemInvocation *invocation = [[ReturnBasketItemInvocation alloc] init];
	invocation.basketID = basketID;
	invocation.itemScanID = itemScanID;
	invocation.itemSerialNumber = itemSerialNumber;
	invocation.originalItemDescription = originalItemDescription;
	invocation.originalItemScanId = originalItemScanId;
	invocation.originalTransactionBarcode = originalTransactionBarcode;
	invocation.originalTransactionDate = originalTransactionDate;
	invocation.originalTransactionId = originalTransactionId;
	invocation.originalTransactionStoreId = originalTransactionStoreId;
	invocation.originalTransactionTill = originalTransactionTill;
	invocation.price = price;
	invocation.reasonId = reasonId;
	invocation.complete = 
	^(RESTInvocation*invocation, NSString*invocationServer, id invocationResult, NSError *invocationError, ServerError *invocationServerError, NSString *invocationEtag)
	{
		if (complete)
		{
			complete(invocationResult, invocationError, invocationServerError);
		}
	};
	[self invokeSynchronous:invocation
	                 server:nil
	                   etag:nil
	               complete:invocation.complete];
}

-(void)returnWithBasketId:(NSString*)basketId itemScanId:(NSString*)itemScanId itemSerialNumber:(NSString*)itemSerialNumber lineId:(NSString*)lineId originalCustomerCard:(NSString*)originalCustomerCard originalItemDescription:(NSString*)originalItemDescription originalItemScanId:(NSString*)originalItemScanId originalReferenceId:(NSString*)originalReferenceId originalSalesPerson:(NSString*)originalSalesPerson originalTransactionDate:(NSString*)originalTransactionDate originalTransactionId:(NSString*)originalTransactionId originalTransactionStoreId:(NSString*)originalTransactionStoreId originalTransactionTill:(NSString*)originalTransactionTill price:(NSString*)price reasonDescription:(NSString*)reasonDescription reasonId:(NSString*)reasonId type:(NSString*)type complete:(void (^)(BasketAssociateDTO* result, NSError* error, ServerError* serverError))complete
{
	ReturnWithTypeInvocation *invocation = [[ReturnWithTypeInvocation alloc] init];
	invocation.basketId = basketId;
	invocation.itemScanId = itemScanId;
	invocation.itemSerialNumber = itemSerialNumber;
	invocation.lineId = lineId;
	invocation.originalCustomerCard = originalCustomerCard;
	invocation.originalItemDescription = originalItemDescription;
	invocation.originalItemScanId = originalItemScanId;
	invocation.originalReferenceId = originalReferenceId;
	invocation.originalSalesPerson = originalSalesPerson;
	invocation.originalTransactionDate = originalTransactionDate;
	invocation.originalTransactionId = originalTransactionId;
	invocation.originalTransactionStoreId = originalTransactionStoreId;
	invocation.originalTransactionTill = originalTransactionTill;
	invocation.price = price;
	invocation.reasonDescription = reasonDescription;
	invocation.reasonId = reasonId;
	invocation.type = type;
	invocation.complete = 
	^(RESTInvocation*invocation, NSString*invocationServer, id invocationResult, NSError *invocationError, ServerError *invocationServerError, NSString *invocationEtag)
	{
		if (complete)
		{
			complete(invocationResult, invocationError, invocationServerError);
		}
	};
	[self invokeSynchronous:invocation
	                 server:nil
	                   etag:nil
	               complete:invocation.complete];
}

-(void)searchItemsWithDepartment:(NSString*)department itemDescription:(NSString*)itemDescription pageNumber:(NSInteger)pageNumber storeID:(NSString*)storeID complete:(void (^)(NSArray* result, NSError* error, ServerError* serverError))complete
{
	SearchItemsInvocation *invocation = [[SearchItemsInvocation alloc] init];
	invocation.department = department;
	invocation.itemDescription = itemDescription;
	invocation.pageNumber = pageNumber;
	invocation.storeID = storeID;
	invocation.complete = 
	^(RESTInvocation*invocation, NSString*invocationServer, id invocationResult, NSError *invocationError, ServerError *invocationServerError, NSString *invocationEtag)
	{
		if (complete)
		{
			complete(invocationResult, invocationError, invocationServerError);
		}
	};
	[self invokeSynchronous:invocation
	                 server:nil
	                   etag:nil
	               complete:invocation.complete];
}

-(void)setNotesForCustomerWithCustomerId:(NSString*)customerId customerNotes:(NSArray*)customerNotes complete:(void (^)(NSArray* result, NSError* error, ServerError* serverError))complete
{
	SetNotesForCustomerInvocation *invocation = [[SetNotesForCustomerInvocation alloc] init];
	invocation.customerId = customerId;
	invocation.customerNotes = customerNotes;
	invocation.complete = 
	^(RESTInvocation*invocation, NSString*invocationServer, id invocationResult, NSError *invocationError, ServerError *invocationServerError, NSString *invocationEtag)
	{
		if (complete)
		{
			complete(invocationResult, invocationError, invocationServerError);
		}
	};
	[self invokeSynchronous:invocation
	                 server:nil
	                   etag:nil
	               complete:invocation.complete];
}

-(void)suspendBasketWithBasketDescription:(NSString*)basketDescription basketID:(NSString*)basketID complete:(void (^)(BasketAssociateDTO* result, NSError* error, ServerError* serverError))complete
{
	SuspendBasketInvocation *invocation = [[SuspendBasketInvocation alloc] init];
	invocation.basketDescription = basketDescription;
	invocation.basketID = basketID;
	invocation.complete = 
	^(RESTInvocation*invocation, NSString*invocationServer, id invocationResult, NSError *invocationError, ServerError *invocationServerError, NSString *invocationEtag)
	{
		if (complete)
		{
			complete(invocationResult, invocationError, invocationServerError);
		}
	};
	[self invokeSynchronous:invocation
	                 server:nil
	                   etag:nil
	               complete:invocation.complete];
}

-(void)toggleTrainingModeComplete:(void (^)(BOOL result, NSError* error, ServerError* serverError))complete
{
	ToggleTrainingModeInvocation *invocation = [[ToggleTrainingModeInvocation alloc] init];
	invocation.complete = 
	^(RESTInvocation*invocation, NSString*invocationServer, id invocationResult, NSError *invocationError, ServerError *invocationServerError, NSString *invocationEtag)
	{
		if (complete)
		{
			complete([invocationResult boolValue], invocationError, invocationServerError);
		}
	};
	[self invokeSynchronous:invocation
	                 server:nil
	                   etag:nil
	               complete:invocation.complete];
}

-(void)unlinkDeviceWithDeviceStationId:(NSString*)deviceStationId complete:(void (^)(DeviceStation* result, NSError* error, ServerError* serverError))complete
{
	UnlinkDeviceInvocation *invocation = [[UnlinkDeviceInvocation alloc] init];
	invocation.deviceStationId = deviceStationId;
	invocation.complete = 
	^(RESTInvocation*invocation, NSString*invocationServer, id invocationResult, NSError *invocationError, ServerError *invocationServerError, NSString *invocationEtag)
	{
		if (complete)
		{
			complete(invocationResult, invocationError, invocationServerError);
		}
	};
	[self invokeSynchronous:invocation
	                 server:nil
	                   etag:nil
	               complete:invocation.complete];
}

-(void)unlinkedReturnWithBasketId:(NSString*)basketId itemScanId:(NSString*)itemScanId itemSerialNumber:(NSString*)itemSerialNumber originalCustomerCard:(NSString*)originalCustomerCard originalItemDescription:(NSString*)originalItemDescription originalItemScanId:(NSString*)originalItemScanId originalSalesPerson:(NSString*)originalSalesPerson originalTransactionDate:(NSString*)originalTransactionDate originalTransactionId:(NSString*)originalTransactionId originalTransactionStoreId:(NSString*)originalTransactionStoreId originalTransactionTill:(NSString*)originalTransactionTill price:(NSString*)price reasonDescription:(NSString*)reasonDescription reasonId:(NSString*)reasonId complete:(void (^)(BasketAssociateDTO* result, NSError* error, ServerError* serverError))complete
{
	UnlinkedReturnInvocation *invocation = [[UnlinkedReturnInvocation alloc] init];
	invocation.basketId = basketId;
	invocation.itemScanId = itemScanId;
	invocation.itemSerialNumber = itemSerialNumber;
	invocation.originalCustomerCard = originalCustomerCard;
	invocation.originalItemDescription = originalItemDescription;
	invocation.originalItemScanId = originalItemScanId;
	invocation.originalSalesPerson = originalSalesPerson;
	invocation.originalTransactionDate = originalTransactionDate;
	invocation.originalTransactionId = originalTransactionId;
	invocation.originalTransactionStoreId = originalTransactionStoreId;
	invocation.originalTransactionTill = originalTransactionTill;
	invocation.price = price;
	invocation.reasonDescription = reasonDescription;
	invocation.reasonId = reasonId;
	invocation.complete = 
	^(RESTInvocation*invocation, NSString*invocationServer, id invocationResult, NSError *invocationError, ServerError *invocationServerError, NSString *invocationEtag)
	{
		if (complete)
		{
			complete(invocationResult, invocationError, invocationServerError);
		}
	};
	[self invokeSynchronous:invocation
	                 server:nil
	                   etag:nil
	               complete:invocation.complete];
}

-(void)updateCustomerWithCustomer:(Customer*)customer customerId:(NSString*)customerId complete:(void (^)(NSError* error, ServerError* serverError))complete
{
	UpdateCustomerInvocation *invocation = [[UpdateCustomerInvocation alloc] init];
	invocation.customer = customer;
	invocation.customerId = customerId;
	invocation.complete = 
	^(RESTInvocation*invocation, NSString*invocationServer, id invocationResult, NSError *invocationError, ServerError *invocationServerError, NSString *invocationEtag)
	{
		if (complete)
		{
			complete(invocationError, invocationServerError);
		}
	};
	[self invokeSynchronous:invocation
	                 server:nil
	                   etag:nil
	               complete:invocation.complete];
}

-(void)updateDeviceWithDevice:(DeviceProfile*)device server:(NSString *)server complete:(void (^)(NSError* error, ServerError* serverError))complete
{
	UpdateDeviceInvocation *invocation = [[UpdateDeviceInvocation alloc] init];
	invocation.device = device;
	invocation.complete = 
	^(RESTInvocation*invocation, NSString*invocationServer, id invocationResult, NSError *invocationError, ServerError *invocationServerError, NSString *invocationEtag)
	{
		if (complete)
		{
			complete(invocationError, invocationServerError);
		}
	};
	[self invokeSynchronous:invocation
	                 server:server
	                   etag:nil
	               complete:invocation.complete];
}

-(void)updateDeviceStationWithDeviceStation:(DeviceStation*)deviceStation deviceStationId:(NSString*)deviceStationId complete:(void (^)(DeviceStation* result, NSError* error, ServerError* serverError))complete
{
	UpdateDeviceStationInvocation *invocation = [[UpdateDeviceStationInvocation alloc] init];
	invocation.deviceStation = deviceStation;
	invocation.deviceStationId = deviceStationId;
	invocation.complete = 
	^(RESTInvocation*invocation, NSString*invocationServer, id invocationResult, NSError *invocationError, ServerError *invocationServerError, NSString *invocationEtag)
	{
		if (complete)
		{
			complete(invocationResult, invocationError, invocationServerError);
		}
	};
	[self invokeSynchronous:invocation
	                 server:nil
	                   etag:nil
	               complete:invocation.complete];
}

-(void)updateGiftReceiptWithBasket:(BasketAssociateDTO*)basket complete:(void (^)(BasketAssociateDTO* result, NSError* error, ServerError* serverError))complete
{
	UpdateGiftReceiptInvocation *invocation = [[UpdateGiftReceiptInvocation alloc] init];
	invocation.basket = basket;
	invocation.complete = 
	^(RESTInvocation*invocation, NSString*invocationServer, id invocationResult, NSError *invocationError, ServerError *invocationServerError, NSString *invocationEtag)
	{
		if (complete)
		{
			complete(invocationResult, invocationError, invocationServerError);
		}
	};
	[self invokeSynchronous:invocation
	                 server:nil
	                   etag:nil
	               complete:invocation.complete];
}

-(NSString*)typeForTag:(NSString*)tag
{
	tag = tag.lowercaseString;
	if ([tag isEqual:@"affiliation"])
	{
		return @"Affiliation";
	}
	if ([tag isEqual:@"airportcode"])
	{
		return @"AirportCode";
	}
	if ([tag isEqual:@"airportzone"])
	{
		return @"AirportZone";
	}
	if ([tag isEqual:@"airsideoption"])
	{
		return @"AirsideOption";
	}
	if ([tag isEqual:@"associate"])
	{
		return @"Associate";
	}
	if ([tag isEqual:@"associatesaleshistoryrequest"])
	{
		return @"AssociateSalesHistoryRequest";
	}
	if ([tag isEqual:@"associatebasket"])
	{
		return @"BasketAssociateDTO";
	}
	if ([tag isEqual:@"associateinfo"])
	{
		return @"BasketAssociateInfo";
	}
	if ([tag isEqual:@"basketcustomer"])
	{
		return @"BasketCustomer";
	}
	if ([tag isEqual:@"customeraffiliation"])
	{
		return @"BasketCustomerAffiliation";
	}
	if ([tag isEqual:@"customercard"])
	{
		return @"BasketCustomerCard";
	}
	if ([tag isEqual:@"flightdetails"])
	{
		return @"BasketFlightDetails";
	}
	if ([tag isEqual:@"foreignsubtotal"])
	{
		return @"BasketForeignSubTotalDTO";
	}
	if ([tag isEqual:@"basketitem"])
	{
		return @"BasketItem";
	}
	if ([tag isEqual:@"basketpricing"])
	{
		return @"BasketPricingDTO";
	}
	if ([tag isEqual:@"basketreward"])
	{
		return @"BasketRewardLine";
	}
	if ([tag isEqual:@"linediscountamount"])
	{
		return @"BasketRewardLineDiscountAmount";
	}
	if ([tag isEqual:@"linediscountpercent"])
	{
		return @"BasketRewardLineDiscountPercent";
	}
	if ([tag isEqual:@"linepriceadjust"])
	{
		return @"BasketRewardLinePriceAdjust";
	}
	if ([tag isEqual:@"basketreward"])
	{
		return @"BasketRewardSale";
	}
	if ([tag isEqual:@"salediscountamount"])
	{
		return @"BasketRewardSaleDiscountAmount";
	}
	if ([tag isEqual:@"salediscountpercent"])
	{
		return @"BasketRewardSaleDiscountPercent";
	}
	if ([tag isEqual:@"salepromotionamount"])
	{
		return @"BasketRewardSalePromotionAmount";
	}
	if ([tag isEqual:@"taxfree"])
	{
		return @"BasketTaxFree";
	}
	if ([tag isEqual:@"taxfreebasket"])
	{
		return @"BasketTaxFreeDocumentDTO";
	}
	if ([tag isEqual:@"baskettender"])
	{
		return @"BasketTender";
	}
	if ([tag isEqual:@"authorization"])
	{
		return @"BasketTenderAuthorization";
	}
	if ([tag isEqual:@"baskettenderforeigncurrency"])
	{
		return @"BasketTenderForeignCurrency";
	}
	if ([tag isEqual:@"clientsettings"])
	{
		return @"ClientSettings";
	}
	if ([tag isEqual:@"configuration"])
	{
		return @"Configuration";
	}
	if ([tag isEqual:@"country"])
	{
		return @"Country";
	}
	if ([tag isEqual:@"credentials"])
	{
		return @"Credentials";
	}
	if ([tag isEqual:@"customer"])
	{
		return @"Customer";
	}
	if ([tag isEqual:@"customeraddress"])
	{
		return @"CustomerAddress";
	}
	if ([tag isEqual:@"customercard"])
	{
		return @"CustomerCard";
	}
	if ([tag isEqual:@"customeremail"])
	{
		return @"CustomerEmail";
	}
	if ([tag isEqual:@"customeridentificationtype"])
	{
		return @"CustomerIdentificationType";
	}
	if ([tag isEqual:@"customersearch"])
	{
		return @"CustomerSearch";
	}
	if ([tag isEqual:@"softkeydataevent"])
	{
		return @"DataEventDTO";
	}
	if ([tag isEqual:@"device"])
	{
		return @"Device";
	}
	if ([tag isEqual:@"device"])
	{
		return @"DeviceProfile";
	}
	if ([tag isEqual:@"devicestation"])
	{
		return @"DeviceStation";
	}
	if ([tag isEqual:@"peripheral"])
	{
		return @"DeviceStationPeripheral";
	}
	if ([tag isEqual:@"discountcode"])
	{
		return @"DiscountCode";
	}
	if ([tag isEqual:@"reason"])
	{
		return @"DiscountReason";
	}
	if ([tag isEqual:@"reasongroup"])
	{
		return @"DiscountReasonGroup";
	}
	if ([tag isEqual:@"dumpcode"])
	{
		return @"DumpCode";
	}
	if ([tag isEqual:@"eftpayment"])
	{
		return @"EFTPayment";
	}
	if ([tag isEqual:@"formcontext"])
	{
		return @"FormConfigurationDTO";
	}
	if ([tag isEqual:@"giftreceiptrequest"])
	{
		return @"GiftReceiptRequest";
	}
	if ([tag isEqual:@"item"])
	{
		return @"Item";
	}
	if ([tag isEqual:@"itemavailability"])
	{
		return @"ItemAvailability";
	}
	if ([tag isEqual:@"itemavailabilityrecord"])
	{
		return @"ItemAvailabilityRecord";
	}
	if ([tag isEqual:@"itemimages"])
	{
		return @"ItemImages";
	}
	if ([tag isEqual:@"itemmodifier"])
	{
		return @"ItemModifier";
	}
	if ([tag isEqual:@"itemmodifiergroup"])
	{
		return @"ItemModifierGroup";
	}
	if ([tag isEqual:@"itemproperties"])
	{
		return @"ItemProperties";
	}
	if ([tag isEqual:@"keyvalue"])
	{
		return @"KeyValueDTO";
	}
	if ([tag isEqual:@"keyvaluegroup"])
	{
		return @"KeyValueGroup";
	}
	if ([tag isEqual:@"array"])
	{
		return @"KeyValues";
	}
	if ([tag isEqual:@"language"])
	{
		return @"Language";
	}
	if ([tag isEqual:@"localizable"])
	{
		return @"Localizable";
	}
	if ([tag isEqual:@"localizabletable"])
	{
		return @"LocalizableTable";
	}
	if ([tag isEqual:@"mandatorymodifiergroup"])
	{
		return @"MandatoryModifierGroup";
	}
	if ([tag isEqual:@"optionalmodifiergroup"])
	{
		return @"OptionalModifierGroup";
	}
	if ([tag isEqual:@"overrideerrormessage"])
	{
		return @"OverrideErrorMessage";
	}
	if ([tag isEqual:@"passwordrule"])
	{
		return @"PasswordRule";
	}
	if ([tag isEqual:@"payment"])
	{
		return @"Payment";
	}
	if ([tag isEqual:@"permission"])
	{
		return @"Permission";
	}
	if ([tag isEqual:@"permissiongroup"])
	{
		return @"PermissionGroup";
	}
	if ([tag isEqual:@"poserror"])
	{
		return @"POSError";
	}
	if ([tag isEqual:@"poserrormessage"])
	{
		return @"POSErrorMessage";
	}
	if ([tag isEqual:@"softkeyposfunctionevent"])
	{
		return @"POSFunctionSoftKeyEventDTO";
	}
	if ([tag isEqual:@"printdataline"])
	{
		return @"PrintDataLine";
	}
	if ([tag isEqual:@"element"])
	{
		return @"PrintElement";
	}
	if ([tag isEqual:@"productsearch"])
	{
		return @"ProductSearch";
	}
	if ([tag isEqual:@"property"])
	{
		return @"Property";
	}
	if ([tag isEqual:@"returnablesaleitem"])
	{
		return @"ReturnableSaleItem";
	}
	if ([tag isEqual:@"returninstance"])
	{
		return @"ReturnInstance";
	}
	if ([tag isEqual:@"returnitem"])
	{
		return @"ReturnItem";
	}
	if ([tag isEqual:@"saleitem"])
	{
		return @"SaleItem";
	}
	if ([tag isEqual:@"salesperson"])
	{
		return @"SalesPerson";
	}
	if ([tag isEqual:@"selectedmodifier"])
	{
		return @"SelectedModifier";
	}
	if ([tag isEqual:@"error"])
	{
		return @"ServerError";
	}
	if ([tag isEqual:@"affiliationrequiredmessage"])
	{
		return @"ServerErrorAffiliationRequiredMessage";
	}
	if ([tag isEqual:@"agerestrictionmessage"])
	{
		return @"ServerErrorAgeRestrictionMessage";
	}
	if ([tag isEqual:@"barcodeitemnotreturnedmessage"])
	{
		return @"ServerErrorBarcodeItemNotReturnedMessage";
	}
	if ([tag isEqual:@"devicestationclaimedmessage"])
	{
		return @"ServerErrorDeviceStationClaimedMessage";
	}
	if ([tag isEqual:@"devicestationdrawerinusemessage"])
	{
		return @"ServerErrorDeviceStationMessage";
	}
	if ([tag isEqual:@"message"])
	{
		return @"ServerErrorMessage";
	}
	if ([tag isEqual:@"unitofmeasuremessage"])
	{
		return @"ServerErrorUnitOfMeasureMessage";
	}
	if ([tag isEqual:@"serverrequest"])
	{
		return @"ServerRequest";
	}
	if ([tag isEqual:@"softkey"])
	{
		return @"SoftKey";
	}
	if ([tag isEqual:@"softkeycontext"])
	{
		return @"SoftKeyContext";
	}
	if ([tag isEqual:@"softkeyinputevent"])
	{
		return @"SoftKeyInputEventDTO";
	}
	if ([tag isEqual:@"payment"])
	{
		return @"StandardPayment";
	}
	if ([tag isEqual:@"store"])
	{
		return @"Store";
	}
	if ([tag isEqual:@"stores"])
	{
		return @"Stores";
	}
	if ([tag isEqual:@"tender"])
	{
		return @"Tender";
	}
	if ([tag isEqual:@"till"])
	{
		return @"Till";
	}
	if ([tag isEqual:@"softkeyuicontextevent"])
	{
		return @"UIContextEventDTO";
	}
	if ([tag isEqual:@"verifonepayment"])
	{
		return @"VerifonePayment";
	}
	if ([tag isEqual:@"yespaypayment"])
	{
		return @"YespayPayment";
	}
	return nil;
}

- (Class)classForContextName:(NSString*)contextName
{
	contextName = contextName.uppercaseString;
	if ([contextName isEqual:@"ADD_ITEM"])
	{
		return AddItemInvocation.class;
	}
	if ([contextName isEqual:@"ADD_ITEMS"])
	{
		return AddItemsInvocation.class;
	}
	if ([contextName isEqual:@"ADJUST_BASKET_ITEM"])
	{
		return AdjustBasketItemInvocation.class;
	}
	if ([contextName isEqual:@"ADJUST_BASKET_ITEM_PRICE"])
	{
		return AdjustBasketItemPriceInvocation.class;
	}
	if ([contextName isEqual:@"ADJUST_BASKET_ITEM_QUANTITY"])
	{
		return AdjustBasketItemQuantityInvocation.class;
	}
	if ([contextName isEqual:@"ADJUST_SALE"])
	{
		return AdjustSaleInvocation.class;
	}
	if ([contextName isEqual:@"ADJUST_TAX_FREE_FORM"])
	{
		return AdjustTaxFreeFormInvocation.class;
	}
	if ([contextName isEqual:@"AFFILIATION"])
	{
		return Affiliation.class;
	}
	if ([contextName isEqual:@"AIRPORTCODE"])
	{
		return AirportCode.class;
	}
	if ([contextName isEqual:@"AIRPORTZONE"])
	{
		return AirportZone.class;
	}
	if ([contextName isEqual:@"AIRSIDEOPTION"])
	{
		return AirsideOption.class;
	}
	if ([contextName isEqual:@"APPLY_BASKET_DISCOUNT"])
	{
		return ApplyBasketDiscountInvocation.class;
	}
	if ([contextName isEqual:@"APPLY_BASKET_ITEM_DISCOUNT"])
	{
		return ApplyBasketItemDiscountInvocation.class;
	}
	if ([contextName isEqual:@"ASSIGN_CUSTOMER"])
	{
		return AssignCustomerInvocation.class;
	}
	if ([contextName isEqual:@"ASSIGN_EMAIL_ADDRESS"])
	{
		return AssignEmailAddressInvocation.class;
	}
	if ([contextName isEqual:@"ASSOCIATE"])
	{
		return Associate.class;
	}
	if ([contextName isEqual:@"ASSOCIATE_DEVICE_STATION"])
	{
		return AssociateDeviceStationInvocation.class;
	}
	if ([contextName isEqual:@"ASSOCIATESALESHISTORYREQUEST"])
	{
		return AssociateSalesHistoryRequest.class;
	}
	if ([contextName isEqual:@"ASSOCIATE_TEXT_RECEIPT"])
	{
		return AssociateTextReceiptInvocation.class;
	}
	if ([contextName isEqual:@"BASKETASSOCIATE"])
	{
		return BasketAssociateDTO.class;
	}
	if ([contextName isEqual:@"BASKETASSOCIATEINFO"])
	{
		return BasketAssociateInfo.class;
	}
	if ([contextName isEqual:@"BASKETCUSTOMER"])
	{
		return BasketCustomer.class;
	}
	if ([contextName isEqual:@"BASKETCUSTOMERAFFILIATION"])
	{
		return BasketCustomerAffiliation.class;
	}
	if ([contextName isEqual:@"BASKETCUSTOMERCARD"])
	{
		return BasketCustomerCard.class;
	}
	if ([contextName isEqual:@"BASKET"])
	{
		return BasketDTO.class;
	}
	if ([contextName isEqual:@"BASKETFLIGHTDETAILS"])
	{
		return BasketFlightDetails.class;
	}
	if ([contextName isEqual:@"BASKETFOREIGNSUBTOTAL"])
	{
		return BasketForeignSubTotalDTO.class;
	}
	if ([contextName isEqual:@"BASKETITEM"])
	{
		return BasketItem.class;
	}
	if ([contextName isEqual:@"BASKETPRICING"])
	{
		return BasketPricingDTO.class;
	}
	if ([contextName isEqual:@"BASKETREWARDLINE"])
	{
		return BasketRewardLine.class;
	}
	if ([contextName isEqual:@"BASKETREWARDLINEDISCOUNTAMOUNT"])
	{
		return BasketRewardLineDiscountAmount.class;
	}
	if ([contextName isEqual:@"BASKETREWARDLINEDISCOUNTPERCENT"])
	{
		return BasketRewardLineDiscountPercent.class;
	}
	if ([contextName isEqual:@"BASKETREWARDLINEPRICEADJUST"])
	{
		return BasketRewardLinePriceAdjust.class;
	}
	if ([contextName isEqual:@"BASKETREWARDSALE"])
	{
		return BasketRewardSale.class;
	}
	if ([contextName isEqual:@"BASKETREWARDSALEDISCOUNTAMOUNT"])
	{
		return BasketRewardSaleDiscountAmount.class;
	}
	if ([contextName isEqual:@"BASKETREWARDSALEDISCOUNTPERCENT"])
	{
		return BasketRewardSaleDiscountPercent.class;
	}
	if ([contextName isEqual:@"BASKETREWARDSALEPROMOTIONAMOUNT"])
	{
		return BasketRewardSalePromotionAmount.class;
	}
	if ([contextName isEqual:@"BASKETTAXFREE"])
	{
		return BasketTaxFree.class;
	}
	if ([contextName isEqual:@"TAXFREEBASKET"])
	{
		return BasketTaxFreeDocumentDTO.class;
	}
	if ([contextName isEqual:@"BASKETTENDER"])
	{
		return BasketTender.class;
	}
	if ([contextName isEqual:@"BASKETTENDERAUTHORIZATION"])
	{
		return BasketTenderAuthorization.class;
	}
	if ([contextName isEqual:@"BASKETTENDERFOREIGNCURRENCY"])
	{
		return BasketTenderForeignCurrency.class;
	}
	if ([contextName isEqual:@"CANCEL_BASKET"])
	{
		return CancelBasketInvocation.class;
	}
	if ([contextName isEqual:@"CANCEL_CHECKOUT_BASKET"])
	{
		return CancelCheckoutBasketInvocation.class;
	}
	if ([contextName isEqual:@"CANCEL_TAX_FREE_FORM"])
	{
		return CancelTaxFreeFormInvocation.class;
	}
	if ([contextName isEqual:@"CARDS_FOR_CUSTOMER"])
	{
		return CardsForCustomerInvocation.class;
	}
	if ([contextName isEqual:@"CHANGE_CASH_DRAWER"])
	{
		return ChangeCashDrawerInvocation.class;
	}
	if ([contextName isEqual:@"CHECKOUT_BASKET"])
	{
		return CheckoutBasketInvocation.class;
	}
	if ([contextName isEqual:@"CLIENTSETTINGS"])
	{
		return ClientSettings.class;
	}
	if ([contextName isEqual:@"CONFIGURATION"])
	{
		return Configuration.class;
	}
	if ([contextName isEqual:@"CONTROL_VOID_TRANSACTION"])
	{
		return ControlVoidTransactionInvocation.class;
	}
	if ([contextName isEqual:@"COUNTRY"])
	{
		return Country.class;
	}
	if ([contextName isEqual:@"CREATE_BASKET"])
	{
		return CreateBasketInvocation.class;
	}
	if ([contextName isEqual:@"CREATE_CUSTOMER_EMAIL"])
	{
		return CreateCustomerEmailInvocation.class;
	}
	if ([contextName isEqual:@"CREATE_EFT_TENDER"])
	{
		return CreateEFTTenderInvocation.class;
	}
	if ([contextName isEqual:@"CREATE_STANDARD_TENDER"])
	{
		return CreateStandardTenderInvocation.class;
	}
	if ([contextName isEqual:@"CREATE_VERIFONE_TENDER"])
	{
		return CreateVerifoneTenderInvocation.class;
	}
	if ([contextName isEqual:@"CREATE_YESPAY_TENDER"])
	{
		return CreateYespayTenderInvocation.class;
	}
	if ([contextName isEqual:@"CREDENTIALS"])
	{
		return Credentials.class;
	}
	if ([contextName isEqual:@"CUSTOMER"])
	{
		return Customer.class;
	}
	if ([contextName isEqual:@"CUSTOMERADDRESS"])
	{
		return CustomerAddress.class;
	}
	if ([contextName isEqual:@"CUSTOMERCARD"])
	{
		return CustomerCard.class;
	}
	if ([contextName isEqual:@"CUSTOMEREMAIL"])
	{
		return CustomerEmail.class;
	}
	if ([contextName isEqual:@"CUSTOMERIDENTIFICATIONTYPE"])
	{
		return CustomerIdentificationType.class;
	}
	if ([contextName isEqual:@"CUSTOMERSEARCH"])
	{
		return CustomerSearch.class;
	}
	if ([contextName isEqual:@"CUSTOMERS_WITH_EMAIL"])
	{
		return CustomersWithEmailInvocation.class;
	}
	if ([contextName isEqual:@"CUSTOMER_WITH_CARD_NO"])
	{
		return CustomerWithCardNoInvocation.class;
	}
	if ([contextName isEqual:@"CUSTOMER_WITH_CUSTOMER_ID"])
	{
		return CustomerWithCustomerIdInvocation.class;
	}
	if ([contextName isEqual:@"DATAEVENT"])
	{
		return DataEventDTO.class;
	}
	if ([contextName isEqual:@"DEVICE"])
	{
		return Device.class;
	}
	if ([contextName isEqual:@"DEVICEPROFILE"])
	{
		return DeviceProfile.class;
	}
	if ([contextName isEqual:@"DEVICESTATION"])
	{
		return DeviceStation.class;
	}
	if ([contextName isEqual:@"DEVICESTATIONPERIPHERAL"])
	{
		return DeviceStationPeripheral.class;
	}
	if ([contextName isEqual:@"DISASSOCIATE_DEVICE_STATION"])
	{
		return DisassociateDeviceStationInvocation.class;
	}
	if ([contextName isEqual:@"DISCOUNTCODE"])
	{
		return DiscountCode.class;
	}
	if ([contextName isEqual:@"DISCOUNTREASON"])
	{
		return DiscountReason.class;
	}
	if ([contextName isEqual:@"DISCOUNTREASONGROUP"])
	{
		return DiscountReasonGroup.class;
	}
	if ([contextName isEqual:@"DUMPCODE"])
	{
		return DumpCode.class;
	}
	if ([contextName isEqual:@"EFTPAYMENT"])
	{
		return EFTPayment.class;
	}
	if ([contextName isEqual:@"FIND_ITEM"])
	{
		return FindItemInvocation.class;
	}
	if ([contextName isEqual:@"FIND_LINE_ID"])
	{
		return FindLineIdInvocation.class;
	}
	if ([contextName isEqual:@"FINISH_BASKET"])
	{
		return FinishBasketInvocation.class;
	}
	if ([contextName isEqual:@"FINISH_TAX_FREE_BASKET"])
	{
		return FinishTaxFreeBasketInvocation.class;
	}
	if ([contextName isEqual:@"FORMCONFIGURATION"])
	{
		return FormConfigurationDTO.class;
	}
	if ([contextName isEqual:@"GET_ACTIVE_BASKET"])
	{
		return GetActiveBasketInvocation.class;
	}
	if ([contextName isEqual:@"GET_ASSOCIATE_SALES_HISTORY"])
	{
		return GetAssociateSalesHistoryInvocation.class;
	}
	if ([contextName isEqual:@"GET_BASKET"])
	{
		return GetBasketInvocation.class;
	}
	if ([contextName isEqual:@"GET_CONFIGURATION"])
	{
		return GetConfigurationInvocation.class;
	}
	if ([contextName isEqual:@"GET_DEVICE_STATION"])
	{
		return GetDeviceStationInvocation.class;
	}
	if ([contextName isEqual:@"GET_DEVICE_STATIONS"])
	{
		return GetDeviceStationsInvocation.class;
	}
	if ([contextName isEqual:@"GET_ITEM_AVAILABILITY"])
	{
		return GetItemAvailabilityInvocation.class;
	}
	if ([contextName isEqual:@"GET_STORE_DETAILS"])
	{
		return GetStoreDetailsInvocation.class;
	}
	if ([contextName isEqual:@"GIFTRECEIPTREQUEST"])
	{
		return GiftReceiptRequest.class;
	}
	if ([contextName isEqual:@"INTERNET_RETURN"])
	{
		return InternetReturnInvocation.class;
	}
	if ([contextName isEqual:@"ITEM"])
	{
		return Item.class;
	}
	if ([contextName isEqual:@"ITEMAVAILABILITY"])
	{
		return ItemAvailability.class;
	}
	if ([contextName isEqual:@"ITEMAVAILABILITYRECORD"])
	{
		return ItemAvailabilityRecord.class;
	}
	if ([contextName isEqual:@"ITEMIMAGES"])
	{
		return ItemImages.class;
	}
	if ([contextName isEqual:@"ITEMMODIFIER"])
	{
		return ItemModifier.class;
	}
	if ([contextName isEqual:@"ITEMMODIFIERGROUP"])
	{
		return ItemModifierGroup.class;
	}
	if ([contextName isEqual:@"ITEMPROPERTIES"])
	{
		return ItemProperties.class;
	}
	if ([contextName isEqual:@"KEYVALUE"])
	{
		return KeyValueDTO.class;
	}
	if ([contextName isEqual:@"KEYVALUEGROUP"])
	{
		return KeyValueGroup.class;
	}
	if ([contextName isEqual:@"KEYVALUES"])
	{
		return KeyValues.class;
	}
	if ([contextName isEqual:@"LANGUAGE"])
	{
		return Language.class;
	}
	if ([contextName isEqual:@"LINK_DEVICE_STATION"])
	{
		return LinkDeviceStationInvocation.class;
	}
	if ([contextName isEqual:@"LINKED_RETURN"])
	{
		return LinkedReturnInvocation.class;
	}
	if ([contextName isEqual:@"LIST_STORES"])
	{
		return ListStoresInvocation.class;
	}
	if ([contextName isEqual:@"LOCALIZABLE"])
	{
		return Localizable.class;
	}
	if ([contextName isEqual:@"LOCALIZABLETABLE"])
	{
		return LocalizableTable.class;
	}
	if ([contextName isEqual:@"LOGIN_ASSOCIATE"])
	{
		return LoginAssociateInvocation.class;
	}
	if ([contextName isEqual:@"LOG_MOBILE_CLIENT_CRASH_EVENT"])
	{
		return LogMobileClientCrashEventInvocation.class;
	}
	if ([contextName isEqual:@"LOGOUT_ASSOCIATE"])
	{
		return LogoutAssociateInvocation.class;
	}
	if ([contextName isEqual:@"LOOKUP_SALE"])
	{
		return LookupSaleInvocation.class;
	}
	if ([contextName isEqual:@"MANDATORYMODIFIERGROUP"])
	{
		return MandatoryModifierGroup.class;
	}
	if ([contextName isEqual:@"NO_RECEIPT"])
	{
		return NoReceiptInvocation.class;
	}
	if ([contextName isEqual:@"NOTES_FOR_CUSTOMER"])
	{
		return NotesForCustomerInvocation.class;
	}
	if ([contextName isEqual:@"OPEN_CASH_DRAWER"])
	{
		return OpenCashDrawerInvocation.class;
	}
	if ([contextName isEqual:@"OPTIONALMODIFIERGROUP"])
	{
		return OptionalModifierGroup.class;
	}
	if ([contextName isEqual:@"OVERRIDEERRORMESSAGE"])
	{
		return OverrideErrorMessage.class;
	}
	if ([contextName isEqual:@"PASSWORDRULE"])
	{
		return PasswordRule.class;
	}
	if ([contextName isEqual:@"PAYMENT"])
	{
		return Payment.class;
	}
	if ([contextName isEqual:@"PERMISSION"])
	{
		return Permission.class;
	}
	if ([contextName isEqual:@"PERMISSIONGROUP"])
	{
		return PermissionGroup.class;
	}
	if ([contextName isEqual:@"POSERROR"])
	{
		return POSError.class;
	}
	if ([contextName isEqual:@"POSERRORMESSAGE"])
	{
		return POSErrorMessage.class;
	}
	if ([contextName isEqual:@"POSFUNCTIONSOFTKEYEVENT"])
	{
		return POSFunctionSoftKeyEventDTO.class;
	}
	if ([contextName isEqual:@"PRINTDATALINE"])
	{
		return PrintDataLine.class;
	}
	if ([contextName isEqual:@"PRINTELEMENT"])
	{
		return PrintElement.class;
	}
	if ([contextName isEqual:@"PRODUCTSEARCH"])
	{
		return ProductSearch.class;
	}
	if ([contextName isEqual:@"PROPERTY"])
	{
		return Property.class;
	}
	if ([contextName isEqual:@"REGISTER_CUSTOMER"])
	{
		return RegisterCustomerInvocation.class;
	}
	if ([contextName isEqual:@"REMOVE_BASKET_DISCOUNT"])
	{
		return RemoveBasketDiscountInvocation.class;
	}
	if ([contextName isEqual:@"REMOVE_BASKET_ITEM"])
	{
		return RemoveBasketItemInvocation.class;
	}
	if ([contextName isEqual:@"REMOVE_BASKET_ITEM_DISCOUNT"])
	{
		return RemoveBasketItemDiscountInvocation.class;
	}
	if ([contextName isEqual:@"REPRINT_LAST_TX"])
	{
		return ReprintLastTxInvocation.class;
	}
	if ([contextName isEqual:@"RESUME_BASKET"])
	{
		return ResumeBasketInvocation.class;
	}
	if ([contextName isEqual:@"RETURNABLESALEITEM"])
	{
		return ReturnableSaleItem.class;
	}
	if ([contextName isEqual:@"RETURN_BASKET_ITEM"])
	{
		return ReturnBasketItemInvocation.class;
	}
	if ([contextName isEqual:@"RETURNINSTANCE"])
	{
		return ReturnInstance.class;
	}
	if ([contextName isEqual:@"RETURNITEM"])
	{
		return ReturnItem.class;
	}
	if ([contextName isEqual:@"RETURN_WITH_context"])
	{
		return ReturnWithTypeInvocation.class;
	}
	if ([contextName isEqual:@"SALEITEM"])
	{
		return SaleItem.class;
	}
	if ([contextName isEqual:@"SALESPERSON"])
	{
		return SalesPerson.class;
	}
	if ([contextName isEqual:@"SEARCH_ITEMS"])
	{
		return SearchItemsInvocation.class;
	}
	if ([contextName isEqual:@"SELECTEDMODIFIER"])
	{
		return SelectedModifier.class;
	}
	if ([contextName isEqual:@"SERVERERROR"])
	{
		return ServerError.class;
	}
	if ([contextName isEqual:@"SERVERERRORAFFILIATIONREQUIREDMESSAGE"])
	{
		return ServerErrorAffiliationRequiredMessage.class;
	}
	if ([contextName isEqual:@"SERVERERRORAGERESTRICTIONMESSAGE"])
	{
		return ServerErrorAgeRestrictionMessage.class;
	}
	if ([contextName isEqual:@"SERVERERRORBARCODEITEMNOTRETURNEDMESSAGE"])
	{
		return ServerErrorBarcodeItemNotReturnedMessage.class;
	}
	if ([contextName isEqual:@"SERVERERRORDEVICESTATIONCLAIMEDMESSAGE"])
	{
		return ServerErrorDeviceStationClaimedMessage.class;
	}
	if ([contextName isEqual:@"SERVERERRORDEVICESTATIONMESSAGE"])
	{
		return ServerErrorDeviceStationMessage.class;
	}
	if ([contextName isEqual:@"SERVERERRORMESSAGE"])
	{
		return ServerErrorMessage.class;
	}
	if ([contextName isEqual:@"SERVERERRORUNITOFMEASUREMESSAGE"])
	{
		return ServerErrorUnitOfMeasureMessage.class;
	}
	if ([contextName isEqual:@"SERVERREQUEST"])
	{
		return ServerRequest.class;
	}
	if ([contextName isEqual:@"SET_NOTES_FOR_CUSTOMER"])
	{
		return SetNotesForCustomerInvocation.class;
	}
	if ([contextName isEqual:@"SHOPPERDETAILS"])
	{
		return ShopperDetailsDTO.class;
	}
	if ([contextName isEqual:@"SOFTKEY"])
	{
		return SoftKey.class;
	}
	if ([contextName isEqual:@"SOFTKEYCONTEXT"])
	{
		return SoftKeyContext.class;
	}
	if ([contextName isEqual:@"SOFTKEYEVENT"])
	{
		return SoftKeyEventDTO.class;
	}
	if ([contextName isEqual:@"SOFTKEYINPUTEVENT"])
	{
		return SoftKeyInputEventDTO.class;
	}
	if ([contextName isEqual:@"STANDARDPAYMENT"])
	{
		return StandardPayment.class;
	}
	if ([contextName isEqual:@"STORE"])
	{
		return Store.class;
	}
	if ([contextName isEqual:@"STORES"])
	{
		return Stores.class;
	}
	if ([contextName isEqual:@"SUSPEND_BASKET"])
	{
		return SuspendBasketInvocation.class;
	}
	if ([contextName isEqual:@"TENDER"])
	{
		return Tender.class;
	}
	if ([contextName isEqual:@"TILL"])
	{
		return Till.class;
	}
	if ([contextName isEqual:@"TOGGLE_TRAINING_MODE"])
	{
		return ToggleTrainingModeInvocation.class;
	}
	if ([contextName isEqual:@"UICONTEXTEVENT"])
	{
		return UIContextEventDTO.class;
	}
	if ([contextName isEqual:@"UNLINK_DEVICE"])
	{
		return UnlinkDeviceInvocation.class;
	}
	if ([contextName isEqual:@"UNLINKED_RETURN"])
	{
		return UnlinkedReturnInvocation.class;
	}
	if ([contextName isEqual:@"UPDATE_CUSTOMER"])
	{
		return UpdateCustomerInvocation.class;
	}
	if ([contextName isEqual:@"UPDATE_DEVICE"])
	{
		return UpdateDeviceInvocation.class;
	}
	if ([contextName isEqual:@"UPDATE_DEVICE_STATION"])
	{
		return UpdateDeviceStationInvocation.class;
	}
	if ([contextName isEqual:@"UPDATE_GIFT_RECEIPT"])
	{
		return UpdateGiftReceiptInvocation.class;
	}
	if ([contextName isEqual:@"VERIFONEPAYMENT"])
	{
		return VerifonePayment.class;
	}
	if ([contextName isEqual:@"YESPAYPAYMENT"])
	{
		return YespayPayment.class;
	}
	return Nil;
}

@end

@implementation AddItemInvocation

- (NSString*)RESTMethod
{
	return @"POST";
}

- (NSString*)RESTPath
{
	return @"/private/secure/basketItem";
}

- (NSString*)RESTReturnType
{
	return @"BasketAssociateDTO";
}

-(NSDictionary*)RESTParameterTypes
{
	NSMutableDictionary *parameterTypes = [NSMutableDictionary dictionaryWithDictionary:[super RESTParameterTypes]];
	parameterTypes[@"ageRestriction"] = @"NSString";
	parameterTypes[@"barcodeType"] = @"NSString";
	parameterTypes[@"basketID"] = @"NSString";
	parameterTypes[@"entryMethod"] = @"NSString";
	parameterTypes[@"itemDescription"] = @"NSArray";
	parameterTypes[@"itemScanID"] = @"NSString";
	parameterTypes[@"itemSerialNumber"] = @"NSString";
	parameterTypes[@"measure"] = @"NSString";
	parameterTypes[@"originalItemDescription"] = @"NSString";
	parameterTypes[@"originalItemScanId"] = @"NSString";
	parameterTypes[@"price"] = @"NSString";
	parameterTypes[@"quantity"] = @"NSString";
	parameterTypes[@"track1"] = @"NSString";
	parameterTypes[@"track2"] = @"NSString";
	parameterTypes[@"track3"] = @"NSString";
	return parameterTypes;
}
@end

@implementation AddItemsInvocation

- (NSString*)RESTMethod
{
	return @"POST";
}

- (NSString*)RESTPath
{
	return @"/private/secure/basketItem";
}

- (NSString*)RESTReturnType
{
	return @"BasketAssociateDTO";
}

-(NSDictionary*)RESTParameterTypes
{
	NSMutableDictionary *parameterTypes = [NSMutableDictionary dictionaryWithDictionary:[super RESTParameterTypes]];
	parameterTypes[@"basketID"] = @"NSString";
	parameterTypes[@"entryMethod"] = @"NSString";
	parameterTypes[@"itemDescription"] = @"NSArray";
	parameterTypes[@"itemScanID"] = @"NSArray";
	return parameterTypes;
}
@end

@implementation AdjustBasketItemInvocation

- (NSString*)RESTMethod
{
	return @"POST";
}

- (NSString*)RESTPath
{
	return @"/private/secure/basketItem/salesperson";
}

- (NSString*)RESTReturnType
{
	return @"BasketAssociateDTO";
}

-(NSDictionary*)RESTParameterTypes
{
	NSMutableDictionary *parameterTypes = [NSMutableDictionary dictionaryWithDictionary:[super RESTParameterTypes]];
	parameterTypes[@"basketID"] = @"NSString";
	parameterTypes[@"basketItemID"] = @"NSArray";
	parameterTypes[@"operatorID"] = @"NSString";
	return parameterTypes;
}
@end

@implementation AdjustBasketItemPriceInvocation

- (NSString*)RESTMethod
{
	return @"POST";
}

- (NSString*)RESTPath
{
	return @"/private/secure/basketItem/price";
}

- (NSString*)RESTReturnType
{
	return @"BasketAssociateDTO";
}

-(NSDictionary*)RESTParameterTypes
{
	NSMutableDictionary *parameterTypes = [NSMutableDictionary dictionaryWithDictionary:[super RESTParameterTypes]];
	parameterTypes[@"basketID"] = @"NSString";
	parameterTypes[@"basketItemID"] = @"NSString";
	parameterTypes[@"price"] = @"NSString";
	parameterTypes[@"reasonDescription"] = @"NSString";
	parameterTypes[@"reasonID"] = @"NSString";
	return parameterTypes;
}
@end

@implementation AdjustBasketItemQuantityInvocation

- (NSString*)RESTMethod
{
	return @"POST";
}

- (NSString*)RESTPath
{
	return @"/private/secure/basketItem/quantity";
}

- (NSString*)RESTReturnType
{
	return @"BasketAssociateDTO";
}

-(NSDictionary*)RESTParameterTypes
{
	NSMutableDictionary *parameterTypes = [NSMutableDictionary dictionaryWithDictionary:[super RESTParameterTypes]];
	parameterTypes[@"basketID"] = @"NSString";
	parameterTypes[@"basketItemID"] = @"NSString";
	parameterTypes[@"quantity"] = @"NSInteger";
	return parameterTypes;
}
@end

@implementation AdjustSaleInvocation

- (NSString*)RESTMethod
{
	return @"POST";
}

- (NSString*)RESTPath
{
	return @"/private/secure/basket/adjust";
}

- (NSString*)RESTBodyParameter
{
	return @"basket";
}

- (NSString*)RESTReturnType
{
	return @"BasketDTO";
}

-(NSDictionary*)RESTParameterTypes
{
	NSMutableDictionary *parameterTypes = [NSMutableDictionary dictionaryWithDictionary:[super RESTParameterTypes]];
	parameterTypes[@"basket"] = @"BasketDTO";
	return parameterTypes;
}
@end

@implementation AdjustTaxFreeFormInvocation

- (NSString*)RESTMethod
{
	return @"POST";
}

- (NSString*)RESTPath
{
	return @"/private/secure/basket/adjust";
}

- (NSString*)RESTBodyParameter
{
	return @"basket";
}

- (NSString*)RESTReturnType
{
	return @"BasketTaxFreeDocumentDTO";
}

-(NSDictionary*)RESTParameterTypes
{
	NSMutableDictionary *parameterTypes = [NSMutableDictionary dictionaryWithDictionary:[super RESTParameterTypes]];
	parameterTypes[@"basket"] = @"BasketTaxFreeDocumentDTO";
	return parameterTypes;
}
@end

@implementation ApplyBasketDiscountInvocation

- (NSString*)RESTMethod
{
	return @"POST";
}

- (NSString*)RESTPath
{
	return @"/private/secure/basket/discount";
}

- (NSString*)RESTReturnType
{
	return @"BasketAssociateDTO";
}

-(NSDictionary*)RESTParameterTypes
{
	NSMutableDictionary *parameterTypes = [NSMutableDictionary dictionaryWithDictionary:[super RESTParameterTypes]];
	parameterTypes[@"amount"] = @"NSString";
	parameterTypes[@"basketID"] = @"NSString";
	parameterTypes[@"discountCode"] = @"NSString";
	parameterTypes[@"discountType"] = @"NSString";
	parameterTypes[@"reasonDescription"] = @"NSString";
	parameterTypes[@"reasonID"] = @"NSString";
	return parameterTypes;
}
@end

@implementation ApplyBasketItemDiscountInvocation

- (NSString*)RESTMethod
{
	return @"POST";
}

- (NSString*)RESTPath
{
	return @"/private/secure/basketItem/discount";
}

- (NSString*)RESTReturnType
{
	return @"BasketAssociateDTO";
}

-(NSDictionary*)RESTParameterTypes
{
	NSMutableDictionary *parameterTypes = [NSMutableDictionary dictionaryWithDictionary:[super RESTParameterTypes]];
	parameterTypes[@"amount"] = @"NSString";
	parameterTypes[@"basketID"] = @"NSString";
	parameterTypes[@"basketItemID"] = @"NSArray";
	parameterTypes[@"discountCode"] = @"NSString";
	parameterTypes[@"discountType"] = @"NSString";
	parameterTypes[@"reasonDescription"] = @"NSString";
	parameterTypes[@"reasonID"] = @"NSString";
	return parameterTypes;
}
@end

@implementation AssignCustomerInvocation

- (NSString*)RESTMethod
{
	return @"POST";
}

- (NSString*)RESTPath
{
	return @"/private/secure/basket/customer/assign";
}

- (NSString*)RESTReturnType
{
	return @"BasketAssociateDTO";
}

-(NSDictionary*)RESTParameterTypes
{
	NSMutableDictionary *parameterTypes = [NSMutableDictionary dictionaryWithDictionary:[super RESTParameterTypes]];
	parameterTypes[@"affiliationType"] = @"NSString";
	parameterTypes[@"basketID"] = @"NSString";
	parameterTypes[@"customer"] = @"NSString";
	return parameterTypes;
}
@end

@implementation AssignEmailAddressInvocation

- (NSString*)RESTMethod
{
	return @"POST";
}

- (NSString*)RESTPath
{
	return @"/private/secure/basket/email";
}

- (NSString*)RESTReturnType
{
	return @"BasketAssociateDTO";
}

-(NSDictionary*)RESTParameterTypes
{
	NSMutableDictionary *parameterTypes = [NSMutableDictionary dictionaryWithDictionary:[super RESTParameterTypes]];
	parameterTypes[@"basketID"] = @"NSString";
	parameterTypes[@"emailAddress"] = @"NSString";
	return parameterTypes;
}
@end

@implementation AssociateDeviceStationInvocation

- (NSString*)RESTMethod
{
	return @"POST";
}

- (NSString*)RESTPath
{
	return @"/private/secure/devicestation/{deviceStationId}/till";
}

- (NSString*)RESTReturnType
{
	return @"DeviceStation";
}

-(NSDictionary*)RESTParameterTypes
{
	NSMutableDictionary *parameterTypes = [NSMutableDictionary dictionaryWithDictionary:[super RESTParameterTypes]];
	parameterTypes[@"deviceStationId"] = @"NSString";
	parameterTypes[@"tillId"] = @"NSString";
	return parameterTypes;
}
@end

@implementation AssociateTextReceiptInvocation

- (NSString*)RESTMethod
{
	return @"GET";
}

- (NSString*)RESTPath
{
	return @"/private/secure/receipt/paper";
}

- (NSString*)RESTReturnType
{
	return @"NSString";
}

-(NSDictionary*)RESTParameterTypes
{
	NSMutableDictionary *parameterTypes = [NSMutableDictionary dictionaryWithDictionary:[super RESTParameterTypes]];
	parameterTypes[@"receiptId"] = @"NSString";
	return parameterTypes;
}
@end

@implementation CancelBasketInvocation

- (NSString*)RESTMethod
{
	return @"POST";
}

- (NSString*)RESTPath
{
	return @"/private/secure/basket/cancel";
}

- (NSString*)RESTReturnType
{
	return @"BasketAssociateDTO";
}

-(NSDictionary*)RESTParameterTypes
{
	NSMutableDictionary *parameterTypes = [NSMutableDictionary dictionaryWithDictionary:[super RESTParameterTypes]];
	parameterTypes[@"basketID"] = @"NSString";
	return parameterTypes;
}
@end

@implementation CancelCheckoutBasketInvocation

- (NSString*)RESTMethod
{
	return @"POST";
}

- (NSString*)RESTPath
{
	return @"/private/secure/basket/checkout/cancel";
}

- (NSString*)RESTReturnType
{
	return @"BasketAssociateDTO";
}

-(NSDictionary*)RESTParameterTypes
{
	NSMutableDictionary *parameterTypes = [NSMutableDictionary dictionaryWithDictionary:[super RESTParameterTypes]];
	parameterTypes[@"basketID"] = @"NSString";
	return parameterTypes;
}
@end

@implementation CancelTaxFreeFormInvocation

- (NSString*)RESTMethod
{
	return @"POST";
}

- (NSString*)RESTPath
{
	return @"/private/secure/taxfree/cancel";
}

- (NSString*)RESTReturnType
{
	return @"BasketTaxFreeDocumentDTO";
}

-(NSDictionary*)RESTParameterTypes
{
	NSMutableDictionary *parameterTypes = [NSMutableDictionary dictionaryWithDictionary:[super RESTParameterTypes]];
	parameterTypes[@"basketID"] = @"NSString";
	return parameterTypes;
}
@end

@implementation CardsForCustomerInvocation

- (NSString*)RESTMethod
{
	return @"GET";
}

- (NSString*)RESTPath
{
	return @"/private/secure/customer/{customerId}/cards";
}

- (NSString*)RESTReturnType
{
	return @"NSArray";
}

-(NSDictionary*)RESTParameterTypes
{
	NSMutableDictionary *parameterTypes = [NSMutableDictionary dictionaryWithDictionary:[super RESTParameterTypes]];
	parameterTypes[@"customerId"] = @"NSString";
	return parameterTypes;
}
@end

@implementation ChangeCashDrawerInvocation

- (NSString*)RESTMethod
{
	return @"POST";
}

- (NSString*)RESTPath
{
	return @"/private/secure/control/drawerchange";
}

-(NSDictionary*)RESTParameterTypes
{
	NSMutableDictionary *parameterTypes = [NSMutableDictionary dictionaryWithDictionary:[super RESTParameterTypes]];
	parameterTypes[@"deviceStationId"] = @"NSString";
	return parameterTypes;
}
@end

@implementation CheckoutBasketInvocation

- (NSString*)RESTMethod
{
	return @"POST";
}

- (NSString*)RESTPath
{
	return @"/private/secure/basket/checkout";
}

- (NSString*)RESTReturnType
{
	return @"BasketAssociateDTO";
}

-(NSDictionary*)RESTParameterTypes
{
	NSMutableDictionary *parameterTypes = [NSMutableDictionary dictionaryWithDictionary:[super RESTParameterTypes]];
	parameterTypes[@"basketID"] = @"NSString";
	parameterTypes[@"currencyCode"] = @"NSString";
	return parameterTypes;
}
@end

@implementation ControlVoidTransactionInvocation

- (NSString*)RESTMethod
{
	return @"POST";
}

- (NSString*)RESTPath
{
	return @"/private/secure/{transactionType}/void";
}

- (NSString*)RESTReturnType
{
	return @"id";
}

-(NSDictionary*)RESTParameterTypes
{
	NSMutableDictionary *parameterTypes = [NSMutableDictionary dictionaryWithDictionary:[super RESTParameterTypes]];
	parameterTypes[@"basketID"] = @"NSString";
	parameterTypes[@"businessDay"] = @"NSDate";
	parameterTypes[@"confirmedAmount"] = @"NSString";
	parameterTypes[@"scanData"] = @"NSString";
	parameterTypes[@"storeID"] = @"NSString";
	parameterTypes[@"transactionType"] = @"NSString";
	parameterTypes[@"workstationID"] = @"NSString";
	return parameterTypes;
}
@end

@implementation CreateBasketInvocation

- (NSString*)RESTMethod
{
	return @"POST";
}

- (NSString*)RESTPath
{
	return @"/private/secure/{transactionType}";
}

- (NSString*)RESTReturnType
{
	return @"id";
}

-(NSDictionary*)RESTParameterTypes
{
	NSMutableDictionary *parameterTypes = [NSMutableDictionary dictionaryWithDictionary:[super RESTParameterTypes]];
	parameterTypes[@"basketID"] = @"NSString";
	parameterTypes[@"businessDay"] = @"NSDate";
	parameterTypes[@"deviceID"] = @"NSString";
	parameterTypes[@"issuerCountryId"] = @"NSString";
	parameterTypes[@"lastTransaction"] = @"NSString";
	parameterTypes[@"scanData"] = @"NSString";
	parameterTypes[@"storeID"] = @"NSString";
	parameterTypes[@"transactionType"] = @"NSString";
	parameterTypes[@"workstationID"] = @"NSString";
	return parameterTypes;
}
@end

@implementation CreateCustomerEmailInvocation

- (NSString*)RESTMethod
{
	return @"POST";
}

- (NSString*)RESTPath
{
	return @"/private/secure/email";
}

-(NSDictionary*)RESTParameterTypes
{
	NSMutableDictionary *parameterTypes = [NSMutableDictionary dictionaryWithDictionary:[super RESTParameterTypes]];
	parameterTypes[@"emailAddress"] = @"NSString";
	parameterTypes[@"includeGiftReceipt"] = @"BOOL";
	parameterTypes[@"receiptID"] = @"NSString";
	return parameterTypes;
}
@end

@implementation CreateEFTTenderInvocation

- (NSString*)RESTMethod
{
	return @"POST";
}

- (NSString*)RESTPath
{
	return @"/private/secure/sales/payment/eft";
}

- (NSString*)RESTBodyParameter
{
	return @"tender";
}

- (NSString*)RESTReturnType
{
	return @"BasketAssociateDTO";
}

-(NSDictionary*)RESTParameterTypes
{
	NSMutableDictionary *parameterTypes = [NSMutableDictionary dictionaryWithDictionary:[super RESTParameterTypes]];
	parameterTypes[@"deviceStationId"] = @"NSString";
	parameterTypes[@"tender"] = @"EFTPayment";
	return parameterTypes;
}
@end

@implementation CreateStandardTenderInvocation

- (NSString*)RESTMethod
{
	return @"POST";
}

- (NSString*)RESTPath
{
	return @"/private/secure/sales/payment";
}

- (NSString*)RESTBodyParameter
{
	return @"tender";
}

- (NSString*)RESTReturnType
{
	return @"BasketAssociateDTO";
}

-(NSDictionary*)RESTParameterTypes
{
	NSMutableDictionary *parameterTypes = [NSMutableDictionary dictionaryWithDictionary:[super RESTParameterTypes]];
	parameterTypes[@"tender"] = @"StandardPayment";
	return parameterTypes;
}
@end

@implementation CreateVerifoneTenderInvocation

- (NSString*)RESTMethod
{
	return @"POST";
}

- (NSString*)RESTPath
{
	return @"/private/secure/sales/payment/verifone";
}

- (NSString*)RESTBodyParameter
{
	return @"tender";
}

- (NSString*)RESTReturnType
{
	return @"BasketAssociateDTO";
}

-(NSDictionary*)RESTParameterTypes
{
	NSMutableDictionary *parameterTypes = [NSMutableDictionary dictionaryWithDictionary:[super RESTParameterTypes]];
	parameterTypes[@"tender"] = @"VerifonePayment";
	return parameterTypes;
}
@end

@implementation CreateYespayTenderInvocation

- (NSString*)RESTMethod
{
	return @"POST";
}

- (NSString*)RESTPath
{
	return @"/private/secure/sales/payment/yespay";
}

- (NSString*)RESTBodyParameter
{
	return @"tender";
}

- (NSString*)RESTReturnType
{
	return @"BasketAssociateDTO";
}

-(NSDictionary*)RESTParameterTypes
{
	NSMutableDictionary *parameterTypes = [NSMutableDictionary dictionaryWithDictionary:[super RESTParameterTypes]];
	parameterTypes[@"tender"] = @"YespayPayment";
	return parameterTypes;
}
@end

@implementation CustomersWithEmailInvocation

- (NSString*)RESTMethod
{
	return @"GET";
}

- (NSString*)RESTPath
{
	return @"/private/secure/customer";
}

- (NSString*)RESTReturnType
{
	return @"NSArray";
}

-(NSDictionary*)RESTParameterTypes
{
	NSMutableDictionary *parameterTypes = [NSMutableDictionary dictionaryWithDictionary:[super RESTParameterTypes]];
	parameterTypes[@"email"] = @"NSString";
	return parameterTypes;
}
@end

@implementation CustomerWithCardNoInvocation

- (NSString*)RESTMethod
{
	return @"GET";
}

- (NSString*)RESTPath
{
	return @"/private/secure/customer";
}

- (NSString*)RESTReturnType
{
	return @"Customer";
}

-(NSDictionary*)RESTParameterTypes
{
	NSMutableDictionary *parameterTypes = [NSMutableDictionary dictionaryWithDictionary:[super RESTParameterTypes]];
	parameterTypes[@"cardNo"] = @"NSString";
	parameterTypes[@"cardType"] = @"NSString";
	return parameterTypes;
}
@end

@implementation CustomerWithCustomerIdInvocation

- (NSString*)RESTMethod
{
	return @"GET";
}

- (NSString*)RESTPath
{
	return @"/private/secure/customer/{customerId}";
}

- (NSString*)RESTReturnType
{
	return @"Customer";
}

-(NSDictionary*)RESTParameterTypes
{
	NSMutableDictionary *parameterTypes = [NSMutableDictionary dictionaryWithDictionary:[super RESTParameterTypes]];
	parameterTypes[@"customerId"] = @"NSString";
	return parameterTypes;
}
@end

@implementation DisassociateDeviceStationInvocation

- (NSString*)RESTMethod
{
	return @"DELETE";
}

- (NSString*)RESTPath
{
	return @"/private/secure/devicestation/{deviceStationId}/till";
}

- (NSString*)RESTReturnType
{
	return @"DeviceStation";
}

-(NSDictionary*)RESTParameterTypes
{
	NSMutableDictionary *parameterTypes = [NSMutableDictionary dictionaryWithDictionary:[super RESTParameterTypes]];
	parameterTypes[@"deviceStationId"] = @"NSString";
	parameterTypes[@"tillId"] = @"NSString";
	return parameterTypes;
}
@end

@implementation FindItemInvocation

- (NSString*)RESTMethod
{
	return @"GET";
}

- (NSString*)RESTPath
{
	return @"/private/item/{scanID}";
}

- (NSString*)RESTReturnType
{
	return @"Item";
}

-(NSDictionary*)RESTParameterTypes
{
	NSMutableDictionary *parameterTypes = [NSMutableDictionary dictionaryWithDictionary:[super RESTParameterTypes]];
	parameterTypes[@"scanID"] = @"NSString";
	parameterTypes[@"storeID"] = @"NSString";
	return parameterTypes;
}
@end

@implementation FindLineIdInvocation

- (NSString*)RESTMethod
{
	return @"GET";
}

- (NSString*)RESTPath
{
	return @"/private/secure/basket/return/sku";
}

- (NSString*)RESTReturnType
{
	return @"ReturnableSaleItem";
}

-(NSDictionary*)RESTParameterTypes
{
	NSMutableDictionary *parameterTypes = [NSMutableDictionary dictionaryWithDictionary:[super RESTParameterTypes]];
	parameterTypes[@"itemScanId"] = @"NSString";
	parameterTypes[@"originalBasketId"] = @"NSString";
	parameterTypes[@"originalStoreId"] = @"NSString";
	parameterTypes[@"originalTillNumber"] = @"NSString";
	parameterTypes[@"originalTransactionDate"] = @"NSString";
	return parameterTypes;
}
@end

@implementation FinishBasketInvocation

- (NSString*)RESTMethod
{
	return @"POST";
}

- (NSString*)RESTPath
{
	return @"/private/secure/basket/finish";
}

- (NSString*)RESTReturnType
{
	return @"BasketAssociateDTO";
}

-(NSDictionary*)RESTParameterTypes
{
	NSMutableDictionary *parameterTypes = [NSMutableDictionary dictionaryWithDictionary:[super RESTParameterTypes]];
	parameterTypes[@"basketID"] = @"NSString";
	parameterTypes[@"deviceStationId"] = @"NSString";
	parameterTypes[@"emailAddress"] = @"NSString";
	parameterTypes[@"emailReceipt"] = @"BOOL";
	parameterTypes[@"enablePrint"] = @"BOOL";
	parameterTypes[@"printSummaryReceipt"] = @"BOOL";
	parameterTypes[@"tillNumber"] = @"NSString";
	return parameterTypes;
}
@end

@implementation FinishTaxFreeBasketInvocation

- (NSString*)RESTMethod
{
	return @"POST";
}

- (NSString*)RESTPath
{
	return @"/private/secure/taxfree/finish";
}

- (NSString*)RESTReturnType
{
	return @"BasketTaxFreeDocumentDTO";
}

-(NSDictionary*)RESTParameterTypes
{
	NSMutableDictionary *parameterTypes = [NSMutableDictionary dictionaryWithDictionary:[super RESTParameterTypes]];
	parameterTypes[@"basketID"] = @"NSString";
	parameterTypes[@"deviceStationId"] = @"NSString";
	parameterTypes[@"emailAddress"] = @"NSString";
	parameterTypes[@"emailReceipt"] = @"BOOL";
	parameterTypes[@"enablePrint"] = @"BOOL";
	parameterTypes[@"printSummaryReceipt"] = @"BOOL";
	parameterTypes[@"tillNumber"] = @"NSString";
	return parameterTypes;
}
@end

@implementation GetActiveBasketInvocation

- (NSString*)RESTMethod
{
	return @"GET";
}

- (NSString*)RESTPath
{
	return @"/private/secure/basket/active";
}

- (NSString*)RESTReturnType
{
	return @"BasketAssociateDTO";
}

-(NSDictionary*)RESTParameterTypes
{
	NSMutableDictionary *parameterTypes = [NSMutableDictionary dictionaryWithDictionary:[super RESTParameterTypes]];
	parameterTypes[@"deviceID"] = @"NSString";
	return parameterTypes;
}
@end

@implementation GetAssociateSalesHistoryInvocation

- (NSString*)RESTMethod
{
	return @"GET";
}

- (NSString*)RESTPath
{
	return @"/private/secure/sales/list";
}

- (NSString*)RESTReturnType
{
	return @"NSArray";
}

-(NSDictionary*)RESTParameterTypes
{
	NSMutableDictionary *parameterTypes = [NSMutableDictionary dictionaryWithDictionary:[super RESTParameterTypes]];
	parameterTypes[@"associateId"] = @"NSString";
	parameterTypes[@"from"] = @"NSDate";
	parameterTypes[@"locationId"] = @"NSString";
	parameterTypes[@"status"] = @"NSString";
	parameterTypes[@"to"] = @"NSDate";
	return parameterTypes;
}
@end

@implementation GetBasketInvocation

- (NSString*)RESTMethod
{
	return @"GET";
}

- (NSString*)RESTPath
{
	return @"/private/secure/basket";
}

- (NSString*)RESTReturnType
{
	return @"BasketAssociateDTO";
}

-(NSDictionary*)RESTParameterTypes
{
	NSMutableDictionary *parameterTypes = [NSMutableDictionary dictionaryWithDictionary:[super RESTParameterTypes]];
	parameterTypes[@"basketID"] = @"NSString";
	return parameterTypes;
}
@end

@implementation GetConfigurationInvocation

- (NSString*)RESTMethod
{
	return @"GET";
}

- (NSString*)RESTPath
{
	return @"/private/configuration";
}

- (NSString*)RESTReturnType
{
	return @"Configuration";
}

- (NSTimeInterval)RESTTimeoutInterval
{
	return 120;
}

-(NSDictionary*)RESTParameterTypes
{
	NSMutableDictionary *parameterTypes = [NSMutableDictionary dictionaryWithDictionary:[super RESTParameterTypes]];
	parameterTypes[@"clearServerCache"] = @"BOOL";
	parameterTypes[@"storeID"] = @"NSString";
	parameterTypes[@"tillNumber"] = @"NSString";
	return parameterTypes;
}
@end

@implementation GetDeviceStationInvocation

- (NSString*)RESTMethod
{
	return @"GET";
}

- (NSString*)RESTPath
{
	return @"/private/secure/devicestation/{deviceStationId}";
}

- (NSString*)RESTReturnType
{
	return @"DeviceStation";
}

-(NSDictionary*)RESTParameterTypes
{
	NSMutableDictionary *parameterTypes = [NSMutableDictionary dictionaryWithDictionary:[super RESTParameterTypes]];
	parameterTypes[@"deviceStationId"] = @"NSString";
	return parameterTypes;
}
@end

@implementation GetDeviceStationsInvocation

- (NSString*)RESTMethod
{
	return @"GET";
}

- (NSString*)RESTPath
{
	return @"/private/devicestation/devices";
}

- (NSString*)RESTReturnType
{
	return @"NSArray";
}

-(NSDictionary*)RESTParameterTypes
{
	NSMutableDictionary *parameterTypes = [NSMutableDictionary dictionaryWithDictionary:[super RESTParameterTypes]];
	parameterTypes[@"deviceType"] = @"NSString";
	parameterTypes[@"storeId"] = @"NSString";
	return parameterTypes;
}
@end

@implementation GetItemAvailabilityInvocation

- (NSString*)RESTMethod
{
	return @"GET";
}

- (NSString*)RESTPath
{
	return @"/private/item/{scanID}/availability";
}

- (NSString*)RESTReturnType
{
	return @"NSArray";
}

-(NSDictionary*)RESTParameterTypes
{
	NSMutableDictionary *parameterTypes = [NSMutableDictionary dictionaryWithDictionary:[super RESTParameterTypes]];
	parameterTypes[@"scanID"] = @"NSString";
	parameterTypes[@"storeID"] = @"NSString";
	return parameterTypes;
}
@end

@implementation GetStoreDetailsInvocation

- (NSString*)RESTMethod
{
	return @"GET";
}

- (NSString*)RESTPath
{
	return @"/private/store/{storeId}";
}

- (NSString*)RESTReturnType
{
	return @"Store";
}

-(NSDictionary*)RESTParameterTypes
{
	NSMutableDictionary *parameterTypes = [NSMutableDictionary dictionaryWithDictionary:[super RESTParameterTypes]];
	parameterTypes[@"storeId"] = @"NSString";
	return parameterTypes;
}
@end

@implementation InternetReturnInvocation

- (NSString*)RESTMethod
{
	return @"POST";
}

- (NSString*)RESTPath
{
	return @"/private/secure/basket/basketItem/internetreturn";
}

- (NSString*)RESTReturnType
{
	return @"BasketAssociateDTO";
}

-(NSDictionary*)RESTParameterTypes
{
	NSMutableDictionary *parameterTypes = [NSMutableDictionary dictionaryWithDictionary:[super RESTParameterTypes]];
	parameterTypes[@"basketId"] = @"NSString";
	parameterTypes[@"itemScanId"] = @"NSString";
	parameterTypes[@"itemSerialNumber"] = @"NSString";
	parameterTypes[@"originalItemDescription"] = @"NSString";
	parameterTypes[@"originalItemScanId"] = @"NSString";
	parameterTypes[@"originalReferenceId"] = @"NSString";
	parameterTypes[@"price"] = @"NSString";
	parameterTypes[@"reasonDescription"] = @"NSString";
	parameterTypes[@"reasonId"] = @"NSString";
	return parameterTypes;
}
@end

@implementation LinkDeviceStationInvocation

- (NSString*)RESTMethod
{
	return @"POST";
}

- (NSString*)RESTPath
{
	return @"/private/secure/devicestation/{deviceStationId}/link";
}

- (NSString*)RESTReturnType
{
	return @"DeviceStation";
}

-(NSDictionary*)RESTParameterTypes
{
	NSMutableDictionary *parameterTypes = [NSMutableDictionary dictionaryWithDictionary:[super RESTParameterTypes]];
	parameterTypes[@"deviceStationId"] = @"NSString";
	parameterTypes[@"force"] = @"BOOL";
	return parameterTypes;
}
@end

@implementation LinkedReturnInvocation

- (NSString*)RESTMethod
{
	return @"POST";
}

- (NSString*)RESTPath
{
	return @"/private/secure/basket/basketItem/linkedreturn";
}

- (NSString*)RESTReturnType
{
	return @"BasketAssociateDTO";
}

-(NSDictionary*)RESTParameterTypes
{
	NSMutableDictionary *parameterTypes = [NSMutableDictionary dictionaryWithDictionary:[super RESTParameterTypes]];
	parameterTypes[@"basketId"] = @"NSString";
	parameterTypes[@"itemScanId"] = @"NSString";
	parameterTypes[@"itemSerialNumber"] = @"NSString";
	parameterTypes[@"lineId"] = @"NSString";
	parameterTypes[@"originalItemDescription"] = @"NSString";
	parameterTypes[@"originalItemScanId"] = @"NSString";
	parameterTypes[@"originalTransactionDate"] = @"NSString";
	parameterTypes[@"originalTransactionId"] = @"NSString";
	parameterTypes[@"originalTransactionStoreId"] = @"NSString";
	parameterTypes[@"originalTransactionTill"] = @"NSString";
	parameterTypes[@"reasonDescription"] = @"NSString";
	parameterTypes[@"reasonId"] = @"NSString";
	return parameterTypes;
}
@end

@implementation ListStoresInvocation

- (NSString*)RESTMethod
{
	return @"GET";
}

- (NSString*)RESTPath
{
	return @"/private/store/list";
}

- (NSString*)RESTReturnType
{
	return @"Stores";
}

-(NSDictionary*)RESTParameterTypes
{
	NSMutableDictionary *parameterTypes = [NSMutableDictionary dictionaryWithDictionary:[super RESTParameterTypes]];
	parameterTypes[@"brandId"] = @"NSString";
	parameterTypes[@"isHomeStore"] = @"BOOL";
	parameterTypes[@"latitude"] = @"double";
	parameterTypes[@"longitude"] = @"double";
	return parameterTypes;
}
@end

@implementation LogMobileClientCrashEventInvocation

- (NSString*)RESTMethod
{
	return @"POST";
}

- (NSString*)RESTPath
{
	return @"/public/diagnostics/crashreport";
}

- (NSString*)RESTBodyParameter
{
	return @"crashText";
}

-(NSDictionary*)RESTParameterTypes
{
	NSMutableDictionary *parameterTypes = [NSMutableDictionary dictionaryWithDictionary:[super RESTParameterTypes]];
	parameterTypes[@"crashText"] = @"NSString";
	parameterTypes[@"crashTime"] = @"NSDate";
	parameterTypes[@"storeID"] = @"NSString";
	parameterTypes[@"version"] = @"NSString";
	parameterTypes[@"workStationId"] = @"NSString";
	return parameterTypes;
}
@end

@implementation LoginAssociateInvocation

- (NSString*)RESTMethod
{
	return @"POST";
}

- (NSString*)RESTPath
{
	return @"/private/auth";
}

- (NSString*)RESTBodyParameter
{
	return @"credentials";
}

- (NSString*)RESTReturnType
{
	return @"Credentials";
}

-(NSDictionary*)RESTParameterTypes
{
	NSMutableDictionary *parameterTypes = [NSMutableDictionary dictionaryWithDictionary:[super RESTParameterTypes]];
	parameterTypes[@"credentials"] = @"Credentials";
	parameterTypes[@"signOffExistingOperator"] = @"BOOL";
	return parameterTypes;
}
@end

@implementation LogoutAssociateInvocation

- (NSString*)RESTMethod
{
	return @"POST";
}

- (NSString*)RESTPath
{
	return @"/private/auth/logout";
}

- (NSString*)RESTBodyParameter
{
	return @"credentials";
}

- (NSString*)RESTReturnType
{
	return @"Credentials";
}

-(NSDictionary*)RESTParameterTypes
{
	NSMutableDictionary *parameterTypes = [NSMutableDictionary dictionaryWithDictionary:[super RESTParameterTypes]];
	parameterTypes[@"credentials"] = @"Credentials";
	return parameterTypes;
}
@end

@implementation LookupSaleInvocation

- (NSString*)RESTMethod
{
	return @"GET";
}

- (NSString*)RESTPath
{
	return @"/private/secure/basket/return";
}

- (NSString*)RESTReturnType
{
	return @"BasketAssociateDTO";
}

-(NSDictionary*)RESTParameterTypes
{
	NSMutableDictionary *parameterTypes = [NSMutableDictionary dictionaryWithDictionary:[super RESTParameterTypes]];
	parameterTypes[@"barcode"] = @"NSString";
	parameterTypes[@"originalBasketId"] = @"NSString";
	parameterTypes[@"originalStoreId"] = @"NSString";
	parameterTypes[@"originalTillNumber"] = @"NSString";
	parameterTypes[@"originalTransactionDate"] = @"NSString";
	return parameterTypes;
}
@end

@implementation NoReceiptInvocation

- (NSString*)RESTMethod
{
	return @"POST";
}

- (NSString*)RESTPath
{
	return @"/private/secure/basket/basketItem/noreceiptreturn";
}

- (NSString*)RESTReturnType
{
	return @"BasketAssociateDTO";
}

-(NSDictionary*)RESTParameterTypes
{
	NSMutableDictionary *parameterTypes = [NSMutableDictionary dictionaryWithDictionary:[super RESTParameterTypes]];
	parameterTypes[@"basketId"] = @"NSString";
	parameterTypes[@"itemScanId"] = @"NSString";
	parameterTypes[@"itemSerialNumber"] = @"NSString";
	parameterTypes[@"originalItemDescription"] = @"NSString";
	parameterTypes[@"originalItemScanId"] = @"NSString";
	parameterTypes[@"price"] = @"NSString";
	parameterTypes[@"reasonDescription"] = @"NSString";
	parameterTypes[@"reasonId"] = @"NSString";
	return parameterTypes;
}
@end

@implementation NotesForCustomerInvocation

- (NSString*)RESTMethod
{
	return @"GET";
}

- (NSString*)RESTPath
{
	return @"/private/secure/customer/{customerId}/notes";
}

- (NSString*)RESTReturnType
{
	return @"NSArray";
}

-(NSDictionary*)RESTParameterTypes
{
	NSMutableDictionary *parameterTypes = [NSMutableDictionary dictionaryWithDictionary:[super RESTParameterTypes]];
	parameterTypes[@"customerId"] = @"NSString";
	return parameterTypes;
}
@end

@implementation OpenCashDrawerInvocation

- (NSString*)RESTMethod
{
	return @"POST";
}

- (NSString*)RESTPath
{
	return @"/private/secure/control/draweropen";
}

-(NSDictionary*)RESTParameterTypes
{
	NSMutableDictionary *parameterTypes = [NSMutableDictionary dictionaryWithDictionary:[super RESTParameterTypes]];
	parameterTypes[@"deviceStationId"] = @"NSString";
	return parameterTypes;
}
@end

@implementation RegisterCustomerInvocation

- (NSString*)RESTMethod
{
	return @"POST";
}

- (NSString*)RESTPath
{
	return @"/private/secure/customer";
}

- (NSString*)RESTBodyParameter
{
	return @"customer";
}

- (NSString*)RESTReturnType
{
	return @"Customer";
}

-(NSDictionary*)RESTParameterTypes
{
	NSMutableDictionary *parameterTypes = [NSMutableDictionary dictionaryWithDictionary:[super RESTParameterTypes]];
	parameterTypes[@"customer"] = @"Customer";
	return parameterTypes;
}
@end

@implementation RemoveBasketDiscountInvocation

- (NSString*)RESTMethod
{
	return @"POST";
}

- (NSString*)RESTPath
{
	return @"/private/secure/basket/discount/remove";
}

- (NSString*)RESTReturnType
{
	return @"BasketAssociateDTO";
}

-(NSDictionary*)RESTParameterTypes
{
	NSMutableDictionary *parameterTypes = [NSMutableDictionary dictionaryWithDictionary:[super RESTParameterTypes]];
	parameterTypes[@"basketDiscountID"] = @"NSArray";
	parameterTypes[@"basketID"] = @"NSString";
	return parameterTypes;
}
@end

@implementation RemoveBasketItemInvocation

- (NSString*)RESTMethod
{
	return @"POST";
}

- (NSString*)RESTPath
{
	return @"/private/secure/basketItem/remove";
}

- (NSString*)RESTReturnType
{
	return @"BasketAssociateDTO";
}

-(NSDictionary*)RESTParameterTypes
{
	NSMutableDictionary *parameterTypes = [NSMutableDictionary dictionaryWithDictionary:[super RESTParameterTypes]];
	parameterTypes[@"basketID"] = @"NSString";
	parameterTypes[@"basketItemID"] = @"NSArray";
	parameterTypes[@"deviceStationId"] = @"NSString";
	return parameterTypes;
}
@end

@implementation RemoveBasketItemDiscountInvocation

- (NSString*)RESTMethod
{
	return @"POST";
}

- (NSString*)RESTPath
{
	return @"/private/secure/basketItem/discount/remove";
}

- (NSString*)RESTReturnType
{
	return @"BasketAssociateDTO";
}

-(NSDictionary*)RESTParameterTypes
{
	NSMutableDictionary *parameterTypes = [NSMutableDictionary dictionaryWithDictionary:[super RESTParameterTypes]];
	parameterTypes[@"basketID"] = @"NSString";
	parameterTypes[@"basketItemDiscountID"] = @"NSArray";
	parameterTypes[@"basketItemID"] = @"NSString";
	return parameterTypes;
}
@end

@implementation ReprintLastTxInvocation

- (NSString*)RESTMethod
{
	return @"POST";
}

- (NSString*)RESTPath
{
	return @"/private/secure/control/reprint";
}

- (NSString*)RESTReturnType
{
	return @"BasketAssociateDTO";
}

-(NSDictionary*)RESTParameterTypes
{
	NSMutableDictionary *parameterTypes = [NSMutableDictionary dictionaryWithDictionary:[super RESTParameterTypes]];
	parameterTypes[@"deviceStationId"] = @"NSString";
	return parameterTypes;
}
@end

@implementation ResumeBasketInvocation

- (NSString*)RESTMethod
{
	return @"POST";
}

- (NSString*)RESTPath
{
	return @"/private/secure/basket/resume";
}

- (NSString*)RESTReturnType
{
	return @"BasketAssociateDTO";
}

-(NSDictionary*)RESTParameterTypes
{
	NSMutableDictionary *parameterTypes = [NSMutableDictionary dictionaryWithDictionary:[super RESTParameterTypes]];
	parameterTypes[@"basketBarcode"] = @"NSString";
	parameterTypes[@"basketID"] = @"NSString";
	parameterTypes[@"deviceID"] = @"NSString";
	parameterTypes[@"tillNumber"] = @"NSString";
	return parameterTypes;
}
@end

@implementation ReturnBasketItemInvocation

- (NSString*)RESTMethod
{
	return @"POST";
}

- (NSString*)RESTPath
{
	return @"/private/secure/basket/basketItem/return";
}

- (NSString*)RESTReturnType
{
	return @"BasketAssociateDTO";
}

-(NSDictionary*)RESTParameterTypes
{
	NSMutableDictionary *parameterTypes = [NSMutableDictionary dictionaryWithDictionary:[super RESTParameterTypes]];
	parameterTypes[@"basketID"] = @"NSString";
	parameterTypes[@"itemScanID"] = @"NSString";
	parameterTypes[@"itemSerialNumber"] = @"NSString";
	parameterTypes[@"originalItemDescription"] = @"NSString";
	parameterTypes[@"originalItemScanId"] = @"NSString";
	parameterTypes[@"originalTransactionBarcode"] = @"NSString";
	parameterTypes[@"originalTransactionDate"] = @"NSDate";
	parameterTypes[@"originalTransactionId"] = @"NSNumber";
	parameterTypes[@"originalTransactionStoreId"] = @"NSNumber";
	parameterTypes[@"originalTransactionTill"] = @"NSNumber";
	parameterTypes[@"price"] = @"NSString";
	parameterTypes[@"reasonId"] = @"NSString";
	return parameterTypes;
}
@end

@implementation ReturnWithTypeInvocation

- (NSString*)RESTMethod
{
	return @"POST";
}

- (NSString*)RESTPath
{
	return @"/private/secure/basket/basketItem/{type}";
}

- (NSString*)RESTReturnType
{
	return @"BasketAssociateDTO";
}

-(NSDictionary*)RESTParameterTypes
{
	NSMutableDictionary *parameterTypes = [NSMutableDictionary dictionaryWithDictionary:[super RESTParameterTypes]];
	parameterTypes[@"basketId"] = @"NSString";
	parameterTypes[@"itemScanId"] = @"NSString";
	parameterTypes[@"itemSerialNumber"] = @"NSString";
	parameterTypes[@"lineId"] = @"NSString";
	parameterTypes[@"originalCustomerCard"] = @"NSString";
	parameterTypes[@"originalItemDescription"] = @"NSString";
	parameterTypes[@"originalItemScanId"] = @"NSString";
	parameterTypes[@"originalReferenceId"] = @"NSString";
	parameterTypes[@"originalSalesPerson"] = @"NSString";
	parameterTypes[@"originalTransactionDate"] = @"NSString";
	parameterTypes[@"originalTransactionId"] = @"NSString";
	parameterTypes[@"originalTransactionStoreId"] = @"NSString";
	parameterTypes[@"originalTransactionTill"] = @"NSString";
	parameterTypes[@"price"] = @"NSString";
	parameterTypes[@"reasonDescription"] = @"NSString";
	parameterTypes[@"reasonId"] = @"NSString";
	parameterTypes[@"type"] = @"NSString";
	return parameterTypes;
}
@end

@implementation SearchItemsInvocation

- (NSString*)RESTMethod
{
	return @"GET";
}

- (NSString*)RESTPath
{
	return @"/private/item/search";
}

- (NSString*)RESTReturnType
{
	return @"NSArray";
}

-(NSDictionary*)RESTParameterTypes
{
	NSMutableDictionary *parameterTypes = [NSMutableDictionary dictionaryWithDictionary:[super RESTParameterTypes]];
	parameterTypes[@"department"] = @"NSString";
	parameterTypes[@"itemDescription"] = @"NSString";
	parameterTypes[@"pageNumber"] = @"NSInteger";
	parameterTypes[@"storeID"] = @"NSString";
	return parameterTypes;
}
@end

@implementation SetNotesForCustomerInvocation

- (NSString*)RESTMethod
{
	return @"POST";
}

- (NSString*)RESTPath
{
	return @"/private/secure/customer/{customerId}/notes";
}

- (NSString*)RESTReturnType
{
	return @"NSArray";
}

-(NSDictionary*)RESTParameterTypes
{
	NSMutableDictionary *parameterTypes = [NSMutableDictionary dictionaryWithDictionary:[super RESTParameterTypes]];
	parameterTypes[@"customerId"] = @"NSString";
	parameterTypes[@"customerNotes"] = @"NSArray";
	return parameterTypes;
}
@end

@implementation SuspendBasketInvocation

- (NSString*)RESTMethod
{
	return @"POST";
}

- (NSString*)RESTPath
{
	return @"/private/secure/basket/suspend";
}

- (NSString*)RESTReturnType
{
	return @"BasketAssociateDTO";
}

-(NSDictionary*)RESTParameterTypes
{
	NSMutableDictionary *parameterTypes = [NSMutableDictionary dictionaryWithDictionary:[super RESTParameterTypes]];
	parameterTypes[@"basketDescription"] = @"NSString";
	parameterTypes[@"basketID"] = @"NSString";
	return parameterTypes;
}
@end

@implementation ToggleTrainingModeInvocation

- (NSString*)RESTMethod
{
	return @"POST";
}

- (NSString*)RESTPath
{
	return @"/private/secure/sales/trainingmode";
}

- (NSString*)RESTReturnType
{
	return @"BOOL";
}

-(NSDictionary*)RESTParameterTypes
{
	NSMutableDictionary *parameterTypes = [NSMutableDictionary dictionaryWithDictionary:[super RESTParameterTypes]];
	return parameterTypes;
}
@end

@implementation UnlinkDeviceInvocation

- (NSString*)RESTMethod
{
	return @"DELETE";
}

- (NSString*)RESTPath
{
	return @"/private/secure/devicestation/{deviceStationId}/link";
}

- (NSString*)RESTReturnType
{
	return @"DeviceStation";
}

-(NSDictionary*)RESTParameterTypes
{
	NSMutableDictionary *parameterTypes = [NSMutableDictionary dictionaryWithDictionary:[super RESTParameterTypes]];
	parameterTypes[@"deviceStationId"] = @"NSString";
	return parameterTypes;
}
@end

@implementation UnlinkedReturnInvocation

- (NSString*)RESTMethod
{
	return @"POST";
}

- (NSString*)RESTPath
{
	return @"/private/secure/basket/basketItem/unlinkedreturn";
}

- (NSString*)RESTReturnType
{
	return @"BasketAssociateDTO";
}

-(NSDictionary*)RESTParameterTypes
{
	NSMutableDictionary *parameterTypes = [NSMutableDictionary dictionaryWithDictionary:[super RESTParameterTypes]];
	parameterTypes[@"basketId"] = @"NSString";
	parameterTypes[@"itemScanId"] = @"NSString";
	parameterTypes[@"itemSerialNumber"] = @"NSString";
	parameterTypes[@"originalCustomerCard"] = @"NSString";
	parameterTypes[@"originalItemDescription"] = @"NSString";
	parameterTypes[@"originalItemScanId"] = @"NSString";
	parameterTypes[@"originalSalesPerson"] = @"NSString";
	parameterTypes[@"originalTransactionDate"] = @"NSString";
	parameterTypes[@"originalTransactionId"] = @"NSString";
	parameterTypes[@"originalTransactionStoreId"] = @"NSString";
	parameterTypes[@"originalTransactionTill"] = @"NSString";
	parameterTypes[@"price"] = @"NSString";
	parameterTypes[@"reasonDescription"] = @"NSString";
	parameterTypes[@"reasonId"] = @"NSString";
	return parameterTypes;
}
@end

@implementation UpdateCustomerInvocation

- (NSString*)RESTMethod
{
	return @"PUT";
}

- (NSString*)RESTPath
{
	return @"/private/secure/customer/{customerId}";
}

- (NSString*)RESTBodyParameter
{
	return @"customer";
}

-(NSDictionary*)RESTParameterTypes
{
	NSMutableDictionary *parameterTypes = [NSMutableDictionary dictionaryWithDictionary:[super RESTParameterTypes]];
	parameterTypes[@"customer"] = @"Customer";
	parameterTypes[@"customerId"] = @"NSString";
	return parameterTypes;
}
@end

@implementation UpdateDeviceInvocation

- (NSString*)RESTMethod
{
	return @"POST";
}

- (NSString*)RESTPath
{
	return @"/private/device";
}

- (NSString*)RESTBodyParameter
{
	return @"device";
}

-(NSDictionary*)RESTParameterTypes
{
	NSMutableDictionary *parameterTypes = [NSMutableDictionary dictionaryWithDictionary:[super RESTParameterTypes]];
	parameterTypes[@"device"] = @"DeviceProfile";
	return parameterTypes;
}
@end

@implementation UpdateDeviceStationInvocation

- (NSString*)RESTMethod
{
	return @"POST";
}

- (NSString*)RESTPath
{
	return @"/private/secure/devicestation/{deviceStationId}";
}

- (NSString*)RESTBodyParameter
{
	return @"deviceStation";
}

- (NSString*)RESTReturnType
{
	return @"DeviceStation";
}

-(NSDictionary*)RESTParameterTypes
{
	NSMutableDictionary *parameterTypes = [NSMutableDictionary dictionaryWithDictionary:[super RESTParameterTypes]];
	parameterTypes[@"deviceStation"] = @"DeviceStation";
	parameterTypes[@"deviceStationId"] = @"NSString";
	return parameterTypes;
}
@end

@implementation UpdateGiftReceiptInvocation

- (NSString*)RESTMethod
{
	return @"POST";
}

- (NSString*)RESTPath
{
	return @"/private/secure/basket/giftreceipt";
}

- (NSString*)RESTBodyParameter
{
	return @"basket";
}

- (NSString*)RESTReturnType
{
	return @"BasketAssociateDTO";
}

-(NSDictionary*)RESTParameterTypes
{
	NSMutableDictionary *parameterTypes = [NSMutableDictionary dictionaryWithDictionary:[super RESTParameterTypes]];
	parameterTypes[@"basket"] = @"BasketAssociateDTO";
	return parameterTypes;
}
@end

@implementation Affiliation

-(NSString*)xmlTag
{
	return @"affiliation";
}

+(NSString*)contextName
{
	return @"AFFILIATION";
}

-(NSDictionary*)xmlAttributes
{
	NSMutableDictionary *xmlAttributes = [NSMutableDictionary dictionaryWithDictionary:[super xmlAttributes]];
	xmlAttributes[@"affiliationDescription"] = @"NSString";
	xmlAttributes[@"affiliationDescriptionLanguages"] = @"NSArray";
	xmlAttributes[@"affiliationName"] = @"NSString";
	xmlAttributes[@"affiliationNameLanguages"] = @"NSArray";
	xmlAttributes[@"affiliationType"] = @"NSString";
	xmlAttributes[@"isStaff"] = @"BOOL";
	return xmlAttributes;
}

-(NSDictionary*)objectAttributes
{
	NSMutableDictionary *objectAttributes = [NSMutableDictionary dictionaryWithDictionary:[super objectAttributes]];
	objectAttributes[@"affiliationDescription"] = @"primitiveAffiliationDescription";
	objectAttributes[@"affiliationDescriptionLanguages"] = @"affiliationDescriptionLanguages";
	objectAttributes[@"affiliationName"] = @"primitiveAffiliationName";
	objectAttributes[@"affiliationNameLanguages"] = @"affiliationNameLanguages";
	objectAttributes[@"affiliationType"] = @"affiliationType";
	objectAttributes[@"isStaff"] = @"isStaff";
	return objectAttributes;
}

-(NSString*)affiliationDescription
{
	return [self localizedStringFromLanguages:_affiliationDescriptionLanguages value:_affiliationDescription];
}

-(NSString*)primitiveAffiliationDescription
{
	return _affiliationDescription;
}

-(NSString*)affiliationName
{
	return [self localizedStringFromLanguages:_affiliationNameLanguages value:_affiliationName];
}

-(NSString*)primitiveAffiliationName
{
	return _affiliationName;
}
@end

@implementation AirportCode

-(NSString*)xmlTag
{
	return @"airportCode";
}

+(NSString*)contextName
{
	return @"AIRPORTCODE";
}

-(NSDictionary*)xmlAttributes
{
	NSMutableDictionary *xmlAttributes = [NSMutableDictionary dictionaryWithDictionary:[super xmlAttributes]];
	xmlAttributes[@"airportCode"] = @"NSString";
	xmlAttributes[@"airportInZone"] = @"NSString";
	return xmlAttributes;
}

-(NSDictionary*)objectAttributes
{
	NSMutableDictionary *objectAttributes = [NSMutableDictionary dictionaryWithDictionary:[super objectAttributes]];
	objectAttributes[@"airportCode"] = @"airportCode";
	objectAttributes[@"airportInZone"] = @"airportInZone";
	return objectAttributes;
}
@end

@implementation AirportZone

-(NSString*)xmlTag
{
	return @"airportZone";
}

+(NSString*)contextName
{
	return @"AIRPORTZONE";
}

-(NSDictionary*)xmlAttributes
{
	NSMutableDictionary *xmlAttributes = [NSMutableDictionary dictionaryWithDictionary:[super xmlAttributes]];
	xmlAttributes[@"airportZoneId"] = @"NSString";
	xmlAttributes[@"zoneDutyFree"] = @"BOOL";
	xmlAttributes[@"zoneLanguagues"] = @"NSArray";
	xmlAttributes[@"zoneTaxExempt"] = @"BOOL";
	return xmlAttributes;
}

-(NSDictionary*)objectAttributes
{
	NSMutableDictionary *objectAttributes = [NSMutableDictionary dictionaryWithDictionary:[super objectAttributes]];
	objectAttributes[@"airportZoneId"] = @"airportZoneId";
	objectAttributes[@"zoneDutyFree"] = @"zoneDutyFree";
	objectAttributes[@"zoneLanguagues"] = @"zoneLanguagues";
	objectAttributes[@"zoneTaxExempt"] = @"zoneTaxExempt";
	return objectAttributes;
}
@end

@implementation AirsideOption

-(NSString*)xmlTag
{
	return @"airsideOption";
}

+(NSString*)contextName
{
	return @"AIRSIDEOPTION";
}

-(NSDictionary*)xmlAttributes
{
	NSMutableDictionary *xmlAttributes = [NSMutableDictionary dictionaryWithDictionary:[super xmlAttributes]];
	xmlAttributes[@"airportCodes"] = @"NSArray";
	xmlAttributes[@"airportZones"] = @"NSArray";
	xmlAttributes[@"localAirportCode"] = @"NSString";
	return xmlAttributes;
}

-(NSDictionary*)objectAttributes
{
	NSMutableDictionary *objectAttributes = [NSMutableDictionary dictionaryWithDictionary:[super objectAttributes]];
	objectAttributes[@"airportCodes"] = @"airportCodes";
	objectAttributes[@"airportZones"] = @"airportZones";
	objectAttributes[@"localAirportCode"] = @"localAirportCode";
	return objectAttributes;
}
@end

@implementation Associate

-(NSString*)xmlTag
{
	return @"associate";
}

+(NSString*)contextName
{
	return @"ASSOCIATE";
}

-(NSDictionary*)xmlAttributes
{
	NSMutableDictionary *xmlAttributes = [NSMutableDictionary dictionaryWithDictionary:[super xmlAttributes]];
	xmlAttributes[@"associateId"] = @"NSString";
	xmlAttributes[@"displayName"] = @"NSString";
	xmlAttributes[@"email1"] = @"NSString";
	xmlAttributes[@"email2"] = @"NSString";
	xmlAttributes[@"firstName"] = @"NSString";
	xmlAttributes[@"language"] = @"NSString";
	xmlAttributes[@"lastName"] = @"NSString";
	xmlAttributes[@"phone1"] = @"NSString";
	xmlAttributes[@"phone2"] = @"NSString";
	xmlAttributes[@"title"] = @"NSString";
	return xmlAttributes;
}

-(NSDictionary*)objectAttributes
{
	NSMutableDictionary *objectAttributes = [NSMutableDictionary dictionaryWithDictionary:[super objectAttributes]];
	objectAttributes[@"associateId"] = @"associateId";
	objectAttributes[@"displayName"] = @"displayName";
	objectAttributes[@"email1"] = @"email1";
	objectAttributes[@"email2"] = @"email2";
	objectAttributes[@"firstName"] = @"firstName";
	objectAttributes[@"language"] = @"language";
	objectAttributes[@"lastName"] = @"lastName";
	objectAttributes[@"phone1"] = @"phone1";
	objectAttributes[@"phone2"] = @"phone2";
	objectAttributes[@"title"] = @"title";
	return objectAttributes;
}
@end

@implementation BasketCustomer

-(NSString*)xmlTag
{
	return @"basketCustomer";
}

+(NSString*)contextName
{
	return @"BASKETCUSTOMER";
}

-(NSDictionary*)xmlAttributes
{
	NSMutableDictionary *xmlAttributes = [NSMutableDictionary dictionaryWithDictionary:[super xmlAttributes]];
	xmlAttributes[@"amountDue"] = @"NSString";
	xmlAttributes[@"associate"] = @"Associate";
	xmlAttributes[@"barcode"] = @"NSString";
	xmlAttributes[@"basketID"] = @"NSString";
	xmlAttributes[@"basketItems"] = @"NSArray";
	xmlAttributes[@"basketRewards"] = @"NSArray";
	xmlAttributes[@"created"] = @"NSString";
	xmlAttributes[@"customer"] = @"Customer";
	xmlAttributes[@"dateCompleted"] = @"NSString";
	xmlAttributes[@"netAmount"] = @"NSString";
	xmlAttributes[@"receipt"] = @"NSString";
	xmlAttributes[@"resolution"] = @"NSString";
	xmlAttributes[@"rewardTotal"] = @"NSString";
	xmlAttributes[@"status"] = @"NSString";
	xmlAttributes[@"store"] = @"Store";
	xmlAttributes[@"tenders"] = @"NSArray";
	return xmlAttributes;
}

-(NSDictionary*)objectAttributes
{
	NSMutableDictionary *objectAttributes = [NSMutableDictionary dictionaryWithDictionary:[super objectAttributes]];
	objectAttributes[@"amountDue"] = @"amountDue";
	objectAttributes[@"associate"] = @"associate";
	objectAttributes[@"barcode"] = @"barcode";
	objectAttributes[@"basketID"] = @"basketID";
	objectAttributes[@"basketItems"] = @"basketItems";
	objectAttributes[@"basketRewards"] = @"basketRewards";
	objectAttributes[@"created"] = @"created";
	objectAttributes[@"customer"] = @"customer";
	objectAttributes[@"dateCompleted"] = @"dateCompleted";
	objectAttributes[@"netAmount"] = @"netAmount";
	objectAttributes[@"receipt"] = @"receipt";
	objectAttributes[@"resolution"] = @"resolution";
	objectAttributes[@"rewardTotal"] = @"rewardTotal";
	objectAttributes[@"status"] = @"status";
	objectAttributes[@"store"] = @"store";
	objectAttributes[@"tenders"] = @"tenders";
	return objectAttributes;
}
@end

@implementation BasketDTO

+(NSString*)contextName
{
	return @"BASKET";
}

-(NSDictionary*)xmlAttributes
{
	NSMutableDictionary *xmlAttributes = [NSMutableDictionary dictionaryWithDictionary:[super xmlAttributes]];
	xmlAttributes[@"amountDue"] = @"BasketPricingDTO";
	xmlAttributes[@"barcode"] = @"NSString";
	xmlAttributes[@"basketDescription"] = @"NSString";
	xmlAttributes[@"basketID"] = @"NSString";
	xmlAttributes[@"basketItems"] = @"NSArray";
	xmlAttributes[@"basketRewards"] = @"NSArray";
	xmlAttributes[@"changeDue"] = @"NSString";
	xmlAttributes[@"completed"] = @"NSString";
	xmlAttributes[@"created"] = @"NSDate";
	xmlAttributes[@"currencyCode"] = @"NSString";
	xmlAttributes[@"customerCards"] = @"NSArray";
	xmlAttributes[@"email"] = @"NSString";
	xmlAttributes[@"foreignSubtotal"] = @"BasketForeignSubTotalDTO";
	xmlAttributes[@"netAmount"] = @"NSString";
	xmlAttributes[@"receipt"] = @"NSString";
	xmlAttributes[@"resolution"] = @"NSString";
	xmlAttributes[@"rewardTotal"] = @"BasketPricingDTO";
	xmlAttributes[@"status"] = @"NSString";
	xmlAttributes[@"store"] = @"Store";
	xmlAttributes[@"taxFree"] = @"BasketTaxFree";
	xmlAttributes[@"tillNumber"] = @"NSString";
	xmlAttributes[@"total"] = @"NSString";
	xmlAttributes[@"trainingMode"] = @"BOOL";
	xmlAttributes[@"transactionDate"] = @"NSString";
	xmlAttributes[@"transactionNumber"] = @"NSString";
	return xmlAttributes;
}

-(NSDictionary*)objectAttributes
{
	NSMutableDictionary *objectAttributes = [NSMutableDictionary dictionaryWithDictionary:[super objectAttributes]];
	objectAttributes[@"amountDue"] = @"amountDue";
	objectAttributes[@"barcode"] = @"barcode";
	objectAttributes[@"basketDescription"] = @"basketDescription";
	objectAttributes[@"basketID"] = @"basketID";
	objectAttributes[@"basketItems"] = @"basketItems";
	objectAttributes[@"basketRewards"] = @"basketRewards";
	objectAttributes[@"changeDue"] = @"changeDue";
	objectAttributes[@"completed"] = @"completed";
	objectAttributes[@"created"] = @"created";
	objectAttributes[@"currencyCode"] = @"currencyCode";
	objectAttributes[@"customerCards"] = @"customerCards";
	objectAttributes[@"email"] = @"email";
	objectAttributes[@"foreignSubtotal"] = @"foreignSubtotal";
	objectAttributes[@"netAmount"] = @"netAmount";
	objectAttributes[@"receipt"] = @"receipt";
	objectAttributes[@"resolution"] = @"resolution";
	objectAttributes[@"rewardTotal"] = @"rewardTotal";
	objectAttributes[@"status"] = @"status";
	objectAttributes[@"store"] = @"store";
	objectAttributes[@"taxFree"] = @"taxFree";
	objectAttributes[@"tillNumber"] = @"tillNumber";
	objectAttributes[@"total"] = @"total";
	objectAttributes[@"trainingMode"] = @"trainingMode";
	objectAttributes[@"transactionDate"] = @"transactionDate";
	objectAttributes[@"transactionNumber"] = @"transactionNumber";
	return objectAttributes;
}
@end

@implementation BasketAssociateDTO

-(NSString*)xmlTag
{
	return @"associateBasket";
}

+(NSString*)contextName
{
	return @"BASKETASSOCIATE";
}

-(NSDictionary*)xmlAttributes
{
	NSMutableDictionary *xmlAttributes = [NSMutableDictionary dictionaryWithDictionary:[super xmlAttributes]];
	xmlAttributes[@"customer"] = @"Customer";
	xmlAttributes[@"operator"] = @"Associate";
	return xmlAttributes;
}

-(NSDictionary*)objectAttributes
{
	NSMutableDictionary *objectAttributes = [NSMutableDictionary dictionaryWithDictionary:[super objectAttributes]];
	objectAttributes[@"customer"] = @"customer";
	objectAttributes[@"operator"] = @"operator";
	return objectAttributes;
}
@end

@implementation BasketTaxFreeDocumentDTO

-(NSString*)xmlTag
{
	return @"taxFreeBasket";
}

+(NSString*)contextName
{
	return @"TAXFREEBASKET";
}

-(NSDictionary*)xmlAttributes
{
	NSMutableDictionary *xmlAttributes = [NSMutableDictionary dictionaryWithDictionary:[super xmlAttributes]];
	xmlAttributes[@"basketItems"] = @"NSArray";
	xmlAttributes[@"issuerCountryId"] = @"NSString";
	xmlAttributes[@"operator"] = @"Associate";
	xmlAttributes[@"shopper"] = @"ShopperDetailsDTO";
	xmlAttributes[@"shopperConfirmed"] = @"NSString";
	xmlAttributes[@"shopperDetails"] = @"ShopperDetailsDTO";
	xmlAttributes[@"shopperValidatedBy"] = @"NSString";
	xmlAttributes[@"taxFreeForm"] = @"NSArray";
	xmlAttributes[@"transactionNumber"] = @"NSString";
	xmlAttributes[@"travellerId"] = @"NSString";
	return xmlAttributes;
}

-(NSDictionary*)objectAttributes
{
	NSMutableDictionary *objectAttributes = [NSMutableDictionary dictionaryWithDictionary:[super objectAttributes]];
	objectAttributes[@"basketItems"] = @"basketItems";
	objectAttributes[@"issuerCountryId"] = @"issuerCountryId";
	objectAttributes[@"operator"] = @"operator";
	objectAttributes[@"shopper"] = @"shopper";
	objectAttributes[@"shopperConfirmed"] = @"shopperConfirmed";
	objectAttributes[@"shopperDetails"] = @"shopperDetails";
	objectAttributes[@"shopperValidatedBy"] = @"shopperValidatedBy";
	objectAttributes[@"taxFreeForm"] = @"taxFreeForm";
	objectAttributes[@"transactionNumber"] = @"transactionNumber";
	objectAttributes[@"travellerId"] = @"travellerId";
	return objectAttributes;
}
@end

@implementation BasketFlightDetails

-(NSString*)xmlTag
{
	return @"flightDetails";
}

+(NSString*)contextName
{
	return @"BASKETFLIGHTDETAILS";
}

-(NSDictionary*)xmlAttributes
{
	NSMutableDictionary *xmlAttributes = [NSMutableDictionary dictionaryWithDictionary:[super xmlAttributes]];
	xmlAttributes[@"airlineCode"] = @"NSString";
	xmlAttributes[@"boardingPassBarcode"] = @"NSString";
	xmlAttributes[@"destinationAirportCode"] = @"NSString";
	xmlAttributes[@"flightNumber"] = @"NSString";
	xmlAttributes[@"manualEntry"] = @"BOOL";
	xmlAttributes[@"nationality"] = @"NSString";
	xmlAttributes[@"notFlying"] = @"BOOL";
	xmlAttributes[@"originAirportCode"] = @"NSString";
	return xmlAttributes;
}

-(NSDictionary*)objectAttributes
{
	NSMutableDictionary *objectAttributes = [NSMutableDictionary dictionaryWithDictionary:[super objectAttributes]];
	objectAttributes[@"airlineCode"] = @"airlineCode";
	objectAttributes[@"boardingPassBarcode"] = @"boardingPassBarcode";
	objectAttributes[@"destinationAirportCode"] = @"destinationAirportCode";
	objectAttributes[@"flightNumber"] = @"flightNumber";
	objectAttributes[@"manualEntry"] = @"manualEntry";
	objectAttributes[@"nationality"] = @"nationality";
	objectAttributes[@"notFlying"] = @"notFlying";
	objectAttributes[@"originAirportCode"] = @"originAirportCode";
	return objectAttributes;
}
@end

@implementation BasketForeignSubTotalDTO

-(NSString*)xmlTag
{
	return @"foreignSubtotal";
}

+(NSString*)contextName
{
	return @"BASKETFOREIGNSUBTOTAL";
}

-(NSDictionary*)xmlAttributes
{
	NSMutableDictionary *xmlAttributes = [NSMutableDictionary dictionaryWithDictionary:[super xmlAttributes]];
	xmlAttributes[@"basket"] = @"BasketAssociateDTO";
	xmlAttributes[@"currencyCode"] = @"NSString";
	xmlAttributes[@"desc"] = @"NSString";
	xmlAttributes[@"exchangeRate"] = @"NSNumber";
	xmlAttributes[@"foreignAmount"] = @"BasketPricingDTO";
	return xmlAttributes;
}

-(NSDictionary*)objectAttributes
{
	NSMutableDictionary *objectAttributes = [NSMutableDictionary dictionaryWithDictionary:[super objectAttributes]];
	objectAttributes[@"basket"] = @"basket";
	objectAttributes[@"currencyCode"] = @"currencyCode";
	objectAttributes[@"desc"] = @"desc";
	objectAttributes[@"exchangeRate"] = @"exchangeRate";
	objectAttributes[@"foreignAmount"] = @"foreignAmount";
	return objectAttributes;
}
@end

@implementation BasketItem

-(NSString*)xmlTag
{
	return @"basketItem";
}

+(NSString*)contextName
{
	return @"BASKETITEM";
}

-(NSDictionary*)xmlAttributes
{
	NSMutableDictionary *xmlAttributes = [NSMutableDictionary dictionaryWithDictionary:[super xmlAttributes]];
	xmlAttributes[@"ageRestriction"] = @"NSString";
	xmlAttributes[@"barcodeType"] = @"NSString";
	xmlAttributes[@"basket"] = @"BasketDTO";
	xmlAttributes[@"basketItemChangeResult"] = @"BOOL";
	xmlAttributes[@"basketItemCreatedError"] = @"ServerError";
	xmlAttributes[@"basketItemId"] = @"NSString";
	xmlAttributes[@"basketRewards"] = @"NSArray";
	xmlAttributes[@"desc"] = @"NSString";
	xmlAttributes[@"editable"] = @"BOOL";
	xmlAttributes[@"entryMethod"] = @"NSString";
	xmlAttributes[@"giftReceiptRequest"] = @"NSArray";
	xmlAttributes[@"hasBasketItemChanged"] = @"BOOL";
	xmlAttributes[@"hasBasketItemCreated"] = @"BOOL";
	xmlAttributes[@"imageUrl"] = @"NSString";
	xmlAttributes[@"itemID"] = @"NSString";
	xmlAttributes[@"itemScanId"] = @"NSString";
	xmlAttributes[@"itemSerialNumber"] = @"NSString";
	xmlAttributes[@"mandatoryModifierGroups"] = @"NSArray";
	xmlAttributes[@"measure"] = @"NSString";
	xmlAttributes[@"message"] = @"NSString";
	xmlAttributes[@"netAmount"] = @"NSString";
	xmlAttributes[@"optionalModifierGroups"] = @"NSArray";
	xmlAttributes[@"orginalQuantity"] = @"NSString";
	xmlAttributes[@"originalItemDescription"] = @"NSString";
	xmlAttributes[@"originalItemScanId"] = @"NSString";
	xmlAttributes[@"pricing"] = @"BasketPricingDTO";
	xmlAttributes[@"quantity"] = @"NSString";
	xmlAttributes[@"retailPrice"] = @"BasketPricingDTO";
	xmlAttributes[@"rewardTotal"] = @"NSString";
	xmlAttributes[@"saleAssociateID"] = @"NSString";
	xmlAttributes[@"salesPerson"] = @"SalesPerson";
	xmlAttributes[@"selectedModifiers"] = @"NSArray";
	xmlAttributes[@"serialNumber"] = @"NSString";
	xmlAttributes[@"totalPrice"] = @"BasketPricingDTO";
	xmlAttributes[@"track1"] = @"NSString";
	xmlAttributes[@"track2"] = @"NSString";
	xmlAttributes[@"track3"] = @"NSString";
	return xmlAttributes;
}

-(NSDictionary*)objectAttributes
{
	NSMutableDictionary *objectAttributes = [NSMutableDictionary dictionaryWithDictionary:[super objectAttributes]];
	objectAttributes[@"ageRestriction"] = @"ageRestriction";
	objectAttributes[@"barcodeType"] = @"barcodeType";
	objectAttributes[@"basket"] = @"basket";
	objectAttributes[@"basketItemChangeResult"] = @"basketItemChangeResult";
	objectAttributes[@"basketItemCreatedError"] = @"basketItemCreatedError";
	objectAttributes[@"basketItemId"] = @"basketItemId";
	objectAttributes[@"basketRewards"] = @"basketRewards";
	objectAttributes[@"desc"] = @"desc";
	objectAttributes[@"editable"] = @"editable";
	objectAttributes[@"entryMethod"] = @"entryMethod";
	objectAttributes[@"giftReceiptRequest"] = @"giftReceiptRequest";
	objectAttributes[@"hasBasketItemChanged"] = @"hasBasketItemChanged";
	objectAttributes[@"hasBasketItemCreated"] = @"hasBasketItemCreated";
	objectAttributes[@"imageUrl"] = @"imageUrl";
	objectAttributes[@"itemID"] = @"itemID";
	objectAttributes[@"itemScanId"] = @"itemScanId";
	objectAttributes[@"itemSerialNumber"] = @"itemSerialNumber";
	objectAttributes[@"mandatoryModifierGroups"] = @"mandatoryModifierGroups";
	objectAttributes[@"measure"] = @"measure";
	objectAttributes[@"message"] = @"message";
	objectAttributes[@"netAmount"] = @"netAmount";
	objectAttributes[@"optionalModifierGroups"] = @"optionalModifierGroups";
	objectAttributes[@"orginalQuantity"] = @"orginalQuantity";
	objectAttributes[@"originalItemDescription"] = @"originalItemDescription";
	objectAttributes[@"originalItemScanId"] = @"originalItemScanId";
	objectAttributes[@"pricing"] = @"pricing";
	objectAttributes[@"quantity"] = @"quantity";
	objectAttributes[@"retailPrice"] = @"retailPrice";
	objectAttributes[@"rewardTotal"] = @"rewardTotal";
	objectAttributes[@"saleAssociateID"] = @"saleAssociateID";
	objectAttributes[@"salesPerson"] = @"salesPerson";
	objectAttributes[@"selectedModifiers"] = @"selectedModifiers";
	objectAttributes[@"serialNumber"] = @"serialNumber";
	objectAttributes[@"totalPrice"] = @"totalPrice";
	objectAttributes[@"track1"] = @"track1";
	objectAttributes[@"track2"] = @"track2";
	objectAttributes[@"track3"] = @"track3";
	return objectAttributes;
}

-(NSString*)parentAttribute
{
	return @"basket";
}
@end

@implementation BasketAssociateInfo

-(NSString*)xmlTag
{
	return @"associateInfo";
}

+(NSString*)contextName
{
	return @"BASKETASSOCIATEINFO";
}

-(NSDictionary*)xmlAttributes
{
	NSMutableDictionary *xmlAttributes = [NSMutableDictionary dictionaryWithDictionary:[super xmlAttributes]];
	xmlAttributes[@"employeeId"] = @"NSString";
	xmlAttributes[@"operatorDisplayName"] = @"NSString";
	xmlAttributes[@"operatorId"] = @"NSString";
	return xmlAttributes;
}

-(NSDictionary*)objectAttributes
{
	NSMutableDictionary *objectAttributes = [NSMutableDictionary dictionaryWithDictionary:[super objectAttributes]];
	objectAttributes[@"employeeId"] = @"employeeId";
	objectAttributes[@"operatorDisplayName"] = @"operatorDisplayName";
	objectAttributes[@"operatorId"] = @"operatorId";
	return objectAttributes;
}
@end

@implementation BasketCustomerAffiliation

-(NSString*)xmlTag
{
	return @"customerAffiliation";
}

+(NSString*)contextName
{
	return @"BASKETCUSTOMERAFFILIATION";
}

-(NSDictionary*)xmlAttributes
{
	NSMutableDictionary *xmlAttributes = [NSMutableDictionary dictionaryWithDictionary:[super xmlAttributes]];
	xmlAttributes[@"affiliationMembership"] = @"NSString";
	xmlAttributes[@"affiliationName"] = @"NSString";
	xmlAttributes[@"affiliationNumber"] = @"NSString";
	xmlAttributes[@"affiliationTypeId"] = @"NSString";
	return xmlAttributes;
}

-(NSDictionary*)objectAttributes
{
	NSMutableDictionary *objectAttributes = [NSMutableDictionary dictionaryWithDictionary:[super objectAttributes]];
	objectAttributes[@"affiliationMembership"] = @"affiliationMembership";
	objectAttributes[@"affiliationName"] = @"affiliationName";
	objectAttributes[@"affiliationNumber"] = @"affiliationNumber";
	objectAttributes[@"affiliationTypeId"] = @"affiliationTypeId";
	return objectAttributes;
}
@end

@implementation BasketCustomerCard

-(NSString*)xmlTag
{
	return @"customerCard";
}

+(NSString*)contextName
{
	return @"BASKETCUSTOMERCARD";
}

-(NSDictionary*)xmlAttributes
{
	NSMutableDictionary *xmlAttributes = [NSMutableDictionary dictionaryWithDictionary:[super xmlAttributes]];
	xmlAttributes[@"affiliationType"] = @"NSString";
	xmlAttributes[@"cardName"] = @"NSString";
	xmlAttributes[@"cardNumber"] = @"NSString";
	xmlAttributes[@"customer"] = @"Customer";
	xmlAttributes[@"customerCardNumber"] = @"NSString";
	xmlAttributes[@"customerDisplayName"] = @"NSString";
	return xmlAttributes;
}

-(NSDictionary*)objectAttributes
{
	NSMutableDictionary *objectAttributes = [NSMutableDictionary dictionaryWithDictionary:[super objectAttributes]];
	objectAttributes[@"affiliationType"] = @"affiliationType";
	objectAttributes[@"cardName"] = @"cardName";
	objectAttributes[@"cardNumber"] = @"cardNumber";
	objectAttributes[@"customer"] = @"customer";
	objectAttributes[@"customerCardNumber"] = @"customerCardNumber";
	objectAttributes[@"customerDisplayName"] = @"customerDisplayName";
	return objectAttributes;
}
@end

@implementation BasketRewardSale

-(NSString*)xmlTag
{
	return @"basketReward";
}

+(NSString*)contextName
{
	return @"BASKETREWARDSALE";
}

-(NSDictionary*)xmlAttributes
{
	NSMutableDictionary *xmlAttributes = [NSMutableDictionary dictionaryWithDictionary:[super xmlAttributes]];
	xmlAttributes[@"code"] = @"id<NSCoding>";
	xmlAttributes[@"reason"] = @"id<NSCoding>";
	xmlAttributes[@"rewardAmount"] = @"BasketPricingDTO";
	xmlAttributes[@"rewardId"] = @"NSString";
	xmlAttributes[@"rewardText"] = @"NSString";
	xmlAttributes[@"rewardType"] = @"NSString";
	return xmlAttributes;
}

-(NSDictionary*)objectAttributes
{
	NSMutableDictionary *objectAttributes = [NSMutableDictionary dictionaryWithDictionary:[super objectAttributes]];
	objectAttributes[@"code"] = @"code";
	objectAttributes[@"reason"] = @"reason";
	objectAttributes[@"rewardAmount"] = @"rewardAmount";
	objectAttributes[@"rewardId"] = @"rewardId";
	objectAttributes[@"rewardText"] = @"rewardText";
	objectAttributes[@"rewardType"] = @"rewardType";
	return objectAttributes;
}
@end

@implementation BasketRewardSaleDiscountAmount

-(NSString*)xmlTag
{
	return @"saleDiscountAmount";
}

+(NSString*)contextName
{
	return @"BASKETREWARDSALEDISCOUNTAMOUNT";
}

-(NSDictionary*)xmlAttributes
{
	NSMutableDictionary *xmlAttributes = [NSMutableDictionary dictionaryWithDictionary:[super xmlAttributes]];
	return xmlAttributes;
}

-(NSDictionary*)objectAttributes
{
	NSMutableDictionary *objectAttributes = [NSMutableDictionary dictionaryWithDictionary:[super objectAttributes]];
	return objectAttributes;
}
@end

@implementation BasketRewardSaleDiscountPercent

-(NSString*)xmlTag
{
	return @"saleDiscountPercent";
}

+(NSString*)contextName
{
	return @"BASKETREWARDSALEDISCOUNTPERCENT";
}

-(NSDictionary*)xmlAttributes
{
	NSMutableDictionary *xmlAttributes = [NSMutableDictionary dictionaryWithDictionary:[super xmlAttributes]];
	return xmlAttributes;
}

-(NSDictionary*)objectAttributes
{
	NSMutableDictionary *objectAttributes = [NSMutableDictionary dictionaryWithDictionary:[super objectAttributes]];
	return objectAttributes;
}
@end

@implementation BasketRewardSalePromotionAmount

-(NSString*)xmlTag
{
	return @"salePromotionAmount";
}

+(NSString*)contextName
{
	return @"BASKETREWARDSALEPROMOTIONAMOUNT";
}

-(NSDictionary*)xmlAttributes
{
	NSMutableDictionary *xmlAttributes = [NSMutableDictionary dictionaryWithDictionary:[super xmlAttributes]];
	return xmlAttributes;
}

-(NSDictionary*)objectAttributes
{
	NSMutableDictionary *objectAttributes = [NSMutableDictionary dictionaryWithDictionary:[super objectAttributes]];
	return objectAttributes;
}
@end

@implementation BasketTender

-(BOOL)hasCreatedTenderValue {return [self.hasCreatedTender boolValue];}
-(void)setHasCreatedTenderValue:(BOOL)hasCreatedTenderValue {self.hasCreatedTender = @(hasCreatedTenderValue);}
-(NSString*)xmlTag
{
	return @"basketTender";
}

+(NSString*)contextName
{
	return @"BASKETTENDER";
}

-(NSDictionary*)xmlAttributes
{
	NSMutableDictionary *xmlAttributes = [NSMutableDictionary dictionaryWithDictionary:[super xmlAttributes]];
	xmlAttributes[@"amount"] = @"NSString";
	xmlAttributes[@"authorization"] = @"BasketTenderAuthorization";
	xmlAttributes[@"currencyCode"] = @"NSString";
	xmlAttributes[@"desc"] = @"NSString";
	xmlAttributes[@"displayAmount"] = @"NSString";
	xmlAttributes[@"displayText"] = @"NSString";
	xmlAttributes[@"foreignCurrency"] = @"BasketTenderForeignCurrency";
	xmlAttributes[@"hasCreatedTender"] = @"BOOL";
	xmlAttributes[@"tender"] = @"Tender";
	xmlAttributes[@"tenderAmount"] = @"BasketPricingDTO";
	xmlAttributes[@"tenderType"] = @"NSString";
	return xmlAttributes;
}

-(NSDictionary*)objectAttributes
{
	NSMutableDictionary *objectAttributes = [NSMutableDictionary dictionaryWithDictionary:[super objectAttributes]];
	objectAttributes[@"amount"] = @"amount";
	objectAttributes[@"authorization"] = @"authorization";
	objectAttributes[@"currencyCode"] = @"currencyCode";
	objectAttributes[@"desc"] = @"desc";
	objectAttributes[@"displayAmount"] = @"displayAmount";
	objectAttributes[@"displayText"] = @"displayText";
	objectAttributes[@"foreignCurrency"] = @"foreignCurrency";
	objectAttributes[@"hasCreatedTender"] = @"hasCreatedTender";
	objectAttributes[@"tender"] = @"tender";
	objectAttributes[@"tenderAmount"] = @"tenderAmount";
	objectAttributes[@"tenderType"] = @"tenderType";
	return objectAttributes;
}
@end

@implementation EFTPayment

-(NSString*)xmlTag
{
	return @"eftPayment";
}

+(NSString*)contextName
{
	return @"EFTPAYMENT";
}

-(NSDictionary*)xmlAttributes
{
	NSMutableDictionary *xmlAttributes = [NSMutableDictionary dictionaryWithDictionary:[super xmlAttributes]];
	xmlAttributes[@"authorizationCode"] = @"NSString";
	xmlAttributes[@"authorizingTermId"] = @"NSString";
	xmlAttributes[@"basketId"] = @"NSString";
	xmlAttributes[@"basketTenderId"] = @"NSString";
	xmlAttributes[@"cardBin"] = @"NSString";
	xmlAttributes[@"cardType"] = @"NSString";
	xmlAttributes[@"cardTypeCode"] = @"NSString";
	xmlAttributes[@"customerAlias"] = @"NSString";
	xmlAttributes[@"dccCommissionFee"] = @"NSNumber";
	xmlAttributes[@"dccConvertedAmount"] = @"NSNumber";
	xmlAttributes[@"dccConvertedCurrency"] = @"NSString";
	xmlAttributes[@"dccExchangeRate"] = @"NSNumber";
	xmlAttributes[@"dccMarkup"] = @"NSNumber";
	xmlAttributes[@"dccOriginalAmount"] = @"NSNumber";
	xmlAttributes[@"dccOriginalCurrency"] = @"NSString";
	xmlAttributes[@"dccSource"] = @"NSString";
	xmlAttributes[@"eftProvider"] = @"NSString";
	xmlAttributes[@"encryptedAccountNumber"] = @"NSString";
	xmlAttributes[@"issuerCountryId"] = @"NSString";
	xmlAttributes[@"providerCentralTenderId"] = @"NSString";
	xmlAttributes[@"providerTenderId"] = @"NSString";
	xmlAttributes[@"rawData"] = @"NSString";
	xmlAttributes[@"receiptCustomer"] = @"NSString";
	xmlAttributes[@"receiptMerchant"] = @"NSString";
	xmlAttributes[@"referenceNumber"] = @"NSString";
	xmlAttributes[@"resultCode"] = @"NSNumber";
	xmlAttributes[@"signature"] = @"NSString";
	return xmlAttributes;
}

-(NSDictionary*)objectAttributes
{
	NSMutableDictionary *objectAttributes = [NSMutableDictionary dictionaryWithDictionary:[super objectAttributes]];
	objectAttributes[@"authorizationCode"] = @"authorizationCode";
	objectAttributes[@"authorizingTermId"] = @"authorizingTermId";
	objectAttributes[@"basketId"] = @"basketId";
	objectAttributes[@"basketTenderId"] = @"basketTenderId";
	objectAttributes[@"cardBin"] = @"cardBin";
	objectAttributes[@"cardType"] = @"cardType";
	objectAttributes[@"cardTypeCode"] = @"cardTypeCode";
	objectAttributes[@"customerAlias"] = @"customerAlias";
	objectAttributes[@"dccCommissionFee"] = @"dccCommissionFee";
	objectAttributes[@"dccConvertedAmount"] = @"dccConvertedAmount";
	objectAttributes[@"dccConvertedCurrency"] = @"dccConvertedCurrency";
	objectAttributes[@"dccExchangeRate"] = @"dccExchangeRate";
	objectAttributes[@"dccMarkup"] = @"dccMarkup";
	objectAttributes[@"dccOriginalAmount"] = @"dccOriginalAmount";
	objectAttributes[@"dccOriginalCurrency"] = @"dccOriginalCurrency";
	objectAttributes[@"dccSource"] = @"dccSource";
	objectAttributes[@"eftProvider"] = @"eftProvider";
	objectAttributes[@"encryptedAccountNumber"] = @"encryptedAccountNumber";
	objectAttributes[@"issuerCountryId"] = @"issuerCountryId";
	objectAttributes[@"providerCentralTenderId"] = @"providerCentralTenderId";
	objectAttributes[@"providerTenderId"] = @"providerTenderId";
	objectAttributes[@"rawData"] = @"rawData";
	objectAttributes[@"receiptCustomer"] = @"receiptCustomer";
	objectAttributes[@"receiptMerchant"] = @"receiptMerchant";
	objectAttributes[@"referenceNumber"] = @"referenceNumber";
	objectAttributes[@"resultCode"] = @"resultCode";
	objectAttributes[@"signature"] = @"signature";
	return objectAttributes;
}
@end

@implementation StandardPayment

-(NSString*)xmlTag
{
	return @"payment";
}

+(NSString*)contextName
{
	return @"STANDARDPAYMENT";
}

-(NSDictionary*)xmlAttributes
{
	NSMutableDictionary *xmlAttributes = [NSMutableDictionary dictionaryWithDictionary:[super xmlAttributes]];
	xmlAttributes[@"authCode"] = @"NSString";
	xmlAttributes[@"basketID"] = @"NSString";
	xmlAttributes[@"cardNumber"] = @"NSString";
	xmlAttributes[@"currencyCode"] = @"NSString";
	xmlAttributes[@"reference"] = @"NSString";
	xmlAttributes[@"tenderType"] = @"NSString";
	return xmlAttributes;
}

-(NSDictionary*)objectAttributes
{
	NSMutableDictionary *objectAttributes = [NSMutableDictionary dictionaryWithDictionary:[super objectAttributes]];
	objectAttributes[@"authCode"] = @"authCode";
	objectAttributes[@"basketID"] = @"basketID";
	objectAttributes[@"cardNumber"] = @"cardNumber";
	objectAttributes[@"currencyCode"] = @"currencyCode";
	objectAttributes[@"reference"] = @"reference";
	objectAttributes[@"tenderType"] = @"tenderType";
	return objectAttributes;
}
@end

@implementation VerifonePayment

-(BOOL)hasDataValue {return [self.hasData boolValue];}
-(void)setHasDataValue:(BOOL)hasDataValue {self.hasData = @(hasDataValue);}
-(BOOL)hasReplyValue {return [self.hasReply boolValue];}
-(void)setHasReplyValue:(BOOL)hasReplyValue {self.hasReply = @(hasReplyValue);}
-(NSString*)xmlTag
{
	return @"veriFonePayment";
}

+(NSString*)contextName
{
	return @"VERIFONEPAYMENT";
}

-(NSDictionary*)xmlAttributes
{
	NSMutableDictionary *xmlAttributes = [NSMutableDictionary dictionaryWithDictionary:[super xmlAttributes]];
	xmlAttributes[@"authCode"] = @"NSString";
	xmlAttributes[@"basketTenderId"] = @"NSString";
	xmlAttributes[@"cardScheme"] = @"NSString";
	xmlAttributes[@"hasData"] = @"BOOL";
	xmlAttributes[@"hasReply"] = @"BOOL";
	xmlAttributes[@"identifier"] = @"NSString";
	xmlAttributes[@"isError"] = @"NSString";
	xmlAttributes[@"maskedExpiry"] = @"NSString";
	xmlAttributes[@"maskedPAN"] = @"NSString";
	xmlAttributes[@"paymentId"] = @"NSString";
	xmlAttributes[@"provider"] = @"NSString";
	xmlAttributes[@"receiptData"] = @"NSString";
	xmlAttributes[@"receiptSignature"] = @"NSString";
	xmlAttributes[@"responseMsg"] = @"NSString";
	xmlAttributes[@"salesBasketId"] = @"NSString";
	xmlAttributes[@"tenderType"] = @"NSString";
	xmlAttributes[@"tipAmount"] = @"NSString";
	xmlAttributes[@"totalCharge"] = @"NSString";
	xmlAttributes[@"track1Data"] = @"NSString";
	xmlAttributes[@"track2Data"] = @"NSString";
	xmlAttributes[@"track3Data"] = @"NSString";
	xmlAttributes[@"transaction"] = @"NSString";
	xmlAttributes[@"transId"] = @"NSString";
	return xmlAttributes;
}

-(NSDictionary*)objectAttributes
{
	NSMutableDictionary *objectAttributes = [NSMutableDictionary dictionaryWithDictionary:[super objectAttributes]];
	objectAttributes[@"authCode"] = @"authCode";
	objectAttributes[@"basketTenderId"] = @"basketTenderId";
	objectAttributes[@"cardScheme"] = @"cardScheme";
	objectAttributes[@"hasData"] = @"hasData";
	objectAttributes[@"hasReply"] = @"hasReply";
	objectAttributes[@"identifier"] = @"identifier";
	objectAttributes[@"isError"] = @"isError";
	objectAttributes[@"maskedExpiry"] = @"maskedExpiry";
	objectAttributes[@"maskedPAN"] = @"maskedPAN";
	objectAttributes[@"paymentId"] = @"paymentId";
	objectAttributes[@"provider"] = @"provider";
	objectAttributes[@"receiptData"] = @"receiptData";
	objectAttributes[@"receiptSignature"] = @"receiptSignature";
	objectAttributes[@"responseMsg"] = @"responseMsg";
	objectAttributes[@"salesBasketId"] = @"salesBasketId";
	objectAttributes[@"tenderType"] = @"tenderType";
	objectAttributes[@"tipAmount"] = @"tipAmount";
	objectAttributes[@"totalCharge"] = @"totalCharge";
	objectAttributes[@"track1Data"] = @"track1Data";
	objectAttributes[@"track2Data"] = @"track2Data";
	objectAttributes[@"track3Data"] = @"track3Data";
	objectAttributes[@"transaction"] = @"transaction";
	objectAttributes[@"transId"] = @"transId";
	return objectAttributes;
}
@end

@implementation YespayPayment

-(BOOL)hasReplyValue {return [self.hasReply boolValue];}
-(void)setHasReplyValue:(BOOL)hasReplyValue {self.hasReply = @(hasReplyValue);}
-(NSString*)xmlTag
{
	return @"yespayPayment";
}

+(NSString*)contextName
{
	return @"YESPAYPAYMENT";
}

-(NSDictionary*)xmlAttributes
{
	NSMutableDictionary *xmlAttributes = [NSMutableDictionary dictionaryWithDictionary:[super xmlAttributes]];
	xmlAttributes[@"accountType"] = @"NSString";
	xmlAttributes[@"aid"] = @"NSString";
	xmlAttributes[@"authCode"] = @"NSString";
	xmlAttributes[@"basketId"] = @"NSString";
	xmlAttributes[@"basketTenderId"] = @"NSString";
	xmlAttributes[@"batchNumber"] = @"NSString";
	xmlAttributes[@"cardExpiryDate"] = @"NSString";
	xmlAttributes[@"cashAmount"] = @"NSString";
	xmlAttributes[@"cic"] = @"NSString";
	xmlAttributes[@"convertedDCCAmount"] = @"NSString";
	xmlAttributes[@"currencyConversionRate"] = @"NSString";
	xmlAttributes[@"currencyName"] = @"NSString";
	xmlAttributes[@"customerDeclaration"] = @"NSString";
	xmlAttributes[@"cvm"] = @"NSString";
	xmlAttributes[@"cvvResponse"] = @"NSString";
	xmlAttributes[@"eftSequenceNumber"] = @"NSString";
	xmlAttributes[@"emvCardHolderName"] = @"NSString";
	xmlAttributes[@"emvCardHolderNameExtended"] = @"NSString";
	xmlAttributes[@"emvEffectiveDate"] = @"NSString";
	xmlAttributes[@"emvLabel"] = @"NSString";
	xmlAttributes[@"gcBalance"] = @"NSString";
	xmlAttributes[@"gratuityAmount"] = @"NSString";
	xmlAttributes[@"hasReply"] = @"BOOL";
	xmlAttributes[@"loyaltyCode"] = @"NSString";
	xmlAttributes[@"maskedPAN"] = @"NSString";
	xmlAttributes[@"merchantAddress"] = @"NSString";
	xmlAttributes[@"merchantId"] = @"NSString";
	xmlAttributes[@"merchantName"] = @"NSString";
	xmlAttributes[@"mswr"] = @"NSString";
	xmlAttributes[@"panOrIssue"] = @"NSString";
	xmlAttributes[@"panSHA1"] = @"NSString";
	xmlAttributes[@"pdAmount"] = @"NSString";
	xmlAttributes[@"pem"] = @"NSString";
	xmlAttributes[@"pgtr"] = @"NSString";
	xmlAttributes[@"receiptNumber"] = @"NSString";
	xmlAttributes[@"refPhone1"] = @"NSString";
	xmlAttributes[@"refPhone2"] = @"NSString";
	xmlAttributes[@"refundCounts"] = @"NSString";
	xmlAttributes[@"retentionReminder"] = @"NSString";
	xmlAttributes[@"rrn"] = @"NSString";
	xmlAttributes[@"salesCount"] = @"NSString";
	xmlAttributes[@"startDate"] = @"NSString";
	xmlAttributes[@"terminalId"] = @"NSString";
	xmlAttributes[@"totalAmount"] = @"NSString";
	xmlAttributes[@"totalRefundAmount"] = @"NSString";
	xmlAttributes[@"totalSaleAmount"] = @"NSString";
	xmlAttributes[@"transDate"] = @"NSString";
	xmlAttributes[@"transReference"] = @"NSString";
	xmlAttributes[@"transResult"] = @"NSString";
	xmlAttributes[@"transStatusInfo"] = @"NSString";
	xmlAttributes[@"transTime"] = @"NSString";
	xmlAttributes[@"transType"] = @"NSString";
	xmlAttributes[@"transVerificationResult"] = @"NSString";
	return xmlAttributes;
}

-(NSDictionary*)objectAttributes
{
	NSMutableDictionary *objectAttributes = [NSMutableDictionary dictionaryWithDictionary:[super objectAttributes]];
	objectAttributes[@"accountType"] = @"accountType";
	objectAttributes[@"aid"] = @"aid";
	objectAttributes[@"authCode"] = @"authCode";
	objectAttributes[@"basketId"] = @"basketId";
	objectAttributes[@"basketTenderId"] = @"basketTenderId";
	objectAttributes[@"batchNumber"] = @"batchNumber";
	objectAttributes[@"cardExpiryDate"] = @"cardExpiryDate";
	objectAttributes[@"cashAmount"] = @"cashAmount";
	objectAttributes[@"cic"] = @"cic";
	objectAttributes[@"convertedDCCAmount"] = @"convertedDCCAmount";
	objectAttributes[@"currencyConversionRate"] = @"currencyConversionRate";
	objectAttributes[@"currencyName"] = @"currencyName";
	objectAttributes[@"customerDeclaration"] = @"customerDeclaration";
	objectAttributes[@"cvm"] = @"cvm";
	objectAttributes[@"cvvResponse"] = @"cvvResponse";
	objectAttributes[@"eftSequenceNumber"] = @"eftSequenceNumber";
	objectAttributes[@"emvCardHolderName"] = @"emvCardHolderName";
	objectAttributes[@"emvCardHolderNameExtended"] = @"emvCardHolderNameExtended";
	objectAttributes[@"emvEffectiveDate"] = @"emvEffectiveDate";
	objectAttributes[@"emvLabel"] = @"emvLabel";
	objectAttributes[@"gcBalance"] = @"gcBalance";
	objectAttributes[@"gratuityAmount"] = @"gratuityAmount";
	objectAttributes[@"hasReply"] = @"hasReply";
	objectAttributes[@"loyaltyCode"] = @"loyaltyCode";
	objectAttributes[@"maskedPAN"] = @"maskedPAN";
	objectAttributes[@"merchantAddress"] = @"merchantAddress";
	objectAttributes[@"merchantId"] = @"merchantId";
	objectAttributes[@"merchantName"] = @"merchantName";
	objectAttributes[@"mswr"] = @"mswr";
	objectAttributes[@"panOrIssue"] = @"panOrIssue";
	objectAttributes[@"panSHA1"] = @"panSHA1";
	objectAttributes[@"pdAmount"] = @"pdAmount";
	objectAttributes[@"pem"] = @"pem";
	objectAttributes[@"pgtr"] = @"pgtr";
	objectAttributes[@"receiptNumber"] = @"receiptNumber";
	objectAttributes[@"refPhone1"] = @"refPhone1";
	objectAttributes[@"refPhone2"] = @"refPhone2";
	objectAttributes[@"refundCounts"] = @"refundCounts";
	objectAttributes[@"retentionReminder"] = @"retentionReminder";
	objectAttributes[@"rrn"] = @"rrn";
	objectAttributes[@"salesCount"] = @"salesCount";
	objectAttributes[@"startDate"] = @"startDate";
	objectAttributes[@"terminalId"] = @"terminalId";
	objectAttributes[@"totalAmount"] = @"totalAmount";
	objectAttributes[@"totalRefundAmount"] = @"totalRefundAmount";
	objectAttributes[@"totalSaleAmount"] = @"totalSaleAmount";
	objectAttributes[@"transDate"] = @"transDate";
	objectAttributes[@"transReference"] = @"transReference";
	objectAttributes[@"transResult"] = @"transResult";
	objectAttributes[@"transStatusInfo"] = @"transStatusInfo";
	objectAttributes[@"transTime"] = @"transTime";
	objectAttributes[@"transType"] = @"transType";
	objectAttributes[@"transVerificationResult"] = @"transVerificationResult";
	return objectAttributes;
}
@end

@implementation CustomerEmail

-(NSString*)xmlTag
{
	return @"customerEmail";
}

+(NSString*)contextName
{
	return @"CUSTOMEREMAIL";
}

-(NSDictionary*)xmlAttributes
{
	NSMutableDictionary *xmlAttributes = [NSMutableDictionary dictionaryWithDictionary:[super xmlAttributes]];
	xmlAttributes[@"address"] = @"NSString";
	return xmlAttributes;
}

-(NSDictionary*)objectAttributes
{
	NSMutableDictionary *objectAttributes = [NSMutableDictionary dictionaryWithDictionary:[super objectAttributes]];
	objectAttributes[@"address"] = @"address";
	return objectAttributes;
}
@end

@implementation PrintDataLine

-(NSString*)xmlTag
{
	return @"printDataLine";
}

+(NSString*)contextName
{
	return @"PRINTDATALINE";
}

-(NSDictionary*)xmlAttributes
{
	NSMutableDictionary *xmlAttributes = [NSMutableDictionary dictionaryWithDictionary:[super xmlAttributes]];
	xmlAttributes[@"printData"] = @"NSString";
	xmlAttributes[@"printed"] = @"BOOL";
	xmlAttributes[@"printResult"] = @"NSString";
	return xmlAttributes;
}

-(NSDictionary*)objectAttributes
{
	NSMutableDictionary *objectAttributes = [NSMutableDictionary dictionaryWithDictionary:[super objectAttributes]];
	objectAttributes[@"printData"] = @"printData";
	objectAttributes[@"printed"] = @"printed";
	objectAttributes[@"printResult"] = @"printResult";
	return objectAttributes;
}
@end

@implementation ReturnItem

-(NSInteger)originalTransactionIdValue {return [self.originalTransactionId integerValue];}
-(void)setOriginalTransactionIdValue:(NSInteger)originalTransactionIdValue {self.originalTransactionId = @(originalTransactionIdValue);}
-(NSInteger)originalTransactionStoreIdValue {return [self.originalTransactionStoreId integerValue];}
-(void)setOriginalTransactionStoreIdValue:(NSInteger)originalTransactionStoreIdValue {self.originalTransactionStoreId = @(originalTransactionStoreIdValue);}
-(NSInteger)originalTransactionTillValue {return [self.originalTransactionTill integerValue];}
-(void)setOriginalTransactionTillValue:(NSInteger)originalTransactionTillValue {self.originalTransactionTill = @(originalTransactionTillValue);}
-(NSString*)xmlTag
{
	return @"returnItem";
}

+(NSString*)contextName
{
	return @"RETURNITEM";
}

-(NSDictionary*)xmlAttributes
{
	NSMutableDictionary *xmlAttributes = [NSMutableDictionary dictionaryWithDictionary:[super xmlAttributes]];
	xmlAttributes[@"originalTransactionBarcode"] = @"NSString";
	xmlAttributes[@"originalTransactionDate"] = @"NSDate";
	xmlAttributes[@"originalTransactionId"] = @"NSInteger";
	xmlAttributes[@"originalTransactionStoreId"] = @"NSInteger";
	xmlAttributes[@"originalTransactionTill"] = @"NSInteger";
	xmlAttributes[@"reasonId"] = @"NSString";
	return xmlAttributes;
}

-(NSDictionary*)objectAttributes
{
	NSMutableDictionary *objectAttributes = [NSMutableDictionary dictionaryWithDictionary:[super objectAttributes]];
	objectAttributes[@"originalTransactionBarcode"] = @"originalTransactionBarcode";
	objectAttributes[@"originalTransactionDate"] = @"originalTransactionDate";
	objectAttributes[@"originalTransactionId"] = @"originalTransactionId";
	objectAttributes[@"originalTransactionStoreId"] = @"originalTransactionStoreId";
	objectAttributes[@"originalTransactionTill"] = @"originalTransactionTill";
	objectAttributes[@"reasonId"] = @"reasonId";
	return objectAttributes;
}
@end

@implementation SaleItem

-(BOOL)generateGiftReceiptValue {return [self.generateGiftReceipt boolValue];}
-(void)setGenerateGiftReceiptValue:(BOOL)generateGiftReceiptValue {self.generateGiftReceipt = @(generateGiftReceiptValue);}
-(BOOL)originalGenerateGiftReceiptValue {return [self.originalGenerateGiftReceipt boolValue];}
-(void)setOriginalGenerateGiftReceiptValue:(BOOL)originalGenerateGiftReceiptValue {self.originalGenerateGiftReceipt = @(originalGenerateGiftReceiptValue);}
-(NSString*)xmlTag
{
	return @"saleItem";
}

+(NSString*)contextName
{
	return @"SALEITEM";
}

-(NSDictionary*)xmlAttributes
{
	NSMutableDictionary *xmlAttributes = [NSMutableDictionary dictionaryWithDictionary:[super xmlAttributes]];
	xmlAttributes[@"generateGiftReceipt"] = @"BOOL";
	xmlAttributes[@"measure"] = @"NSString";
	xmlAttributes[@"originalGenerateGiftReceipt"] = @"BOOL";
	return xmlAttributes;
}

-(NSDictionary*)objectAttributes
{
	NSMutableDictionary *objectAttributes = [NSMutableDictionary dictionaryWithDictionary:[super objectAttributes]];
	objectAttributes[@"generateGiftReceipt"] = @"generateGiftReceipt";
	objectAttributes[@"measure"] = @"measure";
	objectAttributes[@"originalGenerateGiftReceipt"] = @"originalGenerateGiftReceipt";
	return objectAttributes;
}
@end

@implementation ReturnableSaleItem

-(NSString*)xmlTag
{
	return @"returnableSaleItem";
}

+(NSString*)contextName
{
	return @"RETURNABLESALEITEM";
}

-(NSDictionary*)xmlAttributes
{
	NSMutableDictionary *xmlAttributes = [NSMutableDictionary dictionaryWithDictionary:[super xmlAttributes]];
	xmlAttributes[@"returnedQuantity"] = @"NSString";
	xmlAttributes[@"returnInstances"] = @"NSArray";
	return xmlAttributes;
}

-(NSDictionary*)objectAttributes
{
	NSMutableDictionary *objectAttributes = [NSMutableDictionary dictionaryWithDictionary:[super objectAttributes]];
	objectAttributes[@"returnedQuantity"] = @"returnedQuantity";
	objectAttributes[@"returnInstances"] = @"returnInstances";
	return objectAttributes;
}
@end

@implementation BasketPricingDTO

-(NSString*)xmlTag
{
	return @"basketPricing";
}

+(NSString*)contextName
{
	return @"BASKETPRICING";
}

-(NSDictionary*)xmlAttributes
{
	NSMutableDictionary *xmlAttributes = [NSMutableDictionary dictionaryWithDictionary:[super xmlAttributes]];
	xmlAttributes[@"amount"] = @"NSString";
	xmlAttributes[@"basketTender"] = @"BasketTender";
	xmlAttributes[@"display"] = @"NSString";
	return xmlAttributes;
}

-(NSDictionary*)objectAttributes
{
	NSMutableDictionary *objectAttributes = [NSMutableDictionary dictionaryWithDictionary:[super objectAttributes]];
	objectAttributes[@"amount"] = @"amount";
	objectAttributes[@"basketTender"] = @"basketTender";
	objectAttributes[@"display"] = @"display";
	return objectAttributes;
}
@end

@implementation BasketRewardLine

-(NSString*)xmlTag
{
	return @"basketReward";
}

+(NSString*)contextName
{
	return @"BASKETREWARDLINE";
}

-(NSDictionary*)xmlAttributes
{
	NSMutableDictionary *xmlAttributes = [NSMutableDictionary dictionaryWithDictionary:[super xmlAttributes]];
	xmlAttributes[@"basket"] = @"BasketAssociateDTO";
	xmlAttributes[@"basketItem"] = @"BasketItem";
	xmlAttributes[@"code"] = @"id<NSCoding>";
	xmlAttributes[@"reason"] = @"id<NSCoding>";
	xmlAttributes[@"rewardAmount"] = @"BasketPricingDTO";
	xmlAttributes[@"rewardId"] = @"NSString";
	xmlAttributes[@"rewardText"] = @"NSString";
	xmlAttributes[@"rewardType"] = @"NSString";
	return xmlAttributes;
}

-(NSDictionary*)objectAttributes
{
	NSMutableDictionary *objectAttributes = [NSMutableDictionary dictionaryWithDictionary:[super objectAttributes]];
	objectAttributes[@"basket"] = @"basket";
	objectAttributes[@"basketItem"] = @"basketItem";
	objectAttributes[@"code"] = @"code";
	objectAttributes[@"reason"] = @"reason";
	objectAttributes[@"rewardAmount"] = @"rewardAmount";
	objectAttributes[@"rewardId"] = @"rewardId";
	objectAttributes[@"rewardText"] = @"rewardText";
	objectAttributes[@"rewardType"] = @"rewardType";
	return objectAttributes;
}
@end

@implementation BasketRewardLineDiscountAmount

-(NSString*)xmlTag
{
	return @"lineDiscountAmount";
}

+(NSString*)contextName
{
	return @"BASKETREWARDLINEDISCOUNTAMOUNT";
}

-(NSDictionary*)xmlAttributes
{
	NSMutableDictionary *xmlAttributes = [NSMutableDictionary dictionaryWithDictionary:[super xmlAttributes]];
	return xmlAttributes;
}

-(NSDictionary*)objectAttributes
{
	NSMutableDictionary *objectAttributes = [NSMutableDictionary dictionaryWithDictionary:[super objectAttributes]];
	return objectAttributes;
}
@end

@implementation BasketRewardLineDiscountPercent

-(NSString*)xmlTag
{
	return @"lineDiscountPercent";
}

+(NSString*)contextName
{
	return @"BASKETREWARDLINEDISCOUNTPERCENT";
}

-(NSDictionary*)xmlAttributes
{
	NSMutableDictionary *xmlAttributes = [NSMutableDictionary dictionaryWithDictionary:[super xmlAttributes]];
	return xmlAttributes;
}

-(NSDictionary*)objectAttributes
{
	NSMutableDictionary *objectAttributes = [NSMutableDictionary dictionaryWithDictionary:[super objectAttributes]];
	return objectAttributes;
}
@end

@implementation BasketRewardLinePriceAdjust

-(NSString*)xmlTag
{
	return @"linePriceAdjust";
}

+(NSString*)contextName
{
	return @"BASKETREWARDLINEPRICEADJUST";
}

-(NSDictionary*)xmlAttributes
{
	NSMutableDictionary *xmlAttributes = [NSMutableDictionary dictionaryWithDictionary:[super xmlAttributes]];
	return xmlAttributes;
}

-(NSDictionary*)objectAttributes
{
	NSMutableDictionary *objectAttributes = [NSMutableDictionary dictionaryWithDictionary:[super objectAttributes]];
	return objectAttributes;
}
@end

@implementation BasketTaxFree

-(NSString*)xmlTag
{
	return @"taxFree";
}

+(NSString*)contextName
{
	return @"BASKETTAXFREE";
}

-(NSDictionary*)xmlAttributes
{
	NSMutableDictionary *xmlAttributes = [NSMutableDictionary dictionaryWithDictionary:[super xmlAttributes]];
	xmlAttributes[@"basket"] = @"BasketDTO";
	xmlAttributes[@"documentId"] = @"NSString";
	xmlAttributes[@"documentType"] = @"NSString";
	return xmlAttributes;
}

-(NSDictionary*)objectAttributes
{
	NSMutableDictionary *objectAttributes = [NSMutableDictionary dictionaryWithDictionary:[super objectAttributes]];
	objectAttributes[@"basket"] = @"basket";
	objectAttributes[@"documentId"] = @"documentId";
	objectAttributes[@"documentType"] = @"documentType";
	return objectAttributes;
}

-(NSString*)parentAttribute
{
	return @"basket";
}
@end

@implementation BasketTenderAuthorization

-(NSString*)xmlTag
{
	return @"authorization";
}

+(NSString*)contextName
{
	return @"BASKETTENDERAUTHORIZATION";
}

-(NSDictionary*)xmlAttributes
{
	NSMutableDictionary *xmlAttributes = [NSMutableDictionary dictionaryWithDictionary:[super xmlAttributes]];
	xmlAttributes[@"authorizationCode"] = @"NSString";
	xmlAttributes[@"providerVoided"] = @"BOOL";
	xmlAttributes[@"referenceNumber"] = @"NSString";
	return xmlAttributes;
}

-(NSDictionary*)objectAttributes
{
	NSMutableDictionary *objectAttributes = [NSMutableDictionary dictionaryWithDictionary:[super objectAttributes]];
	objectAttributes[@"authorizationCode"] = @"authorizationCode";
	objectAttributes[@"providerVoided"] = @"providerVoided";
	objectAttributes[@"referenceNumber"] = @"referenceNumber";
	return objectAttributes;
}
@end

@implementation BasketTenderForeignCurrency

-(NSString*)xmlTag
{
	return @"basketTenderForeignCurrency";
}

+(NSString*)contextName
{
	return @"BASKETTENDERFOREIGNCURRENCY";
}

-(NSDictionary*)xmlAttributes
{
	NSMutableDictionary *xmlAttributes = [NSMutableDictionary dictionaryWithDictionary:[super xmlAttributes]];
	xmlAttributes[@"amount"] = @"NSString";
	xmlAttributes[@"basketTender"] = @"BasketTender";
	xmlAttributes[@"currencyCode"] = @"NSString";
	xmlAttributes[@"displayAmount"] = @"NSString";
	xmlAttributes[@"rate"] = @"NSString";
	return xmlAttributes;
}

-(NSDictionary*)objectAttributes
{
	NSMutableDictionary *objectAttributes = [NSMutableDictionary dictionaryWithDictionary:[super objectAttributes]];
	objectAttributes[@"amount"] = @"amount";
	objectAttributes[@"basketTender"] = @"basketTender";
	objectAttributes[@"currencyCode"] = @"currencyCode";
	objectAttributes[@"displayAmount"] = @"displayAmount";
	objectAttributes[@"rate"] = @"rate";
	return objectAttributes;
}
@end

@implementation ClientSettings

-(NSString*)xmlTag
{
	return @"clientSettings";
}

+(NSString*)contextName
{
	return @"CLIENTSETTINGS";
}

-(NSDictionary*)xmlAttributes
{
	NSMutableDictionary *xmlAttributes = [NSMutableDictionary dictionaryWithDictionary:[super xmlAttributes]];
	xmlAttributes[@"adminPinCode"] = @"NSString";
	xmlAttributes[@"authURL"] = @"NSString";
	xmlAttributes[@"autoPrint"] = @"BOOL";
	xmlAttributes[@"basketLayout"] = @"NSString";
	xmlAttributes[@"basketSuspendDescriptionPrompt"] = @"BOOL";
	xmlAttributes[@"DRAFT_discountUnitPercentageDecimalDigits"] = @"NSInteger";
	xmlAttributes[@"DRAFT_finishPredicate"] = @"NSString";
	xmlAttributes[@"DRAFT_finishPredicateMessage"] = @"NSString";
	xmlAttributes[@"DRAFT_fontFamily"] = @"NSString";
	xmlAttributes[@"DRAFT_keyboardAppearance"] = @"NSString";
	xmlAttributes[@"logoutAfterInactivityPeriod"] = @"NSInteger";
	xmlAttributes[@"logoutAfterSaleComplete"] = @"BOOL";
	xmlAttributes[@"maxItemQuantity"] = @"NSString";
	xmlAttributes[@"nativeCurrencyCode"] = @"NSString";
	xmlAttributes[@"DRAFT_numberPadIsReversed"] = @"BOOL";
	xmlAttributes[@"DRAFT_preprintReceiptImage"] = @"NSString";
	xmlAttributes[@"presetTip1"] = @"NSNumber";
	xmlAttributes[@"presetTip2"] = @"NSNumber";
	xmlAttributes[@"DRAFT_primaryColor"] = @"NSString";
	xmlAttributes[@"printJobDelay"] = @"double";
	xmlAttributes[@"promptForDumpCodeDescription"] = @"BOOL";
	xmlAttributes[@"promptQtyForDumpCode"] = @"BOOL";
	xmlAttributes[@"receiptManager"] = @"NSString";
	xmlAttributes[@"receiptMethods"] = @"NSString";
	xmlAttributes[@"releaseAuthSessionUrl"] = @"NSString";
	xmlAttributes[@"salesAssociateIDMask"] = @"NSString";
	xmlAttributes[@"scanMode"] = @"NSString";
	xmlAttributes[@"DRAFT_secondaryColor"] = @"NSString";
	xmlAttributes[@"signOnMethod"] = @"NSString";
	xmlAttributes[@"DRAFT_verifoneAppName"] = @"NSString";
	xmlAttributes[@"DRAFT_verifoneBypassConfirmation"] = @"BOOL";
	xmlAttributes[@"DRAFT_verifonePIN"] = @"NSString";
	return xmlAttributes;
}

-(NSDictionary*)objectAttributes
{
	NSMutableDictionary *objectAttributes = [NSMutableDictionary dictionaryWithDictionary:[super objectAttributes]];
	objectAttributes[@"adminPinCode"] = @"adminPinCode";
	objectAttributes[@"authURL"] = @"authURL";
	objectAttributes[@"autoPrint"] = @"autoPrint";
	objectAttributes[@"basketLayout"] = @"basketLayout";
	objectAttributes[@"basketSuspendDescriptionPrompt"] = @"basketSuspendDescriptionPrompt";
	objectAttributes[@"DRAFT_discountUnitPercentageDecimalDigits"] = @"DRAFT_discountUnitPercentageDecimalDigits";
	objectAttributes[@"DRAFT_finishPredicate"] = @"DRAFT_finishPredicate";
	objectAttributes[@"DRAFT_finishPredicateMessage"] = @"DRAFT_finishPredicateMessage";
	objectAttributes[@"DRAFT_fontFamily"] = @"DRAFT_fontFamily";
	objectAttributes[@"DRAFT_keyboardAppearance"] = @"DRAFT_keyboardAppearance";
	objectAttributes[@"logoutAfterInactivityPeriod"] = @"logoutAfterInactivityPeriod";
	objectAttributes[@"logoutAfterSaleComplete"] = @"logoutAfterSaleComplete";
	objectAttributes[@"maxItemQuantity"] = @"maxItemQuantity";
	objectAttributes[@"nativeCurrencyCode"] = @"nativeCurrencyCode";
	objectAttributes[@"DRAFT_numberPadIsReversed"] = @"DRAFT_numberPadIsReversed";
	objectAttributes[@"DRAFT_preprintReceiptImage"] = @"DRAFT_preprintReceiptImage";
	objectAttributes[@"presetTip1"] = @"presetTip1";
	objectAttributes[@"presetTip2"] = @"presetTip2";
	objectAttributes[@"DRAFT_primaryColor"] = @"DRAFT_primaryColor";
	objectAttributes[@"printJobDelay"] = @"printJobDelay";
	objectAttributes[@"promptForDumpCodeDescription"] = @"promptForDumpCodeDescription";
	objectAttributes[@"promptQtyForDumpCode"] = @"promptQtyForDumpCode";
	objectAttributes[@"receiptManager"] = @"receiptManager";
	objectAttributes[@"receiptMethods"] = @"receiptMethods";
	objectAttributes[@"releaseAuthSessionUrl"] = @"releaseAuthSessionUrl";
	objectAttributes[@"salesAssociateIDMask"] = @"salesAssociateIDMask";
	objectAttributes[@"scanMode"] = @"scanMode";
	objectAttributes[@"DRAFT_secondaryColor"] = @"DRAFT_secondaryColor";
	objectAttributes[@"signOnMethod"] = @"signOnMethod";
	objectAttributes[@"DRAFT_verifoneAppName"] = @"DRAFT_verifoneAppName";
	objectAttributes[@"DRAFT_verifoneBypassConfirmation"] = @"DRAFT_verifoneBypassConfirmation";
	objectAttributes[@"DRAFT_verifonePIN"] = @"DRAFT_verifonePIN";
	return objectAttributes;
}
@end

@implementation Configuration

-(NSString*)xmlTag
{
	return @"configuration";
}

+(NSString*)contextName
{
	return @"CONFIGURATION";
}

-(NSDictionary*)xmlAttributes
{
	NSMutableDictionary *xmlAttributes = [NSMutableDictionary dictionaryWithDictionary:[super xmlAttributes]];
	xmlAttributes[@"affiliations"] = @"NSArray";
	xmlAttributes[@"airsideOption"] = @"AirsideOption";
	xmlAttributes[@"clientSettings"] = @"ClientSettings";
	xmlAttributes[@"countries"] = @"NSArray";
	xmlAttributes[@"customerIdentificationTypes"] = @"NSArray";
	xmlAttributes[@"devices"] = @"NSArray";
	xmlAttributes[@"discountCodes"] = @"NSArray";
	xmlAttributes[@"dumpCodes"] = @"NSArray";
	xmlAttributes[@"inputFields"] = @"NSArray";
	xmlAttributes[@"itemModifierGroups"] = @"NSArray";
	xmlAttributes[@"keyValueGroups"] = @"NSArray";
	xmlAttributes[@"localizableTables"] = @"NSArray";
	xmlAttributes[@"passwordRules"] = @"NSArray";
	xmlAttributes[@"permissionGroups"] = @"NSArray";
	xmlAttributes[@"posErrors"] = @"NSArray";
	xmlAttributes[@"reasonGroups"] = @"NSArray";
	xmlAttributes[@"softKeyContexts"] = @"NSArray";
	xmlAttributes[@"softKeys"] = @"NSArray";
	xmlAttributes[@"tenders"] = @"NSArray";
	return xmlAttributes;
}

-(NSDictionary*)objectAttributes
{
	NSMutableDictionary *objectAttributes = [NSMutableDictionary dictionaryWithDictionary:[super objectAttributes]];
	objectAttributes[@"affiliations"] = @"affiliations";
	objectAttributes[@"airsideOption"] = @"airsideOption";
	objectAttributes[@"clientSettings"] = @"clientSettings";
	objectAttributes[@"countries"] = @"countries";
	objectAttributes[@"customerIdentificationTypes"] = @"customerIdentificationTypes";
	objectAttributes[@"devices"] = @"devices";
	objectAttributes[@"discountCodes"] = @"discountCodes";
	objectAttributes[@"dumpCodes"] = @"dumpCodes";
	objectAttributes[@"inputFields"] = @"inputFields";
	objectAttributes[@"itemModifierGroups"] = @"itemModifierGroups";
	objectAttributes[@"keyValueGroups"] = @"keyValueGroups";
	objectAttributes[@"localizableTables"] = @"localizableTables";
	objectAttributes[@"passwordRules"] = @"passwordRules";
	objectAttributes[@"permissionGroups"] = @"permissionGroups";
	objectAttributes[@"posErrors"] = @"posErrors";
	objectAttributes[@"reasonGroups"] = @"reasonGroups";
	objectAttributes[@"softKeyContexts"] = @"softKeyContexts";
	objectAttributes[@"softKeys"] = @"softKeys";
	objectAttributes[@"tenders"] = @"tenders";
	return objectAttributes;
}
@end

@implementation Country

-(NSString*)xmlTag
{
	return @"country";
}

+(NSString*)contextName
{
	return @"COUNTRY";
}

-(NSDictionary*)xmlAttributes
{
	NSMutableDictionary *xmlAttributes = [NSMutableDictionary dictionaryWithDictionary:[super xmlAttributes]];
	xmlAttributes[@"countryId"] = @"NSString";
	xmlAttributes[@"countryLanguages"] = @"NSArray";
	return xmlAttributes;
}

-(NSDictionary*)objectAttributes
{
	NSMutableDictionary *objectAttributes = [NSMutableDictionary dictionaryWithDictionary:[super objectAttributes]];
	objectAttributes[@"countryId"] = @"countryId";
	objectAttributes[@"countryLanguages"] = @"countryLanguages";
	return objectAttributes;
}
@end

@implementation Credentials

-(NSString*)xmlTag
{
	return @"credentials";
}

+(NSString*)contextName
{
	return @"CREDENTIALS";
}

-(NSDictionary*)xmlAttributes
{
	NSMutableDictionary *xmlAttributes = [NSMutableDictionary dictionaryWithDictionary:[super xmlAttributes]];
	xmlAttributes[@"code"] = @"NSString";
	xmlAttributes[@"deviceId"] = @"NSString";
	xmlAttributes[@"displayName"] = @"NSString";
	xmlAttributes[@"password"] = @"NSString";
	xmlAttributes[@"permissionsGroup"] = @"NSString";
	xmlAttributes[@"preferredLanguage"] = @"NSString";
	xmlAttributes[@"storeId"] = @"NSString";
	xmlAttributes[@"tillNumber"] = @"NSString";
	xmlAttributes[@"token"] = @"NSString";
	xmlAttributes[@"track1"] = @"NSString";
	xmlAttributes[@"track2"] = @"NSString";
	xmlAttributes[@"track3"] = @"NSString";
	xmlAttributes[@"trainingMode"] = @"BOOL";
	xmlAttributes[@"updatedPassword"] = @"NSString";
	xmlAttributes[@"username"] = @"NSString";
	return xmlAttributes;
}

-(NSDictionary*)objectAttributes
{
	NSMutableDictionary *objectAttributes = [NSMutableDictionary dictionaryWithDictionary:[super objectAttributes]];
	objectAttributes[@"code"] = @"code";
	objectAttributes[@"deviceId"] = @"deviceId";
	objectAttributes[@"displayName"] = @"displayName";
	objectAttributes[@"password"] = @"password";
	objectAttributes[@"permissionsGroup"] = @"permissionsGroup";
	objectAttributes[@"preferredLanguage"] = @"preferredLanguage";
	objectAttributes[@"storeId"] = @"storeId";
	objectAttributes[@"tillNumber"] = @"tillNumber";
	objectAttributes[@"token"] = @"token";
	objectAttributes[@"track1"] = @"track1";
	objectAttributes[@"track2"] = @"track2";
	objectAttributes[@"track3"] = @"track3";
	objectAttributes[@"trainingMode"] = @"trainingMode";
	objectAttributes[@"updatedPassword"] = @"updatedPassword";
	objectAttributes[@"username"] = @"username";
	return objectAttributes;
}
@end

@implementation Customer

-(BOOL)cardsRefreshRequiredValue {return [self.cardsRefreshRequired boolValue];}
-(void)setCardsRefreshRequiredValue:(BOOL)cardsRefreshRequiredValue {self.cardsRefreshRequired = @(cardsRefreshRequiredValue);}
-(BOOL)editableValue {return [self.editable boolValue];}
-(void)setEditableValue:(BOOL)editableValue {self.editable = @(editableValue);}
-(BOOL)hasCustomerChangedValue {return [self.hasCustomerChanged boolValue];}
-(void)setHasCustomerChangedValue:(BOOL)hasCustomerChangedValue {self.hasCustomerChanged = @(hasCustomerChangedValue);}
-(BOOL)hasWishlistChangedValue {return [self.hasWishlistChanged boolValue];}
-(void)setHasWishlistChangedValue:(BOOL)hasWishlistChangedValue {self.hasWishlistChanged = @(hasWishlistChangedValue);}
-(BOOL)notesRefreshRequiredValue {return [self.notesRefreshRequired boolValue];}
-(void)setNotesRefreshRequiredValue:(BOOL)notesRefreshRequiredValue {self.notesRefreshRequired = @(notesRefreshRequiredValue);}
-(BOOL)walletUpdateRequiredValue {return [self.walletUpdateRequired boolValue];}
-(void)setWalletUpdateRequiredValue:(BOOL)walletUpdateRequiredValue {self.walletUpdateRequired = @(walletUpdateRequiredValue);}
-(BOOL)wishlistRefreshRequiredValue {return [self.wishlistRefreshRequired boolValue];}
-(void)setWishlistRefreshRequiredValue:(BOOL)wishlistRefreshRequiredValue {self.wishlistRefreshRequired = @(wishlistRefreshRequiredValue);}
-(NSString*)xmlTag
{
	return @"customer";
}

+(NSString*)contextName
{
	return @"CUSTOMER";
}

-(NSDictionary*)xmlAttributes
{
	NSMutableDictionary *xmlAttributes = [NSMutableDictionary dictionaryWithDictionary:[super xmlAttributes]];
	xmlAttributes[@"address"] = @"CustomerAddress";
	xmlAttributes[@"baskets"] = @"NSArray";
	xmlAttributes[@"cardNumber"] = @"NSString";
	xmlAttributes[@"cardsRefreshError"] = @"ServerError";
	xmlAttributes[@"cardsRefreshRequired"] = @"BOOL";
	xmlAttributes[@"customerCards"] = @"NSArray";
	xmlAttributes[@"customerChangeError"] = @"ServerError";
	xmlAttributes[@"customerId"] = @"NSString";
	xmlAttributes[@"editable"] = @"BOOL";
	xmlAttributes[@"email"] = @"NSString";
	xmlAttributes[@"firstName"] = @"NSString";
	xmlAttributes[@"gender"] = @"NSString";
	xmlAttributes[@"hasCustomerChanged"] = @"BOOL";
	xmlAttributes[@"hasWishlistChanged"] = @"BOOL";
	xmlAttributes[@"lastName"] = @"NSString";
	xmlAttributes[@"notes"] = @"NSString";
	xmlAttributes[@"notesRefreshError"] = @"ServerError";
	xmlAttributes[@"notesRefreshRequired"] = @"BOOL";
	xmlAttributes[@"phone1"] = @"NSString";
	xmlAttributes[@"salutation"] = @"NSString";
	xmlAttributes[@"search"] = @"NSArray";
	xmlAttributes[@"wallets"] = @"NSArray";
	xmlAttributes[@"walletUpdateRequired"] = @"BOOL";
	xmlAttributes[@"wishList"] = @"NSArray";
	xmlAttributes[@"wishlistRefreshRequired"] = @"BOOL";
	return xmlAttributes;
}

-(NSDictionary*)objectAttributes
{
	NSMutableDictionary *objectAttributes = [NSMutableDictionary dictionaryWithDictionary:[super objectAttributes]];
	objectAttributes[@"address"] = @"address";
	objectAttributes[@"baskets"] = @"baskets";
	objectAttributes[@"cardNumber"] = @"cardNumber";
	objectAttributes[@"cardsRefreshError"] = @"cardsRefreshError";
	objectAttributes[@"cardsRefreshRequired"] = @"cardsRefreshRequired";
	objectAttributes[@"customerCards"] = @"customerCards";
	objectAttributes[@"customerChangeError"] = @"customerChangeError";
	objectAttributes[@"customerId"] = @"customerId";
	objectAttributes[@"editable"] = @"editable";
	objectAttributes[@"email"] = @"email";
	objectAttributes[@"firstName"] = @"firstName";
	objectAttributes[@"gender"] = @"gender";
	objectAttributes[@"hasCustomerChanged"] = @"hasCustomerChanged";
	objectAttributes[@"hasWishlistChanged"] = @"hasWishlistChanged";
	objectAttributes[@"lastName"] = @"lastName";
	objectAttributes[@"notes"] = @"notes";
	objectAttributes[@"notesRefreshError"] = @"notesRefreshError";
	objectAttributes[@"notesRefreshRequired"] = @"notesRefreshRequired";
	objectAttributes[@"phone1"] = @"phone1";
	objectAttributes[@"salutation"] = @"salutation";
	objectAttributes[@"search"] = @"search";
	objectAttributes[@"wallets"] = @"wallets";
	objectAttributes[@"walletUpdateRequired"] = @"walletUpdateRequired";
	objectAttributes[@"wishList"] = @"wishList";
	objectAttributes[@"wishlistRefreshRequired"] = @"wishlistRefreshRequired";
	return objectAttributes;
}
@end

@implementation CustomerAddress

-(NSString*)xmlTag
{
	return @"customerAddress";
}

+(NSString*)contextName
{
	return @"CUSTOMERADDRESS";
}

-(NSDictionary*)xmlAttributes
{
	NSMutableDictionary *xmlAttributes = [NSMutableDictionary dictionaryWithDictionary:[super xmlAttributes]];
	xmlAttributes[@"city"] = @"NSString";
	xmlAttributes[@"country"] = @"NSString";
	xmlAttributes[@"customer"] = @"Customer";
	xmlAttributes[@"line1"] = @"NSString";
	xmlAttributes[@"line2"] = @"NSString";
	xmlAttributes[@"postcode"] = @"NSString";
	xmlAttributes[@"territory"] = @"NSString";
	return xmlAttributes;
}

-(NSDictionary*)objectAttributes
{
	NSMutableDictionary *objectAttributes = [NSMutableDictionary dictionaryWithDictionary:[super objectAttributes]];
	objectAttributes[@"city"] = @"city";
	objectAttributes[@"country"] = @"country";
	objectAttributes[@"customer"] = @"customer";
	objectAttributes[@"line1"] = @"line1";
	objectAttributes[@"line2"] = @"line2";
	objectAttributes[@"postcode"] = @"postcode";
	objectAttributes[@"territory"] = @"territory";
	return objectAttributes;
}
@end

@implementation CustomerCard

-(NSString*)xmlTag
{
	return @"customerCard";
}

+(NSString*)contextName
{
	return @"CUSTOMERCARD";
}

-(NSDictionary*)xmlAttributes
{
	NSMutableDictionary *xmlAttributes = [NSMutableDictionary dictionaryWithDictionary:[super xmlAttributes]];
	xmlAttributes[@"cardName"] = @"NSString";
	xmlAttributes[@"cardNumber"] = @"NSString";
	xmlAttributes[@"customer"] = @"Customer";
	return xmlAttributes;
}

-(NSDictionary*)objectAttributes
{
	NSMutableDictionary *objectAttributes = [NSMutableDictionary dictionaryWithDictionary:[super objectAttributes]];
	objectAttributes[@"cardName"] = @"cardName";
	objectAttributes[@"cardNumber"] = @"cardNumber";
	objectAttributes[@"customer"] = @"customer";
	return objectAttributes;
}
@end

@implementation CustomerIdentificationType

-(NSString*)xmlTag
{
	return @"customerIdentificationType";
}

+(NSString*)contextName
{
	return @"CUSTOMERIDENTIFICATIONTYPE";
}

-(NSDictionary*)xmlAttributes
{
	NSMutableDictionary *xmlAttributes = [NSMutableDictionary dictionaryWithDictionary:[super xmlAttributes]];
	xmlAttributes[@"customerIdentificationTypeDescription"] = @"NSString";
	xmlAttributes[@"descriptionLanguages"] = @"NSArray";
	xmlAttributes[@"type"] = @"NSString";
	return xmlAttributes;
}

-(NSDictionary*)objectAttributes
{
	NSMutableDictionary *objectAttributes = [NSMutableDictionary dictionaryWithDictionary:[super objectAttributes]];
	objectAttributes[@"customerIdentificationTypeDescription"] = @"customerIdentificationTypeDescription";
	objectAttributes[@"descriptionLanguages"] = @"descriptionLanguages";
	objectAttributes[@"type"] = @"type";
	return objectAttributes;
}
@end

@implementation Device

-(NSString*)xmlTag
{
	return @"device";
}

+(NSString*)contextName
{
	return @"DEVICE";
}

-(NSDictionary*)xmlAttributes
{
	NSMutableDictionary *xmlAttributes = [NSMutableDictionary dictionaryWithDictionary:[super xmlAttributes]];
	xmlAttributes[@"connection"] = @"NSString";
	xmlAttributes[@"deviceName"] = @"NSString";
	xmlAttributes[@"enabled"] = @"BOOL";
	xmlAttributes[@"manager"] = @"NSString";
	xmlAttributes[@"properties"] = @"NSArray";
	xmlAttributes[@"scanToIdentify"] = @"BOOL";
	xmlAttributes[@"selection"] = @"NSString";
	xmlAttributes[@"timeout"] = @"NSString";
	xmlAttributes[@"userSelectable"] = @"BOOL";
	return xmlAttributes;
}

-(NSDictionary*)objectAttributes
{
	NSMutableDictionary *objectAttributes = [NSMutableDictionary dictionaryWithDictionary:[super objectAttributes]];
	objectAttributes[@"connection"] = @"connection";
	objectAttributes[@"deviceName"] = @"deviceName";
	objectAttributes[@"enabled"] = @"enabled";
	objectAttributes[@"manager"] = @"manager";
	objectAttributes[@"properties"] = @"properties";
	objectAttributes[@"scanToIdentify"] = @"scanToIdentify";
	objectAttributes[@"selection"] = @"selection";
	objectAttributes[@"timeout"] = @"timeout";
	objectAttributes[@"userSelectable"] = @"userSelectable";
	return objectAttributes;
}
@end

@implementation DeviceProfile

-(NSString*)xmlTag
{
	return @"device";
}

+(NSString*)contextName
{
	return @"DEVICEPROFILE";
}

-(NSDictionary*)xmlAttributes
{
	NSMutableDictionary *xmlAttributes = [NSMutableDictionary dictionaryWithDictionary:[super xmlAttributes]];
	xmlAttributes[@"deviceId"] = @"NSString";
	xmlAttributes[@"storeId"] = @"NSString";
	xmlAttributes[@"tillNumber"] = @"NSString";
	return xmlAttributes;
}

-(NSDictionary*)objectAttributes
{
	NSMutableDictionary *objectAttributes = [NSMutableDictionary dictionaryWithDictionary:[super objectAttributes]];
	objectAttributes[@"deviceId"] = @"deviceId";
	objectAttributes[@"storeId"] = @"storeId";
	objectAttributes[@"tillNumber"] = @"tillNumber";
	return objectAttributes;
}
@end

@implementation DeviceStation

-(NSString*)xmlTag
{
	return @"devicestation";
}

+(NSString*)contextName
{
	return @"DEVICESTATION";
}

-(NSDictionary*)xmlAttributes
{
	NSMutableDictionary *xmlAttributes = [NSMutableDictionary dictionaryWithDictionary:[super xmlAttributes]];
	xmlAttributes[@"available"] = @"BOOL";
	xmlAttributes[@"claimed"] = @"BOOL";
	xmlAttributes[@"claimedUserId"] = @"NSString";
	xmlAttributes[@"claimedUserName"] = @"NSString";
	xmlAttributes[@"deviceStationId"] = @"NSString";
	xmlAttributes[@"peripherals"] = @"NSArray";
	xmlAttributes[@"terminal"] = @"NSString";
	xmlAttributes[@"tillId"] = @"NSString";
	return xmlAttributes;
}

-(NSDictionary*)objectAttributes
{
	NSMutableDictionary *objectAttributes = [NSMutableDictionary dictionaryWithDictionary:[super objectAttributes]];
	objectAttributes[@"available"] = @"available";
	objectAttributes[@"claimed"] = @"claimed";
	objectAttributes[@"claimedUserId"] = @"claimedUserId";
	objectAttributes[@"claimedUserName"] = @"claimedUserName";
	objectAttributes[@"deviceStationId"] = @"deviceStationId";
	objectAttributes[@"peripherals"] = @"peripherals";
	objectAttributes[@"terminal"] = @"terminal";
	objectAttributes[@"tillId"] = @"tillId";
	return objectAttributes;
}
@end

@implementation DeviceStationPeripheral

-(NSString*)xmlTag
{
	return @"peripheral";
}

+(NSString*)contextName
{
	return @"DEVICESTATIONPERIPHERAL";
}

-(NSDictionary*)xmlAttributes
{
	NSMutableDictionary *xmlAttributes = [NSMutableDictionary dictionaryWithDictionary:[super xmlAttributes]];
	xmlAttributes[@"codePage"] = @"NSString";
	xmlAttributes[@"networkAddress"] = @"NSString";
	xmlAttributes[@"peripheralDescription"] = @"NSString";
	xmlAttributes[@"peripheralId"] = @"NSString";
	xmlAttributes[@"peripheralStatus"] = @"NSString";
	xmlAttributes[@"peripheralType"] = @"NSString";
	return xmlAttributes;
}

-(NSDictionary*)objectAttributes
{
	NSMutableDictionary *objectAttributes = [NSMutableDictionary dictionaryWithDictionary:[super objectAttributes]];
	objectAttributes[@"codePage"] = @"codePage";
	objectAttributes[@"networkAddress"] = @"networkAddress";
	objectAttributes[@"peripheralDescription"] = @"peripheralDescription";
	objectAttributes[@"peripheralId"] = @"peripheralId";
	objectAttributes[@"peripheralStatus"] = @"peripheralStatus";
	objectAttributes[@"peripheralType"] = @"peripheralType";
	return objectAttributes;
}
@end

@implementation DiscountCode

-(NSString*)xmlTag
{
	return @"discountCode";
}

+(NSString*)contextName
{
	return @"DISCOUNTCODE";
}

-(NSDictionary*)xmlAttributes
{
	NSMutableDictionary *xmlAttributes = [NSMutableDictionary dictionaryWithDictionary:[super xmlAttributes]];
	xmlAttributes[@"affiliationName"] = @"NSString";
	xmlAttributes[@"affiliationType"] = @"NSString";
	xmlAttributes[@"discountCodeID"] = @"NSString";
	xmlAttributes[@"discountDescription"] = @"NSString";
	xmlAttributes[@"discountDescriptionLanguages"] = @"NSArray";
	xmlAttributes[@"discountType"] = @"NSString";
	xmlAttributes[@"discountValueType"] = @"NSString";
	xmlAttributes[@"isDiscountValueRequired"] = @"BOOL";
	xmlAttributes[@"isReasonRequired"] = @"BOOL";
	xmlAttributes[@"numberOfDecimalPlaces"] = @"NSNumber";
	return xmlAttributes;
}

-(NSDictionary*)objectAttributes
{
	NSMutableDictionary *objectAttributes = [NSMutableDictionary dictionaryWithDictionary:[super objectAttributes]];
	objectAttributes[@"affiliationName"] = @"affiliationName";
	objectAttributes[@"affiliationType"] = @"affiliationType";
	objectAttributes[@"discountCodeID"] = @"discountCodeID";
	objectAttributes[@"discountDescription"] = @"primitiveDiscountDescription";
	objectAttributes[@"discountDescriptionLanguages"] = @"discountDescriptionLanguages";
	objectAttributes[@"discountType"] = @"discountType";
	objectAttributes[@"discountValueType"] = @"discountValueType";
	objectAttributes[@"isDiscountValueRequired"] = @"isDiscountValueRequired";
	objectAttributes[@"isReasonRequired"] = @"isReasonRequired";
	objectAttributes[@"numberOfDecimalPlaces"] = @"numberOfDecimalPlaces";
	return objectAttributes;
}

-(NSString*)discountDescription
{
	return [self localizedStringFromLanguages:_discountDescriptionLanguages value:_discountDescription];
}

-(NSString*)primitiveDiscountDescription
{
	return _discountDescription;
}
@end

@implementation DiscountReason

-(NSString*)xmlTag
{
	return @"reason";
}

+(NSString*)contextName
{
	return @"DISCOUNTREASON";
}

-(NSDictionary*)xmlAttributes
{
	NSMutableDictionary *xmlAttributes = [NSMutableDictionary dictionaryWithDictionary:[super xmlAttributes]];
	xmlAttributes[@"reasonId"] = @"NSString";
	xmlAttributes[@"text"] = @"NSString";
	xmlAttributes[@"textLanguages"] = @"NSArray";
	return xmlAttributes;
}

-(NSDictionary*)objectAttributes
{
	NSMutableDictionary *objectAttributes = [NSMutableDictionary dictionaryWithDictionary:[super objectAttributes]];
	objectAttributes[@"reasonId"] = @"reasonId";
	objectAttributes[@"text"] = @"primitiveText";
	objectAttributes[@"textLanguages"] = @"textLanguages";
	return objectAttributes;
}

-(NSString*)text
{
	return [self localizedStringFromLanguages:_textLanguages value:_text];
}

-(NSString*)primitiveText
{
	return _text;
}
@end

@implementation DiscountReasonGroup

-(NSString*)xmlTag
{
	return @"reasonGroup";
}

+(NSString*)contextName
{
	return @"DISCOUNTREASONGROUP";
}

-(NSDictionary*)xmlAttributes
{
	NSMutableDictionary *xmlAttributes = [NSMutableDictionary dictionaryWithDictionary:[super xmlAttributes]];
	xmlAttributes[@"name"] = @"NSString";
	xmlAttributes[@"reasons"] = @"NSArray";
	return xmlAttributes;
}

-(NSDictionary*)objectAttributes
{
	NSMutableDictionary *objectAttributes = [NSMutableDictionary dictionaryWithDictionary:[super objectAttributes]];
	objectAttributes[@"name"] = @"name";
	objectAttributes[@"reasons"] = @"reasons";
	return objectAttributes;
}
@end

@implementation DumpCode

-(NSString*)xmlTag
{
	return @"dumpCode";
}

+(NSString*)contextName
{
	return @"DUMPCODE";
}

-(NSDictionary*)xmlAttributes
{
	NSMutableDictionary *xmlAttributes = [NSMutableDictionary dictionaryWithDictionary:[super xmlAttributes]];
	xmlAttributes[@"desc"] = @"NSString";
	xmlAttributes[@"descLanguages"] = @"NSArray";
	xmlAttributes[@"dumpCodeId"] = @"NSString";
	xmlAttributes[@"itemSellingCode"] = @"NSString";
	return xmlAttributes;
}

-(NSDictionary*)objectAttributes
{
	NSMutableDictionary *objectAttributes = [NSMutableDictionary dictionaryWithDictionary:[super objectAttributes]];
	objectAttributes[@"desc"] = @"primitiveDesc";
	objectAttributes[@"descLanguages"] = @"descLanguages";
	objectAttributes[@"dumpCodeId"] = @"dumpCodeId";
	objectAttributes[@"itemSellingCode"] = @"itemSellingCode";
	return objectAttributes;
}

-(NSString*)desc
{
	return [self localizedStringFromLanguages:_descLanguages value:_desc];
}

-(NSString*)primitiveDesc
{
	return _desc;
}
@end

@implementation FormConfigurationDTO

-(NSString*)xmlTag
{
	return @"formContext";
}

+(NSString*)contextName
{
	return @"FORMCONFIGURATION";
}

-(NSDictionary*)xmlAttributes
{
	NSMutableDictionary *xmlAttributes = [NSMutableDictionary dictionaryWithDictionary:[super xmlAttributes]];
	xmlAttributes[@"columns"] = @"NSNumber";
	xmlAttributes[@"contextType"] = @"NSString";
	xmlAttributes[@"onCloseFunction"] = @"SoftKey";
	xmlAttributes[@"onSubmitFunction"] = @"SoftKey";
	xmlAttributes[@"presentation"] = @"NSString";
	xmlAttributes[@"rootMenu"] = @"NSString";
	xmlAttributes[@"softKeys"] = @"NSArray";
	xmlAttributes[@"title"] = @"NSString";
	xmlAttributes[@"titleLanguages"] = @"NSArray";
	return xmlAttributes;
}

-(NSDictionary*)objectAttributes
{
	NSMutableDictionary *objectAttributes = [NSMutableDictionary dictionaryWithDictionary:[super objectAttributes]];
	objectAttributes[@"columns"] = @"columns";
	objectAttributes[@"contextType"] = @"contextType";
	objectAttributes[@"onCloseFunction"] = @"onCloseFunction";
	objectAttributes[@"onSubmitFunction"] = @"onSubmitFunction";
	objectAttributes[@"presentation"] = @"presentation";
	objectAttributes[@"rootMenu"] = @"rootMenu";
	objectAttributes[@"softKeys"] = @"softKeys";
	objectAttributes[@"title"] = @"primitiveTitle";
	objectAttributes[@"titleLanguages"] = @"titleLanguages";
	return objectAttributes;
}

-(NSString*)title
{
	return [self localizedStringFromLanguages:_titleLanguages value:_title];
}

-(NSString*)primitiveTitle
{
	return _title;
}
@end

@implementation SoftKeyContext

-(NSString*)xmlTag
{
	return @"softKeyContext";
}

+(NSString*)contextName
{
	return @"SOFTKEYCONTEXT";
}

-(NSDictionary*)xmlAttributes
{
	NSMutableDictionary *xmlAttributes = [NSMutableDictionary dictionaryWithDictionary:[super xmlAttributes]];
	return xmlAttributes;
}

-(NSDictionary*)objectAttributes
{
	NSMutableDictionary *objectAttributes = [NSMutableDictionary dictionaryWithDictionary:[super objectAttributes]];
	return objectAttributes;
}
@end

@implementation Item

-(BOOL)needsAvailabilityValue {return [self.needsAvailability boolValue];}
-(void)setNeedsAvailabilityValue:(BOOL)needsAvailabilityValue {self.needsAvailability = @(needsAvailabilityValue);}
-(NSString*)xmlTag
{
	return @"item";
}

+(NSString*)contextName
{
	return @"ITEM";
}

-(NSDictionary*)xmlAttributes
{
	NSMutableDictionary *xmlAttributes = [NSMutableDictionary dictionaryWithDictionary:[super xmlAttributes]];
	xmlAttributes[@"additionalImages"] = @"NSArray";
	xmlAttributes[@"availability"] = @"NSArray";
	xmlAttributes[@"availabilityError"] = @"ServerError";
	xmlAttributes[@"displayPrice"] = @"NSString";
	xmlAttributes[@"image"] = @"NSString";
	xmlAttributes[@"images"] = @"NSArray";
	xmlAttributes[@"itemRefId"] = @"NSString";
	xmlAttributes[@"longDesc"] = @"NSString";
	xmlAttributes[@"needsAvailability"] = @"BOOL";
	xmlAttributes[@"price"] = @"NSString";
	xmlAttributes[@"productSearch"] = @"ProductSearch";
	xmlAttributes[@"properties"] = @"NSArray";
	xmlAttributes[@"quantity"] = @"NSString";
	xmlAttributes[@"shortDesc"] = @"NSString";
	return xmlAttributes;
}

-(NSDictionary*)objectAttributes
{
	NSMutableDictionary *objectAttributes = [NSMutableDictionary dictionaryWithDictionary:[super objectAttributes]];
	objectAttributes[@"additionalImages"] = @"additionalImages";
	objectAttributes[@"availability"] = @"availability";
	objectAttributes[@"availabilityError"] = @"availabilityError";
	objectAttributes[@"displayPrice"] = @"displayPrice";
	objectAttributes[@"image"] = @"image";
	objectAttributes[@"images"] = @"images";
	objectAttributes[@"itemRefId"] = @"itemRefId";
	objectAttributes[@"longDesc"] = @"longDesc";
	objectAttributes[@"needsAvailability"] = @"needsAvailability";
	objectAttributes[@"price"] = @"price";
	objectAttributes[@"productSearch"] = @"productSearch";
	objectAttributes[@"properties"] = @"properties";
	objectAttributes[@"quantity"] = @"quantity";
	objectAttributes[@"shortDesc"] = @"shortDesc";
	return objectAttributes;
}
@end

@implementation ItemAvailability

-(NSString*)xmlTag
{
	return @"itemAvailability";
}

+(NSString*)contextName
{
	return @"ITEMAVAILABILITY";
}

-(NSDictionary*)xmlAttributes
{
	NSMutableDictionary *xmlAttributes = [NSMutableDictionary dictionaryWithDictionary:[super xmlAttributes]];
	xmlAttributes[@"distance"] = @"NSString";
	xmlAttributes[@"item"] = @"Item";
	xmlAttributes[@"quantity"] = @"NSString";
	xmlAttributes[@"storeId"] = @"NSString";
	xmlAttributes[@"storeName"] = @"NSString";
	return xmlAttributes;
}

-(NSDictionary*)objectAttributes
{
	NSMutableDictionary *objectAttributes = [NSMutableDictionary dictionaryWithDictionary:[super objectAttributes]];
	objectAttributes[@"distance"] = @"distance";
	objectAttributes[@"item"] = @"item";
	objectAttributes[@"quantity"] = @"quantity";
	objectAttributes[@"storeId"] = @"storeId";
	objectAttributes[@"storeName"] = @"storeName";
	return objectAttributes;
}
@end

@implementation ItemAvailabilityRecord

-(NSString*)xmlTag
{
	return @"itemAvailabilityRecord";
}

+(NSString*)contextName
{
	return @"ITEMAVAILABILITYRECORD";
}

-(NSDictionary*)xmlAttributes
{
	NSMutableDictionary *xmlAttributes = [NSMutableDictionary dictionaryWithDictionary:[super xmlAttributes]];
	xmlAttributes[@"distance"] = @"NSString";
	xmlAttributes[@"quantity"] = @"NSString";
	xmlAttributes[@"storeId"] = @"NSString";
	xmlAttributes[@"storeName"] = @"NSString";
	return xmlAttributes;
}

-(NSDictionary*)objectAttributes
{
	NSMutableDictionary *objectAttributes = [NSMutableDictionary dictionaryWithDictionary:[super objectAttributes]];
	objectAttributes[@"distance"] = @"distance";
	objectAttributes[@"quantity"] = @"quantity";
	objectAttributes[@"storeId"] = @"storeId";
	objectAttributes[@"storeName"] = @"storeName";
	return objectAttributes;
}
@end

@implementation ItemImages

-(NSString*)xmlTag
{
	return @"itemImages";
}

+(NSString*)contextName
{
	return @"ITEMIMAGES";
}

-(NSDictionary*)xmlAttributes
{
	NSMutableDictionary *xmlAttributes = [NSMutableDictionary dictionaryWithDictionary:[super xmlAttributes]];
	xmlAttributes[@"image"] = @"NSString";
	xmlAttributes[@"item"] = @"Item";
	return xmlAttributes;
}

-(NSDictionary*)objectAttributes
{
	NSMutableDictionary *objectAttributes = [NSMutableDictionary dictionaryWithDictionary:[super objectAttributes]];
	objectAttributes[@"image"] = @"image";
	objectAttributes[@"item"] = @"item";
	return objectAttributes;
}
@end

@implementation ItemModifier

-(NSString*)xmlTag
{
	return @"itemModifier";
}

+(NSString*)contextName
{
	return @"ITEMMODIFIER";
}

-(NSDictionary*)xmlAttributes
{
	NSMutableDictionary *xmlAttributes = [NSMutableDictionary dictionaryWithDictionary:[super xmlAttributes]];
	xmlAttributes[@"itemModifierGroupId"] = @"NSString";
	xmlAttributes[@"itemModifierGroupName"] = @"NSString";
	xmlAttributes[@"itemModifierId"] = @"NSString";
	xmlAttributes[@"name"] = @"NSString";
	xmlAttributes[@"retailPrice"] = @"BasketPricingDTO";
	xmlAttributes[@"sku"] = @"NSString";
	xmlAttributes[@"type"] = @"NSString";
	return xmlAttributes;
}

-(NSDictionary*)objectAttributes
{
	NSMutableDictionary *objectAttributes = [NSMutableDictionary dictionaryWithDictionary:[super objectAttributes]];
	objectAttributes[@"itemModifierGroupId"] = @"itemModifierGroupId";
	objectAttributes[@"itemModifierGroupName"] = @"itemModifierGroupName";
	objectAttributes[@"itemModifierId"] = @"itemModifierId";
	objectAttributes[@"name"] = @"name";
	objectAttributes[@"retailPrice"] = @"retailPrice";
	objectAttributes[@"sku"] = @"sku";
	objectAttributes[@"type"] = @"type";
	return objectAttributes;
}
@end

@implementation ItemModifierGroup

-(NSString*)xmlTag
{
	return @"itemModifierGroup";
}

+(NSString*)contextName
{
	return @"ITEMMODIFIERGROUP";
}

-(NSDictionary*)xmlAttributes
{
	NSMutableDictionary *xmlAttributes = [NSMutableDictionary dictionaryWithDictionary:[super xmlAttributes]];
	xmlAttributes[@"itemModifierGroupId"] = @"NSString";
	xmlAttributes[@"itemModifiers"] = @"NSArray";
	xmlAttributes[@"mandatoryBasketItem"] = @"BasketItem";
	xmlAttributes[@"name"] = @"NSString";
	xmlAttributes[@"optionalBasketItem"] = @"BasketItem";
	xmlAttributes[@"type"] = @"NSString";
	return xmlAttributes;
}

-(NSDictionary*)objectAttributes
{
	NSMutableDictionary *objectAttributes = [NSMutableDictionary dictionaryWithDictionary:[super objectAttributes]];
	objectAttributes[@"itemModifierGroupId"] = @"itemModifierGroupId";
	objectAttributes[@"itemModifiers"] = @"itemModifiers";
	objectAttributes[@"mandatoryBasketItem"] = @"mandatoryBasketItem";
	objectAttributes[@"name"] = @"name";
	objectAttributes[@"optionalBasketItem"] = @"optionalBasketItem";
	objectAttributes[@"type"] = @"type";
	return objectAttributes;
}
@end

@implementation MandatoryModifierGroup

-(NSString*)xmlTag
{
	return @"mandatoryModifierGroup";
}

+(NSString*)contextName
{
	return @"MANDATORYMODIFIERGROUP";
}

-(NSDictionary*)xmlAttributes
{
	NSMutableDictionary *xmlAttributes = [NSMutableDictionary dictionaryWithDictionary:[super xmlAttributes]];
	return xmlAttributes;
}

-(NSDictionary*)objectAttributes
{
	NSMutableDictionary *objectAttributes = [NSMutableDictionary dictionaryWithDictionary:[super objectAttributes]];
	return objectAttributes;
}
@end

@implementation OptionalModifierGroup

-(NSString*)xmlTag
{
	return @"optionalModifierGroup";
}

+(NSString*)contextName
{
	return @"OPTIONALMODIFIERGROUP";
}

-(NSDictionary*)xmlAttributes
{
	NSMutableDictionary *xmlAttributes = [NSMutableDictionary dictionaryWithDictionary:[super xmlAttributes]];
	return xmlAttributes;
}

-(NSDictionary*)objectAttributes
{
	NSMutableDictionary *objectAttributes = [NSMutableDictionary dictionaryWithDictionary:[super objectAttributes]];
	return objectAttributes;
}
@end

@implementation ItemProperties

-(NSString*)xmlTag
{
	return @"itemProperties";
}

+(NSString*)contextName
{
	return @"ITEMPROPERTIES";
}

-(NSDictionary*)xmlAttributes
{
	NSMutableDictionary *xmlAttributes = [NSMutableDictionary dictionaryWithDictionary:[super xmlAttributes]];
	xmlAttributes[@"item"] = @"Item";
	xmlAttributes[@"key"] = @"NSString";
	xmlAttributes[@"value"] = @"NSString";
	return xmlAttributes;
}

-(NSDictionary*)objectAttributes
{
	NSMutableDictionary *objectAttributes = [NSMutableDictionary dictionaryWithDictionary:[super objectAttributes]];
	objectAttributes[@"item"] = @"item";
	objectAttributes[@"key"] = @"key";
	objectAttributes[@"value"] = @"value";
	return objectAttributes;
}
@end

@implementation KeyValueDTO

-(NSString*)xmlTag
{
	return @"keyValue";
}

+(NSString*)contextName
{
	return @"KEYVALUE";
}

-(NSDictionary*)xmlAttributes
{
	NSMutableDictionary *xmlAttributes = [NSMutableDictionary dictionaryWithDictionary:[super xmlAttributes]];
	xmlAttributes[@"key"] = @"NSString";
	xmlAttributes[@"value"] = @"NSString";
	return xmlAttributes;
}

-(NSDictionary*)objectAttributes
{
	NSMutableDictionary *objectAttributes = [NSMutableDictionary dictionaryWithDictionary:[super objectAttributes]];
	objectAttributes[@"key"] = @"key";
	objectAttributes[@"value"] = @"value";
	return objectAttributes;
}
@end

@implementation KeyValueGroup

-(NSString*)xmlTag
{
	return @"keyValueGroup";
}

+(NSString*)contextName
{
	return @"KEYVALUEGROUP";
}

-(NSDictionary*)xmlAttributes
{
	NSMutableDictionary *xmlAttributes = [NSMutableDictionary dictionaryWithDictionary:[super xmlAttributes]];
	xmlAttributes[@"groupName"] = @"NSString";
	xmlAttributes[@"keyValues"] = @"KeyValues";
	return xmlAttributes;
}

-(NSDictionary*)objectAttributes
{
	NSMutableDictionary *objectAttributes = [NSMutableDictionary dictionaryWithDictionary:[super objectAttributes]];
	objectAttributes[@"groupName"] = @"groupName";
	objectAttributes[@"keyValues"] = @"keyValues";
	return objectAttributes;
}
@end

@implementation KeyValues

-(NSString*)xmlTag
{
	return @"array";
}

+(NSString*)contextName
{
	return @"KEYVALUES";
}

-(NSString*)xmlAlias
{
	return @"NSArray";
}

+(instancetype)keyValuesWithArray:(NSArray*)array
{
	KeyValues* object = [[KeyValues alloc] init];
	object.array = array;
	return object;
}

-(NSDictionary*)xmlAttributes
{
	NSMutableDictionary *xmlAttributes = [NSMutableDictionary dictionaryWithDictionary:[super xmlAttributes]];
	return xmlAttributes;
}

-(NSDictionary*)objectAttributes
{
	NSMutableDictionary *objectAttributes = [NSMutableDictionary dictionaryWithDictionary:[super objectAttributes]];
	objectAttributes[@"array"] = @"array";
	return objectAttributes;
}
@end

@implementation Language

-(NSString*)xmlTag
{
	return @"language";
}

+(NSString*)contextName
{
	return @"LANGUAGE";
}

-(NSDictionary*)xmlAttributes
{
	NSMutableDictionary *xmlAttributes = [NSMutableDictionary dictionaryWithDictionary:[super xmlAttributes]];
	xmlAttributes[@"code"] = @"NSString";
	xmlAttributes[@"value"] = @"NSString";
	return xmlAttributes;
}

-(NSDictionary*)objectAttributes
{
	NSMutableDictionary *objectAttributes = [NSMutableDictionary dictionaryWithDictionary:[super objectAttributes]];
	objectAttributes[@"code"] = @"code";
	objectAttributes[@"value"] = @"value";
	return objectAttributes;
}
@end

@implementation Localizable

-(NSString*)xmlTag
{
	return @"localizable";
}

+(NSString*)contextName
{
	return @"LOCALIZABLE";
}

-(NSDictionary*)xmlAttributes
{
	NSMutableDictionary *xmlAttributes = [NSMutableDictionary dictionaryWithDictionary:[super xmlAttributes]];
	xmlAttributes[@"comment"] = @"NSString";
	xmlAttributes[@"identifier"] = @"NSString";
	xmlAttributes[@"key"] = @"NSString";
	xmlAttributes[@"value"] = @"NSString";
	xmlAttributes[@"valueLanguages"] = @"NSArray";
	return xmlAttributes;
}

-(NSDictionary*)objectAttributes
{
	NSMutableDictionary *objectAttributes = [NSMutableDictionary dictionaryWithDictionary:[super objectAttributes]];
	objectAttributes[@"comment"] = @"comment";
	objectAttributes[@"identifier"] = @"identifier";
	objectAttributes[@"key"] = @"key";
	objectAttributes[@"value"] = @"primitiveValue";
	objectAttributes[@"valueLanguages"] = @"valueLanguages";
	return objectAttributes;
}

-(NSString*)value
{
	return [self localizedStringFromLanguages:_valueLanguages value:_value];
}

-(NSString*)primitiveValue
{
	return _value;
}
@end

@implementation LocalizableTable

-(NSString*)xmlTag
{
	return @"localizableTable";
}

+(NSString*)contextName
{
	return @"LOCALIZABLETABLE";
}

-(NSDictionary*)xmlAttributes
{
	NSMutableDictionary *xmlAttributes = [NSMutableDictionary dictionaryWithDictionary:[super xmlAttributes]];
	xmlAttributes[@"localizables"] = @"NSArray";
	xmlAttributes[@"table"] = @"NSString";
	return xmlAttributes;
}

-(NSDictionary*)objectAttributes
{
	NSMutableDictionary *objectAttributes = [NSMutableDictionary dictionaryWithDictionary:[super objectAttributes]];
	objectAttributes[@"localizables"] = @"localizables";
	objectAttributes[@"table"] = @"table";
	return objectAttributes;
}
@end

@implementation POSError

-(NSString*)xmlTag
{
	return @"posError";
}

+(NSString*)contextName
{
	return @"POSERROR";
}

-(NSDictionary*)xmlAttributes
{
	NSMutableDictionary *xmlAttributes = [NSMutableDictionary dictionaryWithDictionary:[super xmlAttributes]];
	xmlAttributes[@"errorLevel"] = @"NSString";
	xmlAttributes[@"posErrorNumber"] = @"NSString";
	return xmlAttributes;
}

-(NSDictionary*)objectAttributes
{
	NSMutableDictionary *objectAttributes = [NSMutableDictionary dictionaryWithDictionary:[super objectAttributes]];
	objectAttributes[@"errorLevel"] = @"errorLevel";
	objectAttributes[@"posErrorNumber"] = @"posErrorNumber";
	return objectAttributes;
}
@end

@implementation PasswordRule

-(NSString*)xmlTag
{
	return @"passwordRule";
}

+(NSString*)contextName
{
	return @"PASSWORDRULE";
}

-(NSDictionary*)xmlAttributes
{
	NSMutableDictionary *xmlAttributes = [NSMutableDictionary dictionaryWithDictionary:[super xmlAttributes]];
	xmlAttributes[@"ruleDescription"] = @"NSString";
	xmlAttributes[@"ruleDescriptionLanguages"] = @"NSArray";
	xmlAttributes[@"ruleExpression"] = @"NSString";
	xmlAttributes[@"ruleId"] = @"NSString";
	return xmlAttributes;
}

-(NSDictionary*)objectAttributes
{
	NSMutableDictionary *objectAttributes = [NSMutableDictionary dictionaryWithDictionary:[super objectAttributes]];
	objectAttributes[@"ruleDescription"] = @"primitiveRuleDescription";
	objectAttributes[@"ruleDescriptionLanguages"] = @"ruleDescriptionLanguages";
	objectAttributes[@"ruleExpression"] = @"ruleExpression";
	objectAttributes[@"ruleId"] = @"ruleId";
	return objectAttributes;
}

-(NSString*)ruleDescription
{
	return [self localizedStringFromLanguages:_ruleDescriptionLanguages value:_ruleDescription];
}

-(NSString*)primitiveRuleDescription
{
	return _ruleDescription;
}
@end

@implementation Payment

-(double)amountValue {return [self.amount doubleValue];}
-(void)setAmountValue:(double)amountValue {self.amount = @(amountValue);}
-(NSString*)xmlTag
{
	return @"payment";
}

+(NSString*)contextName
{
	return @"PAYMENT";
}

-(NSDictionary*)xmlAttributes
{
	NSMutableDictionary *xmlAttributes = [NSMutableDictionary dictionaryWithDictionary:[super xmlAttributes]];
	xmlAttributes[@"amount"] = @"double";
	xmlAttributes[@"authCode"] = @"NSString";
	xmlAttributes[@"basket"] = @"BasketAssociateDTO";
	xmlAttributes[@"cardNumber"] = @"NSString";
	xmlAttributes[@"reference"] = @"NSString";
	xmlAttributes[@"tender"] = @"Tender";
	return xmlAttributes;
}

-(NSDictionary*)objectAttributes
{
	NSMutableDictionary *objectAttributes = [NSMutableDictionary dictionaryWithDictionary:[super objectAttributes]];
	objectAttributes[@"amount"] = @"amount";
	objectAttributes[@"authCode"] = @"authCode";
	objectAttributes[@"basket"] = @"basket";
	objectAttributes[@"cardNumber"] = @"cardNumber";
	objectAttributes[@"reference"] = @"reference";
	objectAttributes[@"tender"] = @"tender";
	return objectAttributes;
}
@end

@implementation Permission

-(NSString*)xmlTag
{
	return @"permission";
}

+(NSString*)contextName
{
	return @"PERMISSION";
}

-(NSString*)xmlAlias
{
	return @"NSString";
}

+(instancetype)permissionWithString:(NSString*)string
{
	Permission* object = [[Permission alloc] init];
	object.permission = string;
	return object;
}

-(NSDictionary*)xmlAttributes
{
	NSMutableDictionary *xmlAttributes = [NSMutableDictionary dictionaryWithDictionary:[super xmlAttributes]];
	return xmlAttributes;
}

-(NSDictionary*)objectAttributes
{
	NSMutableDictionary *objectAttributes = [NSMutableDictionary dictionaryWithDictionary:[super objectAttributes]];
	objectAttributes[@"permission"] = @"permission";
	return objectAttributes;
}
@end

@implementation PermissionGroup

-(NSString*)xmlTag
{
	return @"permissionGroup";
}

+(NSString*)contextName
{
	return @"PERMISSIONGROUP";
}

-(NSDictionary*)xmlAttributes
{
	NSMutableDictionary *xmlAttributes = [NSMutableDictionary dictionaryWithDictionary:[super xmlAttributes]];
	xmlAttributes[@"groupName"] = @"NSString";
	xmlAttributes[@"permissions"] = @"NSArray";
	return xmlAttributes;
}

-(NSDictionary*)objectAttributes
{
	NSMutableDictionary *objectAttributes = [NSMutableDictionary dictionaryWithDictionary:[super objectAttributes]];
	objectAttributes[@"groupName"] = @"groupName";
	objectAttributes[@"permissions"] = @"permissions";
	return objectAttributes;
}
@end

@implementation PrintElement

-(NSString*)xmlTag
{
	return @"element";
}

+(NSString*)contextName
{
	return @"PRINTELEMENT";
}

-(NSDictionary*)xmlAttributes
{
	NSMutableDictionary *xmlAttributes = [NSMutableDictionary dictionaryWithDictionary:[super xmlAttributes]];
	xmlAttributes[@"align"] = @"NSString";
	xmlAttributes[@"barcodetype"] = @"NSString";
	xmlAttributes[@"elementtype"] = @"NSString";
	xmlAttributes[@"printImageURL"] = @"NSString";
	xmlAttributes[@"size"] = @"NSString";
	xmlAttributes[@"style"] = @"NSString";
	xmlAttributes[@"value"] = @"NSString";
	return xmlAttributes;
}

-(NSDictionary*)objectAttributes
{
	NSMutableDictionary *objectAttributes = [NSMutableDictionary dictionaryWithDictionary:[super objectAttributes]];
	objectAttributes[@"align"] = @"align";
	objectAttributes[@"barcodetype"] = @"barcodetype";
	objectAttributes[@"elementtype"] = @"elementtype";
	objectAttributes[@"printImageURL"] = @"printImageURL";
	objectAttributes[@"size"] = @"size";
	objectAttributes[@"style"] = @"style";
	objectAttributes[@"value"] = @"value";
	return objectAttributes;
}
@end

@implementation Property

-(NSString*)xmlTag
{
	return @"property";
}

+(NSString*)contextName
{
	return @"PROPERTY";
}

-(NSDictionary*)xmlAttributes
{
	NSMutableDictionary *xmlAttributes = [NSMutableDictionary dictionaryWithDictionary:[super xmlAttributes]];
	xmlAttributes[@"name"] = @"NSString";
	xmlAttributes[@"value"] = @"NSString";
	return xmlAttributes;
}

-(NSDictionary*)objectAttributes
{
	NSMutableDictionary *objectAttributes = [NSMutableDictionary dictionaryWithDictionary:[super objectAttributes]];
	objectAttributes[@"name"] = @"name";
	objectAttributes[@"value"] = @"value";
	return objectAttributes;
}
@end

@implementation ReturnInstance

-(NSString*)xmlTag
{
	return @"returnInstance";
}

+(NSString*)contextName
{
	return @"RETURNINSTANCE";
}

-(NSDictionary*)xmlAttributes
{
	NSMutableDictionary *xmlAttributes = [NSMutableDictionary dictionaryWithDictionary:[super xmlAttributes]];
	xmlAttributes[@"date"] = @"NSDate";
	xmlAttributes[@"quantity"] = @"NSInteger";
	xmlAttributes[@"storeID"] = @"NSString";
	return xmlAttributes;
}

-(NSDictionary*)objectAttributes
{
	NSMutableDictionary *objectAttributes = [NSMutableDictionary dictionaryWithDictionary:[super objectAttributes]];
	objectAttributes[@"date"] = @"date";
	objectAttributes[@"quantity"] = @"quantity";
	objectAttributes[@"storeID"] = @"storeID";
	return objectAttributes;
}
@end

@implementation SalesPerson

-(NSString*)xmlTag
{
	return @"salesPerson";
}

+(NSString*)contextName
{
	return @"SALESPERSON";
}

-(NSDictionary*)xmlAttributes
{
	NSMutableDictionary *xmlAttributes = [NSMutableDictionary dictionaryWithDictionary:[super xmlAttributes]];
	xmlAttributes[@"basketItem"] = @"BasketItem";
	xmlAttributes[@"displayName"] = @"NSString";
	xmlAttributes[@"operatorId"] = @"NSString";
	return xmlAttributes;
}

-(NSDictionary*)objectAttributes
{
	NSMutableDictionary *objectAttributes = [NSMutableDictionary dictionaryWithDictionary:[super objectAttributes]];
	objectAttributes[@"basketItem"] = @"basketItem";
	objectAttributes[@"displayName"] = @"displayName";
	objectAttributes[@"operatorId"] = @"operatorId";
	return objectAttributes;
}
@end

@implementation SelectedModifier

-(NSString*)xmlTag
{
	return @"selectedModifier";
}

+(NSString*)contextName
{
	return @"SELECTEDMODIFIER";
}

-(NSDictionary*)xmlAttributes
{
	NSMutableDictionary *xmlAttributes = [NSMutableDictionary dictionaryWithDictionary:[super xmlAttributes]];
	xmlAttributes[@"basketItem"] = @"BasketItem";
	xmlAttributes[@"itemModifierId"] = @"NSString";
	xmlAttributes[@"name"] = @"NSString";
	xmlAttributes[@"retailPrice"] = @"BasketPricingDTO";
	xmlAttributes[@"sku"] = @"NSString";
	return xmlAttributes;
}

-(NSDictionary*)objectAttributes
{
	NSMutableDictionary *objectAttributes = [NSMutableDictionary dictionaryWithDictionary:[super objectAttributes]];
	objectAttributes[@"basketItem"] = @"basketItem";
	objectAttributes[@"itemModifierId"] = @"itemModifierId";
	objectAttributes[@"name"] = @"name";
	objectAttributes[@"retailPrice"] = @"retailPrice";
	objectAttributes[@"sku"] = @"sku";
	return objectAttributes;
}
@end

@implementation ServerError

-(NSString*)xmlTag
{
	return @"error";
}

+(NSString*)contextName
{
	return @"SERVERERROR";
}

-(NSDictionary*)xmlAttributes
{
	NSMutableDictionary *xmlAttributes = [NSMutableDictionary dictionaryWithDictionary:[super xmlAttributes]];
	xmlAttributes[@"changedBasketItem"] = @"BasketItem";
	xmlAttributes[@"context"] = @"NSString";
	xmlAttributes[@"createdBasketItem"] = @"BasketItem";
	xmlAttributes[@"messages"] = @"NSArray";
	return xmlAttributes;
}

-(NSDictionary*)objectAttributes
{
	NSMutableDictionary *objectAttributes = [NSMutableDictionary dictionaryWithDictionary:[super objectAttributes]];
	objectAttributes[@"changedBasketItem"] = @"changedBasketItem";
	objectAttributes[@"context"] = @"context";
	objectAttributes[@"createdBasketItem"] = @"createdBasketItem";
	objectAttributes[@"messages"] = @"messages";
	return objectAttributes;
}
@end

@implementation ServerErrorMessage

-(NSInteger)codeValue {return [self.code integerValue];}
-(void)setCodeValue:(NSInteger)codeValue {self.code = @(codeValue);}
-(NSInteger)orderValue {return [self.order integerValue];}
-(void)setOrderValue:(NSInteger)orderValue {self.order = @(orderValue);}
-(NSString*)xmlTag
{
	return @"message";
}

+(NSString*)contextName
{
	return @"SERVERERRORMESSAGE";
}

-(NSDictionary*)xmlAttributes
{
	NSMutableDictionary *xmlAttributes = [NSMutableDictionary dictionaryWithDictionary:[super xmlAttributes]];
	xmlAttributes[@"basketItemId"] = @"NSString";
	xmlAttributes[@"code"] = @"NSInteger";
	xmlAttributes[@"name"] = @"NSString";
	xmlAttributes[@"order"] = @"NSInteger";
	xmlAttributes[@"value"] = @"NSString";
	return xmlAttributes;
}

-(NSDictionary*)objectAttributes
{
	NSMutableDictionary *objectAttributes = [NSMutableDictionary dictionaryWithDictionary:[super objectAttributes]];
	objectAttributes[@"basketItemId"] = @"basketItemId";
	objectAttributes[@"code"] = @"code";
	objectAttributes[@"name"] = @"name";
	objectAttributes[@"order"] = @"order";
	objectAttributes[@"value"] = @"value";
	return objectAttributes;
}
@end

@implementation OverrideErrorMessage

-(NSString*)xmlTag
{
	return @"overrideErrorMessage";
}

+(NSString*)contextName
{
	return @"OVERRIDEERRORMESSAGE";
}

-(NSDictionary*)xmlAttributes
{
	NSMutableDictionary *xmlAttributes = [NSMutableDictionary dictionaryWithDictionary:[super xmlAttributes]];
	xmlAttributes[@"errorCode"] = @"NSString";
	xmlAttributes[@"errorLevel"] = @"NSInteger";
	xmlAttributes[@"errorMessage"] = @"NSString";
	return xmlAttributes;
}

-(NSDictionary*)objectAttributes
{
	NSMutableDictionary *objectAttributes = [NSMutableDictionary dictionaryWithDictionary:[super objectAttributes]];
	objectAttributes[@"errorCode"] = @"errorCode";
	objectAttributes[@"errorLevel"] = @"errorLevel";
	objectAttributes[@"errorMessage"] = @"errorMessage";
	return objectAttributes;
}
@end

@implementation POSErrorMessage

-(NSString*)xmlTag
{
	return @"posErrorMessage";
}

+(NSString*)contextName
{
	return @"POSERRORMESSAGE";
}

-(NSDictionary*)xmlAttributes
{
	NSMutableDictionary *xmlAttributes = [NSMutableDictionary dictionaryWithDictionary:[super xmlAttributes]];
	xmlAttributes[@"invalidatesBasket"] = @"BOOL";
	return xmlAttributes;
}

-(NSDictionary*)objectAttributes
{
	NSMutableDictionary *objectAttributes = [NSMutableDictionary dictionaryWithDictionary:[super objectAttributes]];
	objectAttributes[@"invalidatesBasket"] = @"invalidatesBasket";
	return objectAttributes;
}
@end

@implementation ServerErrorAffiliationRequiredMessage

-(NSString*)xmlTag
{
	return @"affiliationRequiredMessage";
}

+(NSString*)contextName
{
	return @"SERVERERRORAFFILIATIONREQUIREDMESSAGE";
}

-(NSDictionary*)xmlAttributes
{
	NSMutableDictionary *xmlAttributes = [NSMutableDictionary dictionaryWithDictionary:[super xmlAttributes]];
	xmlAttributes[@"affiliationName"] = @"NSString";
	xmlAttributes[@"affiliationType"] = @"NSString";
	return xmlAttributes;
}

-(NSDictionary*)objectAttributes
{
	NSMutableDictionary *objectAttributes = [NSMutableDictionary dictionaryWithDictionary:[super objectAttributes]];
	objectAttributes[@"affiliationName"] = @"affiliationName";
	objectAttributes[@"affiliationType"] = @"affiliationType";
	return objectAttributes;
}
@end

@implementation ServerErrorAgeRestrictionMessage

-(NSString*)xmlTag
{
	return @"ageRestrictionMessage";
}

+(NSString*)contextName
{
	return @"SERVERERRORAGERESTRICTIONMESSAGE";
}

-(NSDictionary*)xmlAttributes
{
	NSMutableDictionary *xmlAttributes = [NSMutableDictionary dictionaryWithDictionary:[super xmlAttributes]];
	xmlAttributes[@"minimumAge"] = @"NSString";
	return xmlAttributes;
}

-(NSDictionary*)objectAttributes
{
	NSMutableDictionary *objectAttributes = [NSMutableDictionary dictionaryWithDictionary:[super objectAttributes]];
	objectAttributes[@"minimumAge"] = @"minimumAge";
	return objectAttributes;
}
@end

@implementation ServerErrorBarcodeItemNotReturnedMessage

-(NSInteger)originalTransactionIdValue {return [self.originalTransactionId integerValue];}
-(void)setOriginalTransactionIdValue:(NSInteger)originalTransactionIdValue {self.originalTransactionId = @(originalTransactionIdValue);}
-(NSInteger)originalTransactionStoreIdValue {return [self.originalTransactionStoreId integerValue];}
-(void)setOriginalTransactionStoreIdValue:(NSInteger)originalTransactionStoreIdValue {self.originalTransactionStoreId = @(originalTransactionStoreIdValue);}
-(NSInteger)originalTransactionTillValue {return [self.originalTransactionTill integerValue];}
-(void)setOriginalTransactionTillValue:(NSInteger)originalTransactionTillValue {self.originalTransactionTill = @(originalTransactionTillValue);}
-(NSString*)xmlTag
{
	return @"barcodeItemNotReturnedMessage";
}

+(NSString*)contextName
{
	return @"SERVERERRORBARCODEITEMNOTRETURNEDMESSAGE";
}

-(NSDictionary*)xmlAttributes
{
	NSMutableDictionary *xmlAttributes = [NSMutableDictionary dictionaryWithDictionary:[super xmlAttributes]];
	xmlAttributes[@"originalItemPrice"] = @"NSString";
	xmlAttributes[@"originalTransactionDate"] = @"NSString";
	xmlAttributes[@"originalTransactionId"] = @"NSInteger";
	xmlAttributes[@"originalTransactionStoreId"] = @"NSInteger";
	xmlAttributes[@"originalTransactionTill"] = @"NSInteger";
	return xmlAttributes;
}

-(NSDictionary*)objectAttributes
{
	NSMutableDictionary *objectAttributes = [NSMutableDictionary dictionaryWithDictionary:[super objectAttributes]];
	objectAttributes[@"originalItemPrice"] = @"originalItemPrice";
	objectAttributes[@"originalTransactionDate"] = @"originalTransactionDate";
	objectAttributes[@"originalTransactionId"] = @"originalTransactionId";
	objectAttributes[@"originalTransactionStoreId"] = @"originalTransactionStoreId";
	objectAttributes[@"originalTransactionTill"] = @"originalTransactionTill";
	return objectAttributes;
}
@end

@implementation ServerErrorDeviceStationClaimedMessage

-(NSString*)xmlTag
{
	return @"deviceStationClaimedMessage";
}

+(NSString*)contextName
{
	return @"SERVERERRORDEVICESTATIONCLAIMEDMESSAGE";
}

-(NSDictionary*)xmlAttributes
{
	NSMutableDictionary *xmlAttributes = [NSMutableDictionary dictionaryWithDictionary:[super xmlAttributes]];
	xmlAttributes[@"terminal"] = @"NSString";
	xmlAttributes[@"user"] = @"NSString";
	return xmlAttributes;
}

-(NSDictionary*)objectAttributes
{
	NSMutableDictionary *objectAttributes = [NSMutableDictionary dictionaryWithDictionary:[super objectAttributes]];
	objectAttributes[@"terminal"] = @"terminal";
	objectAttributes[@"user"] = @"user";
	return objectAttributes;
}
@end

@implementation ServerErrorDeviceStationMessage

-(NSString*)xmlTag
{
	return @"deviceStationDrawerInUseMessage";
}

+(NSString*)contextName
{
	return @"SERVERERRORDEVICESTATIONMESSAGE";
}

-(NSDictionary*)xmlAttributes
{
	NSMutableDictionary *xmlAttributes = [NSMutableDictionary dictionaryWithDictionary:[super xmlAttributes]];
	xmlAttributes[@"deviceStationId"] = @"NSString";
	xmlAttributes[@"deviceStationName"] = @"NSString";
	xmlAttributes[@"peripherals"] = @"NSString";
	xmlAttributes[@"terminal"] = @"NSString";
	xmlAttributes[@"user"] = @"NSString";
	return xmlAttributes;
}

-(NSDictionary*)objectAttributes
{
	NSMutableDictionary *objectAttributes = [NSMutableDictionary dictionaryWithDictionary:[super objectAttributes]];
	objectAttributes[@"deviceStationId"] = @"deviceStationId";
	objectAttributes[@"deviceStationName"] = @"deviceStationName";
	objectAttributes[@"peripherals"] = @"peripherals";
	objectAttributes[@"terminal"] = @"terminal";
	objectAttributes[@"user"] = @"user";
	return objectAttributes;
}
@end

@implementation ServerErrorUnitOfMeasureMessage

-(NSString*)xmlTag
{
	return @"unitOfMeasureMessage";
}

+(NSString*)contextName
{
	return @"SERVERERRORUNITOFMEASUREMESSAGE";
}

-(NSDictionary*)xmlAttributes
{
	NSMutableDictionary *xmlAttributes = [NSMutableDictionary dictionaryWithDictionary:[super xmlAttributes]];
	xmlAttributes[@"displayUnits"] = @"NSString";
	xmlAttributes[@"maxValue"] = @"NSString";
	xmlAttributes[@"minValue"] = @"NSString";
	xmlAttributes[@"scale"] = @"NSString";
	return xmlAttributes;
}

-(NSDictionary*)objectAttributes
{
	NSMutableDictionary *objectAttributes = [NSMutableDictionary dictionaryWithDictionary:[super objectAttributes]];
	objectAttributes[@"displayUnits"] = @"displayUnits";
	objectAttributes[@"maxValue"] = @"maxValue";
	objectAttributes[@"minValue"] = @"minValue";
	objectAttributes[@"scale"] = @"scale";
	return objectAttributes;
}
@end

@implementation ServerRequest

-(BOOL)resultValue {return [self.result boolValue];}
-(void)setResultValue:(BOOL)resultValue {self.result = @(resultValue);}
-(NSString*)xmlTag
{
	return @"serverRequest";
}

+(NSString*)contextName
{
	return @"SERVERREQUEST";
}

-(NSDictionary*)xmlAttributes
{
	NSMutableDictionary *xmlAttributes = [NSMutableDictionary dictionaryWithDictionary:[super xmlAttributes]];
	xmlAttributes[@"error"] = @"ServerError";
	xmlAttributes[@"result"] = @"BOOL";
	return xmlAttributes;
}

-(NSDictionary*)objectAttributes
{
	NSMutableDictionary *objectAttributes = [NSMutableDictionary dictionaryWithDictionary:[super objectAttributes]];
	objectAttributes[@"error"] = @"error";
	objectAttributes[@"result"] = @"result";
	return objectAttributes;
}
@end

@implementation AssociateSalesHistoryRequest

-(NSString*)xmlTag
{
	return @"associateSalesHistoryRequest";
}

+(NSString*)contextName
{
	return @"ASSOCIATESALESHISTORYREQUEST";
}

-(NSDictionary*)xmlAttributes
{
	NSMutableDictionary *xmlAttributes = [NSMutableDictionary dictionaryWithDictionary:[super xmlAttributes]];
	xmlAttributes[@"associateId"] = @"NSString";
	xmlAttributes[@"baskets"] = @"NSArray";
	xmlAttributes[@"from"] = @"NSString";
	xmlAttributes[@"locationId"] = @"NSString";
	xmlAttributes[@"status"] = @"NSString";
	xmlAttributes[@"to"] = @"NSString";
	return xmlAttributes;
}

-(NSDictionary*)objectAttributes
{
	NSMutableDictionary *objectAttributes = [NSMutableDictionary dictionaryWithDictionary:[super objectAttributes]];
	objectAttributes[@"associateId"] = @"associateId";
	objectAttributes[@"baskets"] = @"baskets";
	objectAttributes[@"from"] = @"from";
	objectAttributes[@"locationId"] = @"locationId";
	objectAttributes[@"status"] = @"status";
	objectAttributes[@"to"] = @"to";
	return objectAttributes;
}
@end

@implementation CustomerSearch

-(NSString*)xmlTag
{
	return @"customerSearch";
}

+(NSString*)contextName
{
	return @"CUSTOMERSEARCH";
}

-(NSDictionary*)xmlAttributes
{
	NSMutableDictionary *xmlAttributes = [NSMutableDictionary dictionaryWithDictionary:[super xmlAttributes]];
	xmlAttributes[@"cardNo"] = @"NSString";
	xmlAttributes[@"cardType"] = @"NSString";
	xmlAttributes[@"customerId"] = @"NSString";
	xmlAttributes[@"customers"] = @"NSArray";
	xmlAttributes[@"email"] = @"NSString";
	return xmlAttributes;
}

-(NSDictionary*)objectAttributes
{
	NSMutableDictionary *objectAttributes = [NSMutableDictionary dictionaryWithDictionary:[super objectAttributes]];
	objectAttributes[@"cardNo"] = @"cardNo";
	objectAttributes[@"cardType"] = @"cardType";
	objectAttributes[@"customerId"] = @"customerId";
	objectAttributes[@"customers"] = @"customers";
	objectAttributes[@"email"] = @"email";
	return objectAttributes;
}
@end

@implementation GiftReceiptRequest

-(NSString*)xmlTag
{
	return @"giftReceiptRequest";
}

+(NSString*)contextName
{
	return @"GIFTRECEIPTREQUEST";
}

-(NSDictionary*)xmlAttributes
{
	NSMutableDictionary *xmlAttributes = [NSMutableDictionary dictionaryWithDictionary:[super xmlAttributes]];
	xmlAttributes[@"basket"] = @"BasketAssociateDTO";
	xmlAttributes[@"basketItems"] = @"NSArray";
	xmlAttributes[@"emailAddress"] = @"NSString";
	xmlAttributes[@"emailRequested"] = @"BOOL";
	xmlAttributes[@"printRequested"] = @"BOOL";
	xmlAttributes[@"textReceipt"] = @"NSString";
	return xmlAttributes;
}

-(NSDictionary*)objectAttributes
{
	NSMutableDictionary *objectAttributes = [NSMutableDictionary dictionaryWithDictionary:[super objectAttributes]];
	objectAttributes[@"basket"] = @"basket";
	objectAttributes[@"basketItems"] = @"basketItems";
	objectAttributes[@"emailAddress"] = @"emailAddress";
	objectAttributes[@"emailRequested"] = @"emailRequested";
	objectAttributes[@"printRequested"] = @"printRequested";
	objectAttributes[@"textReceipt"] = @"textReceipt";
	return objectAttributes;
}
@end

@implementation ProductSearch

-(NSInteger)reasonValue {return [self.reason integerValue];}
-(void)setReasonValue:(NSInteger)reasonValue {self.reason = @(reasonValue);}
-(NSString*)xmlTag
{
	return @"productSearch";
}

+(NSString*)contextName
{
	return @"PRODUCTSEARCH";
}

-(NSDictionary*)xmlAttributes
{
	NSMutableDictionary *xmlAttributes = [NSMutableDictionary dictionaryWithDictionary:[super xmlAttributes]];
	xmlAttributes[@"items"] = @"NSArray";
	xmlAttributes[@"location"] = @"NSString";
	xmlAttributes[@"name"] = @"NSString";
	xmlAttributes[@"reason"] = @"NSInteger";
	xmlAttributes[@"sku"] = @"NSString";
	return xmlAttributes;
}

-(NSDictionary*)objectAttributes
{
	NSMutableDictionary *objectAttributes = [NSMutableDictionary dictionaryWithDictionary:[super objectAttributes]];
	objectAttributes[@"items"] = @"items";
	objectAttributes[@"location"] = @"location";
	objectAttributes[@"name"] = @"name";
	objectAttributes[@"reason"] = @"reason";
	objectAttributes[@"sku"] = @"sku";
	return objectAttributes;
}
@end

@implementation ShopperDetailsDTO

+(NSString*)contextName
{
	return @"SHOPPERDETAILS";
}

-(NSDictionary*)xmlAttributes
{
	NSMutableDictionary *xmlAttributes = [NSMutableDictionary dictionaryWithDictionary:[super xmlAttributes]];
	xmlAttributes[@"keyValue"] = @"NSArray";
	xmlAttributes[@"keyValues"] = @"KeyValues";
	return xmlAttributes;
}

-(NSDictionary*)objectAttributes
{
	NSMutableDictionary *objectAttributes = [NSMutableDictionary dictionaryWithDictionary:[super objectAttributes]];
	objectAttributes[@"keyValue"] = @"keyValue";
	objectAttributes[@"keyValues"] = @"keyValues";
	return objectAttributes;
}
@end

@implementation SoftKey

-(NSString*)xmlTag
{
	return @"softKey";
}

+(NSString*)contextName
{
	return @"SOFTKEY";
}

-(NSDictionary*)xmlAttributes
{
	NSMutableDictionary *xmlAttributes = [NSMutableDictionary dictionaryWithDictionary:[super xmlAttributes]];
	xmlAttributes[@"backgroundColor"] = @"NSString";
	xmlAttributes[@"data"] = @"NSString";
	xmlAttributes[@"dataLocation"] = @"NSString";
	xmlAttributes[@"defaultSelection"] = @"NSString";
	xmlAttributes[@"displayClass"] = @"NSString";
	xmlAttributes[@"downImage"] = @"NSString";
	xmlAttributes[@"foregroundColor"] = @"NSString";
	xmlAttributes[@"keyDescription"] = @"NSString";
	xmlAttributes[@"keyDescriptionLanguages"] = @"NSArray";
	xmlAttributes[@"keyId"] = @"NSString";
	xmlAttributes[@"keyType"] = @"NSString";
	xmlAttributes[@"posFunc"] = @"NSString";
	xmlAttributes[@"softKeyEvents"] = @"NSArray";
	xmlAttributes[@"softKeyMenu"] = @"NSString";
	xmlAttributes[@"tenderType"] = @"NSString";
	xmlAttributes[@"upc"] = @"NSString";
	xmlAttributes[@"upImage"] = @"NSString";
	return xmlAttributes;
}

-(NSDictionary*)objectAttributes
{
	NSMutableDictionary *objectAttributes = [NSMutableDictionary dictionaryWithDictionary:[super objectAttributes]];
	objectAttributes[@"backgroundColor"] = @"backgroundColor";
	objectAttributes[@"data"] = @"data";
	objectAttributes[@"dataLocation"] = @"dataLocation";
	objectAttributes[@"defaultSelection"] = @"defaultSelection";
	objectAttributes[@"displayClass"] = @"displayClass";
	objectAttributes[@"downImage"] = @"downImage";
	objectAttributes[@"foregroundColor"] = @"foregroundColor";
	objectAttributes[@"keyDescription"] = @"primitiveKeyDescription";
	objectAttributes[@"keyDescriptionLanguages"] = @"keyDescriptionLanguages";
	objectAttributes[@"keyId"] = @"keyId";
	objectAttributes[@"keyType"] = @"keyType";
	objectAttributes[@"posFunc"] = @"posFunc";
	objectAttributes[@"softKeyEvents"] = @"softKeyEvents";
	objectAttributes[@"softKeyMenu"] = @"softKeyMenu";
	objectAttributes[@"tenderType"] = @"tenderType";
	objectAttributes[@"upc"] = @"upc";
	objectAttributes[@"upImage"] = @"upImage";
	return objectAttributes;
}

-(NSString*)keyDescription
{
	return [self localizedStringFromLanguages:_keyDescriptionLanguages value:_keyDescription];
}

-(NSString*)primitiveKeyDescription
{
	return _keyDescription;
}
@end

@implementation SoftKeyEventDTO

+(NSString*)contextName
{
	return @"SOFTKEYEVENT";
}

-(NSDictionary*)xmlAttributes
{
	NSMutableDictionary *xmlAttributes = [NSMutableDictionary dictionaryWithDictionary:[super xmlAttributes]];
	return xmlAttributes;
}

-(NSDictionary*)objectAttributes
{
	NSMutableDictionary *objectAttributes = [NSMutableDictionary dictionaryWithDictionary:[super objectAttributes]];
	return objectAttributes;
}
@end

@implementation DataEventDTO

-(NSString*)xmlTag
{
	return @"softkeyDataEvent";
}

+(NSString*)contextName
{
	return @"DATAEVENT";
}

-(NSDictionary*)xmlAttributes
{
	NSMutableDictionary *xmlAttributes = [NSMutableDictionary dictionaryWithDictionary:[super xmlAttributes]];
	xmlAttributes[@"dataLocation"] = @"NSString";
	xmlAttributes[@"dataValue"] = @"NSString";
	return xmlAttributes;
}

-(NSDictionary*)objectAttributes
{
	NSMutableDictionary *objectAttributes = [NSMutableDictionary dictionaryWithDictionary:[super objectAttributes]];
	objectAttributes[@"dataLocation"] = @"dataLocation";
	objectAttributes[@"dataValue"] = @"dataValue";
	return objectAttributes;
}
@end

@implementation POSFunctionSoftKeyEventDTO

-(NSString*)xmlTag
{
	return @"softkeyPOSfunctionEvent";
}

+(NSString*)contextName
{
	return @"POSFUNCTIONSOFTKEYEVENT";
}

-(NSDictionary*)xmlAttributes
{
	NSMutableDictionary *xmlAttributes = [NSMutableDictionary dictionaryWithDictionary:[super xmlAttributes]];
	xmlAttributes[@"function"] = @"NSString";
	return xmlAttributes;
}

-(NSDictionary*)objectAttributes
{
	NSMutableDictionary *objectAttributes = [NSMutableDictionary dictionaryWithDictionary:[super objectAttributes]];
	objectAttributes[@"function"] = @"function";
	return objectAttributes;
}
@end

@implementation SoftKeyInputEventDTO

-(NSString*)xmlTag
{
	return @"softkeyInputEvent";
}

+(NSString*)contextName
{
	return @"SOFTKEYINPUTEVENT";
}

-(NSDictionary*)xmlAttributes
{
	NSMutableDictionary *xmlAttributes = [NSMutableDictionary dictionaryWithDictionary:[super xmlAttributes]];
	xmlAttributes[@"dataLocation"] = @"NSString";
	xmlAttributes[@"dataSource"] = @"NSString";
	xmlAttributes[@"editClass"] = @"NSString";
	xmlAttributes[@"fieldDescription"] = @"NSString";
	xmlAttributes[@"fieldId"] = @"NSString";
	xmlAttributes[@"keyboards"] = @"NSString";
	xmlAttributes[@"required"] = @"BOOL";
	return xmlAttributes;
}

-(NSDictionary*)objectAttributes
{
	NSMutableDictionary *objectAttributes = [NSMutableDictionary dictionaryWithDictionary:[super objectAttributes]];
	objectAttributes[@"dataLocation"] = @"dataLocation";
	objectAttributes[@"dataSource"] = @"dataSource";
	objectAttributes[@"editClass"] = @"editClass";
	objectAttributes[@"fieldDescription"] = @"fieldDescription";
	objectAttributes[@"fieldId"] = @"fieldId";
	objectAttributes[@"keyboards"] = @"keyboards";
	objectAttributes[@"required"] = @"required";
	return objectAttributes;
}
@end

@implementation UIContextEventDTO

-(NSString*)xmlTag
{
	return @"softkeyUiContextEvent";
}

+(NSString*)contextName
{
	return @"UICONTEXTEVENT";
}

-(NSDictionary*)xmlAttributes
{
	NSMutableDictionary *xmlAttributes = [NSMutableDictionary dictionaryWithDictionary:[super xmlAttributes]];
	xmlAttributes[@"context"] = @"NSString";
	return xmlAttributes;
}

-(NSDictionary*)objectAttributes
{
	NSMutableDictionary *objectAttributes = [NSMutableDictionary dictionaryWithDictionary:[super objectAttributes]];
	objectAttributes[@"context"] = @"context";
	return objectAttributes;
}
@end

@implementation Store

-(NSString*)xmlTag
{
	return @"store";
}

+(NSString*)contextName
{
	return @"STORE";
}

-(NSDictionary*)xmlAttributes
{
	NSMutableDictionary *xmlAttributes = [NSMutableDictionary dictionaryWithDictionary:[super xmlAttributes]];
	xmlAttributes[@"address1"] = @"NSString";
	xmlAttributes[@"address2"] = @"NSString";
	xmlAttributes[@"city"] = @"NSString";
	xmlAttributes[@"distance"] = @"double";
	xmlAttributes[@"email"] = @"NSString";
	xmlAttributes[@"hours1"] = @"NSString";
	xmlAttributes[@"hours2"] = @"NSString";
	xmlAttributes[@"hours3"] = @"NSString";
	xmlAttributes[@"hours4"] = @"NSString";
	xmlAttributes[@"hours5"] = @"NSString";
	xmlAttributes[@"hours6"] = @"NSString";
	xmlAttributes[@"hours7"] = @"NSString";
	xmlAttributes[@"latitude"] = @"NSNumber";
	xmlAttributes[@"longitude"] = @"NSNumber";
	xmlAttributes[@"name"] = @"NSString";
	xmlAttributes[@"phone"] = @"NSString";
	xmlAttributes[@"shortName"] = @"NSString";
	xmlAttributes[@"state"] = @"NSString";
	xmlAttributes[@"storeId"] = @"NSString";
	xmlAttributes[@"tills"] = @"NSArray";
	xmlAttributes[@"zipCode"] = @"NSString";
	return xmlAttributes;
}

-(NSDictionary*)objectAttributes
{
	NSMutableDictionary *objectAttributes = [NSMutableDictionary dictionaryWithDictionary:[super objectAttributes]];
	objectAttributes[@"address1"] = @"address1";
	objectAttributes[@"address2"] = @"address2";
	objectAttributes[@"city"] = @"city";
	objectAttributes[@"distance"] = @"distance";
	objectAttributes[@"email"] = @"email";
	objectAttributes[@"hours1"] = @"hours1";
	objectAttributes[@"hours2"] = @"hours2";
	objectAttributes[@"hours3"] = @"hours3";
	objectAttributes[@"hours4"] = @"hours4";
	objectAttributes[@"hours5"] = @"hours5";
	objectAttributes[@"hours6"] = @"hours6";
	objectAttributes[@"hours7"] = @"hours7";
	objectAttributes[@"latitude"] = @"latitude";
	objectAttributes[@"longitude"] = @"longitude";
	objectAttributes[@"name"] = @"name";
	objectAttributes[@"phone"] = @"phone";
	objectAttributes[@"shortName"] = @"shortName";
	objectAttributes[@"state"] = @"state";
	objectAttributes[@"storeId"] = @"storeId";
	objectAttributes[@"tills"] = @"tills";
	objectAttributes[@"zipCode"] = @"zipCode";
	return objectAttributes;
}
@end

@implementation Stores

-(NSString*)xmlTag
{
	return @"stores";
}

+(NSString*)contextName
{
	return @"STORES";
}

-(NSString*)xmlAlias
{
	return @"NSArray";
}

+(instancetype)storesWithArray:(NSArray*)array
{
	Stores* object = [[Stores alloc] init];
	object.stores = array;
	return object;
}

-(NSDictionary*)xmlAttributes
{
	NSMutableDictionary *xmlAttributes = [NSMutableDictionary dictionaryWithDictionary:[super xmlAttributes]];
	return xmlAttributes;
}

-(NSDictionary*)objectAttributes
{
	NSMutableDictionary *objectAttributes = [NSMutableDictionary dictionaryWithDictionary:[super objectAttributes]];
	objectAttributes[@"stores"] = @"stores";
	return objectAttributes;
}
@end

@implementation Tender

-(NSString*)xmlTag
{
	return @"tender";
}

+(NSString*)contextName
{
	return @"TENDER";
}

-(NSDictionary*)xmlAttributes
{
	NSMutableDictionary *xmlAttributes = [NSMutableDictionary dictionaryWithDictionary:[super xmlAttributes]];
	xmlAttributes[@"allowsChange"] = @"BOOL";
	xmlAttributes[@"currencyCode"] = @"NSString";
	xmlAttributes[@"isEFTTender"] = @"BOOL";
	xmlAttributes[@"isForeignCurrency"] = @"BOOL";
	xmlAttributes[@"maximumAmount"] = @"NSString";
	xmlAttributes[@"minimumAmount"] = @"NSString";
	xmlAttributes[@"minimumDenomination"] = @"double";
	xmlAttributes[@"promptForAmount"] = @"BOOL";
	xmlAttributes[@"promptForAuthCode"] = @"BOOL";
	xmlAttributes[@"promptForCardNumber"] = @"BOOL";
	xmlAttributes[@"promptForCardNumberCaptureMethod"] = @"NSString";
	xmlAttributes[@"promptForReference"] = @"BOOL";
	xmlAttributes[@"promptForTip"] = @"BOOL";
	xmlAttributes[@"tenderClass"] = @"NSString";
	xmlAttributes[@"tenderName"] = @"NSString";
	xmlAttributes[@"tenderNameLanguages"] = @"NSArray";
	xmlAttributes[@"tenderType"] = @"NSString";
	xmlAttributes[@"trainingModeTender"] = @"BOOL";
	xmlAttributes[@"validationRegex"] = @"NSString";
	return xmlAttributes;
}

-(NSDictionary*)objectAttributes
{
	NSMutableDictionary *objectAttributes = [NSMutableDictionary dictionaryWithDictionary:[super objectAttributes]];
	objectAttributes[@"allowsChange"] = @"allowsChange";
	objectAttributes[@"currencyCode"] = @"currencyCode";
	objectAttributes[@"isEFTTender"] = @"isEFTTender";
	objectAttributes[@"isForeignCurrency"] = @"isForeignCurrency";
	objectAttributes[@"maximumAmount"] = @"maximumAmount";
	objectAttributes[@"minimumAmount"] = @"minimumAmount";
	objectAttributes[@"minimumDenomination"] = @"minimumDenomination";
	objectAttributes[@"promptForAmount"] = @"promptForAmount";
	objectAttributes[@"promptForAuthCode"] = @"promptForAuthCode";
	objectAttributes[@"promptForCardNumber"] = @"promptForCardNumber";
	objectAttributes[@"promptForCardNumberCaptureMethod"] = @"promptForCardNumberCaptureMethod";
	objectAttributes[@"promptForReference"] = @"promptForReference";
	objectAttributes[@"promptForTip"] = @"promptForTip";
	objectAttributes[@"tenderClass"] = @"tenderClass";
	objectAttributes[@"tenderName"] = @"primitiveTenderName";
	objectAttributes[@"tenderNameLanguages"] = @"tenderNameLanguages";
	objectAttributes[@"tenderType"] = @"tenderType";
	objectAttributes[@"trainingModeTender"] = @"trainingModeTender";
	objectAttributes[@"validationRegex"] = @"validationRegex";
	return objectAttributes;
}

-(NSString*)tenderName
{
	return [self localizedStringFromLanguages:_tenderNameLanguages value:_tenderName];
}

-(NSString*)primitiveTenderName
{
	return _tenderName;
}
@end

@implementation Till

-(NSString*)xmlTag
{
	return @"till";
}

+(NSString*)contextName
{
	return @"TILL";
}

-(NSString*)xmlAlias
{
	return @"NSString";
}

+(instancetype)tillWithString:(NSString*)string
{
	Till* object = [[Till alloc] init];
	object.till = string;
	return object;
}

-(NSDictionary*)xmlAttributes
{
	NSMutableDictionary *xmlAttributes = [NSMutableDictionary dictionaryWithDictionary:[super xmlAttributes]];
	return xmlAttributes;
}

-(NSDictionary*)objectAttributes
{
	NSMutableDictionary *objectAttributes = [NSMutableDictionary dictionaryWithDictionary:[super objectAttributes]];
	objectAttributes[@"till"] = @"till";
	return objectAttributes;
}
@end
