//
//  BasketTaxFreeDocumentDTO+MPOSExtensions.m
//  mPOS
//
//  Created by John Scott on 18/03/2015.
//  Copyright (c) 2015 Clarity. All rights reserved.
//

#import "BasketTaxFreeDocumentDTO+MPOSExtensions.h"

@implementation BasketTaxFreeDocumentDTO (MPOSExtensions)

-(NSString *)basketType
{
    return @"taxfree";
}

@end
