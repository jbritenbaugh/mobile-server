//
//  ServerErrorMessage+MPOSExtensions.m
//  mPOS
//
//  Created by John Scott on 07/05/2015.
//  Copyright (c) 2015 Clarity. All rights reserved.
//

#import "ServerErrorMessage+MPOSExtensions.h"

@implementation ServerErrorMessage (MPOSExtensions)

-(id)valueForKey:(NSString *)key
{
    if ([key isEqualToString:@"value"] && [self isMemberOfClass:ServerErrorMessage.class])
    {
        NSString *localizedString = NSLocalizedStringWithDefaultValue(self.name, @"MShopperExceptions", [NSBundle mainBundle], (NSString*) [NSNull null], nil);
        
        if (![localizedString isEqual:[NSNull null]])
        {
            return localizedString;
        }
    }
    return [super valueForKey:key];
}


@end
