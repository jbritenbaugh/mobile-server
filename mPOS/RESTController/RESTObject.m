//
//  RESTObject.m
//  mPOS
//
//  Created by John Scott on 14/02/2014.
//  Copyright (c) 2014 Clarity. All rights reserved.
//

#import "RESTObject.h"
#import "RESTController.h"
#import <objc/runtime.h>

@implementation RESTObject
{
    NSMutableDictionary *_dynamicValues;
}

-(void)encodeWithCoder:(NSCoder *)aCoder
{
    NSDictionary *objectAttributes = [self objectAttributes];
    
    for (NSString *attribute in objectAttributes)
    {
        [aCoder encodeObject:[self valueForKey:objectAttributes[attribute]] forKey:attribute];
    }
}

-(id)initWithCoder:(NSCoder *)aDecoder
{
    self = [self init];
    if (self != nil)
    {
        NSDictionary *objectAttributes = [self objectAttributes];
        
        for (NSString *attribute in objectAttributes)
        {
            [self setValue:[aDecoder decodeObjectForKey:attribute] forKey:attribute];
        }
    }
    return self;
}

-(instancetype)init
{
    self = [super init];
    if (self)
    {
        _dynamicValues = [NSMutableDictionary dictionary];
    }
    return self;
}

#if 0
-(NSString*)OCGDebugger_htmlDescriptionWithFormatOptions:(NSDictionary *)formatOptions
{
    if ([self xmlAlias])
    {
        return OCGDebuggerHtmlObjectDescription([self valueForKey:[self xmlTag]], formatOptions);
    }
    
    NSDictionary *objectAttributes = [self objectAttributes];
    NSDictionary *xmlAttributes = [self xmlAttributes];
    
    NSMutableDictionary *values = [NSMutableDictionary dictionaryWithCapacity:objectAttributes.count];
    
    for (NSString *attribute in objectAttributes)
    {
        id value = [self valueForKey:objectAttributes[attribute]];
        
        if ([attribute isEqual:[self parentAttribute]])
        {
            if (value)
            {
                values[attribute] = [value primitiveDescription];
            }
        }
        else
        {
            values[attribute] = OCGDebuggerHtmlObjectDescription(value, formatOptions);
        }
    }
    
    NSMutableString *description = [NSMutableString string];
    
    [description appendString:@"<div class='object'>"];
    [description appendString:OCGDebuggerHtmlObjectDescription([super description], formatOptions)];
    [description appendString:@"<dl>"];
    for (NSString* attribute in [values.allKeys sortedArrayUsingSelector:@selector(compare:)])
    {
        [description appendFormat:@"<dt>%@</dt><dd>%@</dd>", attribute, values[attribute]];
    }
    [description appendString:@"</dl>"];
    [description appendString:@"</div>"];
    return description;
}
#endif

#if DEBUG
/*
 See http://stackoverflow.com/a/23936634/542244. isNSDictionary__ is set here to ensire that NSDictionary
 and NSArray call descriptionWithLocale:indent:

 THIS IS A HACK and must not be part of a release build.
 */

- (BOOL) isNSDictionary__
{
	return YES;
}

- (NSString *) description
{
    return [self descriptionWithLocale:nil indent:0];
}

- (NSString *)primitiveDescription
{
    return [super description];
}

- (NSString *)descriptionWithLocale:(id)locale indent:(NSUInteger)level
{
    id values = nil;
    if ([self xmlAlias])
    {
        values = [self valueForKey:[self xmlTag]];
    }
    else
    {
        NSDictionary *objectAttributes = [self objectAttributes];
        NSDictionary *xmlAttributes = [self xmlAttributes];
        
        values = [NSMutableDictionary dictionaryWithCapacity:objectAttributes.count];

        for (NSString *attribute in objectAttributes)
        {
            id value = [self valueForKey:objectAttributes[attribute]];
            
            if ([attribute isEqual:[self parentAttribute]])
            {
                if (value)
                {
                    values[attribute] = [value primitiveDescription];
                }
            }
            else if ([xmlAttributes[objectAttributes[attribute]] isEqualToString:@"BOOL"])
            {
                values[attribute] = [value boolValue] ? @"true" : @"false";
            }
            else if (value)
            {
                values[attribute] = value;
            }
        }
    }
    
    if ([values respondsToSelector:@selector(descriptionWithLocale:indent:)])
    {
        return [[super description] stringByAppendingString:[values descriptionWithLocale:locale indent:level+1]];
    }
    else if (values)
    {
        return [[[super description] stringByAppendingString:@" "] stringByAppendingString:[values description]];
    }
    else
    {
        return [super description];
    }
}

#endif


- (void)setNilValueForKey:(NSString *)key
{
    [self setValue:@(0) forKey:key];
}

- (id)valueForUndefinedKey:(NSString *)key
{
    return [_dynamicValues valueForKey:key];
}

-(void)setValue:(id)value forUndefinedKey:(NSString *)key
{
    [_dynamicValues setValue:value forKey:key];
}

- (NSString*)xmlTag
{
    return nil;
}

+ (NSString*)contextName
{
    return nil;
}

- (NSString*)xmlAlias
{
    return nil;
}

- (NSDictionary*)xmlAttributes
{
    return @{};
}

- (NSDictionary*)objectAttributes
{
    return @{};
}

- (NSString*)parentAttribute
{
    return nil;
}

-(NSString*)xmlString
{
    return [RESTController.sharedInstance stringForObject:self type:nil];
}

-(NSString*)localizedStringFromLanguages:(NSArray*)languages value:(NSString*)value
{
    NSString *localeLanguageCode = [[NSLocale currentLocale] localeIdentifier];
    NSString *localizedString = value;
    BOOL didFindLocalizedString = NO;
    for (id language in languages)
    {
        NSString *localizedStringCandidate = [language valueForKey:@"value"];
        if ([[language valueForKey:@"code"] isEqual:localeLanguageCode] && ![localizedStringCandidate hasPrefix:@"™™_"])
        {
            localizedString = localizedStringCandidate;
            didFindLocalizedString = YES;
            break;
        }
    }
    
#if DEBUG
    if (!didFindLocalizedString && localeLanguageCode.length && ![localeLanguageCode hasPrefix:@"en"] && localizedString.length > 0)
    {
        localizedString = [NSString stringWithFormat:@"⚠%@⚠", localizedString];
    }
#endif
    return localizedString;
}

- (id)copyWithZone:(NSZone *)zone;
{
    RESTObject *copy = [[self.class allocWithZone:zone] init];
    [copy copyValuesFromObject:self];
    return copy;
}

- (void)copyValuesFromObject:(RESTObject*)object
{
    if ([object xmlAlias])
    {
        NSString *attribute = [object xmlTag];
        [self setValue:[[object valueForKey:attribute] copy] forKey:attribute];
    }
    else
    {
        NSDictionary *objectAttributes = [object objectAttributes];
        
        for (NSString *attribute in objectAttributes)
        {
            id value = [object valueForKey:objectAttributes[attribute]];
            
            if (![attribute isEqual:self.parentAttribute])
            {
                value = [value copy];
            }
            
            [self setValue:value forKey:attribute];
        }
    }
}

#define IS_EQUAL(v1, v2) ((v1) == (v2) || [(v1) isEqual:(v2)])
#define IS_ATTRIBUTE_EQUAL(o1, o2, a) IS_EQUAL([o1 valueForKey:a], [o2 valueForKey:a])

-(BOOL)isEqual:(RESTObject*)object
{
    if (self.class == object.class)
    {
        NSDictionary *objectAttributes = [self objectAttributes];
        
        for (NSString *attribute in objectAttributes)
        {
            if (![attribute isEqual:self.parentAttribute] && !IS_ATTRIBUTE_EQUAL(self, object, objectAttributes[attribute]))
            {
                return NO;
            }
        }
        return YES;
    }
    else
    {
        return NO;
    }
}

/*
 The following methods provide support properties for defined in categories. This means we don't have to contaminate the
 web_definitions.ini file with properties that are just used in the client.
 
 methodSignatureForSelector: and forwardInvocation: work together to present functioning getters and setter methods
 for each @dynamic property defined on a subclass or category of a subclass. Objects are saved and returned as-is while
 we use NSValue to box primitave values.
 
 All values are stored in _dynamicValues
 */

NSString *_RESTObjectPropertyNameFromSelector(SEL aSelector, BOOL *isSetter)
{
    NSString *propertyName = NSStringFromSelector(aSelector);
    if ([propertyName hasSuffix:@":"])
    {
        if (isSetter)
        {
            *isSetter = YES;
        }
        propertyName = [propertyName stringByReplacingOccurrencesOfString:@"set" withString:@""];
        propertyName = [propertyName stringByReplacingOccurrencesOfString:@":" withString:@""];
        
        propertyName = [propertyName stringByReplacingCharactersInRange:NSMakeRange(0, 1)
                                                 withString:[[propertyName substringWithRange:NSMakeRange(0, 1)] lowercaseString]];
    }
    else
    {
        if (isSetter)
        {
            *isSetter = NO;
        }
    }
    return propertyName;
}

-(NSMethodSignature *)methodSignatureForSelector:(SEL)aSelector
{
    BOOL isSetter = NO;
    NSString *propertyName = _RESTObjectPropertyNameFromSelector(aSelector, &isSetter);
    objc_property_t property = class_getProperty(self.class, propertyName.UTF8String);
    NSMethodSignature *methodSignature = nil;
    if (property)
    {
        char *propertyType = property_copyAttributeValue(property, "T");
        NSString *types = nil;
        if (isSetter)
        {
            types = [NSString stringWithFormat:@"v@:%s", propertyType];
        }
        else
        {
            types = [NSString stringWithFormat:@"%s@:", propertyType];
        }
        methodSignature = [NSMethodSignature signatureWithObjCTypes:types.UTF8String];
        // to quote property_copyAttributeValue's documentation "You must free the returned value string with free()."
        // apparently this applies in ARC land too.
        free(propertyType);
    }
    return methodSignature;
}

-(void)forwardInvocation:(NSInvocation *)anInvocation
{
    BOOL isSetter = NO;
    NSString *propertyName = _RESTObjectPropertyNameFromSelector(anInvocation.selector, &isSetter);
    
    if (isSetter)
    {
        const char *propertyType = [anInvocation.methodSignature getArgumentTypeAtIndex:2];
        void *value = NULL;
        [anInvocation getArgument:&value atIndex:2];
        id propertyValue = nil;
        if (propertyType[0] != @encode(id)[0])
        {
            propertyValue = [NSValue valueWithBytes:&value objCType:propertyType];
        }
        else
        {
            propertyValue = (__bridge id)value;
        }
        [_dynamicValues setValue:propertyValue forKey:propertyName];
    }
    else
    {
        const char *propertyType = [anInvocation.methodSignature methodReturnType];
        void *value = NULL;
        id propertyValue = [_dynamicValues valueForKey:propertyName];
        if (propertyType[0] != @encode(id)[0])
        {
            [propertyValue getValue:&value];
        }
        else
        {
            value = (__bridge void *)propertyValue;
        }
        [anInvocation setReturnValue:&value];
    }
}

@end
