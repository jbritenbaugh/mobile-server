//
//  BasketTaxFreeDocumentDTO+MPOSExtensions.h
//  mPOS
//
//  Created by John Scott on 18/03/2015.
//  Copyright (c) 2015 Clarity. All rights reserved.
//

#import "RESTController.h"

@interface BasketTaxFreeDocumentDTO (MPOSExtensions)

@end
