//
//  Device+MPOSExtensions.h
//  mPOS
//
//  Created by Antonio Strijdom on 25/02/2015.
//  Copyright (c) 2015 Clarity. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RESTController.h"

typedef NS_ENUM(NSInteger, DeviceSelectionMode)
{
    DeviceSelectionModeNone = 0,
    DeviceSelectionModeRDM = 1,
    DeviceSelectionModeProvider = 2
};

typedef NS_ENUM(NSInteger, DeviceConnectionType)
{
    DeviceConnectionTypeIP = 0,
    DeviceConnectionTypeBT = 1
};

@interface Device (MPOSExtensions)

@property (nonatomic, readonly) DeviceSelectionMode selectionMode;
@property (nonatomic, readonly) DeviceConnectionType connectionType;

@end
