//
//  CoreDataWrapper.h
//  mPOS
//
//  Created by John Scott on 30/05/2014.
//  Copyright (c) 2014 Clarity. All rights reserved.
//

#import "RESTController.h"

@compatibility_alias MPOSBasket BasketDTO;
@compatibility_alias MPOSBasketCustomerAffiliation BasketCustomerAffiliation;
@compatibility_alias MPOSBasketCustomerCard BasketCustomerCard;
@compatibility_alias MPOSBasketItem BasketItem;
@compatibility_alias MPOSBasketRewardLine BasketRewardLine;
@compatibility_alias MPOSBasketRewardLineDiscountAmount BasketRewardLineDiscountAmount;
@compatibility_alias MPOSBasketRewardLineDiscountPercent BasketRewardLineDiscountPercent;
@compatibility_alias MPOSBasketRewardLinePriceAdjust BasketRewardLinePriceAdjust;
@compatibility_alias MPOSBasketRewardSale BasketRewardSale;
@compatibility_alias MPOSBasketRewardSaleDiscountAmount BasketRewardSaleDiscountAmount;
@compatibility_alias MPOSBasketRewardSaleDiscountPercent BasketRewardSaleDiscountPercent;
@compatibility_alias MPOSBasketRewardSalePromotionAmount BasketRewardSalePromotionAmount;
@compatibility_alias MPOSBasketTender BasketTender;
@compatibility_alias MPOSCustomer Customer;
@compatibility_alias MPOSCustomerAddress CustomerAddress;
@compatibility_alias MPOSCustomerCard CustomerCard;
@compatibility_alias MPOSCustomerEmail CustomerEmail;
@compatibility_alias MPOSCustomerSearch CustomerSearch;
@compatibility_alias MPOSEFTPayment EFTPayment;
@compatibility_alias MPOSItem Item;
@compatibility_alias MPOSItemAvailability ItemAvailability;
@compatibility_alias MPOSItemImages ItemImages;
@compatibility_alias MPOSModifierGroup ItemModifierGroup;
@compatibility_alias MPOSItemProperties ItemProperties;
@compatibility_alias MPOSProductSearch ProductSearch;
@compatibility_alias MPOSReturnItem ReturnItem;
@compatibility_alias MPOSSaleItem SaleItem;
@compatibility_alias MPOSSalesPerson SalesPerson;
@compatibility_alias MPOSSelectedModifier SelectedModifier;
@compatibility_alias MPOSServerRequest ServerRequest;
@compatibility_alias MPOSStandardPayment StandardPayment;
@compatibility_alias MPOSStore Store;
@compatibility_alias MPOSVerifonePayment VerifonePayment;
@compatibility_alias MPOSYespayPayment YespayPayment;
@compatibility_alias MPOSPrintDataLine PrintDataLine;
