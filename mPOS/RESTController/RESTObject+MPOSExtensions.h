//
//  RESTObject+MPOSExtensions.h
//  mPOS
//
//  Created by John Scott on 26/03/2015.
//  Copyright (c) 2015 Clarity. All rights reserved.
//

#import "RESTObject.h"

@interface RESTObject (MPOSExtensions)

@property (nonatomic, strong) NSString* uiState;

@end
