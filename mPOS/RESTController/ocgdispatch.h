//
//  ocgdispatch.h
//  mPOS
//
//  Created by John Scott on 28/04/2014.
//  Copyright (c) 2014 Clarity. All rights reserved.
//

#import <Foundation/Foundation.h>

/*
 convenience function to dispatch a block just once
 */

void ocg_once(dispatch_block_t block);

#define ocg_once(b) \
({ \
    static dispatch_once_t predicate; \
    dispatch_once(&predicate, b); \
})

/*
 dispatch block on the main queue
*/
void ocg_sync_main(dispatch_block_t block);

/*
 dispatch block on a background queue but block the UI.
 */
void ocg_async_background_overlay(dispatch_block_t block);

/*
 call block directly (mostly used in testing)
*/

void ocg_sync(dispatch_block_t block);

/*
 create a weak reference to self (saves faffing about with __weak and types et al)
 */

#define ocg_define_weak_self() \
__weak __typeof(self) __ocg_weak_self = self;

#define ocg_get_weak_self() \
(__ocg_weak_self)

