//
//  KeyValues+MPOSExtensions.m
//  mPOS
//
//  Created by John Scott on 16/04/2015.
//  Copyright (c) 2015 Clarity. All rights reserved.
//

#import "KeyValues+MPOSExtensions.h"

@implementation KeyValues (MPOSExtensions)

-(id)valueForKey:(NSString *)key
{
    if (![key isEqualToString:@"array"])
    {
        for (KeyValueDTO *keyValue in self.array)
        {
            if ([key isEqualToString:keyValue.key])
            {
                return keyValue.value;
            }
        }
        
        return nil;
    }
    else
    {
        return [super valueForKey:key];
    }
}

-(void)setValue:(id)value forKey:(NSString *)key
{
    if (![key isEqualToString:@"array"])
    {
        for (KeyValueDTO *keyValue in self.array)
        {
            if ([key isEqualToString:keyValue.key])
            {
                keyValue.value = value;
                return;
            }
        }
        
        // Not found add a new one.
        
        KeyValueDTO *keyValue = [[KeyValueDTO alloc] init];
        keyValue.key = key;
        keyValue.value = value;
        self.array = [self.array?:@[] arrayByAddingObject:keyValue];
    }
    else
    {
        [super setValue:value forKey:key];
    }
}

@end
