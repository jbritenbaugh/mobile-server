//
//  BasketAssociateDTO+MPOSExtensions.m
//  mPOS
//
//  Created by John Scott on 18/03/2015.
//  Copyright (c) 2015 Clarity. All rights reserved.
//

#import "BasketAssociateDTO+MPOSExtensions.h"

@implementation BasketAssociateDTO (MPOSExtensions)

-(NSString *)basketType
{
    return @"basket";
}

@end
