//
//  ServerError+MPOSExtensions.m
//  mPOS
//
//  Created by John Scott on 29/01/2015.
//  Copyright (c) 2015 Clarity. All rights reserved.
//

#import "ServerError+MPOSExtensions.h"
#import "OverrideErrorMessage+MPOSExtensions.h"

@implementation ServerError (MPOSExtensions)

@dynamic transportError;
@dynamic invocation;

+(id)fakeError
{
    ServerError *error = [[ServerError alloc] init];
    ServerErrorMessage *errorMessage = [[ServerErrorMessage alloc] init];
    errorMessage.orderValue = 0;
    errorMessage.code = @(359);
    errorMessage.value = @"Unable to contact New Providence colony";
    error.messages = @[errorMessage];
    return error;
}

-(void)addMessage:(ServerErrorMessage*)message
{
    NSMutableArray *messages = [NSMutableArray arrayWithArray:self.messages];
    [messages addObject:message];
    self.messages = [messages copy];
}

-(void)removeMessage:(ServerErrorMessage*)message
{
    NSMutableArray *messages = [NSMutableArray arrayWithArray:self.messages];
    [messages removeObject:message];
    self.messages = [messages copy];
}

- (BOOL) overrideErrorMessageCheck
{
    BOOL result = NO;
    
    for (ServerErrorMessage *message in self.messages) {
        if ([message isKindOfClass:[OverrideErrorMessage class]]) {
            OverrideErrorMessage *override = (OverrideErrorMessage *)message;
            if ((override.parsedErrorLevel != OverrideErrorMessageLevelUnknown) &&
                (override.parsedErrorLevel != OverrideErrorMessageLevelAbort)) {
                result = YES;
            }
            break;
        }
    }
    
    return result;
}

- (ServerErrorMessage *) deviceStationErrorMessage
{
    ServerErrorMessage *result = nil;
    
    for (ServerErrorMessage *message in self.messages) {
        if ((message.codeValue == MShopperDeviceStationNotFoundExceptionCode) ||
            (message.codeValue == MShopperDeviceStationClaimedExceptionCode) ||
            (message.codeValue == MShopperDeviceStationConfigurationErrorExceptionCode) ||
            (message.codeValue == MShopperDeviceStationDeviceErrorExceptionCode) ||
            (message.codeValue == MShopperDeviceStationNotClaimedExceptionCode) ||
            (message.codeValue == MShopperDeviceStationUnavailableExceptionCode) ||
            (message.codeValue == MShopperDeviceStationDrawerNotAssignedExceptionCode) ||
            (message.codeValue == MShopperDeviceStationPrinterCoverOpenExceptionCode) ||
            (message.codeValue == MShopperDeviceStationPrinterPaperLowExceptionCode) ||
            (message.codeValue == MShopperDeviceStationPrinterPaperOutExceptionCode) ||
            (message.codeValue == MShopperDeviceStationPrinterSlipInsertionRequiredExceptionCode) ||
            (message.codeValue == MShopperDeviceStationPrinterSlipRemovalRequiredExceptionCode) ||
            (message.codeValue == MShopperDeviceStationPrinterErrorExceptionCode) ||
            (message.codeValue == MShopperDeviceStationCashDrawerErrorExceptionCode) ||
            (message.codeValue == MShopperDeviceStationDrawerValueInUseExceptionCode) ||
            (message.codeValue == MShopperDeviceStationErrorExceptionCode) ||
            (message.codeValue == MShopperDeviceStationInvalidDrawerValueProvidedExceptionCode)) {
            result = message;
        }
    }
    
    return result;
}

@end
