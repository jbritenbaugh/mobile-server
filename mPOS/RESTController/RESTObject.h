//
//  RESTObject.h
//  mPOS
//
//  Created by John Scott on 14/02/2014.
//  Copyright (c) 2014 Clarity. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RESTInvocation.h"

@protocol RESTObject <NSObject>

- (NSString*)xmlTag;
+ (NSString*)contextName;
- (NSString*)xmlAlias;
- (NSDictionary*)xmlAttributes;
- (NSDictionary*)objectAttributes;
- (NSString*)parentAttribute;
- (NSString*)localizedStringFromLanguages:(NSArray*)languages value:(NSString*)value;

@end

/*
  One of the last actions in removing the old RESTClient will be to
 remove xmlString.
 */

@interface RESTObject : NSObject <RESTObject, NSCoding, NSCopying>

-(NSString*)xmlString;

- (void)copyValuesFromObject:(RESTObject*)object;

@property (nonatomic, strong) NSString *status;

@end
