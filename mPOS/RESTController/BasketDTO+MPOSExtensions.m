//
//  BasketDTO+MPOSExtensions.m
//  mPOS
//
//  Created by John Scott on 25/06/2014.
//  Copyright (c) 2014 Clarity. All rights reserved.
//

#import "BasketDTO+MPOSExtensions.h"

@implementation MPOSBasket (MPOSExtensions)

@dynamic deviceStationId;
@dynamic emailAddress;
@dynamic emailReceipt;
@dynamic enablePrint;
@dynamic flightDetails;
@dynamic giftReceipt;
@dynamic giftReceiptRequired;
@dynamic printSummaryReceipt;
@dynamic reprint;

-(NSString *)basketType
{
    NSAssert(NO, @"BasketDTO is an abstract class so calling basketType will not end well");
    return nil;
}

-(void)addBasketItem:(MPOSBasketItem*)basketItem
{
    NSMutableArray *basketItems = [NSMutableArray arrayWithArray:self.basketItems];
    basketItem.basket = self;
    [basketItems addObject:basketItem];
    self.basketItems = [basketItems copy];
}

-(void)removeBasketItem:(MPOSBasketItem*)basketItem
{
    NSMutableArray *basketItems = [NSMutableArray arrayWithArray:self.basketItems];
    [basketItems removeObject:basketItem];
    self.basketItems = [basketItems copy];
}

@end
