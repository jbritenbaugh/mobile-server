//
//  MPOSBasket+MPOSExtensions.h
//  mPOS
//
//  Created by John Scott on 25/06/2014.
//  Copyright (c) 2014 Clarity. All rights reserved.
//

#import "RESTController.h"

@interface BasketDTO (MPOSExtensions)

@property (nonatomic, strong) NSString* deviceStationId;
@property (nonatomic, strong) NSString* emailAddress;
@property (nonatomic, assign) BOOL emailReceipt;
@property (nonatomic, assign) BOOL enablePrint;
@property (nonatomic, strong) BasketFlightDetails* flightDetails;
@property (nonatomic, strong) NSString* giftReceipt;
@property (nonatomic, assign) BOOL giftReceiptRequired;
@property (nonatomic, assign) BOOL printSummaryReceipt;
@property (nonatomic, assign) BOOL reprint;

@property (nonatomic, readonly) NSString *basketType;

-(void)addBasketItem:(MPOSBasketItem*)basketItem;
-(void)removeBasketItem:(MPOSBasketItem*)basketItem;

@end

