//
//  ServerError+MPOSExtensions.h
//  mPOS
//
//  Created by John Scott on 29/01/2015.
//  Copyright (c) 2015 Clarity. All rights reserved.
//

#import "RESTController.h"

@interface ServerError (MPOSExtensions)

-(void)addMessage:(ServerErrorMessage*)message;
-(void)removeMessage:(ServerErrorMessage*)message;

@property (nonatomic, strong) NSError *transportError;

/** @property invocation
 *  @brief The REST invocation that created this RESTObject.
 */
@property (nonatomic, strong) RESTInvocation *invocation;

/** @brief Determines if this server error contains an override.
 *  @return YES if the error has an override, otherwise NO.
 */
- (BOOL) overrideErrorMessageCheck;

/** @brief Returns a device station error message (if one exists).
 *  @return The device station error message or nil.
 */
- (ServerErrorMessage *) deviceStationErrorMessage;

@end
