//
//  RESTInvocation.h
//  mPOS
//
//  Created by John Scott on 02/03/2015.
//  Copyright (c) 2015 Clarity. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RESTControllerBase.h"

@interface RESTInvocation : NSObject <NSCopying>

@property (nonatomic, readonly) NSString* RESTMethod;
@property (nonatomic, readonly) NSString* RESTPath;
@property (nonatomic, readonly) NSDictionary* RESTParameterTypes;
@property (nonatomic, readonly) NSString* RESTBodyParameter;
@property (nonatomic, readonly) NSString* RESTReturnType;

@property (nonatomic, readonly) NSTimeInterval RESTTimeoutInterval;

@property (nonatomic, strong) NSString* token;
@property (nonatomic, strong) RESTControllerCompleteBlockType complete;

@property (nonatomic, readonly) NSArray *errorNumber;
@property (nonatomic, readonly) NSArray *overrideBasketItemId;
@property (nonatomic, readonly) NSArray *approverId;
@property (nonatomic, readonly) NSArray *approverPwd;

- (void) addOverrideForErrorNumber:(NSString *)errorNumber
                   forBasketItemId:(NSString *)basketItemId
                    withApproverId:(NSString *)approverId
               andApproverPassword:(NSString *)approverPassword;

@end
