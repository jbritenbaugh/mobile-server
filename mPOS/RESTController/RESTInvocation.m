//
//  RESTInvocation.m
//  mPOS
//
//  Created by John Scott on 02/03/2015.
//  Copyright (c) 2015 Clarity. All rights reserved.
//

#import "RESTInvocation.h"

@implementation RESTInvocation

-(NSDictionary *)RESTParameterTypes
{
    NSMutableDictionary *parameterTypes = [NSMutableDictionary dictionary];
    parameterTypes[@"token"] = @"NSString";
    parameterTypes[@"overrideBasketItemId"] = @"NSArray";
    parameterTypes[@"approverId"] = @"NSArray";
    parameterTypes[@"approverPwd"] = @"NSArray";
    parameterTypes[@"errorNumber"] = @"NSArray";
    return parameterTypes;
}

-(NSString *)RESTBodyParameter
{
    return nil;
}

-(NSString *)RESTReturnType
{
    return nil;
}

-(NSTimeInterval)RESTTimeoutInterval
{
    return 30;
}

- (void) addOverrideForErrorNumber:(NSString *)errorNumber
                   forBasketItemId:(NSString *)basketItemId
                    withApproverId:(NSString *)approverId
               andApproverPassword:(NSString *)approverPassword
{
    NSAssert(errorNumber != nil, @"Must specify an error number for overrides");
    
    // errorNumber is required
    if (errorNumber) {
        NSMutableArray *errorNumbers = [self.errorNumber mutableCopy];
        NSMutableArray *basketItemIds = [self.overrideBasketItemId mutableCopy];
        NSMutableArray *approverIds = [self.approverId mutableCopy];
        NSMutableArray *approverPasswords = [self.approverPwd mutableCopy];
        
        if (errorNumbers == nil) errorNumbers = [NSMutableArray array];
        if (basketItemIds == nil) basketItemIds = [NSMutableArray array];
        if (approverIds == nil) approverIds = [NSMutableArray array];
        if (approverPasswords == nil) approverPasswords = [NSMutableArray array];
        
        [errorNumbers addObject:errorNumber];
        [basketItemIds addObject:(basketItemId == nil) ? @"" : basketItemId];
        [approverIds addObject:(approverId == nil) ? @"" : approverId];
        [approverPasswords addObject:(approverPassword == nil) ? @"" : approverPassword];
        
        _errorNumber = errorNumbers;
        _overrideBasketItemId = basketItemIds;
        _approverId = approverIds;
        _approverPwd = approverPasswords;
    }
}

- (id)copyWithZone:(NSZone *)zone;
{
    RESTInvocation *copy = [[self.class allocWithZone:zone] init];
    [copy copyValuesFromObject:self];
    return copy;
}

- (void)copyValuesFromObject:(RESTInvocation*)object
{
    NSDictionary *parameters = object.RESTParameterTypes;
    
    for (NSString *parameter in parameters)
    {
        [self setValue:[[object valueForKey:parameter] copy] forKey:parameter];
    }
}

#if DEBUG
/*
 See http://stackoverflow.com/a/23936634/542244. isNSDictionary__ is set here to ensire that NSDictionary
 and NSArray call descriptionWithLocale:indent:
 
 THIS IS A HACK and must not be part of a release build.
 */

- (BOOL) isNSDictionary__
{
    return YES;
}

- (NSString *) description
{
    return [self descriptionWithLocale:nil indent:0];
}

- (NSString *)descriptionWithLocale:(id)locale indent:(NSUInteger)level
{
    NSDictionary *parameterTypes = [self RESTParameterTypes];
    
    NSMutableDictionary *values = [NSMutableDictionary dictionaryWithCapacity:parameterTypes.count];
    
    
    for (NSString *attribute in parameterTypes)
    {
        id value = [self valueForKey:attribute];
        
        if ([parameterTypes[attribute] isEqualToString:@"BOOL"])
        {
            values[attribute] = [value boolValue] ? @"true" : @"false";
        }
        else if (value)
        {
            values[attribute] = value;
        }
    }

    return [[super description] stringByAppendingString:[values descriptionWithLocale:locale indent:level+1]];
}

#endif

@end
