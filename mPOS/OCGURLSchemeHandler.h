//
//  OCGURLSchemeHandler.h
//  mPOS
//
//  Created by Antonio Strijdom on 12/08/2013.
//  Copyright (c) 2013 Clarity. All rights reserved.
//

#import <Foundation/Foundation.h>

/** @file OCGURLSchemeHandler.h */

/** @protocol OCGURLProviderProtocol 
 *  @brief Defines a common protocol for handling URL integration
 */
@protocol OCGURLProviderProtocol <NSObject>
@required
/** @brief This should be called by your app delegate to pass on values
 @return YES if the scheme handler successfully handled the request;
 NO if the attempt to open the URL resource failed.
 */
- (BOOL) application:(UIApplication *)application
             openURL:(NSURL *)url
   sourceApplication:(NSString *)sourceApplication
          annotation:(id)annotation;
@optional
/** @brief Determines if the URL passed in is a valid
 *  URL for this provider.
 *  @param url The url to check.
 *  @return YES if the url is handled by this provider, otherwise NO.
 */
- (BOOL) isProviderURL:(NSURL *)url;
@end

/** @brief Base class for URL scheme handlers. */
@interface OCGURLSchemeHandler : NSObject <OCGURLProviderProtocol>

/** @brief Builds a dictionary from a URL query string key/value pairs.
 *  @param query The query string to convert.
 *  @return The dictionary.
 */
- (NSDictionary *) dictionaryForQuery:(NSString *)query;

/** @brief Converts a dictionary into a percent escaped URL query key/value string.
 *  @param dictionary The dictionary to convert.
 *  @return The query string.
 */
- (NSString *) queryForDictionary:(NSDictionary *)dictionary;

/** @brief Converts a hex string (in UTF8 format) into a string.
 *  @param hexString The hex string.
 *  @note Must be a string of hex numbers with UTF8 encoding.
 *  @return The string.
 */
- (NSString *) stringForHexString:(NSString *)hexString;

/** @brief Converts a string into a UTF8 encoded hex string.
 *  @param The string to convert.
 *  @return The hex string.
 */
- (NSString *) hexStringForString:(NSString *)string;

/** @brief Returns the URL scheme used by this class
 *  @return returnURLScheme the URL scheme used by this class
 */
- (NSString *) returnURLScheme;

@end
