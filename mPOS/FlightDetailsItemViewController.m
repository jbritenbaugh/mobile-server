//
//  FlightDetailsItemViewController.m
//  mPOS
//
//  Created by John Scott on 05/02/2014.
//  Copyright (c) 2015 Clarity. All rights reserved.
//

#import "FlightDetailsItemViewController.h"
#import "NSArray+OCGIndexPath.h"

#import "UICollectionView+OCGExtensions.h"
#import "FNKCollectionViewLayout.h"

#import "OCGCollectionViewCell.h"
#import "BasketItemCollectionViewCell.h"
#import "OCGTextField.h"
#import "OCGFirstResponderAccessoryView.h"

#import "RESTController.h"
#import "ServerErrorHandler.h"
#import "UIAlertView+OCGBlockAdditions.h"
#import "BarcodeScannerController.h"
#import "OCGSelectViewController.h"
#import "BasketController.h"
#import "CRSSkinning.h"
#import "OCGNumberTextFieldValidator.h"
#import "CRSLocationController.h"

@interface FlightDetailsItemViewController () <UITextFieldDelegate, OCGFirstResponderAccessoryViewDelegate>

@end

@implementation FlightDetailsItemViewController
{
    NSMutableArray *_visibleDetails;
    NSMutableDictionary *_itemDetails;
    OCGFirstResponderAccessoryView *_firstResponderAccessoryView;
    UIDatePicker *_datePicker;
    NSDateFormatter *_dateFormatter;
    NSDateFormatter *_displayDateFormatter;
    OCGNumberTextFieldValidator *_priceValidator;
    Item *_scannedItem;
    BOOL _hasHandledInitialItemDetailItemScanId;
    BasketItemCollectionViewCell *_fakeBasketItemTableViewCell;
    BOOL _startScan;
}

#pragma mark - Private

static NSString *CellIdentifierBasketItem = @"CellBasketItem";

- (void) submitReturn
{
    ocg_async_background_overlay(^{});
}

#pragma mark - Properties

-(void)setType:(FlightDetailsItemViewControllerType)type
{
    _type = type;
    [_visibleDetails removeAllObjects];
    
    NSArray *layout = nil;
    
    switch (_type)
    {
        case FlightDetailsItemViewControllerTypeManualEntry:
            layout = @[
                       @(FlightDetailsItemDetailAirlineCode),
                       @(FlightDetailsItemDetailBoardingPassBarcode),
                       @(FlightDetailsItemDetailDestinationAirportCode),
                       @(FlightDetailsItemDetailFlightNumber),
                       @(FlightDetailsItemDetailNationality),
                       @(FlightDetailsItemDetailOriginAirportCode),
                       ];
            break;
            
        case FlightDetailsItemViewControllerTypeBoardingPassBarcode:
        case FlightDetailsItemViewControllerTypeNotFlying:
            layout = @[
                       @(FlightDetailsItemDetailNationality),
                       ];
            break;
            
        default:
            break;
    }
    
    
    
    [_visibleDetails addObjectsFromArray:layout];
    
    [self reloadData];
}

-(void)setInitialItemDetails:(NSDictionary *)initialItemDetails
{
    _initialItemDetails = initialItemDetails;
    [_itemDetails removeAllObjects];
    [_itemDetails addEntriesFromDictionary:_initialItemDetails];
    [self reloadData];
}

#pragma mark - init

- (id)init
{
    FNKCollectionViewLayout *layout = [[FNKCollectionViewLayout alloc] init];
    self = [super initWithCollectionViewLayout:layout];
    if (self) {
        _itemDetails = [NSMutableDictionary dictionary];
        _visibleDetails = [NSMutableArray array];
        _priceValidator = [[OCGNumberTextFieldValidator alloc] init];
        _priceValidator.maxValue = INT_MAX;
        _priceValidator.minValue = 0;
        _priceValidator.currencyCode = BasketController.sharedInstance.currencyCode;
        _startScan = YES;
    }
    return self;
}

#pragma mark - UIViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self setType:FlightDetailsItemViewControllerTypeManualEntry];
    
    _fakeBasketItemTableViewCell = [[BasketItemCollectionViewCell alloc] init];
    
    [OCGCollectionViewCell registerForDetailClass:[UITextField class]
                                          options:OCGCollectionViewCellDetailOptionUseTextLabelBaseline
                                   collectionView:self.collectionView];
    
    [self.collectionView registerClass:[BasketItemCollectionViewCell class]
            forCellWithReuseIdentifier:CellIdentifierBasketItem];
    
    self.collectionView.delaysContentTouches = NO;
    
    _firstResponderAccessoryView = [[OCGFirstResponderAccessoryView alloc] init];
    
    _firstResponderAccessoryView.delegate = self;
    
    _datePicker = [[UIDatePicker alloc] initWithFrame:CGRectMake(0, 0, 500, 200)];
    _datePicker.datePickerMode = UIDatePickerModeDate;
    
    _dateFormatter = [[NSDateFormatter alloc] init];
    _dateFormatter.dateFormat = @"yyyy-MM-dd";
    
    _displayDateFormatter = [[NSDateFormatter alloc] init];
    _displayDateFormatter.dateStyle = NSDateFormatterMediumStyle;
    _displayDateFormatter.timeStyle = NSDateFormatterNoStyle;
    
    [_datePicker addTarget:self action:@selector(dateChanged) forControlEvents:UIControlEventValueChanged];
    
    self.collectionView.backgroundColor = [UIColor colorWithRed:0.937 green:0.937 blue:0.957 alpha:1.000];
}

-(void)viewWillAppear:(BOOL)animated
{
    self.title = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁲󠁭󠁵󠁶󠁘󠁹󠀰󠁴󠁨󠀫󠁇󠁄󠁩󠁱󠀶󠁴󠁃󠁔󠀳󠁋󠁄󠁹󠁑󠁗󠁔󠁣󠀸󠁿*/ @"Return", nil);
    
    [super viewWillAppear:animated];
    [self reloadData];
    
    [BasketController.sharedInstance ensureBasketComplete:nil];
    [[CRSSkinning currentSkin] applyViewSkin:self];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    if (_startScan) {
        dispatch_async(dispatch_get_main_queue(), ^{
            NSIndexPath *indexPath = [NSIndexPath indexPathForItem:0 inSection:0];
            [self.collectionView selectItemAtIndexPath:indexPath
                                              animated:NO
                                        scrollPosition:UICollectionViewScrollPositionTop];
            [self collectionView:self.collectionView didSelectItemAtIndexPath:indexPath];
        });
    }
    _startScan = NO;
}

-(void)viewWillDisappear:(BOOL)animated
{
    BarcodeScannerController *controller = [BarcodeScannerController sharedInstance];
    // are we scanning?
    if (controller.scanning)
    {
        [controller stopScanning];
    }
    [super viewWillDisappear:animated];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView
     numberOfItemsInSection:(NSInteger)section
{
    return [_visibleDetails count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView
                  cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    FlightDetailsItemDetail itemDetail = [@[_visibleDetails] integerForIndexPath:indexPath];
    OCGCollectionViewCell *cell = nil;
    {
        NSString *cellIdentifier =
        [OCGCollectionViewCell reuseIdentifierForDetailClass:[UITextField class]
                                                     options:OCGCollectionViewCellDetailOptionUseTextLabelBaseline];
        cell = [collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier
                                                         forIndexPath:indexPath];
        [self updateTextFieldTableViewCell:(OCGCollectionViewCell *)cell
                         forRowAtIndexPath:indexPath];
    }
    
    switch (itemDetail)
    {
        case FlightDetailsItemDetailNationality:
        case FlightDetailsItemDetailDestinationAirportCode:
        case FlightDetailsItemDetailOriginAirportCode:
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;

            break;
            
        default:
            cell.accessoryType = UITableViewCellAccessoryNone;
            break;
    }
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.backgroundColor = [UIColor whiteColor];
    cell.layer.borderWidth = 1.;
    cell.layer.borderColor = [[UIColor colorWithRed:0.89 green:0.89 blue:0.90 alpha:1.000] CGColor];
    
    [[CRSSkinning currentSkin] applyViewSkin:cell
                             withTagOverride:cell.tag];
    
    return cell;
}

#pragma mark - FNKCollectionViewDataSourceDelegate

- (NSArray * /* FNKSupplementaryItem */)collectionView:(UICollectionView *)collectionView
                                                layout:(UICollectionViewLayout*)collectionViewLayout
               supplementaryItemsForHeadersAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.item == 0) {
        FNKSupplementaryItem *result = [[FNKSupplementaryItem alloc] init];
        result.title = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁩󠁆󠁭󠀰󠁵󠁐󠁙󠁬󠁹󠁘󠁥󠁩󠁭󠁧󠁧󠁗󠁹󠀹󠁋󠁁󠁸󠁪󠀫󠁇󠁐󠁡󠁧󠁿*/ @"Item details", nil);
        
        return @[result];
    } else {
        return nil;
    }
}

- (CGFloat)collectionView:(UICollectionView *)collectionView
 heightForItemAtIndexPath:(NSIndexPath *)indexPath
{
    FlightDetailsItemDetail itemDetail = [@[_visibleDetails] integerForIndexPath:indexPath];
    {
        return 44.f;
    }
}

#pragma mark - UICollectionViewDelegate

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    FlightDetailsItemDetail itemDetail = [@[_visibleDetails] integerForIndexPath:indexPath];
    
    switch (itemDetail)
    {
        case FlightDetailsItemDetailNationality:
        case FlightDetailsItemDetailDestinationAirportCode:
        case FlightDetailsItemDetailOriginAirportCode:
        {
            [self.view endEditing:YES];
            OCGSelectViewController *selectViewController = [[OCGSelectViewController alloc] init];
            selectViewController.title = [self titleforItemDetail:itemDetail];
            selectViewController.options = @[@[]];
            selectViewController.titleForOption = ^(OCGSelectViewController *selectViewController, NSIndexPath *indexPath) {
                return indexPath.description;
            };
            selectViewController.didSelectOption = ^(OCGSelectViewController *selectViewController, NSIndexPath *indexPath)
            {
                id selectedOption = [selectViewController.options objectForIndexPath:indexPath];
                _itemDetails[@(itemDetail)] = selectedOption;
                [self.navigationController popViewControllerAnimated:YES];
            };
            [self.navigationController pushViewController:selectViewController
                                                 animated:YES];
            break;
        }
        default:
        {
            OCGCollectionViewCell *cell = (OCGCollectionViewCell *)[self.collectionView cellForItemAtIndexPath:indexPath];
            if ([cell isKindOfClass:[OCGCollectionViewCell class]]) {
                [cell.detailTextField becomeFirstResponder];
            }
            break;
        }
    }
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    NSIndexPath *indexPath = [self.collectionView indexPathForCell:textField.OCGCollectionViewCell_collectionViewCell];
    FlightDetailsItemDetail itemDetail = [@[_visibleDetails] integerForIndexPath:indexPath];
    
    BOOL textFieldShouldBeginEditing = YES;
    switch (itemDetail) {
        case FlightDetailsItemDetailNationality:
        case FlightDetailsItemDetailDestinationAirportCode:
        case FlightDetailsItemDetailOriginAirportCode:
        case FlightDetailsItemDetailBoardingPassBarcode:
            textFieldShouldBeginEditing = NO;
            break;
        default:
            textFieldShouldBeginEditing = YES;
            break;
    }
    return textFieldShouldBeginEditing;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    NSIndexPath *indexPath = [self.collectionView indexPathForCell:textField.OCGCollectionViewCell_collectionViewCell];
    _firstResponderAccessoryView.currentPosition = indexPath;
    
    FlightDetailsItemDetail itemDetail = [@[_visibleDetails] integerForIndexPath:indexPath];
    
    OCGCollectionViewCell *cell = (OCGCollectionViewCell *)[self.collectionView cellForItemAtIndexPath:indexPath];
    
//    if (itemDetail == FlightDetailsItemDetailOriginalTransactionDate) {
//        _datePicker.date = [_dateFormatter dateFromString:_itemDetails[@(itemDetail)]] ?: [NSDate date];
//        _itemDetails[@(itemDetail)] = [_dateFormatter stringFromDate:_datePicker.date];
//        cell.detailTextField.text = [_displayDateFormatter stringFromDate:_datePicker.date];
//    } else if (itemDetail == FlightDetailsItemDetailPrice) {
//        cell.detailTextField.text = [_priceValidator stringFromNumber:_itemDetails[@(itemDetail)]];
//    }
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    _firstResponderAccessoryView.currentPosition = nil;
}

- (BOOL)textFieldShouldClear:(UITextField *)textField
{
    NSIndexPath *indexPath = [self.collectionView indexPathForCell:textField.OCGCollectionViewCell_collectionViewCell];
    
    FlightDetailsItemDetail itemDetail = [@[_visibleDetails] integerForIndexPath:indexPath];
    [_itemDetails removeObjectForKey:@(itemDetail)];
    
    [self updateNavigationItem];
    
    OCGCollectionViewCell *cell = (OCGCollectionViewCell *)[self.collectionView cellForItemAtIndexPath:indexPath];
    cell.detailTextField.text = nil;
    
    return NO;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if ([self canSubmitItem]) {
        [self submitButtonTapped];
    } else {
        [_firstResponderAccessoryView moveFirstResponderByOffset:1];
    }
    return NO;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSIndexPath *indexPath = [self.collectionView indexPathForCell:textField.OCGCollectionViewCell_collectionViewCell];
    NSString *text = [textField.text stringByReplacingCharactersInRange:range withString:string];
    
    FlightDetailsItemDetail itemDetail = [@[_visibleDetails] integerForIndexPath:indexPath];
    
    {
        _itemDetails[@(itemDetail)] = text;
        
        [self updateNavigationItem];
        
        return YES;
    }
}

#pragma mark - OCGFirstResponderAccessoryViewDelegate

- (UIResponder *)OCGFirstResponderAccessoryView:(OCGFirstResponderAccessoryView *)firstResponderAccessoryView
                  firstResponderViewForPosition:(id)position
{
    [self.collectionView scrollToItemAtIndexPath:position
                                atScrollPosition:UICollectionViewScrollPositionCenteredVertically
                                        animated:NO];
    OCGCollectionViewCell *cell = (OCGCollectionViewCell *)[self.collectionView cellForItemAtIndexPath:position];
    if ([cell respondsToSelector:@selector(detailTextField)]) {
        return cell.detailTextField;
    } else {
        return nil;
    }
}

- (id)OCGFirstResponderAccessoryView:(OCGFirstResponderAccessoryView*)firstResponderAccessoryView
                positionFromPosition:(id)position offset:(NSInteger)offset
{
    return [self.collectionView indexPathFromIndexPath:position offset:offset wrap:YES];
}

#pragma mark - Date picker

- (void)dateChanged
{
    NSIndexPath *indexPath = _firstResponderAccessoryView.currentPosition;
    
    NSString *itemDetail = [@[_visibleDetails] objectForIndexPath:indexPath];
    _itemDetails[itemDetail] = [_dateFormatter stringFromDate:_datePicker.date];
    
    OCGCollectionViewCell *cell = (OCGCollectionViewCell *)[self.collectionView cellForItemAtIndexPath:indexPath];
    cell.detailTextField.text = [_displayDateFormatter stringFromDate:_datePicker.date];
    
    [self updateNavigationItem];
}

#pragma mark - Methods

-(void)reloadData
{
    [self.collectionView reloadData];
    [self updateNavigationItem];
    self.collectionView.tag = kEditableTableSkinningTag;
    [[CRSSkinning currentSkin] applyViewSkin:self];
}

-(void)updateTextFieldTableViewCell:(OCGCollectionViewCell *)cell
                  forRowAtIndexPath:(NSIndexPath *)indexPath
{
    cell.detailTextField.delegate = self;
    cell.detailTextField.inputAccessoryView = _firstResponderAccessoryView;
    cell.textLabelWidth = 120;
    cell.detailTextField.textAlignment = NSTextAlignmentRight;
    cell.detailTextField.autocapitalizationType = UITextAutocapitalizationTypeNone;
    cell.detailTextField.autocorrectionType = UITextAutocorrectionTypeNo;
    cell.detailTextField.keyboardType = UIKeyboardTypeNumberPad;
    cell.tag = kEditableTableCellSkinningTag;
    cell.textLabel.tag = kEditableCellKeySkinningTag;
    cell.detailTextField.tag = kEditableCellValueSkinningTag;
    cell.detailTextField.clearButtonMode = UITextFieldViewModeAlways;
    cell.detailRequirement = OCGCollectionViewCellDetailRequirementRequired;
    
    FlightDetailsItemDetail itemDetail = [@[_visibleDetails] integerForIndexPath:indexPath];
    
    BOOL textFieldShouldAllowEditing = YES;
    switch (itemDetail) {
        case FlightDetailsItemDetailNationality:
        case FlightDetailsItemDetailDestinationAirportCode:
        case FlightDetailsItemDetailOriginAirportCode:
        case FlightDetailsItemDetailBoardingPassBarcode:
            textFieldShouldAllowEditing = NO;
            break;
        default:
            textFieldShouldAllowEditing = YES;
            break;
    }
    cell.detailTextField.userInteractionEnabled = textFieldShouldAllowEditing;
    
    cell.textLabel.text = [self titleforItemDetail:itemDetail];
    
//    switch (itemDetail) {
//        case FlightDetailsItemDetailReasonDescription:
//            break;
//            
//        case FlightDetailsItemDetailOriginalCustomerCard:
//            cell.detailTextField.placeholder = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠀱󠁢󠁱󠁒󠁷󠁁󠁅󠁄󠁈󠀳󠁌󠁑󠀯󠀱󠁃󠁭󠁎󠁩󠁓󠁋󠀹󠁣󠁬󠀷󠁏󠁪󠁙󠁿*/ @"Original Customer", nil);
//            cell.detailTextField.inputView = nil;
//            cell.detailRequirement = OCGCollectionViewCellDetailRequirementNone;
//            break;
//            
//        case FlightDetailsItemDetailOriginalTransactionStoreId:
//            cell.detailTextField.placeholder = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠀳󠁶󠀹󠁣󠁱󠁡󠀰󠀴󠁤󠁎󠀷󠁍󠁇󠁹󠁏󠁒󠁷󠁢󠁁󠀸󠁌󠁔󠁒󠁲󠁐󠀷󠁣󠁿*/ @"Original store number", nil);
//            cell.detailTextField.inputView = nil;
//            break;
//            
//        case FlightDetailsItemDetailOriginalTransactionTillNumber:
//            cell.detailTextField.placeholder = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁬󠁭󠁰󠀸󠁵󠁕󠁥󠁯󠁺󠁹󠁥󠁰󠁷󠁇󠁱󠁊󠁚󠀶󠁆󠁢󠁚󠀰󠁗󠁥󠁹󠁏󠁧󠁿*/ @"Original till or operator #", nil);
//            cell.detailTextField.inputView = nil;
//            break;
//            
//        case FlightDetailsItemDetailOriginalTransactionId:
//            cell.detailTextField.placeholder = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁁󠁯󠁍󠀴󠀯󠁭󠁙󠁓󠁅󠁳󠁷󠁷󠁔󠁴󠁦󠁆󠁶󠁨󠀵󠀳󠁘󠁯󠁭󠁏󠁅󠁙󠁳󠁿*/ @"Original transaction #", nil);
//            cell.detailTextField.inputView = nil;
//            break;
//            
//        case FlightDetailsItemDetailOriginalTransactionDate:
//            cell.detailTextField.placeholder = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁆󠀲󠁎󠁏󠁐󠁅󠀹󠀸󠁴󠀱󠁥󠁐󠀹󠁦󠁹󠁡󠁕󠁴󠁡󠁥󠁕󠁘󠁊󠁒󠀸󠁨󠁧󠁿*/ @"Original transaction date", nil);
//            cell.detailTextField.inputView = _datePicker;
//            break;
//            
//        case FlightDetailsItemDetailOriginalItemDescription:
//            cell.detailTextField.placeholder = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁑󠁙󠀶󠀱󠁰󠁷󠁖󠁌󠁁󠁏󠁹󠁦󠁷󠁲󠁣󠀳󠁋󠀰󠀴󠁲󠁅󠀲󠁑󠁷󠁫󠁖󠀴󠁿*/ @"Item Description", nil);
//            cell.detailTextField.inputView = nil;
//            cell.detailTextField.keyboardType = UIKeyboardTypeDefault;
//            break;
//            
//        case FlightDetailsItemDetailOriginalItemScanId:
//            cell.detailTextField.clearButtonMode = UITextFieldViewModeNever;
//            
//        case FlightDetailsItemDetailItemScanId:
//            cell.detailTextField.placeholder = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁐󠁋󠀸󠀱󠁵󠁷󠁐󠀶󠁢󠁗󠀴󠁨󠁥󠁔󠁔󠁏󠁦󠁸󠁂󠁘󠁯󠁤󠀸󠁙󠁚󠁵󠁉󠁿*/ @"Refund Item", nil);
//            cell.detailTextField.inputView = nil;
//            break;
//            
//        case FlightDetailsItemDetailOriginalReferenceId:
//            cell.detailTextField.placeholder = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁚󠁦󠁫󠁑󠀰󠁡󠁰󠁪󠁲󠁯󠁓󠁯󠁴󠁔󠁦󠁌󠁑󠁵󠀲󠁶󠁔󠁖󠁺󠁘󠁓󠀷󠁯󠁿*/ @"Original Reference", nil);
//            cell.detailTextField.inputView = nil;
//            cell.detailTextField.keyboardType = UIKeyboardTypeDefault;
//            break;
//            
//        case FlightDetailsItemDetailOriginalSalesPerson:
//            cell.detailTextField.placeholder = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁁󠀶󠀸󠀲󠁙󠀵󠁅󠁭󠀱󠁑󠀵󠀫󠁃󠁨󠁒󠀲󠁓󠁰󠁦󠁯󠁸󠁡󠁙󠁚󠁍󠁩󠀸󠁿*/ @"Associate ID", nil);
//            cell.detailTextField.inputView = nil;
//            cell.detailRequirement = OCGCollectionViewCellDetailRequirementNone;
//            break;
//            
//        case FlightDetailsItemDetailPrice:
//            cell.detailTextField.placeholder = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠀵󠁫󠁸󠁢󠁐󠁱󠀫󠁣󠁏󠁣󠁒󠁚󠁇󠁳󠁐󠁹󠁉󠀸󠀯󠁈󠁥󠁏󠁤󠁺󠁔󠁉󠀰󠁿*/ @"Price", nil);
//            cell.detailTextField.inputView = nil;
//            break;
//            
//        case FlightDetailsItemDetailDumpCodeDescription:
//            cell.detailTextField.placeholder = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁕󠀯󠁹󠁬󠁈󠀹󠁮󠁹󠁑󠁐󠁨󠁃󠀷󠁋󠁤󠁩󠀱󠁃󠁨󠁯󠁥󠁅󠁄󠁕󠁄󠁔󠁳󠁿*/ @"Dump code", nil);
//            cell.detailTextField.inputView = nil;
//            cell.detailTextField.clearButtonMode = UITextFieldViewModeNever;
//            break;
//            
//        default:
//            
//            break;
//    }
//    
//    if (itemDetail == FlightDetailsItemDetailOriginalTransactionDate && _itemDetails[@(itemDetail)] != nil) {
//        NSDate *date = [_dateFormatter dateFromString:_itemDetails[@(itemDetail)]];
//        cell.detailTextField.text = [_displayDateFormatter stringFromDate:date];
//    } else if (itemDetail == FlightDetailsItemDetailPrice) {
//        cell.detailTextField.text = [_priceValidator stringFromNumber:_itemDetails[@(itemDetail)]];
//    } else {
//        cell.detailTextField.text = _itemDetails[@(itemDetail)] ?: @"";
//    }
//    
//    if (itemDetail == FlightDetailsItemDetailReasonDescription ||
//        (itemDetail == FlightDetailsItemDetailDumpCodeDescription && [BasketController.sharedInstance.dumpCodes count] > 1)) {
//        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
//        cell.selectionStyle = UITableViewCellSelectionStyleBlue;
//    } else {
//        cell.accessoryType = UITableViewCellAccessoryNone;
//        cell.selectionStyle = UITableViewCellSelectionStyleNone;
//    }
    
    [[CRSSkinning currentSkin] applyViewSkin:cell
                             withTagOverride:cell.tag];
}

-(void)updateNavigationItem
{
    if ([self.navigationController.viewControllers[0] isEqual:self])
    {
        UIBarButtonItem *cancelButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel
                                                                                      target:self
                                                                                      action:@selector(dismissViewController)];
        
        self.navigationItem.leftBarButtonItem = cancelButton;
    }
    
    UIBarButtonItem *submitButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁈󠁳󠁲󠁲󠁌󠀴󠀱󠁍󠁘󠁗󠁐󠁔󠁘󠁏󠁕󠀱󠁚󠁣󠁮󠁏󠁴󠀲󠁖󠁹󠁣󠁄󠁉󠁿*/ @"Submit", nil)
                                                                     style:UIBarButtonItemStyleDone
                                                                    target:self
                                                                    action:@selector(submitButtonTapped)];
    
    submitButton.enabled = [self canSubmitItem];
    
    self.navigationItem.rightBarButtonItem = submitButton;
}

-(void)dismissViewController
{
    [self dismissViewControllerAnimated:YES completion:NULL];
}

-(void)submitButtonTapped
{
    [self submitReturn];
}

//-(BOOL)handleExceptionCode:(enum MShopperExceptionCode)exceptionCode
//{
//    NSArray *requiredDetails = nil;
//    
//    switch (exceptionCode) {
//        case MShopperItemDescriptionRequiredExceptionCode:
//        case MShopperSellItemRequiresOriginalItemCodeExceptionCode:
//            requiredDetails = @[
//                                @(FlightDetailsItemDetailOriginalItemScanId),
//                                @(FlightDetailsItemDetailOriginalItemDescription)];
//            
//            break;
//            
//        case MShopperItemSerialNumberRequiredExceptionCode:
//            requiredDetails = @[
//                                @(FlightDetailsItemDetailItemSerialNumber),
//                                ];
//            break;
//            
//        case MShopperItemNotFoundExceptionCode:
//            
//            if (_type == FlightDetailsItemViewControllerTypeLinkedReturn)
//            {
//                requiredDetails = @[
//                                    @(FlightDetailsItemDetailDumpCodeDescription),
//                                    ];
//                
//                ReturnableSaleItem *basketItem = _itemDetails[@(FlightDetailsItemDetailBastketItem)];
//                
//                _itemDetails[@(FlightDetailsItemDetailOriginalItemScanId)] = basketItem.itemID;
//                _itemDetails[@(FlightDetailsItemDetailOriginalItemDescription)] = basketItem.desc;
//                if ([BasketController.sharedInstance.dumpCodes count] == 1)
//                {
//                    DumpCode *dumpCode = [BasketController.sharedInstance.dumpCodes lastObject];
//                    _itemDetails[@(FlightDetailsItemDetailDumpCodeDescription)] = dumpCode.desc ?: dumpCode.itemSellingCode;
//                    _itemDetails[@(FlightDetailsItemDetailItemScanId)] = dumpCode.itemSellingCode;
//                }
//            }
//            break;
//            
//        default:
//            break;
//    }
//    
//    BOOL didAddItemDetail = NO;
//    
//    for (id itemDetail in requiredDetails)
//    {
//        if (![_visibleDetails containsObject:itemDetail])
//        {
//            [_visibleDetails addObject:itemDetail];
//            didAddItemDetail = YES;
//        }
//    }
//    return didAddItemDetail;
//}

-(BOOL)canSubmitItem
{
    BOOL canSubmitItem = YES;
    
    NSArray *optionalDetails = @[];
    
    for (id itemDetail in _visibleDetails)
    {
        if (![optionalDetails containsObject:itemDetail])
        {
            if ([_itemDetails[itemDetail] isKindOfClass:[NSNumber class]])
            {
                if ([_itemDetails[itemDetail] doubleValue] == 0)
                {
                    canSubmitItem = NO;
                    break;
                }
            }
            else if ([_itemDetails[itemDetail] length] == 0)
            {
                canSubmitItem = NO;
                break;
            }
        }
    }
    
    return canSubmitItem;
}

-(NSString*)titleforItemDetail:(FlightDetailsItemDetail)itemDetail
{
    NSString *title = nil;
    
    switch (itemDetail)
    {
        case FlightDetailsItemDetailAirlineCode:
            title = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁤󠁫󠁓󠁒󠁗󠁤󠁰󠁺󠁊󠀴󠁩󠀰󠁩󠀰󠁈󠁦󠁘󠀸󠁇󠁴󠁭󠁵󠁂󠀰󠁨󠁳󠁕󠁿*/ @"Airline code", nil);
            break;
        case FlightDetailsItemDetailBoardingPassBarcode:
            title = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁃󠁶󠁪󠁩󠁬󠁰󠀴󠁅󠁊󠁋󠁋󠁯󠁲󠁙󠁶󠁶󠁚󠁋󠁪󠁇󠁢󠁴󠁺󠀶󠁃󠀱󠁷󠁿*/ @"Boarding pass", nil);
            break;
        case FlightDetailsItemDetailDestinationAirportCode:
            title = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁗󠁵󠁉󠁒󠁤󠁯󠀶󠁊󠁎󠁔󠀳󠁚󠁪󠀲󠁬󠁕󠁄󠁴󠁮󠁆󠀰󠀹󠁏󠁦󠁯󠀹󠁯󠁿*/ @"Destination airport", nil);
            break;
        case FlightDetailsItemDetailFlightNumber:
            title = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁇󠁶󠁵󠁈󠁣󠁉󠁡󠁬󠁮󠁒󠀸󠁨󠁢󠁱󠁗󠁊󠁂󠁅󠁌󠁌󠁔󠁂󠁶󠁰󠁲󠁴󠀸󠁿*/ @"Flight number", nil);
            break;
        case FlightDetailsItemDetailManualEntry:
            title = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁆󠁯󠀰󠁶󠁃󠀷󠁷󠁃󠁚󠁩󠁸󠁊󠁊󠁭󠁪󠁴󠁐󠁡󠁢󠁅󠁋󠁷󠀯󠁑󠁢󠁇󠁷󠁿*/ @"Manual entry", nil);
            break;
        case FlightDetailsItemDetailNationality:
            title = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁍󠁚󠁱󠁲󠀵󠁳󠁊󠀵󠁈󠁪󠁑󠁋󠁊󠀲󠀶󠁉󠁶󠁰󠀳󠁁󠀲󠁖󠁔󠁃󠁗󠁤󠀸󠁿*/ @"Nationality", nil);
            break;
        case FlightDetailsItemDetailNotFlying:
            title = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁩󠀴󠁹󠁕󠀵󠁸󠀹󠁭󠁐󠁅󠁙󠁄󠁣󠀫󠁮󠀸󠁦󠁋󠁮󠁹󠁔󠁇󠀴󠁢󠁅󠁲󠁫󠁿*/ @"Not flying", nil);
            break;
        case FlightDetailsItemDetailOriginAirportCode:
            title = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁩󠁴󠁗󠀫󠁩󠁭󠁃󠀷󠁌󠁱󠁲󠁚󠀸󠁫󠀱󠁤󠁦󠁓󠁎󠀲󠀱󠁓󠁮󠁔󠁐󠁘󠁷󠁿*/ @"Origin airport", nil);
            break;
            
        default:
            title = [@(itemDetail) description];
            
            break;
    }
    return title;
}

//- (void) startScan:(FlightDetailsItemDetail)itemDetail
//{
//    BarcodeScannerController *controller = [BarcodeScannerController sharedInstance];
//    // are we scanning?
//    if (!controller.scanning) {
//        // no, start scanning
//        [controller beginScanningInViewController:self
//                                        forReason:[self titleforItemDetail:itemDetail]
//                                   withButtonSize:CGSizeZero
//                                     withAddTitle:nil
//                                         mustScan:NO
//                              alphaNumericAllowed:YES
//                                        withBlock:^(NSString *barcode, ScannerEntryMethod entryMethod, SMBarcodeType barcodeType, BOOL *continueScanning) {
//                                            // debug assertions
//                                            NSAssert(barcode != nil,
//                                                     @"The reference to the scanned barcode has not been initialized.");
//                                            NSAssert([[barcode stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length] > 0,
//                                                     @"The returned barcode does not contain any characters.");
//                                            // we only want to scan the one barcode
//                                            *continueScanning = NO;
//                                            // grab the barcode we scanned
//                                            [self handleBarcode:barcode itemDetail:itemDetail];
//                                        }
//                                       terminated:^{
//                                           //                                           if (_FlightDetailsBarcode != nil) {
//                                           //                                               // process the barcode
//                                           //                                               [self handleFlightDetailsBarcode:_FlightDetailsBarcode];
//                                           //                                               _itemDetails[@"itemScanId"] = nil;
//                                           //                                           } else {
//                                           //                                               [self cancelButtonTapped];
//                                           //                                           }
//                                       }
//                                            error:^(NSString *errorDesc) {
//                                                if (errorDesc) {
//                                                    // display an alert
//                                                    UIAlertView *alert =
//                                                    [[UIAlertView alloc] initWithTitle:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁯󠀫󠁋󠁆󠁢󠁚󠁱󠁩󠁈󠁕󠀲󠀸󠁫󠁲󠁧󠁦󠀲󠁴󠁘󠁣󠁈󠁎󠀶󠀲󠁯󠀶󠁳󠁿*/ @"Scanning Error", nil)
//                                                                               message:errorDesc
//                                                                              delegate:nil
//                                                                     cancelButtonTitle:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁄󠁴󠁍󠁉󠁣󠀫󠁺󠁔󠁗󠁦󠁹󠁡󠁯󠀷󠁨󠁔󠁩󠀸󠁯󠀫󠁨󠁘󠀹󠁱󠁌󠀶󠀸󠁿*/ @"OK", nil)
//                                                                     otherButtonTitles:nil];
//                                                    [alert show];
//                                                }
//                                            }];
//    }
//    else
//    {
//        [controller stopScanning];
//    }
//}
//
//-(void)handleBarcode:(NSString *)barcode itemDetail:(FlightDetailsItemDetail)itemDetail
//{
//    ocg_async_background(^{
//        
//        _itemDetails[@(itemDetail)] =
//        [barcode stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
//        
//        if (itemDetail == FlightDetailsItemDetailItemScanId)
//        {
//            NSError *error = nil;
//            ServerError *serverError = nil;
//            
//            NSString *locationKey = [CRSLocationController getLocationKey];
//            
//            _scannedItem =
//            [RESTController.sharedInstance findItemWithScanID:barcode
//                                                      storeID:locationKey
//                                                        error:&error
//                                                  serverError:&serverError];
//            
//            dispatch_async(dispatch_get_main_queue(), ^{
//                
//                if (error != nil || serverError != nil)
//                {
//                    [_itemDetails removeObjectForKey:@(itemDetail)];
//                    
//                    ServerErrorMessage *message = [serverError.messages lastObject];
//                    
//                    if ([message.code integerValue] == MShopperItemNotFoundExceptionCode && [BasketController.sharedInstance.dumpCodes count] > 0)
//                    {
//                        UIAlertView *alert =
//                        [[UIAlertView alloc] initWithTitle:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁚󠀳󠁖󠀶󠁙󠁶󠀱󠁂󠁎󠁍󠀸󠁎󠁢󠀴󠁋󠁳󠁚󠁮󠁏󠁦󠁆󠁥󠁶󠁖󠀲󠁉󠁉󠁿*/ @"Item not found", nil)
//                                                   message:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁺󠀫󠁒󠁷󠁙󠁐󠁅󠁵󠀴󠀰󠁦󠀫󠁙󠁗󠁺󠁆󠁋󠀱󠁢󠁇󠁨󠁰󠀰󠁁󠁭󠀴󠁑󠁿*/ @"Would you like to substitute a dump code?", nil)
//                                                  delegate:[UIAlertView class]
//                                         cancelButtonTitle:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁩󠁢󠁺󠁬󠁏󠁶󠁣󠁧󠁫󠁍󠁋󠁧󠁫󠀱󠁶󠁐󠁐󠀹󠁘󠀴󠁘󠁡󠁕󠁭󠁹󠁷󠀰󠁿*/ @"Cancel", nil)
//                                         otherButtonTitles:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁄󠁴󠁍󠁉󠁣󠀫󠁺󠁔󠁗󠁦󠁹󠁡󠁯󠀷󠁨󠁔󠁩󠀸󠁯󠀫󠁨󠁘󠀹󠁱󠁌󠀶󠀸󠁿*/ @"OK", nil), nil];
//                        
//                        alert.dismissBlock = ^(int buttonIndex) {
//                            if (buttonIndex == 0)
//                            {
//                                [_itemDetails removeObjectForKey:@(FlightDetailsItemDetailPrice)];
//                                _itemDetails[@(FlightDetailsItemDetailOriginalItemScanId)] = barcode;
//                                if ([BasketController.sharedInstance.dumpCodes count] == 1)
//                                {
//                                    DumpCode *dumpCode = [BasketController.sharedInstance.dumpCodes lastObject];
//                                    _itemDetails[@(FlightDetailsItemDetailDumpCodeDescription)] = dumpCode.desc ?: dumpCode.itemSellingCode;
//                                    _itemDetails[@(FlightDetailsItemDetailItemScanId)] = dumpCode.itemSellingCode;
//                                }
//                                [_visibleDetails replaceObjectAtIndex:[_visibleDetails indexOfObject:@(FlightDetailsItemDetailItemScanId)] withObject:@(FlightDetailsItemDetailDumpCodeDescription)];
//                                [self handleExceptionCode:MShopperSellItemRequiresOriginalItemCodeExceptionCode];
//                                [self reloadData];
//                            }
//                        };
//                        [alert show];
//                    }
//                    else
//                    {
//                        [[ServerErrorHandler sharedInstance] handleServerError:serverError
//                                                                transportError:error
//                                                                   withContext:nil
//                                                                  dismissBlock:nil
//                                                                   repeatBlock:nil];
//                    }
//                    
//                }
//                else
//                {
//                    if (_scannedItem.price != nil) {
//                        _priceValidator.currencyCode = BasketController.sharedInstance.currencyCode;
//                        _itemDetails[@(FlightDetailsItemDetailPrice)] = [_priceValidator numberFromString:_scannedItem.price];
//                    } else {
//                        _itemDetails[@(FlightDetailsItemDetailPrice)] = @"";
//                    }
//                }
//                
//                [self reloadData];
//            });
//        }
//        else
//        {
//            dispatch_async(dispatch_get_main_queue(), ^{
//                [self reloadData];
//            });
//        }
//    });
//}

@end
