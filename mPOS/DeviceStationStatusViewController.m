//
//  DeviceStationStatusViewController.m
//  mPOS
//
//  Created by Antonio Strijdom on 23/10/2013.
//  Copyright (c) 2013 Clarity. All rights reserved.
//

#import "DeviceStationStatusViewController.h"
#import "RESTController.h"
#import "CRSSkinning.h"
#import "UIImage+CRSUniversal.h"
#import "UIAlertView+MKBlockAdditions.h"
#import "DeviceStationViewController.h"
#import "BasketViewController.h"
#import "BasketController.h"
#import "OCGSwitchTableViewCell.h"
#import "POSFuncController.h"
#import "UIImage+OCGExtensions.h"
#import "OCGKeyboardTypeButtonItem.h"

@interface OCGRingedImageView : UIView;
@property (nonatomic, strong) UIColor *ringGlowColor;
@property (nonatomic, strong) UIImage *image;
@end

@implementation OCGRingedImageView;

@synthesize ringGlowColor = _ringGlowColor;
- (void) setRingGlowColor:(UIColor *)ringGlowColor
{
    _ringGlowColor = ringGlowColor;
    [self setNeedsDisplay];
}

- (void) drawRect:(CGRect)rect
{
    [super drawRect:rect];
    // draw the image centered
    CGFloat x = (self.frame.size.width / 2.0f) - (self.image.size.width / 2.0f);
    CGFloat y = (self.frame.size.height / 2.0f) - (self.image.size.height / 2.0f);
    [self.image drawAtPoint:CGPointMake(x, y)];
    // draw a ring around the image
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetStrokeColorWithColor(context, [[UIColor blackColor] CGColor]);
    CGContextSetLineWidth(context, 1.0f);
    CGContextBeginPath(context);
    // inset the ring a bit
    CGContextAddEllipseInRect(context, CGRectInset(self.bounds, 5.0f, 5.0f));
    CGContextClosePath(context);
    CGContextSetShadowWithColor(context,
                                CGSizeMake(0.0, 0.0),
                                2.0,
                                [self.ringGlowColor CGColor]);
    CGContextDrawPath(context, kCGPathStroke);
}

@end

@interface DeviceStationHeaderCell : UITableViewCell
@property (nonatomic, strong) UILabel *idDescLabel;
@property (nonatomic, strong) UILabel *idLabel;
@property (nonatomic, strong) UILabel *statusDescLabel;
@property (nonatomic, strong) UILabel *statusLabel;
@property (nonatomic, strong) UILabel *claimedUserDescLabel;
@property (nonatomic, strong) UILabel *claimedUserLabel;
@property (nonatomic, strong) UILabel *claimedTerminalDescLabel;
@property (nonatomic, strong) UILabel *claimedTerminalLabel;
@property (nonatomic, strong) OCGRingedImageView *printerImageView;
@end

@implementation DeviceStationHeaderCell

- (id) initWithStyle:(UITableViewCellStyle)style
     reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style
                reuseIdentifier:reuseIdentifier];
    if (self != nil) {
        self.accessoryType = UITableViewCellAccessoryNone;
        self.selectionStyle = UITableViewCellEditingStyleNone;
        // icon
        UIImage *printerImage = [UIImage OCGExtensions_imageNamed:@"REPRINT_RECEIPT"];
        printerImage = [printerImage imageTintedWithColor:[UIColor blackColor]];
        self.printerImageView = [[OCGRingedImageView alloc] init];
        self.printerImageView.image = printerImage;
        self.printerImageView.ringGlowColor = [UIColor clearColor];
        self.printerImageView.backgroundColor = [UIColor clearColor];
        self.printerImageView.translatesAutoresizingMaskIntoConstraints = NO;
        [self.contentView addSubview:self.printerImageView];
        [self.contentView addConstraint:
         [NSLayoutConstraint constraintWithItem:self.printerImageView
                                      attribute:NSLayoutAttributeTop
                                      relatedBy:NSLayoutRelationEqual
                                         toItem:self.contentView
                                      attribute:NSLayoutAttributeTop
                                     multiplier:1.0f
                                       constant:0.0f]];
        [self.contentView addConstraint:
         [NSLayoutConstraint constraintWithItem:self.printerImageView
                                      attribute:NSLayoutAttributeLeft
                                      relatedBy:NSLayoutRelationEqual
                                         toItem:self.contentView
                                      attribute:NSLayoutAttributeLeft
                                     multiplier:1.0f
                                       constant:0.0f]];
        [self.contentView addConstraint:
         [NSLayoutConstraint constraintWithItem:self.printerImageView
                                      attribute:NSLayoutAttributeHeight
                                      relatedBy:NSLayoutRelationEqual
                                         toItem:nil
                                      attribute:NSLayoutAttributeNotAnAttribute
                                     multiplier:1.0
                                       constant:50.0f]];
        [self.contentView addConstraint:
         [NSLayoutConstraint constraintWithItem:self.printerImageView
                                      attribute:NSLayoutAttributeWidth
                                      relatedBy:NSLayoutRelationEqual
                                         toItem:nil
                                      attribute:NSLayoutAttributeNotAnAttribute
                                     multiplier:1.0
                                       constant:50.0f]];
        // device station id
        self.idDescLabel = [[UILabel alloc] init];
        self.idDescLabel.translatesAutoresizingMaskIntoConstraints = NO;
        self.idDescLabel.backgroundColor = [UIColor clearColor];
        self.idDescLabel.textAlignment = NSTextAlignmentLeft;
        [self.contentView addSubview:self.idDescLabel];
        [self.contentView addConstraint:
         [NSLayoutConstraint constraintWithItem:self.idDescLabel
                                      attribute:NSLayoutAttributeTop
                                      relatedBy:NSLayoutRelationEqual
                                         toItem:self.contentView
                                      attribute:NSLayoutAttributeTop
                                     multiplier:1.0f
                                       constant:5.0f]];
        [self.contentView addConstraint:
         [NSLayoutConstraint constraintWithItem:self.idDescLabel
                                      attribute:NSLayoutAttributeLeft
                                      relatedBy:NSLayoutRelationEqual
                                         toItem:self.printerImageView
                                      attribute:NSLayoutAttributeRight
                                     multiplier:1.0f
                                       constant:0.0f]];
        self.idLabel = [[UILabel alloc] init];
        self.idLabel.translatesAutoresizingMaskIntoConstraints = NO;
        self.idLabel.adjustsFontSizeToFitWidth = YES;
        self.idLabel.minimumScaleFactor = 0.5;
        self.idLabel.backgroundColor = [UIColor clearColor];
        self.idLabel.textAlignment = NSTextAlignmentRight;
        [self.contentView addSubview:self.idLabel];
        [self.contentView addConstraint:
         [NSLayoutConstraint constraintWithItem:self.idLabel
                                      attribute:NSLayoutAttributeTop
                                      relatedBy:NSLayoutRelationEqual
                                         toItem:self.idDescLabel
                                      attribute:NSLayoutAttributeTop
                                     multiplier:1.0f
                                       constant:0.0f]];
        [self.contentView addConstraint:
         [NSLayoutConstraint constraintWithItem:self.idLabel
                                      attribute:NSLayoutAttributeLeft
                                      relatedBy:NSLayoutRelationEqual
                                         toItem:self.idDescLabel
                                      attribute:NSLayoutAttributeRight
                                     multiplier:1.0f
                                       constant:0.0f]];
        [self.contentView addConstraint:
         [NSLayoutConstraint constraintWithItem:self.idLabel
                                      attribute:NSLayoutAttributeRight
                                      relatedBy:NSLayoutRelationEqual
                                         toItem:self.contentView
                                      attribute:NSLayoutAttributeRight
                                     multiplier:1.0f
                                       constant:-5.0f]];
        // device status
        self.statusDescLabel = [[UILabel alloc] init];
        self.statusDescLabel.translatesAutoresizingMaskIntoConstraints = NO;
        self.statusDescLabel.backgroundColor = [UIColor clearColor];
        self.statusDescLabel.textAlignment = NSTextAlignmentLeft;
        [self.contentView addSubview:self.statusDescLabel];
        [self.contentView addConstraint:
         [NSLayoutConstraint constraintWithItem:self.statusDescLabel
                                      attribute:NSLayoutAttributeTop
                                      relatedBy:NSLayoutRelationEqual
                                         toItem:self.idDescLabel
                                      attribute:NSLayoutAttributeBottom
                                     multiplier:1.0f
                                       constant:5.0f]];
        [self.contentView addConstraint:
         [NSLayoutConstraint constraintWithItem:self.statusDescLabel
                                      attribute:NSLayoutAttributeLeft
                                      relatedBy:NSLayoutRelationEqual
                                         toItem:self.printerImageView
                                      attribute:NSLayoutAttributeRight
                                     multiplier:1.0f
                                       constant:0.0f]];
        self.statusLabel = [[UILabel alloc] init];
        self.statusLabel.translatesAutoresizingMaskIntoConstraints = NO;
        self.statusLabel.adjustsFontSizeToFitWidth = YES;
        self.statusLabel.minimumScaleFactor = 0.5;
        self.statusLabel.backgroundColor = [UIColor clearColor];
        self.statusLabel.textAlignment = NSTextAlignmentRight;
        [self.contentView addSubview:self.statusLabel];
        [self.contentView addConstraint:
         [NSLayoutConstraint constraintWithItem:self.statusLabel
                                      attribute:NSLayoutAttributeTop
                                      relatedBy:NSLayoutRelationEqual
                                         toItem:self.statusDescLabel
                                      attribute:NSLayoutAttributeTop
                                     multiplier:1.0f
                                       constant:0.0f]];
        [self.contentView addConstraint:
         [NSLayoutConstraint constraintWithItem:self.statusLabel
                                      attribute:NSLayoutAttributeLeft
                                      relatedBy:NSLayoutRelationEqual
                                         toItem:self.statusDescLabel
                                      attribute:NSLayoutAttributeRight
                                     multiplier:1.0f
                                       constant:0.0f]];
        [self.contentView addConstraint:
         [NSLayoutConstraint constraintWithItem:self.statusLabel
                                      attribute:NSLayoutAttributeRight
                                      relatedBy:NSLayoutRelationEqual
                                         toItem:self.contentView
                                      attribute:NSLayoutAttributeRight
                                     multiplier:1.0f
                                       constant:-5.0f]];
        // claimed user
        self.claimedUserDescLabel = [[UILabel alloc] init];
        self.claimedUserDescLabel.translatesAutoresizingMaskIntoConstraints = NO;
        self.claimedUserDescLabel.backgroundColor = [UIColor clearColor];
        self.claimedUserDescLabel.textAlignment = NSTextAlignmentLeft;
        [self.contentView addSubview:self.claimedUserDescLabel];
        [self.contentView addConstraint:
         [NSLayoutConstraint constraintWithItem:self.claimedUserDescLabel
                                      attribute:NSLayoutAttributeTop
                                      relatedBy:NSLayoutRelationEqual
                                         toItem:self.statusDescLabel
                                      attribute:NSLayoutAttributeBottom
                                     multiplier:1.0f
                                       constant:5.0f]];
        [self.contentView addConstraint:
         [NSLayoutConstraint constraintWithItem:self.claimedUserDescLabel
                                      attribute:NSLayoutAttributeLeft
                                      relatedBy:NSLayoutRelationEqual
                                         toItem:self.printerImageView
                                      attribute:NSLayoutAttributeRight
                                     multiplier:1.0f
                                       constant:0.0f]];
        self.claimedUserLabel = [[UILabel alloc] init];
        self.claimedUserLabel.translatesAutoresizingMaskIntoConstraints = NO;
        self.claimedUserLabel.adjustsFontSizeToFitWidth = YES;
        self.claimedUserLabel.minimumScaleFactor = 0.5;
        self.claimedUserLabel.backgroundColor = [UIColor clearColor];
        self.claimedUserLabel.textAlignment = NSTextAlignmentRight;
        [self.contentView addSubview:self.claimedUserLabel];
        [self.contentView addConstraint:
         [NSLayoutConstraint constraintWithItem:self.claimedUserLabel
                                      attribute:NSLayoutAttributeTop
                                      relatedBy:NSLayoutRelationEqual
                                         toItem:self.claimedUserDescLabel
                                      attribute:NSLayoutAttributeTop
                                     multiplier:1.0f
                                       constant:0.0f]];
        [self.contentView addConstraint:
         [NSLayoutConstraint constraintWithItem:self.claimedUserLabel
                                      attribute:NSLayoutAttributeLeft
                                      relatedBy:NSLayoutRelationEqual
                                         toItem:self.printerImageView
                                      attribute:NSLayoutAttributeLeft
                                     multiplier:1.0f
                                       constant:0.0f]];
        [self.contentView addConstraint:
         [NSLayoutConstraint constraintWithItem:self.claimedUserLabel
                                      attribute:NSLayoutAttributeRight
                                      relatedBy:NSLayoutRelationEqual
                                         toItem:self.contentView
                                      attribute:NSLayoutAttributeRight
                                     multiplier:1.0f
                                       constant:-5.0f]];
        // claimed terminal
        self.claimedTerminalDescLabel = [[UILabel alloc] init];
        self.claimedTerminalDescLabel.translatesAutoresizingMaskIntoConstraints = NO;
        self.claimedTerminalDescLabel.backgroundColor = [UIColor clearColor];
        self.claimedTerminalDescLabel.textAlignment = NSTextAlignmentLeft;
        [self.contentView addSubview:self.claimedTerminalDescLabel];
        [self.contentView addConstraint:
         [NSLayoutConstraint constraintWithItem:self.claimedTerminalDescLabel
                                      attribute:NSLayoutAttributeTop
                                      relatedBy:NSLayoutRelationEqual
                                         toItem:self.claimedUserDescLabel
                                      attribute:NSLayoutAttributeBottom
                                     multiplier:1.0f
                                       constant:5.0f]];
        [self.contentView addConstraint:
         [NSLayoutConstraint constraintWithItem:self.claimedTerminalDescLabel
                                      attribute:NSLayoutAttributeLeft
                                      relatedBy:NSLayoutRelationEqual
                                         toItem:self.printerImageView
                                      attribute:NSLayoutAttributeRight
                                     multiplier:1.0f
                                       constant:0.0f]];
        self.claimedTerminalLabel = [[UILabel alloc] init];
        self.claimedTerminalLabel.translatesAutoresizingMaskIntoConstraints = NO;
        self.claimedTerminalLabel.adjustsFontSizeToFitWidth = YES;
        self.claimedTerminalLabel.minimumScaleFactor = 0.5;
        self.claimedTerminalLabel.backgroundColor = [UIColor clearColor];
        self.claimedTerminalLabel.textAlignment = NSTextAlignmentRight;
        [self.contentView addSubview:self.claimedTerminalLabel];
        [self.contentView addConstraint:
         [NSLayoutConstraint constraintWithItem:self.claimedTerminalLabel
                                      attribute:NSLayoutAttributeTop
                                      relatedBy:NSLayoutRelationEqual
                                         toItem:self.claimedTerminalDescLabel
                                      attribute:NSLayoutAttributeTop
                                     multiplier:1.0f
                                       constant:0.0f]];
        [self.contentView addConstraint:
         [NSLayoutConstraint constraintWithItem:self.claimedTerminalLabel
                                      attribute:NSLayoutAttributeLeft
                                      relatedBy:NSLayoutRelationEqual
                                         toItem:self.claimedTerminalDescLabel
                                      attribute:NSLayoutAttributeRight
                                     multiplier:1.0f
                                       constant:0.0f]];
        [self.contentView addConstraint:
         [NSLayoutConstraint constraintWithItem:self.claimedTerminalLabel
                                      attribute:NSLayoutAttributeRight
                                      relatedBy:NSLayoutRelationEqual
                                         toItem:self.contentView
                                      attribute:NSLayoutAttributeRight
                                     multiplier:1.0f
                                       constant:-5.0f]];
    }
    
    return self;
}

- (void) prepareForReuse
{
    [super prepareForReuse];
    self.idLabel.text = nil;
    self.statusLabel.text = nil;
}

@end

@interface DeviceStationStatusViewController () <UIAlertViewDelegate, OCGSwitchTableViewCellDelegate, UITextFieldDelegate> {
    UIAlertView *_alert;
}
@property (nonatomic, strong) DeviceStationAssociationCompleteBlockType associateComplete;
@property (nonatomic, strong) NSString *associateDeviceStationId;
@property (nonatomic, readonly) BOOL canUnlink;
@property (nonatomic, readonly) BOOL canAssociate;
- (NSString *) statusDescription;
- (UIColor *) colorForStatus;
- (CGFloat) rowHeightForStatus;
- (void) refreshStatus:(id)sender;
- (void) unlinkStation;
- (void) disableStation;
- (void) disassociateStation;
@end

@implementation DeviceStationStatusViewController

#pragma mark - Private

const NSInteger kRowIndexHeader = 0;
const NSInteger kRowIndexUnlink = 1;
const NSInteger kRowIndexStatus = 2;
const NSInteger kRowIndexDrawerInsert = 3;

- (NSString *) statusDescription
{
    NSString *result = nil;
    
    // check if the station is claimed or not
    if (self.station.claimed) {
        // station is claimed
        result = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁔󠁍󠁸󠀯󠁈󠁗󠁨󠁅󠀹󠀷󠀫󠁆󠁲󠁲󠁁󠁴󠁵󠁪󠁁󠁂󠁴󠀶󠀴󠁙󠁰󠁂󠁍󠁿*/ @"Claimed", nil);
    } else {
        // not claimed, check if available
        if (self.station.available) {
            // check for any device errors
            BOOL error = NO;
            for (DeviceStationPeripheral *peripheral in self.station.peripherals) {
                // do something about the status
            }
            if (error) {
                result = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁮󠁳󠁒󠀵󠁆󠁹󠁖󠁲󠀸󠀲󠁈󠁍󠁔󠁚󠁸󠁪󠁣󠁑󠁩󠁡󠁌󠁚󠁍󠁵󠀫󠁤󠁕󠁿*/ @"Device Error", nil);
            } else {
                result = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁋󠁶󠁇󠁪󠁖󠀲󠁬󠀯󠁐󠁩󠁋󠁋󠁄󠁓󠁏󠁶󠁈󠁯󠁥󠁵󠁚󠀴󠁔󠁈󠁉󠁍󠀸󠁿*/ @"Ready", nil);
            }
        } else {
            // not available
            result = @"Unavailable";
        }
    }
    
    return result;
}

- (UIColor *) colorForStatus
{
    UIColor *result = [UIColor clearColor];
    
    // check if the station is claimed or not
    if (self.station.claimed) {
        // station is claimed
        result = [UIColor redColor];
    } else {
        // not claimed, check if available
        if (self.station.available) {
            // check for any device errors
            BOOL error = NO;
            for (DeviceStationPeripheral *peripheral in self.station.peripherals) {
                // do something about the status
            }
            if (error) {
                result = [UIColor yellowColor];
            } else {
                result = [UIColor greenColor];
            }
        } else {
            // not available
            result = [UIColor lightGrayColor];
        }
    }
    
    return result;
}

- (CGFloat) rowHeightForStatus
{
    CGFloat result = 55.0f;
    
    if (self.station.claimed) {
        result = 95.0f;
    }
    
    return result;
}

- (void) refreshStatus:(id)sender
{
    ocg_async_background_overlay(^{
        [RESTController.sharedInstance  getDeviceStationWithDeviceStationId:self.station.deviceStationId
                                                                   complete:^(DeviceStation *newStation, NSError *restError, ServerError *restServerError)
         {
             if ((restError == nil) && (restServerError == nil)) {
                 dispatch_async(dispatch_get_main_queue(), ^{
                     self.station = newStation;
                     [self.tableView reloadData];
                 });
             } else {
                 // an error occurred
                 // get the error
                 ServerErrorMessage *errorMessage = nil;
                 for (ServerErrorMessage *message in restServerError.messages) {
                     errorMessage = message;
                     break;
                 }
                 dispatch_async(dispatch_get_main_queue(), ^{
                     // display the error
                     DeviceStationViewController *vc = [[DeviceStationViewController alloc] init];
                     [vc presentAlertForDeviceStationError:errorMessage
                                       withDeviceStationId:nil
                                      andDeviceStationName:nil
                                                 andTillId:nil
                                               andTerminal:nil
                                                   andUser:nil
                                 andUnavailablePeripherals:nil
                                                 dismissed:nil];
                 });
             }
         }];
    });
}

- (void) unlinkStation
{
    UIAlertView *alert =
    [UIAlertView alertViewWithTitle:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁮󠁊󠁯󠁔󠁅󠁑󠁡󠁒󠀯󠀳󠁑󠁏󠁤󠁅󠁗󠀸󠀯󠁯󠁰󠁹󠁪󠀴󠁴󠁬󠁡󠁥󠁙󠁿*/ @"Unlink Station", nil)
                            message:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁲󠁫󠀫󠁢󠁹󠁑󠁺󠁊󠁸󠁘󠁉󠀶󠁌󠁔󠁒󠀷󠁶󠁳󠁆󠁑󠁳󠁸󠁱󠁸󠀳󠁦󠁉󠁿*/ @"Are you sure you wish to unlink this station?", nil)
                  cancelButtonTitle:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁢󠁆󠁳󠁚󠁙󠀯󠁩󠁴󠁴󠀱󠁓󠁫󠁳󠁳󠁤󠀯󠁬󠁑󠁂󠁯󠀰󠁬󠀸󠀱󠁙󠁗󠁣󠁿*/ @"No", nil)
                  otherButtonTitles:@[NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠀶󠀸󠀹󠁢󠁺󠁔󠁍󠁫󠁄󠁴󠁏󠀶󠀯󠁃󠀴󠁆󠁓󠁲󠁰󠁂󠁮󠁰󠁁󠁥󠀲󠁡󠁍󠁿*/ @"Yes", nil)]
                          onDismiss:^(int buttonIndex) {
                              ocg_async_background_overlay(^{
                                  [RESTController.sharedInstance unlinkDeviceWithDeviceStationId:self.station.deviceStationId
                                                                                        complete:^(DeviceStation *newStation, NSError *restError, ServerError *restServerError)
                                   {
                                       if ((restError == nil) && (restServerError == nil)) {
                                           dispatch_async(dispatch_get_main_queue(), ^{
                                               // refresh the UI
                                               self.station = newStation;
                                               [self.tableView reloadData];
                                           });
                                       } else {
                                           // an error occurred
                                           // get the error
                                           ServerErrorMessage *errorMessage = nil;
                                           for (ServerErrorMessage *message in restServerError.messages) {
                                               errorMessage = message;
                                               break;
                                           }
                                           dispatch_async(dispatch_get_main_queue(), ^{
                                               // display the error
                                               DeviceStationViewController *vc = [[DeviceStationViewController alloc] init];
                                               [vc presentAlertForDeviceStationError:errorMessage
                                                                 withDeviceStationId:nil
                                                                andDeviceStationName:nil
                                                                           andTillId:nil
                                                                         andTerminal:nil
                                                                             andUser:nil
                                                           andUnavailablePeripherals:nil
                                                                           dismissed:nil];
                                           });
                                       }
                                   }];
                                  
                              });
                          }
                           onCancel:^{
                               
                           }];
    [alert show];
}

- (void) disableStation
{
    NSString *message = [NSString stringWithFormat:@"%@ %@",
                         NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁵󠁨󠁂󠁹󠁹󠀳󠁲󠁷󠁪󠁙󠁶󠁑󠁋󠁩󠁸󠁁󠁱󠀶󠁲󠁫󠁘󠁫󠁵󠀶󠁂󠀶󠁯󠁿*/ @"Are you sure you wish to make this station", nil),
                         (self.station.available ? NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁳󠀱󠁶󠁏󠁩󠁵󠀲󠁖󠁃󠁳󠁮󠁧󠁲󠁷󠁍󠁈󠁤󠁍󠁱󠀲󠁕󠁩󠁊󠁃󠁑󠁲󠀰󠁿*/ @"unavailable", nil) : NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠀸󠁣󠀸󠁃󠁄󠁮󠁬󠀰󠁫󠁡󠁫󠁣󠁹󠁉󠁍󠁑󠁦󠀲󠁁󠁡󠁓󠁆󠁧󠁤󠁸󠁯󠁳󠁿*/ @"available", nil))];
    UIAlertView *alert =
    [UIAlertView alertViewWithTitle:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁄󠁤󠁨󠁩󠁑󠀯󠁒󠁓󠁅󠁓󠁁󠁚󠁨󠀫󠁉󠁷󠁱󠁎󠁡󠁶󠀵󠀫󠁋󠁸󠁄󠁱󠀰󠁿*/ @"Update Station Status", nil)
                            message:message
                  cancelButtonTitle:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁢󠁆󠁳󠁚󠁙󠀯󠁩󠁴󠁴󠀱󠁓󠁫󠁳󠁳󠁤󠀯󠁬󠁑󠁂󠁯󠀰󠁬󠀸󠀱󠁙󠁗󠁣󠁿*/ @"No", nil)
                  otherButtonTitles:@[NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠀶󠀸󠀹󠁢󠁺󠁔󠁍󠁫󠁄󠁴󠁏󠀶󠀯󠁃󠀴󠁆󠁓󠁲󠁰󠁂󠁮󠁰󠁁󠁥󠀲󠁡󠁍󠁿*/ @"Yes", nil)]
                          onDismiss:^(int buttonIndex) {
                              ocg_async_background_overlay(^{
                                  // toggle station availability
                                  self.station.available = !self.station.available;
                                  [RESTController.sharedInstance updateDeviceStationWithDeviceStation:self.station
                                                                                      deviceStationId:self.station.deviceStationId
                                                                                             complete:^(DeviceStation *newStation, NSError *restError, ServerError *restServerError)
                                   {
                                       if ((restError == nil) && (restServerError == nil)) {
                                           dispatch_async(dispatch_get_main_queue(), ^{
                                               // refresh the UI
                                               self.station = newStation;
                                               [self.tableView reloadData];
                                           });
                                       } else {
                                           // an error occurred
                                           // get the error
                                           ServerErrorMessage *errorMessage = nil;
                                           for (ServerErrorMessage *message in restServerError.messages) {
                                               errorMessage = message;
                                               break;
                                           }
                                           NSString *errorDeviceStationId = nil;
                                           NSString *errorDeviceStationName = nil;
                                           NSString *errorTerminal = nil;
                                           NSString *errorUser = nil;
                                           NSString *errorUnavailablePeripherals = nil;
                                           if ([errorMessage isKindOfClass:[ServerErrorDeviceStationMessage class]]) {
                                               errorDeviceStationId = [(ServerErrorDeviceStationMessage *)errorMessage deviceStationId];
                                               errorDeviceStationName = [(ServerErrorDeviceStationMessage *)errorMessage deviceStationName];
                                               errorTerminal = [(ServerErrorDeviceStationMessage *)errorMessage terminal];
                                               errorUser = [(ServerErrorDeviceStationMessage *)errorMessage user];
                                               errorUnavailablePeripherals = [(ServerErrorDeviceStationMessage *)errorMessage peripherals];
                                           }
                                           dispatch_async(dispatch_get_main_queue(), ^{
                                               // display the error
                                               DeviceStationViewController *vc = [[DeviceStationViewController alloc] init];
                                               [vc presentAlertForDeviceStationError:errorMessage
                                                                 withDeviceStationId:errorDeviceStationId
                                                                andDeviceStationName:errorDeviceStationName
                                                                           andTillId:nil
                                                                         andTerminal:errorTerminal
                                                                             andUser:errorUser
                                                           andUnavailablePeripherals:errorUnavailablePeripherals
                                                                           dismissed:nil];
                                           });
                                       }
                                   }];
                                  
                              });
                          }
                           onCancel:^{
                               
                           }];
    [alert show];
}

- (void) disassociateStation
{
    NSString *message = [NSString stringWithFormat:@"%@ %@ %@",
                         NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁒󠀲󠁪󠁩󠀶󠁙󠁚󠀳󠁪󠁸󠁨󠀫󠁡󠁕󠁢󠁆󠁫󠁙󠁎󠁦󠁓󠁉󠁂󠁧󠀳󠁺󠁕󠁿*/ @"Are you sure you wish to disassociate drawer", nil),
                         self.station.tillId,
                         NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁈󠁖󠁶󠁑󠁍󠁥󠁉󠁊󠁰󠀸󠁥󠁎󠁁󠁺󠁈󠁎󠀴󠁱󠁔󠁏󠁋󠁔󠁃󠁲󠁹󠁹󠀴󠁿*/ @"from this station?", nil)];
    UIAlertView *alert =
    [UIAlertView alertViewWithTitle:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁡󠀶󠁏󠁨󠁡󠁯󠁴󠁄󠁓󠁴󠀹󠁈󠀷󠀰󠁩󠁳󠁒󠁮󠀯󠁓󠁋󠁣󠁔󠀯󠁉󠁋󠁉󠁿*/ @"Disassociate", nil)
                            message:message
                  cancelButtonTitle:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁢󠁆󠁳󠁚󠁙󠀯󠁩󠁴󠁴󠀱󠁓󠁫󠁳󠁳󠁤󠀯󠁬󠁑󠁂󠁯󠀰󠁬󠀸󠀱󠁙󠁗󠁣󠁿*/ @"No", nil)
                  otherButtonTitles:@[NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠀶󠀸󠀹󠁢󠁺󠁔󠁍󠁫󠁄󠁴󠁏󠀶󠀯󠁃󠀴󠁆󠁓󠁲󠁰󠁂󠁮󠁰󠁁󠁥󠀲󠁡󠁍󠁿*/ @"Yes", nil)]
                          onDismiss:^(int buttonIndex) {
                              ocg_async_background_overlay(^{
                                  [RESTController.sharedInstance disassociateDeviceStationWithDeviceStationId:self.station.deviceStationId
                                                                                                       tillId:self.station.tillId
                                                                                                     complete:^(DeviceStation *newStation, NSError *restError, ServerError *restServerError)
                                   {
                                       if ((restError == nil) && (restServerError == nil)) {
                                           dispatch_async(dispatch_get_main_queue(), ^{
                                               self.station = newStation;
                                               [self.tableView reloadData];
                                           });
                                       } else {
                                           // an error occurred
                                           // get the error
                                           ServerErrorMessage *errorMessage = nil;
                                           for (ServerErrorMessage *message in restServerError.messages) {
                                               errorMessage = message;
                                               break;
                                           }
                                           NSString *errorDeviceStationId = nil;
                                           NSString *errorDeviceStationName = nil;
                                           NSString *errorTerminal = nil;
                                           NSString *errorUser = nil;
                                           NSString *errorUnavailablePeripherals = nil;
                                           if ([errorMessage isKindOfClass:[ServerErrorDeviceStationMessage class]]) {
                                               errorDeviceStationId = [(ServerErrorDeviceStationMessage *)errorMessage deviceStationId];
                                               errorDeviceStationName = [(ServerErrorDeviceStationMessage *)errorMessage deviceStationName];
                                               errorTerminal = [(ServerErrorDeviceStationMessage *)errorMessage terminal];
                                               errorUser = [(ServerErrorDeviceStationMessage *)errorMessage user];
                                               errorUnavailablePeripherals = [(ServerErrorDeviceStationMessage *)errorMessage peripherals];
                                           }
                                           dispatch_async(dispatch_get_main_queue(), ^{
                                               // display the error
                                               DeviceStationViewController *vc = [[DeviceStationViewController alloc] init];
                                               [vc presentAlertForDeviceStationError:errorMessage
                                                                 withDeviceStationId:errorDeviceStationId
                                                                andDeviceStationName:errorDeviceStationName
                                                                           andTillId:nil
                                                                         andTerminal:errorTerminal
                                                                             andUser:errorUser
                                                           andUnavailablePeripherals:errorUnavailablePeripherals
                                                                           dismissed:nil];
                                           });
                                       }
                                   }];
                                  
                              });
                          }
                           onCancel:^{
                               
                           }];
    [alert show];
}

#pragma mark - Properties

- (BOOL) canUnlink
{
    return (self.station.terminal != nil) && (![self.station.terminal isEqualToString:@""]);
}

- (BOOL) canAssociate
{
    BOOL result = NO;
    
    // if we are associating
    if ((self.station.tillId != nil) && (![self.station.tillId isEqualToString:@""])) {
        // only allow disassociate if we aren't in a sale
        result = [[BasketController sharedInstance] canDisassociateDrawerInsert];
    } else {
        // always allow associate
        result = YES;
    }
    
    return result;
}

#pragma mark - UIViewController

- (void) viewDidLoad
{
    [super viewDidLoad];
    
    // add the refresh button
    self.navigationItem.rightBarButtonItem =
    [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemRefresh
                                                  target:self
                                                  action:@selector(refreshStatus:)];
    // build the table header cell
    [self.tableView registerClass:[DeviceStationHeaderCell class]
           forCellReuseIdentifier:@"HeaderCell"];
    [self.tableView registerClass:[OCGSwitchTableViewCell class]
           forCellReuseIdentifier:@"SwitchCell"];
    // skin
    self.tableView.tag = kTableSkinningTag;
}

- (void) viewWillAppear:(BOOL)animated
{
    self.title = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁥󠀷󠁶󠁸󠀲󠁙󠁈󠁆󠁗󠁋󠁇󠁷󠁕󠁗󠁘󠁦󠁚󠁉󠀵󠁙󠁺󠁈󠁂󠀰󠀵󠁱󠁍󠁿*/ @"Status", nil);
    
    [[CRSSkinning currentSkin] applyViewSkin:self];
    [super viewWillAppear:animated];
}

#pragma mark - UITableViewDataSource

- (NSInteger) numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 4;
}

- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat result = 44.0f;
    
    if (indexPath.row == kRowIndexHeader) {
        result = [self rowHeightForStatus];
    }
    
    return result;
}

- (UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = nil;
    
    if (indexPath.row == kRowIndexHeader) {
        cell = [tableView dequeueReusableCellWithIdentifier:@"HeaderCell"];
    } else if (indexPath.row == kRowIndexStatus) {
        cell = [tableView dequeueReusableCellWithIdentifier:@"SwitchCell"];
    } else if (indexPath.row == kRowIndexDrawerInsert) {
        cell = [tableView dequeueReusableCellWithIdentifier:@"DetailValueCell"];
        if (cell == nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1
                                          reuseIdentifier:@"DetailValueCell"];
        }
    } else {
        cell = [tableView dequeueReusableCellWithIdentifier:@"DetailCell"];
        if (cell == nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                          reuseIdentifier:@"DetailCell"];
        }
    }
    
    if (cell != nil) {
        cell.tag = kTableCellSkinningTag;
        switch (indexPath.row) {
            case kRowIndexHeader: {
                DeviceStationHeaderCell *headerCell = (DeviceStationHeaderCell *)cell;
                headerCell.printerImageView.ringGlowColor = [self colorForStatus];
                headerCell.idDescLabel.text = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠀱󠁆󠁋󠀱󠀯󠁊󠁒󠀷󠁊󠁦󠁣󠁩󠁤󠀫󠀰󠀳󠁨󠁔󠁳󠁡󠀫󠀴󠁁󠀫󠁊󠁱󠁕󠁿*/ @"Device station id", nil);
                headerCell.idDescLabel.tag = kTableCellKeySkinningTag;
                headerCell.idLabel.text = self.station.deviceStationId;
                headerCell.idLabel.tag = kTableCellValueSkinningTag;
                headerCell.statusDescLabel.text = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁥󠀷󠁶󠁸󠀲󠁙󠁈󠁆󠁗󠁋󠁇󠁷󠁕󠁗󠁘󠁦󠁚󠁉󠀵󠁙󠁺󠁈󠁂󠀰󠀵󠁱󠁍󠁿*/ @"Status", nil);
                headerCell.statusDescLabel.tag = kTableCellKeySkinningTag;
                headerCell.statusLabel.text = [self statusDescription];
                headerCell.statusLabel.tag = kTableCellValueSkinningTag;
                // if claimed
                if (self.station.claimed) {
                    // show user
                    headerCell.claimedUserDescLabel.hidden = NO;
                    headerCell.claimedUserLabel.hidden = NO;
                    headerCell.claimedUserDescLabel.text = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠀱󠀰󠁙󠁋󠀲󠁶󠁌󠁉󠀱󠁲󠁣󠁒󠁚󠁦󠁊󠁱󠁊󠀷󠁎󠁬󠁎󠁗󠁹󠁎󠁸󠁦󠁧󠁿*/ @"Claimed by", nil);
                    headerCell.claimedUserDescLabel.tag = kTableCellKeySkinningTag;
                    headerCell.claimedUserLabel.text = self.station.claimedUserName;
                    headerCell.claimedUserLabel.tag = kTableCellValueSkinningTag;
                    // and terminal
                    headerCell.claimedTerminalDescLabel.hidden = NO;
                    headerCell.claimedTerminalLabel.hidden = NO;
                    headerCell.claimedTerminalDescLabel.text = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁷󠁘󠁳󠁂󠁡󠁪󠁦󠁕󠀸󠁹󠁥󠁌󠁤󠁃󠁍󠁳󠁷󠁒󠁩󠁈󠁆󠁶󠁧󠁂󠁑󠁑󠁙󠁿*/ @"On terminal", nil);
                    headerCell.claimedTerminalDescLabel.tag = kTableCellKeySkinningTag;
                    headerCell.claimedTerminalLabel.text = self.station.terminal;
                    headerCell.claimedTerminalLabel.tag = kTableCellValueSkinningTag;
                } else {
                    headerCell.claimedUserDescLabel.hidden = YES;
                    headerCell.claimedUserLabel.hidden = YES;
                    headerCell.claimedTerminalDescLabel.hidden = YES;
                    headerCell.claimedTerminalLabel.hidden = YES;
                }
                break;
            }
            case kRowIndexUnlink: {
                cell.textLabel.text = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁸󠁭󠁗󠁡󠁦󠁐󠁯󠁮󠀴󠁄󠁙󠁪󠁁󠁣󠁁󠁇󠁨󠁋󠁍󠁦󠁎󠁸󠁊󠁃󠁧󠁇󠁫󠁿*/ @"Unlink device station", nil);
                cell.textLabel.tag = kTableCellTextSkinningTag;
                if (self.canUnlink) {
                    cell.textLabel.alpha = 1.0;
                    cell.textLabel.opaque = YES;
                } else {
                    cell.textLabel.opaque = NO;
                    cell.textLabel.alpha = 0.2;
                }
                break;
            }
            case kRowIndexStatus: {
                OCGSwitchTableViewCell *switchCell = (OCGSwitchTableViewCell *)cell;
                switchCell.textLabel.text = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁙󠀱󠁕󠁊󠁥󠁍󠁴󠁥󠁮󠁇󠁭󠁧󠁧󠀹󠀯󠁲󠁳󠁣󠀸󠁮󠁱󠁰󠀵󠁡󠁋󠁒󠁉󠁿*/ @"Available", nil);
                switchCell.textLabel.tag = kTableCellTextSkinningTag;
                switchCell.on = self.station.available;
                switchCell.delegate = self;
                switchCell.readonly = (![[BasketController sharedInstance] doesUserHavePermissionTo:POSPermissionChangeDeviceStationStatus]);
                break;
            }
            case kRowIndexDrawerInsert: {
                cell.textLabel.text =
                [NSString stringWithFormat:@"%@ %@",
                 ((self.station.tillId == nil) || ([self.station.tillId isEqualToString:@""]) ? NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁑󠁶󠁑󠁚󠀳󠀲󠁱󠁊󠁬󠁲󠁅󠀶󠀵󠁶󠁣󠁷󠀫󠀲󠁮󠁪󠁓󠁗󠁐󠁌󠁆󠁫󠀸󠁿*/ @"Associate", nil) : NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁡󠀶󠁏󠁨󠁡󠁯󠁴󠁄󠁓󠁴󠀹󠁈󠀷󠀰󠁩󠁳󠁒󠁮󠀯󠁓󠁋󠁣󠁔󠀯󠁉󠁋󠁉󠁿*/ @"Disassociate", nil)),
                 NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁎󠁅󠁨󠁱󠁑󠁤󠀯󠁆󠁑󠀵󠁚󠁐󠁤󠁹󠁹󠁷󠁐󠁋󠁱󠁩󠁬󠁧󠁸󠁒󠀹󠁯󠁑󠁿*/ @"drawer insert", nil)];
                cell.textLabel.tag = kTableCellTextSkinningTag;
                cell.detailTextLabel.text = self.station.tillId;
                cell.detailTextLabel.tag = kTableCellValueSkinningTag;
                if (self.canAssociate) {
                    cell.textLabel.alpha = 1.0;
                    cell.textLabel.opaque = YES;
                    cell.detailTextLabel.alpha = 1.0;
                    cell.detailTextLabel.opaque = YES;
                } else {
                    cell.textLabel.opaque = NO;
                    cell.textLabel.alpha = 0.2;
                    cell.detailTextLabel.alpha = 1.0;
                    cell.detailTextLabel.opaque = YES;
                }
                break;
            }
            default:
                break;
        }
    }
    
    return cell;
}

- (void) tableView:(UITableView *)tableView
   willDisplayCell:(UITableViewCell *)cell
 forRowAtIndexPath:(NSIndexPath *)indexPath
{
    [[CRSSkinning currentSkin] applyCellSkin:cell
                                    forStyle:tableView.style];
}

#pragma mark - UITableViewDelegate

- (NSIndexPath *) tableView:(UITableView *)tableView
   willSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSIndexPath *result = indexPath;
    
    if ((indexPath.row == kRowIndexStatus) || (indexPath.row == kRowIndexStatus)) {
        result = nil;
    } else if (indexPath.row == kRowIndexUnlink) {
        // don't let the user unlink if the station isn't linked
        if (!self.canUnlink) {
            result = nil;
        }
    } else if (indexPath.row == kRowIndexDrawerInsert) {
        // don't let the user disassociate if there is a sale in progress
        if (!self.canAssociate) {
            result = nil;
        }
    }
    
    return result;
}

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath
                             animated:YES];
    switch (indexPath.row) {
        case kRowIndexUnlink: {
            if ([[BasketController sharedInstance] doesUserHavePermissionTo:POSPermissionUnlinkDeviceStation]) {
                [self unlinkStation];
            } else {
                [POSFuncController presentPermissionAlert:POSPermissionUnlinkDeviceStation
                                                withBlock:nil];
            }
            break;
        }
        case kRowIndexDrawerInsert: {
            if ((self.station.tillId == nil) || [self.station.tillId isEqualToString:@""]) {
                if ([[BasketController sharedInstance] doesUserHavePermissionTo:POSPermissionAssociateDeviceStationTillid]) {
                    [self associateStationWithId:self.station.deviceStationId
                                        complete:^(DeviceStation *deviceStation) {
                                            if (deviceStation != nil) {
                                                self.station = deviceStation;
                                            }
                                            [self.tableView reloadData];
                                        }];
                } else {
                    [POSFuncController presentPermissionAlert:POSPermissionUnlinkDeviceStation
                                                    withBlock:nil];
                }
            } else {
                if ([[BasketController sharedInstance] doesUserHavePermissionTo:POSPermissionDisassociateDeviceStationTillid]) {
                    [self disassociateStation];
                } else {
                    [POSFuncController presentPermissionAlert:POSPermissionDisassociateDeviceStationTillid
                                                    withBlock:nil];
                }
            }
            break;
        }
        default:
            break;
    }
}

#pragma mark - OCGSwitchTableViewCellDelegate

- (void) switchTableViewCell:(OCGSwitchTableViewCell *)cell
                 didToggleTo:(BOOL)state
{
    if ([[BasketController sharedInstance] doesUserHavePermissionTo:POSPermissionChangeDeviceStationStatus]) {
        [self disableStation];
    } else {
        [POSFuncController presentPermissionAlert:POSPermissionChangeDeviceStationStatus
                                        withBlock:nil];
    }
}

#pragma mark - UIAlertViewDelegate

- (void) alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 1) {
        // user pushed ok, get the till id they entered
        UITextField *alertTextField = [alertView textFieldAtIndex:0];
        NSString *tillId = alertTextField.text;
        // validate
        if (![tillId isEqualToString:@""]) {
            ocg_async_background_overlay(^{
                [RESTController.sharedInstance associateDeviceStationWithDeviceStationId:self.associateDeviceStationId
                                                                                  tillId:tillId
                                                                                complete:^(DeviceStation *newStation, NSError *restError, ServerError *restServerError)
                 {
                     if ((restError == nil) && (restServerError == nil)) {
                         dispatch_async(dispatch_get_main_queue(), ^{
                             if (self.associateComplete != nil) {
                                 self.associateComplete(newStation);
                             }
                         });
                     } else {
                         // an error occurred
                         // get the error
                         ServerErrorMessage *errorMessage = nil;
                         for (ServerErrorMessage *message in restServerError.messages) {
                             errorMessage = message;
                             break;
                         }
                         NSString *errorDeviceStationId = nil;
                         NSString *errorDeviceStationName = nil;
                         NSString *errorTerminal = nil;
                         NSString *errorUser = nil;
                         NSString *errorUnavailablePeripherals = nil;
                         if ([errorMessage isKindOfClass:[ServerErrorDeviceStationMessage class]]) {
                             errorDeviceStationId = [(ServerErrorDeviceStationMessage *)errorMessage deviceStationId];
                             errorDeviceStationName = [(ServerErrorDeviceStationMessage *)errorMessage deviceStationName];
                             errorTerminal = [(ServerErrorDeviceStationMessage *)errorMessage terminal];
                             errorUser = [(ServerErrorDeviceStationMessage *)errorMessage user];
                             errorUnavailablePeripherals = [(ServerErrorDeviceStationMessage *)errorMessage peripherals];
                         }
                         dispatch_async(dispatch_get_main_queue(), ^{
                             // display the error
                             DeviceStationViewController *vc = [[DeviceStationViewController alloc] init];
                             [vc presentAlertForDeviceStationError:errorMessage
                                               withDeviceStationId:errorDeviceStationId
                                              andDeviceStationName:errorDeviceStationName
                                                         andTillId:tillId
                                                       andTerminal:errorTerminal
                                                           andUser:errorUser
                                         andUnavailablePeripherals:errorUnavailablePeripherals
                                                         dismissed:^{
                                                             if (self.associateComplete != nil) {
                                                                 self.associateComplete(nil);
                                                             }
                                                         }];
                         });
                     }
                 }];
            });
        }
    } else {
        if (self.associateComplete != nil) {
            self.associateComplete(nil);
        }
    }
    _alert = nil;
}

- (BOOL) alertViewShouldEnableFirstOtherButton:(UIAlertView *)alertView
{
    BOOL result = YES;
    
    UITextField *alertTextField = [alertView textFieldAtIndex:0];
    NSString *tillId = alertTextField.text;
    // validate
    if ([tillId isEqualToString:@""]) {
        result = NO;
    } else {
        result = (([tillId integerValue] > 0) && ([tillId integerValue] < 10000));
    }
    
    return result;
}

#pragma mark - UITextFieldDelegate

- (BOOL) textFieldShouldReturn:(UITextField *)textField
{
    if (_alert) {
        [_alert dismissWithClickedButtonIndex:1
                                     animated:YES];
    }
    [textField resignFirstResponder];
    return NO;
}

#pragma mark - Methods

- (void) associateStationWithId:(NSString *)deviceStationId
                       complete:(DeviceStationAssociationCompleteBlockType)complete
{
    // make a note of the params for later
    self.associateDeviceStationId = deviceStationId;
    self.associateComplete = complete;
    // get the new till id
    _alert =
    [[UIAlertView alloc] initWithTitle:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁑󠁶󠁑󠁚󠀳󠀲󠁱󠁊󠁬󠁲󠁅󠀶󠀵󠁶󠁣󠁷󠀫󠀲󠁮󠁪󠁓󠁗󠁐󠁌󠁆󠁫󠀸󠁿*/ @"Associate", nil)
                               message:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁭󠁤󠀴󠁤󠁏󠁺󠁸󠁶󠁑󠁋󠀰󠀸󠀱󠁍󠀳󠁲󠁨󠁋󠁶󠁎󠁬󠀰󠀳󠁱󠁇󠁉󠁑󠁿*/ @"Please enter the drawer insert number", nil)
                              delegate:self
                     cancelButtonTitle:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁩󠁢󠁺󠁬󠁏󠁶󠁣󠁧󠁫󠁍󠁋󠁧󠁫󠀱󠁶󠁐󠁐󠀹󠁘󠀴󠁘󠁡󠁕󠁭󠁹󠁷󠀰󠁿*/ @"Cancel", nil)
                     otherButtonTitles:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁄󠁴󠁍󠁉󠁣󠀫󠁺󠁔󠁗󠁦󠁹󠁡󠁯󠀷󠁨󠁔󠁩󠀸󠁯󠀫󠁨󠁘󠀹󠁱󠁌󠀶󠀸󠁿*/ @"OK", nil), nil];
    _alert.alertViewStyle = UIAlertViewStylePlainTextInput;
    UITextField *alertTextField = [_alert textFieldAtIndex:0];
    alertTextField.placeholder = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁩󠀷󠁒󠁕󠁧󠁱󠁒󠁢󠁒󠁸󠁘󠁡󠁏󠀲󠁄󠁆󠁓󠁲󠀱󠀸󠀳󠁶󠁬󠀲󠁴󠁚󠁙󠁿*/ @"Drawer insert number", nil);
    [OCGKeyboardTypeButtonItem setKeyboardType:kOCGKeyboardTypeNumberPad forTarget:alertTextField];
    alertTextField.text = nil;
    alertTextField.delegate = self;
    [_alert show];
}

@end
