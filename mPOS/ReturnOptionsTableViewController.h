//
//  ReturnOptionsTableViewController.h
//  mPOS
//
//  Created by Antonio Strijdom on 01/11/2013.
//  Copyright (c) 2013 Clarity. All rights reserved.
//

#import <UIKit/UIKit.h>

@class MPOSItem;

@interface ReturnOptionsTableViewController : UITableViewController

/** @property itemScanId
 *  @brief The item we are returning.
 */
@property (nonatomic, strong) MPOSItem *item;

/** @property originalScanId
 *  @brief When returning a dump code, specify the original item ref here.
 */
@property (nonatomic, strong) NSString *originalScanId;

/** @property originalDescription
 *  @brief When returning a dump code, specify the original item description here.
 */
@property (nonatomic, strong) NSString *originalDescription;

@end
