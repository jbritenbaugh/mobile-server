//
//  FNKScanViewController.m
//  mPOS
//
//  Created by John Scott on 06/01/2015.
//  Copyright (c) 2015 Clarity. All rights reserved.
//

#import "FNKScanViewController.h"

#import "UICollectionView+OCGExtensions.h"
#import "FNKCollectionViewLayout.h"
#import "OCGContextCollectionViewDataSource.h"

#import "OCGCollectionViewCell.h"

#import "BarcodeScannerController.h"
#import "ReceiptSearchViewController.h"
#import "ServerErrorHandler.h"
#import "ReceiptListViewController.h"
#import "RESTController.h"
#import "UIAlertView+MKBlockAdditions.h"
#import "ReceiptItemViewController.h"
#import "BasketController.h"
#import "POSFuncController.h"
#import "POSFunc.h"
#import "CRSSkinning.h"
#import "SoftKeyContextType.h"

@interface FNKScanViewController ()

@end

@implementation FNKScanViewController
{
    NSString *_receiptBarcode;
    OCGContextCollectionViewDataSource *_dataSource;
}

#pragma mark - init

- (id)init
{
    FNKCollectionViewLayout *layout = [[FNKCollectionViewLayout alloc] init];
    self = [super initWithCollectionViewLayout:layout];
    if (self) {
        
    }
    return self;
}


#pragma mark - Public

-(void)setSoftKeyContext:(SoftKeyContext *)softKeyContext
{
    _softKeyContext = softKeyContext;
    if (self.view.window)
    {
        [_dataSource updataDataSourceWithContext:_softKeyContext
                                        withData:_displayData];
    }
}

-(void)setDisplayData:(id)displayData
{
    _displayData = displayData;
    if (self.view.window)
    {
        [_dataSource updataDataSourceWithContext:_softKeyContext
                                        withData:_displayData];
    }
}

#pragma mark - UIViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    _dataSource = [[OCGContextCollectionViewDataSource alloc] init];
    self.collectionView.dataSource = _dataSource;
    [_dataSource registerCellReuseIdsWithCollectionView:self.collectionView];
    self.collectionView.delaysContentTouches = NO;
    
    self.collectionView.tag = kEditableTableSkinningTag;
    [[CRSSkinning currentSkin] applyViewSkin:self];
    self.collectionView.backgroundColor = [UIColor colorWithRed:0.937 green:0.937 blue:0.957 alpha:1.000];
    
    UIBarButtonItem *cancelButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel
                                                                                  target:self
                                                                                  action:@selector(dismissViewController)];
    self.navigationItem.leftBarButtonItem = cancelButton;
}

- (void)viewWillAppear:(BOOL)animated
{
    // reload the layout
    [_dataSource updataDataSourceWithContext:_softKeyContext
                                    withData:_displayData];
    
    [super viewWillAppear:animated];
    [[CRSSkinning currentSkin] applyViewSkin:self];
    // TFS68365 - start scanning automatically
    [BasketController.sharedInstance enumerateSoftKeysForKeyMenu:_softKeyContext.rootMenu
                                                  softKeyContext:_softKeyContext
                                                      usingBlock:^(SoftKey *softKey, BOOL *stop)
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self startScanningForBarcode];
        });
        *stop = YES;
    }];
}

- (void)viewWillDisappear:(BOOL)animated
{
    BarcodeScannerController *controller = [BarcodeScannerController sharedInstance];
    // are we scanning?
    if (controller.scanning)
    {
        [controller stopScanning];
    }
    [super viewWillDisappear:animated];
}

#pragma mark - UICollectionViewDelegate

- (BOOL)collectionView:(UICollectionView *)collectionView shouldSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    OCGContextCollectionViewDataSource *selectedItem = [_dataSource dataSourceForIndexPath:indexPath];
    return [self FNKScanViewDelegate_shouldSelectSoftKey:selectedItem.softKey];
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    // get the data for the row selected
    OCGContextCollectionViewDataSource *selectedItem = [_dataSource dataSourceForIndexPath:indexPath];
    [self FNKScanViewDelegate_didSelectSoftKey:selectedItem.softKey];
}

#pragma mark - Calls to FNKScanViewDelegate

- (void)FNKScanViewDelegate_didSelectSoftKey:(SoftKey *)softKey
{
    if ([self.delegate respondsToSelector:@selector(FNKScanViewController:didSelectSoftKey:)])
    {
        [self.delegate FNKScanViewController:self
                                  didSelectSoftKey:softKey];
    }
}

- (BOOL)FNKScanViewDelegate_shouldSelectSoftKey:(SoftKey *)softKey
{
    BOOL result = YES;
    if ([self.delegate respondsToSelector:@selector(FNKScanViewController:shouldSelectSoftKey:)])
    {
        result = [self.delegate FNKScanViewController:self
                                        shouldSelectSoftKey:softKey];
    }
    else if (softKey)
    {
        // Default behaviour if the delegate method is not defined
        
        if ([softKey.keyType isEqualToString:@"DISPLAY_MESSAGE"])
        {
            result = NO;
        }
    }
    return result;
}

- (void)FNKScanViewDelegate_handleScannedBarcode:(NSString*)barcode
{
    if ([self.delegate respondsToSelector:@selector(FNKScanViewController:handleScannedBarcode:)])
    {
        [self.delegate FNKScanViewController:self
                              handleScannedBarcode:barcode];
    }
}

#pragma mark - Methods

-(void)dismissViewController
{
    [self dismissViewControllerAnimated:YES completion:^{
        BarcodeScannerController *controller = [BarcodeScannerController sharedInstance];
        // are we scanning?
        if (controller.scanning)
        {
            [controller stopScanning];
        }
    }];
}

- (void)startScanningForBarcode
{
    //    return;
    BarcodeScannerController *controller = [BarcodeScannerController sharedInstance];
    // are we scanning?
    if (!controller.scanning) {
        // no, start scanning
        [controller beginScanningInViewController:self
                                        forReason:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁓󠁔󠀹󠀹󠀯󠁒󠁡󠁲󠁰󠁪󠀹󠁶󠁁󠁙󠁹󠀴󠁲󠀫󠁎󠁕󠁈󠀯󠁱󠁒󠁨󠁴󠁁󠁿*/ @"Receipt", nil)
                                   withButtonSize:CGSizeZero
                                     withAddTitle:nil
                                         mustScan:NO
                              alphaNumericAllowed:YES
                                        withBlock:^(NSString *barcode, ScannerEntryMethod entryMethod, SMBarcodeType barcodeType, BOOL *continueScanning) {
                                            // debug assertions
                                            NSAssert(barcode != nil,
                                                     @"The reference to the scanned barcode has not been initialized.");
                                            NSAssert([[barcode stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length] > 0,
                                                     @"The returned barcode does not contain any characters.");
                                            // we only want to scan the one barcode
                                            *continueScanning = NO;
                                            // grab the barcode we scanned
                                            _receiptBarcode =
                                            [barcode stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                                        }
                                       terminated:^{
                                           if (_receiptBarcode != nil) {
                                               // process the barcode
                                               ocg_define_weak_self();
                                               [self FNKScanViewDelegate_handleScannedBarcode:_receiptBarcode];
                                               _receiptBarcode = nil;
                                           }
                                       }
                                            error:^(NSString *errorDesc) {
                                                if (errorDesc) {
                                                    // display an alert
                                                    UIAlertView *alert =
                                                    [[UIAlertView alloc] initWithTitle:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁯󠀫󠁋󠁆󠁢󠁚󠁱󠁩󠁈󠁕󠀲󠀸󠁫󠁲󠁧󠁦󠀲󠁴󠁘󠁣󠁈󠁎󠀶󠀲󠁯󠀶󠁳󠁿*/ @"Scanning Error", nil)
                                                                               message:errorDesc
                                                                              delegate:nil
                                                                     cancelButtonTitle:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁄󠁴󠁍󠁉󠁣󠀫󠁺󠁔󠁗󠁦󠁹󠁡󠁯󠀷󠁨󠁔󠁩󠀸󠁯󠀫󠁨󠁘󠀹󠁱󠁌󠀶󠀸󠁿*/ @"OK", nil)
                                                                     otherButtonTitles:nil];
                                                    [alert show];
                                                }
                                            }];
    }
}

@end
