#!/usr/bin/xcrun --sdk macosx swift
//
//  xml_bdm.swift
//  mPOS
//
//  Created by John Scott on 20/04/2015.
//  Copyright (c) 2015 Clarity. All rights reserved.
//

import Foundation

var xmlString:String = "<string from example>"
var error:NSError? = nil
var document:NSXMLDocument? = NSXMLDocument(XMLString: xmlString, options: 0, error: &error)

println(document)