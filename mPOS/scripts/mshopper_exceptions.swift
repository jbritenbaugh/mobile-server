#!/usr/bin/xcrun --sdk macosx swift
//
//  mshopper_exceptions.swift
//  mPOS
//
//  Created by John Scott on 23/03/2015.
//  Copyright (c) 2015 Clarity. All rights reserved.
//

import Foundation

extension String
{
    var lowercaseFirstCharacter: String
    {
            return substringToIndex(advance(startIndex, 1)).lowercaseString+substringFromIndex(advance(startIndex, 1))
    }
    
    var uppercaseFirstCharacter: String
    {
            return substringToIndex(advance(startIndex, 1)).uppercaseString+substringFromIndex(advance(startIndex, 1))
    }
    
    func writeToFileIfChanged(path: String, atomically: Bool, encoding: NSStringEncoding, error: NSErrorPointer) -> Bool
    {
//        println("\(path) :\n\(self)\n\n")
        
        var shouldWriteToFile = true
        
        if let original : String = String(contentsOfFile: path, encoding: encoding, error: error)
        {
            if self == original
            {
                shouldWriteToFile = false
            }
        }
        
        if shouldWriteToFile
        {
            return writeToFile(path, atomically:atomically, encoding:encoding, error:error)
        }
        else
        {
            return true
        }
    }
    
    func rangeFromNSRange(range: NSRange) -> Range<String.Index>
    {
        let start = advance(self.startIndex, range.location)
        let end = advance(start, range.length)
        let swiftRange = Range<String.Index>(start: start, end: end)
        return swiftRange
    }
}

struct Exception {
    let name : String
    let code : Int
    let text : String
    let isDeprecated : Bool
}

let path = Process.arguments[1]

let source : String? = String(contentsOfFile: path, encoding: NSUTF8StringEncoding, error: nil)

if let values = source?.componentsSeparatedByString("\n").filter({count($0) > 0 })//.sorted({ ($0 as String) < ($1 as String)})
{
    var exceptions : Array<Exception> = []
    
    for (index, line) in enumerate(values)
    {
        if let match = NSRegularExpression(pattern: "((?:@Deprecated)?)\\s*([A-Za-z_]+)\\s*\\(\\s*(\\d+)\\s*,\\s*\"([^\"]*)\"", options : .AnchorsMatchLines, error : nil)?.firstMatchInString(line, options: .Anchored, range: NSMakeRange(0, count(line)))
        {
            var isDeprecated = line.substringWithRange(line.rangeFromNSRange(match.rangeAtIndex(1)))
            var name = line.substringWithRange(line.rangeFromNSRange(match.rangeAtIndex(2)))
            var code = line.substringWithRange(line.rangeFromNSRange(match.rangeAtIndex(3)))
            var text = line.substringWithRange(line.rangeFromNSRange(match.rangeAtIndex(4)))
            exceptions.append(Exception(name: name, code:code.toInt()!, text:text, isDeprecated:isDeprecated == "@Deprecated"))
        }
    }
    
    /*
        Update the source if necessary
    */
    var compiledSource = ""
    
    for (index, exception) in enumerate(exceptions.sorted({$0.name.lowercaseString < $1.name.lowercaseString}))
    {
        if  exception.isDeprecated
        {
            compiledSource += "@Deprecated "
        }
        compiledSource += "\(exception.name)(\(exception.code), \"\(exception.text)\")\n"
    }
    
    compiledSource.writeToFileIfChanged(path, atomically: true, encoding: NSUTF8StringEncoding, error: nil)
    
    /*
        Update the headers if necessary
    */
    
    var compiledHeaders = ""
    
    compiledHeaders += "typedef NS_ENUM(NSUInteger, MShopperExceptionCode) {\n"
    
    compiledHeaders += "  MShopperUnknownExceptionCode = -2,\n"
    compiledHeaders += "  MShopperNullExceptionCode = -1,\n"
    
    for (index, exception) in enumerate(exceptions.sorted({$0.code < $1.code}))
    {
        compiledHeaders += "  MShopper\(exception.name)ExceptionCode"
        
        if  exception.isDeprecated
        {
//            compiledHeaders += " __attribute__((deprecated))"
            compiledHeaders += " /* deprecated */"
        }
        
        compiledHeaders += " = \(exception.code),\n"
    }
    
    compiledHeaders += "};\n"
    compiledHeaders += "\n"
    
    compiledHeaders += "NSString* MShopperExceptionNameFromCode(MShopperExceptionCode code);\n"
    
    compiledHeaders.writeToFileIfChanged(path.stringByDeletingPathExtension.stringByAppendingPathExtension("h")!, atomically: true, encoding: NSUTF8StringEncoding, error: nil)

    /*
        Update the methods if necessary
    */
    
    var compiledMethods = ""
    
    compiledMethods += "#import \"MShopperExceptions.h\"\n"
    compiledMethods += "\n"
    
    compiledMethods += "NSString* MShopperExceptionNameFromCode(MShopperExceptionCode code) {\n"
    
    compiledMethods +=  "  switch(code) {\n";
    
    compiledMethods +=  "    case -2: return nil;\n"
    compiledMethods +=  "    case -1: return nil;\n"
    
    for (index, exception) in enumerate(exceptions.sorted({$0.code < $1.code}))
    {
        compiledMethods +=  "    case \(exception.code): return @\"\(exception.name)\";\n"
    }
    
    compiledMethods += "  }\n"
    compiledMethods += "  return nil;\n"
    compiledMethods += "}\n"
    
    compiledMethods.writeToFileIfChanged(path.stringByDeletingPathExtension.stringByAppendingPathExtension("m")!, atomically: true, encoding: NSUTF8StringEncoding, error: nil)

    /*
    Update the strings if necessary
    */
    
    
    var compiledStrings = ""
    
    for (index, exception) in enumerate(exceptions.sorted({$0.name.lowercaseString < $1.name.lowercaseString}))
    {
        compiledStrings += "\"\(exception.name)\" = \"\(exception.text)\";\n"
    }
    
    compiledStrings.writeToFileIfChanged(path.stringByDeletingLastPathComponent.stringByAppendingPathComponent("en.lproj/MShopperExceptions.strings"), atomically: true, encoding: NSUTF8StringEncoding, error: nil)
}

