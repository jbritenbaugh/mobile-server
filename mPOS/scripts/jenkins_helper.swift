#!/usr/bin/xcrun --sdk macosx swift
//
//  jenkins_helper.swift
//  mPOS
//
//  Created by John Scott on 24/03/2015.
//  Copyright (c) 2015 Clarity. All rights reserved.
//

import Foundation

let environment = NSProcessInfo.processInfo().environment

if let workspace = environment["WORKSPACE"] as! String?
{
    var content = ""
    for (key, value) in environment
    {
        content += "\(key)=\(value)\n"
    }
    
    content.writeToFile(workspace.stringByAppendingPathComponent("jenkins_helper.txt"), atomically: true, encoding: NSUTF8StringEncoding, error: nil)
}
