#!/usr/bin/xcrun --sdk macosx swift
//
//  main.swift
//  scripts
//
//  Created by John Scott on 24/04/2015.
//  Copyright (c) 2015 Clarity. All rights reserved.
//

import Foundation

println("Nothing to see here. Move along now.")

import Foundation

extension String
{
    var lowercaseFirstCharacter: String
        {
            return substringToIndex(advance(startIndex, 1)).lowercaseString+substringFromIndex(advance(startIndex, 1))
    }
    
    var uppercaseFirstCharacter: String
        {
            return substringToIndex(advance(startIndex, 1)).uppercaseString+substringFromIndex(advance(startIndex, 1))
    }
    
    func writeToFileIfChanged(path: String, atomically: Bool, encoding: NSStringEncoding, error: NSErrorPointer) -> Bool
    {
        //        println("\(path) :\n\(self)\n\n")
        
        var shouldWriteToFile = true
        
        if let original : String = String(contentsOfFile: path, encoding: encoding, error: error)
        {
            if self == original
            {
                shouldWriteToFile = false
            }
        }
        
        if shouldWriteToFile
        {
            return writeToFile(path, atomically:atomically, encoding:encoding, error:error)
        }
        else
        {
            return true
        }
    }
    
    func rangeFromNSRange(range: NSRange) -> Range<String.Index>
    {
        let start = advance(self.startIndex, range.location)
        let end = advance(start, range.length)
        let swiftRange = Range<String.Index>(start: start, end: end)
        return swiftRange
    }
    
    
    private func fullRange() -> NSRange
    {
        return NSRange(location:0, length:count(self))
    }
}

/**
* Swift wrapper over NSRegularExpression, aiming at giving a Ruby-like API for RegExps.
* Inspired by http://benscheirman.com/2014/06/regex-in-swift/
*/
class RegExp {
    
    var regexp:NSRegularExpression!
    
    
    /// failable init - will return nil if regexp syntax is misformed
    init?(pattern:String, options:NSRegularExpressionOptions) {
        var error:NSError?
        
        var testRegExp = NSRegularExpression(pattern: pattern, options: options, error: &error)
        
        if error != nil {
            return nil
        }
        
        regexp = testRegExp
    }
    
    convenience init?(_ pattern:String)
    {
        self.init(pattern: pattern, options: NSRegularExpressionOptions.CaseInsensitive)
    }
    
    /// returns the matching part of a String
    func match(string:String) -> [String]? {
        if let result = regexp.firstMatchInString(string, options: NSMatchingOptions(0), range: string.fullRange())
        {
            var match : [String] = []
            for i in 1..<result.numberOfRanges {
                
                let subMatch = string.substringWithRange(string.rangeFromNSRange(result.rangeAtIndex(i)))
                match.append(subMatch as String)
            }
            return match
        } else {
            return nil
        }
    }
}

/// Some operator overloading
infix operator =~ {}

func =~ (left:String, right:RegExp) -> [String]?
{
    return right.match(left)
}

//struct Token<U>
//{
//    let _value<U>;
//    let _
//}


let path = "/Users/johnscott/Repositories/mpos-fraggle/mPOS/scripts/webservice_definitions.ini"// Process.arguments[1]

let source : String? = String(contentsOfFile: path, encoding: NSUTF8StringEncoding, error: nil)

var sections : [String : [String : String]];
if let lines = source?.componentsSeparatedByString("\n").filter({count($0) > 0 })//.sorted({ ($0 as String) < ($1 as String)})
{
    for line in lines
    {
        if let foo = RegExp("^\\[([^\\]]+)\\]")?.match(line)
        {
            println("[\(foo[0])]")
        }
        else if let foo = RegExp("\\s*([^=\\s]+)\\s*=\\s*([^=\\s]+)\\s*")?.match(line)
        {
            println("\(foo[0]) = \(foo[1])")
        }
    }
}









