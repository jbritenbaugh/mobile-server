#!/usr/bin/xcrun --sdk macosx swift
//
//  update_enum.swift
//  mPOS
//
//  Created by John Scott on 20/03/2015.
//  Copyright (c) 2015 Clarity. All rights reserved.
//

import Foundation

extension String
{
    var lowercaseFirstCharacter: String
    {
        return substringToIndex(advance(startIndex, 1)).lowercaseString+substringFromIndex(advance(startIndex, 1))
    }
    
    var uppercaseFirstCharacter: String
    {
        return substringToIndex(advance(startIndex, 1)).uppercaseString+substringFromIndex(advance(startIndex, 1))
    }
    
    func writeToFileIfChanged(path: String, atomically: Bool, encoding: NSStringEncoding, error: NSErrorPointer) -> Bool
    {
        var shouldWriteToFile = true
        
        if let original : String = String(contentsOfFile: path, encoding: encoding, error: error)
        {
            if self == original
            {
                shouldWriteToFile = false
            }
        }
        
        if shouldWriteToFile
        {
            return writeToFile(path, atomically:atomically, encoding:encoding, error:error)
        }
        else
        {
            return true
        }
    }
}

func underscoreToCamelCase(string: String) -> String {
    return join("", string.componentsSeparatedByString("_").map({($0 as String).lowercaseString.uppercaseFirstCharacter}))
}

let path = Process.arguments[1]

let source : String? = String(contentsOfFile: path, encoding: NSUTF8StringEncoding, error: nil)

if let values = source?.componentsSeparatedByString("\n").filter({count($0) > 0 }).sorted({ ($0 as String) < ($1 as String)})
{
    /* 
        Update the source if necessary
    */
    let compiledSource : String = "\n".join(values)
    
    compiledSource.writeToFileIfChanged(path, atomically: true, encoding: NSUTF8StringEncoding, error: nil)
    
    let typeName = path.lastPathComponent.stringByDeletingPathExtension
    
    /*
        Update the headers if necessary
    */
    
    var compiledHeaders = ""
    
    compiledHeaders += "typedef NS_ENUM(NSInteger, \(typeName)) {\n"
    
    compiledHeaders += "  \(typeName)Unknown = -2,\n"
    compiledHeaders += "  \(typeName)Null = -1,\n"
    
    for (index, value) in enumerate(values)
    {
        compiledHeaders += "  \(typeName)\(underscoreToCamelCase(value)),\n"
    }
    
    compiledHeaders += "};\n"
    compiledHeaders += "\n"

    compiledHeaders += "\(typeName) \(typeName)ForString(NSString *string);\n"
    compiledHeaders += "NSString* NSStringFor\(typeName)(\(typeName) a\(typeName));\n"

    compiledHeaders.writeToFileIfChanged(path.stringByDeletingPathExtension.stringByAppendingPathExtension("h")!, atomically: true, encoding: NSUTF8StringEncoding, error: nil)
    
    /*
        Update the methods if necessary
    */
    
    var compiledMethods = ""
    
    compiledMethods += "#import \"\(typeName).h\"\n"
    compiledMethods += "\n"
    
    compiledMethods += "\(typeName) \(typeName)ForString(NSString *string) {\n"
    
    compiledMethods +=  "  if (string == nil) return \(typeName)Null;\n"
    
    for (index, value) in enumerate(values)
    {
        compiledMethods +=  "  if ([string isEqualToString:@\"\(value)\"]) return \(typeName)\(underscoreToCamelCase(value));\n"
    }

    compiledMethods +=  "  return \(typeName)Unknown;\n"
    
    compiledMethods += "}\n"
    
    compiledMethods += "NSString* NSStringFor\(typeName)(\(typeName) a\(typeName)) {\n"
    
    compiledMethods +=  "  switch(a\(typeName)) {\n"
    
    compiledMethods +=  "    case \(typeName)Null: return nil;\n"
    
    for (index, value) in enumerate(values)
    {
        compiledMethods +=  "    case \(typeName)\(underscoreToCamelCase(value)): return @\"\(value)\";\n"
    }
    
    compiledMethods += "    default: return nil;\n"
    
    compiledMethods += "  }\n"
    
    compiledMethods += "}\n"
    
    compiledMethods.writeToFileIfChanged(path.stringByDeletingPathExtension.stringByAppendingPathExtension("m")!, atomically: true, encoding: NSUTF8StringEncoding, error: nil)

}
