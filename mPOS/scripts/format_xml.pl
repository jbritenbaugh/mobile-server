#!/usr/bin/perl

use strict;
use Data::Dumper;
use File::Slurp;

my $text = "";


# takes the first argument as a file to convert of the contents of the paste board and cleans it up for readability

if (@ARGV)
{
	$text = read_file($ARGV[0]);
}
else
{
	$text = join "", qx(pbpaste);
}

$text =~ s!\n+!!g;

$text =~ s!&lt;!<!gi;
$text =~ s!&gt;!>!gi;
$text =~ s!&amp;!&!gi;

# print $text;

my @parts = grep {length} split /(<[^>]+>)/, $text;

my @stack;
foreach my $part (@parts)
{
	push @stack, [] unless @stack;
	if ($part =~ m!</!)
	{
		push @{$stack[-1]}, $part;
		pop @stack;
	}
	elsif ($part =~ m!<!)
	{
		my $frame = [];
		push @{$stack[-1]}, $frame;
		push @stack, $frame;
		push @{$stack[-1]}, $part;
		if ($part =~ m![/?]>!)
		{
			pop @stack;
		}
	}
	else
	{
		push @{$stack[-1]}, $part;
	}
}

my $buffer;

my $html = 1;

my $filename;

if (@ARGV)
{
	$filename = $ARGV[0];
}
else
{
	$filename = 'formated_xml';
}

if ($html)
{
	$filename .= '.html';
}
else
{
	$filename .= '.txt';
}

my $root = $stack[0];

display_xml($root, 0);

write_file( $filename, $buffer ) ;

system 'open', '-a', '/Applications/Google Chrome.app', $filename;


sub display_xml
{
	my ($frame, $indent) = @_;
	my @frame = @{$_[0]};
	if ((3 == @frame && !ref $frame[0] && !ref $frame[1] && !ref $frame[2]) || (2 == @frame && !ref $frame[0] && !ref $frame[1]))
	{
			$buffer .= format_indent($indent);
			for my $part (@frame)
			{
				$buffer .= format_part($part);
			}
			$buffer .= format_newline();
	}
	else
	{
		for my $part (@frame)
		{
			if (ref $part)
			{
				display_xml($part, $indent+1);
			}
			else
			{
				$buffer .= format_indent($indent);
				$buffer .= format_part($part);
				$buffer .= format_newline();
			}
		}
	}
}

sub format_part
{
	my ($part) = @_;
	if ($html)
	{	
		if ($part =~ /</)
		{
			my @bits = grep {length} split m!(</|>|<)|([^ =]+="[^"]+")!, $part;
			$part = "";
			for my $bit (@bits)
			{
				if ($bit =~ m![</>"]!)
				{
					$part .= '<span style="color:grey">'.escape_html($bit).'</span>'
				}
				else
				{
					$part .= '<span style="color:#ff8888">'.escape_html($bit).'</span>'
				}
			}
		}
		else
		{
			$part = '<span style="color:#0000ff">'.escape_html($part).'</span>';
		}
	}
	return $part;
}

sub escape_html
{
	my ($part) = @_;
	$part =~ s|&|&amp;|g;
	$part =~ s|<|&lt;|g;
	$part =~ s|>|&gt;|g;
	return $part;
}

sub format_indent
{
	my ($indent) = @_;
	if ($html)
	{
		return "&nbsp;&nbsp;" x $indent;
	}
	else
	{
		return "  " x $indent;
	}
}

sub format_newline
{
	my ($indent) = @_;
	if ($html)
	{
		return "<br>";
	}
	else
	{
		return "\n";
	}
}