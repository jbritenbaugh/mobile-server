#!/usr/bin/perl

use strict;
use FileHandle;
use File::Slurp;
use Data::Dumper;

# This script translates the webservice_definitions.ini file into RESTController.h
# and RESTController.m.
# 
# The INI file contains two types of sections:
# 
# Web service call definition
# ^^^^^^^^^^^^^^^^^^^^^^^^^^^
# 
# The name of this section ALWAYS starts with lower-case letter and ALWAYS contains
# a _path key.
# 
# _path     The URL of the webservice
# _return   The return type (defaults to 'void')
# _etag     Sets whether the call returns an 'Etag' in the response headers.
#           ('yes' or 'no')
# _server   Set whether the server address is explicitly passed to this call.
#           ('yes' or 'no')
# _method   The HTTP method. ('GET', 'POST' or 'PUT')
# 
# All non-underscore-keys are arguments for the method. The value is the argument's
# type.
# 
# Class definition
# ^^^^^^^^^^^^^^^^
# The name of this section ALWAYS starts with upper-case letter and NEVER contains a
# _path key.
# 
# _tag      The default XML tag for this type. Used if the object is the root element
#           or an element in a list.
# _super    The parent class of the this type. (defaults to 'RESTObject')
# _alias    If set, this does not create a class, but uses the preferred class for
#           everything apart from the _tag value.
# _parent   The property that points to the parent object. If set the named property
#           will be switched use 'weak' storage and automatically set by the the XML
#           parser.
# 
# All non-underscore-keys properties for the object. The value is the property's type.

# The type_details is list of hashes containing details about known classes.
#
# DO NOT add classes defined in webservice_definitions.ini; they are
# automatically added.
# 
# Typically, you will only need to define type keys:
# 
# type      This is the name of the class or primitive type. For example, 'BOOL'
#           or 'NSNumber'.
# storage   This is the memory management attribute for the type. This should be
#           'strong' or 'assign'.
# 
# Some other keys may be overridden if different from the norm:
# 
# boxing    This is used when converting a primitive type into anto a boxed
#           object. This should only be defined if the standard '@(...)' should
#           not be used. All instances of '{}' are replaced with the argument's name
# usage     This is used when defined in an instance variablle. It should only be
#           used if the standard '<primitive type>' or '<class>*' should not be
#           used.

my @type_details =
(
	{
		'type' => 'BOOL',
		'storage' => 'assign',
		'boxing' => '({} ? @"true" : @"false")',
	},
	{
		'type' => 'double',
		'storage' => 'assign',
	},
	{
		'type' => 'id<NSCoding>',
		'storage' => 'strong',
		'usage' => 'id<NSCoding>',
	},
	{
		'type' => 'NSArray',
		'storage' => 'strong',
	},
	{
		'type' => 'NSData',
		'storage' => 'strong',
	},
	{
		'type' => 'NSDate',
		'storage' => 'strong',
	},
	{
		'type' => 'NSDictionary',
		'storage' => 'strong',
	},
	{
		'type' => 'NSInteger',
		'storage' => 'assign',
	},
	{
		'type' => 'NSNumber',
		'storage' => 'strong',
	},
	{
		'type' => 'NSString',
		'storage' => 'strong',
	},
	{
		'type' => 'RESTObject',
		'storage' => 'strong',
	},
    {
        'type' => 'id',
        'storage' => 'strong',
        'usage' => 'id',
    },
);

chdir "mPOS";

my $lines = read_file('scripts/webservice_definitions.ini');

my %definitions;

my $current_definition;

my $line_number = 0;
my %line_numbers;

my %unique_propterties;

foreach my $line (split m!\n!, $lines)
{
	$line_number++;
	$line =~ s![;#].*!!g;
	$line =~ s![\n\s]*!!g;
	next if $line eq '';
		
	if (my ($definition) = $line =~ m!\[([^\]]+)\]!)
	{
		$current_definition = $definition;
		$definitions{$current_definition} ||= {};
		push @type_details,
		{
			'type' => $definition,
			'storage' => 'strong',
		};
	}
	elsif (my ($property, $value) = $line =~ m!([^=]+)=([^=]+)!)
	{
		$definitions{$current_definition}{$property} = $value;
		$line_numbers{$current_definition}{$property} = $line_number;
				
		if ($property =~ s!^DRAFT_!!)
		{
# 			warn sprintf qq(./mPOS/scripts/webservice_definitions.ini:%d: warning: Draft property %s\n), $line_number, $property;
		}
		
		if (exists $unique_propterties{$current_definition}{lc $property})
		{
			if ($unique_propterties{$current_definition}{lc $property} > 0)
			{
				warn sprintf qq(./mPOS/scripts/webservice_definitions.ini:%d: warning: Duplicate property %s\n), $unique_propterties{$current_definition}{lc $property}, $property;
				$unique_propterties{lc $property} = 0;
			}

			warn sprintf qq(./mPOS/scripts/webservice_definitions.ini:%d: warning: Duplicate property %s\n), $line_number, $property;
		}
		
		$unique_propterties{$current_definition}{lc $property} = $line_number;
	}
	else
	{
		die qq(./mPOS/scripts/webservice_definitions.ini:).$line_number.qq(: error: can't parse line unrecognised line\n);
	}
}

my %type_details;

foreach my $type_details (@type_details)
{
	$type_details{$type_details->{'type'}} = $type_details;
}

my %subclasses;

foreach my $definition (sort {ck($a) cmp ck($b)} keys %definitions)
{
	if	(exists $definitions{$definition}{'_path'})
	{
		if (!exists $definitions{$definition}{'_etag'})
		{
			$definitions{$definition}{'_etag'} = 'no';
		}
		
		if ($definitions{$definition}{'_etag'} ne 'yes' && $definitions{$definition}{'_etag'} ne 'no')
		{
			die qq(./mPOS/scripts/webservice_definitions.ini:).$line_numbers{$definition}{'_etag'}.qq(: error: _etag value MUST be 'yes' or 'no'\n);
		}
		
		if (!exists $definitions{$definition}{'_server'})
		{
			$definitions{$definition}{'_server'} = 'no';
		}
        
        if (!exists $definitions{$definition}{'_context'})
        {
            my $name = detail_for_property($definition, undef, 'type');
            $name =~ s/([a-z])([A-Z])/$1_$2/g;
            $name =~ s/([A-Z][a-z])/_$1/g;
            $name =~ s/~/_with_/g;
            $name =~ s/_+/_/g;
            $definitions{$definition}{'_context'} = uc $name;
        }
		
		if ($definitions{$definition}{'_server'} ne 'yes' && $definitions{$definition}{'_server'} ne 'no')
		{
			die qq(./mPOS/scripts/webservice_definitions.ini:).$line_numbers{$definition}{'_server'}.qq(: error: _server value MUST be 'yes' or 'no'\n);
		}
	}
	else
	{
		$subclasses{$definitions{$definition}{'_super'}}{$definition}++;
        
        if (!exists $definitions{$definition}{'_context'})
        {
            my $name = $definition;
            $name =~ s/DTO$//;
            $definitions{$definition}{'_context'} = uc $name;
        }
	}
}

my $h_content = qq(/* web definitions header */\n\n#import "RESTControllerBase.h"\n#import "RESTObject.h"\n#import "RESTInvocation.h"\n\n);
my $m_content = qq(/* web definitions implementation */\n\n#import "RESTController.h");


foreach my $definition (sort {ck($a) cmp ck($b)} keys %definitions)
{
	if (exists $definitions{$definition}{'_path'})
	{
		my $name = detail_for_property($definition, undef, 'type');
		$name =~ s/~([^~]+)/'With'.ucfirst $1/ge;
		$name = ucfirst $name;		
		$h_content .= sprintf qq(\@class %sInvocation;\n), $name;
	}
	else
	{
		$h_content .= sprintf qq(\@class %s;\n), detail_for_property($definition, undef, 'type');
	}
}

$h_content .= qq(\n\@protocol RESTControllerMethods <NSObject>\n\@optional\n);

$m_content .= qq(\n\@implementation RESTController\n);

foreach my $definition (sort {ck($a) cmp ck($b)} keys %definitions)
{
	if (exists $definitions{$definition}{'_path'})
	{
		my $prototype = '';
		my $name = $definition;
		$name =~ s/~[^~]+//;
		$prototype .= sprintf qq(\n-(%s)%s), detail_for_property($definition,'_return', 'usage'), $name;
		my $first = 1;
		foreach my $property (sort {ck($a) cmp ck($b)} keys %{$definitions{$definition}})
		{
			next if !is_property($property);
						
			my $argument = $property;
			if ($first)
			{
				$prototype .= qq(With);
				$first = 0;
				$argument = ucfirst $argument;
			}
			else
			{
				$prototype .= ' ';
			}
			$prototype .= sprintf qq(%s:(%s)%s), $argument, detail_for_property($definition, $property, 'usage'), $property;
		}
		
		if ($definitions{$definition}{'_server'} eq 'yes')
		{
			$prototype .= qq! server:(NSString *)server!;
		}
		
		if ($first)
		{
			$prototype .= qq!Error:(NSError **)error serverError:(ServerError **)serverError!;
			$first = 0;
		}
		else
		{
			$prototype .= qq! error:(NSError **)error serverError:(ServerError **)serverError!;
		}
		if ($definitions{$definition}{'_etag'} eq 'yes')
		{
			$prototype .= qq! etag:(NSString **)etag!;
		}
		
		$h_content .= $prototype;
		$h_content .= qq(  __attribute__((deprecated));\n);
		
		$m_content .= $prototype;
		$m_content .= qq(\n{\n);
		
		my $name = detail_for_property($definition, undef, 'type');
		$name =~ s/~([^~]+)/'With'.ucfirst $1/ge;
		$name = ucfirst $name;
		
		$m_content .= sprintf qq(\t%sInvocation *invocation = [[%sInvocation alloc] init];\n), $name, $name;
		foreach my $property (sort {ck($a) cmp ck($b)} keys %{$definitions{$definition}})
		{
			next if !is_property($property);
			$m_content .= sprintf qq(\tinvocation.%s = %s;\n), $property, $property;
		}
		
		if (detail_for_property($definition, '_return', 'type') eq 'BOOL')
		{
			$m_content .= qq(\t__block NSNumber* result;\n);
		}
		elsif (detail_for_property($definition, '_return', 'type') ne 'void')
		{
			$m_content .= sprintf qq(\t__block %s result;\n), detail_for_property($definition,'_return', 'usage');
		}
	
		
		$m_content .= qq(	[self invokeSynchronous:invocation\n);
		$m_content .= sprintf qq(	                 server:%s\n), ($definitions{$definition}{'_server'} eq 'yes' ? 'server' : 'nil');
		$m_content .= sprintf qq(	                   etag:%s\n), $definitions{$definition}{'_etag'} eq 'yes' ? '*etag' : 'nil';
		$m_content .= qq(	               complete:^(RESTInvocation*invocation, NSString*invocationServer, id invocationResult, NSError *invocationError, ServerError *invocationServerError, NSString *invocationEtag) {\n);
		if (detail_for_property($definition, '_return', 'type') ne 'void')
		{
			$m_content .= qq(	                   result = invocationResult;\n);
		}
		$m_content .= qq(	                   if (error)\n);
		$m_content .= qq(	                   {\n);
		$m_content .= qq(	                       *error = invocationError;\n);
		$m_content .= qq(	                   }\n);
		$m_content .= qq(	                   if (serverError)\n);
		$m_content .= qq(	                   {\n);
		$m_content .= qq(	                       *serverError = invocationServerError;\n);
		$m_content .= qq(	                   }\n);
		

		if ($definitions{$definition}{'_etag'} eq 'yes')
		{
			$m_content .= qq(	                   if (etag)\n);
			$m_content .= qq(	                   {\n);
			$m_content .= qq(	                       *etag = invocationEtag;\n);
			$m_content .= qq(	                   }\n);
		}
		$m_content .= qq(	               }];\n);

		if (detail_for_property($definition, '_return', 'type') eq 'BOOL')
		{
			$m_content .= qq(\treturn [result boolValue];\n);
		}
		elsif (detail_for_property($definition, '_return', 'type') ne 'void')
		{
			$m_content .= qq(\treturn result;\n);
		}
		$m_content .= qq(}\n);
	}
}

# New code

foreach my $definition (sort {ck($a) cmp ck($b)} keys %definitions)
{	
	
	if (exists $definitions{$definition}{'_path'})
	{
		my $prototype = '';
		my $name = $definition;
		$name =~ s/~[^~]+//;
		$prototype .= sprintf qq(\n-(void)%s), $name;
		
		my @arguments;
		foreach my $property (sort {ck($a) cmp ck($b)} keys %{$definitions{$definition}})
		{
			next if !is_property($property);
						
			my $argument = $property;
			
			if (!@arguments)
			{
				$argument = qq(with).ucfirst($property);
			}
			push @arguments, sprintf qq(%s:(%s)%s), $argument, detail_for_property($definition, $property, 'usage'), $property;
		}
				
		if ($definitions{$definition}{'_server'} eq 'yes')
		{
			push @arguments, qq!server:(NSString *)server!;
		}
		
		if ($definitions{$definition}{'_etag'} eq 'yes')
		{
			push @arguments, qq!etag:(NSString *)etag!;
		}
		
		my $complete = qq!complete:(void (^)(!;
		
		if (detail_for_property($definition,'_return', 'usage') ne 'void')
		{
			$complete .= sprintf qq!%s result, !, detail_for_property($definition,'_return', 'usage');
		}
		
		$complete .= qq!NSError* error, ServerError* serverError!;

		if ($definitions{$definition}{'_etag'} eq 'yes')
		{
			$complete .= qq!, NSString* etag!;
		}
		
		$complete .= qq!))complete!;
		
		push @arguments, $complete;
		
		$prototype .= ucfirst join (" ", @arguments);

		$h_content .= $prototype;
		$h_content .= qq(;\n);
		
		$m_content .= $prototype;
		$m_content .= qq(\n{\n);
		
		my $name = detail_for_property($definition, undef, 'type');
		$name =~ s/~([^~]+)/'With'.ucfirst $1/ge;
		$name = ucfirst $name;
		
		$m_content .= sprintf qq(\t%sInvocation *invocation = [[%sInvocation alloc] init];\n), $name, $name;
		foreach my $property (sort {ck($a) cmp ck($b)} keys %{$definitions{$definition}})
		{
			next if !is_property($property);
			$m_content .= sprintf qq(\tinvocation.%s = %s;\n), $property, $property;
		}
		
        $m_content .= qq(	invocation.complete = \n);
        $m_content .= qq(	^(RESTInvocation*invocation, NSString*invocationServer, id invocationResult, NSError *invocationError, ServerError *invocationServerError, NSString *invocationEtag)\n);
        $m_content .= qq(	{\n);
            
	$m_content .= qq!		if (complete)\n!;
	$m_content .= qq!		{\n!;
        $m_content .= qq!			complete(!;
        
        if (detail_for_property($definition, '_return', 'type') eq 'BOOL')
        {
            $m_content .= qq([invocationResult boolValue], );
        }
        elsif (detail_for_property($definition, '_return', 'type') ne 'void')
        {
            $m_content .= qq(invocationResult, );
        }
        
        $m_content .= qq!invocationError, invocationServerError!;
        
        if ($definitions{$definition}{'_etag'} eq 'yes')
        {
            $m_content .= qq!, invocationEtag!;
        }
        $m_content .= qq!);\n!;
        $m_content .= qq!		}\n!;
        
        $m_content .= qq(	};\n);
		$m_content .= qq(	[self invokeSynchronous:invocation\n);
		$m_content .= sprintf qq(	                 server:%s\n), ($definitions{$definition}{'_server'} eq 'yes' ? 'server' : 'nil');
		$m_content .= sprintf qq(	                   etag:%s\n), $definitions{$definition}{'_etag'} eq 'yes' ? 'etag' : 'nil';
        $m_content .= qq(	               complete:invocation.complete];\n);

		$m_content .= qq(}\n);
	}
}

$h_content .= qq(\n\@end\n);

$h_content .= qq(\n\@interface RESTController : RESTControllerBase <RESTControllerMethods>\n);
$h_content .= qq(\@end\n);

$m_content .= qq(\n-(NSString*)typeForTag:(NSString*)tag\n{\n);
$m_content .= qq(\ttag = tag.lowercaseString;\n);

foreach my $definition (sort {ck($a) cmp ck($b)} keys %definitions)
{
	if (exists $definitions{$definition}{'_tag'})
	{
		my $type = $definition;
		$m_content .= sprintf qq(\tif ([tag isEqual:@"%s"])\n\t{\n\t\treturn @"%s";\n\t}\n), lc $definitions{$definition}{'_tag'}, detail_for_property($type, undef, 'type');
	}
}

$m_content .= qq(\treturn nil;\n}\n);

$m_content .= qq(\n- (Class)classForContextName:(NSString*)contextName\n{\n);
$m_content .= qq(\tcontextName = contextName.uppercaseString;\n);

foreach my $definition (sort {ck($a) cmp ck($b)} keys %definitions)
{
    if (exists $definitions{$definition}{'_context'})
    {
        my $class = detail_for_property($definition, undef, 'type');
        if (exists $definitions{$definition}{'_path'})
        {
            $class =~ s/~([^~]+)/'With'.ucfirst $1/ge;
            $class = ucfirst $class;
            $class .= "Invocation";
        }
        
        my $name = $definitions{$definition}{'_context'};
        
        $m_content .= sprintf qq(\tif ([contextName isEqual:@"%s"])\n\t{\n\t\treturn %s.class;\n\t}\n), $name, $class;
    }
}

$m_content .= qq(\treturn Nil;\n}\n);


$m_content .= qq(\n\@end\n);

my %special_assigns = map {$_,1} qw
(
	ServerErrorMessage
	BasketAssociate
	SaleItem
	BasketTender
	Item
	ProductSearch
	CreateCustomerEmailRequest
	ServerRequest
	ReturnItem
	Pricing
	Customer
	VerifonePayment
	YespayPayment
	Payment
	BarcodeItemNotReturnedMessage
	ServerErrorBarcodeItemNotReturnedMessage
);

foreach my $definition (sort {$a cmp $b} keys %definitions)
{
	if (exists $definitions{$definition}{'_path'})
	{
		my $name = detail_for_property($definition, undef, 'type');
		$name =~ s/~([^~]+)/'With'.ucfirst $1/ge;
		$name = ucfirst $name;		
		$h_content .= sprintf qq(\n\@interface %sInvocation : RESTInvocation\n), $name;
		
		foreach my $property (sort {ck($a) cmp ck($b)} keys %{$definitions{$definition}})
		{
			next if !is_property($property);
			
			my $storage = detail_for_property($definition, $property, 'storage');
			$h_content .= sprintf qq(\@property (nonatomic, %s) %s %s;\n), $storage, detail_for_property($definition, $property, 'usage'), $property;
		}

		$h_content .= qq(\@end\n);
		$m_content .= sprintf qq(\n\@implementation %sInvocation\n), $name;
		
		$m_content .= sprintf qq(\n- (NSString*)RESTMethod\n{\n\treturn @"%s";\n}\n), $definitions{$definition}{'_method'};
		$m_content .= sprintf qq(\n- (NSString*)RESTPath\n{\n\treturn @"%s";\n}\n), $definitions{$definition}{'_path'};
		
		
		if (length $definitions{$definition}{'_post'})
		{
			$m_content .= sprintf qq(\n- (NSString*)RESTBodyParameter\n{\n\treturn @"%s";\n}\n), $definitions{$definition}{'_post'};
		}
		
		if (detail_for_property($definition, '_return', 'type') ne 'void')
		{
			$m_content .= sprintf qq(\n- (NSString*)RESTReturnType\n{\n\treturn @"%s";\n}\n),detail_for_property($definition, '_return', 'type');
		}
        
        if (length $definitions{$definition}{'_timeout'})
        {
            $m_content .= sprintf qq(\n- (NSTimeInterval)RESTTimeoutInterval\n{\n\treturn %s;\n}\n), $definitions{$definition}{'_timeout'};
        }
				
		$m_content .= qq(\n-(NSDictionary*)RESTParameterTypes\n{\n\tNSMutableDictionary *parameterTypes = [NSMutableDictionary dictionaryWithDictionary:[super RESTParameterTypes]];\n);
				
		foreach my $property (sort {ck($a) cmp ck($b)} keys %{$definitions{$definition}})
		{
			next if !is_property($property);
					
			$m_content .= sprintf qq(\tparameterTypes[\@"%s"] = \@"%s";\n), $property, detail_for_property($definition, $property, 'type');
		}
		
		$m_content .= qq(\treturn parameterTypes;\n}\n);
		
		$m_content .= qq(\@end\n);
	}
}

sub declare_classes
{
	foreach my $definition (sort {$a cmp $b} @_)
	{
		$h_content .= sprintf qq(\n\@interface %s : %s\n), detail_for_property($definition, undef, 'type'), detail_for_property($definitions{$definition}{'_super'} || 'RESTObject', undef, 'type');
	
		foreach my $property (sort {ck($a) cmp ck($b)} keys %{$definitions{$definition}})
		{
			next if !is_property($property);
				
			my $storage = 'strong';
			if ($property eq $definitions{$definition}{'_parent'})
			{
				$storage = 'weak';
			}
			else
			{
				$storage = detail_for_property($definition, $property, 'storage');
			}
		
			if ($storage eq 'assign' && $special_assigns{$definition})
			{
				$h_content .= sprintf qq(\@property (nonatomic, strong) NSNumber *%s;\n), $property;
				$h_content .= sprintf qq(\@property (nonatomic, assign) %s %sValue;), detail_for_property($definition, $property, 'usage'), $property;
			}
			else
			{
				$h_content .= sprintf qq(\@property (nonatomic, %s) %s %s;), $storage, detail_for_property($definition, $property, 'usage'), $property;
			}
		
		
			if (exists $definitions{$definition}{$property.'Languages'})
			{
			$h_content .= sprintf qq( // Localized, use primitive%s to access _%s\n), ucfirst($property), $property;
			$h_content .= sprintf qq(\@property (nonatomic, readonly) %s primitive%s;\n), detail_for_property($definition, $property, 'usage'), ucfirst($property);
			}
			else
			{
				$h_content .= qq(\n);
			}	
		}
	
		if (exists $definitions{$definition}{'_alias'})
		{
			$h_content .= sprintf qq(\@property (nonatomic, strong) %s %s;\n), detail_for_property($definition, '_alias', 'usage'),$definitions{$definition}{'_tag'} ;
		
			my $alias = $definitions{$definition}{'_alias'};
			$alias =~ s![A-Z]+?([A-Z][a-z].*)!$1!;
		
			$h_content .= sprintf qq(+(instancetype)%sWith%s:(%s)%s;\n), lcfirst($definition), $alias, detail_for_property($definition, '_alias', 'usage'), lcfirst($alias);
		}
	
		$h_content .= qq(\@end\n);
			
		$m_content .= sprintf qq(\n\@implementation %s\n), detail_for_property($definition, undef, 'type');


		if ($special_assigns{$definition})
		{
			foreach my $property (sort {ck($a) cmp ck($b)} keys %{$definitions{$definition}})
			{
				next if !is_property($property);
				
				my $storage = detail_for_property($definition, $property, 'storage');
				if ($storage eq 'assign')
				{
					my $type = detail_for_property($definition, $property, 'usage');
					$type =~ s/^NS//g;
					$type = lc($type);
					$m_content .= sprintf qq(\n-(%s)%sValue {return [self.%s %sValue];}), detail_for_property($definition, $property, 'usage'), $property, $property, $type;
					$m_content .= sprintf qq(\n-(void)set%sValue:(%s)%sValue {self.%s = @(%sValue);}), ucfirst($property), detail_for_property($definition, $property, 'usage'), $property, $property, $property;
				}	
			}
		}

		if (exists $definitions{$definition}{'_tag'})
		{
			$m_content .= sprintf qq(\n-(NSString*)xmlTag\n{\n\treturn @"%s";\n}\n), $definitions{$definition}{'_tag'};
		}
		
		if (exists $definitions{$definition}{'_context'})
		{
			$m_content .= sprintf qq(\n+(NSString*)contextName\n{\n\treturn @"%s";\n}\n), $definitions{$definition}{'_context'};
		}
	
		if (exists $definitions{$definition}{'_alias'})
		{
			$m_content .= sprintf qq(\n-(NSString*)xmlAlias\n{\n\treturn @"%s";\n}\n\n), $definitions{$definition}{'_alias'};
		
			my $alias = $definitions{$definition}{'_alias'};
			$alias =~ s![A-Z]+?([A-Z][a-z].*)!$1!;
		
			$m_content .= sprintf qq(+(instancetype)%sWith%s:(%s)%s\n{\n\t%s object = [[%s alloc] init];\n\tobject.%s = %s;\n\treturn object;\n}\n), lcfirst($definition), $alias, detail_for_property($definition, '_alias', 'usage'), lcfirst($alias),detail_for_property($definition, undef, 'usage'), $definition, $definitions{$definition}{'_tag'}, lcfirst($alias);
		}

		$m_content .= qq(\n-(NSDictionary*)xmlAttributes\n{\n\tNSMutableDictionary *xmlAttributes = [NSMutableDictionary dictionaryWithDictionary:[super xmlAttributes]];\n);
	
		foreach my $property (sort {ck($a) cmp ck($b)} keys %{$definitions{$definition}})
		{
			next if !is_property($property);
				
			$m_content .= sprintf qq(\txmlAttributes[\@"%s"] = \@"%s";\n), $property, detail_for_property($definition, $property, 'type');
		
		}
	
		$m_content .= qq(\treturn xmlAttributes;\n}\n);
	
		$m_content .= qq(\n-(NSDictionary*)objectAttributes\n{\n\tNSMutableDictionary *objectAttributes = [NSMutableDictionary dictionaryWithDictionary:[super objectAttributes]];\n);
	
		if (exists $definitions{$definition}{'_alias'})
		{
			$m_content .= sprintf qq(\tobjectAttributes[\@"%s"] = \@"%s";\n), $definitions{$definition}{'_tag'}, $definitions{$definition}{'_tag'};
		}
		else
		{
			foreach my $property (sort {ck($a) cmp ck($b)} keys %{$definitions{$definition}})
			{
				next if !is_property($property);
		
				if (exists $definitions{$definition}{$property.'Languages'})
				{
					$m_content .= sprintf qq(\tobjectAttributes[\@"%s"] = \@"primitive%s";\n), $property, ucfirst($property);
				}
				else
				{
					$m_content .= sprintf qq(\tobjectAttributes[\@"%s"] = \@"%s";\n), $property, $property;
				}
			}
		}
	
		$m_content .= qq(\treturn objectAttributes;\n}\n);
	
		if (exists $definitions{$definition}{'_parent'})
		{
			$m_content .= sprintf qq(\n-(NSString*)parentAttribute\n{\n\treturn @"%s";\n}\n), $definitions{$definition}{'_parent'};
		}

		foreach my $property (sort {ck($a) cmp ck($b)} keys %{$definitions{$definition}})
		{
			next if !is_property($property);
			next unless exists $definitions{$definition}{$property.'Languages'};
	
			$m_content .= sprintf qq(\n-(%s)%s\n{\n\treturn [self localizedStringFromLanguages:_%sLanguages value:_%s];\n}\n), detail_for_property($definition, $property, 'usage'), $property, $property, $property;
		
			$m_content .= sprintf qq(\n-(%s)primitive%s\n{\n\treturn _%s;\n}\n), detail_for_property($definition, $property, 'usage'), ucfirst($property), $property, $property;
		}

		$m_content .= qq(\@end\n);
		declare_classes(keys %{$subclasses{$definition}});
	}
}

$h_content .= qq(\n);
$h_content .= qq(#pragma clang diagnostic push\n);
$h_content .= qq(#pragma clang diagnostic ignored "-Wobjc-property-synthesis"\n\n);

declare_classes(keys %{$subclasses{''}});

$h_content .= qq(#pragma clang diagnostic pop\n);

write_file_if_changed('RESTController/RESTController.h', $h_content);
write_file_if_changed('RESTController/RESTController.m', $m_content);

my $ini_content = "";
foreach my $definition (sort {lc $a cmp lc $b} keys %definitions)
{
	if (length $ini_content)
	{
		$ini_content .= qq(\n);
	}
	$ini_content .= sprintf "[%s]\n", $definition;
	foreach my $property (sort {ck($a) cmp ck($b)} keys %{$definitions{$definition}})
	{
		$ini_content .= sprintf "%s = %s\n", $property, $definitions{$definition}{$property};
	}
}
write_file('scripts/webservice_definitions.ini', $ini_content);

sub detail_for_property
{
	my ($definition, $property, $detail) = @_;
	
	my $type = length $property ? $definitions{$definition}{$property} : $definition;
	
	if ($property eq '_return' && $type eq '')
	{
		return 'void';
	}
	elsif (exists $type_details{$type})
	{
		if (exists $type_details{$type}{$detail})
		{
			return $type_details{$type}{$detail};
		}
		elsif ($detail ne 'storage')
		{
			my $storage = $type_details{$type}{'storage'};
			
			if ($detail eq 'type')
			{
				return $type;
			}
			elsif ($detail eq 'usage')
			{
				if ($storage eq 'strong')
				{
					return $type.'*';
				}
				else
				{
					return $type;
				}
			}
			elsif ($detail eq 'boxing')
			{
				if ($storage eq 'assign')
				{
					return "@({})";
				}
				else
				{
					return "{}";
				}
			}
		}
	}
	else
	{
die qq(./mPOS/scripts/webservice_definitions.ini:).$line_numbers{$definition}{$property}.qq(: error: type ').$type.qq(' not recognised for definiation ').$definition.qq(', property ').$property.qq('\n);
	}
}

sub write_file_if_changed
{
	my ($path, $content) = @_;
	if (!-e $path || $content ne read_file($path))
	{
		write_file($path, $content);
		return 1;
	}
	return 0;
}

sub is_property
{
	return $_[0] !~ m~^_~;
}

sub ck
{
	my ($v) = @_;
	$v =~ s!^DRAFT_!!;
	$v = lc $v;
	return $v;
}
