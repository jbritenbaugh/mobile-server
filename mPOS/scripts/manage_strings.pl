#!/usr/bin/perl -CSDL

# manage_strings
# ^^^^^^^^^^^^^^


use strict;
use Data::Dumper;
use Getopt::Long;
use File::Temp 'tempdir';
use DirHandle;
use File::Slurp;
use File::Path 'make_path';
use File::Find;
use Digest::SHA1  qw(sha1_base64);


my $languages = $ENV{'MANAGE_STRINGS_LANGUAGES'};
my $default_table;
my $root;
my $source = 1;
my $export_path = $ENV{'MANAGE_STRINGS_EXPORT'};
my $export_value = 1;
my $keep_empty_values = 1;

GetOptions
(
	"languages=s" => \$languages,
	"table=s" => \$default_table,
	"root=s" => \$root,
	"source!" => \$source,
	"export=s" => \$export_path,
	"value!" => \$export_value,
);

my %strings;

my @languages = split ',', $languages;

my $primary_language = $languages[0];
my %updates;
my %export;
my %translations;
#print Dumper(\@languages);

#exit;


foreach my $lang (@languages)
{
	my $string_path = $root.'/'.$lang.'.lproj/'.(length $default_table ? $default_table : 'Localizable').'.strings';
	my $comment = '';
	if (-e $string_path)
	{
		my $strings_content = read_file($string_path, { binmode => ':utf8' });

		while ($strings_content =~ m!(?:/\*\s*(.*?)\s*\*/)?\s*"([^"]*)"\s*=\s*"([^"]*)";!gs)
		{
			my ($comment, $key, $value) = ($1, $2, $3);
			$strings{$key}{'values'}{$lang} = $value;
# 			warn $value;
			if ($lang eq $primary_language)
			{
				if (length $comment && $comment ne $strings{$key}{'values'}{$lang})
				{
					$strings{$key}{'comment'} = $comment;
				}
			}
			
			$translations{$strings{$key}{'values'}{$primary_language}}{$strings{$key}{'comment'}}{$lang} = $value;
		}
	}
}

# die Dumper(\%strings);

my @paths;

if ($source)
{
	find(sub {if (/^[^.]+\.m$/) {push @paths, $File::Find::name}}, $root);
}
else
{
	my $table = $default_table;
	foreach my $key (keys %strings)
	{
		my $comment = $strings{$key}{'comment'};
		my $value = $strings{$key}{'values'}{$primary_language};
		if ($comment eq $key || $comment eq $value)
		{
			$comment = undef;
		}
		$export{tag($table, $key, $comment)} = [$table, $key, $value, $comment];
	}
}

# die Dumper(\@paths);

sub substitueCommand
{
	my ($pass, $tag, $key, $table, $comment) = @_;
	
	$tag = unhide($tag);
	if (exists $strings{$key})
	{
		$comment = $strings{$key}{'comment'};
		$key = $strings{$key}{'values'}{$primary_language};
	}
	
	if (exists $strings{$key})
	{
		$comment = $strings{$key}{'comment'};
		$key = $strings{$key}{'values'}{$primary_language};
	}
 	
 	if ($key eq $comment)
 	{
 		$comment = undef;
 	}
 	
 	if (0 == length $table)
 	{
 		$table = $default_table;
 	}
	
	my $updated_tag = tag($table, $key, $comment);
	
	if (length $tag && $tag ne $updated_tag)
	{
		$updates{$tag} = [$updated_tag, $key, $table, $comment];
	}
	
	if (exists $updates{$tag})
	{
		($updated_tag, $key, $table, $comment) = @{$updates{$tag}};
	}
	
	$tag = $updated_tag;
		
	if ($pass eq 'write')
	{
		$export{$tag} = [$table, undef, $key, $comment];
	}

	$tag = hide($tag);
	if	(length $table)
	{
		return sprintf q(NSLocalizedStringFromTable(/*%s*/ @"%s", %s, %s)), $tag, $key, wrap($table), wrap($comment);
	}
	else
	{
		return sprintf q(NSLocalizedString(/*%s*/ @"%s", %s)), $tag, $key, wrap($comment);
	}

}

foreach my $pass ('read', 'write')
{
	foreach my $path (sort {lc $a cmp lc $b} @paths)
	{
		my $path_content = read_file($path, { binmode => ':utf8' });
		
		$path_content =~ s!
			NSLocalizedStringFromTable
			\(
				\s*(?:\/\*(.*?)\*\/)?
				\s*(?:(?:\@"([^"]*)")|(?:nil))?\s*,
				\s*(?:(?:\@"([^"]*)")|(?:nil))?\s*,
				\s*(?:(?:\@"([^"]*)")|(?:nil))?\s*
			\)
		!substitueCommand($pass, $1, $2, $3, $4)!gex;
	
		$path_content =~ s!
			NSLocalizedString
			\(
				\s*(?:\/\*(.*?)\*\/)?
				\s*(?:(?:\@"([^"]*)")|(?:nil))?\s*,
				\s*(?:(?:\@"([^"]*)")|(?:nil))?\s*
			\)
		!substitueCommand($pass, $1, $2, $default_table, $3)!gex;

		if ($pass eq 'write')
		{
			write_file_if_changed($path, $path_content);
		}
	}
}

#  die Dumper(\%updates);

if (length $export_path)
{
	my $content = "";
	if (-e $export_path)
	{
		$content = read_file($export_path, { binmode => ':utf8' });
	}
	
	foreach my $entry (values %export)
	{
		my ($table, $key, $value, $comment) = @{$entry};
		$table = 'Localizable' if 0 == length $table;
		my $sha1 = sha1_base64($key."\0".$value."\0".$comment);
		
		if (length $key || $keep_empty_values)
		{
			$content .= sprintf qq(\t<FREEDOMOPTIONSETVALUE Path="\\\\FREEDOM\\mPosLocalizableTable[%s]\\Identifier[%s]), escape($table), escape($sha1);
			$content .= sprintf qq(\\Key[%s]"/>\n), escape($key);
		}
		if ((length $value || $keep_empty_values))
		{
			$content .= sprintf qq(\t<FREEDOMOPTIONSETVALUE Path="\\\\FREEDOM\\mPosLocalizableTable[%s]\\Identifier[%s]), escape($table), escape($sha1);
			$content .= sprintf qq(\\Value[%s]"/>\n), escape($value);
			
			$content .= sprintf qq(\t<FREEDOMOPTIONSETVALUE Path="\\\\FREEDOM\\mPosLocalizableTable[%s]\\Identifier[%s]), escape($table), escape($sha1);
			$content .= sprintf qq(\\Language[Default]\\Value[%s]"/>\n), escape($value);
		}
		if (length $comment || $keep_empty_values)
		{
			$content .= sprintf qq(\t<FREEDOMOPTIONSETVALUE Path="\\\\FREEDOM\\mPosLocalizableTable[%s]\\Identifier[%s]), escape($table), escape($sha1);
			$content .= sprintf qq(\\Comment[%s]"/>\n), escape($comment);
		}
				
# 		if (exists $translations{$value || $key}{$comment})
# 		{
# 			my $translations = $translations{$value || $key}{$comment};
# 			foreach my $lang (@languages)
# 			{
# 				if ((length $translations->{$lang} || $keep_empty_values) && $lang ne $primary_language)
# 				{
# 			$content .= sprintf qq(\t<FREEDOMOPTIONSETVALUE Path="\\\\FREEDOM\\mPosLocalizableTable[%s]\\Identifier[%s]), escape($table), escape($sha1);
# 					$content .= sprintf qq(\\Language[%s]\\Value[%s]"/>\n), escape($lang), escape($translations->{$lang});
# 				}
# 			}		
# 		}
	}
	
	$content = join "", map {(/Comment\[/?"\n<!-- -->\n\n":"").$_."\n"} sort {$a cmp $b} grep {/\t<FREEDOMOPTIONSETVALUE Path/} split "\n", $content;
	
	$content = qq(<BATCH HostBatchID="1" Type="Partial" ScheduleType="Immediate" FormatVersion="2.00.00.00">\n).
				qq(<FREEDOMOPTIONSETVALUE Action="DeleteWildcard" Path="\\\\Freedom\\mPosLocalizableTable[%]"/>\n).
				$content.
				qq(</BATCH>\n);

	write_file_if_changed($export_path, $content);
}

sub escape
{
	my ($v) = @_;
	if (defined $v)
	{
		$v =~ s/\]/&#93/g;
	}
	return $v;
}

sub write_file_if_changed
{
	my ($path, $content) = @_;
	if (!-e $path || $content ne read_file($path, { binmode => ':utf8' }))
	{
		write_file($path, { binmode => ':utf8' }, $content);
		return 1;
	}
	return 0;
}

sub hide
{
	return join '', chr(0xE0001), (map {chr(0xE0000 + $_)} unpack "C*", $_[0]), chr(0xE007F);
}

sub unhide
{
	join '', map {($_ > 0xE0001) && ($_ < 0xE007E) ? chr($_-0xE0000):''} unpack "U*", $_[0];
}

sub tag
{
	return 'x-ocg-sha1-'.sha1_base64(join "\0", @_);
}

sub wrap
{
	return length($_[0])?qq(@").$_[0].qq("):qq(nil);
}
