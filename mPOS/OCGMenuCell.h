//
//  OCGMenuCell.h
//  mPOS
//
//  Created by Antonio Strijdom on 09/01/2014.
//  Copyright (c) 2014 Clarity. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RESTController.h"

@interface OCGMenuCell : UICollectionViewCell

@property (strong, nonatomic) SoftKey *menuItem;
@property (strong, nonatomic) UIFont *font;
@property (strong, nonatomic) UIColor *foregroundColor;
@property (strong, nonatomic) UIColor *backgroundColor;

@end
