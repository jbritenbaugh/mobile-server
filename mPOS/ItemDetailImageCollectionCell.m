//
//  ItemDetailImageCollectionCell.m
//  mPOS
//
//  Created by Antonio Strijdom on 22/05/2013.
//  Copyright (c) 2013 Clarity. All rights reserved.
//

#import "ItemDetailImageCollectionCell.h"

@implementation ItemDetailImageCollectionCell

#pragma mark - UITableViewCell

- (id) initWithStyle:(UITableViewCellStyle)style
     reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style
                reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void) setSelected:(BOOL)selected
            animated:(BOOL)animated
{
    [super setSelected:selected
              animated:animated];

    // Configure the view for the selected state
}

@end
