//
//  CustomerSearchViewController.h
//  mPOS
//
//  Created by Antonio Strijdom on 25/09/2013.
//  Copyright (c) 2013 Clarity. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomerViewController.h"

/** @file CustomerViewController.h */

/** @brief Table view controller for searching for customers.
 */
@interface CustomerSearchViewController : UITableViewController

/** @property delegate
 *  @brief The delegate of this view.
 *  @see CustomerViewControllerDelegate
 */
@property (nonatomic, weak) id<CustomerViewControllerDelegate> delegate;
/** @property allowSearch
 *  @brief Set to YES to display the search bar.
 */
@property (assign, nonatomic) BOOL allowSearch;
/** @property allowAdd
 *  @brief Set to YES to display the add button.
 */
@property (assign, nonatomic) BOOL allowAdd;

/** @property addButton
 *  @brief The add button for adding customers.
 */
@property (strong, nonatomic) UIBarButtonItem *addButton;

@end
