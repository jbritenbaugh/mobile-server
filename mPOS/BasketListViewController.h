//
//  BasketListViewController.h
//  mPOS
//
//  Created by John Scott on 07/09/2012.
//  Copyright (c) 2012 Clarity. All rights reserved.
//

#import <UIKit/UIKit.h>

@class BasketListViewController;
@class MPOSBasket;

/** @brief Protocol that the owner delegate for this view controller needs to implement.
 */
@protocol BasketListViewControllerDelegate <NSObject>
@required

/** @brief Called whenever the user selected a basket that should be recalled.
 *  @param basketId The identifier of the basket to be recalled.
 */
- (void) basketListViewController:(BasketListViewController *)basketListController recallBasket:(BasketDTO*)basket;

/** @brief Called whenever the user selected a basket that should be recalled.
 *  @param basketBarcode The barcode of the basket to be recalled.
 */
- (void) basketListViewController:(BasketListViewController *)basketListController recallBarcode:(NSString *)basketBarcode;

/** @brief Called whenever the user chosed to dismiss the view without recalling a basket.
 *  @param viewController A reference to the view controller to be dismissed.
 */
- (void) dismissBasketListViewController:(BasketListViewController *)basketListViewController;

@end

/** @brief View controller that allows the user to select and recall a suspended basket.
 */
@interface BasketListViewController : UICollectionViewController

/** @brief A reference to the owner delegate which will receive notifications from this view controller.
 */
@property (strong, nonatomic) id<BasketListViewControllerDelegate> delegate;

@end
