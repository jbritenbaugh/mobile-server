#import "POSPermission.h"

POSPermission POSPermissionForString(NSString *string) {
  if (string == nil) return POSPermissionNull;
  if ([string isEqualToString:@"ADD_ITEM_WITH_PRICE"]) return POSPermissionAddItemWithPrice;
  if ([string isEqualToString:@"ADD_RETURN_ITEM"]) return POSPermissionAddReturnItem;
  if ([string isEqualToString:@"ADD_RETURN_ITEM_INTERNET"]) return POSPermissionAddReturnItemInternet;
  if ([string isEqualToString:@"ADD_RETURN_ITEM_LINKED"]) return POSPermissionAddReturnItemLinked;
  if ([string isEqualToString:@"ADD_RETURN_ITEM_NORECEIPT"]) return POSPermissionAddReturnItemNoreceipt;
  if ([string isEqualToString:@"ADD_RETURN_ITEM_UNLINKED"]) return POSPermissionAddReturnItemUnlinked;
  if ([string isEqualToString:@"ADD_TENDER"]) return POSPermissionAddTender;
  if ([string isEqualToString:@"ASSIGN_AFFILIATION"]) return POSPermissionAssignAffiliation;
  if ([string isEqualToString:@"ASSIGN_CUSTOMERCARD"]) return POSPermissionAssignCustomercard;
  if ([string isEqualToString:@"ASSIGN_EMAIL"]) return POSPermissionAssignEmail;
  if ([string isEqualToString:@"ASSOCIATE_DEVICE_STATION_TILLID"]) return POSPermissionAssociateDeviceStationTillid;
  if ([string isEqualToString:@"CANCEL_ITEM"]) return POSPermissionCancelItem;
  if ([string isEqualToString:@"CANCEL_SALE"]) return POSPermissionCancelSale;
  if ([string isEqualToString:@"CHANGE_DEVICE_STATION_STATUS"]) return POSPermissionChangeDeviceStationStatus;
  if ([string isEqualToString:@"DEVICE_STATION_STATUS"]) return POSPermissionDeviceStationStatus;
  if ([string isEqualToString:@"DISASSOCIATE_DEVICE_STATION_TILLID"]) return POSPermissionDisassociateDeviceStationTillid;
  if ([string isEqualToString:@"FINISH_SALE"]) return POSPermissionFinishSale;
  if ([string isEqualToString:@"ITEM_DETAILS"]) return POSPermissionItemDetails;
  if ([string isEqualToString:@"ITEM_SEARCH"]) return POSPermissionItemSearch;
  if ([string isEqualToString:@"LOOKUP_CUSTOMER"]) return POSPermissionLookupCustomer;
  if ([string isEqualToString:@"MODIFY_CHECKOUT"]) return POSPermissionModifyCheckout;
  if ([string isEqualToString:@"MODIFY_ITEM_DISCOUNT"]) return POSPermissionModifyItemDiscount;
  if ([string isEqualToString:@"MODIFY_ITEM_PRICE"]) return POSPermissionModifyItemPrice;
  if ([string isEqualToString:@"MODIFY_ITEM_QUANTITY"]) return POSPermissionModifyItemQuantity;
  if ([string isEqualToString:@"MODIFY_SALESPERSON"]) return POSPermissionModifySalesperson;
  if ([string isEqualToString:@"MODIFY_SALE_DISCOUNT"]) return POSPermissionModifySaleDiscount;
  if ([string isEqualToString:@"MODIFY_SETTINGS"]) return POSPermissionModifySettings;
  if ([string isEqualToString:@"OPEN_CASH_DRAWER"]) return POSPermissionOpenCashDrawer;
  if ([string isEqualToString:@"PRINT_LAST_TRANSACTION"]) return POSPermissionPrintLastTransaction;
  if ([string isEqualToString:@"REPRINT_RECEIPT"]) return POSPermissionReprintReceipt;
  if ([string isEqualToString:@"SUSPEND_RESUME_SALE"]) return POSPermissionSuspendResumeSale;
  if ([string isEqualToString:@"TAX_FREE_SALE"]) return POSPermissionTaxFreeSale;
  if ([string isEqualToString:@"TOGGLE_TRAINING_MODE"]) return POSPermissionToggleTrainingMode;
  if ([string isEqualToString:@"UNLINK_DEVICE_STATION"]) return POSPermissionUnlinkDeviceStation;
  return POSPermissionUnknown;
}
NSString* NSStringForPOSPermission(POSPermission aPOSPermission) {
  switch(aPOSPermission) {
    case POSPermissionNull: return nil;
    case POSPermissionAddItemWithPrice: return @"ADD_ITEM_WITH_PRICE";
    case POSPermissionAddReturnItem: return @"ADD_RETURN_ITEM";
    case POSPermissionAddReturnItemInternet: return @"ADD_RETURN_ITEM_INTERNET";
    case POSPermissionAddReturnItemLinked: return @"ADD_RETURN_ITEM_LINKED";
    case POSPermissionAddReturnItemNoreceipt: return @"ADD_RETURN_ITEM_NORECEIPT";
    case POSPermissionAddReturnItemUnlinked: return @"ADD_RETURN_ITEM_UNLINKED";
    case POSPermissionAddTender: return @"ADD_TENDER";
    case POSPermissionAssignAffiliation: return @"ASSIGN_AFFILIATION";
    case POSPermissionAssignCustomercard: return @"ASSIGN_CUSTOMERCARD";
    case POSPermissionAssignEmail: return @"ASSIGN_EMAIL";
    case POSPermissionAssociateDeviceStationTillid: return @"ASSOCIATE_DEVICE_STATION_TILLID";
    case POSPermissionCancelItem: return @"CANCEL_ITEM";
    case POSPermissionCancelSale: return @"CANCEL_SALE";
    case POSPermissionChangeDeviceStationStatus: return @"CHANGE_DEVICE_STATION_STATUS";
    case POSPermissionDeviceStationStatus: return @"DEVICE_STATION_STATUS";
    case POSPermissionDisassociateDeviceStationTillid: return @"DISASSOCIATE_DEVICE_STATION_TILLID";
    case POSPermissionFinishSale: return @"FINISH_SALE";
    case POSPermissionItemDetails: return @"ITEM_DETAILS";
    case POSPermissionItemSearch: return @"ITEM_SEARCH";
    case POSPermissionLookupCustomer: return @"LOOKUP_CUSTOMER";
    case POSPermissionModifyCheckout: return @"MODIFY_CHECKOUT";
    case POSPermissionModifyItemDiscount: return @"MODIFY_ITEM_DISCOUNT";
    case POSPermissionModifyItemPrice: return @"MODIFY_ITEM_PRICE";
    case POSPermissionModifyItemQuantity: return @"MODIFY_ITEM_QUANTITY";
    case POSPermissionModifySalesperson: return @"MODIFY_SALESPERSON";
    case POSPermissionModifySaleDiscount: return @"MODIFY_SALE_DISCOUNT";
    case POSPermissionModifySettings: return @"MODIFY_SETTINGS";
    case POSPermissionOpenCashDrawer: return @"OPEN_CASH_DRAWER";
    case POSPermissionPrintLastTransaction: return @"PRINT_LAST_TRANSACTION";
    case POSPermissionReprintReceipt: return @"REPRINT_RECEIPT";
    case POSPermissionSuspendResumeSale: return @"SUSPEND_RESUME_SALE";
    case POSPermissionTaxFreeSale: return @"TAX_FREE_SALE";
    case POSPermissionToggleTrainingMode: return @"TOGGLE_TRAINING_MODE";
    case POSPermissionUnlinkDeviceStation: return @"UNLINK_DEVICE_STATION";
    default: return nil;
  }
}
