//
//  ItemDiscountTableViewCell.h
//  mPOS
//
//  Created by Meik Schuetz on 20/08/2012.
//  Copyright (c) 2012 Clarity. All rights reserved.
//

#import <UIKit/UIKit.h>

/** @file ItemDiscountTableViewCell */

/** @brief Class that implements all layout logic for the item discount cell.
 */
@interface ItemDiscountTableViewCell : UITableViewCell

/** @brief A reference to the discount object.
 */
@property (strong, nonatomic) id discount;

/** @brief A reference to the label that shows the discounts description.
 */
@property (weak, nonatomic) IBOutlet UILabel *discountDescription;

/** @brief A reference to the label that shows the discount value.
 */
@property (weak, nonatomic) IBOutlet UILabel *discountValue;

@end
