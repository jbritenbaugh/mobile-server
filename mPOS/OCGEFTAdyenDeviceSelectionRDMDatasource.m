//
//  OCGEFTAdyenDeviceSelectionRDMDatasource.m
//  mPOS
//
//  Created by Antonio Strijdom on 16/03/2015.
//  Copyright (c) 2015 Clarity. All rights reserved.
//

#import "OCGEFTAdyenDeviceSelectionRDMDatasource.h"
#import "CRSLocationController.h"
#import "ServerErrorHandler.h"

@interface OCGEFTAdyenDeviceSelectionRDMDatasource ()
- (NSArray *) mergeNewDevices:(NSArray *)newDevices;
@end

@implementation OCGEFTAdyenDeviceSelectionRDMDatasource

#pragma mark - Private

- (NSArray *) mergeNewDevices:(NSArray *)newDevices
{
    NSArray *result = nil;
    
    // make a copy of the adyen device list
    ADYDeviceRegistry *deviceRegistry = [[Adyen sharedInstance] deviceRegistry];
    if (deviceRegistry) {
        result = [deviceRegistry.devices copy];
    }
    
    // remove missing devices
    for (ADYDevice *device in result) {
        BOOL found = NO;
        for (DeviceStation *station in newDevices) {
            if (station.peripherals.count > 0) {
                DeviceStationPeripheral *peripheral = station.peripherals[0];
                if ([peripheral.peripheralId isEqualToString:device.hostname]) {
                    found = YES;
                    break;
                }
            }
        }
        if (!found) {
            [deviceRegistry removeDeviceWithHostname:device.hostname];
        }
    }
    
    // add new devices
    for (DeviceStation *station in newDevices) {
        BOOL found = NO;
        if (station.peripherals.count > 0) {
            DeviceStationPeripheral *peripheral = station.peripherals[0];
            for (ADYDevice *device in result) {
                if ([peripheral.peripheralId isEqualToString:device.hostname]) {
                    found = YES;
                    break;
                }
            }
            if (!found) {
                [deviceRegistry addDeviceWithHostname:peripheral.peripheralId];
            }
        }
    }
    
    // update device list
    result = [deviceRegistry.devices copy];
    
    return result;
}

#pragma mark - DeviceSelectionViewControllerDataSource

- (BOOL) deviceSelectionController:(DeviceSelectionViewController *)deviceSelectionController
                         getStatus:(NSString **)status
                          ofDevice:(id)device
{
    BOOL result = NO;
    
    ADYDevice *adyenDevice = (ADYDevice *)device;
    switch (adyenDevice.status) {
        case ADYDeviceStatusInitialized: {
            *status = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁋󠁶󠁇󠁪󠁖󠀲󠁬󠀯󠁐󠁩󠁋󠁋󠁄󠁓󠁏󠁶󠁈󠁯󠁥󠁵󠁚󠀴󠁔󠁈󠁉󠁍󠀸󠁿*/ @"Ready", nil);
            result = YES;
            break;
        }
        case ADYDeviceStatusInitializing: {
            *status = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠀶󠁢󠁢󠁑󠀵󠁺󠁒󠁦󠁂󠁷󠁵󠀷󠀰󠀯󠀱󠁬󠁣󠀫󠁯󠀲󠀵󠁆󠁘󠁮󠀰󠀷󠁣󠁿*/ @"Initialising", nil);
            result = NO;
            break;
        }
        case ADYDeviceStatusNotBoarded: {
            *status = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁬󠁙󠁃󠁪󠁱󠁈󠁕󠁁󠁡󠁘󠁆󠁯󠀫󠀱󠁁󠁖󠁗󠁨󠁡󠁊󠁂󠁒󠁱󠁐󠁎󠁫󠁉󠁿*/ @"Awaiting boarding", nil);
            result = YES;
            break;
        }
        case ADYDeviceStatusStopped:
            *status = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠀵󠁭󠁵󠁏󠁷󠁃󠁩󠁰󠁴󠀱󠁳󠀱󠁷󠁧󠁯󠀴󠁯󠁚󠁺󠁒󠁓󠁒󠁯󠁨󠁵󠁩󠁉󠁿*/ @"Stopped", nil);
            break;
        case ADYDeviceStatusGone:
            *status = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁎󠁵󠁙󠁈󠁆󠀳󠁉󠁃󠀲󠁣󠁫󠁙󠁲󠁣󠁢󠁔󠀫󠁚󠁐󠁹󠁮󠁥󠁑󠁌󠁲󠁆󠀸󠁿*/ @"Disconnected", nil);
            break;
        case ADYDeviceStatusError:
            *status = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁫󠀵󠁉󠁤󠁘󠁖󠁏󠁁󠁆󠀷󠁬󠀯󠁯󠁇󠀲󠀳󠁵󠁢󠀫󠁘󠁦󠀯󠁩󠁁󠁦󠁒󠁑󠁿*/ @"Error", nil);
            break;
        default:
            break;
    }
    
    return result;
}

- (NSString *) deviceSelectionController:(DeviceSelectionViewController *)deviceSelectionController
                            nameOfDevice:(id)device
{
    ADYDevice *adyenDevice = (ADYDevice *)device;
    NSString *result = adyenDevice.name;
    
    for (DeviceStation *station in self.deviceStations) {
        if (station.peripherals.count > 0) {
            DeviceStationPeripheral *peripheral = station.peripherals[0];
            if ([peripheral.peripheralId isEqualToString:adyenDevice.hostname]) {
                result = station.deviceStationId;
                break;
            }
        }
    }
    
    return result;
}

- (void) deviceSelectionController:(DeviceSelectionViewController *)deviceSelectionController
            refreshDevicesComplete:(DeviceRefreshBlock)complete
                    currentDevices:(NSArray *)currentDevices
{
    [super deviceSelectionController:deviceSelectionController
              refreshDevicesComplete:^(NSArray *devices) {
                  // merge the devices
                  devices = [self mergeNewDevices:devices];
                  // update notifications
                  // remove old
                  for (id device in currentDevices) {
                      [device removeObserver:deviceSelectionController
                                  forKeyPath:@"status"
                                     context:NULL];
                  }
                  // add new
                  for (id device in devices) {
                      [device addObserver:deviceSelectionController
                               forKeyPath:@"status"
                                  options:NSKeyValueObservingOptionNew
                                  context:NULL];
                  }
                  // done
                  complete(devices);
              }
                      currentDevices:currentDevices];
}

#pragma mark - Methods

- (void) deviceForBarcode:(NSString *)barcode
                 complete:(void (^)(ADYDevice *adyenDevice))complete
{
    ocg_async_background_overlay(^{
        [RESTController.sharedInstance getDeviceStationsWithDeviceType:self.deviceType
                                                               storeId:[CRSLocationController getLocationKey]
                                                              complete:^(NSArray *result, NSError *error, ServerError *serverError)
         {
             ocg_sync_main(^{
                 if ((error == nil) && (serverError == nil)) {
                     DeviceStation *deviceStation = nil;
                     for (DeviceStation *station in result) {
                         if ([station.deviceStationId isEqualToString:barcode]) {
                             deviceStation = station;
                             break;
                         }
                     }
                     if (deviceStation) {
                         ADYDevice *adyenDevice = nil;
                         if (deviceStation.peripherals.count > 0) {
                             DeviceStationPeripheral *peripheral = deviceStation.peripherals[0];
                             ADYDeviceRegistry *deviceRegistry = [[Adyen sharedInstance] deviceRegistry];
                             NSArray *devices = [deviceRegistry.devices copy];
                             for (ADYDevice *device in devices) {
                                 if ([device.hostname isEqualToString:peripheral.peripheralId]) {
                                     adyenDevice = device;
                                     break;
                                 }
                             }
                             if (adyenDevice == nil) {
                                 [deviceRegistry addDeviceWithHostname:peripheral.peripheralId];
                                 devices = [deviceRegistry.devices copy];
                                 for (ADYDevice *device in devices) {
                                     if ([device.hostname isEqualToString:peripheral.peripheralId]) {
                                         adyenDevice = device;
                                         break;
                                     }
                                 }
                             }
                             if (adyenDevice) {
                                 if (complete) complete(adyenDevice);
                             } else {
                                 UIAlertController *alert =
                                 [UIAlertController alertControllerWithTitle:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁫󠀵󠁉󠁤󠁘󠁖󠁏󠁁󠁆󠀷󠁬󠀯󠁯󠁇󠀲󠀳󠁵󠁢󠀫󠁘󠁦󠀯󠁩󠁁󠁦󠁒󠁑󠁿*/ @"Error", nil)
                                                                     message:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁵󠁺󠁣󠁡󠁭󠁲󠁌󠁂󠁆󠁵󠁵󠁆󠁶󠁋󠁡󠁵󠀫󠁸󠁕󠁫󠀫󠁸󠁭󠁮󠁅󠁮󠀰󠁿*/ @"No device found for barcode", nil)
                                                              preferredStyle:UIAlertControllerStyleAlert];
                                 [alert addAction:[UIAlertAction actionWithTitle:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁄󠁴󠁍󠁉󠁣󠀫󠁺󠁔󠁗󠁦󠁹󠁡󠁯󠀷󠁨󠁔󠁩󠀸󠁯󠀫󠁨󠁘󠀹󠁱󠁌󠀶󠀸󠁿*/ @"OK", nil)
                                                                           style:UIAlertActionStyleCancel
                                                                         handler:nil]];
                                 [ServerErrorHandler topMostControllerComplete:^(UIViewController *viewController) {
                                     [viewController presentViewController:alert
                                                                  animated:YES
                                                                completion:nil];
                                 }];
                             }
                         } else {
                             UIAlertController *alert =
                             [UIAlertController alertControllerWithTitle:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁫󠀵󠁉󠁤󠁘󠁖󠁏󠁁󠁆󠀷󠁬󠀯󠁯󠁇󠀲󠀳󠁵󠁢󠀫󠁘󠁦󠀯󠁩󠁁󠁦󠁒󠁑󠁿*/ @"Error", nil)
                                                                 message:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁵󠁺󠁣󠁡󠁭󠁲󠁌󠁂󠁆󠁵󠁵󠁆󠁶󠁋󠁡󠁵󠀫󠁸󠁕󠁫󠀫󠁸󠁭󠁮󠁅󠁮󠀰󠁿*/ @"No device found for barcode", nil)
                                                          preferredStyle:UIAlertControllerStyleAlert];
                             [alert addAction:[UIAlertAction actionWithTitle:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁄󠁴󠁍󠁉󠁣󠀫󠁺󠁔󠁗󠁦󠁹󠁡󠁯󠀷󠁨󠁔󠁩󠀸󠁯󠀫󠁨󠁘󠀹󠁱󠁌󠀶󠀸󠁿*/ @"OK", nil)
                                                                       style:UIAlertActionStyleCancel
                                                                     handler:nil]];
                             [ServerErrorHandler topMostControllerComplete:^(UIViewController *viewController) {
                                 [viewController presentViewController:alert
                                                              animated:YES
                                                            completion:nil];
                             }];
                         }
                     } else {
                         UIAlertController *alert =
                         [UIAlertController alertControllerWithTitle:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁫󠀵󠁉󠁤󠁘󠁖󠁏󠁁󠁆󠀷󠁬󠀯󠁯󠁇󠀲󠀳󠁵󠁢󠀫󠁘󠁦󠀯󠁩󠁁󠁦󠁒󠁑󠁿*/ @"Error", nil)
                                                             message:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁵󠁺󠁣󠁡󠁭󠁲󠁌󠁂󠁆󠁵󠁵󠁆󠁶󠁋󠁡󠁵󠀫󠁸󠁕󠁫󠀫󠁸󠁭󠁮󠁅󠁮󠀰󠁿*/ @"No device found for barcode", nil)
                                                      preferredStyle:UIAlertControllerStyleAlert];
                         [alert addAction:[UIAlertAction actionWithTitle:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁄󠁴󠁍󠁉󠁣󠀫󠁺󠁔󠁗󠁦󠁹󠁡󠁯󠀷󠁨󠁔󠁩󠀸󠁯󠀫󠁨󠁘󠀹󠁱󠁌󠀶󠀸󠁿*/ @"OK", nil)
                                                                   style:UIAlertActionStyleCancel
                                                                 handler:nil]];
                         [ServerErrorHandler topMostControllerComplete:^(UIViewController *viewController) {
                             [viewController presentViewController:alert
                                                          animated:YES
                                                        completion:nil];
                         }];
                     }
                 } else {
                     [ServerErrorHandler.sharedInstance handleServerError:serverError
                                                           transportError:error
                                                              withContext:nil
                                                             dismissBlock:nil
                                                              repeatBlock:nil];
                 }
             });
         }];
    });
}

@end
