//
//  BasketController.h
//  mPOS
//
//  Created by Meik Schuetz on 09/08/2012.
//  Copyright (c) 2012 Clarity. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "POSPermission.h"
#import "RESTController.h"
#import "BasketDTO+MPOSExtensions.h"
#import "ServerError+MPOSExtensions.h"
#import "OCGSettingsController.h"
#import "RESTObject+MPOSExtensions.h"
#import "SoftKey+MPOSExtensions.h"

@class BasketDTO;
@class BasketItem;
@class ServerError;
@class PrinterQueueController;
@class Credentials;

#pragma mark - Notifications

#pragma mark Configuration
/** @brief Posted when the configuration changes */
extern NSString * const kBasketControllerConfigurationDidChangeNotification;

#pragma mark Active basket
/** @brief Posted when the active basket changes */
extern NSString * const kBasketControllerActiveBasketChangeNotification;
extern NSString * const kBasketControllerTaxFreeChangeNotification;

/** @brief Posted when the active basket sync error occurs */
extern NSString * const kBasketControllerActiveBasketErrorNotification;
/** @brief Key in the user info that contains the actual MPOSError that was raised. */
extern NSString * const kBasketControllerActiveBasketErrorKey;

#pragma mark Basket
/** @brief Posted when the basket property changes. */
extern NSString *const kBasketControllerBasketChangeNotification;
/** @brief Posted when the basket completes.
 *  @discussion A basket is considered 'complete' if it is paid off, suspended or cancelled.
 */
extern NSString *const kBasketControllerBasketCompleteNotification;
/** @brief Key in the user info that contains the reason the basket completed.
 */
extern NSString *const kBasketControllerBasketCompleteResultKey;
extern NSString *const kBasketControllerBasketCompleteResultCompleted;
extern NSString *const kBasketControllerBasketCompleteResultSuspended;
extern NSString *const kBasketControllerBasketCompleteResultCancelled;

#pragma mark Basket Item
/** @brief Posted when a basket item changes. */
extern NSString * const kBasketControllerBasketItemChangeNotification;
/** @brief Posted when a basket has been added . */
extern NSString * const kBasketControllerBasketItemAddedNotification;
/** @brief Posted when an item is being added. */
extern NSString * const kBasketControllerBasketAddItemNotification;
/** @brief Posted when a basket item add error occurs. */
extern NSString * const kBasketControllerBasketAddItemErrorNotification;
/** @brief Key in the user info that contains the actual MPOSError that was raised. */
extern NSString * const kBasketControllerBasketItemErrorKey;
/** @brief Posted when a customer affiliation is added to the basket. */
extern NSString *const kBasketControllerCustomerAffiliationAddedNotification;
/** @brief Posted when a customer card is added to the basket. */
extern NSString *const kBasketControllerCustomerCardAddedNotification;

#pragma mark Checkout basket
/** @brief Posted when the basket is checked out. */
extern NSString * const kBasketControllerBasketCheckedOutChangeNotification;
/** @brief Posted when the basket checked out errors. */
extern NSString * const kBasketControllerBasketCheckedOutErrorNotification;

#pragma mark Discounts
/** @brief Posted when a discount (reward) changes. */
extern NSString * const kBasketControllerDiscountChangeNotification;

#pragma mark Payments
/** @brief Posted when a payment changes.
 */
extern NSString * const kBasketControllerPaymentChangeNotification;

#pragma mark Customer
/** @brief Posted when a customer is created. */
extern NSString * const kBasketControllerCustomerCreatedNotification;
/** @brief Posted when the assigned customer changes. */
extern NSString * const kBasketControllerCustomerChangedNotification;

#pragma mark User
/** @brief Posted when a user logs on. */
extern NSString * const kBasketControllerUserLoggedOnNotification;
/** @brief Posted when the user is signed out on the server.
 */
extern NSString * const kBasketControllerHasLoggedOutNotification;

#pragma mark Training mode
/** @brief Posted when training mode is toggled on or off. */
extern NSString * const kBasketControllerTrainingModeToggledNotification;
/** @brief Posted when there is an error toggling training mode. */
extern NSString * const kBasketControllerTrainingModeErrorNotification;
/** @brief Key in the user info that contains the actual NSError that was raised. */
extern NSString * const kBasketControllerTrainingModeErrorKey;
/** @brief Key in the user info that contains the actual ServerError that was raised. */
extern NSString * const kBasketControllerTrainingModeServerErrorKey;

#pragma mark General
/** @brief Posted when the activity status changes. */
extern NSString * const kBasketControllerActivityChangeNotification;
/** @brief Posted when a generic basket error occurs. */
extern NSString * const kBasketControllerGeneralErrorNotification;
/** @brief Key in the user info that contains the actual MPOSError that was raised. */
extern NSString * const kBasketControllerGeneralErrorKey;
/** @brief Posted when the current device id is invalidated. */
extern NSString * const kBasketControllerDeviceNotFoundNotification;
/** @brief Posted when an overridable error occurs. */
extern NSString * const kBasketControllerOverrideErrorNotification;

#pragma mark Product search
/** @brief Posted when a product search completed. */
extern NSString * const kBasketControllerProductSearchCompleteNotification;
/** @brief Posted when a product search fails. */
extern NSString * const kBasketControllerProductSearchErrorNotification;
/** @brief Key in the user info that containst the original search string. */
extern NSString * const kBasketControllerProductSearchRequestKey;
/** @brief Key in the user info that contains the actual NSOrderedSet of products matching the search criteria. */
extern NSString * const kBasketControllerProductSearchResultsKey;
/** @brief Key in the user info that containst the reason for the product search.
 *  @see ProductSearchReason.
 */
extern NSString * const kBasketControllerProductSearchReasonKey;
/** @brief Posted when item availability is loaded. */
extern NSString * const kBasketControllerItemAvailabilityGetCompleteNotification;
/** @brief Key in the user info that contains the actual NSOrderedSet of item availability data. */
extern NSString * const kBasketControllerItemAvailabilityResultsKey;

#pragma mark Control transactions
/** @brief Posted when a reprint error occurs. */
extern NSString *const kBasketControllerControlErrorNotification;
/** @brief Key in the user info that contains the block reference of the block to execute on retry.
 *  @see BasketControllerControlRetryBlockType
 */
extern NSString *const kBasketControllerControlRetryBlockKey;
extern NSString *const kBasketControllerRetryBlockKey;
/** @brief Key in the user info that contains a BOOL that indicates whether the user can cancel.
 */
extern NSString *const kBasketControllerControlCancelBlockKey;

extern NSString *const kBasketControllerKickedNotification;


#pragma mark - Block types

/** @brief Block type used by control transaction error notifications.
 */
typedef void(^BasketControllerControlRetryBlockType)(NSString *deviceStationId, NSUInteger errorCode);

#pragma mark - Enums

/** @enum PaymentCardCaptureMethod
 *  @brief Enumerates the gift card capture methods.
 */
NS_ENUM(NSInteger, PaymentCardCaptureMethod) {
    /** @brief Capture giftcard via card swipe.
     */
    PaymentCardCaptureMethodSwipe = 0,
    /** @brief Capture giftcard via barcode scan.
     */
    PaymentCardCaptureMethodScan = 1
};

/** @enum ProductSearchReason
 *  @brief Enumerates the product search reasons.
 */
NS_ENUM(NSInteger, ProductSearchReason) {
    ProductSearchReasonItemDetails = 0,
    ProductSearchReasonReturnItem = 1
};

@interface BasketController : NSObject <OCGSettingsControllerDelegate>

#pragma mark - Properties

/** @property hasConfiguration
 *  @brief Flag that indicates whether a configuration has been provided by the server.
 */
@property (readonly, nonatomic) BOOL hasConfiguration;

/** @property basket
 *  @brief A reference to the currently active basket.
 *  @note Changing this value sends a kBasketControllerBasketChangeNotification notification.
 */
@property (strong, nonatomic) BasketDTO *basket;

/** @property basketTotal
 *  @brief Total value if the current basket.
 */
@property (readonly, nonatomic) NSNumber *basketTotal;

/** @property canAssignCustomerToBasket
 *  @brief Flag that indicates if a customer can be associated with the active basket.
 */
@property (readonly, nonatomic) BOOL canAssignCustomerToBasket;

/** @property canCheckOut
 *  @brief Flag that indicates if the current basket can be checked out.
 */
@property (readonly, nonatomic) BOOL canCheckOut;

/** @property canClearBasket
 *  @brief Flag that indicates if the current basket can be cleared.
 */
@property (readonly, nonatomic) BOOL canClearBasket;

/** @property canDiscountBasket
 *  @brief Flag that indicates if the user can apply a basket level discount.
 */
@property (readonly, nonatomic) BOOL canDiscountBasket;

/** @property canDisassociateDrawerInsert
 *  @brief Flag that indicates if the user can disassociate a till drawer insert.
 */
@property (readonly, nonatomic) BOOL canDisassociateDrawerInsert;

/** @property canEdit
 *  @brief Flag that indicates if the current basket can be edited.
 */
@property (readonly, nonatomic) BOOL canEdit;

/** @property isActive
 *  @brief Flag that indicates if the current basket is in progress.
 */
@property (readonly, nonatomic) BOOL isActive;

/** @property canLogout
 *  @brief Flag that indicates whether the user can log out.
 */
@property (readonly, nonatomic) BOOL canLogout;

/** @property canRecallBaskets
 *  @brief Flag that indicates if the user can recall suspended baskets
 */
@property (readonly, nonatomic) BOOL canRecallBaskets;

/** @brief Flag that indicates if the user can reload the configuration options
 */
@property (readonly, nonatomic) BOOL canReloadOptions;

/** @property canReprintReceipts
 *  @brief Flag that indicates if the user can reprint receipts.
 */
@property (readonly, nonatomic) BOOL canReprintReceipts;

/** @property canScanToBasket
 *  @brief Flag that indicates if the user is allowed to scan new items to the basket.
 */
@property (readonly, nonatomic) BOOL canScanToBasket;

/** @property canSuspendBasket
 *  @brief Flag that indicates if the current basket can be suspended.
 */
@property (readonly, nonatomic) BOOL canSuspendBasket;

/** @property externalAuthEnabled
 *  @brief Whether or not mPOS uses an external sign on method.
 */
@property (readonly, nonatomic) BOOL externalAuthEnabled;

/** @property externalAuthURL
 *  @brief The URL mPOS uses for the external sign on method.
 */
@property (readonly, nonatomic) NSURL *externalAuthURL;

/** @property hasCustomerAssociated
 *  @brief Flag that indicates if a customer has been associated with the active basket.
 */
@property (readonly, nonatomic) BOOL hasCustomerAssociated;

/** @property isAddingBasketItem
 *  @brief Flag that indicates that we're currently adding an item to the active basket.
 *  @note Changing this property sends a kBasketControllerBasketAddItemNotification notification.
 */
@property (assign, nonatomic) BOOL isAddingBasketItem;

/** @property isCheckedOut
 *  @brief Flag that indicates if the current basket is checked out.
 */
@property (readonly, nonatomic) BOOL isCheckedOut;

/** @property isPrinting
 *  @brief Flag that indicates if the current basket is printing.
 */
@property (readonly, nonatomic) BOOL isPrinting;

/** @property isFinished
 *  @brief Flag that indicates if the current basket is finished.
 */
@property (readonly, nonatomic) BOOL isFinished;

/** @property isGiftReceiptAvailable
 *  @brief Flag indicating if a gift receipt is available
 */
@property (readonly, nonatomic) BOOL isGiftReceiptAvailable;

/** @property isGiftReceiptRequired
 *  @brief Flag indicating if a gift receipt is required
 */
@property (readonly, nonatomic) BOOL isGiftReceiptRequired;

/** @property isInTrainingMode
 *  @brief Flag indicating if we are in training mode.
 */
@property (readonly, nonatomic) BOOL isInTrainingMode;

/** @property isReturnItem
 *  @brief Item price required flag
 */
@property (assign, nonatomic) BOOL isReturnItem;

/** @property isShowingActivity
 *  @brief Flag that determines if a activity indicator should be shown.
 *  @note Changing this value sends a kBasketControllerActivityChangeNotification notification.
 */
//@property (assign, nonatomic) BOOL isShowingActivity;

/** @property isTendered
 *  @brief Flag that indicates if the current basket is tendered.
 */
@property (readonly, nonatomic) BOOL isTendered;

/** @property isPartiallyTendered
 *  @brief Flag that indicates if the current basket is partially tendered.
 */
@property (readonly, nonatomic) BOOL isPartiallyTendered;

/** @property printQueue
 *  @brief The printer controller that manages the print job queue.
 */
@property (nonatomic, readonly) PrinterQueueController *printQueue;

/** @property promptForBasketDescription
 *  @brief Specifies whether to prompt the user for a description when suspending a basket.
 */
@property (nonatomic, readonly) BOOL promptForBasketDescription;

/** @property signOnMethodProperties
 *  @brief The signon method properties specified in device settings.
 */
@property (strong, readonly, nonatomic) NSArray *signOnMethodProperties;

/** @property autoPrintingEnabled
 *  @brief Whether or not printdatalines are automatically printed.
 */
@property (readonly, nonatomic) BOOL autoPrintingEnabled;

/*
 replacement for saveOrDie
 */

-(void)kick;

#pragma mark - Methods

/** @brief Returns the singleton instance of the class.
 */
+ (BasketController *) sharedInstance;

/** @brief Call this in response to MShopperDeviceNotFoundExceptionCode errors.
 *  @note Posts a kBasketControllerDeviceNotFoundNotification.
 */
- (void) deviceNotFound;

@property (strong, nonatomic) Credentials *credentials;

/** @brief Determines if we need the user to authenticate themselves.
 *  @return YES, if the user needs to authenticate themselves.
 */
- (BOOL) needsAuthentication;

/** @brief Sets that the user needs to authenticate themselves.
 */
- (void)setNeedsAuthentication;

/** @brief get the currenlty logged in associate's username
 */

-(NSString*)associateUserName;

/** @brief Authenticates the current user
 *  @param credentials The user credentials for the authentication request
 *  @param succeeded The block that is executed when the request has succeeded.
 *  @param failed The block that is executed when the request has failed.
 */
- (void) authenticateWithCredentials: (Credentials *)credentials
             signOffExistingOperator: (BOOL)signOffExistingOperator
                           succeeded: (void (^)(BasketController *controller))succeeded
                              failed: (void (^)(BasketController *controller, NSError *error, ServerError *serverError))failed;

/** @brief Logs out the current user
 *  @param succeeded The block that is executed when the request has succeeded.
 *  @param failed The block that is executed when the request has failed.
 */
- (void) logoutSucceeded: (void (^)(BasketController *controller))succeeded
                  failed: (void (^)(BasketController *controller, NSError *error, ServerError *serverError))failed;

/** @brief Call this in response to MShopperOperatorNotSignedOnExceptionCode errors.
 *  @note Posts the kBasketControllerHasLoggedOutNotification notification.
 */
- (void) hasLoggedOut;

/** @brief Method that determines whether the user has permission to perform an action.
 *  @return YES if the user can perform the action, otherwise NO.
 */
- (BOOL) doesUserHavePermissionTo:(POSPermission)permission;

/** @brief Requests the active basket from the web service.
 */
- (void) getActiveBasket;

/** @brief Removes the local copy of the basket and the active basket from the web service.
 */
- (void) resetBasketToServer;

/** @brief Removes the local copy of the basket but DOES NOT retreive the active basket from the web service.
 */
- (void) wipeLocalBasket;

/** @brief Finishes the basket off.
 *  @note Calling this method will clear the basket from the server.
 *  @param deviceStationId Optional device station to print/fire drawer on.
 */
- (void) finishBasketWithDeviceStationWithId:(NSString *)deviceStationId;

/** @brief Assigns an email address to the basket.
 *  @param emailAddress The email address to assign to the basket.
 */
- (void) assignEmail:(NSString *)emailAddress;

/**
 *  Adds the basket item specified to the basket on the server.
 *  @param basketItem The basket to add.
 */
- (void) addItem:(MPOSBasketItem *)basketItem completion:(void (^)(void))completion;

/** @brief Adds the item with the specified item scan identifier to the specified basket.
 *  @param itemScanId The item scan identifier of the item to be added to the basket.
 */
- (BOOL) addItemWithScanId: (NSString *)itemScanId
                     price: (NSString *) price
                   measure: (NSString *) measure
                  quantity: (NSString *) quantity
               entryMethod: (NSString *) entryMethod
                    track1: (NSString *) track1
                    track2: (NSString *) track2
                    track3: (NSString *) track3
               barcodeType: (NSString *) barcodeType;

/** @brief Adds multiple items with the specified item scan identifiers to the specified basket.
 *  @param itemScanIds An array of item scan identifiers of the items to be added to the basket.
 *  @param itemDescriptions An array of item description strings. Send an empty string to use the on file description.
 */
- (BOOL) addItemsWithScanIds:(NSArray *)itemScanIds
             andDescriptions:(NSArray *)itemDescriptions;

/** @brief Adds the return item with the specified item scan identifier to the specified basket.
 *  @param itemScanId The item scan identifier of the item to be added to the basket.
 *  @param price The price of the item.
 *  @param reason The reason the user selected for the return.
 *  @param originalScanId When returning a dump code item, the original scan id needs to be specified.
 *  @param originalItemDescription When returning a dump code item, the description needs to be specified.
 *  @param barcode The barcode of the original transaction.
 *  @param originalTransactionStoreId The id of the store the item was originally sold in.
 *  @param originalTransactionTill The id of the till the item was originally sold on.
 *  @param originalTransactionId The id of the transaction the item was originally sold in.
 *  @param originalTransactionDate The date the original item was sold on.
 *  @note If the barcode parameter is specified, the return will be processed by RMS.
 *  If the barcode parameter is not specified, the originalTransaction* fields are used.
 *  If neither are specified, the return is not linked back to the original transaction.
 */
- (void) returnItemWithScanId:(NSString *)itemScanId
                     andPrice:(NSNumber *)price
                    forReason:(DiscountReason *)reason
               originalScanId:(NSString *)originalScanId
      originalItemDescription:(NSString *)originalItemDescription
   originalTransactionBarcode:(NSString *)barcode
   originalTransactionStoreId:(NSNumber *)originalTransactionStoreId
      originalTransactionTill:(NSNumber *)originalTransactionTill
        originalTransactionId:(NSNumber *)originalTransactionId
      originalTransactionDate:(NSDate *)originalTransactionDate;

/** @brief Gets the list of return reasons.
 *  @return The list of return reasons or nil if not configured.
 */
- (NSArray *) returnReasons;

/** @brief Removes the specified item from the current basket.
 *  @param basketItems An array of items to remove from the basket.
 *  @param complete Block to execute once the item(s) are removed.
 */
- (void) removeBasketItems:(NSArray *)basketItems
                  complete:(dispatch_block_t)complete;

/** @brief Removes the specified item from the current basket.
 *  @param basketItems An array of items to remove from the basket.
 *  @param deviceStationId Device station to print to (if required).
 *  @param complete Block to execute once the item(s) are removed.
 */
- (void) removeBasketItems:(NSArray *)basketItems
       withDeviceStationId:(NSString *)deviceStationId
                  complete:(dispatch_block_t)complete;

/** @brief Applies a discount to the specified basket.
 *  @param amount The discount value.
 *  @param type The type of the discount (currency, percentage).
 *  @param reason The discount reason associated to the discount.
 *  @param description The reason description for the discount.
 */
- (void) applyDiscountWithAmount:(NSString *)amount
                          ofType:(NSString *)type
                        withCode:(DiscountCode *)discountCode
                      withReason:(DiscountReason *)discountReason
                  andDescription:(NSString *)description;

/** @brief Removes a discount from the basket.
 *  @param discount The reference to the discount to remove.
 */
- (void) removeDiscount:(MPOSBasketRewardSale *)discount;

/** @brief Applies a discount to the specified basket items.
 *  @param basketItems An array of basket items to apply the discount to.
 *  @param amount The discount value.
 *  @param type The type of the discount (currency, percentage).
 *  @param reason The discount reason associated to the discount.
 *  @param description The reason description for the discount.
 */
- (void) applyDiscountToBasketItems:(NSArray *)basketItems
                         withAmount:(NSString *)amount
                             ofType:(NSString *)type
                           withCode:(DiscountCode *)discountCode
                         withReason:(DiscountReason *)discountReason
                     andDescription:(NSString *)description;

/** @brief Applies a discount to the specified basket item.
 *  @param basketItem A reference to the basket item the discount refers to.
 *  @param amount The discount value.
 *  @param type The type of the discount (currency, percentage).
 *  @param reason The discount reason associated to the discount.
 *  @param description The reason description for the discount.
 */
- (void) adjustBasketItem:(MPOSBasketItem *)item
                withPrice:(NSString *)price
               withReason: (DiscountReason *)discountReason
           andDescription:(NSString *)reasonDescription;

/** @brief Removes the discount from the specified basket item.
 *  @param discount A reference to the discount to remove.
 *  @param basketItem A reference to the basket item.
 */
- (void) removeDiscount:(MPOSBasketRewardLine *)discount
         fromBasketItem:(MPOSBasketItem *)basketItem;

/** @brief Changes the quantity of the specified basket item.
 *  @param basketItem A reference to the basket item of which the quantity should be changed.
 *  @param quantity The new item quantity.
 */
- (void) changeQuantityOfItem: (MPOSBasketItem *)basketItem
                   toQuantity: (int) quantity;

/** @brief Changes the operator id of the basket items specified.
 *  @param basketItems The items to change.
 *  @param operatorId The new operator id to assign to the items.
 */
- (void) modifyBasketItems:(NSArray *)basketItems
               salesPerson:(NSString *)operatorId;

/** @brief Marks the specified basket items to be printed on the gift receipt.
 */
- (void) markBasketItemsForGiftReceipt:(NSArray *)basketItems;

/** @brief Clears all items from the currently active basket.
 */
- (void) clearBasket;

/** @brief Suspends the current basket.
 *  @param basketDescription Optional description to identify the suspended basket
 */
- (void) suspendBasketWithDescription:(NSString *)basketDescription;

/** @brief Resume the basket with the specified basket key.
 *  @param basketId The unique identifier of the basket to be recalled.
 *  @param basketBarcode The unique barcode number of the basket to be recalled.
 *  @param tillNumber The till the basket was suspended on.
 */
- (void) resumeBasket:(NSString *)basketId
          fromBarcode:(NSString *)basketBarcode
       withTillNumber:(NSString *)tillNumber
              success:(dispatch_block_t)successBlock;

/** @brief Checks out the current basket.
 */
- (void) checkoutBasket;

/** @brief Cancels the checkout state for the specified basket id.
 */
- (void) cancelBasketCheckout;

/** @brief Sets whether the user requires a gift receipt and gets the gift receipt for the basket (if not already loaded).
 *  @param required Whether the receipt is required or not.
 */
- (void) setGiftReceiptRequired:(BOOL)required;

/** @brief Sends the receipt to the specified email.
 *  @param emailAddress The email address to which the receipt should be send.
 *  @param complete The block to execute after the email request is sent.
 */
- (void) sendReceiptToEmail:(NSString *)emailAddress
                   complete:(dispatch_block_t)complete;

/** @brief Reprints the last receipt.
 *  @param deviceStationId The device station to print to.
 */
- (void) reprintLastTxToDeviceStation:(NSString *)deviceStationId;

/** @brief Opens the cash drawer on the device station specified.
 *  @param deviceStationId The device station to open the cash drawer on.
 */
- (void) openCashDrawerOnDeviceStation:(NSString *)deviceStationId;

- (void) noReceiptWithItemScanId:(NSString*)itemScanId
                           price:(NSString*)price
                        reasonId:(NSString*)reasonId
               reasonDescription:(NSString*)reasonDescription
                itemSerialNumber:(NSString*)itemSerialNumber
              originalItemScanId:(NSString*)originalItemScanId
         originalItemDescription:(NSString*)originalItemDescription
                        complete:(void (^)(BasketAssociateDTO* result, NSError* error, ServerError* serverError))complete;

- (void) internetReturnWithItemScanId:(NSString*)itemScanId
                                price:(NSString*)price
                             reasonId:(NSString*)reasonId
                    reasonDescription:(NSString*)reasonDescription
                     itemSerialNumber:(NSString*)itemSerialNumber
                   originalItemScanId:(NSString*)originalItemScanId
              originalItemDescription:(NSString*)originalItemDescription
                  originalReferenceId:(NSString*)originalReferenceId
                             complete:(void (^)(BasketAssociateDTO* result, NSError* error, ServerError* serverError))complete;

- (void) unlinkedReturnWithItemScanId:(NSString*)itemScanId
              originalItemDescription:(NSString*)originalItemDescription
           originalTransactionStoreId:(NSString*)originalTransactionStoreId
              originalTransactionTill:(NSString*)originalTransactionTill
                originalTransactionId:(NSString*)originalTransactionId
              originalTransactionDate:(NSString*)originalTransactionDate
                                price:(NSString*)price
                             reasonId:(NSString*)reasonId
                    reasonDescription:(NSString*)reasonDescription
                     itemSerialNumber:(NSString*)itemSerialNumber
                   originalItemScanId:(NSString*)originalItemScanId
                  originalSalesPerson:(NSString*)originalSalesPerson
                 originalCustomerCard:(NSString*)originalCustomerCard
                             complete:(void (^)(BasketAssociateDTO* result, NSError* error, ServerError* serverError))complete;

- (void) linkedReturnWithItemScanId:(NSString*)itemScanId
                             lineId:(NSString*)lineId
            originalItemDescription:(NSString*)originalItemDescription
         originalTransactionStoreId:(NSString*)originalTransactionStoreId
            originalTransactionTill:(NSString*)originalTransactionTill
              originalTransactionId:(NSString*)originalTransactionId
            originalTransactionDate:(NSString*)originalTransactionDate
                           reasonId:(NSString*)reasonId
                  reasonDescription:(NSString*)reasonDescription
                   itemSerialNumber:(NSString*)itemSerialNumber
                 originalItemScanId:(NSString*)originalItemScanId
                           complete:(void (^)(BasketAssociateDTO* result, NSError* error, ServerError* serverError))complete;

/** EFT methods **/

/** @brief Returns the localized human readable description of the current EFT payment, or
 *  nil if no transaction is in progress.
 *  @return The localized human readable description of the current EFT payment, or
 *  nil if no transaction is in progress.
 */
- (NSString *) localizedEFTPaymentStatus;

/** @brief Performs a card sale for a specific tender for a specific amount.
 *  @param tender The tender to take the payment against.
 *  @param amount The amount to take a payment for. Pass nil for the current basket balance.
 */
- (void) cardTender:(Tender *)tender
         withAmount:(NSNumber *)amount;

- (void) addVerifonePaymentTender:(VerifonePayment *)payment;

- (void) addYespayPaymentTender:(YespayPayment *)payment;

- (void) addEFTPaymentTender:(EFTPayment *)payment
                    complete:(dispatch_block_t)complete;

- (void) addEFTPaymentTender:(EFTPayment *)payment
         withDeviceStationId:(NSString *)deviceStationId
                    complete:(dispatch_block_t)complete;

/** Giftcard methods */

- (enum PaymentCardCaptureMethod) paymentCardCaptureMethodForTender:(Tender *)tender;

/** @brief Forces the server to discard its cache and refresh ALL configuration.
 */

- (void) forceServerConfigurationCacheRefresh;

/** @brief Gets the latest device configuration from the server.
 *  @param clearLocalCache Discard the etag.
 */
- (void) refreshConfiguration:(BOOL)clearLocalCache;

/** @brief Gets the latest device configuration from the server.
 *  @param force Discard the etag.
 *  @param complete Block to execute when a configuration is received.
 */
- (void) refreshConfiguration:(BOOL)clearLocalCache
                     complete:(dispatch_block_t)complete;

/** @brief Gets the configured tenders.
 *  @return The configured tenders.
 */
- (NSDictionary*)tenders;

/** @brief Gets the configured password rules.
 *  @return The configured password rules.
 */
- (NSArray*)passwordRules;

/** @brief Gets the configured discount codes for the given type.
 *  @param discountType The discount type
 *  @return The configured discount codes.
 */
- (NSArray*)discountCodesWithType:(NSString*)discountType;

/** @brief Request a payment on the current basket
 *  @param tender The tender to base the payment on.
 *  @param amount (optional) The amount to take a payment for. Pass nil for the current basket balance.
 *  @param reference (optional) The reference for the payment, eg a giftcard number
 *  @param cardNumber (optional) The
 */
- (void) paymentWithTender:(Tender *)tender
                    amount:(NSNumber *)amount
                 reference:(NSString *)reference
                  authCode:(NSString *)authCode
                cardNumber:(NSString *)cardNumber;

/** @brief Filters the discount reasons for the specified reason group.
 *  @param reasonGroup The discount reason group name.
 *  @return An array of discount reasons for the specified group.
 */
- (NSArray *) discountReasonsForGroup:(NSString *)reasonGroup;

/** @brief Gets the soft key menu with the name specified.
 *  @param name The name of the menu.
 *  @return The soft key context for the name specified or nil.
 */
- (SoftKeyContext *) getLayoutForContextNamed:(NSString *)name;


/** @brief Gets the soft key menu with the details specified.
 *  @param object The root object of the menu.
 *  @param basketStatus The status of the basket associated with the object.
 *  @param element The UI element containing the menu, eg BAR, LAYOUT, LEFT_GESTURE etc
 *  @param state The current UI state, eg EDIT. (can be nil)
 *  @return The soft key context for the details specified or nil.
 */
- (SoftKeyContext *) getLayoutForObject:(id)object
                           basketStatus:(NSString*)basketStatus
                                element:(NSString*)element
                                  state:(NSString*)state;

/** @brief Gets the soft key
 *  @param keyId the id of the softkey
 *  @param softKeyContext the containing context
 *  @return The soft key for the details specified or nil.
 */
-(SoftKey*)softKeyForKeyId:(NSString*)keyId
            softKeyContext:(SoftKeyContext*)softKeyContext;

/** @brief Enumerates all the non-MENU softkeys given a list of softKeys
 *  @param keyMenu the list of softKey ids
 *  @param softKeyContext the containing context
 *  @param block the enumeration block
 *  @discussion This method will iterate down through all the sub menus.
 */
- (void)enumerateSoftKeysForKeyMenu:(NSString*)keyMenu
                     softKeyContext:(SoftKeyContext*)softKeyContext
                         usingBlock:(void (^)(SoftKey*softKey, BOOL *stop))block;

/** @brief Convienance function to find out if the key list is in any way valid.
 *  @param keyMenu the list of softKey ids
 *  @param softKeyContext the containing context
 *  @return TRUE if menu contains at least one valid softkey.
 */
- (BOOL)menuContainsSoftKeys:(NSString*)keyMenu
              softKeyContext:(SoftKeyContext*)softKeyContext;

/** @brief Gets the keyvalue group with the details specified.
 *  @param groupName the list of softKey ids
 *  @return The keyvalue group
 */
-(KeyValueGroup*)keyValueGroupWithName:(NSString*)groupName;

/** @brief Gets the configured clientSettings.
 *  @return The configured clientSettings.
 */
-(ClientSettings*)clientSettings;


/** @brief Gets the configured affiliations.
 *  @return The configured affiliations.
 */
- (NSArray*)affiliations;

/** @brief Returns the currency code for the basket.
 */
- (NSString *) currencyCode;

/** @brief Toggles training mode on or off.
 *  Once training mode is toggled a kBasketControllerTrainingModeToggledNotification notification is posted.
 */
- (void) toggleTrainingMode;

/** @brief Gets item details for the sku specified.
 *  @param sku The item SKU.
 *  @param reason The reason for the search. This will be passed as a user info key in the complete notification.
 *  Once the item find is complete a kBasketControllerProductSearchCompleteNotification is raised.
 *  If an error occurs a kBasketControllerGeneralErrorNotification is raised.
 */
- (void) findProductForSKU:(NSString *)sku
                 forReason:(enum ProductSearchReason)reason;

/** @brief Gets availability for the item specified.
 *  @param item The item to get availability for.
 */
- (void) getAvailabilityForItem:(MPOSItem *)item;

/** @brief Gets the configured dump codes.
 *  @return The configured dump codes.
 */
- (NSArray *) dumpCodes;

/** @brief Determines if an item is a configured dump code item.
 *  @param itemId The id of the item to check.
 *  @return YES if the item is a dump code item, otherwise NO.
 */
- (BOOL) isDumpCodeItem:(NSString *)itemId;

/** @brief Retreives the modifier group with the id specified.
 *  @param modifierGroupId The id of the modifier group we want.
 *  @return The modifier group with the id specified for nil if it doesn't exist.
 */
- (ItemModifierGroup *)modifierGroupWithId:(NSString *)modifierGroupId;

/** @brief This method is called before the server is switched between primary and fallback.
 *  If there is a checked out basket, it is un checked out.
 *  If there is a basket, it is discarded.
 *  If the user is logged on, they are logged off.
 */
- (void) prepareForServerSwitch;

/** @brief creates a basket if none exists
 */
- (void)ensureBasketComplete:(void (^)(BasketDTO *basket, NSError *error, ServerError *serverError))complete;

-(NSArray *)customerIdentificationTypes;

- (BasketDTO*)adjustSale:(BasketDTO*)basket
                   error:(NSError **)error
             serverError:(ServerError **)serverError;

- (NSString *)localizedStringForString:(NSString *)string
                               comment:(NSString*)comment
                                 table:(NSString *)tableName;

- (void) printPrintDataLine:(MPOSPrintDataLine *)printLine
                    skipped:(BOOL)skipped
                  newStatus:(NSString *)status;

@end
