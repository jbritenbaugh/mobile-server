//
//  ExternalAuthViewController.h
//  mPOS
//
//  Created by Antonio Strijdom on 19/09/2013.
//  Copyright (c) 2013 Clarity. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LoginViewController.h"

@interface ExternalAuthViewController : UIViewController

/** @property delegate
 *  @brief A reference to the delegate to which we'll send notifications.
 */
@property (weak, nonatomic) id<LoginViewControllerDelegate> delegate;

@end
