//
//  PAYwareController.h
//  mPOS
//
//  Created by John Scott on 20/03/2013.
//  Copyright (c) 2013 Omnico Group. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "OCGURLSchemeHandler.h"

extern NSString * const kPAYwareControllerNotification;

extern NSString * const kPAYwareControllerUserKey;
extern NSString * const kPAYwareControllerTrack1DataKey;
extern NSString * const kPAYwareControllerTrack2DataKey;
extern NSString * const kPAYwareControllerTrack3DataKey;
extern NSString * const kPAYwareControllerResponseMessageKey;

@interface PAYwareController : OCGURLSchemeHandler

/** @brief Returns the singleton instance of the PAYware controller.
 */
+ (PAYwareController *) sharedInstance;

/** @brief This requests a card swipe.
 @param key This value will be returned in userData[kPAYwareControllerUserKey].
 @note A response will be posted as a kPAYwareControllerNotification where
 userData[kPAYwareControllerUserKey] has been set to key
  */
- (void) requestCardDataWithKey:(NSString*)key;

@end
