//
//  BasketTableViewController.h
//  mPOS
//
//  Created by Antonio Strijdom on 12/02/2014.
//  Copyright (c) 2014 Clarity. All rights reserved.
//

#import <UIKit/UIKit.h>

/** @file BasketTableViewController.h */

/** @brief Table view controller for displaying basket items. */
@interface BasketTableViewController : UICollectionViewController

/** @property allowsSelection
 *  @brief Whether or not selection is enabled.
 */
@property (nonatomic, assign) BOOL allowsSelection;

/** @property allowsEditing
 *  @brief Whether or not editing is enabled.
 */
@property (nonatomic, assign) BOOL allowsEditing;

/** @property selectedBasketItem
 *  @brief The current basket item selected.
 */
@property (nonatomic, weak) MPOSBasketItem *selectedBasketItem;

/** @brief Reloads the table data source and table.
 */
- (void) reloadTableDataAndScroll:(BOOL)scroll;

@end
