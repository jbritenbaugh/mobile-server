//
//  OCGEFTProviderAdyen.m
//  mPOS
//
//  Created by Antonio Strijdom on 21/01/2015.
//  Copyright (c) 2015 Clarity. All rights reserved.
//

#import "OCGEFTProviderAdyen.h"
#import "BasketController.h"
#import "DeviceSelectionViewController.h"
#import "OCGEFTAdyenDeviceSelectionRDMDatasource.h"
#import "OCGProgressOverlay.h"
#import "UIAlertView+OCGBlockAdditions.h"
#import "Device+MPOSExtensions.h"

typedef void (^DeviceSelectionDeviceSelectedBlock)(ADYDevice *device);

@interface OCGEFTProviderAdyen () <ADYLoginDelegate, ADYDeviceManagerDelegate, ADYTransactionProcessorDelegate, DeviceSelectionViewControllerDataSource, DeviceSelectionViewControllerDelegate>
- (void) resetTimeout;
- (void) returnErrorWithErrorCode:(ADYErrorCode)errorCode
                          toBlock:(EFTProviderCompleteBlock)complete;
- (void) returnErrorWithErrorCode:(ADYErrorCode)errorCode
                          toBlock:(EFTProviderCompleteBlock)complete
                     supressError:(BOOL)supressError;
- (NSString *) localizedDescriptionForErrorCode:(ADYErrorCode)errorCode;
- (void) loginCheckComplete:(EFTProviderCompleteBlock)complete;
- (void) deviceCheckForTx:(BOOL)forTx
                 complete:(EFTProviderCompleteBlock)complete;
- (void) boardDeviceComplete:(EFTProviderCompleteBlock)complete;
- (void) selectDeviceAllowSelection:(BOOL)allowSelection
                           complete:(DeviceSelectionDeviceSelectedBlock)complete;
- (NSArray *) getDeviceList;
- (void) ensureDeviceReadyComplete:(EFTProviderCompleteBlock)complete;
- (void) processTransactionComplete:(EFTProviderCompleteBlock)complete;
- (void) updateEFTPayment:(EFTPayment *)payment
       fromADYTransaction:(ADYTransactionData *)transaction;
@property (nonatomic, readonly) CGFloat timeout;
@property (nonatomic, strong) NSDate *timeoutStart;
@property (nonatomic, strong) NSDate *transactionTimeoutStart;
@property (nonatomic, strong) ADYDevice *currentEFTDevice;
@property (nonatomic, strong) ADYTransactionRequest *currentEFTRequest;
@property (nonatomic, strong) ADYSignatureRequest *currentSignatureRequest;
@property (nonatomic, strong) NSError *lastError;
@property (nonatomic, readonly) OCGEFTManager *manager;
@property (nonatomic, strong) EFTProviderCompleteBlock loginDelegateCompleteBlock;
@property (nonatomic, strong) EFTProviderCompleteBlock deviceDelegateCompleteBlock;
@property (nonatomic, assign) BOOL rememberSelection;
@property (nonatomic, strong) DeviceSelectionDeviceSelectedBlock deviceSelectedBlock;
@property (nonatomic, strong) EFTProviderCompleteBlock transactionDelegateCompleteBlock;
@property (nonatomic, strong) NSString *localizedInitializingStatus;
@property (nonatomic, strong) NSString *localizedProcessingStatus;
@property (nonatomic, strong) OCGEFTAdyenDeviceSelectionRDMDatasource *remoteDeviceSelectionDataSource;
@end

@implementation OCGEFTProviderAdyen

#pragma mark - Private

- (void) returnErrorWithErrorCode:(ADYErrorCode)errorCode
                          toBlock:(EFTProviderCompleteBlock)complete
{
    [self returnErrorWithErrorCode:errorCode
                           toBlock:complete
                      supressError:NO];
}

- (void) returnErrorWithErrorCode:(ADYErrorCode)errorCode
                          toBlock:(EFTProviderCompleteBlock)complete
                     supressError:(BOOL)supressError
{
    NSError *error = [NSError errorWithDomain:ADYErrorDomain
                                         code:errorCode
                                     userInfo:nil];
    self.lastError = error;
    complete(kEFTProviderErrorCodeApplicationError, error, supressError);
}

- (void) resetTimeout
{
    NSDate *lastTransactionUpdateTimestamp = [NSDate date];
    self.transactionTimeoutStart = lastTransactionUpdateTimestamp;
    // don't timeout during signature capture
    if ((kEFTPaymentStatusInitializing == self.manager.currentPayment.paymentStatus) ||
        (kEFTPaymentStatusProcessing == self.manager.currentPayment.paymentStatus) ||
        (kEFTPaymentStatusSignatureRequired == self.manager.currentPayment.paymentStatus)) {
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(self.timeout * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            if (self.transactionTimeoutStart == lastTransactionUpdateTimestamp) {
                if ([lastTransactionUpdateTimestamp timeIntervalSinceNow] <= self.timeout * -1.0) {
                    // timeout, cancel the TX
                    self.transactionTimeoutStart = nil;
                    // don't timeout during signature capture
                    DebugLog(@"Timeout fired - %@", self.manager.currentPayment.localizedStringForCurrentStatus);
                    if ((kEFTPaymentStatusInitializing == self.manager.currentPayment.paymentStatus) ||
                        (kEFTPaymentStatusProcessing == self.manager.currentPayment.paymentStatus) ||
                        (kEFTPaymentStatusSignatureRequired == self.manager.currentPayment.paymentStatus)) {
                        [self cancelTx];
                    }
                }
            }
        });
    }
}

NSString * const kOCGEFTProviderAdyenDeviceConfigKey = @"kOCGEFTProviderAdyenDeviceConfigKey";

- (UIViewController*) topMostController
{
    UIViewController *topController = [UIApplication sharedApplication].keyWindow.rootViewController;
    
    while (topController.presentedViewController) {
        topController = topController.presentedViewController;
    }
    
    return topController;
}

#pragma mark - Adyen

- (NSString *) localizedDescriptionForErrorCode:(ADYErrorCode)errorCode
{
    NSString *result = nil;
    
    switch (errorCode) {
        case ADYErrorCodeUnknownError:
            result = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠀫󠁪󠁤󠁓󠀶󠁲󠁹󠀯󠁊󠁙󠁏󠁸󠁍󠁈󠁣󠁫󠁐󠁚󠁃󠁺󠀸󠁄󠁤󠁌󠁱󠁘󠁍󠁿*/ @"Unknown error", nil);
            break;
        case ADYErrorCodeNoDeviceConnected:
            result = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁫󠁭󠁵󠁗󠁈󠁺󠁺󠁄󠁔󠁁󠁅󠀰󠀶󠁇󠁌󠁦󠁫󠁺󠁄󠀳󠁐󠁣󠀵󠀵󠁸󠁵󠁳󠁿*/ @"No device connected", nil);
            break;
        case ADYErrorCodeUnauthorizedForDevice:
            result = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁔󠁑󠀹󠁓󠁏󠁕󠁗󠀵󠁣󠁩󠁉󠁁󠁗󠁐󠀳󠀵󠀱󠁖󠁲󠀱󠀷󠁹󠁓󠁳󠁈󠁎󠁣󠁿*/ @"Unauthorised device", nil);
            break;
        case ADYErrorCodeDeviceError:
            result = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁘󠀶󠁴󠁏󠁫󠁲󠁂󠁴󠁉󠀶󠁣󠁶󠁣󠀰󠁡󠁫󠁬󠁰󠁺󠀷󠁵󠁴󠁤󠁵󠁙󠁃󠁧󠁿*/ @"Device error", nil);
            break;
        case ADYErrorCodeInvalidCredentialsForPaymentDevice:
            result = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠀯󠁤󠁢󠀸󠀲󠁅󠁙󠁒󠁰󠀷󠁰󠁌󠁢󠁨󠁧󠁒󠁙󠁨󠀷󠁯󠁦󠁒󠁥󠁙󠁬󠁣󠁕󠁿*/ @"Invalid credentials for device", nil);
            break;
        case ADYErrorCodeAmountTooLow:
            result = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁊󠁵󠁃󠁖󠁤󠀲󠁆󠁕󠁶󠁺󠀹󠀸󠁦󠁅󠀱󠁋󠁔󠁰󠁸󠀱󠁡󠁨󠀹󠁭󠁩󠁇󠁁󠁿*/ @"Amount too low", nil);
            break;
        case ADYErrorCodeTransactionInitializationError:
            result = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁐󠁲󠁶󠁃󠁯󠀲󠀷󠁧󠁣󠁡󠁒󠁡󠁰󠀹󠁬󠁚󠁃󠁨󠁹󠁩󠁺󠁤󠁔󠁳󠀶󠁚󠁍󠁿*/ @"Transaction initialisation error", nil);
            break;
        case ADYErrorCodeTransactionRequiresLocationServices:
            result = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁕󠁂󠁋󠁵󠁈󠁧󠁒󠁕󠁵󠁎󠁑󠁢󠁏󠁳󠀶󠁧󠁶󠀶󠁸󠁑󠁈󠁅󠁣󠁪󠁆󠁅󠀸󠁿*/ @"Location services are required, but not available", nil);
            break;
        case ADYErrorCodeInvalidCredentialsForPSP:
            result = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁪󠁖󠁒󠁃󠁣󠁧󠁦󠁎󠁘󠁑󠁯󠁫󠁄󠁑󠁧󠁂󠁑󠁮󠁐󠁵󠁖󠁈󠁌󠁓󠁧󠁉󠁉󠁿*/ @"Invalid merchant credentials", nil);
            break;
        case ADYErrorCodePaymentDeviceConnectionError:
            result = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁚󠁴󠁑󠁳󠁦󠁶󠁌󠁁󠁵󠁡󠁎󠁳󠀸󠁪󠀹󠁱󠁅󠁔󠁲󠁬󠀶󠁂󠁆󠁴󠁘󠁍󠁍󠁿*/ @"Device connection error", nil);
            break;
        case ADYErrorCodeNetworkError:
            result = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁺󠀶󠁬󠁢󠁮󠁱󠁕󠁍󠀵󠁈󠁤󠁍󠁕󠁈󠀰󠀴󠁑󠁺󠀵󠀹󠁋󠁢󠁁󠁄󠀳󠀫󠁙󠁿*/ @"Network error", nil);
            break;
        case ADYErrorCodeNoInternetConnection:
            result = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁎󠀸󠀲󠁲󠁧󠁹󠁊󠁒󠁢󠁪󠁲󠁱󠀫󠁊󠁰󠁓󠁪󠁶󠁭󠁈󠀯󠁁󠁷󠁍󠀹󠁥󠀰󠁿*/ @"No internet connection available", nil);
            break;
        case ADYErrorCodeInternalServerError:
            result = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁴󠁓󠁧󠁮󠁔󠁡󠀹󠁗󠁬󠁈󠁲󠁮󠁡󠁉󠀵󠁇󠁧󠁦󠁯󠁇󠀲󠁏󠁃󠁒󠀱󠀲󠁷󠁿*/ @"Internal server error", nil);
            break;
        case ADYErrorCodeAppRegistrationFailed:
            result = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁑󠁗󠁉󠁏󠀸󠁧󠁓󠁂󠀸󠁡󠁩󠁰󠁩󠀵󠁗󠀵󠁆󠁔󠁰󠀫󠁚󠁣󠁤󠁴󠁇󠁶󠁕󠁿*/ @"Registration failed", nil);
            break;
        case ADYErrorCodeInvalidApp:
            result = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁍󠁊󠁧󠀴󠁫󠁖󠁘󠁸󠁹󠁒󠁓󠁩󠁆󠀰󠁥󠁥󠀵󠀰󠁶󠁮󠀴󠁑󠁶󠁘󠀷󠁬󠁳󠁿*/ @"Registration required", nil);
            break;
        case ADYErrorCodeUserPermissionError:
            result = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁢󠁔󠁄󠁰󠁃󠁨󠁫󠀰󠁄󠁢󠁘󠁫󠀷󠁙󠀯󠁘󠁏󠁅󠀷󠁷󠁆󠁙󠁭󠁙󠁁󠁹󠁷󠁿*/ @"Invalid permissions to perform operation", nil);
        case ADYErrorCodeInvalidMerchant:
            result = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁯󠁚󠁧󠀱󠁐󠁓󠁌󠁵󠁃󠁐󠁋󠀯󠁑󠁄󠁡󠁡󠁱󠁶󠁤󠁍󠁯󠁲󠁑󠁹󠁃󠁦󠁍󠁿*/ @"Invalid merchant account", nil);
            break;
        case ADYErrorCodeAlreadyLoggedIn:
            result = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁹󠁍󠁕󠁯󠁚󠀱󠁪󠁧󠁓󠁎󠁈󠁚󠁁󠁶󠀹󠁆󠁰󠁡󠁈󠀸󠀵󠁰󠁡󠁓󠁋󠁔󠀰󠁿*/ @"Already logged in", nil);
            break;
        case ADYErrorCodeNotLoggedIn:
            result = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁰󠁘󠁸󠀲󠁨󠁳󠁎󠁈󠁈󠀴󠁌󠁡󠁯󠁑󠁳󠀴󠁣󠁔󠀴󠀶󠁫󠁩󠀸󠁍󠁎󠁃󠁳󠁿*/ @"Not logged in", nil);
            break;
        case ADYErrorCodeAppRegistrationTokenCheckTimeout:
            result = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁑󠁏󠁒󠀰󠁸󠁎󠁴󠀯󠀷󠁯󠁩󠁃󠁊󠁱󠀲󠀱󠁑󠀴󠁕󠁆󠁓󠁨󠁘󠁮󠁅󠀶󠁯󠁿*/ @"Registration token check timed out", nil);
            break;
        case ADYErrorCodeNotSupported:
            result = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁶󠁆󠁡󠀯󠁯󠁙󠁳󠁶󠁎󠁸󠁶󠁤󠁯󠁓󠁔󠁄󠁱󠁣󠁧󠁖󠁃󠁙󠁍󠁄󠁬󠁚󠀸󠁿*/ @"Transaction not supported", nil);
            break;
        case ADYErrorCodeDeviceEMVError:
            result = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁲󠀳󠁦󠁴󠁑󠁸󠀸󠀳󠁅󠀵󠁃󠁹󠁰󠀸󠁷󠀶󠁉󠁪󠁑󠁉󠁸󠁗󠁵󠁬󠁉󠀰󠁳󠁿*/ @"Device EMV error", nil);
            break;
        case ADYErrorCodeDeviceSAPIError:
            result = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁬󠀶󠁏󠁯󠁈󠁚󠁧󠁖󠁲󠁹󠁐󠁹󠁫󠁡󠀵󠀲󠁰󠁷󠁔󠁓󠁑󠁴󠁬󠁣󠁩󠀱󠁙󠁿*/ @"Device SAPI error", nil);
            break;
        case ADYErrorCodeDevicePrintReceiptFailed:
            result = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁱󠁮󠁯󠁚󠁢󠀰󠀱󠁢󠁸󠁇󠀸󠁩󠁏󠁁󠁚󠁙󠁁󠀲󠀸󠁤󠀳󠁱󠁐󠁚󠀲󠁴󠁕󠁿*/ @"Receipt printing failed", nil);
            break;
        case ADYErrorCodePSPValidateError:
            result = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁷󠁵󠀷󠁚󠁂󠁭󠁱󠁰󠀰󠀶󠁅󠁊󠁈󠁤󠁔󠁳󠁪󠁱󠁢󠁣󠁌󠁐󠁉󠁕󠁋󠀸󠁕󠁿*/ @"PSP Validation error", nil);
            break;
        default:
            result = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁣󠁁󠀹󠁯󠁇󠁢󠁑󠁢󠀴󠀷󠁰󠁘󠁐󠁴󠁇󠁓󠀸󠁷󠁖󠀯󠁓󠀶󠁆󠁪󠁎󠀳󠁁󠁿*/ @"Undefined error", nil);
            break;
    }
    
    return NSLocalizedString(result, nil);
}

- (BOOL) waitForLoginStatusToChange:(ADYLoginStatus)status
                      numberSeconds:(NSUInteger)numTries
{
    __block BOOL result = NO;
    __block NSUInteger tries = 0;
    
    Adyen *adyen = [Adyen sharedInstance];
    while ((tries < numTries) && (status == adyen.loginStatus)) {
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            if (status == adyen.loginStatus) {
                tries++;
            } else {
                result = YES;
            }
        });
    }
    
    return result;
}

- (void) loginCheckComplete:(EFTProviderCompleteBlock)complete
{
    // get the prerequisite information
    Device *EFTDevice = [[BasketController sharedInstance] deviceForSettingWithName:kOCGEFTManagerConfiguredPaymentProviderKey];
    if (EFTDevice) {
        NSString *merchantCode = EFTDevice.propertyDict[@"MID"];
        NSString *userName = EFTDevice.propertyDict[@"EFTUserName"];
        NSString *password = EFTDevice.propertyDict[@"EFTPassword"];
        if ((merchantCode.length > 0) && (userName.length > 0) && (password.length > 0)) {
            // perform Adyen login check
            Adyen *adyen = [Adyen sharedInstance];
            // check if we are logging in
            if (ADYLoginStatusLoggingIn == adyen.loginStatus) {
                self.localizedInitializingStatus = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁹󠁈󠁎󠁡󠁁󠀯󠁇󠁧󠁭󠁉󠁈󠁧󠁧󠁦󠁕󠁣󠁱󠀰󠁬󠁷󠁮󠀴󠁑󠁑󠁶󠁔󠀰󠁿*/ @"Logging in", nil);
                // wait till we are logged in (shouldn't take more than 2 minutes)
                [self waitForLoginStatusToChange:ADYLoginStatusLoggingIn
                                   numberSeconds:120];
            }
            // check if we are logged in
            if (ADYLoginStatusLoggedIn == adyen.loginStatus) {
                complete(kEFTProviderErrorCodeSuccess, nil, NO);
            } else {
                // check if we are logging out
                if (ADYLoginStatusLoggingOut == adyen.loginStatus) {
                    self.localizedInitializingStatus = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁥󠁃󠁧󠁆󠀰󠁕󠁺󠀸󠁁󠀸󠁺󠁭󠁏󠁥󠀵󠁣󠁰󠀸󠁡󠁡󠁁󠁄󠁃󠁌󠁃󠁅󠁉󠁿*/ @"Logging out", nil);
                    // wait till we log out
                    [self waitForLoginStatusToChange:ADYLoginStatusLoggingOut
                                       numberSeconds:60];
                }
                // check if we aren't logged in
                if (ADYLoginStatusLoggedOut == adyen.loginStatus) {
                    // show spinner
                    [OCGProgressOverlay show];
                    // make a note of the complete block
                    self.loginDelegateCompleteBlock = complete;
                    // log in
                    self.localizedInitializingStatus = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁹󠁈󠁎󠁡󠁁󠀯󠁇󠁧󠁭󠁉󠁈󠁧󠁧󠁦󠁕󠁣󠁱󠀰󠁬󠁷󠁮󠀴󠁑󠁑󠁶󠁔󠀰󠁿*/ @"Logging in", nil);
                    [adyen loginWithMerchantCode:merchantCode
                                    withUsername:userName
                                    withPassword:password
                                     andDelegate:self];
                    // adyen login delegate methods take over...
                } else {
                    ErrorLog(@"Unhandled Adyen login status: %i", adyen.loginStatus);
                    [self returnErrorWithErrorCode:ADYErrorCodeNotLoggedIn
                                           toBlock:complete];
                }
            }
        } else {
            ErrorLog(@"Missing Adyen credentials");
            [self returnErrorWithErrorCode:ADYErrorCodeNotLoggedIn
                                   toBlock:complete];
        }
    } else {
        ErrorLog(@"No EFT device configured");
        [self returnErrorWithErrorCode:ADYErrorCodeNotLoggedIn
                               toBlock:complete];
    }
}

- (void) deviceCheckForTx:(BOOL)forTx
                 complete:(EFTProviderCompleteBlock)complete
{
    // check to see if there is a device configured
    ADYDeviceRegistry *deviceRegistry = [[Adyen sharedInstance] deviceRegistry];
    if (deviceRegistry) {
        // stop polling unnecessarily
        [deviceRegistry stopPolling];
        // get the device name
        NSString *deviceName =
        [[OCGSettingsController sharedInstance] valueForSettingWithName:kOCGEFTProviderAdyenDeviceConfigKey];
        if (deviceName.length > 0) {
            // if there is a configured device, get it from Adyen
            self.currentEFTDevice = [deviceRegistry findDeviceByName:deviceName];
            [self ensureDeviceReadyComplete:^(EFTProviderErrorCode resultCode, NSError *error, BOOL suppressError) {
                if (kEFTProviderErrorCodeSuccess == resultCode) {
                    complete(kEFTProviderErrorCodeSuccess, nil, NO);
                } else {
                    // configured device isn't available, show selection
                    self.rememberSelection = NO;
                    [self selectDeviceAllowSelection:forTx
                                    complete:^(ADYDevice *device) {
                                        if (device || forTx) {
                                            self.currentEFTDevice = device;
                                            [self ensureDeviceReadyComplete:complete];
                                        } else {
                                            complete(kEFTProviderErrorCodeSuccess, nil, NO);
                                        }
                                    }];
                }
            }];
        } else {
            // no device configured, display the list to the user
            self.rememberSelection = NO;
            [self selectDeviceAllowSelection:forTx
                                    complete:^(ADYDevice *device) {
                                        if (device || forTx) {
                                            self.currentEFTDevice = device;
                                            [self ensureDeviceReadyComplete:complete];
                                        } else {
                                            complete(kEFTProviderErrorCodeSuccess, nil, NO);
                                        }
            }];
        }
    } else {
        ErrorLog(@"Could not get device registry");
        [self returnErrorWithErrorCode:ADYErrorCodeUnknownError
                               toBlock:complete];
    }
}

- (void) boardDeviceComplete:(EFTProviderCompleteBlock)complete
{
    // attempt to board the device
    DebugLog(@"Boarding device...");
    self.deviceDelegateCompleteBlock = complete;
    [OCGProgressOverlay show];
    [self.currentEFTDevice boardDeviceWithDelegate:self];
}

- (void) selectDeviceAllowSelection:(BOOL)allowSelection
                           complete:(DeviceSelectionDeviceSelectedBlock)complete
{
    // if selection is allowed
    if ([self.manager deviceSelectionAllowed] && allowSelection) {
        // make a note of the block
        self.deviceSelectedBlock = complete;
        // show the selection view controller
        DeviceSelectionViewController *deviceSelectionVC =
        [[DeviceSelectionViewController alloc] initWithStyle:UITableViewStyleGrouped];
        deviceSelectionVC.delegate = self;
        deviceSelectionVC.title = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠀲󠁇󠁓󠁵󠁩󠀱󠁔󠁺󠁓󠁬󠁷󠁲󠁲󠁭󠁃󠁃󠁢󠁃󠁎󠁉󠁉󠀸󠁩󠁋󠁃󠀸󠁧󠁿*/ @"EFT Devices", nil);
        Device *EFTDevice = [[BasketController sharedInstance] deviceForSettingWithName:kOCGEFTManagerConfiguredPaymentProviderKey];
        if (EFTDevice && EFTDevice.selectionMode == DeviceSelectionModeRDM) {
            deviceSelectionVC.dataSource = self.remoteDeviceSelectionDataSource;
        } else {
            deviceSelectionVC.dataSource = self;
        }
        // sign it up for KVO notifications
        ADYDeviceRegistry *deviceRegistry = [[Adyen sharedInstance] deviceRegistry];
        [deviceRegistry startPolling];
        if (deviceRegistry) {
            [deviceRegistry addObserver:deviceSelectionVC
                             forKeyPath:@"devices"
                                options:NSKeyValueObservingOptionInitial | NSKeyValueObservingOptionOld | NSKeyValueObservingOptionNew
                                context:NULL];
        }
        deviceSelectionVC.deviceCurrentlySelectedBlock = ^BOOL (id device) {
            BOOL result = NO;
            
            NSString *deviceName =
            [[OCGSettingsController sharedInstance] valueForSettingWithName:kOCGEFTProviderAdyenDeviceConfigKey];
            ADYDevice *adyenDevice = (ADYDevice *)device;
            if ([adyenDevice.name isEqualToString:deviceName]) {
                result = YES;
            }
            
            return result;
        };
        UINavigationController *deviceNav = [[UINavigationController alloc] initWithRootViewController:deviceSelectionVC];
        deviceNav.modalPresentationStyle = UIModalPresentationFormSheet;
        [[self topMostController] presentViewController:deviceNav
                                               animated:YES
                                             completion:nil];
    } else {
        complete(nil);
    }
}

- (NSArray *) getDeviceList
{
    NSArray *result = nil;
    
    // make a copy of the adyen device list
    ADYDeviceRegistry *deviceRegistry = [[Adyen sharedInstance] deviceRegistry];
    if (deviceRegistry) {
        result = [deviceRegistry.devices copy];
    }
    
    return result;
}
    
- (void) ensureDeviceReadyComplete:(EFTProviderCompleteBlock)complete
{
    if (nil == self.timeoutStart) {
        self.timeoutStart = [NSDate date];
    }
    if (self.currentEFTDevice) {
        switch (self.currentEFTDevice.status) {
            case ADYDeviceStatusInitialized:
                self.timeoutStart = nil;
                [OCGProgressOverlay hide];
                complete(kEFTProviderErrorCodeSuccess, nil, NO);
                break;
            case ADYDeviceStatusNotBoarded: {
                self.timeoutStart = nil;
                [OCGProgressOverlay hide];
                // device not ready, attempt to board it
                [self boardDeviceComplete:^(EFTProviderErrorCode resultCode, NSError *error, BOOL suppressError) {
                    if (kEFTProviderErrorCodeSuccess == resultCode) {
                        UIAlertController *alert =
                        [UIAlertController alertControllerWithTitle:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠀫󠀱󠁏󠁸󠁩󠀸󠁅󠁒󠁺󠁤󠁕󠀷󠁫󠁃󠁔󠁐󠁺󠁡󠀰󠁴󠀰󠁇󠀱󠁤󠀰󠁄󠁅󠁿*/ @"Boarding successful", nil)
                                                            message:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁒󠁪󠁑󠁓󠁔󠁋󠁔󠁵󠁄󠁎󠁳󠁍󠁖󠁍󠁇󠁩󠁺󠁑󠁕󠁡󠁰󠁈󠁕󠁂󠁖󠁘󠁳󠁿*/ @"Please wait while the device restarts.", nil)
                                                     preferredStyle:UIAlertControllerStyleAlert];
                        [alert addAction:[UIAlertAction actionWithTitle:NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁄󠁴󠁍󠁉󠁣󠀫󠁺󠁔󠁗󠁦󠁹󠁡󠁯󠀷󠁨󠁔󠁩󠀸󠁯󠀫󠁨󠁘󠀹󠁱󠁌󠀶󠀸󠁿*/ @"OK", nil)
                                                                  style:UIAlertActionStyleDefault
                                                                handler:^(UIAlertAction *action) {
                                                                    complete(kEFTProviderErrorCodeSuccess, nil, NO);
                                                                }]];
                        [[self topMostController] presentViewController:alert
                                                               animated:YES
                                                             completion:nil];
                    } else {
                        // could not board
                        self.currentEFTDevice = nil;
                        ErrorLog(@"Could not board device: %@", error);
                        [self returnErrorWithErrorCode:ADYErrorCodeDeviceError
                                               toBlock:complete];
                    }
                }];
                break;
            }
            default: {
                if ([self.timeoutStart timeIntervalSinceNow] <= self.timeout * -1.0f) {
                    self.timeoutStart = nil;
                    [OCGProgressOverlay hide];
                    self.currentEFTDevice = nil;
                    ErrorLog(@"Unhandled device status");
                    [self returnErrorWithErrorCode:ADYErrorCodeDeviceError
                                           toBlock:complete];
                } else {
                    // wait for the device status to change
                    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0f * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                        [self ensureDeviceReadyComplete:complete];
                    });
                }
                break;
            }
        }
    } else {
        ErrorLog(@"No device");
        [self returnErrorWithErrorCode:ADYErrorCodeDeviceError
                               toBlock:complete
                          supressError:YES];
    }
}

- (void) processTransactionComplete:(EFTProviderCompleteBlock)complete
{
    // perform the transaction
    
    if (ADYLoginStatusLoggedIn == [Adyen sharedInstance].loginStatus) {
        // make sure we have a device
        if (self.currentEFTDevice) {
            // and that it is ready to take a transaction
            if (self.currentEFTDevice.status == ADYDeviceStatusInitialized) {
                // start the transaction
                NSError *error = nil;
                self.currentEFTRequest =
                [self.currentEFTDevice createTransactionRequestWithReference:self.manager.currentPayment.uniqueMerchantReference
                                                                       error:&error];
                if (self.currentEFTRequest) {
                    // fill in request details
                    self.currentEFTRequest.currency = self.manager.currentPayment.currencyCode;
                    self.currentEFTRequest.amount = @(self.manager.currentPayment.tenderAmount.amount.doubleValue * 100.0f);
                    switch (self.manager.currentPayment.transactionType) {
                        case kEFTPaymentTransactionTypeSale:
                            self.currentEFTRequest.transactionType = ADYTransactionTypeGoodsServices;
                            break;
                        case kEFTPaymentTransactionTypeRefund:
                            self.currentEFTRequest.transactionType = ADYTransactionTypeRefund;
                            break;
                        default:
                            ErrorLog(@"Unhandled transaction type: %i", self.manager.currentPayment.transactionType);
                            break;
                    }
                    self.currentEFTRequest.options = @{ ADYTransactionOptionAdditionalDataKey : @"TRUE",
                                                        ADYTransactionOptionCustomReceiptHandler : @"TRUE" };
                    // start the transaction
                    error = nil;
                    self.transactionDelegateCompleteBlock = complete;
                    if (![self.currentEFTRequest startWithDelegate:self
                                                             error:&error]) {
                        // couldn't initiate transaction
                        self.currentEFTDevice = nil;
                        self.currentEFTRequest = nil;
                        ErrorLog(@"Couldn't initiate transaction: %@", error);
                        self.lastError = error;
                        complete(kEFTProviderErrorCodeApplicationError, error, NO);
                    }
                } else {
                    // couldn't create transaction request
                    self.currentEFTDevice = nil;
                    ErrorLog(@"Couldn't create transaction request");
                    self.lastError = error;
                    complete(kEFTProviderErrorCodeApplicationError, nil, NO);
                }
            } else {
                // device not ready, fail
                self.currentEFTDevice = nil;
                ErrorLog(@"Device not ready");
                [self returnErrorWithErrorCode:ADYErrorCodeDeviceError
                                       toBlock:complete];
            }
        } else {
            // device not ready, fail
            self.currentEFTDevice = nil;
            ErrorLog(@"Device not ready");
            [self returnErrorWithErrorCode:ADYErrorCodeDeviceError
                                   toBlock:complete];
        }
    } else {
        // device not ready, fail
        self.currentEFTDevice = nil;
        ErrorLog(@"Not logged in");
        [self returnErrorWithErrorCode:ADYErrorCodeNotLoggedIn
                               toBlock:complete];
    }
}

- (void) updateEFTPayment:(EFTPayment *)payment
       fromADYTransaction:(ADYTransactionData *)transaction
{
    // update the payment from the transaction
    payment.encryptedAccountNumber = transaction.cardNumber;
    payment.authorizationCode = transaction.pspAuthCode;
    payment.authorizingTermId = transaction.tid;
    payment.cardTypeCode = transaction.cardType;
    payment.eftProvider = @"Adyen";
    payment.providerTenderId = transaction.tenderReference;
    payment.providerCentralTenderId = transaction.pspReference;
    payment.rawData = [self RawStringForTransaction];
    payment.tenderType = payment.tender.tenderType;
    // TFS74824 - add customer alias
    payment.customerAlias = transaction.cardAlias;
    // add issuer country
    payment.issuerCountryId = transaction.issuerCountry;
    // cardBin
    payment.cardBin = transaction.cardBin;
    // cardType
    payment.cardType = transaction.cardString;
}

#pragma mark ADYLoginDelegate

- (void) loginUpdatedProgress:(float)progress
{
    InfoLog(@"Login process at %f%%", progress);
}

- (void) loginCompletedSuccessfullyWithInfo:(NSDictionary *)info
{
    DebugLog(@"Login successful");
    [OCGProgressOverlay hide];
    self.localizedInitializingStatus = nil;
    // logged in successfully
    if (self.loginDelegateCompleteBlock) {
        self.loginDelegateCompleteBlock(kEFTProviderErrorCodeSuccess, nil, NO);
    }
    self.loginDelegateCompleteBlock = nil;
}

- (void) loginFailedWithError:(NSError*)error
{
    ErrorLog(@"Login failed - %@", [error localizedDescription]);
    [OCGProgressOverlay hide];
    self.localizedInitializingStatus = nil;
    self.lastError = error;
    // login failed
    if (self.loginDelegateCompleteBlock) {
        self.loginDelegateCompleteBlock(kEFTProviderErrorCodeApplicationError, error, NO);
    }
    self.loginDelegateCompleteBlock = nil;
}

- (void) loginChangedStatus:(ADYLoginDelegateStatus)status
{
    self.localizedInitializingStatus = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁹󠁈󠁎󠁡󠁁󠀯󠁇󠁧󠁭󠁉󠁈󠁧󠁧󠁦󠁕󠁣󠁱󠀰󠁬󠁷󠁮󠀴󠁑󠁑󠁶󠁔󠀰󠁿*/ @"Logging in", nil);
    switch (status) {
        case ADYLoginDelegateStatusLoggingIn:
            DebugLog(@"Login - Logging in");
            break;
        case ADYLoginDelegateStatusLoggedIn:
            DebugLog(@"Login - Logged in");
            break;
        case ADYLoginDelegateStatusPreSettling:
            DebugLog(@"Login - Pre Settling");
            break;
        case ADYLoginDelegateStatusVerifyingToken:
            DebugLog(@"Login - Verifying token");
            break;
        case ADYLoginDelegateStatusTokenNotYetValid:
            DebugLog(@"Login - Token not yet valid");
            break;
        case ADYLoginDelegateStatusTokenValid:
            DebugLog(@"Login - Token valid");
            break;
        case ADYLoginDelegateStatusPostSettling:
            DebugLog(@"Login - Pre Settling");
            break;
        default:
            DebugLog(@"Login - Unknown login status");
            break;
    }
}

#pragma mark ADYDeviceManagerDelegate

- (void) deviceManagerWillBeginStep:(ADYEMVDeviceManagerStep)step
{
    switch (step) {
        case ADYEMVDeviceManagerStepIdentifyPaymentDevice:
            DebugLog(@"Boarding - started Identify Payment Device step");
            break;
        case ADYEMVDeviceManagerStepRegisterPaymentDevice:
            DebugLog(@"Boarding - started Register Payment Device step");
            break;
        case ADYEMVDeviceManagerStepLoadConfiguration:
            DebugLog(@"Boarding - started Load Configuration step");
            break;
        case ADYEMVDeviceManagerStepStoreConfiguration:
            DebugLog(@"Boarding - started Store Configuration step");
            break;
        case ADYEMVDeviceManagerStepCheckConfiguration:
            DebugLog(@"Boarding - started Check Configuration step");
            break;
        default:
            DebugLog(@"Boarding - started UNKNOWN step");
            break;
    }
}

- (void) deviceManagerDidCompleteStep:(ADYEMVDeviceManagerStep)step
{
    switch (step) {
        case ADYEMVDeviceManagerStepIdentifyPaymentDevice:
            DebugLog(@"Boarding - completed Identify Payment Device step");
            break;
        case ADYEMVDeviceManagerStepRegisterPaymentDevice:
            DebugLog(@"Boarding - completed Register Payment Device step");
            break;
        case ADYEMVDeviceManagerStepLoadConfiguration:
            DebugLog(@"Boarding - completed Load Configuration step");
            break;
        case ADYEMVDeviceManagerStepStoreConfiguration:
            DebugLog(@"Boarding - completed Store Configuration step");
            break;
        case ADYEMVDeviceManagerStepCheckConfiguration:
            DebugLog(@"Boarding - completed Check Configuration step");
            break;
        default:
            DebugLog(@"Boarding - completed UNKNOWN step");
            break;
    }
}

- (void) deviceManagerDidFailStep:(ADYEMVDeviceManagerStep)step
                        withError:(NSError*)error
{
    ErrorLog(@"Boarding failed - %@", [error localizedDescription]);
    [OCGProgressOverlay hide];
    UIAlertView *alert =
    [[UIAlertView alloc] initWithTitle:@"Error"
                               message:@"Device could not be boarded."
                              delegate:nil
                     cancelButtonTitle:@"OK"
                     otherButtonTitles:nil];
    alert.dismissBlock = ^(NSInteger buttonindex) {
        self.deviceDelegateCompleteBlock(kEFTProviderErrorCodeApplicationError, error, NO);
    };
    [alert show];
}

- (void) deviceManagerDidComplete
{
    DebugLog(@"Boarding successful");
    [OCGProgressOverlay hide];
    // boarded successfully
    self.deviceDelegateCompleteBlock(kEFTProviderErrorCodeSuccess, nil, NO);
}

#pragma mark ADYTransactionProcessorDelegate

- (void) transactionComplete:(ADYTransactionData *)transaction
{
    DebugLog(@"Transaction complete");
    // check if we are doing signature capture
    if (kEFTPaymentStatusSignatureRequired == self.manager.currentPayment.paymentStatus) {
        // dismiss the signature capture view controller
        [[self topMostController] dismissViewControllerAnimated:NO
                                                     completion:nil];
    }
    self.transactionTimeoutStart = nil;
    self.localizedProcessingStatus = nil;
    self.currentEFTRequest = nil;
    // make a note of the transaction
    self.manager.currentPayment.transaction = transaction;
    // update the payment
    EFTPayment *payment = self.manager.currentPayment;
    payment.referenceNumber = self.manager.currentPayment.uniqueMerchantReference;
    [self updateEFTPayment:payment
        fromADYTransaction:transaction];
    // payment complete
    if (self.transactionDelegateCompleteBlock) {
        if (ADYTenderStateError == transaction.finalState) {
            self.lastError = transaction.rawFailureReason;
            self.transactionDelegateCompleteBlock(kEFTProviderErrorCodeApplicationError, transaction.rawFailureReason, NO);
        } else {
            self.transactionDelegateCompleteBlock(kEFTProviderErrorCodeSuccess, nil, NO);
        }
    }
    self.transactionDelegateCompleteBlock = nil;
}

- (void) transactionRequiresSignature:(ADYSignatureRequest *)signatureRequest
{
    DebugLog(@"Signature required");
    [self resetTimeout];
    self.currentSignatureRequest = signatureRequest;
    // send the signature requested error back to the manager
    if (self.transactionDelegateCompleteBlock) {
        self.transactionDelegateCompleteBlock(kEFTProviderErrorCodeSignatureRequired, nil, NO);
    }
}

- (void) transactionProvidesAdditionalData:(ADYAdditionalDataRequest *)additionalDataRequest
{
    DebugLog(@"Additional data required");
    [self resetTimeout];
    // todo handle additional data required, just move on for now
    [additionalDataRequest continueWithUpdatedAmount:nil];
}

- (void) transactionRequiresPrintedReceipt:(ADYPrintReceiptRequest *)printReceiptRequest
{
    DebugLog(@"Print receipt");
    [self resetTimeout];
    // make a note of the receipts
    ADYReceipt *customerReceipt = [printReceiptRequest cardHolderReceipt];
    if (customerReceipt) {
        NSArray *receiptLines = customerReceipt.header;
        receiptLines = [receiptLines arrayByAddingObjectsFromArray:customerReceipt.content];
        receiptLines = [receiptLines arrayByAddingObjectsFromArray:customerReceipt.footer];
        self.manager.currentPayment.receiptCustomer = [ADYReceipt getReceiptFormatted:receiptLines];
    }
    ADYReceipt *merchantReceipt = [printReceiptRequest merchantReceipt];
    if (merchantReceipt) {
        NSArray *receiptLines = merchantReceipt.header;
        receiptLines = [receiptLines arrayByAddingObjectsFromArray:merchantReceipt.content];
        receiptLines = [receiptLines arrayByAddingObjectsFromArray:merchantReceipt.footer];
        self.manager.currentPayment.receiptMerchant = [ADYReceipt getReceiptFormatted:receiptLines];
    }
    // confirm that they are printed (or will be)
    [printReceiptRequest confirmReceiptPrinted:YES];
}

- (void) transactionSuggestsDcc:(ADYDynamicCurrencyConversionData *)data
{
    DebugLog(@"Suggests DCC");
    [self resetTimeout];
    // handle DCC
    [self updateCurrentPaymentWithDcc:data];
}

-(void)updateCurrentPaymentWithDcc:(ADYDynamicCurrencyConversionData *)data
{
    self.manager.currentPayment.dccOriginalAmount = data.originalAmountValue;
    self.manager.currentPayment.dccOriginalCurrency = data.originalAmountCurrency;
    self.manager.currentPayment.dccConvertedAmount = data.convertedAmountValue;
    self.manager.currentPayment.dccConvertedCurrency = data.convertedAmountCurrency;
    self.manager.currentPayment.dccExchangeRate = data.exchangeRate;
    self.manager.currentPayment.dccMarkup = data.markup;
    self.manager.currentPayment.dccCommissionFee = data.commissionFee;
    self.manager.currentPayment.dccSource = data.source;
}

- (void) transactionGratuityEntered:(int)gratuityAmountMinorUnits
{
    DebugLog(@"Gratuity entered");
    [self resetTimeout];
    // todo handle gratuity
}

- (void) transactionStateChanged:(ADYTenderState)state
{
    switch (state) {
        case ADYTenderStateInitial: {
            DebugLog(@"State changed to - Initial");
            self.localizedProcessingStatus = nil;
            break;
        }
        case ADYTenderStateCreated: {
            DebugLog(@"State changed to - Created");
            self.localizedProcessingStatus = nil;
            break;
        }
        case ADYTenderStateProcessing: {
            DebugLog(@"State changed to - Processing");
            self.localizedProcessingStatus = nil;
            break;
        }
        case ADYTenderStateAdditionalDataAvailable: {
            DebugLog(@"State changed to - Additional data available");
            self.localizedProcessingStatus = nil;
            break;
        }
        case ADYTenderStateWaitForAmountAdjustment: {
            DebugLog(@"State changed to - Amount adjustment");
            self.localizedProcessingStatus = nil;
            break;
        }
        case ADYTenderStateProvideCardDetails: {
            DebugLog(@"State changed to - Provide card details");
            self.localizedProcessingStatus = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠀸󠁐󠁤󠀱󠁍󠁳󠁢󠁚󠁩󠀫󠀫󠁉󠁳󠁬󠁘󠀹󠁅󠁦󠁌󠀴󠁥󠁢󠁮󠁆󠀷󠀯󠁧󠁿*/ @"Insert/swipe card", nil);
            break;
        }
        case ADYTenderStateCardDetailsProvided: {
            DebugLog(@"State changed to - Card details provided");
            self.localizedProcessingStatus = nil;
            break;
        }
        case ADYTenderStateAskSignature: {
            DebugLog(@"State changed to - Ask signature");
            self.localizedProcessingStatus = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁨󠁭󠁖󠁕󠀸󠁴󠀸󠁩󠁫󠁙󠁏󠁵󠁉󠀫󠀸󠁎󠁷󠁮󠁐󠁱󠁨󠁪󠁥󠁰󠀳󠁭󠀸󠁿*/ @"Signature required", nil);
            break;
        }
        case ADYTenderStateCheckSignature:{
            DebugLog(@"State changed to - Check signature");
            self.localizedProcessingStatus = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁨󠁭󠁖󠁕󠀸󠁴󠀸󠁩󠁫󠁙󠁏󠁵󠁉󠀫󠀸󠁎󠁷󠁮󠁐󠁱󠁨󠁪󠁥󠁰󠀳󠁭󠀸󠁿*/ @"Signature required", nil);
            break;
        }
        case ADYTenderStateSignatureChecked: {
            DebugLog(@"State changed to - Signature checked");
            self.localizedProcessingStatus = nil;
            break;
        }
        case ADYTenderStateReferral: {
            DebugLog(@"State changed to - Referral");
            self.localizedProcessingStatus = nil;
            break;
        }
        case ADYTenderStateReferralChecked: {
            DebugLog(@"State changed to - Referral checked");
            self.localizedProcessingStatus = nil;
            break;
        }
        case ADYTenderStatePrintReceipt: {
            DebugLog(@"State changed to - Print receipt");
            self.localizedProcessingStatus = nil;
            break;
        }
        case ADYTenderStateReceiptPrinted: {
            DebugLog(@"State changed to - Receipt printed");
            self.localizedProcessingStatus = nil;
            break;
        }
        case ADYTenderStateWaitingForAppSelection: {
            DebugLog(@"State changed to - Waiting for app selection");
            self.localizedProcessingStatus = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁧󠁡󠁫󠁄󠁰󠁖󠁗󠀶󠁒󠀫󠁊󠁸󠁈󠁙󠁪󠁋󠁡󠀰󠁨󠁥󠀹󠁇󠀹󠁮󠀯󠁓󠁉󠁿*/ @"App selection", nil);
            break;
        }
        case ADYTenderStateAppSelected: {
            DebugLog(@"State changed to - App selected");
            self.localizedProcessingStatus = nil;
            break;
        }
        case ADYTenderStateWaitingForPin: {
            DebugLog(@"State changed to - Waiting for PIN");
            self.localizedProcessingStatus = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁴󠁫󠁚󠁎󠁯󠁕󠁨󠁦󠁵󠁁󠁂󠁷󠁱󠁘󠁩󠁘󠁇󠀫󠁓󠀳󠀸󠁷󠁯󠁹󠁦󠀷󠁧󠁿*/ @"PIN Entry", nil);
            break;
        }
        case ADYTenderStatePinDigitEntered: {
            DebugLog(@"State changed to - PIN digit entered");
            self.localizedProcessingStatus = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁴󠁫󠁚󠁎󠁯󠁕󠁨󠁦󠁵󠁁󠁂󠁷󠁱󠁘󠁩󠁘󠁇󠀫󠁓󠀳󠀸󠁷󠁯󠁹󠁦󠀷󠁧󠁿*/ @"PIN Entry", nil);
            break;
        }
        case ADYTenderStatePinEntered: {
            DebugLog(@"State changed to - PIN entered");
            self.localizedProcessingStatus = nil;
            break;
        }
        case ADYTenderStateAskGratuity: {
            DebugLog(@"State changed to - Ask gratuity");
            self.localizedProcessingStatus = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁏󠁨󠁓󠁶󠁉󠁓󠁫󠁄󠁏󠁐󠁁󠁸󠀯󠀯󠁕󠁃󠁔󠁍󠀱󠁐󠁙󠀲󠁯󠁦󠁇󠀸󠁧󠁿*/ @"Gratuity", nil);
            break;
        }
        case ADYTenderStateGratuityEntered: {
            DebugLog(@"State changed to - Gratuity entered");
            self.localizedProcessingStatus = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁑󠁙󠁋󠁐󠁱󠁮󠀱󠁋󠁳󠀫󠁕󠁌󠁡󠁓󠀵󠁷󠀲󠀰󠁕󠀰󠀳󠁲󠁒󠁘󠁸󠁂󠁷󠁿*/ @"Gratuity entered", nil);
            break;
        }
        case ADYTenderStateAskDcc: {
            DebugLog(@"State changed to - Ask DCC");
            self.localizedProcessingStatus = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁮󠀶󠁓󠁁󠁐󠁐󠁪󠀷󠀲󠁤󠁌󠁚󠁓󠁁󠁏󠁲󠁕󠁡󠀷󠁦󠁸󠁩󠁯󠁎󠁭󠁸󠁙󠁿*/ @"Suggesting currency conversion", nil);
            break;
        }
        case ADYTenderStateDccAccepted: {
            DebugLog(@"State changed to - DCC accepted");
            self.localizedProcessingStatus = nil;
            break;
        }
        case ADYTenderStateDccRejected: {
            DebugLog(@"State changed to - DCC rejected");
            [self updateCurrentPaymentWithDcc:nil];
            self.localizedProcessingStatus = nil;
            break;
        }
        case ADYTenderStateCardSwiped: {
            DebugLog(@"State changed to - Card swiped");
            self.localizedProcessingStatus = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁱󠁯󠀰󠁨󠁸󠀴󠁪󠁬󠁒󠀸󠁗󠁒󠁄󠁪󠀫󠁨󠀫󠀲󠁎󠁔󠁎󠀫󠀴󠁶󠁓󠁃󠁣󠁿*/ @"Card swiped", nil);
            break;
        }
        case ADYTenderStateCardInserted: {
            DebugLog(@"State changed to - Card inserted");
            self.localizedProcessingStatus = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁫󠁡󠁳󠁭󠁫󠁙󠀵󠁫󠁮󠁔󠁫󠀷󠀵󠀰󠁏󠁍󠁇󠁗󠁇󠀲󠁓󠁭󠁒󠁅󠀴󠁡󠀰󠁿*/ @"Card inserted", nil);
            break;
        }
        case ADYTenderStateCardRemoved: {
            DebugLog(@"State changed to - Card removed");
            self.localizedProcessingStatus = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁯󠁶󠀳󠀯󠀷󠁸󠁵󠁏󠁥󠁊󠁃󠀳󠁺󠁊󠀶󠁘󠁔󠁦󠀸󠁧󠁓󠁎󠀫󠁱󠁕󠁓󠁳󠁿*/ @"Card removed", nil);
            break;
        }
        default:
            break;
    }
    [self resetTimeout];
}

- (void) transactionApplicationSelected:(NSString *)application
{
    DebugLog(@"Application selected: %@", application);
    [self resetTimeout];
}

- (void) transactionPinDigitEntered:(NSUInteger)numberOfDigitsEntered
{
    DebugLog(@"%i digits entered", numberOfDigitsEntered);
    [self resetTimeout];
}

- (void) transactionCardRemoved:(ADYCardStatus)status
{
    WarningLog(@"Card removed");
    [self resetTimeout];
}

- (void) transactionConnectionLost
{
    WarningLog(@"Connection lost");
    [self resetTimeout];
    self.localizedProcessingStatus = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁡󠁨󠁎󠁕󠀴󠁩󠁍󠁎󠁔󠀷󠁅󠁑󠁁󠁔󠁗󠀳󠁍󠁱󠀸󠀲󠁓󠁌󠁬󠁵󠀱󠁥󠁅󠁿*/ @"Connection lost", nil);
}

- (void) transactionConnectionRestored
{
    InfoLog(@"Connection restored");
    [self resetTimeout];
    self.localizedProcessingStatus = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠀵󠁐󠁋󠁓󠀲󠁉󠁱󠁶󠀸󠁵󠁘󠁨󠁦󠁭󠀹󠁢󠁨󠀵󠁋󠁮󠁈󠁰󠀴󠁆󠁎󠁶󠁫󠁿*/ @"Connection restored", nil);
}

#pragma mark - Properties

- (CGFloat) timeout
{
    CGFloat result = 5.0f;
    Device *EFTDevice = [[BasketController sharedInstance] deviceForSettingWithName:kOCGEFTManagerConfiguredPaymentProviderKey];
    if (EFTDevice) {
        NSString *timeoutValue = EFTDevice.timeout;
        if (timeoutValue.length > 0) {
            result = timeoutValue.doubleValue;
        }
    }
    
    return result;
}

@synthesize manager = _manager;

@synthesize localizedInitializingStatus = _localizedInitializingStatus;
- (void) setLocalizedInitializingStatus:(NSString *)localizedInitializingStatus
{
    if (![_localizedInitializingStatus isEqualToString:localizedInitializingStatus]) {
        _localizedInitializingStatus = localizedInitializingStatus;
        [[NSNotificationCenter defaultCenter] postNotificationName:kEFTNotificationTransactionChanged
                                                            object:nil];
    }
}

@synthesize localizedProcessingStatus = _localizedProcessingStatus;
- (void) setLocalizedProcessingStatus:(NSString *)localizedProcessingStatus
{
    if (![_localizedProcessingStatus isEqualToString:localizedProcessingStatus]) {
        _localizedProcessingStatus = localizedProcessingStatus;
        [[NSNotificationCenter defaultCenter] postNotificationName:kEFTNotificationTransactionChanged
                                                            object:nil];
    }
}

#pragma mark - OCGEFTProviderProtocol

- (instancetype) initForManager:(OCGEFTManager *)manager
{
    self = [super init];
    if (self) {
        _manager = manager;
        self.timeoutStart = nil;
        self.remoteDeviceSelectionDataSource = [[OCGEFTAdyenDeviceSelectionRDMDatasource alloc] init];
    }
    
    return self;
}
    
- (BOOL) isURLProvider
{
    return NO;
}

- (ServerError *) getLastServerError
{
    ServerError *error = nil;
    
    // if we have an error
    if (self.lastError) {
        // get error keys
        NSInteger errorCode = self.lastError.code;
        NSString *errorText = [self.lastError localizedDescription];
        // if this is an Adyen error
        if ([self.lastError.domain isEqualToString:ADYErrorDomain]) {
            errorText = [self localizedDescriptionForErrorCode:errorCode];
            NSError *underlyingError = [self.lastError.userInfo valueForKey:NSUnderlyingErrorKey];
            if (underlyingError) {
                errorText = [errorText stringByAppendingFormat:@"\n%@", underlyingError.localizedDescription];
            }
        }

        // build the error
        ServerErrorMessage *message = [[ServerErrorMessage alloc] init];
        message.code = [NSNumber numberWithInteger:errorCode];
        message.value = errorText;
        error = [[ServerError alloc] init];
        error.messages = [NSArray arrayWithObject:message];
    }
    
    // return
    return error;
}

- (NSString *) additionalSetupDescription
{
    return NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠁦󠁌󠁱󠁢󠁌󠁕󠁆󠁙󠁔󠁹󠀳󠁒󠁃󠁡󠁘󠁁󠁁󠁶󠁌󠁮󠁬󠁱󠀫󠁔󠁐󠁙󠁍󠁿*/ @"EFT Device", nil);
}

- (NSString *) additionalSetupValue
{
    NSString *result = [[OCGSettingsController sharedInstance] valueForSettingWithName:kOCGEFTProviderAdyenDeviceConfigKey];
    
    if (result.length == 0) {
        result = NSLocalizedString(/*󠀁󠁸󠀭󠁯󠁣󠁧󠀭󠁳󠁨󠁡󠀱󠀭󠀰󠁐󠁍󠁚󠁇󠁡󠁇󠁡󠁋󠁨󠁔󠁯󠀲󠁶󠁱󠁍󠀲󠁯󠀳󠁙󠁍󠁷󠁪󠀫󠁍󠁪󠁑󠁿*/ @"None", nil);
    }
    
    return result;
}

- (void) additionalSetupComplete:(dispatch_block_t)complete
{
    self.rememberSelection = YES;
    [self selectDeviceAllowSelection:YES
                            complete:^(ADYDevice *device) {
                                self.currentEFTDevice = device;
                                complete();
                                [self ensureDeviceReadyComplete:^(EFTProviderErrorCode resultCode, NSError *error, BOOL suppressError) {
                                    if (kEFTProviderErrorCodeSuccess != resultCode) {
                                        WarningLog(@"Device not ready");
                                    }
                                }];
                            }];
}

- (void) preflightCheckForTransaction:(BOOL)forTx
                             complete:(EFTProviderCompleteBlock)complete
{
    // check that we are logged in
    [self loginCheckComplete:^(EFTProviderErrorCode resultCode, NSError *error, BOOL suppressError) {
        if (kEFTProviderErrorCodeSuccess == resultCode) {
            // check the device is configured and initialised
            [self deviceCheckForTx:forTx
                          complete:^(EFTProviderErrorCode resultCode, NSError *error, BOOL suppressError) {
                if (kEFTProviderErrorCodeSuccess == resultCode) {
                    // ready
                    complete(kEFTProviderErrorCodeSuccess, nil, NO);
                } else {
                    // cannot take payments due to device configuration error
                    self.lastError = error;
                    complete(kEFTProviderErrorCodeApplicationError, error, suppressError);
                }
            }];
        } else {
            // cannot take payments due to login failure
            self.lastError = error;
            complete(kEFTProviderErrorCodeApplicationError, error, suppressError);
        }
    }];
}

- (void) cardSaleWithTender:(Tender *)tender
                     amount:(NSNumber *)amount
            forBasketWithID:(NSString *)basketId
          withCustomerEmail:(NSString *)customerEmail
                   complete:(EFTProviderCompleteBlock)complete
{
    // process the transaction
    [self processTransactionComplete:complete];
}


- (void) cardRefundWithTender:(Tender *)tender
                   withAmount:(NSNumber *)amount
              forBasketWithID:(NSString *)basketId
            withCustomerEmail:(NSString *)customerEmail
                     complete:(EFTProviderCompleteBlock)complete
{
    // process the transaction
    [self processTransactionComplete:complete];
}
    
- (void) resumeTransactionComplete:(EFTProviderTransactionCompleteBlock)complete
{
    EFTPaymentCompleteReason reason = kEFTPaymentCompleteReasonNotComplete;
    if (self.manager.currentPayment.transaction) {
        ADYTransactionData *transaction = self.manager.currentPayment.transaction;
        switch (transaction.finalState) {
            case ADYFinalTenderStateNotFinal:
                reason = kEFTPaymentCompleteReasonNotComplete;
                break;
            case ADYFinalTenderStateApproved:
                reason = kEFTPaymentCompleteReasonApproved;
                break;
            case ADYFinalTenderStateDeclined:
                reason = kEFTPaymentCompleteReasonDeclined;
                break;
            case ADYFinalTenderStateCancelled:
                reason = kEFTPaymentCompleteReasonCancelled;
                break;
            case ADYFinalTenderStateError: {
                reason = kEFTPaymentCompleteReasonError;
                self.lastError = transaction.errorInfo;
                break;
            }
        }
        complete(reason, kEFTProviderErrorCodeSuccess, nil);
    } else {
        NSError *error = [NSError errorWithDomain:ADYErrorDomain
                                             code:ADYErrorCodeUnknownError
                                         userInfo:nil];
        self.lastError = error;
        complete(reason, kEFTProviderErrorCodeApplicationError, error);
    }
}

- (void) cancelTx
{
    // check if there is a request being processed
    if (self.currentEFTRequest) {
        // request cancellation
        [self.currentEFTRequest requestCancel];
    } else if (kEFTPaymentStatusInitializing == self.manager.currentPayment.paymentStatus) {
//        self.currentEFTDevice.status = ADYDeviceStatusError;
//        if (self.deviceSelectedBlock) self.deviceSelectedBlock(nil);
        self.timeoutStart = [NSDate distantPast];
    }
}

- (BOOL) showRemoveCardAlert
{
    if (self.manager.currentPayment.transaction) {
        return ((ADYTransactionData *)self.manager.currentPayment.transaction).cardNeedsToBeRemovedAfterTransaction;
    }
    return NO;
}

- (NSString *) RawStringForTransaction
{
    NSString *result = nil;
    
    ADYTransactionData *transaction = self.manager.currentPayment.transaction;
    
    // first, iterate through each property of the transaction and build a dictionary
    NSMutableDictionary *transactionDictionary =
    [[transaction dictionaryWithValuesForKeys:[OCGEFTProviderAdyen transactionKeys]] mutableCopy];
    [transactionDictionary setObject:@(transaction.transactionType) forKey:@"transactionType"];
    [transactionDictionary setObject:@(transaction.cardScheme) forKey:@"cardScheme"];
    [transactionDictionary setObject:@(transaction.cardNeedsToBeRemovedAfterTransaction) forKey:@"cardNeedsToBeRemovedAfterTransaction"];
    [transactionDictionary setObject:@(transaction.finalState) forKey:@"finalState"];
    [transactionDictionary setObject:@(transaction.processingState) forKey:@"processingState"];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.dateFormat = @"yyyyMMdd";
    [transactionDictionary setObject:[formatter stringFromDate:transaction.dateSubmitted] forKey:@"dateSubmitted"];
    [transactionDictionary setObject:@(transaction.failureReasonCode) forKey:@"failureReasonCode"];
    [transactionDictionary setObject:@(transaction.needsReferralAuthorisation) forKey:@"needsReferralAuthorisation"];
    
    /*
     @property (nonatomic, strong) CLLocation* location;
     @property (nonatomic, strong) NSError *rawFailureReason;
     */
    
    if (transactionDictionary) {
        // serialise the dictionary
        NSError *error = nil;
        NSData *JSONData =
        [NSJSONSerialization dataWithJSONObject:transactionDictionary
                                        options:0
                                          error:&error];
        if (JSONData) {
            result = [[NSString alloc] initWithData:JSONData
                                           encoding:NSUTF8StringEncoding];
        } else {
            ErrorLog(@"Could not generate JSON from transaction: %@", [error localizedDescription]);
        }
    } else {
        result = nil;
    }
    
    return result;
}

- (BOOL) voidsAllowed
{
    return YES;
}

- (void) voidTransactionWithReference:(NSString *)originalTxReference
                             complete:(EFTProviderCompleteBlock)complete
{
    // look up transaction and void it
    Adyen *adyen = [Adyen sharedInstance];
    // make sure we are logged in
    [self loginCheckComplete:^(EFTProviderErrorCode resultCode, NSError *error, BOOL suppressError) {
        if (kEFTProviderErrorCodeSuccess == resultCode) {
            NSArray *transactions = [adyen getTransactionsWithSearch:originalTxReference];
            if (transactions) {
                ADYTransactionData *voidTransaction = nil;
                for (ADYTransactionData *transaction in transactions) {
                    if ([transaction.merchantReference isEqualToString:originalTxReference]) {
                        voidTransaction = transaction;
                        break;
                    }
                }
                if (voidTransaction) {
                    // found the transaction, refund it
                    NSString *voidReference = [voidTransaction.merchantReference stringByAppendingString:@"-VOID"];
                    ADYRefundRequest *request =
                    [[ADYRefundRequest alloc] initWithTransaction:voidTransaction
                                                           amount:voidTransaction.finalAmountValue
                                                        reference:voidReference];
                    [request startWithCompletion:^(ADYProcessingState refundState, NSError *error) {
                        if (refundState != ADYProcessingStateReversalError) {
                            // make a note of the transaction
                            self.manager.currentPayment.transaction = voidTransaction;
                            // update
                            self.manager.currentPayment.referenceNumber = voidReference;
                            [self updateEFTPayment:self.manager.currentPayment
                                fromADYTransaction:voidTransaction];
                            complete(kEFTProviderErrorCodeSuccess, nil, NO);
                        } else {
                            self.lastError = error;
                            complete(kEFTProviderErrorCodeApplicationError, error, suppressError);
                        }
                    }];
                } else {
                    ErrorLog(@"Void - Could not find transaction for %@", originalTxReference);
                    [self returnErrorWithErrorCode:ADYErrorCodeUnknownError
                                           toBlock:complete];
                }
            } else {
                // transactions is nil, probably not logged in
                ErrorLog(@"Void - Could not get transactions");
                [self returnErrorWithErrorCode:ADYErrorCodeNotLoggedIn
                                       toBlock:complete];
            }
        } else {
            // cannot take payments due to login failure
            self.lastError = error;
            complete(kEFTProviderErrorCodeApplicationError, error, suppressError);
        }
    }];
}

- (NSString *) localizedProviderStatus
{
    NSString *result = nil;
    
    if (kEFTPaymentStatusInitializing == self.manager.currentPayment.paymentStatus) {
        result = self.localizedInitializingStatus;
    } else if (kEFTPaymentStatusProcessing == self.manager.currentPayment.paymentStatus) {
        result = self.localizedProcessingStatus;
    }
    
    return result;
}

- (void) confirmSignature:(UIImage *)signature
                 complete:(EFTProviderCompleteBlock)complete
{
    if (self.currentSignatureRequest) {
        self.transactionDelegateCompleteBlock = complete;
        [self.currentSignatureRequest submitConfirmedSignature:signature];
    } else {
        ErrorLog(@"Got confirm signature when not expected");
        [self returnErrorWithErrorCode:ADYErrorCodeUnknownError
                               toBlock:complete];
    }
}

- (void) unconfirmSignature:(UIImage *)signature
                   complete:(EFTProviderCompleteBlock)complete
{
    if (self.currentSignatureRequest) {
        self.transactionDelegateCompleteBlock = complete;
        [self.currentSignatureRequest submitUnconfirmedSignature:signature];
    } else {
        ErrorLog(@"Got confirm signature when not expected");
        [self returnErrorWithErrorCode:ADYErrorCodeUnknownError
                               toBlock:complete];
    }
}

#pragma mark - DeviceSelectionViewControllerDataSource

- (NSString *) deviceSelectionControllerUsageText:(DeviceSelectionViewController *)deviceSelectionViewController
{
    return [self.remoteDeviceSelectionDataSource deviceSelectionControllerUsageText:deviceSelectionViewController];
}

- (Device *) deviceSelectionViewControllerDeviceClass:(DeviceSelectionViewController *)deviceSelectionViewController
{
    return [self.remoteDeviceSelectionDataSource deviceSelectionViewControllerDeviceClass:deviceSelectionViewController];
}

- (void) deviceSelectionController:(DeviceSelectionViewController *)deviceSelectionController
            refreshDevicesComplete:(DeviceRefreshBlock)complete
                    currentDevices:(NSArray *)currentDevices
{
    NSArray *devices = [self getDeviceList];
    // update notifications
    // remove old
    for (id device in currentDevices) {
        [device removeObserver:deviceSelectionController
                    forKeyPath:@"status"
                       context:NULL];
    }
    // add new
    for (id device in devices) {
        [device addObserver:deviceSelectionController
                 forKeyPath:@"status"
                    options:NSKeyValueObservingOptionNew
                    context:NULL];
    }
    // done
    complete(devices);
}

- (BOOL) deviceSelectionController:(DeviceSelectionViewController *)deviceSelectionController
                         getStatus:(NSString **)status
                          ofDevice:(id)device
{
    return [self.remoteDeviceSelectionDataSource deviceSelectionController:deviceSelectionController
                                                                 getStatus:status
                                                                  ofDevice:device];
}

- (NSString *) deviceSelectionController:(DeviceSelectionViewController *)deviceSelectionController
                            nameOfDevice:(id)device
{
    return [self.remoteDeviceSelectionDataSource deviceSelectionController:deviceSelectionController
                                                              nameOfDevice:device];
}

#pragma mark - DeviceSelectionViewControllerDelegate

- (void) deviceSelectionViewController:(DeviceSelectionViewController *)controller
                       didSelectDevice:(id)device
{
    if (self.rememberSelection && device) {
        [[OCGSettingsController sharedInstance] setValue:((ADYDevice *)device).name
                                      forSettingWithName:kOCGEFTProviderAdyenDeviceConfigKey];
    }
    [[self topMostController] dismissViewControllerAnimated:YES
                                                 completion:^{
                                                     ADYDeviceRegistry *deviceRegistry = [[Adyen sharedInstance] deviceRegistry];
                                                     [deviceRegistry stopPolling];
                                                     [deviceRegistry removeObserver:controller
                                                                         forKeyPath:@"devices"];
                                                     for (id device in deviceRegistry.devices) {
                                                         [device removeObserver:controller
                                                                     forKeyPath:@"status"
                                                                        context:NULL];
                                                     }
                                                     if (self.deviceSelectedBlock) self.deviceSelectedBlock(device);
                                                 }];
}

- (void) deviceSelectionViewController:(DeviceSelectionViewController *)controller
                         didScanDevice:(NSString *)deviceBarcode
{
    OCGEFTAdyenDeviceSelectionRDMDatasource *datasource = controller.dataSource;
    [datasource deviceForBarcode:deviceBarcode
                        complete:^(ADYDevice *adyenDevice) {
                            if (adyenDevice) {
                                [self deviceSelectionViewController:controller
                                                    didSelectDevice:adyenDevice];
                            }
                        }];
}
    
#pragma mark - Methods
    
+ (NSArray *) transactionKeys
{
    return @[@"mid",
             @"tid",
             @"posEntryMode",
             @"txRef",
             @"tenderReference",
             @"shopperReference",
             @"shopperEmail",
             @"amountValue",
             @"amountCurrency",
             @"adjustedAmountValue",
             @"adjustedAmountCurrency",
             @"gratuityAmountValue",
             @"gratuityAmountCurrency",
             @"additionalAmountValue",
             @"additionalAmountCurrency",
             @"authAmountValue",
             @"authAmountCurrency",
             @"finalAmountValue",
             @"finalAmountCurrency",
             @"aidCode",
             @"applicationPreferredName",
             @"merchantReference",
             @"taxPercentage",
             @"cardAlias",
             @"cardType",
             @"cardNumber",
             @"cardSeq",
             @"cardHolderName",
             @"cardExpiryYear",
             @"cardExpiryMonth",
             @"cardEffectiveYear",
             @"cardEffectiveMonth",
             @"paymentVerificationData",
             @"issuerCountry",
             @"cardBin",
             @"applicationLabel",
             @"cardHolderVerificationMethodResults",
             @"txDate",
             @"txTime",
             @"cardString",
             @"finalStateString",
             @"pspAuthCode",
             @"pspReference",
             @"terminalId",
             @"signatureData",
             @"logoReceipt",
             @"refundReason"];
}

@end
