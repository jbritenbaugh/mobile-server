//
//  OCGModifierCell.h
//  mPOS
//
//  Created by Antonio Strijdom on 17/09/2014.
//  Copyright (c) 2014 Clarity. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OCGModifierCell : UICollectionViewCell

@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UILabel *priceLabel;

@end
