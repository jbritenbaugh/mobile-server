UIAKeyboard.prototype["KEYBOARD_TYPE_UNKNOWN"] = -1;
UIAKeyboard.prototype["KEYBOARD_TYPE_ALPHA"] = 0;
UIAKeyboard.prototype["KEYBOARD_TYPE_ALPHA_CAPS"] = 1;
UIAKeyboard.prototype["KEYBOARD_TYPE_NUMBER_AND_PUNCTUATION"] = 2;
UIAKeyboard.prototype["KEYBOARD_TYPE_NUMBER_PAD"] = 3;
UIAKeyboard.prototype["keyboardType"] = function() {
	if (this.keys().firstWithName("a").toString() != "[object UIAElementNil]")
		return this.KEYBOARD_TYPE_ALPHA;
	else if (this.keys().firstWithName("A").toString() != "[object UIAElementNil]")
		return this.KEYBOARD_TYPE_ALPHA_CAPS;
	else if (this.keys().firstWithName("1").toString() != "[object UIAElementNil]") {
		if (this.keys().firstWithName(",").toString() != "[object UIAElementNil]") {
			return this.KEYBOARD_TYPE_NUMBER_AND_PUNCTUATION;
		} else {
			return this.KEYBOARD_TYPE_NUMBER_PAD;	
		}
	} else
		return this.KEYBOARD_TYPE_UNKNOWN;
};

UIATextField.prototype["typeString"] = function(pstrString, pbClear)
{	
	UIATarget.localTarget().delay(0.5);
	
	if (pbClear || pstrString.length == 0)
		this.clear();
	
	if (pstrString.length > 0) {
		var app = UIATarget.localTarget().frontMostApp();
		var keyboard = app.keyboard();
		var intKeyboardType = keyboard.keyboardType();
		var bIsAllCaps = (intKeyboardType == keyboard.KEYBOARD_TYPE_ALPHA_CAPS);
		var intNewKeyboardType = intKeyboardType;
		UIALogger.logMessage("Keyboard type : " + intNewKeyboardType);
		var keys = app.keyboard().keys();
		var buttons = app.keyboard().buttons();
		for (var charIndex = 0; charIndex < pstrString.length; charIndex ++) {
			var strChar = pstrString.charAt(charIndex);
			if ((/[a-z]/.test(strChar)) && intKeyboardType == keyboard.KEYBOARD_TYPE_ALPHA_CAPS && !bIsAllCaps) {
				buttons.firstWithName("shift").tap();
				intKeyboardType = keyboard.KEYBOARD_TYPE_ALPHA;
			} else if ((/[A-Z]/.test(strChar)) && intKeyboardType == keyboard.KEYBOARD_TYPE_ALPHA) {
				buttons.firstWithName("shift").tap();
				intKeyboardType = keyboard.KEYBOARD_TYPE_ALPHA_CAPS;
			} else if ((/[A-z]/.test(strChar)) && intKeyboardType == keyboard.KEYBOARD_TYPE_NUMBER_AND_PUNCTUATION) {
				buttons.firstWithName("more, letters").tap();
				intKeyboardType = keyboard.KEYBOARD_TYPE_ALPHA;
			} else if ((/[0-9.]/.test(strChar)) && intKeyboardType != keyboard.KEYBOARD_TYPE_NUMBER_AND_PUNCTUATION && intKeyboardType != keyboard.KEYBOARD_TYPE_NUMBER_PAD) {
				buttons.firstWithName("more, numbers").tap();
				intKeyboardType = keyboard.KEYBOARD_TYPE_NUMBER_AND_PUNCTUATION;
			}
			
			if ((/[a-z]/.test(strChar)) && intKeyboardType == keyboard.KEYBOARD_TYPE_ALPHA_CAPS)
				strChar = strChar.toUpperCase();
			if (strChar == " ") {
				keys["space"].tap();
			} else if (/[0-9]/.test(strChar)) {
				if (strChar == "0") {
					if (intKeyboardType != keyboard.KEYBOARD_TYPE_NUMBER_PAD) {
						strChar = "9";
					} else {
						strChar = "11";	
					}
				} else {
					strChar = (parseInt(strChar) - 1).toString();
				}
				keys[strChar].tap()
			} else {
				keys[strChar].tap();
			}
			UIATarget.localTarget().delay(0.5);
		}
	}
};

var found = UIAElementNil;

function logFirstResponderElement(element)
{
	//UIALogger.logDebug("Element " + element + " has " + element.elements().length + " elements");
	if (element.hasKeyboardFocus()) {
		found = element;
		UIALogger.logWarning("Element " + element + " in " + element.parent() + " has focus");
		element.parent().logElementTree();
		element.logElement();
	} else {
		for (var i = 0; i < element.elements().length; i++) {
			var subElement = element.elements()[i];
			logFirstResponderElement(subElement);
			if (found != UIAElementNil) break;
		}
	}
}

function logFirstResponder() 
{
	UIALogger.logDebug("Finding first responder...");
	var target = UIATarget.localTarget();
	target.setTimeout(0);
	// get the app
	var app = target.frontMostApp();
	
	// loop through windows
	found = UIAElementNil;
	for (var i = 0; i < app.windows().length; i++) {
		var window = app.windows()[i];
		//UIALogger.logDebug("Window " + window + " has " + window.elements().length + " elements");
		for (var j = 0; j < window.elements().length; j++) {
			var element = window.elements()[j];
			logFirstResponderElement(element);
			if (found != UIAElementNil) {
				window.logElement();
				break;
			}
		}
	}
	
	if (found == UIAElementNil) {
		UIALogger.logWarning("No first responder found");	
	}
}

function waitForSync()
{
	// get the target device
	var target = UIATarget.localTarget();
	// get the app
	var app = target.frontMostApp();
	// get the main window
	var window = app.mainWindow();
	// get the syncing label
	var syncingLabel = window.staticTexts()["syncing"];
	// wait for syncing
	while (syncingLabel.isVisible()) {
		target.delay(1);
	}
	target.delay(1);
}

function areElementsEqual(element1, element2)
{
	var element1Rect = element1.rect();
	var element2Rect = element2.rect();
	if ((element1Rect.origin.x == element2Rect.origin.x) && 
		(element1Rect.origin.y == element2Rect.origin.y) &&
		(element1Rect.size.width == element2Rect.size.width) &&
		(element1Rect.size.height == element2Rect.size.height)) {
		return (element1.name === element2.name);
	} else {
		return false;
	}
}

// setup the alert handler
UIATarget.onAlert = function onAlert(alert) {
	// log the current first responder
	logFirstResponder();
	var title = alert.name();
	UIALogger.logWarning("Alert with title '" + title + "' encountered");
	alert.logElementTree();
	if (title == "Enter a Barcode or SKU") {
		// if there is no total yet, add an item
		if (itemAdded == false) {
			alert.textFields()[0].setValue("24");
			alert.buttons()["Submit"].tap();
			itemAdded = true;
		} else {
			// otherwise cancel
			alert.buttons()["Cancel"].tap();
			scanningBarcode = false;
		}
		return true;
	} else if (title == "Discard Sale") {
		alert.buttons()["Discard"].tap();
		discarding = false;
		return true;	
	} else if (title == "Fake Payment") {
		alert.buttons()["Success"].tap();
		paying = false;
		return true;
	} else if (title == "Email the receipt") {
		alert.textFields()[0].setValue("astrijdom@omnicogroup.com");
		alert.buttons()["Send"].tap();
		emailing = false;
		return true;	
	}
	UIALogger.logFail("Unexpected error");
	return false;	
}

for (var i = 1; i <= 20; i++) {
	UIALogger.logStart("Starting test " + i);
	// init state
	var itemAdded = false;
	var scanningBarcode = false;
	var paying = false;
	var emailing = false;
	// get the target device
	var target = UIATarget.localTarget();
	// get the app
	var app = target.frontMostApp();
	// get the main window
	var window = app.mainWindow();
	
	waitForSync();
	UIALogger.logDebug("Pushing scan button");
	// get the scan button
	var scanButton = window.buttons()["Scan Barcode"];
	while (scanButton.toString() == "[object UIAElementNil]") {
		window = app.mainWindow();
		scanButton = window.buttons()["Scan Barcode"];
		target.delay(1);
	}
	// tap the scan button
	scanningBarcode = true;
	scanButton.tap();
	UIALogger.logDebug("Typing in barcode number");
	while (scanningBarcode == true) {
		target.delay(1);
	}
	target.delay(1);
	// tap on the basket item
	var basketTable = window.tableViews()[0];
	var basketItemCell = basketTable.cells()[0];
	basketItemCell.tap();
	target.delay(1);
//	for (var j = 1; j <= 1; j++) {
		// tap on the discount option
		var operationsTable = window.tableViews()[0];
		var discountOperationCell = operationsTable.cells()["Add discount"];
		discountOperationCell.tap();
		target.delay(1);
		// tap on the code option
		var discountTable = window.tableViews()[0];
		var discountReasonCell = discountTable.cells()["Code, None"];
		discountReasonCell.tap();
		target.delay(1);
		// tap on discount %
		var discountCodeTable = window.tableViews()[0];
		var discountPercentCell = discountCodeTable.cells()[7];
		discountPercentCell.tap();
		target.delay(1);
		// log the current first responder
		logFirstResponder();
		// try and enter something
		discountTable = window.tableViews()[0];
		var discountValueCell = discountTable.cells()["Discount (%)"];
		var discountValueTextField = discountValueCell.textFields()[0];
		if (areElementsEqual(found, discountValueTextField)) {
			discountValueTextField.typeString("500");
			target.delay(1);
			// confirm
			var confirmCancelCell = discountTable.cells()["Cancel"];
			var confirmButton = confirmCancelCell.buttons()["Add Discount"];
			confirmButton.tap();
			waitForSync();
			target.delay(2);
			// go back
			window.navigationBar().leftButton().tap();
			target.delay(1);
			// log the current first responder
			logFirstResponder();
			// show the menu
			// get the reveal menu button
			var revealMenuButton = window.buttons()["Reveal menu"];
			revealMenuButton.tap();
			target.delay(1);
			// check out
			var checkoutButton = window.buttons()["Checkout"];
			checkoutButton.tap();
			waitForSync();
			// pay
			var fakePaymentButton = window.buttons()["Fake"];
			while (fakePaymentButton.toString() == "[object UIAElementNil]") {
				fakePaymentButton = window.buttons()["Fake"];
				target.delay(1);
			}
			target.delay(1);
			paying = true;
			fakePaymentButton.tap();
			while (paying == true) {
				target.delay(1);
			}
			waitForSync();
//			var finishButton = window.buttons()["Finish"];
//			finishButton.tap();
			var emailButton = window.buttons()["Email\nReceipt"];
			while (emailButton.toString() == "[object UIAElementNil]") {
				emailButton = window.buttons()["Email\nReceipt"];
				target.delay(1);
			}
			emailing = true;
			emailButton.tap();
			while (emailing == true) {
				target.delay(1);	
			}
			waitForSync();
			UIALogger.logPass();

		} else {
			found.logElement();
			found.parent.logElementTree();
			discountValueTextField.logElement();
			UIALogger.logFail("Expected " + discountValueTextField + " to be first responder, but it is actually " + found);
			break;
		}
//	}

}