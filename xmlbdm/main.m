//
//  main.m
//  xmlbdm
//
//  Created by John Scott on 20/04/2015.
//  Copyright (c) 2015 Clarity. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface Node : NSObject <NSCopying>

@property (nonatomic, copy) NSString *key;
@property (nonatomic, copy) NSString *value;
@property (nonatomic, copy) NSString *optionSet;

@end

@implementation Node

-(BOOL)isEqual:(Node*)object
{
    return [object isKindOfClass:self.class]
    && (self.key == object.key || [self.key isEqualToString:object.key])
    && (self.value == object.value || [self.value isEqualToString:object.value])
    && (self.optionSet == object.optionSet || [self.optionSet isEqualToString:object.optionSet])
    ;
}

-(id)copyWithZone:(NSZone *)zone
{
    Node *copy = [[Node allocWithZone:zone] init];
    copy.key = self.key;
    copy.value = self.value;
    copy.optionSet = self.optionSet;
    return copy;
}

-(NSString *)description
{
    return [NSString stringWithFormat:@"%@ -- %@ -- %@", self.key, self.value, self.optionSet];
}

-(NSUInteger)hash
{
    return self.description.hash;
}

- (NSComparisonResult)compare:(Node*)object
{
    NSComparisonResult result = NSOrderedSame;
    if (result == NSOrderedSame)
    {
        result = [self.key compare:object.key];
    }
    if (result == NSOrderedSame)
    {
        result = [self.optionSet compare:object.optionSet];
    }
    if (result == NSOrderedSame)
    {
        result = [self.value compare:object.value];
    }
    return result;
}

- (NSComparisonResult)localizedCaseInsensitiveCompare:(Node*)object
{
    NSComparisonResult result = NSOrderedSame;
    if (result == NSOrderedSame)
    {
        result = [self.key localizedCaseInsensitiveCompare:object.key];
    }
    if (result == NSOrderedSame)
    {
        result = [self.optionSet localizedCaseInsensitiveCompare:object.optionSet];
    }
    if (result == NSOrderedSame)
    {
        result = [self.value localizedCaseInsensitiveCompare:object.value];
    }
    return result;}


@end

void getOptionSets(NSDictionary *nodes, NSMutableSet* buffer)
{
    for (Node *node in nodes.allKeys)
    {
        getOptionSets(nodes[node], buffer);
        if (node.optionSet.length > 0)
        {
            [buffer addObject:node.optionSet];
        }
    }
}

NSString *escapeString(NSString *value)
{
    NSMutableString *result = [NSMutableString stringWithString:value];
    [result replaceOccurrencesOfString:@"&"  withString:@"&amp;"  options:NSLiteralSearch range:NSMakeRange(0, [result length])];
    [result replaceOccurrencesOfString:@"\"" withString:@"&quot;" options:NSLiteralSearch range:NSMakeRange(0, [result length])];
    [result replaceOccurrencesOfString:@"'"  withString:@"&#x27;" options:NSLiteralSearch range:NSMakeRange(0, [result length])];
    [result replaceOccurrencesOfString:@">"  withString:@"&gt;"   options:NSLiteralSearch range:NSMakeRange(0, [result length])];
    [result replaceOccurrencesOfString:@"<"  withString:@"&lt;"   options:NSLiteralSearch range:NSMakeRange(0, [result length])];
    
    return result;
}


void dumpNodes(NSDictionary *nodes, NSInteger depth, NSMutableString* buffer, BOOL optionSetSet, BOOL insertNewlines)
{
    NSArray *nodeKeys = [nodes.allKeys sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
    
    for (Node *node in nodeKeys)
    {
        if (insertNewlines)
        {
            [buffer appendString:[@"" stringByPaddingToLength:depth*2 withString:@" " startingAtIndex:0]];
        }
        [buffer appendFormat:@"<%@", node.key];
    
        NSMutableSet *optionSets = [NSMutableSet set];
        
        if (node.optionSet.length > 0)
        {
            [optionSets addObject:node.optionSet];
        }
        
        NSDictionary *subnodes = nodes[node];
        
        if (subnodes.count)
        {
            getOptionSets(subnodes, optionSets);

            if (node.value.length > 0)
            {
                [buffer appendFormat:@" key='%@'", escapeString(node.value)];
            }
        }
        
        BOOL subNodesOptionSetSet = optionSetSet;
        BOOL subNodesInsertNewlines = insertNewlines;
        NSArray *inlineKeys = @[
                                @"Inject",
                                @"Language",
                                @"KeyTheme",
                                ];
        
        if ([inlineKeys containsObject:node.key] && subnodes.count < 4)
        {
            subNodesInsertNewlines = NO;
        }
        
        if (optionSets.count == 1 && !subNodesOptionSetSet)
        {
            [buffer appendFormat:@" set='%@'", escapeString(optionSets.anyObject)];
            subNodesOptionSetSet = YES;
        }
        
        
        if (subnodes.count)
        {
            [buffer appendString:@">"];
            if (subNodesInsertNewlines)
            {
                [buffer appendString:@"\n"];
            }
            dumpNodes(subnodes, depth+1, buffer, subNodesOptionSetSet, subNodesInsertNewlines);
            if (subNodesInsertNewlines)
            {
                [buffer appendString:[@"" stringByPaddingToLength:depth*2 withString:@" " startingAtIndex:0]];
            }
        }
        else
        {
            [buffer appendString:@">"];
            [buffer appendString:escapeString(node.value)];
        }
        [buffer appendFormat:@"</%@>", node.key];
        if (insertNewlines)
        {
            [buffer appendString:@"\n"];
        }
    }
}

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        
//        NSMutableArray *args = [NSMutableArray array];
//        for (NSUInteger i=0; i<argc; i++)
//        {
//            [args addObject:[NSString stringWithUTF8String:argv[i]]];
//        }
//        
//        NSLog(@"%@", args);
//        return 1;
        
        

        
        
        NSMutableDictionary *root = [NSMutableDictionary dictionary];
        
        NSRegularExpression *dataValueMatcher =
        [NSRegularExpression regularExpressionWithPattern:@"(?:\\\\([^\\[\\\\]+)(?:\\[([^\\]]*)\\])?)"
                                                  options:0
                                                    error:NULL];
        
        NSError *error;
        
        {
            NSXMLDocument *firstSource = [[NSXMLDocument alloc] initWithContentsOfURL:[NSURL fileURLWithPath:[NSString stringWithUTF8String:argv[1]]] options:0 error:&error];
            
            if (!firstSource && error)
            {
                NSLog(@"error: %@", error);
            }
            
            if ([firstSource.rootElement.name isEqualToString:@"BATCH"])
            {
                for (NSUInteger i=1; i<(argc-1); i++)
                {
                    NSXMLDocument *source = [[NSXMLDocument alloc] initWithContentsOfURL:[NSURL fileURLWithPath:[NSString stringWithUTF8String:argv[i]]] options:0 error:&error];
                    
                    for (NSXMLElement *optionElement in [source nodesForXPath:@"/BATCH/*" error:&error])
                    {
                        NSString *optionPath = [[optionElement attributeForName:@"Path"] stringValue];
                        //                NSString *optionContent = [optionElement stringValue];
                        
                        
                        
                        
                        if ([optionPath hasPrefix:@"\\\\"])
                        {
                            NSMutableArray *nodes = [NSMutableArray array];
                            //                    __block
                            
                            [dataValueMatcher enumerateMatchesInString:optionPath
                                                               options:0
                                                                 range:NSMakeRange(0, optionPath.length)
                                                            usingBlock:^(NSTextCheckingResult *result, NSMatchingFlags flags, BOOL *stop)
                             {
                                 NSRange keyRange = [result rangeAtIndex:1];
                                 NSRange valueRange = [result rangeAtIndex:2];
                                 
                                 Node *node = [[Node alloc] init];
                                 
                                 if (keyRange.location != NSNotFound)
                                 {
                                     node.key = [optionPath substringWithRange:keyRange];
                                 }
                                 
                                 if (valueRange.location != NSNotFound)
                                 {
                                     node.value = [optionPath substringWithRange:valueRange];
                                 }
                                 
                                 [nodes addObject:node];
                                 
                             }];
                            
                            NSMutableDictionary *current = root;
                            for (NSUInteger nodeIndex=1; nodeIndex<nodes.count; nodeIndex++)
                            {
                                Node *node = nodes[nodeIndex];
                                if (nodeIndex==(nodes.count-1))
                                {
                                    node.optionSet = [nodes.firstObject key];
                                }
                                NSMutableDictionary *child = current[node];
                                if (child == nil)
                                {
                                    child = [NSMutableDictionary dictionary];
                                    current[node] = child;
                                }
                                current = child;
                            }
                            
                        }
                    }
                }
                
                NSMutableString *buffer = [NSMutableString string];
                [buffer appendString:@"<?xml version='1.0' encoding='UTF-8' standalone='yes'?>\n"];
                
                Node *node = [[Node alloc] init];
                node.key = @"BDM";
                
                
                dumpNodes(@{node : root}, 0, buffer, NO, YES);
                [buffer writeToFile:[NSString stringWithUTF8String:argv[argc-1]] atomically:YES encoding:NSUTF8StringEncoding error:&error];
            }
            if ([firstSource.rootElement.name isEqualToString:@"BATCH"])
            {
                
            }
            else
            {
                NSLog(@"Unknown root element: %@", firstSource.rootElement.name);
            }
            
            return 1;
        }
     }
    return 0;
}
